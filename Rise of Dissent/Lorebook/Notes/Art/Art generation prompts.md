# GENERAL


>https://en.wikipedia.org/wiki/Camera_angle

***








# MAPS


>An imaginary fantasy black and white ink sketch map

***








# LOCATIONS


>A fantasy-alien assyrian city with steampunk architecture and a large steampunk temple at the center. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy-alien assyrian airport situated in a city with steampunk architecture and a large renaissance airship docking into it. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>Bustling market in a fantasy-alien assyrian city with steampunk architecture. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy assyrian temple with steampunk architecture in the middle of an alien desert and pilgrims walking towards it. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>Steampunk celtic temple. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy traditional chinese city with steampunk architecture and a large temple at the center. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy traditional assyrian chinese city with steampunk architecture set on a flying island among clouds. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A town with viking and northern african mudbrick architecture. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A town with viking and northern african mudbrick architecture located in an alien fantasy winter setting. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A town with viking and northern african mudbrick architecture located in a winter setting with giant mushrooms. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A Star Wars Naboo-like building standing tall at the edge of a cliff, against the backdrop of foggy mountains. It features intricate details such as domes and towers with oval roofs. In the foreground, we can see several bushes with leaves adding some life to the otherwise grey landscape. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A Star Wars Naboo-like hut fishing village sitting on a lake, against the backdrop of foggy mountains. In the foreground, we can see several bushes with leaves adding some life to the otherwise grey landscape. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A large Star Wars Naboo-like city sitting against the backdrop of foggy mountains. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.


### - Watercolor

>A bird's eye view of a bustling market in a fantasy-alien assyrian city with steampunk architecture. It should be a watercoloured sketch art, draw it in the style of Michael Kirkbride or Hugo Pratt.

>A steampunk Aztec temple. It should be a watercoloured sketch art, draw it in the style of Michael Kirkbride or Hugo Pratt.

>A bird's eye view of a temple in a fantasy-alien assyrian city with steampunk architecture. It should be a watercoloured sketch art, draw it in the style of Michael Kirkbride or Hugo Pratt.

***








# FAUNA & FLORA


>An outlandish grasslands scenery with some tall weird coniferous trees. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>An outlandish grasslands scenery with some tall coniferous trees. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.


>An outlandish grasslands scenery with weird coniferous shrubs. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>An outlandish grasslands scenery with coniferous shrubs. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A giant imaginary marine creature floating at the surface of an ocean. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A giant imaginary aquatic creature floating at the surface of an ocean. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A giant imaginary marine creature floating, lying down half submerged in the ocean. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>An alien plant, standing tall with slender stalks that bears elongated grains. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A bird's eye view of a dense alien jungle composed of towering trees adorned with iridescent, oversized fronds. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A hilltop view of an imaginary alien jungle composed of towering trees adorned with iridescent, oversized fronds. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A natural landscape depicting a temperate fantasy desert. It should be a simple hand drawing sketch art, draw it in black and white ink.

>A field of an imaginary alien forest. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy outlandish alien natural landscape with a desert and a cliff. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy outlandish alien natural landscape with a desert, a cliff and weird vegetation. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy outlandish alien natural landscape with a salt cave, a cascade and weird vegetation. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>An cold mountaneous landscape with some vegetation. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>An alien cold mountaneous landscape with some vegetation. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy world map for for a fantasy alien setting, planet or continent. Don't include any writtings or annotations It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>An alien bug-dinosaur-like creature reminiscent of something that you would find in Morrowind in a bare fantasy outlandish alien natural landscape drinking from a lake. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A large alien bug-elephant-like creature reminiscent of something that you would find in Morrowind in a bare fantasy outlandish alien natural landscape. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A large winged alien bug reptilian dragon creature. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.


### - Watercolor

>A large winged alien bug reptilian dragon creature in an alien desert. It should be a watercoloured sketch art, draw it in the style of Michael Kirkbride or Hugo Pratt.

***








# PEOPLE


>A fantasy-renaissance steampunk knight holding a spear. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy roman steampunk centurion holding a spear. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy roman steampunk centurion witha beard holding a spear. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy-renaissance steampunk knight riding a mechanical creature in an alien swamp. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy-renaissance steampunk knight riding a mechanical creature in a bustling market from a fantasy-alien assyrian city with steampunk architecture. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy-renaissance steampunk soldier talking with a pretty fantasy neo-assyrian woman with an exotic and tribal look wearing thin clothes. She has long dark hair that cascades down her back. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>ancient persian steampunk woman. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>ancient persian steampunk woman with an exotic and tribal look throwing a spell. Her face is painted in intricate designs. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy-renaissance steampunk neo-assyrian woman with an exotic and tribal look throwing a spell. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>Fantasy persian steampunk warrior woman wearing a heavy armor, carrying a spear and riding a mechanical creature in an alien natural landscape. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy-renaissance steampunk neo-assyrian soldier woman with an exotic and tribal look holding a staff. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy steampunk soldier standing on the deck of an assyrian steampunk airship. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy neo-assyrian woman with a taller alien steampunk-renaissance mech standing right besides her. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>Young fantasy persian assyrian steampunk enchantress with an exotic and tribal look. Her face is painted in intricate designs she has short red tied hair. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>Young fantasy persian assyrian steampunk woman with an exotic and tribal look. Her face is painted in intricate designs that draw attention to her eyes and lips. She has long dark hair that cascades down her back, framing her face perfectly. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy-renaissance alien mage reading a sci-fi looking book or scroll inside an assyrian steampunk tavern. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A beautiful young fantasy steampunk skinny dwarf woman wearing thin traditional chinese clothing. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A young fantasy steampunk assyrian chinese skinny dwarf woman wearing very thin traditional clothing. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt

>Young fantasy asian chinese assyrian steampunk woman. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>fantasy chinese elf woman. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy steampunk warrior dwarf wearing traditional chinese armor and clothing. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A young fantasy persian assyrian woman wearing a heavy steampunk armor. She's squatting or crouched with her back turned while showing her face to the viewer. She's wearing a veil or scarf covering her entire face that draws attention to her eyes. She has long red hair that cascades down her back. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A young fantasy persian assyrian woman wearing a heavy steampunk armor and holding a spear. She's squatting or crouched with her back turned while showing her face to the viewer. She's wearing a veil or scarf covering her face that draws attention to her eyes. She has short red hair that cascades down her neck. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy renaissance noble steampunk elven lady. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A crouching young fantasy greek hoplite woman wearing a steampunk armor and holding a spear. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A crouching renaissance steampunk mech woman holding a spear. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy viking zulu warrior with slight alien reptilian features. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A noble fantasy viking zulu lady with slight alien reptilian features. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A pretty fantasy young viking zulu lady with slight alien reptilian features. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A cyborg greek sorceress with a taller steampunk-renaissance mech standing right besides her. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A naboo steampunk cyborg greek sorceress reading a book in a cold mountaneous landscape. It should be a simple hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.


### - Watercolor

>A fantasy renaissance steampunk knight riding a mechanical creature in an alien desert. It should be a watercoloured sketch art, draw it in the style of Michael Kirkbride or Hugo Pratt.

>A bird's eye view of a fantasy renaissance steampunk knight riding a mechanical creature walking accross a bustling market in an alien assyrian city with sci-fi architecture. It should be a watercoloured sketch art, draw it in the style of Michael Kirkbride or Hugo Pratt.

***








## MACHINES


>A fantasy-renaissance airship flying accross an esoteric alien desert. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy-renaissance steampunk mech holding a spear. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy-renaissance steampunk mech walking accross a bustling market in an alien assyrian city with sci-fi architecture. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A large greek roman renaissance fantasy steampunk airship. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A large chinese japanese fantasy steampunk airship. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>A fantasy steampunk airship at bay in a large renaissance garage. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.


### - Watercolor

>A fantasy-renaissance steampunk mech walking accross a bustling market in an alien assyrian city with sci-fi architecture. It should be a watercoloured sketch art, draw it in the style of Michael Kirkbride or Hugo Pratt.

>A bird's eye view of a fantasy-renaissance steampunk mech walking accross a bustling market in an alien assyrian city with sci-fi architecture. It should be a watercoloured sketch art, draw it in the style of Michael Kirkbride or Hugo Pratt.

>A large chinese japanese fantasy steampunk airship. It should be a watercoloured sketch art, draw it in the style of Michael Kirkbride or Hugo Pratt.

>A large greek roman renaissance fantasy steampunk airship flying over an alien desert valley. It should be a watercoloured sketch art, draw it in the style of Michael Kirkbride or Hugo Pratt.

***








## ITEMS


>An old scroll inscribed with magic runes. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>An old steampunk clay tablet inscribed with magic cuneiform like runes. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.

>An old steampunk assyrian flask or potion. It should be a hand drawing sketch art, draw it in black and white ink in the style of Michael Kirkbride or Hugo Pratt.