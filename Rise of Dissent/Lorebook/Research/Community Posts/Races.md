# Fictional Races




#### 1

One thing I like about fictional "races" is that they can provide a way to make a society radically different from anything we know without making us question it the way we would if they were human.

For example, with a fictional species maybe only the females are warriors, while males care for the young from birth, and they live deep under ground and survive off of mushrooms they harvest.

That would raise lots of questions if they were human.

But they're not human. The children don't need breast feeding, and the males are much smaller and weaker. They don't need to be exposed to sunlight for their bodies to produce what they need. They can use echolocation and infrared vision, and they don't have the nutritional requirements of humans.

A radically different human society raises questions. An alien inhuman society could be explained by a difference in biology.

#### 2

The main reason I have for different sapient beings is to explore how their different biology, and subsequent cultures, interact with humans.

If a race could be easily replaced by humans or vice versa, then they would be at some point in early history just like we did to the neanderthal. There would have to be some circumstances causing or forcing co-existence.

I have 4 sapient species, including humans. Verseri, Mulden, Humans, and the last currently unnamed. The way I justify them are:

- Verseri - Came to the planet in a crashed starship with wiped memories and self-destructing hardware. Most theories involve them being criminals and dumped.

- Mulden - Being sapient robins the size of housecats, they "flew under the radar". They were only realised to be sapient recently, despite the planet being at around 1900's equivalent

- Unnamed - The planet has a axial tilt of about 60 degrees, causing an arctic equator and tropical poles. Humans evolved in the southern hemisphere, and the unnamed species in the northern. they didn't come into contact until humanity was in their renaissance and made the trek, discovering the unnamed still in their medieval era.

Once you've established them, then the culture shock stories are immense. How do humans react to what they believe to be crystalline golems crashing into the planet? How do they react to large, slothlike beings with large noses roaming the plains for fresh plants to replenish their chloroplast? How to sapient robins react to finding out their is a whole world outside their forests, build by these odd ground-dwelling apes?

How do you design weaponry for birds with only their feet to hold things?

How do liquid beings navigate a cold environment without freezing?

To me, the benefits of additional races are the exploration of non-human, or even non-humanoid, histories and technologies.