Main objective for a character is to gain fulfillment while maintaining resilience.

Fulfillment allows a character to level up and increase its stats (strength, personality, intelligence or wisdom).

Resilience allows a character to put up with failure and not give up in its endeavors.

Fulfillment points are earned after successfully performing actions (the harder the action the more fulfillment earned).

Resilience points are lost after failing when performing actions (the more effort put into an action the more resilience lost).

An action's difficulty is measured through its check score.

A traditional 'character ability + dice roll' system is used to determine outcomes but before rolling, a character can also wager some resilience points in order to increase its chances of success.

If successful, the character earns an amount of fulfillment points equal to the action's total check score (and no resilience is lost).

If unsuccessful, the character loses all the resilience points wagered for the action (and does not gain any fulfillment).

To recover its resilience points, a character must rest itself or engage into some form of relieving activity.

Bonus: the maximum amount of resilience a character can wager when performing an action depends on the type of motivation behind the action being performed (survival - 4, agency - 3, self-esteem - 2, leisure - 1, inanity - 0, curiosity - 1, altruism - 2 and integrity - 3).

***

It might work but I guess it all depends on how random is the chance of success/failure i.e. the resolution mechanics: too random and the whole system feels pointless, too deterministic and the narrative potential of fulfillment/resilience moments gets watered down.

Also, tying the recovering of resilience points and the max amount of resilience you can spend on arbitrary or unimportant tasks is not interesting and just takes away agency from the players. It would be better if it was instead tied on the choices s/he makes, as it happens with earning fulfillment points after a success.

***

Thanks a lot for your feedback and yeah I totally agree. I just needed to check if I could get my point across and it seems like it worked. And nobody said this was too convoluted or over-engineered so that's good too.

But now on to fine-tuning the mechanic I guess.. You're right about not having to force players to spend resilience when performing meaningless actions. And this is why I tried to introduce the "motivation" mechanic at the end of my post. Where the amount of resilience points a character can wager is capped relative to the motivation behind the action it is trying to perform. This is how I see it:

>Case 1: if an action/choice is too easy for a character then it doesn't need a skill check to begin with. The action is resolved automatically and the character doesn't earn any fulfillment points. Breaking the narrative flow in this context is completely unnecessary, I 100% agree with you.

>Case 2: if an action/choice is technically "hard" but the "motivation" behind it is unimportant (aka the character only does it out curiosity, fun or doesn't even know why it does it), then the character can't wager any resilience points to help influence the outcome. This means that if a character is not trying to accomplish something meaningful for itself or others (fight for survival or someone else's life, increase its own wealth or status, follow a revenge or redemption arc, etc...), then it doesn't really see a point in investing resilience points into what it's doing. Meaningless actions can't wager resilience points because characters don't see a point in them. (Not sure if this mechanic is easy to understand or even fun to implement thought).

Finally, about the first part of your message, would you recommend any straightforward already existing system that could help balance the odds for the game? I was thinking of a formula like

>stat value * resilience points used + dice roll > action's check score => success

but I'm not sure if this is actually fun for players. I'm afraid they might have to keep track of too many variables after a while. And by that I mean that this is just the core system without all the extra features that can be added on top of it (equipment bonuses, extra modifiers, mental and physical health, alignment, affinity with other characters etc....)

***

You're welcome. By "arbitrary or unimportant" I meant where you wrote "must rest itself or engage into some form of relieving activity", which should not give any points based on what you said above about tasks that are too easy or not meaningful. All the player has to do is to tell "I go having fun" or "I sleep and meditate". Aside from being inconsistent with the fulfillment mechanics (that is recovered by taking actual risks) and what you said above, all it does is making it impossible to recover resilience during an adventure. If that's enough for you, godspeed, but IMO it would work better if it was integral to the flow of the adventure, not just something that happens off-screen.

About the resolution mechanics, I don't know what to recommend until I know what you actually want. You can get more "realistic" distribution by rolling 2 or more dice and summing them up; you can get wilder distribution by rolling a single dice; you can do away with math by using a dice pool where any roll above X counts as a success and the number of successes tell what's happening. And so on.

For sure adding up many bonuses and a dice roll leads to all kind of problems, especially excessive book-keeping, too much stress because of having to do all that math, and dice becoming less and less important the more the bonuses add up. You know what I'm talking about if you ever played D&D, which this reminds me a lot.

You might want to take a simpler, more sensible and more satisfying route. Personally I'm a fan of dice pools, it just so nice to get more and more dice, but you still have to roll them and cross your fingers, you never get to the point when you're rolling 4+2+2+1+1+1d10 to beat a score of 11 only to have the GM waive the roll because you can't fail...

***

Yes I was reading about dice pools yesterday and tbf that's probably the way to go. Something like "stat level determines which type of dice to use, resilience points determine how many dices to roll and action check score determines how many results should be above a certain threshold value" or something like that. Might be trickier to balance but definitely more fun for players.

Also my bad, I understand your point about the resting mechanic now (and it makes sense). Oc I want to make it an integral part of the adventure but I really don't know how I should go about it without having to bloat the rules too much.

Trying to influence outcomes requires resilience, and taking action "in order to recover resilience" feels counter-intuitive or self-defeating at worse. Should I add a parallel mechanic for resting...? Should only easy actions help recover resilience? What about easy but "boring" actions? Should I add a "fun/boring" variable for every action a player takes? Wouldn't that be too much for the GM to keep track of? Do I really want players to discuss how fun or boring an action is every time there's dice throw?

I've been thinking about this issue a lot but still haven't found a way to solve it properly. As you pointed out, the current idea is that rest and resilience recovery are heavily dependent on context and environment rather than player choice or agency. Which means that yes, a character cannot recover its resilience if it's in the middle of a stressful situation or adventuring. But this also means that other players (outside of a character's agency) can also influence its recovery if they want to. Things like cracking jokes, motivational speeches, playing music or use of abilities tied to personality can be ways of doing just that. Drinking alcohol or taking drugs could also be an option if the character is alone (which can bring psychological downsides if done excessively) and in that sense, items like food and drinks could be considered integral resources for the game (as important as health potions for example).

EXTRA NOTE: A character can help recover another character's resilience but CANNOT deteriorate or decrease it. A character can deteriorate or reduce another character's fulfillment (by "psychologically" attacking it) but CANNOT increase or improve it.

I was also trying to find incentives for players to spec into stats for that sole purpose. For instance a character with high wisdom wouldn't "need much" to recover its resilience whereas a character with low wisdom would require some amount of opulence and excitement.

Anyway thanks a lot for taking the time to answer my posts, really appreciate. I still don't know how to go about this (and pbly neither do you) so I need to see if there are any existing games that implement a similar mechanic in a more engaging way.

***

The way you described it sounds a lot like Darkest Dungeon (a video game) deals with stress: there's a few classes that can recover it during play and a few actions that can help, but it's almost impossible not to accumulate more and more stress (in your case, lose more and more resilience) and it can be "healed" only by ending the adventure and not using the character for a while, which means many adventures or just one if you pay lot of money.

In your case:

There must still be something that makes players want to recover resilience. Darkest Dungeon makes the character do irrational things or even die, but it will still work if losing it is inconvenient in any other way.

if losing too much resilience can lead to dramatic consequences it should be able to be recovered in play in small doses, just enough to survive if done right; they should be of limited use and there should be still a risk of such consequences anyway, otherwise the dramatic part gets watered down. If it's just a sort of counter that limits actions but doesn't risk much more than that it's better if it can't be recovered in play because this also would water down its importance.

It's ok if most of the recovery happens between adventures but that should still have consequences. Assuming the players only have one character and therefore they can't leave the stressed one inactive even for a single adventure, they should lose something else to recover resilience, something that is likely to have an impact in the following adventures so that resilience can still matter in play. It might cost much money or other limited resource, inflict a temporary disadvantage on the character, change the attitude of an important NPC, introduce new difficulties and so on. Things that don't just affect the character but that are also likely to affect others and the world they live in.

***

I never played Darkest Dungeon but I understand where you're coming from. I know some of its mechanics and they're definitelly pretty close to what I want to achieve.

1 - A character "doing irrational things" when no resilience works really well, but I'd have to come up with extra rules and tables for that purpose (or just straight out copy an already existing system). And although this is something I might be doing in the future, for now I just want to keep it as simple/minimalistic as possible so I'm keeping this idea for later.

2 - This is what I got so far: when your resilience reaches 0, you loose all fulfillment earned until that point + you cannot "wager" any more resilience points while performing actions (obviously). And fulfillment cannot be earned without investing some resilience, so you're basically loosing all of your progress and kept at square one until you "take a break".

More dramatic approaches included just straight out "downgrading" a character (have it loose stat points or levels) at the end of each adventure (if its resilience went below 0 too often or something). Or have it leave the party and quit the adventure in the middle of it. Which in my opinion were the worse things that could happen (from a player's perspective) to a character other than just dying.

When it comes to the "being able to recover enough resilience to keep it going" thing, I'm still stuck on the fact that environmental factors rather than player choice should govern this mechanic... A golden rule could be that, as long as a character feels safe and is able to catch a breath then yeah, it should at least be able to recover 1 resilience point (but leave it vulnerable to attacks as a drawback). This means that it would be up to the GM to decide how "hardcore" a game should be when choosing how often these "resting moments" happen.

3 - Interesting, I never thought about that. And it makes total sense. I guess it depends on how dramatic the resilience mechanic is. If very dramatic then having your points go back to full at the end of each game for free seems fair for the players. But if the rule isn't dramatic enough then yes, having characters "spend something" in between adventures is the way to go. Maybe money and/or fulfillment points... Or something that affects lore/npcs as you pointed out. I'll have to think about it.

***

Well, it seems you have figured out what you want and it might be interesting to play while also not being just a copy/paste of something else. Glad to have being able to help, good luck with your project.