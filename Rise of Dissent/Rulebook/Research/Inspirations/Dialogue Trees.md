# ROADWARDEN




## Attitudes


>Are these just used for first impressions...?

- Friendly (*Charm* & *Eloquence*) : Be supportive and cordial.
- Playful (*Creativity* & *Eloquence*) : Use a joke or a witty comment.
- Distanced (*Composure* & *Eloquence*) : Hide your emotions.
- Intimidating (*Strength* & *Eloquence*) : Hint at a threat.
- Vulnerable (*Humility* & *Eloquence*) : Be sad, tired, afraid or hopeless.




## I ask them about their lieutenant


>Context: Protagonist arrives at a millitary camp right at the begining of the game and meets two soldiers.

- **Friendly** : I was hoping to meet with your lieutenant. Could we talk for a bit?
- **Playful** : I'm sure you're busy, so if you could just introduce me to your lieutenant...
- **Distanced** : I'm looking for your lieutenant.
- **Intimidating** : I'd rather speak with someone who's not going to waste my time.
- **Vulnerable** : I had a long journey. I need to have a word with your lieutenant.