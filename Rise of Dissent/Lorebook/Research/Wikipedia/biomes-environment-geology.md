# GENERAL




>https://askabiologist.asu.edu/explore/biomes

***








# DESERT




## General


Deserts make up the hottest biome, but can also get cold temperatures in winter. Such temperature swings make this an extreme environment, where many animals have to burrow underground to find more stable temperatures in order to survive. Plants and animals here must be able to withstand long periods without water.




## Sahara desert


The world's largest hot non-polar desert, covering the northern portion of the African continent. It has an area of 4,619,260 km2 in the hot, hyper-arid centre, surrounded on the north, south, east, and west by regions with higher rainfall and more vegetation. Its surface ranges from large areas of sand dunes (erg), to stone plateaus (hamadas), gravel plains (reg), dry valleys (wadis), and salt flats. The only permanent river that crosses the ecoregion is the Nile River, which originates in east Africa and empties northward into the Mediterranean Sea. Some areas encompass vast underground aquifers, resulting in oases, while other regions severely lack water reserves.

***








# GRASSLAND




## General


Sometimes called plains or prairie, grasslands are almost entirely made of short to tall grasses with no trees. This land type gets just enough rain to help flowers and herbs grow, but stays dry enough that fires are frequent and trees cannot survive. Here we find large mammals that often travel together in huge herds. Although the vegetation is dominated by grasses, sedge and rush can also be found along with variable proportions of legumes, like clover, and other herbs.

Grasslands occur naturally on all continents except *Antarctica* and are found in most ecoregions. Furthermore, grasslands are one of the largest biomes on earth and dominate the landscape worldwide. There are different types of grasslands: natural grasslands, semi-natural grasslands, and agricultural grasslands.




## Hulunbuir Grassland


Hulunbuir Grassland (China's Inner Mongolia Autonomous Region) is famous for its lush plants, fertile soil and beautiful scenery. It is known as the "Grass Kingdom" and covers an area of about 100,000 square kilometers. There are over 3,000 rivers and 500 lakes, providing the pastures with extremely fertile soil for the grass to grow.


### - Flora

Tussock grass, rhizomatous grass, sedge.


### - Fauna

Horses, sheep, cattle, gazelles, cashmere goats, camels.




## Great Hungarian Plain


The Great Hungarian Plain (also known as *Alföld*, *Great Alföld* or *Nagy Alföld*) is a plain occupying the majority of the modern territory of Hungary. Much like the American Midwest, the Great Hungarian Plain consist of fertile flatlands crisscrossed by meandering rivers that nourish the soil and provide the perfect place for growing crops and herding livestock. Flood control, irrigation, and swamp drainage projects have added large areas of cultivable land.

The original arid grasslands or steppe (Hungarian *puszta*) survive in the *Hortobágy* area east of Budapest, and covers a total area of about 50,000 km2. The characteristic landscape is composed of treeless plains, saline steppes and salt lakes, and includes scattered sand dunes, low, wet forests and freshwater marshes along the floodplains of the ancient rivers.
It is strongly associated with traditional Hungarian breeds of domestic animal including the Hungarian Grey breed of cattle, the  breed of , the  breed of horse and the Racka breed of horned sheep.


### - Flora

Cereals, fodder crops, livestock, vegetables, and fruit are widely raised.


### - Fauna

Przewalski's horse (last wild horses of Europe), Nonius horse, Hungarian grey cattle, racka sheep, mangalitsa woolly pig, water buffaloes, more than ten thousand birds in autumn.

***








# SAVANNA




These tree-studded grasslands receive enough seasonal rainfall so that trees can grow in open groups or singly throughout. The animals living here have long legs for escaping predators and usually are seen in herds. A combination of fire and grazing animals are important for maintaining the savannah.

***








# TEMPERATE FOREST




This is the kind of forest where there are four relatively distinct seasons. Many of the trees shed their leaves in the fall and become inactive through the cold winter. In these forests, you find deer, woodpeckers, and bears, some of which hibernate through the winter.

***








# TAIGA 




Taiga is the largest land (terrestrial) biome in the world.  It is made up of mainly conical-shaped evergreen trees with needle-like leaves. These trees are called conifers because their seeds are clumped into cones. The taiga has long, cold winters when most mammals hibernate and birds migrate, or leave the area because the winters are too cold for them to stay. Animals like weasels, grouse and rabbits that do not migrate or hibernate grow dense feathers or fur and turn white to match the snow.

***








# TROPICAL FOREST




You probably picture tropical rainforest as a jungle, where it stays warm all year. There are too many animals to count and the huge numbers of trees keep their leaves year-round. Many of these forests get so much rain that there isn't even much of a dry season – more like a rainy season and a rainier season.

***








# TUNDRA




Tundra is flat and cold with low plants like grass and moss that only grow during the short summer. A thick layer of ice lies just below the shallow soil (permafrost) all year around, and trees cannot penetrate it to anchor their roots. Many birds visit the tundra in the summer to nest, but most escape the winter by migrating to warmer areas. Mice and other small mammals stay active during the winter in protected tunnels under the snow.

***