#!/usr/bin/python3.8
# -*-coding:Utf-8 -*




import datetime
import random
import csv


script_id = "v12_1_dice_probabilities_script.py"
now = datetime.datetime.now()
debug_mode = False
number_of_rolls = 100000

number_of_success = 0
number_of_limited_success = 0
number_of_limited_failure = 0
number_of_failure = 0


challenge = [
	["# Average Challenge (2)", 2],
	["# Difficult Challenge (3)", 3],
	["# Daunting Challenge (4)", 4]
]

position = [
	["## Controlled Position (4)", 4],
	["## Risky Position (3)", 3],
	["## Desperate Position (2)", 2]
]

number_of_die = [
	["### - Standard Roll (4 die)", 2],
	["### - Daring Roll (6 die)", 3]
]

ability_scores = [
	["1/1", 4, 4],
	["1/2", 4, 6],
	["1/3", 4, 8],
	["1/4", 4, 10],
	["1/5", 4, 12],
	["2/2", 6, 6],
	["2/3", 6, 8],
	["2/4", 6, 10],
	["2/5", 6, 12],
	["3/3", 8, 8],
	["3/4", 8, 10],
	["3/5", 8, 12],
	["4/4", 10, 10],
	["4/5", 10, 12],
	["5/5", 12, 12],
]

header = ["Ability Scores", "Success", "Limited Success", "Limited Failure", "Failure"]

results_table = [
	[script_id, " ", " ", " ", " "],
	[str(now), " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "],
    [" ", " ", " ", " ", " "]
]

results_row = [" ", " ", " ", " ", " "]




#
# Roll a single die
#
def rollDice (loc_dice_size):

	loc_result = random.randint(1, loc_dice_size)

	if debug_mode:
		print("Result: ", loc_result)

	return loc_result




#
# Perform a single action and record outcome
#
def rollAction (loc_number_of_die, loc_dice_size_1, loc_dice_size_2, loc_challenge, loc_position):

	global number_of_success
	global number_of_limited_success
	global number_of_limited_failure
	global number_of_failure

	loc_success_score = 0
	loc_trouble_score = 0
	loc_neutral_die = 0

	for x in range(loc_number_of_die):
	
		loc_result = rollDice (loc_dice_size_1)

		if loc_result > 3:
			loc_success_score += 1

		if loc_result == 3:
			loc_neutral_die += 1

		if loc_result < 3:
			loc_trouble_score += 1

	for x in range(loc_number_of_die):
	
		loc_result = rollDice (loc_dice_size_2)

		if loc_result > 3:
			loc_success_score += 1

		if loc_result == 3:
			loc_neutral_die += 1

		if loc_result < 3:
			loc_trouble_score += 1

	if debug_mode:
		print("")
		print("Success Score: ", loc_success_score)
		print("Trouble Score: ", loc_trouble_score)
		print("Neutral Die:   ", loc_neutral_die)
		print("")

	if loc_success_score >= loc_challenge and loc_trouble_score < loc_position:
		number_of_success +=1
		if debug_mode:
			print("Success")
			print("")
			print("")

	if loc_success_score >= loc_challenge and loc_trouble_score >= loc_position:
		number_of_limited_success +=1
		if debug_mode:
			print("Limited Success")
			print("")
			print("")

	if loc_success_score < loc_challenge and loc_trouble_score < loc_position:
		number_of_limited_failure +=1
		if debug_mode:
			print("Limited Failure")
			print("")
			print("")

	if loc_success_score < loc_challenge and loc_trouble_score >= loc_position:
		number_of_failure +=1
		if debug_mode:
			print("Failure")
			print("")
			print("")




#
# Clear all recorded outcomes
#
def clearOutcomes ():

	global number_of_success
	global number_of_limited_success
	global number_of_limited_failure
	global number_of_failure
	global results_row

	number_of_success = 0
	number_of_limited_success = 0
	number_of_limited_failure = 0
	number_of_failure = 0
	results_row = [" ", " ", " ", " ", " "]




#
# Add {loc_number_of_lines} empty lines at the bottom of {results_table}
#
def addEmptyLine (loc_number_of_lines):
	for index_number_of_lines in range(loc_number_of_lines):
		results_row = [" ", " ", " ", " ", " "]
		results_table.append(results_row)




#
# Generate a table of action outcomes for standard or daring rolls and add it at the bottom of {results_table}
#
def computeResultsTable (loc_index_number_of_die, loc_index_challenge, loc_index_position):

	results_row = [number_of_die[loc_index_number_of_die][0], " ", " ", " ", " "]
	results_table.append(results_row)
	addEmptyLine (1)

	results_row = header
	results_table.append(results_row)

	for index_ability_scores in range(len(ability_scores)):

		for index_number_of_rolls in range(number_of_rolls):
			rollAction (number_of_die[loc_index_number_of_die][1], ability_scores[index_ability_scores][1], ability_scores[index_ability_scores][2], challenge[loc_index_challenge][1], position[loc_index_position][1])

		if debug_mode:
			print("#-------------------------------------")
			print("#--------------RESULTS----------------")
			print("#-------------------------------------")
			print("Number of Rolls: ", number_of_rolls)
			print("Die per Roll:    ", number_of_die[loc_index_number_of_die][1]*2)
			print("Ability Die:     ", ability_scores[index_ability_scores][1], "/", ability_scores[index_ability_scores][2])
			print("Challenge:       ", challenge[loc_index_challenge][1])
			print("Position:        ", position[loc_index_position][1])
			print("#-------------------------------------")
			print("Success:         ", round((number_of_success / number_of_rolls) * 100), "%")
			print("Limited Success: ", round((number_of_limited_success / number_of_rolls) * 100), "%")
			print("Stalemate:       ", round((number_of_limited_failure / number_of_rolls) * 100), "%")
			print("Failure:         ", round((number_of_failure / number_of_rolls) * 100), "%")
			print("#-------------------------------------")

		results_row = [ability_scores[index_ability_scores][0],
    		str(round((number_of_success / number_of_rolls) * 100)) + '%', 
    		str(round((number_of_limited_success / number_of_rolls) * 100)) + '%', 
    		str(round((number_of_limited_failure / number_of_rolls) * 100)) + '%', 
    		str(round((number_of_failure / number_of_rolls) * 100)) + '%']

		results_table.append(results_row)
		clearOutcomes()

	addEmptyLine (2)




#
# Generate result tables depending on {position}
#
def computePositionTables (loc_index_position, loc_index_challenge):

	results_row = [position[loc_index_position][0], " ", " ", " ", " "]
	results_table.append(results_row)
	addEmptyLine (2)

	for index_number_of_die in range(len(number_of_die)):
		computeResultsTable(index_number_of_die, loc_index_challenge, loc_index_position)

	addEmptyLine (2)




#
# Generate result tables depending on {challenge}
#
def computeChallengeTables (loc_index_challenge):

	results_row = [challenge[loc_index_challenge][0], " ", " ", " ", " "]
	results_table.append(results_row)
	addEmptyLine (4)

	for index_position in range(len(position)):
		computePositionTables(index_position, loc_index_challenge)

	addEmptyLine (4)




#
# Starting point for the script
#
def main():

	for index_challenge in range(len(challenge)):
		computeChallengeTables(index_challenge)

	results_row = ["END OF RESULTS", " ", " ", " ", " "]
	results_table.append(results_row)




if __name__ == '__main__':
	main()

with open('v12_1_dice_probabilities_script_results.csv', 'w', encoding='UTF8') as f:
    writer = csv.writer(f)

    for x in range(len(results_table)):
    	writer.writerow(results_table[x])
