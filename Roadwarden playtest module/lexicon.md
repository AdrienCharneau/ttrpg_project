- **Cove** : a small sheltered bay in the shoreline of a sea, river, or lake, or a recess or small valley in the side of a mountain.

- **Creek** (*crique*) : a small stream, often a shallow or intermittent tributary to a river.

- **Ford** (*gué*) :

<!-- https://en.wikipedia.org/wiki/Ford_(crossing) -->

- **Game trail** (*chemin de désir*) :

<!-- https://en.wikipedia.org/wiki/Desire_path -->

- **Gnat** (*moucheron*) :

<!-- https://en.wikipedia.org/wiki/Gnat -->

- **Gruel** :

<!-- https://en.wikipedia.org/wiki/Gruel -->

- **Hemp** (*chanvre*) : a genus of flowering plants in the family Cannabaceae. It has long been used for fibre, seeds, oils, leaves as vegetables or juice, medicinal purposes, and as a recreational drug.

- **Linen** (*lin*) : a textile made from the fibers of the flax plant.

- **Meadow** (*prairie*) :

<!-- https://en.wikipedia.org/wiki/Meadow -->

- **Ointment** : a homogeneous, viscous, semi-solid preparation; most commonly a greasy, thick water-in-oil emulsion having a high viscosity, that is intended for external application to the skin or mucous membranes.
