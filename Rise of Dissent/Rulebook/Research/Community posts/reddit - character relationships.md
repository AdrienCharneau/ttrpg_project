Edit: This is for a sub-system in a larger game, rather than a self-contained game. This game is inspired by the Fire Emblem series, and this sub-system is meant to emulate Support Conversations from that series.

I'm creating a game where inter-character relationships are a key to nailing the right gamefeel. After binging hours of character arc and screenwriting 101 videos, I've come up with an idea that seems like it hits on what I want, but I'd like second opinions.

Design Goals:

    I need a mechanic that allows characters to slowly reveal their backstory through periodic sessions of structured roleplay.

    It needs to be highly flexible in order to create a coherent conversation via various combinations of character concepts.

    It needs the conversation to be able to ramp up in impact at a steady rate, ending in an emotional climax.

    It needs to be able to create a unique conversation for each pair of characters (i.e. Conversation A/B needs to handle a different topic than conversation A/C, A/D, and A/E).

    It needs to be quick and light to handle the possibility of frequent character creation.

So here's what I've come up with:

    Players create a "Worldview"

    Players spend narrative xp to establish a [Bond]

    Characters strengthen the bond by spending more narrative xp to roleplay topics in progression (Like/Dislike, Belief, Ghost, Truth)

    Finished bonds change the character's worldview and create a more deeper character

A complete Worldview consists of Likes/Dislikes, Beliefs, Ghosts, and Truths. Definition time: A Like/Dislike is pretty self-explanatory. It describes things a character is openly sharing about their self. A Belief is a view the character has about the world or their self. A belief is not shared openly and may or may not be the basis for a Like/Dislike. A Ghost is a past event the character had that directly informs their beliefs. A Truth is a belief that is confirmed to be accurate. A Truth is not necessarily an actual truth, but rather a confirmation of the character's worldview (e.g. A character might believe 'honesty is the best policy', and a Truth would confirm that's still the case after being tested). At character creation, a player only needs to worry about one Like or Dislike and one Belief per other relevant character. As the bond progresses, players can think about the later steps as they become relevant. At the final stage of a bond, one belief is confirmed a Lie while the other is confirmed a Truth. The Lie is discarded and both characters then accept the Truth as a new belief.

Why it should work:

What this system does is create mini character arcs between two characters. There are essentially two types of character arcs, Change (positive and negative) and Flat. The Change arc is when a character experiences a challenge to their worldview and they realize a Truth from within their self, either for better (positive) or worse (negative). A Flat arc is where a character has their belief tested and the character holds onto their belief, which changes the world around them. This system creates both a change arc and a flat arc between two characters that's only determined once the Truth is revealed in the final step. The belief that becomes Truth creates a flat arc, while the Lie creates a change arc. The system is flexible enough to accept most any kind of belief, progresses the arcs over time as narrative xp is gathered and spent, and creates a unique conversation based on the combined inputs of two characters. The structure is simple enough to be easily followed, yet still creates competent scenes even for the roleplay averse.

To go even further beyond:

I want to expand these mechanics into other areas of the game, but these are less concrete ideas. I'm considering giving each belief a ranking from 1..5 and having situations both inside and outside of Bonds modify the strength of each belief. Truths can be created or destroyed by consistent experiences that confirm or counteract each belief. On that note, I'd like to expand the modification of belief into the (very small) social interaction system. It's a subset of the skill system, and only consists of Inspire (getting people to believe you) and Confidence (resisting Inspire). This is most likely where the strengthening and weakening of beliefs would come into play. I'd also like to reward players with narrative xp when they roleplay their character according to their beliefs. This would create a gameplay loop in the narrative sphere where roleplaying according to character lets you strengthen your bonds, thereby gaining stronger beliefs and netting more narrative xp to spend on things not outlined here. Both ideas together would give me something a bit like Legends of the Wulin's Virtues and Exalted 3e's Intimacies combined. I'm already using LotW's Loresheets concept, so it'd fit right in.

I think that should cover everything. Let me know if I failed to explain something properly or if you think there's a problem with the design

***


I wanted to come up with a system where players could create characters, but not be forced to develop intricate backstories only to have that character die before they could be shared. Unshared backstory is wasted effort, so I decided the backstory should be revealed gradually. Each topic is addressed in stages so that there isn't a massive info dump. You have to reveal what a character's belief is before you can reveal why they have that belief. That's where I started researching how to create characters in a literary sense.

So at the start, you learn very surface level things because your relationship is surface level. As you grow your relationship, you go deeper you learn secrets and experiences that most people wouldn't know. Eventually you start learning about formative experiences, reasons why people have their worldview. Things like "I think the world is cruel because I grew up an orphan". These revelations can be extremely impactful because they reach at the core of what makes the character tick. They also open up the opportunity for the experience to be resolved, or healed. That's where the resolution comes in. The character's formative belief is either confirmed ("Yes, this new experience confirms the world is cruel") or replaced ("No, this new experience shows me the world can be a caring place"), and that fundamentally changes the character. That change is a huge deal, and a reward for engaging in this sub-system. The arc creates the payoff. And it takes time to build because you can't do the whole arc at once. You have to go chunk by chunk and voluntarily invest a currency in order to unlock the complete process.

***

Narrative xp can be spent on learning about the lore of the game, factions you interact with during the game, NPC backstories, and getting more involved with each of those things. For example, if you really like a random shopkeeper, you can spend narrative xp on that shopkeeper and that lets the GM know to make them more relevant in the future. You might get a discount at the store, gain a new quest, or anything along those lines. The essence of narrative xp is to use a game mechanic to tell the GM what you want more of.

***

My friend used a mechanic in a Marvel game that was favor-based. Relationships as an economy. When you acquire a "contact," you can start to trade favors (as opposed to paying someone for something). You get an x in 6 chance they're available or capable of doing what you want, and every time they oblige you, that chance goes down for the next time. It goes up when they ask something of you and you oblige. The warm fuzzy feelings part comes automatically (without awarding bonuses or some such), as it does in real life when you help someone and they help you regularly. If the NPCs have talents and assets, you don't need some abstract "friendship bonus" to a PC roll... ... the favors and the relationship itself will be enough.
___________________________________________________________

For what it's worth, I want you to know you aren't alone in your critique of these kinds of mechanics. I did years of research trying to find a satisfying "social" system to take inspiration from. Nothing I found was at all satisfactory.

In the end, I think all of my complaints can be distilled into one critical issue: social/relationship mechanics are used to control character behavior. This is a problem because it damages player agency, and subverts immersion.

My solution is to have mechanics that tell the player what their character thinks, while still giving the player complete control over the character's actions. For example, a successful attempt to barter might result in the merchant thinking "Yeah, these guys are going to use my goods for a really noble cause! I should give them a discount." However, if the merchant has been on hard times and doesn't have the money to feed his family, the player (or GM) can still have the merchant charge full price. There is still a cost to ignoring the persuasion, though. If the merchant doesn't give a discount, he risks getting stressed. This represents the fact that he was truly persuaded. The persuasion mechanic isn't all-powerful, but it does have an effect on him. He might leave the situation feeling a little guilty because he wasn't able to contribute to the noble cause. Or maybe, if the bartering character can't afford full price, the merchant leaves the scene feeling devastated because he really wishes he could have helped support the cause.

TLDR: I believe social mechanics shouldn't control character behaviors, but they should influence the player's decision-making process that leads to character behaviors.

What do you think? Is this more along the lines of discussion you were pursuing?