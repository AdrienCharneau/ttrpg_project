# The Broken Blade - Intuitive TTRPG Combat

***




>https://www.youtube.com/watch?v=Zk34TanPG3U

- The problem with ttrpgs is that it's usually unnecessarily difficult for a combatant to land a strike on their opponent.

- It makes more sense for two combatants using similar weapons to hit each other rather than both missing one another.

- What's actually difficult is safely hitting another fighter without being struck yourself.

- Even an untrained fighter can pose a significant threat to a more skilled combatant.

>*Fencing master Ridolfo Capo Ferro describes a fighter known as the "Beastial Man", who, despite lacking skill, is dangerous due to his ferocity and unpredictability. A fencer facing such an opponent can easily harm them, but risks being struck in return because the "Beastial Man" either doesn't realize that they're putting themselves in danger, or doesn't care. Ridolfo advises that to safely engage such a fighter, one must adopt specific strategies to minimize the risk of being harmed.*

- **Opposed Rolls** : characters should actively defend against enemy attacks with a "defense roll" or something. This makes it feel like they are doing something heroic, such as dodging, parrying or intercepting blows with their shield, rather than simply benefiting from an opponent's missed attack.

- **Armor** : fencing master Pietro Monte wrote about how soldiers fought while wearing armor. According to him, lightly armored soldiers had the mobility to avoid attacks by moving laterally and backward with ease. He discouraged moving backward while heavily armored, and described fully armored soldiers as being like smiths hammering each other. Unlike lightly armored soldiers, fully armored soldiers should constantly press forward to rob the enemy of space needed for powerful blows, making their armor more effective. Monte also emphasized the importance of strength for fully armored soldiers, stating that contests between them are usually decided by who is stronger.

***
