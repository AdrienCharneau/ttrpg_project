```
 __ __| |             _ ) _)  |    |                  __|                    |   
    |     \    -_)    _ \  |   _|   _|   -_)   _|    (      _ \   _` | (_-<   _| 
   _|  _| _| \___|   ___/ _| \__| \__| \___| _|     \___| \___/ \__,_| ___/ \__|
```

***








>The Bitter Coast is named for the salt marshes along the coast northwest of town. From here to the Odai River, it's rugged coastal hills, then it's roadless, uninhabited swamp all the way north to the West Gash and the Sea of Ghosts. Hla Oad is a fishing settlement north of the Odai, and the fishing village of Gnaar Mok is even farther, and both places are small, isolated, and poor. Hunting is fair, and some folks gather mushrooms, pods, and flowers for alchemists.

> ~ Fargoth, *Morrowind's in-game dialogue*

***








# INTRODUCTION




Those acquainted with Morrowind's geography are wise to avoid journeying through the putrid swamps of the Bitter Coast. Positioned on the southwestern shores of the island Vvardenfell, nestled between the tempestuous Inner Sea, the picturesque Ascadian isles and the feral West Gash, the region is devoid of significant interest for settlers, travelers and merchants. The few who live there are often outcasts seeking solitude or eking out a meager existence through fishing and scavenging. The coastal villages, though scarce, are characterized by ramshackle huts and a sense of isolation. Ancient Dunmer ruins and forgotten shrines dot the landscape, hinting at a history that has long been abandoned.




## Crime


The allure of ill-gotten gains attracts illicit activities. And the Bitter Coast is no exception to that rule. Despite its forsaking, boats laden with riches often traverse the region's coastline, encouraging crime and smuggling operations among its inhabitants.

Smugglers take advantage of the region's remoteness and convoluted waterways to conduct their operations under the cover of darkness. The lawless atmosphere, coupled with the potential for valuable cargo, creates a haven for criminal enterprises to thrive.

***



With its stagnant waters and tangled vegetation, the Bitter Coast remains a hazardous environment. 