```
 _ _|   \ | __ __| _ \   _ \  _ \  |  |  __| __ __| _ _|   _ \   \ |
   |   .  |    |     /  (   | |  | |  | (       |     |   (   | .  |
 ___| _|\_|   _|  _|_\ \___/ ___/ \__/ \___|   _|   ___| \___/ _|\_|
```

***




## Role-Playing Games


Whether delving into realms of magic or navigating the landscapes of science fiction, RPGs provide a platform for players to immerse themselves into improvised storytelling. With a trusty set of die and character sheets, they assume the roles of characters embarking on adventures, solving intricate mysteries and facing thrilling challenges.

- The **Scenario**, often referred to as "the game" or "the adventure", is the overarching series of events that provide a structure for players to engage in roleplaying. It sets the stage for the characters' journey and presents them with a variety of situations, mysteries and opportunities for exploration.

- The **Setting**, or game world, is the fictional environment where a scenario takes place. It encompasses the landscapes, cities, dungeons and other locations that characters can explore and interact with. Presented with its own lore and conflicts, it serves as a canvas for the GM to run their game.

- The **Game master**, also known as the "GM", is the creative mastermind behind every scenario. It is their responsibility to orchestrate the game session, including its characters, conflicts and overall progression. They act as a narrator, describing environments and situations, guiding the story and playing the role of people that players interact with.

- **Players** are the participants who take on the roles of characters, interacting with the game world and its inhabitants. They assume imaginary identities, making choices and roleplaying deeds to propel the story forward.

- **Characters** are the scenario's protagonists, the fictional personas that players impersonate. They serve as the vessel through which players interact with the setting and story. As the game progresses, they grow and develop with experiences, shaping their backstories and forging connections with the game world and other characters.

- **Dice rolls** are the fundamental mechanic that introduces elements of chance into the game. Actions or choices executed by characters may require players to roll a set of die. The results obtained, combined with the characters' abilities and relevant game rules, determine the outcome of a specific event.

- **Non-playable characters**, or NPCs, are entities within the game world that are controlled by the GM rather than the players. They can serve various roles, such as guides, sources of information, merchants, antagonists or allies.




## Required Tools


- A comprehensive set of **Die**, including d4s, d6s, d8s, d10s and d12s. There should be a minimum of six die for each type to ensure an enjoyable experience. Having more is always better, as it allows to accommodate for larger groups of players.

- One **Character sheet** per player. These will contain all the information needed to roleplay a character, including their abilities, skills and other details. They also keep track of a character's physical and mental states alongside their item inventories.

- **Pencils & erasers** are essential tools for filling out character sheets, taking notes and making adjustments during a game.

***
