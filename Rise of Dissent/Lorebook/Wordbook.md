# GEOSPHERE




## General


- **Atouhm** : the universe where the game takes place.

- **Dranihr** : the specific region where the game takes place.

- **Five Suns** : *Vār*, *Pher*, *Muinh*, *Jovh* and *Fiún* alternate over time in the sky, forming a cyclical pattern that people identify as a year.

***








# BIOSPHERE




## Flora


- **Darātta** : *Dranihr* fruit.

- **Fomberry** : small plant with large stems bearing clustered berries.

- **Lalot** : small plant with slender stalks bearing elongated grains.




## Fauna


- **Freightwhale** : large oceanic creature widely used for commerce and transportation before the advent of airships.

***








# SOCIOSPHERE




- **Hīrī** : main culture/ethnicity living in the *Dranihr* region. *Hīrī* is also the name of their language.

- **Kraäl** : "king" or "leader" in *hīrī* culture.

- **Shaëba** : "queen" in *hīrī* culture.




## Cuisine


- **Fyv** : common substance produced through a fermentation-like process. Microorganisms are introduced into cultivated paddy fields, where, over time (and in reaction with specific plants and minerals), they secrete the ingredient.

- **Prasap** : famous *hīrī* drink made of *fyv*.




## Magic


- **Arcana** : this is how "magic" is referred.

- **Arcanism** : the study of *arcana*.

- **Arcanist** : this is how "magicians" or "scientists" are referred to in the world of *Atouhm*.

- **Arithmyum** : ancient non-spoken esoteric language used for channeling *arcana*.

- **Arithmysticism** : the use of *arithmyum* for the purpose of performing *calciphants* ("spellcasting").

- **Artifism** : modern technology that allows the inscription of runes onto contraptions, empowering them with specific *arcanic* functionalities.

- **Calciphant** : "spell" or "magic formula".

***
