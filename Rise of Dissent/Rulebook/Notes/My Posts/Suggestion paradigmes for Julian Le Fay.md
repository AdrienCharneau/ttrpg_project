SUGGESTION - Cultures/Beliefs and how they shape the psychology and moral compass of a character

So here's one of the things I have always wanted to see implemented in rpgs but never seen actually done. I really don't like the good/bad/loyal/chaotic alignment system that you find in classic DnD and have been looking for more "mature" alternatives or tried to come up with my own ideas. Here's my take on this:


Give the player one or multiple culture(s) and one or multiple language(s):

These would define the environment in which he grew up, what sort of food he is used to, which NPCs he can communicate with and what books he is able to read. These are just examples, there could be much more than just that. A player could get unique dialogue options depending on his culture(s) or language(s), could survive better in specific environments or climates or even get benefits inside factions from NPCs that share the same cultural background as him.


Give the player a belief, a paradigm and an endgame:

A belief acts as a moral compass for the character. It implements a set of directives that help him feel more grounded in the world where he lives. It tells him what he should or should not eat, what he should or should not say or what he should or should not read. A belief could be a religion, a philosophy or any form of ideology or way of life he is curently following or has been educated to follow.

A paradigm is the mental representation of the universe a character has. If he follows a religion, that representation would be the one explained in the scriptures (like ancient Sumerian or Egyptian cosmologies for example). On the contrary, if he is an atheist, he would tend to believe in a more rational and scientific depiction of the world and so on. Believeing that the earth is flat vs believing that the earth is round is a perfect example of two paradigms that oppose each other.

But any belief and/or paradigm also has an endgame associated to it which is fundamental to its existence.

An endgame represents the culmination of a belief, the ideal situation in which everything makes sense if things "happen like predicted". Completely universal, it is a delusion, an unreachable goal that paradoxically stays relevant because of all the promesses it can bring if fulfilled. 
I'm going to use real world examples here just to be more clear. Keep in mind that these are not 100% factually accurate, Im just trying to give you a better idea of what I mean:

-Muslims believe that the final warning has been given to humanity. Muhammad is the last prophet and nobody can pretend to speak on behalf of god until the final trial. In Islam, the "endgame" would be that the world could be united under one religion and finally be at peace if everyone converted and accepted the 5 pillars. There would be no misunderstandings among humans as everyone would speak a single language (arabic), nationalisms would disapear and poverty and hunger would be things of the past. 

-Atheists believe, or at least see a future where religions and obscurantism have totally disappeared from the face of the earth. People would only think rationally and try to find practical solutions to every problem they encounter. Supertition and fear of gods would be considered jokes and wars over abstract ideologies or beliefs would not occur. Scientific experimentations and technological inovations would be the key focuses of such world and education would ensure everyone has the same understanding of reality.

-Communists see the world divided into two groups of people: one dominant class (the bourgeoisie) and one oppressed class (the proletariat). In their "endgame", marxists believe that the proletariat is meant to overthrow the bourgeoise and take back the means of production for themselves. A communist world would be an egalitarian utopia where nobody is above nobody, where resources are redistributed equally among humans and where the economy is planned to fulfill at best the needs of every individual.

This can be applied to so many more religions or philosophies like christianism, buddhism, taoism, fascism... The divergences in beliefs, paradigms and endgames would keep a continuous strife in which every character would be in constant search of a "final truth" or trying to impose its own doctrine on others.

Some experiences, dialogues or books could change a character's belief, paradigm or endgame. But it would be fun if the player could also change the beliefs, paradigms and endgames of other NPCs or even come up with its own personnal ideology. In which case he could become some kind of prophet/philosopher (or charlatan/fraud) that could rally followers to do its biding or even create his own unique faction. 

In this context books could be way more than just tools that simply expose lore to the player. They could help the character gain intelligence points and learn about the world, introduce him to new ideas and teach him about things he did not know before. Once read, they could be used to unlock dialogue options, convince NPCs, formulate/solidify arguments towards a specific ideology or even break/question an already cemented ideology. On a more practical level, they could also teach a character skills or spells. Of course, books could only be used if a character knows how to read and understand what is written in them. They would also consume time and energy.






