#!/usr/bin/python3.8
# -*-coding:Utf-8 -*




import random

nums = []
total = 0

min_stat_value = 1
max_stat_value = 7

expected_total = 42

while len(nums) < 12:
    num = random.randint(min_stat_value, max_stat_value)
    if total + num <= expected_total:
        nums.append(num)
        total += num

difference = 42 - total

print("# [PHYSICAL STATS:", nums[0] + nums[1] + nums[2], "]")
print("# Endurance : ", nums[0])
print("# Strength : ", nums[1])
print("# Agility : ", nums[2])
print("#-------------------------------------")
print("# [SOCIAL STATS:", nums[3] + nums[4] + nums[5], "]")
print("# Dexterity : ", nums[3])
print("# Charm : ", nums[4])
print("# Personality : ", nums[5])
print("#-------------------------------------")
print("# [INTELLIGENCE STATS:", nums[6] + nums[7] + nums[8], "]")
print("# Creativity : ", nums[6])
print("# Reasoning : ", nums[7])
print("# Knowledge : ", nums[8])
print("#-------------------------------------")
print("# [WISDOM STATS:", nums[9] + nums[10] + nums[11], "]")
print("# Experience : ", nums[9])
print("# Empathy : ", nums[10])
print("# Humility : ", nums[11])
print("#-------------------------------------")
print("# Social Status : ", difference)