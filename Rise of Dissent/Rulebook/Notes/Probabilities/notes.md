## Dice thrown to achieve projected probability


### Constants

- **C1** = dice type (d6, d8...)
- **C2** = projected probability

If I'm using d8s:

> C1 = 8

For the projected probability, I'm aiming for a 66% ~ 75% rate of succes for average tasks with average skill:

> C2 = (0.66 + 0.75)/2 so roughly 0.7


### Variables

- **V1** = aptitude value
- **V2** = difficulty threshold
- **V3** = number of dice thrown to achieve the projected probability

***








## Finding the right dice


### Constants

- **C1** = average number of dice thrown to achieve the projected probability
- **C2** = projected probability

If on average players consume 3 fatigue to perform an action:

> C1 = 3

For the projected probability, I'm aiming for a 66% ~ 75% rate of succes for average tasks with average skill:

> C2 = (0.66 + 0.75)/2 so roughly 0.7


### Variables

- **V1** = aptitude value
- **V2** = difficulty threshold
- **V3** = dice type (d6, d8...)
