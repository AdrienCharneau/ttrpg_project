# UNPROCESSED




## Xenophon & The Ruins of Assyria / Ancient Greek Primary Source

>From this place they marched one stage, six parasangs, to a great stronghold, deserted and lying in ruins. The name of this city was Mespila, and it was once inhabited by the Medes. The foundation of its wall was made of polished stone full of shells, and was fifty feet in breadth and fifty in height. Upon this foundation was built a wall of brick, fifty feet in breadth and a hundred in height; and the circuit of the wall was six parasangs. Here, as the story goes, Medea, the king's wife, took refuge at the time when the Medes were deprived of their empire by the Persians. To this city also the king of the Persians laid siege, but he was unable to capture it either by length of siege or by storm; Zeus, however, terrified the inhabitants with thunder, and thus the city was taken.

> - https://www.youtube.com/watch?v=iUOKgjMY8so
> - https://en.wikipedia.org/wiki/Anabasis_(Xenophon)
> - http://www.perseus.tufts.edu/hopper/text?doc=Perseus:abo:tlg,0032,006:3:4
> - https://www.reddit.com/r/AskHistorians/comments/5g326k/xenephon_writes_about_huge_abandoned_cities_in/




## Random

>The logic that if you remove everything possible, only the impossible remains. So in order to achieve "an intangible sea of possibility," one must embrace the unknown, challenge conventional boundaries, and explore uncharted territories.

***








# PROCESSED




## Characters: Paradigms?


>*An answer is never truly an answer; it is merely the doorway to the next question. The path of doubt is treacherous. Few are those who dare to wander their thoughts into the darkest and most convoluted corners of their minds, for the return is never guaranteed. Venturing through it is to remain a child for the rest of one's life, entangled in the endless game of questions that only leads to isolation and incomprehension.*

>*In the realm of scepticism and perpetual reshaping, we may find ourselves untethered from the certainties that once provided us with comfort and relief. Believing that we are following the right path is nothing but a self-deceptive lie to seek solace in our everyday reality. The pursuit of truth may look like a worthless task initially, yet, it is within this uncertainty that lies the essence of realization. It is through questioning and challenging one's presumptions that new frontiers of knowledge are discovered and paradigms are shattered.*

>*It is a path for drifters, dreamers, the curious and the relentless seekers of truth. To embrace doubt is to embark on a perpetual journey of self-discovery, unafraid to confront the shadows lurking within and unyielding in the pursuit of a deeper understanding. It is in the dance between questions and answers, doubt and certainty, that we get to behold and understand the world as it is, not as we wished it to be.*

> ~ Shaēb Öhras ~ 3789 AE, *Whispers of the Nebulous Veil*




## Mechanics: Health & Stress?


>*In the realm of life's trials, a delicate balance must be struck lest we succumb to stagnation or surrender. When faced with challenges that are too easy, languor permeates our being, causing our abilities to wither. Conversely, when the weight of overwhelming burdens bears down upon us, our spirits falter under their oppressive might. Both scenarios erode our performance and impede our progress.*

>*Curiously, it is often the challenges imposed upon us that breed this sense of crushing turmoil, contrasting sharply with those that we willingly embrace. Voluntary trials, consciously undertaken with arduous intent, shape the crucible of growth, while coerced challenges, thrust upon us against our will, become burdensome weights. Should the scales tip in favor of an existence rife with coerced challenges, the overwhelming tide sweeps us away, stifling our endeavors and suffocating our pursuit of purpose.*

>*The struggle to ignite inner motivation in the face of life's relentless onslaught can be a formidable feat. It is as if we find ourselves pitted against the ceaseless adversities of existence. However, it is when the scales tip, and willful challenges outweigh the coerced, that the flame of passion flickers to life. When the arduous undertakings we engage in are born out of choice rather than pressure, our spirits are rekindled, and the seeds of success find fertile ground upon which to flourish.*

> ~ Shaēb Öhras ~ 3789 AE, *Whispers of the Nebulous Veil*

***