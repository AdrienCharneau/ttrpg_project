# BLADES IN THE DARK - PLAYBOOKS




>p.61




## Playbook


A playbook is what we call the sheet with all the specific rules to play a certain character type in Blades in the Dark. By choosing a playbook, you're choosing which type of scoundrel your character is (*Cutter*, *Hound*, *Leech*, *Lurk*, *Slide*, *Spider* & *Whisper*).

When you choose a playbook, you’re choosing a set of special abilities (which give your character ways to break the rules in various ways) and a set of xp triggers (which determine how you earn experience for character advancement).




## PC Advancement


>p. 57


When you fill an xp track, clear all the marks and take an advance. During the game session, mark xp:

- When you make a desperate action roll. Mark 1 xp in the attribute for the action you rolled.

At the end of the session, review the xp triggers on your character sheet. For each one, mark 1 xp if it happened at all, or mark 2 xp if it happened a lot during the session. The xp triggers are:

- Your playbook-specific xp trigger. For example, the Cutter’s is *“Address a challenge with violence or coercion"*. To *“address a challenge"*, your character should attempt to overcome a tough obstacle or threat. It doesn’t matter if the action is successful or not.

- You expressed your beliefs, drives, heritage, or background.

- You struggled with issues from your vice or traumas. Mark xp for this if your vice tempted you to some bad action or if a trauma condition caused you trouble. Simply indulging your vice doesn't count as struggling with it (unless you overindulge).

You may mark end-of-session xp on any xp tracks you want (any attribute or your playbook xp track).  When you take an advance from your playbook track, you may choose an additional special ability.

>*Nadja is playing a Hound. At the end of the session, she reviews her xp triggers and tells the group how much xp she's getting. She rolled two desperate Hunt actions during the session, so she marked 2 xp on her Insight xp track.*

>*At the end of the session, she reviews her xp triggers. She addressed several challenges with tracking or violence, so she marks 2 xp for that. She expressed her Iruvian heritage many times when dealing with the Red Sashes, so she takes 2 xp for that. She also showcased her character's beliefs, but 2 xp is the maximum for that category, so she doesn't get any more.*

>*She didn't struggle with her vice or traumas, so no xp there. That's 4 xp at the end of the session. She decides to put it all in her Insight xp track. This fills the track, so she adds a new action dot in Hunt.*

You can also earn xp by training during downtime. When you train, mark xp in one of your attributes or in your playbook. A given xp track can be trained only once per downtime phase.


### - Reddit Summary

>https://www.reddit.com/r/bladesinthedark/comments/yo4rqh/character_advancement/

Characters have 4 XP Tracks

- 3 Attribute Tracks (Insight, Prowess, Resolve)

- 1 Playbook Track

The Attribute Tracks tick up via

- Desperate Actions

- Training Downtime Actions

- End of Session Questions

The Playbook Track ticks up via

- Training Downtime Actions

- End of Session Questions

**When an Attribute Track Fills**, clear it and take a new Action Dot in any of the 4 Actions for that Attribute. The max is 3 unless the Crew has the Mastery Crew Upgrade.

**When the Playbook Track Fills**, clear it and take a new Special Ability from your Playbook or another Playbook. There are no limits to the amount of “Veteran” Advances you can take.

The Training Crew Upgrades allow you to get 2 XP for training a Track during Downtime instead of 1. You can only Train a given Track once per Downtime, though you can do any number of other Downtime Actions as often as you want as long as you have the Coin and/ or Rep to spare to fund extra Downtime Actions.

The XP from End of Session Questions can be funneled and distributed to the 4 different Tracks in whatever way the player would like.

***








# CHRONICLES OF DARKNESS - ASPIRATIONS




>p. 29, p.77




## Aspirations


Aspirations are goals that your character wishes to accomplish. They’re also statements about the sort of stories you want to tell about your character. Accomplishing an Aspiration is one of the main ways you can earn Beats to improve your character. You should aim for accomplishing about one Aspiration per session. If you are playing a one-shot, all of your Aspirations should be focused, short-term goals that you could potentially complete within that session. If you are playing in a long-running chronicle, then choosing long-term goals is okay.

An important thing to consider when deciding on your Aspirations is that you want them to be active goals. They have to be something that you need to do, rather than something that you need to avoid. For example, “Don’t get drunk” wouldn’t be a very good Aspiration for a character, but “Go a day without taking a drink” could be, if your character would find this to be a struggle.

Aspirations give your character life and direction beyond the scenario dreamed up by the Storyteller. They make your character feel more real, give the Storyteller hooks to get him involved in the story, and establish what your character does if there isn’t an immediate crisis in front of him.




## Experience


A player earns Beats for his character in many ways (listed below). When your character has gained five Beats, they convert to one Experience, which can be used to develop your character’s abilities.


### - Beats

You gain Beats when your character fulfills one of the criteria below.

- If your character fulfills an Aspiration, take a Beat. At the end of the game session, replace the Aspiration.

- Each Condition has criteria for resolution. When resolving a Condition, take a Beat.

- Some Conditions provide Beats for actions other than resolution.

- Any time you fail a roll, you may opt to make it a dramatic failure and take a Beat.

- If your character takes lethal damage in one of her rightmost Health boxes, take a Beat. Vampires do not gain Beats from bashing damage.

- At the end of any game session, take a Beat.

- Any time your character risks a breaking point (p. 73), take a beat.


### - Experience Costs

Spend the following amounts of Experiences to improve your character.

- Attribute: 4 Experiences per dot

- Merit: 1 Experience per dot

- Skill Specialty: 1 Experience

- Skill: 2 Experiences per dot

- Integrity: 2 Experiences per dot

***








# THE CHUUBO'S MARVELOUS WISH-GRANTING ENGINE RPG




>POINTER : p.64

>TO READ : p.74, p.92, p.328, p.356, p.400, p.498




## Quests and Experience (p.22)


- **Action XP** (aka **Group XP**) comes from taking various actions that advance the story. It goes into a group pot that will be shared among the players and you can spend your share on any quest.

- **Emotion XP** comes from evoking certain emotions and semi-autonomic responses from the other players.

- **Bonus XP** (aka **Quest XP**) comes from the quests themselves. If you want to learn the flute, you need to practice; practicing the flute, ergo, is the kind of thing that earns bonus XP specific
to that quest.




## Arcs (p.25)


The quests that you’re on are organized into larger-scale “Arcs”— epic stories of personal advancement. Completing one quest will usually earn you a “Perk” like a cute new power, power-up, or bicycle; completing a full Arc will provide a substantive holistic improvement to your entire power set.

- **Blue** or **“Bindings” Arcs**, focusing on sealed, bound powers: containment and targeted use of wicked,
forbidden, or dangerous things.

- **Orange** or **“Knight” Arcs**, wherein you take on a formal role and wield its perquisites and powers.

- **Green** or **“Otherworldly”** Arcs, connecting you to some great force of nature as another part of yourself, and vice versa, often by way of dreams, nature walks, and communication with ally spirits.

- **Red** or **“Storyteller” Arcs**, focusing on telling stories that come true, or, more generally, on changing the boundary between ideas and life.

- **Golden** or **“Aspect” Arcs**, wherein you develop and unleash new powers from your body, mind, and training.

- **Purple** or **“Shepherd” Arcs**, whereupon you learn to guard things, guide things, and wake powers in things.

- **Silver** or **“Emptiness” Arcs**, drawing on the bleak, lifeless power of the far Outside and sometimes the more vivid chaos of the near.

- **Black** or **“Mystic” Arcs**, taking you outside of yourself; on these Arcs, you give yourself into the keeping of, and walk in the footsteps of, higher powers.




## Character Creation Process (p.61)


Campaigns and setting books for the Chuubo’s Marvelous Wish-Granting Engine RPG will often come with campaign- or Region-specific character creation processes. If you don’t have a relevant setting or campaign book, though, here’s what I’d recommend as the process for creating a new PC for this game:

- make sure that there’s a campaign concept already on the table
- choose your starting Arc and first quest
- choose your Skills
- choose your special abilities
- choose your bonus XP emotion
- fill in miscellaneous little details for your character
- choose a “basic quest” for your PC’s daily life




## Choose your Arc (p.61)


>What will your character’s life focus on in the early stages of the game?

>What’s your goal or your mission statement right now?


### - Mortals

- **Bindings** if you want to work with weird and forbidden stuff

- **Knight** if you want to find or live up to your place in society

- **Otherworldly** if you’re torn between two worlds

- **Storyteller** if you’re a larger-than-life, cinematic figure

- **Aspect** if you’re striving to become better, stronger, more

- **Shepherd** if you want to guard, guide, or strengthen others

This turns into your starting level 0 Arc. If you’re in the process of learning Shaolin Kung Fu, for instance, your character might start out with the Arc *Aspect (Shaolin Kung Fu) 0*

Or, if you’re a necromancer from beyond the world, you might start off with *Emptiness (Necromancer) 0*, instead.


### - Miraculous Characters

- **Knight (Become Somebody)** to find or live up to your place in society;

- **Otherworldly (Spiritual)** to develop elemental powers

- **Storyteller (Creature of Fable)** if you’re becoming a story or myth

- **Aspect (the Ace)** if you’re striving to become superhumanly awesome

- **Shepherd (Sentimental)** to create magic stuff or have magical pets

- **Bindings (Wounded Angel)** if you practice deviant science or forbidden magic

- **Knight (Reality Syndrome)** if you’re developing powers that you don’t understand

- **Emptiness (Accursed)** to take the first steps towards a tragedy—towards becoming a living weapon with a terrible life

- **Otherworldly (Child of the Ash)** and maybe possibly turn into a giant monster

- **Storyteller (Creature of the Light)** and become a sacred existence

- **Shepherd (A Keeper of Gardens)** and rule one or more magical places

- **Mystic (Primordial)** and become the incarnate and generally inhuman form of some natural force, e.g., evolving into the dragon of the wind or the many-armed goddess of Hope and the Sun




## Starting Quest (p.64)


>UNFINISHED SECTION

>EXAMPLE QUEST SETS P.356


### - Binding Arcs

You’ve just been thrown into a new relationship with some troubling power—e.g:

- you’ve been assigned to watch a criminal
- you’ve been partnered with something dangerous
- you’ve been given responsibility for something dangerous
- you’ve captured something
- you’ve wound up spiritually entangled with something
- your parents are pressuring you into dating something that’s conventionally “wicked” or “disfavored.”

>*OR* You’re having a crisis of faith. You work with weird and forbidden stuff. You find yourself worrying about whether you’re an OK person anyway. Why are you worrying? Just how bad is it? Are you facing the truth that you’re probably not OK, are you worrying over nothing, or is your moral standing here a legitimately troubling question?

You can either create a custom quest for this or pick one of the following example quests: *Science!* (you’re investigating something. What is it?), *Bind* (you’ve just bound some horror and now you want to redeem it), *Mental Training* (you’re studying... maybe some sort of wicked magic?)


### - Knight Arcs

Text


### - Otherworldly Arcs

Text


### - Storyteller Arcs

Text


### - Aspect Arcs

Text


### - Shepherd Arcs

Text


### - Emptiness Arcs

Text


### - Mystic Arcs

Text

***








# INVISIBLE SUN




>The Key p.23, p.24

Character arcs offer the main way for characters to earn the points that they spend to advance (called Acumen and Crux). They are a way for a character to have a goal and take steps to advance that goal. You might start with a desire to find a mentor, learn a secret, or avenge the death of a friend, for example. The character arc tracks your progress as you attempt to accomplish one of those things.

You start with a single arc. You can undertake as many arcs as you wish as the game progresses many being suggested by the events of the narrative —but some character arcs have a cost to begin. However, the arc you choose at the beginning is always free.




## Acumen and Crux


>The Key p.205

The way you measure character advancement is through Acumen and Crux. These are points you earn for performing tasks or having significant experiences. The things that earn you Acumen or Crux might not be the same things that earn them for other characters, as each character has a different path, different goals, and different arcs.

Crux are divided into Joy and Despair. Very generally speaking, you gain Joy when good things happen, and Despair when bad things happen. 1 Joy and 1 Despair, together, form 1 Crux, so you need some of both in your life.




## Orders


There are four orders of magic, called the Order of the Vance, the Order of Makers, the Order of Weavers, and the Order of Goetica. The orders share one fundamental commonality. They all measure advancement in their ranks in degrees. Degrees are a hierarchy. While they show a progression in the understanding of an order’s concepts and focus, they also show advancement in the social organization. A 1st-degree Goetic is of a lesser rank, station, and position of authority than a 2nd-degree Goetic.

>Degree is a measurement of how well one is accepted in the hierarchy of the order, but it’s also a measurement of one’s skill and knowledge in the focus of that order. A higher-degree Vancian mage is better at preparing and casting specifically Vancian spells than a lower-degree individual.

Apostates don't belong to an order therefore they have no ranks.


### - Advancing

Advancing to a new degree in your order involves achieving various goals. Some of these are related to straightforward game mechanics—typically in earning and spending Crux. Others involve storybased accomplishments as well as dealing with the politics of the order.

Advancing to a new degree requires Crux equal to the degree. Thus, advancing from 1st to 2nd degree requires 2 Crux. There are also storybased requirements specific to each order, such as gaining sponsorship of a higher-degree member, performing certain rites, training lower-ranking members, and so on.


### - Path to Joy

A few suggestions for things that might earn the character Joy. This is not meant to be a limiting list, but rather thought-starters to give players and GMs ideas. Most of these suggestions can be used just one time each.

- **Vance** : Learn a spell of a level higher than any we’ve yet learned, Attend the Conclave of Iov for the first time, Create a new Vancian spell (and name it after yourself ), Learn a spell from the Vancian who created it (assuming they are higher ranking than you), Spend one month instructing lower-ranking Vancians.

- **Maker** : Create an original magically imbued object of power for the first time, Create an object of power more potent than any we have ever crafted before, Instruct another Maker in a crafting technique for a month, Study under another Maker in a crafting technique for a month.

- **Weaver** : Spending one month instructing a Weaver of a lower rank, Winning a match of the Spider’s Game against a Weaver of a higher degree, Finding and recruiting a new member to our cell.

- **Goetica** : Complete a successful colloquy with a being of a level higher than any we have dealt with before, Complete a successful colloquy with a being of a type that we have never dealt with before, Complete a successful colloquy with a specific being that we have dealt with a dozen times or more, Instruct another Goetic in various interaction skills for a month, Come upon a summonable entity’s name or information about a pact through atypical means (not by purchasing it with Acumen).

- **Apostate** : Developing a new spell or other magical practice, Finding a specific success as a direct result of not being part of an order, Discovering an unexpected effect in what had been a tried and true magical practice.


### - Path to Despair

A few things that might happen that would give the character Despair. This is not meant to be a limiting list, but rather suggestions to give players and GMs ideas. Most of these suggestions can be used just one time each.

- **Vance** : Chastisement from a higher-ranking Vancian, A spell producing a different effect than expected (this does not mean failure, just a completely different effect than described), Complete separation from the order and any of its members for more than three months.

- **Maker** : Mishap in the creation of an item, Rejection by a Maker of a higher rank, The destruction or theft (not depletion) of an item we have crafted.

- **Weaver** : A woven effect taking on an entirely unexpected nature, Using a woven effect to accidentally destroy or harm something we love.

- **Goetica** : A summoned being gets out of our control and harms someone or something we hold dear, A summoned being, in addition to performing whatever action we’ve given it, tells us something horrifically disturbing about ourselves, our future, or someone we love.

- **Apostate** : Being presented with a clear example of the benefits of being part of an order, Finding that a new spell, magical practice, or other development that we created is actually not new at all.




## Forte


Forte reflects a talent or focus that is essentially unique to the character. It’s both what you do best and what you can do that most people can’t (or at least, not the way you do it).

*There is a miriad of Fortes being described in "The Key p.77", each with their own "Character Arcs", "Path to Despair" and "Path to Joy".*


### - Example : Bears an Orb

The Fanadon Orbs are well known throughout the Actuality. Those who understand them and have obtained one can tap into this power and use it for their own ends. We call ourselves orb-bearers, and usually end up embedding our orbs into our bodies to more completely attune to their energies

- **Character Arcs** : Current orb-bearers seek knowledge about the Fanadon Orbs, always striving to learn more about their power and their genesis [Uncover a Secret]. - If the bearer believes the orb to be a part of the Legacy, learning more about the various aspects of the Legacy might also play a role in the bearer’s tale [Learn].

- **Path to Joy** : Embedding the orb into flesh at a changery. - Using the power of the orb to help complete a character arc’s climax. - Discovering concrete information about the nature or origin of the Fanadon Orbs.

- **Path to Despair** : The orb is damaged and needs magical repair. - Alcohol consumption interferes with the connection to the orb. - Due to magical fluctuations, the charge drawn from the orb inflicts 1 Wound upon me instead of its proper use. - Another being touches the orb and somehow draws a charge from it, using it for themselves.


### - Advancing

A character’s forte is a suite of additional abilities they can perhaps learn to harness. Each forte has an “ability tree” showing the path of advancement.




## Arcs


Character arcs and story arcs are central to Invisible Sun because they are how the game invests itself in stories and character depth and development.

You choose an arc for your character, and as the character progresses through the story points of the arc, they earn Acumen (and possibly Joy or Despair) to reflect that progress. Because character arcs are frameworks in which to build an individual character’s stories, they are very general. It’s up to the players and the GM to make the details fit.

As you move through the various steps in the arc, from the opening to the climax and the resolution, you earn Acumen rewards. At the end of a session, as each player goes through their character summary, they describe how actions that happened in the session equate to the completion of a step (or possibly more than one step) in their character arc. If everyone agrees, the character gets their Acumen reward, as detailed in that arc.

Within the arc, most of the time a step is probably optional, depending on the situation —although it’s hard to envision most arcs without some kind of opening, climax, or resolution. Steps other than the opening, the climax, and the resolution can be done in any order.

Character arcs should always take at least weeks in game time, and no more than two steps in an arc should be accomplished in a game session or side scene (and most of the time, it should be one step, if any). If neither of these two things is true, then it’s not really a character arc. You can’t, for example, use the Creation arc to guide you through something you can make in an hour or two.

Bonded characters can share character arcs. In other words, two close friends can set out to undo the same wrong, train the same creature, or solve the same mystery.


### - Story Arcs

Story arcs are like character arcs, but they almost always involve multiple characters. These are the stories that drive the central narrative that the group will play. A character arc becomes a story arc when multiple PCs join in on things. If they do, they can all reap the rewards of progressing the arc.

Sometimes, story arcs aren’t just character arcs with multiple characters. They are larger than character arcs. It wouldn’t be wrong to think of them as “major arcs” while character arcs are “minor.” A story arc might encompass multiple character arcs within it. If, for example, a story arc involves defeating a cabal of vislae kidnapping and sacrificing people to sell their souls to demons, this story arc might involve a Solve a Mystery arc (why did our friend disappear?), a Rescue arc (to get them back from the cabal’s clutches) and then a Revenge arc (after the friend is killed by the cabal).

Either kind of story arc can be initiated by the GM as part of the larger story going on in the narrative. This can be a combination of individual character arcs woven into a larger story, or a number of arcs undertaken by some or all of the characters.


### - Beginning a New Arc

At character creation, you can choose one character arc for your character. Once play begins, players can still take on a new arc whenever they wish. However, most arcs have a beginning investment in Acumen that must be paid, reflecting the character’s devotion to the goal. The character will earn this investment back (probably many times over) if the arc is completed.

Character arcs are always player-driven. A GM cannot force one on a character. That said, the events in the narrative often present story arc opportunities and inspire character arcs for characters. It’s certainly in the GM’s purview to suggest possible arcs related to the events going on. For example, if the GM presents an encounter in which an NPC wishes to learn from the PC, it might make sense to suggest taking the Instruction arc. Whether or not the PC takes on the student, the player doesn’t have to adopt the Instruction arc unless they want to.

All arcs have the following parts, called story points:

- **Cost** : This is a cost, usually of 2 Acumen, to start the arc if it is not the character’s first arc, which is chosen at character creation.

- **Opening** : This sets the stage for the rest of the arc. It involves some action, although that might just be the PC agreeing to do the task or undertake the mission. It usually has a reward of 1 Acumen.

- **Step(s)** : This is the action required to move toward the climax. In story terms, this is the movement through the bulk of the arc. It’s the journey. The rising tension. Although there might be just one step, there might also be many, depending on the story told. Each usually has a reward of 1-2 Acumen.

- **Climax** : This is the finale—the point at which the PC likely succeeds or fails at what they’ve set out to do. Reaching this point earns a reward of 3 Acumen, but there’s more, depending on whether the PC is successful. Not every arc ends with victory. If the character is successful, they earn 1 Joy. If they fail, they earn 1 Despair. If a character fails the climax, they very likely ignore the resolution.

- **Resolution** : This is the wrap-up or denouement. It’s a time for the character to reflect on what happened, tie up any loose ends, and figure out what happens next. When things are more or less resolved, the character earns 1 Acumen.


### - Arc models

*Comprehensive list of character arcs p.167, each with their own "Cost", "Opening", "Steps", "Climax" and "Resolution".*

>If you and the GM want to make a new one, it should be fairly easy after looking through these models.


### - Example : Build

You are going to build a physical structure —a house, a fortress, a workshop, a defensive wall, and so on. This arc would also cover renovating an existing structure or substantially adding to one. Of course, this doesn’t have to be physical construction. You might build it with spells or other magic, or this might be an arc to create a magical structure in a distant, mystical half-world.

- **Cost** : The Idea. You pay a cost of 1 Acumen.

- **Opening** : Make a Plan. 1 Acumen reward. This almost certainly involves literally drawing up blueprints or plans.

- **Step(s)** : Find a Site. 1 Acumen reward. This might be extremely straightforward—a simple examination of the site—or it might be an entire exploratory adventure. (If the latter, it might involve multiple such steps.)

- **Step(s)** : Gather Materials. 1 Acumen reward. Depending on what you are building and what it is made out of, this could involve multiple steps. There probably are substantial costs involved as well.

- **Step(s)** : Construction. 1 Acumen reward. Depending on what you are building, this could involve multiple steps. It might also take a considerable amount of time and work.

- **Climax** : Completion. 3 Acumen reward. The structure is finished. A successful construction results in 1 Joy. Failure results in 1 Despair.

- **Resolution** : 1 Acumen reward. You put the structure to its desired use and see if it holds up.




## The Desideratum


Once all the bonds are set, the group works together to decide what they need to start the game. This can be many things, of course, but to start off a narrative, the characters —as a group— need a single desideratum that provides the impetus for the first stories told. Essentially, the desideratum is the players’ way of deciding how they want the narrative to start, at least very broadly.

This decision is probably based on the individual character arcs the players have chosen for their vislae. They can simply choose what’s needed for one character or —even better— find a way to merge more than one arc’s initial step into one need. For example, if one character is looking to avenge their family and another to uncover a secret, they both very likely need a good source of information to start their stories.

Obviously, if two bonded characters share a starting arc, this is a very good starting point for a group desideratum. The desideratum can come in six forms:

- **Money** : The group needs money and is interested in work or a scheme that will earn them a substantial amount.

- **Power** : The group recognizes that to achieve its goals, everyone must become more magically adept or get their hands on some kind of magical aid.

- **Information** : The group needs information. They seek answers, ideas, or just knowledge to start off their story.

- **Allies** : The group needs the assistance of others more powerful than themselves.

- **Travel** : The group needs to go somewhere to get what they want.

- **Altruism** : The group needs to help someone. This could be general altruism (we’re looking to help those in need) or, more likely, they have a specific person or group in mind to help.

After the players select one of these six types, the GM can begin to craft the beginning of the narrative, knowing exactly what motivation will pull the characters into the tale. Essentially, the desideratum is the players’ way of saying, “GM, if you assume this as our motivation, you can start the narrative with us already involved.” In other words, if the group desideratum is information, the GM can start the second session—where the story really begins—by saying, “You all have agreed to travel together to the Library of Rhol, the Left-Handed God. On your way there...”

***