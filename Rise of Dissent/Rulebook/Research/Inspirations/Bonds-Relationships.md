# APOCALYPSE WORLD - HX




Every player’s character has Hx, history, with every other player’s character. Your Hx with someone says how well you know them. It’s based on three things, in some combination: how closely you’ve been observing them, how open they’ve been to you, and specific moments or episodes in your shared past. Your Hx with them, written on your character sheet, says how well you know them; theirs with you, on their sheet, says how well they know you.

It doesn’t say how long you’ve known them, how much you like them, how positive your history together has been, or anything else necessarily, just how well you get them. If your Hx with somebody is negative, that means that you really don’t know them and can’t predict what they’ll do. Thus, you can’t effectively help them OR screw them over. Hx is asymmetrical — My character might know yours Hx+2, but yours might know mine Hx-1, or whatever.

>Okay! Go around to set Hx. MC, your job is just to oversee and give everybody their turn. All of the characters have their own rules for setting Hx with each other. Many of them call for the players to decide things about their characters’ relationships with one another; the players can and ought to bring each other into it when they’re making their choices.




## Characters


*Comprehensive list of characters p.19, each with their own way of setting up "Hx", with unique "Special Moves" that might also use or affect "Hx" during a game.*


### - Example : Angel

Everyone introduces their characters by name, look and outlook. Take your turn. List the other characters’ names. Go around again for Hx. On your turn, choose 1, 2, or all 3:

- One of them put a hand in when it mattered, and helped you save a life. Tell that player Hx+2.

- One of them has been beside you and has seen everything you’ve seen. Tell that player Hx+2.

- One of them, you figure doomed to self-destruction. Tell that player Hx-1.

Tell everyone else Hx+1. You’re an open book. On the others’ turns:

- You try not to get too attached. Whatever number they tell you, give it -1 and write it next to their character’s name.

At the end, find the character with the highest Hx on your sheet. Ask that player which of your stats is most interesting, and highlight it. The MC will have you highlight a second stat too.

- **Professional compassion** (*Angel Move*) : you can choose to roll+sharp instead of roll+Hx when you help someone who’s rolling.




## Character Improvement


A player marks experience — fills in one of the little experience bubbles on her character sheet — under like three circumstances in play. First is when she rolls one of her two highlighted stats. Second is when her Hx with someone goes to +4 or -4. Third is when a move tells her to. When a player marks her fifth experience bubble, she improves her character. When she improves, she erases all her little experience bubbles and starts over at 0.




## Moves


### - Help or Interfere

When you help or interfere with someone who’s making a roll, roll+Hx. On a hit, they take +1 (help) or -2 (interfere) now. On a 7–9, you also expose yourself to fire, danger, retribution or cost.


### - Harm

When you inflict harm on another player’s character, the other character gets +1Hx with you (on their sheet) for every segment of harm you inflict. If this brings them to Hx+4, they reset to Hx+1 as usual, and therefore mark experience.


### - Heal

When you heal another player’s character’s harm, you get +1Hx with them (on your sheet) for every segment of harm you heal. If this brings you to Hx+4, you reset to Hx+1 as usual, and therefore mark experience.


### - Changing Highlights

At the beginning of any session, or at the end if you forgot, anyone can say, “hey, let’s change highlighted stats.” Any player, and you can feel free to say it too as MC. When someone says it, do it. Go around the circle again, following the same procedure you used to highlight them in the first place: the high-Hx player highlights one stat, and you as MC highlight another.


### - Session End

At the end of every session, choose a character who knows you better than they used to. If there’s more than one, choose one at your whim. Tell that player to add +1 to their Hx with you on their sheet. If this brings them to Hx+4, they reset to Hx+1 (and therefore mark experience).




## New Character's HX


The new character will need Hx with everybody else and everybody else will need Hx with her. Here’s how:

- The player introduces her new character, by name, look and outlook.

- She goes through her Hx rules and makes all the requisite decisions — it’s her “your turn.”

- Everybody else takes a turn, but just tells her Hx-1 if their characters aren’t acquainted, Hx+1 if they are.

- Sum as normal, and ta da.

Use this procedure whenever someone creates a new character. When a new player joins a game already underway, everybody else will have to introduce their characters too, naturally.

***








# DUNGEON WORLD - BONDS




Bonds are what make you a party of adventurers, not just a random assortment of people. They’re the feelings, thoughts, and shared history that tie you together. You will always have at least one bond, and you’ll often have more.

Each bond is a simple statement that relates your character to another player character. Your class gives you a few to start with, you’ll replace your starting bonds and gain new ones through play.




## Resolving Bonds


At the end of each session you may resolve one bond. Resolution of a bond depends on both you and the player of the character you share the bond with: you suggest that the bond has been resolved and, if they agree, it is. When you resolve a bond, you get to mark XP.

A bond is resolved when it no longer describes how you relate to that person. That may be because circumstances have changed —Thelian used to have your back but after he abandoned you to the goblins, you’re not so sure. Or it could be because that’s no longer a question —you guided Wesley before and he owed you, but he paid that debt when he saved your life with a well-timed spell. Any time you look at a bond and think “that’s not a big factor in how we relate anymore” the bond is at a good place to be resolved.

If you have a blank bond left over from character creation you can assign a name to it or write a new bond in its place whenever you like. You don’t get an XP for doing so, but you do get more defined bonds to resolve in the future.




## Writing New Bonds


You write a new bond whenever you resolve an old one. Your new bond may be with the same character, but it doesn’t have to be.

When you write a new bond choose another character. Pick something relevant to the last session—maybe a place you traveled together or a treasure you discovered. Choose a thought or belief your character holds that ties the two together and an action, something you’re going to do about it. You’ll end up with something like this:

>Mouse’s quick thinking saved me from the white dragon we faced. I owe her a boon.

>Avon proved himself a coward in the dungeons of Xax’takar. He is a dangerous liability to the party and must be watched.

>Valeria’s kindness to the Gnomes of the Vale has swayed my heart, I will prove to her I am not the callous fiend she thinks I am.

>Xotoq won the Bone-and-Whispers Axe through trickery! It will be mine, I swear it.

These new bonds are just like the old ones—use them, resolve them, replace them.

***








# INVISIBLE SUN - BONDS




PC bonds, unlike NPC bonds, should be developed in the first session with the other players involved. Once everything has been determined, including neighborhoods for each character’s foundation, players should agree on their characters’ relationships with one another. Most of the time, in Invisible Sun, characters already know each other at the start of the game, and thus many have formed powerful bonds.

Most bonds are intended for two characters, but most befit a group of more. Three or more characters could be linked with the close friends bond, for example.

Most characters should have only one PC bond to start the game. It’s possible to develop a bond later in the narrative using the Develop a Bond character arc.

Bonded characters can share character arcs. In other words, two close friends can set out to undo the same wrong, train the same creature, or solve the same mystery.




## Types


### - Close Friends

The two characters are close friends and have been for some time. They are familiar with each other’s homes, general abilities, personalities, and backgrounds.

- **Benefit** : If one friend has a connection with a group or a bond with an NPC, the other gains the benefit of that connection or bond as well. This applies to a maximum of one connection or bond per character.

- **Drawback** : If one friend is gravely imperiled, the other loses one action in fear. The GM determines when this happens.


### - Fated Companions

While not friends, two characters always seem to be crossing paths. They frequently turn up at the same places at the same time. Perhaps there is a reason for all this that will become clear later.

- **Benefit** : At some point in the narrative, fate intervenes. Each character has all their pools refreshed and all Injuries, Wounds, and Anguishes healed, and both succeed on their next action, regardless of what it is (assuming it is not utterly impossible). Both characters must agree as to when this happens and have it fit their background story.

- **Drawback** : Once the characters use the benefit, the bond is basically done.


### - Fellow Students

The characters studied together and thus have known each other for a long time.

- **Benefit** : Each character gains a spell of level 3 or less. It must be the same spell for both PCs.

- **Drawback** : The characters can use the chosen spell only if they are within long range of each other.


### - Friends from the Past

The characters have known each other for a long time. They are not only familiar with each other’s homes, they are also very knowledgeable about each other’s general abilities, personalities, families, and backgrounds.

- **Benefit** : Each friend gets a level 1 connection to a group. It must be the same connection for both characters.

- **Drawback** : If one friend is gravely imperiled, the other loses one action in fear. The GM determines when this happens.


### - Housemates

Two characters live in the same home. They share at least some significant space, but each probably has personal space as well. They must work out various issues (who pays for what? who does the dishes?).

This bond directly impacts character foundation. For example, if a Connected character with an average house is the housemate of a Mendicant character, the two share an average house, probably owned entirely by the Connected character. However, if an Established character with a unique house and a Stalwart character with an average house are housemates, they have a single house that is larger than average with some unique aspect. Two housemates that each should have small houses share an average house. Two that should have average houses share a large house. And so on.

- **Benefit** : Shared expenses, as well as combined house “values,” as described above.

- **Drawback** : The lack of an additional house.


### - Lovers

The two characters are in love. They share a romantic interest in each other and feel very deeply. A lover can also be a spouse. Spouses can also be housemates, and thus have two bonds.

- **Benefit** : When close, the lovers gain +1 to any action they attempt.

- **Drawback** : If apart for more than one day, each character suffers 1 scourge in to one stat pool. On the second day, they suffer 1 scourge in two stat pools. And so on until they face a −1 penalty on all actions.


### - Mystic Bond

The two characters are linked by a deep, magical connection. Magic flows from one character to the other. This is one of the rarest but also the most potent of all bonds. Only two characters can be a part of this bond.

- **Benefit** : The two characters can affect each other with spells and abilities that normally affect only themselves. Further, when the two characters are close, their spells and abilities each gain a +1 level bonus.

- **Drawback** : Anytime one character suffers any kind of damage, both characters suffer that damage, no matter how far apart they are.


### - Relatives

The characters are related in some way. They know more about each other than most friends could ever know.

- **Benefit** : The characters each have a new level 1 skill, but it must be the same skill.

- **Drawback** : When one character suffers a Wound, the other suffers 1 point of mental damage.


### - Rivals

Two characters share a friendly rivalry. They don’t want to see the other outright fail, but each would like to succeed more than the other. The rivalry may have an overt, specific story reason as well as a general one, such as being rivals for the same romantic partner, rivals for a position in a shared order, and so on.

- **Benefit** : Each time one character succeeds at an action, the other is incentivized and gains +1 to their next action. This applies only if the characters are within long distance of each other and can see each other.

- **Drawback** : If the characters are apart for more than an hour, it takes until the next sunrise to “recharge” the benefit power.


### - Shadow Friends

Two characters knew each other in Shadow. It’s possible that they escaped Shadow at the same time, or in the same circumstances.

- **Benefit** : If the characters have the same Shadow skill, it is level 3 instead of level 2.

- **Drawback** : If one PC is pulled back into Shadow, the other suffers a −1 on all actions until the friend returns.


### - Soulmates

“Soulmate” means something slightly different in the Actuality than it does in Shadow. These are two characters who have revealed their secret soul to each other and found that they share the same one (a slight adjustment to one of the PCs may be necessary for this to work).

- **Benefit** : The characters can exchange or trade Joy and Despair as they wish.

- **Drawback** : If anyone knows the secret soul name of either character, they can use it against one or both.


### - Stewardship

One character is in a situation where they must watch over and protect another. One might be the parent of the other, a close servant, someone who owes a life debt, or some other guardian.

- **Benefit** : The protected character gains +1 to Dodge actions when the steward character is close.

- **Drawback** : The steward character suffers −1 to Dodge actions when close to the protected character.


### - Unrequited Bond

One character would like to be closely bonded with the other, possibly romantically but perhaps just in friendship, but the other does not share this feeling. This, then, is a bond that affects only one PC.

- **Benefit** : The character who would like to form a bond gains +1 to all actions when the other is close. The character who is uninterested in the bond, when close to the other, can move a short distance away from the other and it does not count as part of their action.

- **Drawback** : The character who is uninterested in the bond is, as a default, never close. So the character who desires the bond must always move closer to get the bonus.




## Arc : Develop a Bond


You want to get closer to another character. This might be to make a friend, find a mentor, or establish a contact in a position of power. It might be to turn a friend into a much closer friend. The character might be an NPC or a PC. In the case of an NPC, the end result might be an NPC bond, and in the case of a PC it’s a PC bond.

- **Cost** :  Putting Yourself Out There. You pay a cost of 2 Acumen.

- **Opening** : Getting to Know You. 1 Acumen reward. You learn what you can about the other character.

- **Step** : Initial Attempt. 1 Acumen reward. You attempt to make contact. This might involve sending messages or gifts through a courier, using an intermediary, or just going up and saying hello, depending on the situation.

- **Step(s)** : Building a Relationship. 2 Acumen reward. There might be many such steps as you develop the relationship.

- **Climax** : Bond. 3 Acumen reward. You succeed or fail at forging the bond. A successful resolution results in 1 Joy. Failure results in 1 Despair.

- **Resolution** :  Acumen reward. You enjoy the fruits of your new relationship.

***








# SMALLVILLE - RELATIONSHIPS




## Traits


The character sheets that describe Leads and Features are mostly lists of Traits. The three big categories of Traits are Drives, Assets, and Resources. Every character also has five Stress Traits that track how he’s been hurt. Lastly, there’s a space to keep your Plot Points, which are a game currency that you can spend to grease the wheels in your favor. We’ll take a closer look at each category in a little bit. This book calls out most specific Traits by using small caps, **like this**.

Each Trait has a die rating. When that Trait comes into play, you’ll roll that die. Bigger dice tend to roll higher, but no die is safe from rolling a 1.


### - Drives

The first half of your Lead sheet lists your character’s Drives: his Relationships with the people important to him and the Values that he holds dear. Each one has a statement (in italics) and a die rating. The statement describes his impression of each person or value. The rating scores how much he cares about that Drive.

You roll in dice for Drives when your character’s actions advance or assume the truth of the statement. So let’s say you have **Tess** *is playing with fire d8*. You can roll that d8 when you’re telling her that she doesn’t appreciate the consequences of her actions. Or when you’re working to bring down her firewalls and shut down that project that’s about to go nuclear. Or when you’re getting Clark to go confront her about the whatever-it-is that she decided to unleash on the planet this week.

Occasionally —and by occasionally I pretty much mean all the time— you’ll find that your character’s understanding of a person or a value is not quite accurate. Maybe Tess is acting rationally and responsibly; when you defend Tess’ sensible action to Oliver, you can challenge your Relationship with her. Alternately, your Value of **Truth** *I decide who knows it d10* might come up dry when you realize that Clark isn’t going to save you from the monster stalking around the Watchtower because you didn’t tell him about it until too late. When you call Clark frantic for help, you can challenge that Value.

>When you challenge a Drive, you get to triple its die —instead of rolling one d8, you throw three. You also note the die size in your Growth pool on your Lead sheet, which is really important. Unfortunately, because you’re questioning your Drive and aren’t so sure about it anymore, you also have to step it down one die size for the rest of the episode. After all, if you stand up for Tess’ plan, you can’t really turn around and say she’s acting reckless in the next scene—or at least, you can’t do that and expect it to work quite as well.




## Relationships


It’s not just a matter of who you know, but how you know them, what you know about them, and, when it comes right down to it, what you think about them. In this game, who you do things for —or against— is just as important as why you do them, and arguably both matter more than what you actually do.

Relationships are Traits associated with other Leads and Features. Anytime you roll dice, you may add a die to your dice pool for an applicable Relationship.

Your Relationship with another character may have as much impact on your success as a Distinction, Power, or Gear. When dodging a speeding car, it may not be your strength or speed that saves you, but the die you rolled that represented the woman you love. She is your reason to live —which helped you dodge that car.

Conversely, if you and your arch-nemesis (another player’s Lead or a Feature) are both chasing down the same bad guy, you might add your Relationship with your nemesis to your pool of dice for catching the bad guy because you just can’t let that person be the one to catch the crook. You give that extra oomph to your Action for the nemesis you can’t let win.

Remember, your die rating with another character doesn’t reflect how much you like him, but how strongly you feel about him. Sometimes the people we hate motivate our actions as much as or more than the people we love.

>**Example** : Clark and Oliver have always had an interesting friendship. They have also both been involved with Lois, which makes things complicated at times. Lois and Clark mean a great deal to each other, perhaps more than any other Relationship. Oliver keeps things a little cooler with them both. Here’s where the three of them stand in relationship to each other, in dice terms.

>- Clark’s Relationships : *Lois* d12, *Oliver* d8
>- Lois’ Relationships : *Clark* d12, *Oliver* d10
>- Oliver’s Relationships : *Clark* d8, *Lois* d8


### - Statements

Your opinion about someone is what really matters. Your definition of a Relationship not only adds more detail and drama to the story, it also gives your Watchtower ideas for exciting storylines and confrontations.

Your statement for each Lead and Feature you have a Relationship with defines your opinion of that character. Your statement should be one short sentence that sums it all up: *The love of my life*. *I can’t stand him*. *She frightens me*. *I would follow him anywhere*. *He is reckless*. *I don’t trust her*.

This is a game where drama comes first. As you play through your own episodes of Smallville, your experiences and impressions of the other Leads will grow and change. You will be able to challenge your Relationships and alter your statement for these characters to reflect those experiences.

>**Example** : Comparing the die ratings of the Relationships of Oliver, Lois, and Clark serves a purpose, but it’s more of a case of intensity and priorities. It’s much more useful to look at how these three friends see each other, which affects how they handle situations in which the others are involved.

>- Clark’s Relationship statements: *I can’t tell* **Lois** *my secret*; **Oliver** is *reckless*.
>- Lois’ Relationship statements: **Clark** *may be the one*; **Oliver** *isn’t living up to his potential*.
>- Oliver’s Relationship statements: **Clark** *won’t make the hard decisions*; **Lois** *is the one who got away*.

>Will these statements stay the same? Not at all. In fact, they’ve changed frequently as Clark, Oliver, and Lois have gotten to know each other better and as conflict has driven wedges between them. Near the end of the war against Zod’s Kandorian forces, Oliver knows Clark is capable of making hard decisions, and Lois has a dramatic realization of who Clark is. Relationships change, and that’s all part of the fun of Smallville.


### - Using Relationships

Drama happens when two or more characters interact, and drama is at the heart of everything that happens in Smallville. There is more to an action than a simple roll of the dice. There are reasons why you do what you do, people your actions affect, and consequences that may follow.

When you make a roll, you typically include one Relationship die in your dice pool. When deciding which Relationship to add, ask yourself two questions: Who am I performing this action for or against? Does this action agree with the Relationship statement I’ve written for this Lead or Feature?

If you answer yes to the second question, describe how your character’s Relationship drives his dramatic action. If your answer to the second question is no, consider challenging the Relationship.

>**Example** : Oliver’s breaking into the Daily Planet at night to steal an incriminating packet of photos from Tess Mercer’s desk. If Tess sees the photos, she’ll get in the way of a big story Clark and Lois are working on. Josh, Ollie’s player, has to think about Oliver’s Relationships here and see which one’s going to help him get those photos. He has **Lois** *is the one that got away d8* and **Clark** *won’t make the hard decisions d8*. Josh could justify this as a nod to Ollie’s past romance with Lois, but there’s no way it’ll work with his statement about Clark. However, Oliver also has **Tess** *is playing with fire d10*, which is not only more suitable but brings a bigger die. Perfect!


### - Challenging Relationships

Like all Traits dependant on emotions, Relationships are not stagnant. They change and grow at the speeds of the seasons, tides, or even hours of the day. This is doubly true in Smallville where the day-to-day drama can cause hell and high water in the blink of an eye. Old friends become bitter enemies. Longtime foes find friendship and love. Odd acquaintances discover common ground on which to join forces.

Like Values, when you wish to take an action that’s in conflict with your Relationship statement for that Lead or Feature, you can challenge your Relationship and roll three dice instead of one in a Test or Contest. That Relationship steps back by one die rating for the rest of the episode, but in the tag scenes you can rewrite your statement and redefine your connection to regain the original die rating, if you wish.

>**Example** : Clark’s been busy trying to deal with the Kandorians and Zod’s plans to conquer the world, but he’s not sure he can leave the work to anyone else. Oliver’s the ideal choice, but may need convincing since he might not be on board with Clark’s efforts to rehabilitate his people. The solution? Clark challenges his **Oliver** *is reckless d8* Relationship, which not only makes it appropriate to use in this case (he’s depending on Oliver to do things with caution) but it brings in three times the dice. Cam, Clark’s player, adds 3d8 to his roll, steps back his **Oliver** d8 to a d6 for the remainder of the episode, and makes a note to address the statement in a tag scene between Clark and Ollie.

*To challenge a relationship, you*:

>- Declare the challenge.
>- Roll triple the die rating you currently have in this Relationship.
>- Step the challenged Relationship back one die for the remainder of the episode.


### - Rewriting Relationships

Just as your own Values may shift and change over the course of a story, so too can your investment in other people. In fact, your Relationships with other Leads and with Features can change frequently, often dramatically. This is all part of the unfolding narrative in any Smallville story, and it keeps things interesting.

Your Relationships can be stepped back during the course of the episode by challenging them, but this doesn’t immediately affect your statement. Just as with challenged Values, at the conclusion of the episode, you have a chance to rewrite your Relationship statement and how your Lead sees the other person. This happens during a tag scene that involves that other Lead or Feature —or at least your Lead’s experiences with that person.

Any Relationship that was stepped back during the episode may be restored during the tag scene to its previous rating, but because you challenged it, you rewrite the statement to reflect your new perspective. Alternately, you can leave it at the reduced rating and add a die equal to its original rating to your Growth pool. Unlike Values, your Relationships can come and go without any reciprocal stepping up or back of other Traits.

You may also step up a Relationship during a tag scene by using your Growth pool, just as you would with Assets and Resources. You have to put work into a Relationship to make it better.

>**Example** : Tess and Chloe are trapped in Watchtower as the security systems go into full lockdown mode and Checkmate breaks through the firewalls. Prior to this point, Chloe and Tess came to blows over Clark and his ultimate destiny. Tess had **Chloe** *is an obstacle d8*, while Chloe had **Tess** *can’t be trusted with Clark’s destiny d6*. The two women are forced to push aside their mistrust of each other and work to escape a dangerous situation. The only way to get Checkmate off their trail is to kill Tess and thus nullify her tracking implant.

>Mary, Tess’ player, challenges Tess’ Relationship with Chloe during the escape, getting the triple dice for their break-out plan, and Bobbi, Chloe’s player, does likewise; Chloe trusts Tess to be an ally in the future and thus convinces her that she will bring her back to life once the coast is clear. In the tag scene —involving Chloe resuscitating Tess with the paddles— both players step their Relationships back up, having reached a mutual accord, and rewrite the statements. Mary now has **Chloe** *believes in the cause d8*, and Bobbi writes down **Tess** *is willing to make sacrifices d6*. In addition, Bobbi successfully rolls Growth to step up the Relationship from a d6 to a d8 to reflect the stronger bond between the two women.

*To rewrite a relationship, you*:

>- May only do so during a tag scene (at the end of an episode).
>- May step a Relationship up if you stepped it back during the episode by challenging it.
>- Must rewrite your Relationship statement if you’re restoring a challenged Relationship to its prior rating.
>- May step a Relationship up using Growth if you want to improve it more than its original rating.

*Examples of relationship statements*:

Below is a generic list of Relationship Statements. These are just suggestions—instead of choosing from this list, use it as inspiration for writing your own statements. Get creative! And remember, your die rating with another character doesn’t reflect how much you like him, but how strongly you feel about him. Nobody said you couldn’t have a d12 Relationship with the statement *I hate him and all that he stands for*.

>- I can barely tolerate him.
>- She used to be so nice.
>- I must make it up to him.
>- She’s my best friend.
>- He deserves my support.
>- She’s just a silly rich girl.
>- He must pay for his crimes.
>- She has gone too far.
>- He needs me to take care of him.
>- She is stone cold.
>- He’s easy to manipulate.
>- I like her more now…
>- He doesn’t understand my vision of the world.
>- She needs to get her head in the game.
>- I wouldn’t let anything hurt him.
>- She’s someone I can trust.
>- He’ll never change.
>- If this keeps up, I’m going to kill her!
>- He’s about the dumbest person I’ve ever met.
>- She has a good head on her shoulders.

***








# SPIRE - THE CITY MUST FALL - BONDS




Bonds are connections that the player characters share with other people, groups or organisations in Spire. A bond is a broadly positive connection, if perhaps a complicated one.




## Helping out your Friends


Once per situation, when you act in a way that benefits someone you share a bond with, you may do so
with mastery.




## NPC Bonds


You can ask an NPC bond to perform a favour on your behalf – something that they wouldn’t normally do and which puts them, and the relationship, under stress and danger. They’re under no obligation to do what you ask – and the GM might ask for a check to get them to comply – but if they do so, they incur stress based on the scale of the favour asked.

Treat performing a favour as a normal roll. An NPC bond rolls 1D10, +1D10 if it’s within their area of expertise, +1D10 if it’s in their neighbourhood/home. If an NPC bond suffers stress when they roll to achieve a task, treat the relationship as a separate resistance allocated to the character and allocate stress to it.

>**Example** : Lozlyn has an NPC bond with Jackson Crouch, a heretical retroengineer who peddles moody galvanics on the streets of Red Row. She asks him to lend her an experimental gun he’s been working on for an upcoming raid, and the GM rolls to determine the result of her request. This is within Jackson’s area of expertise and also doesn’t involve him leaving his home district, so the GM rolls 3D10. Their highest dice is a 7, which means that Crouch entrusts Lozlyn with the gun (for a while) but the relationship takes D3 stress. Crouch grumbles at the imposition, but hands over the gun, and asks her not to break this one.

You can remove stress from a bond by doing a favour for your ally in return; the bigger the favour, the more stress you’ll remove.




## Sample Stress Levels


- **1 Stress** : Give advice or access to general information within their domain; allow safe passage through space they control; offer temporary accommodation.

- **D3 Stress** : Get you and your comrades access to a private area or event; lend you a piece of equipment; put in a good word with an authority in their domain; turn a blind eye to minor transgressions.

- **D6 Stress** : Gift you a valuable piece of equipment; provide a safe haven for you and your comrades; betray the trust of an outsider; turn a blind eye to major transgressions; commit minor transgressions; engage in moderate-risk actions.

- **D8 Stress** : Betray a friend; commit major transgressions; engage in high-risk actions; donate large amounts of resources to the Ministry.




## Bond Fallout

Bonds are relationships that player characters have with each other or non-player characters; and in the case of NPCs, they can suffer stress and fallout as though they were a Resistance. (In this text, we use “bond” to refer to both the relationship and the person with whom the relationship is shared.) As it’s tracked separately from regular stress, NPC bonds get their own section for fallout.

Bonds don’t count towards your total stress for fallout – they’re handled separately, each as their own track. At the end of each session, roll to check for fallout on each bond that a player marked stress to that session.

(If a bond is temporary, such as one earned from a spell or class ability, then roll for fallout when it is removed.)


### - Minor

- **Mistake** : Your bond’s actions raise suspicion. Mark D3 stress in Shadow.

- **Depleted** : Your bond uses up their resources, suffers an injury, is upset with you, or is otherwise unable or unwilling to help you out. Next time you ask a favour of them, the difficulty of the task they attempt is increased by 1.


### - Moderate

- **In Trouble** : Your bond finds themselves in trouble, and can’t be used until the problem is resolved.

- **Leak** : Your bond unwittingly gives out information that threatens the operation. Mark D6 stress in Shadow.


### - Severe

- **Betrayal** : Mark D8 stress in Shadow. Your bond turns against you, although they will not tell you this until it is too late.

- **Made a Example of** : Your bond’s connection to the resistance is uncovered, and they are made to pay the price. They are dragged into the streets and shot in public after being denounced for their crimes and declared dead.




## Bond Level


There are three levels of NPC bonds: Individual, which is a single person; Street, which is a small-to-medium organisation; and City, which is a district-wide organisation with considerable influence.

>**Example** : Catspaw, a Knight of the North Docks, is Individual level; Catspaw’s knightly order, The Riddling Pig, is Street-level; the Knights of the North Docks are City level.

A bond’s level reflects its overall capabilities, and the scale it works at. The GM and player are encouraged to use their common sense here and judge fairly as to what an organisation of a given size might be able to achieve.

If a bond works against an entity with a lower level than its own, it does so with mastery. A bond can’t work against an entity with a higher level than its own, so try to find a level-appropriate entity within it for them to tackle.

>**Example** : If the Order of the Riddling Pig went after a lone spy, they’d roll an extra dice; if they took on a rival pub, it’d be a normal roll; and they don’t have the sway to take on the Knights as an entire organisation.




## Combining Bonds


You can combine two bonds of equal level and similar description to advance them to one bond of the level above. For instance, if you had a bond with Catspaw from the previous example as well as a bond with Moore, a smuggler, you could remove both and replace them with “River Folk” as a Street-level bond. You’ve still got a connection with both Moore and Catspaw, but the individual connections are less important to you than the relationship with the people who live around the river docks in general.

***








# URBAN SHADOWS - INTIMACY MOVES




Intimacy moves are also unique to each Archetype, reflecting how your character connects with others during moments of closeness. What defines a moment of emotional, mental, or physical intimacy is up to the parties
involved, but the MC might ask, “Is this a moment of intimacy?” if no one is bringing it up. Intimacy moves require both or all parties to agree intimacy occurred: it means they’ve shared a poignant and personal moment with one another.

There are obvious moments of intimacy—sex, vampires feeding, transcendental psychic experiences—but urban fantasy is filled with a broader set of emotional connections. Two characters might get drunk together or tell each other a painful secret or open up about their real desires in the face of the city’s daily grind. Any and all of these moments are triggers for the characters to go deeper into their connection by activating their intimacy move.

When you trigger an intimacy move, both players should read their intimacy moves out loud and follow the instructions given. If an intimacy move requires you to hold one, note that on your Archetype sheet. Sometimes an intimacy move gives other characters moves or bonuses. Tell them to note those on their Archetype sheet as well.

>Olivia is snooping around Veronica’s apartment when she finds Veronica is huddled in a ball near her car, unconscious after barely escaping a vicious beating at the hands of some local demons. Olivia picks Veronica up off the ground and gets her upstairs to safety.

>Andrew, the MC, says, “Veronica, you start to wake up. You’re a bit defensive, but you see that it’s Olivia who saved you.” 

>Sophia, Veronica’s player, nods and says, “Veronica chokes out ‘Thank you…’ and you can see she’s really grateful. You’ve never seen her this vulnerable.”

>Andrew jumps on that statement. “Great! Sounds like a moment of intimacy to me! Right?” Both players nod. “Let’s have you both read your intimacy moves aloud and then we’ll see how they turn out.”

>Sophia says, “Okay, I have to tell you a secret or I owe you a Debt. I think I’ll tell you a secret. Veronica says ‘I lied to Liam when I said that I didn’t know about Yolanda’s plan. I just thought you should know the truth.’ It also says here that you enter my web and owe me a Debt.” Sophia marks down both on her sheet.

>Hadi, Olivia’s player, says, “Huh. I thought you were telling the truth! My move says that I get a ‘specific and clear vision’ about Veronica, and that I can mark corruption to ask more questions. What do I see?”

>Andrew thinks for a moment, then says, “You see Veronica feeding on Liam! They’re in a dark room, and Liam is opening himself up to her, as if he wants her to feed on him. Do you want to mark corruption to ask a question?”

>“Yeah, I want to know ‘When is this going to happen?’”

>“Tonight. You just know it’s going to be tonight. What do you do?”




## The List


### - The Aware

**Intimacy Move** : When you share a moment of intimacy—physical or emotional—with someone who isn’t mortal, mark corruption.


### - The Hunter

**Intimacy Move** : When you share a moment of intimacy—physical or emotional—with another person, ask them a question; they must answer it honestly. They will ask you a question in return; answer it honestly or mark corruption.


### - The Veteran

**Intimacy Move** : When you share a moment of intimacy—physical or emotional—with another person, tell them a story about the past and the lessons you learned.

*Choose 1*:

- You both take +1 forward
- You take +1 forward and they take -1 forward
- Hold 1. Spend the hold to lend a hand to that character from any distance


### - The Spectre

**Intimacy Move** : When you share a moment of intimacy—physical or emotional—with another person, you hold 1. Whenever they get into trouble, you can spend your hold to be there.


### - The Vamp

**Intimacy Move** : When you share a moment of intimacy—physical or emotional—with another person, tell them a secret about yourself or owe them a Debt. Either way, they enter your web and owe you a Debt.


### - The Wolf

**Intimacy Move** : When you share a moment of intimacy—physical or emotional—with another person, you create a primal bond with them; you always know where to find them and when they’re in trouble. This bond lasts until the end of the next session.


### - The Oracle

**Intimacy Move** : When you share a moment of intimacy—physical or emotional—with another person, you gain a specific and clear vision about that person’s future. You can ask up to 3 questions about the vision; mark corruption for each. 


### - The Wizard

**Intimacy Move** : When you share a moment of intimacy—physical or emotional—with another person, decide whether you care about them or not. If you don’t, they go about their business as normal. If you do, they take -1 ongoing to escape until they get some intimacy somewhere else.


### - The Fae

**Intimacy Move** : When you share a moment of intimacy—physical or emotional—with another person, demand a promise from them. If they refuse you or break the promise, they owe you 2 Debts


### - The Tainted

**Intimacy Move** : When you share a moment of intimacy—physical or emotional—with another person, they give you a Debt they hold on someone else.

***