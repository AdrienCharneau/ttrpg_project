#### Blades in the Dark

>core rulebook p.62 (or 53) and p.248 (or 239) 

#### D&D

>https://www.dndbeyond.com/races

#### Earthsea

The cultures of Earthsea are literate non-industrial civilizations and not direct analogues of the real world. Most of the people of Earthsea are described as having brown skin. In the Archipelago, "red-brown" skin is typical; however, the people of the East Reach have darker "black-brown" complexions. The people of Osskil in the north are described as having lighter, sallow complexions, while the Kargs of the Kargad Lands are "white-skinned" and often "yellow-haired". Le Guin has criticized what she described as the general assumption in fantasy that characters should be white and the society should resemble the Middle Ages.

>https://en.wikipedia.org/wiki/Earthsea

#### Exalted

>Exalted - 3rd Edition p. 107 (or106)

#### Numenera

>Numenera - Corebook p.133 (Life in the Ninth World) 

>https://www.reddit.com/r/numenera/comments/awxpuo/comment/ehpvt5o/

#### Reddit idea

>https://www.reddit.com/r/worldbuilding/comments/sisgiw/looking_for_interesting_ideas_for_alien_races/

A while ago I had the idea for two sapient species that evolved in a predator and prey relationship on the same planet so one of them has the instincts of a predator and the other that of a herd animal. They are at peace now but the predators farmed the herd species for food well into their modern era which informs a lot of their present day culture and politics.
