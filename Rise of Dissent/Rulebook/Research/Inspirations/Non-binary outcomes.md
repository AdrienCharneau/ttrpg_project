# FREEFORM UNIVERSAL RPG

Do you get what you want? throw a d6:

- **6 :** *Yes, and...* You get what you want, and something else (**legendary success**)
- **4 :** *Yes...* You get what you want (**complete success**)
- **2 :** *Yes, but...* You get what you want, but at a cost (**only just succeed**)
- **5 :** *No, but...* You don’t get what you want, but it’s not a total loss (**fail by the smallest margin**)
- **3 :** *No...* You don’t get what you were after (**complete failure**)
- **1 :** *No, and...* You don’t get what you want, and things get worse (**epic failure, and then some**)

Alternative:

```
- Yes!
- Yes and
- Yes but
- No
- Hell NO!
```

***




# SPIRE - THE CITY MUST FALL

Dice rolls have non-binary outcomes where, even if a character succeeds an action, that success might also inflict stress on them p.9-11

***




# URBAN SHADOWS

List of actions with their specific non-binary outcomes p.39

***




# CHRONICLES OF DARKNESS

Examples of "dramatic failures" and "exceptional successes" for a variety of actions (persuading, jumping, sneaking...) p.71

***




# BLADES IN THE DARK

## Rolling the dice

There are four types of rolls:

- **Action roll** : when a PC attempts an action that's dangerous or troublesome

- **Downtime roll** : when the PCs are at their leisure after a job, they can perform downtime activities in relative safety

- **Fortune roll** : the GM can make a fortune roll to disclaim decision making and leave something up to chance. How loyal is an NPC? How much does the plague spread? How much evidence is burned before the Bluecoats kick in the door?

- **Resistance roll** : A player can make a resistance roll when their character suffers a consequence they don't like. The roll tells us how much stress their character suffers to reduce the severity of a consequence. When you resist that “Broken Leg” harm, you take some stress and now it's only a Sprained Ankle instead.

Blades in the Dark uses six-sided dice. You roll several at once and read the single highest result. If you ever need to roll but you have zero (or negative) dice, roll two dice and take the single lowest result. You can't roll a critical when you have zero dice.

To create a dice pool for a roll, you’ll use a trait and take dice equal to its rating. You’ll usually end up with one to four dice. Even one die is pretty good in this game—a 50% chance of success.

- If the highest die is a 6, it's a full success—things go well
- If the highest die is a 4 or 5, that’s a partial success — you do what you were trying to do, but there are consequences: trouble, harm, reduced effect, etc.
- If the highest die is 1-3, it’s a bad outcome. Things go poorly. You probably don't achieve your goal and you suffer complications, too.

Each 4/5 and 1-3 outcome lists suggested consequences for the character. The worse your position, the worse the consequences are. PCs have the ability to avoid or reduce the severity of consequences that they suffer by resisting them in exchange for stress.

## Action "positions"

represents how dangerous or troublesome the action might be:

- **Controlled** : exploiting a dominant advantage, set up for success
- **Risky** : taking a chance, going head to head
- **Desperate** : overreaching capabilities, serious trouble, attempting something dangerous

By default, an action roll is risky. You wouldn't be rolling if there was no risk involved. If the situation seems more dangerous, make it desperate. If it seems less dangerous, make it controlled.

> “It's risky? I was thinking it'd be controlled. I know this barkeep is supposed to be a tough old buzzard, but he's not a real threat to me, is he?”

> No, I think the danger is in the situation instead. You're out in the common room in front of everyone, ordering the barkeep to hand over the protection money that he's supposed to pay the Grinders. Who knows if someone in the room might decide to step up and play hero, or to get on the Grinders’ good side? It's an uncertain environment. Maybe if you confront the guy alone, that's more of a controlled position for you. Or maybe if you bring the gang with you and make a show of force in front of everyone.

## Results

### - Controlled:
- **Critical** : You do it with *increased effect*.
- **6** : You do it.
- **4/5** : You hesitate. Withdraw and try a different
approach, or else do it with a minor consequence: a
minor *complication* occurs, you have *reduced effect*,
you suffer *lesser harm*, you end up in a *risky* position.
- **1-3** : You falter. Press on by seizing a *risky* opportunity,
or withdraw and try a different approach.

### - Risky:
- **Critical** : You do it with *increased effect*.
- **6** : You do it.
- **4/5** : You do it, but there's a consequence: you suffer
*harm*, a *complication* occurs, you have *reduced
effect*, you end up in a *desperate* position.
- **1-3** : Things go badly. You suffer *harm*, a
*complication* occurs, you end up in a *desperate*
position, you *lose this opportunity*.

### - Desperate:
- **Critical** : You do it with *increased effect*.
- **6** : You do it.
- **4/5** : You do it, but there's a consequence: you suffer
*severe harm*, a *serious complication* occurs, you
have *reduced effect*.
- **1-3** : It's the worst outcome. You suffer *severe
harm*, a *serious complication* occurs, you *lose this
opportunity* for action.


## Consequences & Harm

### - Reduced Effect:
This consequence represents impaired performance. The PC's action isn’t as effective as they’d anticipated. You hit him, but it's only a flesh wound. She accepts the forged invitation, but she'll keep her eye on you throughout the night. You're able to scale the wall, but it's slow going and you're only halfway up.

### - Complication:
This consequence represents trouble, mounting danger, or a new threat. The GM might introduce an immediate problem that results from the action right now: the room catches fire, you're disarmed etc... A serious complication is more severe: reinforcements surround and trap you, the room catches fire and falling ceiling beams block the door, your weapon is broken...

### - Lost Opportunity:
This consequence represents shifting circumstance. You had an opportunity to achieve your goal with this action, but it slips away. To try again, you need a new approach—usually a new form of action or a change in circumstances. Maybe you tried to Skirmish with the noble to trap her on the balcony, but she evades your maneuver and leaps out of reach.

### - Worse Position:
This consequence represents losing control of the situation—the action carries you into a more dangerous position. Perhaps you make the leap across to the next rooftop, only to end up dangling by your fingertips. You haven't failed, but you haven't succeeded yet, either. You can try again, re-rolling at the new, worse position. This is a good consequence to choose to show escalating action. A situation might go from controlled, to risky, to desperate as the action plays out and the PC gets deeper and deeper in trouble.

### - Harm:
This consequence represents a long-lasting debility (or death).

***




# MEGATRAVELLER

If I don't want the player to know for sure whether they succeeded, then I use the "uncertain" modifier from MegaTraveller.

>Uncertain: If the result of a task attempt is largely "opinion" or if, because of the nature of the task, immediate feedback on how successful the task has been is not possible, then declare the task to be uncertain. With an uncertain task, those associated with the task have some idea of how successful the task attempt was, but they are not certain of the outcomeSensor readings, Interchanges between characters (including any task which might require a reaction roll), psionics, computer programming, repairs, and research are all good candidates for uncertain tasks.Both the player and referee roll for the attempt. The referee's roll is hidden from the player and modifies the player's roll.* If both fall, the result is no truth. The player is misled about the success of the task attempt. Erroneous information is given.* If one succeeds and one falls, the result is some truth. Some valid information is given. The player may fail the attempt and still get information, although he cannot know for sure.* If both succeed, the result is total truth. Totally valid information is given, although the player may stll not believe it.A character may know whether he has succeeded. If the player achieves exceptional success, the referee may elect to tell the player the result of the hidden roll The referee must decide if this is warranted, however, sometimes it is not.

Often I will modify the above rule and instead have the player roll one die while I secretly roll the other. That way I don't have to come up with a "some truth" response but they still can't be sure whether they succeeded (unless their one die plus mods already put them over the success threshold.)

>https://www.reddit.com/r/traveller/comments/11d71mm/do_you_tell_your_players_about_the_difficulty_of/