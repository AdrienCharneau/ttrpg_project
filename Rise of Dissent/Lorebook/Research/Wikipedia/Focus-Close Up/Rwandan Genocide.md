# General




>https://en.wikipedia.org/wiki/History_of_Rwanda

>https://en.wikipedia.org/wiki/Rwandan_Revolution

>https://en.wikipedia.org/wiki/Rwandan_Civil_War

>https://en.wikipedia.org/wiki/Assassination_of_Juv%C3%A9nal_Habyarimana_and_Cyprien_Ntaryamira

>https://en.wikipedia.org/wiki/Rwandan_genocide

>https://www.youtube.com/watch?v=VccTvkAiksg

The first known settlers in what is today present-day Rwanda were the Twa, a group of aboriginal pygmy hunter-gatherers who settled in the area between 8000 and 3000 BC. This group remains in Rwanda today but it was two other groups that the genocide was framed around: the Hutu and the Tutsi, both of which arrived in the area much later sometime between 700 BC and 1580. It's not entirely clear how distinct racial differences were to begin with between the Hutu and the Tutsi, some argue that the differences were more evident along caste lines with the Tutsi predominantly cattle herders while the Hutu farmed the land.

Over time groups or clans became larger and larger until Rwanda was composed of eight different kingdoms by 1700 before eventually merging into the singular Kingdom of Rwanda. By the mid-18th century under a Tutsi king at this point there began to be quite a clear ethnic divide between the majority Hutu and the minority yet usually vastly more powerful Tutsi who controlled most of the more prestigious roles within the government and the military.

***








# Sources




Text

***








# Geography-Locations




- [Map 1](https://www.nationsonline.org/maps/rwanda-map.jpg)

- [Map 2](https://gisgeography.com/wp-content/uploads/2021/07/Rwanda-Map.jpg)

- [Map 3](https://www.worldatlas.com/r/w1200/upload/8a/e6/55/provinces-of-rwanda-map.png)

- [Map 4](https://ontheworldmap.com/rwanda/map-of-rwanda.jpg)

- [Map 5](https://www.mapsland.com/maps/africa/rwanda/large-detailed-political-and-administrative-map-of-rwanda-with-roads-cities-and-airports.jpg)

>https://en.wikipedia.org/wiki/East_African_Rift

>https://en.wikipedia.org/wiki/Geography_of_Rwanda

>https://en.wikipedia.org/wiki/Albertine_Rift_montane_forests




## Landmarks


- **Rivers** : Lake Kivu, Nyabarongo River, Akanyaru River

>https://en.wikipedia.org/wiki/Lake_Kivu

>https://en.wikipedia.org/wiki/Nyabarongo_River

>https://en.wikipedia.org/wiki/Akanyaru_River

- **Mountains** : Mount Karisimbi, Virunga Mountains

>https://en.wikipedia.org/wiki/Mount_Karisimbi

>https://en.wikipedia.org/wiki/Virunga_Mountains




## Cities


>https://en.wikipedia.org/wiki/List_of_cities_in_Rwanda

- **Kigali** :

>https://en.wikipedia.org/wiki/Kigali

***








# Entities-Factions




## Kingdom of Rwanda


>https://en.wikipedia.org/wiki/Kingdom_of_Rwanda

>https://en.wikipedia.org/wiki/List_of_kings_of_Rwanda

- **Gihanga** :

>https://en.wikipedia.org/wiki/Gihanga




## Rwanda Defence Force


>https://en.wikipedia.org/wiki/Rwanda_Defence_Force

- **Bernard Ntuyahaga** :

>https://en.wikipedia.org/wiki/Bernard_Ntuyahaga




## Rwandan Patriotic Front


>https://en.wikipedia.org/wiki/Rwandan_Patriotic_Front

- **Paul Kagame** :

>https://en.wikipedia.org/wiki/Paul_Kagame




## Parmehutu


- **Grégoire Kayibanda** :

>https://en.wikipedia.org/wiki/Gr%C3%A9goire_Kayibanda




## National Revolutionary Movement for Development


>https://en.wikipedia.org/wiki/National_Revolutionary_Movement_for_Development

- **Juvénal Habyarimana** :

>https://en.wikipedia.org/wiki/Juv%C3%A9nal_Habyarimana

- **National Resistance Army** :

>https://en.wikipedia.org/wiki/National_Resistance_Army




## Burundi


-**Cyprien Ntaryamira** :

>https://en.wikipedia.org/wiki/Cyprien_Ntaryamira

***








# Commerce-Society




>https://en.wikipedia.org/wiki/Radio_Rwanda

>https://en.wikipedia.org/wiki/Radio_T%C3%A9l%C3%A9vision_Libre_des_Mille_Collines




## Cultures-Ethnicities


>https://en.wikipedia.org/wiki/Tutsi

>https://en.wikipedia.org/wiki/Hutu

>https://en.wikipedia.org/wiki/Great_Lakes_Twa




## Personalities


Text

***








# Timeline




Text

***