# BLADES IN THE DARK




## Command


When you Command, you compel swift obedience. You might intimidate or threaten to get what you want. You might lead a gang in a group action. You could try to order people around to persuade them (but Consorting might be better).


### - GM questions

- Who do you command? How do you do it —what’s your leverage here?

- What do you hope they’ll do?


### - Controlled

*“You’re on our turf, asshole. There are at least four guns on you right now. Give me the case and piss off.”*

- **Reduced Effect** : He sets the case down and takes a step back. “I’m supposed to get coin for this,” he says.

- **Risky Opportunity** : He snaps a small lighter open and holds the flame near the documents jutting from the valise. “Not until I see some coin!” he says.


### - Risky

*We’ve got each other by the collars, sort of half-wrestling against the wall. I snarl, “Just take your crew and walk away. One death is enough for tonight.”*

- **Harm** : He twists an arm free and belts you across the jaw, ringing your ears. “Now it’s enough,” he says. “Let’s go, boys.” Take level 2 harm, “Concussed.


### - Desperate

*“No, I don’t think so, Bazso. You’ll pay six coin, up front, or we go to the Sashes for a better offer.”*

- **Serious Complication** : You do it! But Bazso looks you up and down with a steely eye. “You’ve got fire, girl,” he says, counting out six-coin worth of silver slugs into a purse and tossing it to you. Start a new clock—let’s call it “Bazso is Fed Up with Your Shit.” Four segments, and, how about you fill three of those in? You might become the next object lesson for young punks who challenge his dominance.

- **Serious Complication** : You don’t do it. Bazso glares at you. “You’ll do it for three and say ‘Thank you Bazso Baz, sir.’” Start a new clock—let’s call it “Bazso Makes an Example of You.” Four segments, and fill three of those in.




## Consort


When you Consort, you socialize with friends and contacts. You might gain access to resources, information, people, or places. You might make a good impression or win someone over with your charm and style. You might make new friends or connect with your heritage or background.

When you Consort with someone, you care about what the other person thinks and feels and in turn they care about what you want (at least a tiny bit). You’re being a charming, open, socially adroit person. You can Consort with people you already know, or try to “fit in” in a new situation so you make a good impression.

To Consort, you need an environment that isn’t totally hostile. You might Consort with the chain gang when you’re thrown into Ironhook (a desperate situation, to be sure) but it’s usually hopeless to Consort with the assassin sent to murder you. When you Consort with people related to your background or heritage, you can expect a better position and/or increased effect.


### - GM Questions

- Who do you consort with? Where do you meet? What do you talk about?

- What do you hope to achieve?


### - Controlled

*Nyrix has been working the Gondoliers for weeks now. I hope she has a good target she can share with me.*

- **Reduced Effect** : She has someone in mind, yeah. A real peach of a target. But she doesn’t know where he lives. You’ll have to follow him home after his shift.

- **Risky Position** : She actually lured a target to the Red Lamp with her. She plops down next to you with this thicknecked Gondolier on her arm, already a bit drunk. She seems to sort of recognize you, or she’s working it out. Nyrix gives you a look like, “Go ahead then!” and closes the curtain to the booth so you all have some privacy.


### - Risky

*I pull out my bottle of fine whiskey and give the room a broad smile. “Who wants the good stuff?” I hope I can keep them occupied while Breen works on the safe.*

- **Harm** : You pull it off, but you have to get wasted with them. Take level- 2 harm, “Trashed.” Your head is killing you and the room is spinning.


### - Desperate

*I know they’re, like, a “secret” society, but can I sort of shmooze around the party and figure out who’s a member of the Circle of Flame?*

- **Serious Complication** : You blend in long enough to spot some subtle hand gestures being used by a few guests. You recognize Arter Clavish, a wealthy nobleman, as one of them. Everything is going great until you realize that all the “normal” guests are gone, and the party has quietly become a Circle of Flame gathering. The time for introductions and bona fides is coming up any moment now. What do you do?

***








# CHRONICLES OF DARKNESS - SOCIAL MANEUVERING




>*Social Maneuvering* p.82




Social maneuvering is a system for applying persuasion and social pressure over time. Social actions within this system may be direct or subtle, complex or simple. For example, your character may shout at another and demand he gets out of the way, or your character may subtly offer clues suggesting someone needs to vote for her.

It is not always possible to get someone to do what you want. For instance, no amount of social maneuvering is going to convince the chief of police in a large city to hold a press conference and admit to murder, even if the player has a dice pool impressive enough to make it happen. This system is designed to allow characters to manipulate or convince other characters to perform favors or undertake actions, but it does raise the question: Is one character dictating another’s actions, and how much of that should be allowed in a roleplaying game? Or, put a different way, can one character seduce another with this system?

Under a strict read of the rules, yes. The goal is “get that character to sleep with my character,” the number of Doors is decided as explained below, and impressions and other factors play into the final result. This is not too different from how seduction and other, less carnal, forms of persuasion actually work — the persuader tries to make the offer as enticing as possible.




## Goals


When using a Social action with this system, the first step is to declare your character’s intended goal. This is as simple as stating what you want the subject to do, and how your character is going about making it happen. You need only announce the initial stages, as the effort will likely occur over multiple rolls, reflecting different actions.

At this point, the Storyteller determines whether the goal is reasonable. A character might, with time and proper tactics, convince a rich person to give him a large sum of money. He probably isn’t going to convince the wealthy individual to abandon all of his wealth to the character (though it might be possible to get him to name the character as his heir, at which point the character can set about speeding up the inheritance process).




## Doors


Once you’ve declared your character’s goal, the next step is to determine the scope of the challenge. We represent this with “Doors,” which reflect a character’s resistance to coercion, her social walls, skepticism, mistrust, or just a hesitance toward intimacy. It’s abstract, and means different things in every given case.

The base number of Doors is equal to the lower of the character’s Resolve or Composure. If the goal would be a breaking point for the character, add two Doors. If accomplishing the goal would prevent a character from resolving an Aspiration, add a Door. Acting in opposition to a Virtue also adds a Door. Doors may increase as the effort continues and the circumstances change. For example, if the goal seems mundane at first but the situation makes it reprehensible, that may increase the number of Doors. If your character gives up on the goal and shifts to another, any Doors currently open remain so, but assess Aspirations, Virtues, and Integrity in case of a potential increase.

Doors must be opened one by one. Each successful roll —not each success — opens one. Exceptional successes open two. Also, Doors are specifically a one-way relationship between two characters. They may each have Doors to one another, or Doors to other characters.




## First Impressions


First (and later) impressions determine the time required between rolls. The Storyteller sets the first impression based on any past history between the characters, the circumstances of their meeting, the nature of the favor being asked (if the acting character is asking right up front — sometimes it’s a better idea not to lead off with what you want), and any other relevant factors.

“Average impressions” call for weekly rolls, which makes the process very slow. Through play, your character may influence the interaction for a “good impression.” This may mean meeting in a pleasant environment, wearing appealing clothing, playing appropriate music, or otherwise making the situation more comfortable. This should not require a roll during a first impression, but requires one if attempted later. An excellent impression requires a roll to influence the situation. For example, you may use a Wits + Socialize to find the right people to invite to a party. Perfect impressions require further factors. It may involve leverage, or playing to a character’s Vice (see below).

Hostile impressions come from tense first impressions, or threatening pitches. These interactions require you manipulate the impression, or to force the Doors (see below).

|Impression |Time Per Roll |
|:---------:|:------------:|
|Perfect    |One Turn      |
|Excellent  |One Hour      |
|Good       |One Day       |
|Average    |One Week      |
|Hostile    |Cannot Roll   |


### - Vices

If your character knows her subject’s Vice, she can use it to influence the interaction. When presenting an offer that tempts that Vice, move the interaction one step up on the chart. As a rule of thumb, if by agreeing to the temptation the target character stands to gain Willpower, then the temptation is a valid form of influence.


### - Soft leverage (gifts and bribes)

Soft Leverage represents offers of services or payments in order to lubricate social interaction. Make the offer. If the recipient agrees, move the impression up once on the chart.

Mechanically, this can be represented by certain Merit dots. For example, a bribe may be represented by a Resources 3 offer, or an offer for a particular service may be reflected by Retainer 3. By default, these bribes give the recipient use of the Merit for a designated amount of time.




## Opening Doors


At each interval, you may make a roll to open Doors to move closer to your character’s goal. The roll might be different each time, depending on the character’s tactics. Some of the rolls might not even be Social. For example, if your character is trying to win someone’s favor, fixing his computer with an Intelligence + Computer roll could open a Door.

As Storyteller, be creative in selecting dice pools. Change them up with each step to keep the interactions dynamic. Similarly, consider contested and resisted rolls. Most resisted actions or contested rolls use either Resolve or Composure, or a combination of the two. But don’t let that stand as a limit. Contested rolls don’t require a resistance trait. For example, Wits might be used to notice a lie, Strength to help a character stand up to threats, or Presence to protect and maintain one’s reputation at a soiree.

Failed rolls impose a cumulative -1 penalty on further rolls. These penalties do not go away after successful rolls. When the player fails a roll, the Storyteller may choose to worsen the impression level by one. If she does so, the player takes a Beat. If this takes the impression level to hostile, the attempt cannot move forward until it improves.


### - Aspirations

Aspirations are quick routes to influence. Find out a character’s goals, wants, and needs, and they can help move interactions forward. If your character presents a clear path and reasoning for how they’ll help a character achieve an Aspiration, remove a Door.

This doesn’t require follow through, but it does require a certain amount of assurance. If the opportunity presents itself, and your character pulls out of an offer, two Doors close.


### - Failure

A social maneuvering attempt can fail utterly under the following circumstances:

- The player rolls a dramatic failure on an attempt to open a Door (the player takes a Beat as usual).

- The target realizes he is being lied to or manipulated. This does not apply if the target is aware that the character is trying to talk him into something, only if the target feels betrayed or conned.

- The impression level reaches hostile, and remains so for a week of game time. The character can try again during the next story.




## Resolution


Once your character opens the final Door, the subject must act. Storyteller characters abide by the intended goal, and follow through as stated. If you allow players’ characters to be the targets of social maneuvering, resolve this stage as a negotiation with two possible outcomes. The subject chooses to abide by the desired goal, or offer a beneficial alternative.


### - Go With the Flow

If the character does as requested, and abides by the intended goal, his player takes a Beat.


### - Offer an Alternative

If the subject’s player chooses, he may offer a beneficial alternative, and the initiator’s player can impose a Condition on his character. This offer exists between players, it does not need to occur within the fiction of the game (though it can). The alternative must be beneficial, and not a twist of intent. The Storyteller adjudicates.

The initiator’s player chooses a Condition to impose on the subject. It must make sense within the context of the scenario.




## Example of Social Maneuvering


Stacy wants Professor Erikson to loan her a book from his private library. She intends to use the book’s contents to summon a demon, but Erickson doesn’t know that. Erickson is protective of his books, but he’d be willing to loan one out under the right circumstances. Erickson has Resolve 3 and Composure 4, so the base number of Doors Stacy needs to open is 3 (the lower of the two). Loaning out of a book wouldn’t be a breaking point, nor does it prevent him from achieving an Aspiration, but it does work against his Virtue (Cautious), so the total number of Doors Stacy needs to open to get the book is 4.

The Storyteller decides that the first impression is average; the two know one another (Stacy is a former student of Erickson’s), but they aren’t close. Stacy arranges to find Erickson at a conference and impresses him with her knowledge of esoteric funerary rites. This requires an Intelligence + Occult roll, plus whatever effort Stacy had to put forth to get into the conference, but changes the impression level to “good.” Now, Stacy can make one attempt to open Doors per day. At the conference, Stacy’s player rolls Manipulation + Persuasion and succeeds; one Door opens. Stacy mentions the book to Erickson and lets him know she’d like to borrow it. He’s not immediately receptive to the idea, but Stacy’s in a good place to continue.

The next day, Stacy emails the professor about a related work (Manipulation + Academics), but fails. Future rolls will have a -1 penalty. The Storyteller decides that the impression level slips to average.

Stacy still has to overcome three Doors. She spends the next week doing research into Erickson and discovers that he wants to become a respected academic. She tells Erickson that she has a colleague who can help break the cipher in which the book is written. This removes one Door without a roll. Now she must overcome two more before he’ll agree. (Note that even if Stacy has no intention of helping Erickson in his quest toward academic glory, as long as he reasonably believes that lending her the book will help him achieve his Aspiration, it opens the Door.)

During her research into the professor’s personality, she also learns that his Vice is Vanity; he likes to see himself as the hero. Stacy goes to his office in tears, saying that she is in danger of being accused of plagiarism for copying a paper, and asks if he can help authenticate her work. Doing this allows him to come to her rescue, which in turn lets him soak up some praise; this would allow him to regain Willpower through his Vice, and as such is enough of a temptation to raise the impression level back to good. Stacy’s player rolls Manipulation + Expression for Stacy to compose a letter of thanks to him, and achieves an exceptional success. The last two Doors open, and Erickson offers to let Stacy borrow the book for a weekend. He probably even thinks it was his idea.

On the other hand, if Erickson is a player-controlled character, his player might decide he really doesn’t want to let that book out of his sight. He might offer an alternative — he’ll bring the book to Stacy, and let her use it for an afternoon. That, of course, might complicate her intended demon summoning, but she does get to put the Flattered Condition on Erickson.




## Forcing Doors


Sometimes, waiting and subtlety just aren’t warranted, desired, or possible. In these cases, your character can attempt to force a character’s Doors. This degree of urgency is high-risk, high-reward. Forcing Doors often leads to burnt bridges and missed opportunities.

When forcing Doors, state your character’s goal and her approach, then roll immediately. The current number of Doors apply as a penalty to the dice roll. The subject contests as normal. If successful, proceed to resolution as normal. If the roll fails, the subject is immune to further efforts at social maneuvering from your character.


### - Hard Leverage

Hard Leverage represents threats, drugging, intimidation, blackmail, kidnapping, or other heavy-handed forms of coercion. It drives home the urgency required to force open a character’s Doors.

Hard Leverage that requires that the character suffers a breaking point removes one Door (if the modifier to the roll — not considering the character’s breaking points — is greater than -2) or two Doors (if the modifier is -3 or less).


### - Example

In the example above, assume Stacy really needs that book now. She goes to Erickson and threatens him at gunpoint to give up the book. Doing this is definitely a breaking point for Stacy. She applies a modifier for her Integrity, and then a modifier based on the severity of the action and the harm it does to her self-image and psyche. She’s not in the habit of committing violent acts and Erickson is obviously terrified, so the Storyteller assigns a -2 modifier to the breaking point roll. This being the case, one Door is removed. If she’d shot him the leg to let him know she was serious, the breaking point modifier would have been at least -3, which would have removed two Doors. In either case, her player rolls Presence + Intimidation plus any bonus for the gun, minus the appropriate penalty.




## Influencing Groups


Influencing a group works in the same way, using the same system. This generally means that influencing a group requires at least an excellent level impression, or forcing their Doors, unless the group meets regularly. The Storyteller determine Doors using the highest Resolve and Composure scores in the group. She also determines three Aspirations, a Virtue, Vice, and relative Integrity score for the group. When resolving the influence, most members will abide by the stated goal. Individual members may depart and do as they will, but a clear majority does as your player suggests.




## Successive Efforts


After opening all Doors and resolving the action’s goal, your character may wish to influence the same person or group again. If successful, successive influence attempts begin with one fewer Door. If failed, or if hard leverage was employed, successive influence attempts begin with two more Doors. These modifiers are cumulative. No matter what, a character will always have at least one Door at the outset.

***








# EXALTED - SOCIAL INFLUENCE




>*Intimacies* `Drives-Ties-Intimacies.md` (`Exalted - 3rd edition.pdf` p.171), *Social Influence* `Exalted - 3rd edition.pdf` p.214

In Exalted, the words of prophets, courtiers, and princes carry as much power as a warrior’s sword or a sorcerer’s magic. Social influence is the system used for resolving interactions in which characters attempt to manipulate, persuade, or coerce each other. With the social actions listed below, characters can gain the trust of others and then use that to influence them, changing or shaping their beliefs over the course of a conversation, oration, or artistic performance.

The core of the social influence system is the influence roll. The dice pool for an influence roll is always one of your social Attributes (Charisma, Manipulation, or Appearance) plus a relevant Ability, against a difficulty of the Resolve rating of the character you’re trying to influence. Successful influence rolls allow you to alter a character’s feelings or beliefs or to convince him to do something for you, based on the social action or Charm used. Occasionally, you will use other Attributes and Abilities to take actions that are not influence rolls, such as rolling (Perception + Socialize) to read a person’s personality, beliefs, or agenda. All of these social actions are described in more detail below.




## Overview


Social influence in Exalted revolves around Intimacies. In terms of gameplay, you’re either going to be influencing someone’s outlook (by creating, destroying, strengthening, or weakening Intimacies), or using Intimacies to persuade someone to do what you want.

Intimacies are the key to this system—defined in full on page 170, a character’s Intimacies dictate what they believe and care about. By studying and interacting with other characters, you can figure out what makes them tick. By engaging other characters socially, you can try to change how they feel about things, and thus change their Intimacies.

Without exploiting an Intimacy, persuasion isn’t possible. No matter how charismatic you might be, a stranger won’t give over his life to your cause just because you say so— there has to be a reason to make him take such course of action. Perhaps they trust and respect you; perhaps they hate someone you’ve set yourself against; perhaps by doing so, they’ll further their own goals or stand up for what they believe in. All such motives are described by Intimacies. In short: You need to play on an Intimacy to be able to talk people into doing things you want them to do. The stronger the Intimacy, the more you can convince people to do in its name.

The resulting gameplay can depict anything from angering a great warrior into declaring war, to gaining the martial support of your sworn brothers because they respect you, to alienating a queen from her consort (to lessen his influence), to using propaganda to stir a revolt, to making a mighty bodyguard distrust and abandon the man he’s sworn to protect.

In such a milieu, enormous charisma and forthright conviction might win the day, but it’s often more effective to first learn what people want and what they care about. You might use your personality to win over a merchant prince, the better to convince him to back your agenda—or you might find it simpler to seduce his favored and trusted concubine, who can whisper your desires into his ear.

This also means Storyteller characters will seek to influence your character’s opinions and actions. Creation is a world of strife and turmoil, full of heroes with their own agendas. Some characters will seek to use or recruit you, and some will offer and take advantage simultaneously— it’s up to your character’s judgment to determine who to trust, who to shun, who to follow, and who to turn away. Creation is a world that thrives on social influence, and no Exalt is an island.

>*SOCIAL INFLUENCE IS ABOUT:*
>*-Discovering the Intimacies of others.*
>*-Protecting your own Intimacies from discovery.*
>*-Exploiting others’ Intimacies to persuade them.*
>*-Instilling others with new Intimacies to affect their feelings or beliefs.*
>*-Strengthening or weakening the Intimacies others already hold.*

>*In short: The social influence system is all about discovering, building or weakening the beliefs or emotional connections of others in order to convince them to do what you want them to do.*




## Order of Actions


Social actions don’t usually have special timing—there are no “Social turns.” When a player’s character tries to seduce a beautiful youth or convince a despot to ally with his fledgling kingdom, he’ll probably be the only person rolling dice —everything else is roleplay between his character and the Storyteller character. When multiple characters attempt social actions in the same scene, who rolls when can be decided by the natural flow of the conversation, or by procedure (in a court, for example, there are strict rules regarding who speaks when). If two characters are both trying to present different arguments to a single individual, then that individual’s player decides who to listen to first. If it’s still not clear who acts in which order, then the characters with the highest (Wits + Socialize) go first, with the Storyteller breaking any ties.


### - Resolve

The Resolve trait represents a character’s mental solidity and strength of will, her ability to resist being influenced by the persuasion and manipulation of others. A character’s Resolve rating is the difficulty for any influence roll made against her. Resolve is calculated as the character’s ([Wits + Integrity + specialty] / 2, round up). If a character is applying her Resolve, then it means she’s resistant to or skeptical of the influence being applied—there’s no need to roll against a character’s Resolve if she’s happy to accept whatever influence is being offered. Resolve is mainly used to resist instill, inspire, persuade, bargain and threaten actions.


### - Guile

The Guile trait represents a character’s ability to conceal his thoughts and innermost feelings. A character with high Guile reveals little about himself through his expression, posture, and speech, while a character with low Guile wears his heart (and Intimacies) on his sleeve. A character’s Guile rating is the difficulty for actions that attempt to figure out his Intimacies, emotions, or thoughts. Guile is calculated as the character’s ([Manipulation + Socialize + specialty] / 2, round up). Guile is typically used to defend against the read intentions action.


### - Intensity

Intimacies represent a character’s relationships, beliefs, ideals, and goals. A character’s Intimacies help determine what types of influence will affect him. The strength of an Intimacy is defined by its intensity — Minor, Major, or Defining. When a character is targeted by an influence roll that opposes one or more of his Intimacies, add a bonus to his Resolve based on the intensity of the most applicable Intimacy. On the other hand, if one or more of a character’s Intimacies supports an influence roll applied against him, then he suffers a corresponding penalty to Resolve based on the most applicable Intimacy.

If more than one Intimacy supports or opposes a social action, only the most relevant Intimacy modifies the target’s Resolve. For example, if you have a Major Tie of hatred against the Realm, and a Defining Tie of hatred toward Ragara Jirun, and Ragara Jirun tries to convince you to act in support of the Realm, both Intimacies would logically oppose the influence, but only the Defining Tie of “Ragara Jirun (Hatred)” boosts your Resolve, as it has the strongest intensity.

Sometimes Social influence is supported by one Intimacy and opposed by another at the same time. For example, if Righteous Thorn has a Defining Tie of hatred toward the Realm, but the Major Principle “Violence solves nothing,” then an attempt to convince him to support an armed revolt against Realm occupation would face no bonus or penalty to his Resolve since the two cancel one another out (-3 for the Defining Intimacy in support, +3 for the Major Intimacy in opposition).

The effects of these different Intensities are listed below.

- **Minor Intimacies** are notable relationships or beliefs that have some influence over a character’s actions without being an integral part of who he is. Characters will usually let their Minor Intimacies dictate their behavior in situations where it is directly relevant, as long as it does not act against a stronger Intimacy or their own self-interest. A man with a Minor Tie of “My Wife (Love)” has a real and profound relationship with his spouse, but it doesn’t have a strong influence on his actions outside the context of his romantic and family life. A peasant with a Minor Principle of “The Immaculate Philosophy” believes sincerely in the teachings of the Immaculate Order, but her reverence has little sway over her non-religious behavior. Minor Intimacies apply a +2 Resolve bonus against influence rolls that oppose them, or a -1 Resolve penalty against rolls that align with them.

- **Major Intimacies** are relationships or beliefs that influence the way the character acts in many areas of life. Major Intimacies influence the behavior of characters even in situations where they are only tangentially or indirectly relevant, and might lead a character to act against their own self-interest as long as they do not face severe harm or life-altering inconveniences because of it. A man with a Major Tie of “My Wife (Love)” has a strong relationship that dictates how he acts outside of just the context of the relationship. A peasant with a Major Principle of “The Immaculate Philosophy” applies the teachings of the Immaculates to almost every aspect of her life. Major Intimacies apply a +3 Resolve bonus against influence rolls that oppose them, or a -2 Resolve penalty against rolls that align with them.

- **Defining Intimacies** represent the most important things in a character’s life, the relationships or beliefs that they place above all others and refuse to compromise. Defining Intimacies influence the greater part of a character’s behavior in all fields of life. If there’s something for which a character is willing to lay down his life, then it is best represented by this level of intensity. A man with a Defining Tie of “My Wife (Love)” is not just a committed spouse; his relationship influences almost everything he does. A peasant with a Defining Principle of “The Immaculate Philosophy” is defined as a person by her reverence for Immaculate tenets. Defining Intimacies apply a +4 Resolve bonus against influence rolls that oppose them, or a -3 Resolve penalty against rolls that align with them.


### - Appearance

While the Appearance Attribute may be rolled as part of social actions, it also has another function in social influence, granting a bonus against weak-willed characters. Those of low Resolve are more easily impressed and awed by striking, beautiful individuals, or easily intimidated by the profoundly ugly (see the Hideous Merit, p. 162).

If a character’s Appearance rating is higher than his target’s Resolve, then he gains a dice bonus on all instill and persuasion attempts against that individual equal to the difference. Thus, a character with Appearance 5 attempting to use a persuade action on an individual with Resolve 3 would gain a +2 bonus. This comparison is made before any bonuses to Resolve are factored in.

If addressing a group (p. 220), compare Appearance to the average Resolve of the group (usually 2 or 3) to determine any appropriate bonus.




## Social Actions


The following is a list of social influence actions, what they can accomplish, and what conditions must be met to retry them after they fail (“Retrying Social Actions,” p. 222).


### - Instill

The instill action is used to change the feelings and beliefs of others. When a player takes an instill action, he declares what he wants to make his target feel or believe. The Storyteller may apply penalties to the roll if this belief is particularly implausible or hard to accept, up to a maximum penalty of -5 for truly unbelievable claims. On a successful roll, the target forms an Intimacy towards that belief.

However, there are limits to what someone will believe when they already have strong opinions to the contrary. The character may need to lessen existing Intimacies before instilling new ones. While the instill action can always be used to create new Minor Intimacies, altering existing Intimacies is more difficult:

- **Strengthening a Minor Intimacy**, or **weakening a Major Intimacy**, can only be done if the target has a different Minor or better Intimacy that supports the attempted influence.

- Likewise, **raising a Major Intimacy**, or **weakening a Defining Intimacy**, is only possible if the target has a different Major or better Intimacy that supports the attempted influence.

- **Strengthening an existing Intimacy** requires that the evidence raised or argument made in favor of strengthening it be more compelling than whatever caused the Intimacy to arrive at its current intensity. For example, if a shopkeeper gained a Minor Intimacy of distrust toward the Guild after learning that they often undercut local merchants, he would need even stronger evidence in order to strengthen his Intimacy to Major. Perhaps he learns that a Guild merchant plans to buy out his shop, or speaks to a man who once partnered with a Guildsman and was betrayed and sold into slavery.


### - Persuade

The persuade action allows you to convince other characters to perform an action or task that you give to them. The extent of the action you can compel with persuade depends on the Intimacies of your target.

Without an Intimacy to support your influence roll, you can only convince others to take relatively trivial and riskfree actions—begging a coin from a passing stranger is about the limit. On the other hand, characters who have an appropriate Tie or Principle can be convinced to undertake great risks, dedicate their lives to a cause, or even die in your name. There are three categories of tasks characters might be asked to undertake, each based on the Intimacy being exploited:

- **Inconvenient Tasks** : Characters who have an appropriate Minor Intimacy can be convinced to perform tasks that pose some mild danger or hindrance to them, as long as it is not severe enough to seriously disrupt their life or livelihood—the prospect of a severe injury, an angry superior, or heavy financial loss is still more than they will allow. With this level of Intimacy, you can persuade people to do things that take longer than a scene to complete, if the amount of time needed is not so long as to disrupt the target’s life.

>*Example: “I need you to deliver this parcel to that big house in Cinnabar District, with the red jade lion statues by the door. If the man on the door has a scorpion tattoo, don’t leave it with him —insist to see the master of the house.”*

- **Serious Tasks** : Characters who have an appropriate Major Intimacy can be convinced to perform even tasks that carry the risk of extreme harm or impediment. At this level, a farmer could be convinced to join your personal militia despite the risk of dying on the battlefield, while an apothecary might provide you with poisons even though he faces imprisonment or corporal punishment for doing so. However, they will still balk if the risk of death or ruin is almost certain. Tasks that take extended amounts of time are possible at this level, even if they require a longterm commitment such as joining an organization.

>*Example: “Just because he’s your father doesn’t make you his slave —why should his fear deny you a place in AnTeng’s glorious uprising against its oppressors? This nation needs heroes; men like you!”*

- **Life-Changing Tasks** : Characters who have a Defining Intimacy can be made to do almost anything. At this level, you could convince a devoted follower to hold off a Wyld Hunt long enough to buy time for your escape, or make a wealthy noble donate the better portion of his fortune to your personal cult. Only in cases where death or utter ruin are absolutely, unavoidably certain will they balk, and even then the Storyteller might decide they’re willing to do it despite all odds.

>*Example: “I know the old scrolls said the heart of this temple is guarded by a fearsome beast of brass and flame. I know it’s frightening, but isn’t this why we came so far and spent our fortunes, to be the first ones to scavenge the Great Ziggurat of Lost Zarlath? I’ll never make it into the final chambers with my leg like this—you’ll have to dare it for both of us!”*


### - Bargain

Bargaining is similar to the persuade action, but doesn’t depend on your target’s Intimacies. Instead, you must offer a bribe, gift, or favor that the character you’re convincing believes is worth the difficulty or danger of the task you’re asking him to perform. The Storyteller should take into account the Intimacies, wealth, and social status of the character in deciding what they will consider sufficient payment (a wealthy noble is not going to be moved to favor your cause in court by the gift of an apple, though a starving beggar might be).


### - Threaten

When all else fails, pain and intimidation are brutally effective motivators. Sometimes also called an intimidate action, threaten works like a bargain: You can convince people to do things without having to appeal to their Intimacies, only rather than offering something they want, you present them with something they don’t —usually the threat of bodily harm, although social blackmail, economic ruin, and general intimidation will also do the job. It can also be used as an instill action which gives the target a Tie of fear towards you, or intensifies such an Intimacy he already has. For a threaten action to be effective, the target must be more afraid of the consequences of refusing you than he is of whatever you want him to do. If the target has any awe- or fear-based Intimacies you’re aware of, that’s usually a good place to start.

Blackmail and bullying won’t make you many friends. Using the threaten action on someone almost always causes him to form an immediate negative Tie towards you, with a context chosen by the target’s player, regardless of whether the attempt succeeds or fails. It can also often weaken any existing positive Ties the target might have, although that’s ultimately up to his player (p. 170).


### - Inspire

The inspire action is used to incite emotions and strong passions in the hearts of others, usually with the Performance Ability, which those others then act on. When a player takes an inspire action, he chooses which emotion he is attempting to inspire—anger, sorrow, hope, lust, hatred, delight, or any other. On a successful inspire roll, the target is impassioned according to the emotion evoked—but the target’s player chooses what form that passion takes.

An impassioned character might form a new passion, or act upon an existing Intimacy that aligns with the emotion evoked. A tribal warrior-queen who is inspired with anger after hearing a Dawn Caste’s roaring oration against her enemies might decide to lead a raid against a hated rival clan, or might decide to begin harassing a political enemy within her own tribe. A merchant prince who is filled with sorrow by the song of a Zenith Caste playing outside his window might spend the rest of the scene weeping with longing for a dead wife or might resolve to undertake some act of generosity with his fortune in remembrance of her kind-hearted ways.

A character doesn’t have to drop everything he’s doing to act upon an inspire action, but he must be genuinely resolved to action. If a devout follower of the Immaculate Philosophy is inspired to make a pilgrimage to the Imperial Mountain, he will generally set his affairs in order, supply himself for the journey, and explain to his family where he is going before setting off.

Inspiration doesn’t automatically create or strengthen Intimacies, but it’s often appropriate for it to do so as characters act upon their inflamed passions. An inflamed passion may be treated as a Major Intimacy for the purposes of modifying Resolve and enabling persuade actions, for as long as the character is acting on it.

The inspiring character doesn’t automatically know what passions he has inspired in his audience; he must use read intentions (see below) to find out. Because a character does not—and without magic, cannot—tailor the outcome of an inspire action to a specific target, inspire actions aren’t subject to the penalty for group influence detailed on page 221, unless specified by a Charm or other effect.


### - Read Intentions

The read intentions action allows a character to discern what another character wants to achieve in a scene of interaction. Reading someone’s intentions is not an influence roll—instead, it is a (Perception + Socialize) roll against the target’s Guile. On a success, the Storyteller should give a brief description of what the character wants out of the interaction: “He’s seducing you to get you alone,” or “She’s making polite small talk and waiting for this party to end.""

Alternatively, this action can be used in order to determine what Intimacies a character has. Before rolling for the action, the player should generally describe what kind of Intimacy he wants to discern (“Does he love anyone?” “How does he feel about me?” “Is he a devout Immaculate?”). On a success, the Storyteller chooses and reveals one of the target’s Intimacies that fits those criteria. If there is no Intimacy that does, the Storyteller should let the player know this.

The read intentions action is not a form of magic. The character using it is analyzing the target’s words and behavior to get a better feel for his motives and Intimacies, and the Storyteller should use common sense in deciding how much information can be gleaned from a character’s behavior and appearance. You might deduce that a young princeling is in love from a look of longing in his eyes or a wistful sigh, but discerning his paramour’s identity might be impossible unless she’s physically present or if he’s carrying some evidence of her identity.

Finally, a character who is unaware he’s being observed suffers a -2 penalty to his Guile.




## Resisting Influence Rolls


The base difficulty for an influence roll is always the Resolve rating of the character targeted by the roll. On a failed roll, the character is unimpressed or unmoved by whatever argument or offer was made. On a successful roll, the target is persuaded or moved by your words.


### - Spending Willpower

Even if a character’s Resolve is overcome, he may still have an opportunity to deny the influence. If the influence is trying to change how he feels, such as by creating, destroying, or changing his Intimacies, he may spend a point of Willpower to:

- Stop a new Intimacy from being created. Although the character is moved, he just can’t afford to invest himself in a new person or cause!

- Stop a Major or Defining Intimacy from being weakened. Even though the influence was convincing, the character just has too much invested in the Intimacy to give up now, even if he wants to!

- Reject a successful inspire action—the character uses his force of will to deny his swelling passions.


### - Decision Points

Refusing successful influence to make the character do something is trickier. When a character fails to resist such influence with his Resolve, he enters into a special state called a Decision Point.

In the Decision Point, the player must choose an Intimacy and explain how it justifies resisting that specific influence. The Intimacy he chooses must be of equal or greater intensity than the Intimacy which supported the influence roll, and it can’t be the one that strengthened his Resolve against the roll in the first place—the influence already overcame that particular source of reluctance when it beat his Resolve, after all.

If those requirements are met, and if the Storyteller accepts the player’s argument for why one of his Intimacies would make him reject the influence, then the character may spend one Willpower point to resist the influence roll. Otherwise, resistance is impossible.

As an example Decision Point, consider Son of Wolves, a Zenith Caste whose Resolve rating has just been overcome by the influence roll of Naya, a scheming Sidereal. The Sidereal seeks to turn him against his Circle (a serious task), and her persuade action is supported by Son of Wolves’s Major Principle of belief in the Immaculate Faith, which manifests as a lingering and unshakable conviction that he and his fellow Solar Exalted are Anathema. Unless Son of Wolves can match this with an Intimacy of Major or Defining intensity, he will be powerless to defy the truth he hears in Naya’s words. While he has a Defining Tie of loyalty towards his Circlemates, he already used that Intimacy to bolster his Resolve, rendering it unavailable in a Decision Point. Luckily, he also has a Major Principle of “I can’t abide a traitor,” owing back to a terrible betrayal he suffered long ago. His player argues that this allows him not to betray his Circlemates even if they are Anathema. The Storyteller accepts this argument, and Son of Wolves is able to spend a point of Willpower to resist.


### - Lengthy Debates

Sometimes after a character has spent Willpower in a Decision Point in order to reject influence, the initiate will return with a new or stronger argument. When this happens, the initiate is engaging the reset rules for social actions (“Retrying Social Actions,” p. 221). If the same issue is being argued as a result, the target may not use an Intimacy which boosted his Resolve against the initial roll. After all, that logic was not enough to stop him from being forced into the Decision Point in the first place. To boost his Resolve against the initiate’s second attempt, he must find another Intimacy to raise his Resolve—or use the one that allowed him to reject influence in the Decision Point. Remember that according to the rules of Decision Points, the Intimacy a character uses to raise his Resolve cannot be invoked in a Decision Point. This rule continues to apply throughout the story—as long as the initiate can find new angles with which to demonstrate the rightness of her persuasion, she can cause her target to use up all of his valid excuses over the course of a story. When a character has no valid Intimacies with which to enter a Decision Point, beating his Resolve will finally convince him to agree with the initiate’s arguments. Thus a prophet may soften the heart of a prince over time.

Special note: Because your Intimacies are the key to avoiding influence you absolutely cannot abide, it’s important to spend a few moments thinking about good “safety net” Intimacies when making your character. For example, if the idea of your character violating his sworn oath is antithetical to how you imagine him, you probably want a Major or Defining Principle along the lines of “My word is my bond” or “Once I give my word, I never go back on it.”


>*- What does influence mean? :*

>*Social influence isn’t some kind of mind control. The changes produced by successful influence rolls represent the way in which people can be affected by the opinions and attitudes of others. When a Zenith Caste prophet takes an instill action to convince a warriorprince of the righteousness of the Unconquered Sun, he is making reasoned arguments or emotional appeals that the warlord will agree with, not brainwashing the prince into his cult.*

>*Resisting influence means you’re exerting effort to override your feelings or better judgment. You may be convinced by a person’s argument or moved by his emotional appeal, but the stakes are too high and your idealism is too strong for you to back down. Alternately, an application of Resolve can be seen as rejecting an appealing or well-supported claim that contradicts one of your own strong beliefs (hello, Intimacies). However, a character need not strongly object to an idea to apply Resolve. She may just be uncertain about a course of action, wary of an unforeseen risk, or may simply hate the speaker despite his wisdom.*

>*That said, Exalted does feature mind control and other forms of severe influence. This is always the province of powerful and dangerous magic, generally marked with the psyche keyword.*




## Unacceptable Influence


There are limits on what can be achieved with social influence. No amount of charisma is sufficient to talk someone into suicide, or to convince a die-hard patriot to betray his cause. If a request is so antithetical to the nature and personality of its target that it cannot possibly succeed, it is said to be unacceptable influence. A character targeted by unacceptable influence may reject it outright without spending Willpower, even if his Resolve would not normally be high enough to defend against it. A player can still choose to have his character follow the course of action put forward by unacceptable influence, but only because he thinks it makes sense—the character cannot be coerced into doing it. Some powerful Charms and spells can also compel characters into bowing to unacceptable influence.

Unacceptable influence includes:

- Any instill action to strengthen or weaken an Intimacy which doesn’t exploit an appropriately strong Intimacy to do so.

>*Example: A Deathlord’s agent sits down next to a Solar in a teahouse and intimates that the Solar’s Lunar companion is not to be trusted, but she gives no reasons—only dim intimations. The Solar has a Major Tie of trust toward the Lunar. Because the Abyssal exploits no Intimacies to support her suggestions, the Solar can ignore her attempt at weakening his Intimacy toward the Lunar.*

- Any persuasion attempt which doesn’t exploit an Intimacy strong enough to support the proposed task.

>*Example: A zealous Immaculate missionary attempts to convince the high priestess of Hamoji, great volcano god of the Wavecrest Archipelago, to abandon her life of worship. This would be a life-changing task, and the priestess has no Defining Intimacies which would support the missionary’s exhortations. As such, the priestess is free to reject the proposed influence at no cost, without the need of a Decision Point.*

- Any bargain attempt which fails to offer a properly enticing incentive or threaten action which is insufficiently threatening.

- Any influence that would cause a character to kill himself, or to do something that he knows would result in his certain death.

- Any influence that would cause a character to completely abandon or end one of his Defining Intimacies is unacceptable. A farmer with a Defining Tie of love to his wife might be seduced by another person, but could not be convinced to abandon or murder his wife. A Solar with a Defining Principle of “I will win the allegiance of the sorceress Raksi” could not be convinced to kill her, as this would make fulfilling that goal impossible. Weakening a Defining Intimacy is still allowed, making it possible to first degrade an Intimacy from Defining to Major and then issue the otherwise unacceptable influence.

- Any seduction attempt that violates a character’s sexual orientation (as defined by the player, or by the Storyteller in the case of Storyteller characters) is unacceptable.

- Certain Charms allow characters to define special kinds of influence that they may treat as unacceptable.




## Social Complications


A number of considerations may arise when winning friends and influencing people:


### - One Target vs. Many Targets

Much of the time, an influence roll only affects one character. An envoy warns a prince of a coming war; a smuggler bribes a magistrate to overlook his cargo; a hedonistic noble seduces an innocent farm boy. Sometimes, however, a character may wish to make a single influence roll against multiple characters. He can choose to target only a select group, or to apply the influence roll against anyone who hears him. However, people find it easier to ignore arguments that are not directly addressed to them. Whenever an influence roll targets more than one character, it suffers a -3 penalty.

Because the different targets of an influence roll can have varying Resolve ratings, the success or failure of the action is determined separately for each target. A Dawn Caste who rolls four successes to threaten a mercenary cadre into backing down from a fight might successfully intimidate the rank and file with Resolve 2, but not the unit’s God-Blooded leader with Resolve 5.


### - Written Social Actions

The written word can be used to persuade or manipulate others just as easily as speech or whisper. Characters who wish to convey influence through a letter, pamphlet, book, or other written work do so as a written social action. The time taken to create a written missive varies based on the form and length of the work. The Storyteller decides how long it takes, with a minimum time of five minutes in most cases. Likewise, the time needed to read the missive is decided by the Storyteller based on length.

The Ability used when rolling for a written social action is always Linguistics. When a character reads the message, compare the successes rolled for it to his Resolve to determine if it succeeds, as with normal influence rolls. Written social actions can be written to apply either against a single intended reader or against anyone who reads them, with the usual effects for targeting multiple characters (p. 220).


### - Gestures and Body Language

When a character attempts to communicate through gestures, appearance, and body language alone, the target of such silent influence adds +2 to his Resolve. This is normally only useful for attempts at intimidation or seduction, but players are free to be creative and think up with other applications. Many things are impossible to communicate silently—no amount of hand-waving or quirked eyebrows can explain the intricacies of a First Age relic or the intricate politics of a Dynastic household. This penalty doesn’t apply to inspire actions using dance.


### - Overturning Influence

Imagine this scenario —Rellus the Glorious Mantle of Dawn, warrior of the Dawn Caste, is speaking with his Lunar lover, who has a standing grudge against the Mask of Winters. She persuades him to round up his Marukani followers and ride against the Mask’s armies, using a persuade action. Upon hearing of his forces mobilizing, a Sidereal approaches Rellus and attempts to convince him that his planned war is ill-omened, and that he would be better off seeking allies rather than taking on the Deathlord alone. Rellus has already been persuaded —how to resolve this situation?

Characters in the world of Exalted are generally loath to abandon a course of action one they’ve set themselves upon it, and so overturning existing social influence with additional social influence is difficult. First, a character who has already been persuaded to do something receives a +3 bonus to his Resolve against any influence that would cause him to abandon or disregard that persuasion. This bonus stacks with the Intimacy bonus from a relevant Intimacy. Second, a petitioner who wishes to overturn existing persuasive influence must spend a point of Willpower before making her argument and roll. So, in the above example, the Sidereal must spend a point of Willpower to impress upon Rellus the dire urgency of the omens she has foreseen—and Rellus’s Resolve is automatically at +3 to resist her influence, before Intimacies come into play.

If the contradictory persuasion succeeds, the targeted character may use a Decision Point to resist it by citing a conflicting Intimacy, without spending a point of Willpower to initiate the Decision Point—it’s easier to stand by a hard-fought decision than to abandon it and reverse yourself. Conversely, if he wants to abandon his present course of action and accept the new influence, the character must spend a point of Willpower and cite the Intimacy which was used to change his mind.

It’s possible to use these rules to model several reversals— if Rellus were to listen to the Sidereal and order his forces to stand down, his Lunar lover would have to spend a Willpower point to convince him to ignore the Sidereal and follow her original advice! However, the Lunar can’t use the same argument she did the first time around—that has already been rejected, and so she’ll have to come up with a new tack, playing off of a different Intimacy to get Rellus to change his mind again.

These conditions remain in place for one story after a character has been influenced to take a course of action— once the next story begins, any lingering influence can be overturned with an ordinary persuasion, bribery, or intimidation attempt.




## Retrying Social Actions


When a scheming courtier’s bid to gain the trust of a prince goes awry or a young lover fails to seduce the object of her desires, they cannot simply repeat their old arguments and expect a different result. Instead, they must change the situation in a way that allows them to try again. Below are retry conditions for each of the social actions. Once this condition has been met, the character may attempt to retry.

For example, Shan Min, an Eclipse Caste merchant, tries to win the favor of a barbarian warlord with an offering of steel weapons and horses—a bargain action. However, he fails his (Charisma + Presence) roll. The warlord still takes the weapons, but refuses to ally with Shan Min. As described below, he cannot retry the roll until he can make a better offer to the warlord, such as an armory of enchanted weapons or a jade daiklave. Having done so, he can make a new roll, and win the warlord’s tribe over to his cause.

The Storyteller should use common sense in adjudicating retry conditions. Once Shan Min has failed his bargain roll, he couldn’t simply send one of his circlemates to offer the same bribes to the warlord in hope of a second chance— even though that character didn’t make the initial roll, common sense dictates that he must still bring a bigger bribe to win over the chieftain.

- **Instill** : If you fail an instill action, you must present your target with substantially greater evidence for whatever you are trying to convince him of before you can retry. A Solar who wishes to make the people of a small farming village trust him might need to defend them from a predatory god or greedy Imperial tax collector before they will accept him. A strategist who fails to convince a general that a war with An-Teng would be a disaster would need to bring back intelligence reports that prove his point before he will be heard. A suitor trying to win a prince’s affection would need to present a much grander display of love.

Alternately, you can try again after the current story has ended.

- **Persuade** : There are three ways to retry a failed persuade action. One is by making a different argument, playing on a different Intimacy of equal or greater strength. Another is to wait and try again during the next story. The final route is to wait until the Intimacy that supported your influence roll has been strengthened to a higher level of intensity—either by using a different social action to strengthen it, or by waiting until the character has strengthened it himself—and then try again.

- **Bargain** : You can only retry a failed bargain roll by making a new offering that’s substantially greater than your previous one. If a peasant is unswayed by a gift equivalent to a day’s wage, then perhaps he will listen to a week’s or month’s pay. A courtier who rejects an offer of marriage to a minor noble might be convinced by a wedding contract with a young Dragon-Blood.

- **Threaten** : You can only retry a failed Threaten roll if you significantly escalate the threat used to coerce your target. A torturer might progress from light cuts to broken bones to potentially lethal torture. A scheming eunuch who fails to cow a court rival with the threat of revealing an illicit affair might threaten to frame him for treason. A Solar might flare his anima banner, revealing that the threat behind his menacing glare is far, far greater than his foes first realized.

- **Inspire** : You must wait until the scene has ended to retry an inspire action.

- **Read Intentions** : Once a Read Intentions action has failed, it can’t be retried on the same target for the rest of the scene.




## Social Actions in Combat


All of the social actions described above work normally during combat, although within the dictates of common sense—you can certainly shout out a fast, desperate offer to triple a mercenary’s pay if he’ll switch sides in the midst of battle (a bargain attempt), or even confess your hopeless love for the beautiful Abyssal trying to take your head (an instill action), but trying to engage in complex contract negotiations in the space of a single turn is probably absurd.

All social actions are considered combat actions, and may be placed in a flurry. They’re resisted as normal. One special case is worth independent consideration: surrender.

Pleading for mercy can convince enemies to accept your surrender rather than killing you. This might be a persuade action, or even a bargain (“My family will pay a great ransom for my safe return!”). On a successful roll, your enemy will allow you to surrender, taking you captive or letting you retreat rather than killing you. In most cases, this doesn’t require an Intimacy to exploit at all—letting a defeated stranger run away is no great hardship. In some cases, it might require exploiting a Minor Intimacy (when the enemy has some particular reason to want you dead rather than merely captured or vanquished), or even in extraordinary cases a Major Intimacy (such as a Solar trying to surrender to the Wyld Hunt—a course we don’t generally recommend, since the master of the Hunt is likely to have a valid Intimacy to cite in the resulting Decision Point as grounds to deny you mercy.)


>*- Playing to the Audience :*

>*Sometimes, a player will attempt to simply sway a mass of people with social influence, rather than appealing to any one character or specific group. While a player character or significant NPC should always be allowed to resist influence with her Resolve, if all the characters in the audience are effectively minor, unimportant characters who would not normally be given their own mechanical definition, the Storyteller can simply treat the entire audience as a single entity with a single Resolve rating.*

>*Most mortal audiences will have a Resolve of 1 or 2, while audiences of exceptional mental resilience, such as ascetic monks or spiritually powerful beings, may have a Resolve of 3 or 5 (though there are few cases when it would be appropriate to lump magical creatures into an audience) The Storyteller can assign Intimacies to the audience based on the general feelings of the crowd—an audience composed of Gem citizens is likely to have a Tie of patriotism towards Gem, for example.*

>*Generally, when the rules for targeting an audience are brought into play, the Storyteller shouldn’t bother with the rules for rejecting influence with Willpower, but instead define what percentage of the audience is convinced, usually based on how it aligns with their Intimacies. This might range from the entire crowd, if the influence aligns with a Defining Intimacy, to only a sizable minority, if it opposes a Defining Intimacy.*

>*Any character who has her own individual traits always resists social influence using her own Resolve and Willpower, even if she’s a member of an audience being targeted by such an action. In the case of magic that specifically targets an audience, such characters are treated as separate targets, rather than automatically following the actions of the crowd.*

***








# MONSTERHEARTS - STRINGS




Strings represent the emotional power that you have over others. You gain Strings on specific individuals, and can then spend them as a sort of currency during interactions with those individuals. Strings can be earned by Turning Someone On, Shutting Someone Down, or in ways specific to each Skin. Strings are abstract, reflecting a general shift in power within a relationship. There is no specific response demanded when someone gains a String, other than playing your character with authenticity as power dynamics are shifting.

There is a space to keep track of Strings on the character sheet. The best way to use that space is to list the other characters, and then whenever you gain a String on one of them draw a little circle next to their name. When you spend the String, fill in the circle.

Whenever you spend a String, there should be something in the fiction to explain the mechanical effect. If you’re adding 1 to a roll against them, what’s causing that effect? Do you utter some snide remark that throws them off balance? Or if you’re tempting them to do your bidding, how do you use your emotional hold to do that? Do you dangle a juicy secret in return for their obedience, or is it your seductive allure drawing them in? Sometimes it can help to think back to how you got the String in the first place.




## Pulling Strings


When you spend a String on someone, choose one:

- Tempt them to do what you want
- Give them a Condition
- Add 1 to your roll against them
- Add 1 to the harm you deal them

The first two options on that list are your proverbial carrot and stick. You can tempt people to do what you want, offering incentives for bending to your will. Or you can punish the people who have wronged you or let you down, generating nasty rumours about them or otherwise using your leverage to mess with their social standing.

When you tempt a character to do what you want, if it’s not something that they can reasonably do in that scene, simply agreeing to do it counts. Whether they follow through on that agreement is up to them.

A character doesn’t need to be present for you to give them a Condition. Talking behind somebody’s back is a tried and true method for working out your resentment and bitterness.

When you tempt another player’s character, it counts as offering them an experience point if they do what you want. When you tempt one of the MC’s characters, the MC will tell you what sort of bribe, threat, or coaxing it’ll take to get that character to do what you want right now.




## Conditions


Conditions represent your character’s social situation and weaknesses. They’re the things that people are saying and thinking about you that shape how you’re treated, or even how you see yourself. Conditions take the form of gossip, unsavory opinions, and interpersonal labels. Conditions are given to people through Shutting Someone Down, Pulling Strings, and a few other avenues.

If you take advantage of a Condition that someone has while making a move against them, add +1 to your roll. In order to take advantage of a Condition mechanically, though, you need to explain what that looks like in the story.

***








# URBAN SHADOWS - DEBTS




Once everyone has finished filling out their Archetypes, it’s time to introduce the characters and assign starting Debts, a network of favors and obligations that bind the characters to each other amidst the chaos of the city.

First, each player takes a minute or two to introduce their character, sharing their name, look, demeanor, and answers to intro questions, as well as any additional information that the other characters might know about them. If your character has a reputation in the city for a particular kind of work or behavior, now’s a good time to note that. Other players, especially the MC, might ask you a few questions about your character to help the group get a better grasp of your life in the city.

Once each character has been introduced, assign Debts. Begin with the person to the left of the MC and go around the circle, each player reading aloud one of the Debt moves listed on their Archetype. Each Debt move establishes a connection between the PC and another character in the form of backstory and ongoing relationships. Most of these relationships will be with other PCs, but it’s fine for some of them to be directed at characters controlled by the MC (non-player characters or NPCs), especially if there are a small number of PCs.

When it’s your turn to assign a Debt, choose one of the Debt moves listed on your Archetype, decide which character the move is describing, and read it out loud to the group. Then work with that player and the MC to make the Debt make sense given what you already know about the characters. Remember that we want you to build on what’s said before, drawing connections whenever you see an opportunity to enrich the story.

When someone owes you a Debt, write down both who owes you and what you did for them on your Archetype. In order to cash in a Debt later, you’ll need to remind them why they owe you a favor. The person that owes the Debt doesn’t have to mark down anything, but they might want to keep a record anyway.

>Ryan decides to start with the first Debt move on the Fae Archetype; he says “I think Veronica, The Vamp, broke an important promise to me, and swore she would make it up to me. She owes me two Debts.” He looks to Veronica’s player, Sophia, to see what she thinks of that Debt. Ryan liked her introduction, and he thinks that Volund probably has big plans for such a… useful beast.

>Sophia grins. Veronica is a street kid vampire determined to move up in the world, and breaking a promise sounds like exactly something she would do. “Maybe I told you I would protect you from something and bailed at the last minute?” Sophia’s put her extra stat point in Blood, and she likes the idea that the other characters would come to her for protection.

>Ryan nods. “Yeah, I’ve been trying to deal with a gang of demons, and you promised that you’d be there when I scheduled a meeting with them. But on the day of the meeting, you were nowhere to be found. Sound good?”

>“Yup! I felt really bad about it later, though. I probably brought you some shitty beers or something to say I was sorry.”

>Ryan notes the Debt on his Archetype: “Veronica owes me for breaking her promise to keep me safe from the demon gang.” Ryan and Sophia will continue to assign Debts and fill in other relationships, but they both know that Veronica owes Volund a favor or two for this broken promise. Ryan looks forward to cashing in that Debt later when he needs some muscle!




## Debt Moves


There’s no way to make another player character dance to your tune in Urban Shadows besides Debt. You can’t persuade them with logic or intimidate them with violence; you might get them to go along with you, but only because it’s easier than fighting with you. There’s no way to roll the dice to make folks make tough choices.

Unless you’ve got a Debt. Once people owe you, you can ask them for all kinds of things. And when you put the weight of a Debt behind something, it carries all new meaning. If they want to be taken seriously in the city, then they need to pay what they owe. Only someone who can’t be trusted—who isn’t worth saving when the chips are down—goes back on their accounts.


### - Do someone a favor

*When you do someone a favor, they owe you a Debt.*

Gaining Debts means going out of your way to do a favor for other characters. Anytime you help someone out without recompense, you get to claim a Debt from them that can be cashed in at a later time. You can claim Debts from both PCs and NPCs, provided you do something useful for them.

A favor has to be acknowledged by the other party or it isn’t a Debt; you can’t do something for someone and claim a Debt if they don’t really care about your efforts. It’s cool to work this out in the fiction in advance —“Yeah, I can totally help you out, but you’re going to owe me”—or to step out of character and draw attention to something that happens in the moment—“I just saved your Fae’s life. I think she owes me a Debt.” If you forget to call out a Debt in the moment, don’t fret. You have a chance at the end of each session to collect Debts you think you’re owed if you miss them in the moment.

If you do someone a favor because you’re already getting something from them, it doesn’t count. Consider that a wash. No need to keep the books when everybody’s breaking even. That said, one-sided deals—“Rat out your friends, and I’ll give you a ride across town”—don’t count as even exchanges. You can’t avoid a Debt by offering something paltry.


### - Cash in a debt

When you cash in a Debt, remind your debtor why they owe you in order to...

*...make a PC*:

- Do you a favor at moderate cost
- Lend a hand to your efforts
- Get in the way of someone else
- Answer a question honestly
- Erase a Debt they hold on someone
- Give you a Debt they hold on someone else

*...make an NPC*:

- Answer a question honestly about their Faction
- Introduce you to a powerful member of their Faction
- Give you a worthy and useful gift without cost
- Erase a Debt they hold on someone
- Give you a Debt they have on someone else
- Give you +3 to persuade them (choose before rolling)

Cashing in a Debt is easy. Whenever you want something from someone who owes you a Debt, remind them why they owe you and tell them what you want. Anything from the list is legit at all times, including making them answer questions honestly or giving you a Debt they’re holding on someone else.

You don’t need to quote the reason for the Debt exactly; alluding to the favor owed is plenty reason enough to trigger the move. What matters is that both parties recall the Debt and acknowledge it, and that they both know that it’s been spent if the debtor honors the Debt. Of course, PC debtors can always refuse to honor a Debt with all the cost and consequences that come with going back on their word.

Getting a favor at moderate cost is a broad option, encompassing all sorts of favors that aren’t already on the list. You might ask someone to hide something sensitive for you, steal something valuable, or back you up in a tough situation. It’s all dependent on the skills and talents of the character who owes you the Debt. For some characters, killing someone is a favor they can perform at moderate cost. As always, the MC arbitrates any disputes on what’s moderate, but look to the fiction to get a sense of what might be moderate for any given character.

Demanding that a PC answer a question honestly is the only way to get the absolute truth from another PC. None of the options on *figure someone out* tell you if a character is lying about any particular statement —*figure someone out* focuses on the political situation a character is in, not the truth—but you can force another PC to be honest by cashing in Debts. If the PC honors the Debt, their answer to the question must be full and complete: no misleading or tricks!

Erasing Debts means that you’re spending a Debt to erase a Debt, effectively clearing the books. You lose a bit of your control over the person who owes you the Debt, but you can get out—or get someone else out—from under their thumb, assuming they don’t hold other Debts over you or the person you’re trying to save.

Transferring Debts requires that you know about the Debts in question. Most people are pretty open with who owes them, but you’ve got to get the information before you start asking people to hand over their Debts to you.


### - Refuse to honor a debt

Just because someone has a Debt over you doesn’t mean you have to honor it… right now. Maybe it’s not a good time or the thing they’re asking for is just out of your reach at the moment. You can’t be all things to all people all the time. And sometimes people ask for “reasonable” things that are going to cost you more than you want to pay.

Refuse to honor a Debt lets you try to slip out of your obligations. It won’t mean you no longer owe the other character—the best you can hope for is delaying the payback for another time—but it might keep you out of the fire until you can sort the situation out.

When you refuse to honor a debt, roll with Heart. On a hit, you weasel out of the current deal, but still owe the Debt. On a 7-9, you choose 1:

- You owe them an additional Debt
- You lose face with their Faction
- You mark corruption

When you try to push off your Debts and miss, you face a tough choice: honor the Debt or face the consequences. Your debtor gets to pick two different options off the list for you or cancel all the Debts you hold over other people. If you won’t honor a Debt, then why should anyone honor their Debts to you? Best be careful.

If you successfully refuse a Debt, the character who tried to cash in a Debt can’t cash another in with you until the situation changes. After all, they already asked for one favor, right? No point in asking again so soon. You’ve already said “no” once.


### - Drop someone's name

When you drop the name of someone who owes you a Debt, roll with their Faction. On a hit, their name carries weight and gives you an opening or opportunity. On a 10+, you keep the Debt and mark their Faction. On a 7-9, you have to cash in the Debt. On a miss, erase the Debt and brace yourself.

Dropping someone’s name means using it as leverage against your opposition, granting a moment’s advantage or hesitation and creating an opening that was previously closed. You might use it to get someone to rethink hurting your character or to gain access to a sensitive location. It’s useful any place you think the person’s name might help you get by.

Just saying the name of the person who owes you isn’t enough; you’ve got to inform your opposition that who you’re naming owes you and you could call in that favor against them specifically. On a hit, that threat carries a serious amount of weight, enough to get you an opening or opportunity. When you miss, you find out too late that you’ve overstepped your bounds, that the name you dropped isn’t going to offer you much assistance. In fact, it might even get you killed.

You have to cash in the Debt on a 7-9, marking it off your sheet and marking Faction, because you’ve already used up that favor; you have to erase the Debt on a miss because you’ve been throwing the Debt around recklessly. No one wants their name dragged through the mud every time you want something. Erasing the Debt means that you don’t get to mark Faction for spending it. See Advancement on 162 for more on marking Faction.




## The List


### - The Aware

**Debts** :

- Someone told you their secrets and you haven’t told anyone about them yet. They owe you 2 Debts.

- Someone thinks they’re protecting you, but it’s really more like you’re protecting them. You owe each other a Debt.

- You’re leveraging dirt you have on someone to get their help with something. You owe them a Debt.


### - The Hunter

**Debts** :

- Someone has enlisted you to protect them from something very dangerous. They owe you a Debt.

- Someone keeps you equipped and supplied. You owe them 2 Debts.

- You consider someone a friend even though the friendship keeps bringing you trouble. They owe you a Debt.


### - The Veteran

**Debts** :

- Someone relies on you for training or knowledge. They owe you 2 Debts.

- You’re working on something big for someone, and it’s nearly ready. They owe you a Debt.

- Someone keeps pulling your ass out of the fire. You owe them 2 Debts.


### - The Spectre

**Debts** :

- Someone, or someone’s progenitor, was involved in your death. They owe you a Debt.

- Someone is watching out for a family member of yours. You owe them 2 Debts.

- You are haunting someone and they know it. You owe them a Debt.


### - The Vamp

**Debts** :

- Someone makes sure you get fed regularly. You owe them 2 Debts.

- Someone relies on you for their fix. They owe you a Debt.

- Someone bears responsibility for you becoming a vampire. They owe you a Debt.


### - The Wolf

**Debts** :

- Someone is hiding you from someone, or something, powerful. You owe them a Debt.

- Someone hired you for a job and you fucked it up. You owe them 2 Debts.

- Someone lives in your territory, benefiting from your protection. They owe you a Debt.


### - The Oracle

**Debts** :

- Someone helps you understand your dreams and visions. You owe them 2 Debts.

- You’ve had a dire prophecy about someone, but you don’t know how to help them...yet. You owe them a Debt.

- You are helping someone realize their true potential through your visions. They owe you a Debt.


### - The Wizard

**Debts** : 

- Someone is helping you keep your demons at bay. You owe them a Debt.

- Someone is your go-to when you get into trouble. You owe them 2 Debts.

- You are helping someone keep a dangerous secret. They owe you a Debt.


### - The Fae

**Debts** :

- Someone broke an important promise to you and swore they would make it up to you. They owe you 2 Debts.

- You are keeping something hidden for someone. They owe you a Debt.

- You entrusted someone with a dangerous task. Ask them if they succeeded or failed. If they succeeded, you owe them a Debt. If they failed, they owe you 2 Debts.


### - The Tainted

**Debts** :

- You’re protecting someone from a dark power. They owe you 2 Debts.

- Someone is trying to save you and keeps suffering for it. You owe them 2 Debts.

- You have a demon patron who holds the contract for your soul. You owe them 3 Debts.
