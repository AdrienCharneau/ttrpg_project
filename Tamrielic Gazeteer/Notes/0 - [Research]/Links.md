# REMEMBER




- Researched terms in UESP : *"Bitter Coast"*, *"Seyda Neen"*




# GENERAL-OTHER




>https://en.uesp.net/wiki/Morrowind:Demographics

>https://en.uesp.net/wiki/Lore:Books_by_Subject

>https://en.uesp.net/wiki/Lore:Dark_Elvish

>https://en.uesp.net/wiki/Lore:Temple_Saints

>https://en.uesp.net/wiki/Lore:Deadlight (cool ESO idea)

>https://en.uesp.net/wiki/Lore:Havocrel (cool ESO idea)

>https://en.uesp.net/wiki/Morrowind:Ancestral_Tombs

>https://en.uesp.net/wiki/Morrowind:Generic_Dialogue

>https://en.uesp.net/wiki/Morrowind:Ingredients

>https://en.uesp.net/wiki/Morrowind:Merchants

>https://en.uesp.net/wiki/Morrowind:Wilderness_People




## Books

>https://en.uesp.net/wiki/Lore:A_Feast_Among_the_Dead,_Chapter_I

>https://en.uesp.net/wiki/Lore:Before_the_Ages_of_Man

***








# HISTORY-REAL LIFE LORE




# Castle Building


>https://en.wikipedia.org/wiki/Castle#Construction

>https://history.howstuffworks.com/historical-figures/castle5.htm

>https://www.quora.com/In-the-medieval-times-how-long-did-it-take-to-build-a-castle

>https://en.wikipedia.org/wiki/Gu%C3%A9delon_Castle

***








# THE BITTER COAST




>https://en.uesp.net/wiki/Lore:Balmora ( https://en.uesp.net/wiki/Morrowind:Balmora_Homes )

>https://en.uesp.net/wiki/Lore:Slavery ( https://en.uesp.net/wiki/Morrowind:Slaves )

>https://en.uesp.net/wiki/Morrowind:Generic_Dialogue_H

>https://en.uesp.net/wiki/Morrowind:Mudcrab_Pests

>https://en.uesp.net/wiki/Morrowind:Odai_Plateau ( https://en.uesp.net/wiki/Morrowind:Stronghold_(Hlaalu) )




## Flora

>https://en.uesp.net/wiki/Lore:Flora_A#Ashen_Fern

>https://en.uesp.net/wiki/Lore:Flora_B#Bat_Bloom

>https://en.uesp.net/wiki/Morrowind:Corkbulb_Root

>https://en.uesp.net/wiki/Lore:Flora_I#Ink-Cap_Mushroom

>https://en.uesp.net/wiki/Lore:Flora_L#Lavaburst_Mushroom

>https://en.uesp.net/wiki/Lore:Flora_L#Luminous_Russula ( https://en.uesp.net/wiki/Morrowind:Luminous_Russula )

>https://en.uesp.net/wiki/Lore:Flora_M#Marshmerrow ( https://en.uesp.net/wiki/Morrowind:Marshmerrow - https://en.uesp.net/wiki/Skyrim:Marshmerrow - https://en.uesp.net/wiki/Lore:Alchemy_M#Marshmerrow )

>https://en.uesp.net/wiki/Lore:Flora_P#Pitcher_Plant

>https://en.uesp.net/wiki/Lore:Flora_P#Prune_Morel_Mushroom

>https://en.uesp.net/wiki/Lore:Flora_S#Slough_Fern ( https://en.uesp.net/wiki/Morrowind:Spore_Pod )

>https://en.uesp.net/wiki/Lore:Flora_T#Trama ( https://en.uesp.net/wiki/Morrowind:Trama_Root - https://en.uesp.net/wiki/Skyrim:Trama_Root )

>https://en.uesp.net/wiki/Lore:Flora_V#Violet_Coprinus ( https://en.uesp.net/wiki/Morrowind:Violet_Coprinus )

>https://en.uesp.net/wiki/Morrowind:Ash_Yam

>https://en.uesp.net/wiki/Morrowind:Chokeweed

>https://en.uesp.net/wiki/Morrowind:Hackle-Lo_Leaf

>https://en.uesp.net/wiki/Morrowind:Heather

>https://en.uesp.net/wiki/Morrowind:Stoneflower_Petals

>https://en.uesp.net/wiki/Morrowind:Wickwheat




## Fauna


>https://en.uesp.net/wiki/Lore:Bestiary_S#Shroom_Beetle

>https://en.uesp.net/wiki/Lore:Cliff_Racer

>https://en.uesp.net/wiki/Lore:Cliff_Strider

>https://en.uesp.net/wiki/Lore:Mudcrab - https://en.uesp.net/wiki/Morrowind:Beasts#Mudcrab - https://en.uesp.net/wiki/Skyrim:Mudcrab

>https://en.uesp.net/wiki/Lore:Netch

>https://en.uesp.net/wiki/Morrowind:Netch_Leather

>https://en.uesp.net/wiki/Lore:Nix-Hound - https://en.uesp.net/wiki/Lore:Bestiary_N#Nix-Hound - https://en.uesp.net/wiki/Morrowind:Beasts#Nix-Hound

>https://en.uesp.net/wiki/Lore:Elder_Scrolls_Online_-_Crown_Store_Showcase/Pets#Bitter_Coast_Cliff_Strider

>https://en.uesp.net/wiki/Lore:Silt_Strider - https://en.uesp.net/wiki/Morrowind:Silt_Strider




## Society


### - Factions

>https://en.uesp.net/wiki/Lore:Ahemmusa_Tribe

>https://en.uesp.net/wiki/Lore:Heran - https://en.uesp.net/wiki/Lore:Factions_H#Heran_Clan

>https://en.uesp.net/wiki/Lore:House_Dagoth

>https://en.uesp.net/wiki/Lore:House_Hlaalu

>https://en.uesp.net/wiki/Lore:Nycotic_Cult


### - Characters

>https://en.uesp.net/wiki/Morrowind:Nine-Toes


### - Food

>https://en.uesp.net/wiki/Lore:Alchemy_B#Bitter_Tea

>https://en.uesp.net/wiki/Lore:Alchemy_S#Scrib_Jerky - https://en.uesp.net/wiki/Morrowind:Scrib_Jerky - https://en.uesp.net/wiki/Skyrim:Scrib_Jerky

>https://en.uesp.net/wiki/Lore:Alcoholic_Beverages#Greef - https://en.uesp.net/wiki/Morrowind:Potions#Greef - https://en.uesp.net/wiki/Online:Greef

>https://en.uesp.net/wiki/Lore:Alcoholic_Beverages#Shein - https://en.uesp.net/wiki/Morrowind:Potions#Shein - https://en.uesp.net/wiki/Skyrim:Food#Shein

>https://en.uesp.net/wiki/Lore:Alcoholic_Beverages#Sujamma - https://en.uesp.net/wiki/Morrowind:Potions#Sujamma - https://en.uesp.net/wiki/Skyrim:Food#Sujamma - https://en.uesp.net/wiki/Online:Sujamma

>https://en.uesp.net/wiki/Lore:Dunmer_Cuisine

>https://en.uesp.net/wiki/Lore:Moon_Sugar - https://en.uesp.net/wiki/Morrowind:Moon_Sugar - https://en.uesp.net/wiki/Skyrim:Moon_Sugar

>https://en.uesp.net/wiki/Lore:Skooma - https://en.uesp.net/wiki/Morrowind:Skooma - https://en.uesp.net/wiki/Oblivion:Skooma - https://en.uesp.net/wiki/Skyrim:Skooma


### - Books

>https://en.uesp.net/wiki/Lore:The_Alchemist%27s_Formulary

>https://en.uesp.net/wiki/Lore:Ashlander_Tribes_and_Customs

>https://en.uesp.net/wiki/Lore:Guide_to_Vvardenfell#The_Bitter_Coast

>https://en.uesp.net/wiki/Lore:Kwama_Breeding_Research_Notes_(Flora)

>https://en.uesp.net/wiki/Lore:Naryu%27s_Journal/Vvardenfell

>https://en.uesp.net/wiki/Lore:Naryu%27s_Journal/Vvardenfell,_Before

>https://en.uesp.net/wiki/Lore:The_Nycotic_Cult

>https://en.uesp.net/wiki/Lore:Vvardenfell_Flora_and_Fauna

>https://en.uesp.net/wiki/Morrowind:Ajira%27s_Mushroom_Report ( https://en.uesp.net/wiki/Morrowind:Four_Types_of_Mushrooms )

>https://en.uesp.net/wiki/Morrowind:Aurane_Frernis%27_Recipies

>https://en.uesp.net/wiki/Tamriel_Rebuilt:Alchemical_Formulae

>https://en.uesp.net/wiki/Tamriel_Data:An_Alchemist%27s_Guide_to_Skooma

>https://en.uesp.net/wiki/Tamriel_Data:Defense_of_Morrowind_Volume_I

>https://en.uesp.net/wiki/Tamriel_Data:The_Purpose_of_the_Corkbulb

>https://en.uesp.net/wiki/Tamriel_Data:Spawn_of_Ash

>https://en.uesp.net/wiki/Tamriel_Rebuilt:Ulvo%27s_Diary

***








# SEYDA NEEN AREA




>https://en.uesp.net/wiki/Morrowind_Mod:AFFresh

>https://en.uesp.net/wiki/Lore:Census_and_Excise

>https://en.uesp.net/wiki/Lore:Dark_Elvish#S

>https://en.uesp.net/wiki/Morrowind:Generic_Dialogue_S

>https://en.uesp.net/wiki/Lore:Gold_Coast_Trading_Company

>https://en.uesp.net/wiki/Lore:Seyda_Neen ( https://en.uesp.net/wiki/Morrowind:Seyda_Neen )

>https://en.uesp.net/wiki/Lore:Places_S#Seyda_Neen

>https://en.uesp.net/wiki/Lore:West_Gash

>https://en.uesp.net/wiki/Morrowind:Crime

>https://en.uesp.net/wiki/Morrowind:Imperial_Prison_Ship

>https://en.uesp.net/wiki/Morrowind:Lighthouse

>https://en.uesp.net/wiki/Morrowind:Remote_Shipwreck

>https://en.uesp.net/wiki/Morrowind:Seyda_Neen_Homes

>https://en.uesp.net/wiki/Morrowind:Unexplored_Shipwreck




## Dungeons


>https://en.uesp.net/wiki/Morrowind:Abaesen-Pulu_Egg_Mine

>https://en.uesp.net/wiki/Morrowind:Addamasartus

>https://en.uesp.net/wiki/Morrowind:Aharunartus

>https://en.uesp.net/wiki/Morrowind:Akimaes_Grotto

>https://en.uesp.net/wiki/Morrowind:Assemanu ( https://en.uesp.net/wiki/Morrowind:Control_the_Ordinators - https://en.uesp.net/wiki/Morrowind:Tholer_Saryoni )

>https://en.uesp.net/wiki/Morrowind:Nimawia_Grotto

>https://en.uesp.net/wiki/Morrowind:Samarys_Ancestral_Tomb

>https://en.uesp.net/wiki/Morrowind:Sarys_Ancestral_Tomb

>https://en.uesp.net/wiki/Morrowind:Thelas_Ancestral_Tomb

>https://en.uesp.net/wiki/Morrowind:Zainsipilu




## Society


### - Factions

>https://en.uesp.net/wiki/Morrowind:Census_and_Excise

>https://en.uesp.net/wiki/Tamriel_Rebuilt:Census_and_Excise_Office


### - Characters

>https://en.uesp.net/wiki/Morrowind:Adraria_Vandacia ( https://en.uesp.net/wiki/Morrowind:Vandacia%27s_Bounty )

>https://en.uesp.net/wiki/Morrowind:Arrille ( https://en.uesp.net/wiki/Morrowind:Arrille%27s_Tradehouse )

>https://en.uesp.net/wiki/Morrowind:Darvame_Hleran

>https://en.uesp.net/wiki/Morrowind:Draren_Thiralas ( https://en.uesp.net/wiki/Morrowind:Draren_Thiralas%27_House - https://en.uesp.net/wiki/Morrowind:Thiralas_Ancestral_Tomb )

>https://en.uesp.net/wiki/Morrowind:Eldafire ( https://en.uesp.net/wiki/Morrowind:Eldafire%27s_House - https://en.uesp.net/wiki/Morrowind:Notes_on_Racial_Phylogeny )

>https://en.uesp.net/wiki/Morrowind:Elone

>https://en.uesp.net/wiki/Morrowind:Erene_Llenim ( https://en.uesp.net/wiki/Morrowind:Erene_Llenim%27s_Shack )

>https://en.uesp.net/wiki/Morrowind:Fargoth ( https://en.uesp.net/wiki/Morrowind:Fargoth%27s_Ring - https://en.uesp.net/wiki/Morrowind:Fargoth%27s_Hiding_Place - https://en.uesp.net/wiki/Morrowind:Fargoth%27s_House - https://en.uesp.net/wiki/Morrowind:I%27m_My_Own_Grandpa - https://en.uesp.net/wiki/Morrowind:Gaeldol )

>https://en.uesp.net/wiki/Morrowind:Fine-Mouth ( https://en.uesp.net/wiki/Morrowind:Fine-Mouth%27s_Shack )

>https://en.uesp.net/wiki/Morrowind:Foryn_Gilnith ( https://en.uesp.net/wiki/Morrowind:Foryn_Gilnith%27s_Shack )

>https://en.uesp.net/wiki/Morrowind:Ganciele_Douar

>https://en.uesp.net/wiki/Morrowind:Hrisskar_Flat-Foot

>https://en.uesp.net/wiki/Morrowind:Indrele_Rathryon ( https://en.uesp.net/wiki/Morrowind:Land_Deed - https://en.uesp.net/wiki/Morrowind:Indrele_Rathryon%27s_Shack - https://en.uesp.net/wiki/Morrowind:Gentleman_Jim_Stacey )

>https://en.uesp.net/wiki/Morrowind:People_in_Seyda_Neen

>https://en.uesp.net/wiki/Morrowind:Processus_Vitellius ( https://en.uesp.net/wiki/Morrowind:Death_of_a_Taxman - part of the Imperial Cult)

>https://en.uesp.net/wiki/Morrowind:Raflod_the_Braggart

>https://en.uesp.net/wiki/Morrowind:Sellus_Gravius

>https://en.uesp.net/wiki/Morrowind:Seyda_Neen_People

>https://en.uesp.net/wiki/Morrowind:Silm-Dar

>https://en.uesp.net/wiki/Morrowind:Socucius_Ergalla

>https://en.uesp.net/wiki/Morrowind:Tandram_Andalen

>https://en.uesp.net/wiki/Morrowind:Tarhiel ( https://en.uesp.net/wiki/Tamriel_Rebuilt:The_Sorceror%27s_Apprentices )

>https://en.uesp.net/wiki/Morrowind:Teruise_Girvayne ( https://en.uesp.net/wiki/Morrowind:Teruise_Girvayne%27s_House )

>https://en.uesp.net/wiki/Morrowind:Tolvise_Othralen

>https://en.uesp.net/wiki/Morrowind:Vodunius_Nuccius_(person) ( https://en.uesp.net/wiki/Morrowind:Vodunius_Nuccius_(quest) - https://en.uesp.net/wiki/Tamriel_Rebuilt:Vodunius_Nuccius_(person) )


### - Items

>https://en.uesp.net/wiki/Lore:Mentor%27s_Ring ( https://en.uesp.net/wiki/Morrowind:Mentor%27s_Ring - https://en.uesp.net/wiki/Morrowind:Albecius_Colollius )


### - Books

>https://en.uesp.net/wiki/Lore:Hlaalu_Construction_Syndic

>https://en.uesp.net/wiki/Lore:Ruins_of_Kemel-Ze

>https://en.uesp.net/wiki/Morrowind:Deed_to_Indrele%27s_House

>https://en.uesp.net/wiki/Morrowind:Directions_to_Caius_Cosades

>https://en.uesp.net/wiki/Morrowind:Elone%27s_Directions_to_Balmora

>https://en.uesp.net/wiki/Morrowind:Journal_of_Tarhiel

>https://en.uesp.net/wiki/Morrowind:Note_to_Hrisskar

>https://en.uesp.net/wiki/Morrowind:Tradehouse_notice (mentions "port of Seyda Neen")

>https://en.uesp.net/wiki/Morrowind:Tax_Record

>https://en.uesp.net/wiki/Morrowind:Warehouse_shipping_log

>https://en.uesp.net/wiki/Tamriel_Data:An_Adventure%27s_End

>https://en.uesp.net/wiki/Tamriel_Data:Glass_in_Morrowind

>https://en.uesp.net/wiki/Tamriel_Data:House_Hlaalu_Stronghold_Plans

>https://en.uesp.net/wiki/Tamriel_Rebuilt:Nadael_Illulivad

>https://en.uesp.net/wiki/Tamriel_Rebuilt:Nadael_Illulivad,_Vol_II

>https://en.uesp.net/wiki/Tamriel_Rebuilt:Unfinished_Letter_(Helnim)

***








# HLA OAD AREA




>https://en.uesp.net/wiki/Lore:Hla_Oad ( https://en.uesp.net/wiki/Lore:Places_H#Hla_Oad )

>https://en.uesp.net/wiki/Morrowind:Grytewake




## Dungeons


>https://en.uesp.net/wiki/Morrowind:Andrethi_Ancestral_Tomb

>https://en.uesp.net/wiki/Lore:Ashalmimilkala ( https://en.uesp.net/wiki/Morrowind:Ashalmimilkala - https://en.uesp.net/wiki/Morrowind:Ultimatum_for_Carecalmo - https://en.uesp.net/wiki/Morrowind:The_Scroll_of_Fiercely_Roasting )

>https://en.uesp.net/wiki/Morrowind:Ashurnibibi

>https://en.uesp.net/wiki/Morrowind:Heran_Ancestral_Tomb

>https://en.uesp.net/wiki/Morrowind:Hlormaren ( https://en.uesp.net/wiki/Morrowind:Writ_for_Brilnosu_Llarys )

>https://en.uesp.net/wiki/Morrowind:Shal ( https://en.uesp.net/wiki/Morrowind:Kill_Necromancer_Telura_Ulver )

>https://en.uesp.net/wiki/Morrowind:Shrine_of_Boethiah ( https://en.uesp.net/wiki/Morrowind:Boethiah%27s_Quest )

>https://en.uesp.net/wiki/Morrowind:Yasamsi

>https://en.uesp.net/wiki/Morrowind:Zanabi




## Society


### - Characters

>https://en.uesp.net/wiki/Morrowind:Fjol ( https://en.uesp.net/wiki/Morrowind:Fjol_the_Outlaw )

>https://en.uesp.net/wiki/Morrowind:Telura_Ulver ( https://en.uesp.net/wiki/Morrowind:Kill_Necromancer_Telura_Ulver )

>https://en.uesp.net/wiki/Morrowind:Velfred_the_Outlaw_(person) ( https://en.uesp.net/wiki/Morrowind:Velfred_the_Outlaw_(quest) )

***








# GNAAR MOK AREA




>https://en.uesp.net/wiki/Morrowind:Abandoned_Houses#An_Abandoned_Shack

>https://en.uesp.net/wiki/Lore:Gnaar_Mok ( https://en.uesp.net/wiki/Lore:Places_G#Gnaar_Mok )

>https://en.uesp.net/wiki/Morrowind:Druegh-jigger%27s_Rest

>https://en.uesp.net/wiki/Morrowind:Neglected_Shipwreck

>https://en.uesp.net/wiki/Morrowind:Shunned_Shipwreck




## Dungeons


>https://en.uesp.net/wiki/Lore:Abernanit ( https://en.uesp.net/wiki/Morrowind:Abernanit - https://en.uesp.net/wiki/Lore:Death_Blow_of_Abernanit - https://en.uesp.net/wiki/Morrowind:Rescue_Dandsa )

>https://en.uesp.net/wiki/Morrowind:Addadshashanammu

>https://en.uesp.net/wiki/Morrowind:Aleft

>https://en.uesp.net/wiki/Morrowind:Band_Egg_Mine

>https://en.uesp.net/wiki/Morrowind:Ilunibi

>https://en.uesp.net/wiki/Morrowind:Mallapi

>https://en.uesp.net/wiki/Morrowind:Sennananit

>https://en.uesp.net/wiki/Morrowind:Shurinbaal_(place)




## Society

### - Characters

>https://en.uesp.net/wiki/Morrowind:Anas_Ulven

>https://en.uesp.net/wiki/Morrowind:Balan

>https://en.uesp.net/wiki/Morrowind:Farvyn_Oreyn

>https://en.uesp.net/wiki/Morrowind:Pelena_Acicius

>https://en.uesp.net/wiki/Morrowind:Sjorvar_Horse-Mouth ( https://en.uesp.net/wiki/Morrowind:Sjorvar_Horse-Mouth%27s_House )

***








# FIREMOTH




## Society

### - Characters

>https://en.uesp.net/wiki/Morrowind:Aronil

>https://en.uesp.net/wiki/Morrowind:Hjrondir

>https://en.uesp.net/wiki/Morrowind:J%27Hanir

>https://en.uesp.net/wiki/Morrowind:Mara