```
- Tools / Character Sheets / Cheat Sheets
- General / Introduction
- Checks / Dice Mechanics / Abilities
- Targeted Checks / Versus Checks / Move Lists
- Health & Stress
- Breakdowns / Traumas / Fallouts
- Paths / Arcs / XP / Character Progression
- Ties / Intimacies / Resonances
- Bonding / Character Backstory / Disposition / Relationships
- Paradigms / Beliefs / Religion
- Skills & Languages
- NPCs & Creatures
- Other / Random
```

***








# TOOLS / CHARACTER SHEETS / CHEAT SHEETS




## Character Sheet


- Added 4 new *drives* in `Summary.md`

- Remove a few *affinities* in order to increase the vertical size of each *affinity*'s name box (they're too small rn).

- Have *drives/weaknesses* + *advantages* be on the left side of the character sheet, and *affinities* + *inventory* be on the right side, as their contents might be more prone to change during a game session (have an alternative version made for left-handed people?).




## GM Cheat Sheet


- Date counter (helps keeping track of time)
- List of *abalities* with one or two sentences describing what they should be used for
- List of *drives* and *weaknesses* with one or two sentences describing in what context they should be used
- *Daring performance* outcomes table

>**Legacy**: Difficulty ratings table

***








# GENERAL / INTRODUCTION




- Why am I writting this? => it's my own spiritual cope.

When reaching the age of 23 in July (I was born in 1989), I came to somewhat of a realization. According to what I thought at the time, people that had firm beliefs, or tended to adhere to well established principles likes the ones found in religions, tended to be more "practical" and "pragmatic" than people that didn't. I had this idea that people with strong religious upbringing didn't have to spend too much time thinking about phisolophy or morals as their tenets had them all already figured out for them. Hence why they could "get on with their lives and move on" much more easily than people that didn't grow up in a religious environment. As someone who grew up in an atheist and liberal environment, this epiphany came with a shock.

Later on, I obviously realized that I was wrong, and that other socio-economic factors (things like "income", "standards of living" or just different lived experience in general) had much more to do with it than just me not experiencing a "religious upbringing". To make it short, I probably had too much free time on my hands.

This project is basically my own "spiritual cope", my own "personal doctrine". It's me trying to come up with my own rules and foundations for my thoughts, just so I can "get on with my life and move on" without having to resort to an already existing philosophy or religion. Plus I liked the idea of doing this through game design instead of writting a book or making a movie.

`Numenera - Corebook.pdf` p.5 (*Dreaming of the Future*)

Role-playing games can be considered modern interactive mythology in a way. Like traditional mythology, RPGs often feature fantastical elements such as magic, mythical creatures, and otherworldly settings. They also often involve heroes embarking on epic quests and facing challenges that test their skills and character. However, unlike traditional mythology, which is passed down orally or through written stories, RPGs allow players to actively participate in and shape the story through their own choices and actions. In this way, RPGs can be seen as a modern, interactive form of mythology.




## Gameplay


## - Urban Shadows

>more stuff I could take inspiration from p.17

When we say in The Preface that a roleplaying game is a conversation, we’re saying that it’s easy. You sit down at a table with sheets and dice and pencils and start talking. For the most part, the conversation revolves around your characters. You say what they do, how they react, what they care about. And since the other players don’t live in your head, you broadcast that information to the other players and the MC.

There are times in the conversation when you slip directly into your character’s shoes, no longer narrating but actually taking on your character’s persona. You make the gestures your character makes; you say things as your character without preface. Most people do this without thinking too much about it. It’s pretty natural to damn near become your character while you’re playing, to want what they want, to fear the outcome of the dice at crucial moments.

The conversation in a roleplaying game evolves naturally over the course of the session. But it’s your job—as a group—to keep the conversation interesting. The rules, the techniques, and everything you bring to the table is about driving that conversation toward interesting places. Dark alleys. Dangerous places. Murderous moments. Wherever the fiction takes you.


### - The One Ring

The gameplay is a dynamic narrative, as the players take action to explore the situations they encounter. The Loremaster then describes what happens as a result of their actions, and the players again react to the new circumstances, and so on. The Loremaster keeps the story flowing through a mixture of preparation and improvisation.

If the session is the first one of an adventure, the Loremaster presents the current situation to the players. This usually sets a date or time of the year (when), a location (where), and defines an introductory situation (what) that includes information that allows for the involvement of the Company (why). If it’s not the first session of play, then the opening scene picks up from where the adventure broke off at the end of the previous session.

Once a session has started, the gameplay transitions from one scene to another, with each scene describing a situation requiring the players to make meaningful decisions.

When the time allocated for the game session is up, the Loremaster and the players face two possible situations:

- If the session ends without the players reaching a satisfactory conclusion, the gameplay will resume at a later date with another session. When the playing group meets again, the Loremaster starts by summarising what happened in the previous one.

- If, on the contrary, the current narrative arc has reached its conclusion, the Loremaster draws the final curtain on the gaming session. The adventure is over.


### - Old Draft 1

A game session is divided into situations that follow one another, like theater scenes through which the [Players] will seek to interpret a role (their [Character]).

Each situation is composed of a location, a circumstance, and actors. When a new [Situation] begins, each [Actor] present in it must choose a [Resolution]. The [Resolution] corresponds to the outcome, the way in which the [Actor] would implicitly or explicitly want the [Situation] to end. This can lead to conflicts among [Actors] who would desire different [Resolutions] for the same [Situation].

Each situation is then divided into a succession of [Turns], during which each [Character] will be able to undertake an [Action]. This applies to all situations that occur during a game session, ranging from combat to simple dialogue. The only time when it is possible to escape this rule is when the [GM] decides to use a temporal skip to progress more quickly in the narration and skip certain boring or repetitive parts. A [Turn] is divided into two parts: the [Decision Phase] and the [Execution Phase]. During the [Decision Phase], each [Player] must choose an [Action] to undertake for their [Character] and discuss their strategy with the [GM]. They will need to determine a [Skill] to use, an [Intention], and an amount of [Resilience Points] to consume for their [Action].


### - Old Draft 2

Le jeu se joue de 3 à + joueurs. L'un des joueurs incarne le Maître du Jeu (MJ), les joueurs incarnent quand à eux des personnages plongés dans le contexte du scénario. Les raisons pour lesquelles ils se sont retrouvés là ou les différentes motivations qui les inspirent pour cette aventure dépendent du choix des joueurs. Les circonstances qui les réunit dans ce même lieu au même moment est expliquée par le MJ, et doit s'harmoniser en fonction du passé et des paramêtres choisis.

Comme dans tout jeu de rôle, chaque joueur possède sa propre fiche perso qui lui indique tous ses paramêtres. Le Joueur n'a le droit de montrer cette fiche qu'au Maître du Jeu, qui est aussi le seul à pouvoir y apporter des modifications. Chaque joueur doit absolument (sauf sous exception) garder secret toutes les informations qui y sont inscrites et ne jamais les dévoiler  aux autres. Chaque joueur possède aussi deux dés à 6 faces. Un dé d'Action et un dé d'Initiative. Deck de cartes = fatigue/énergie. La quantité de cartes dépend de la charactéristique force du personnage.

Dans (Nom du Jeu), les PJs incarnés par les Joueurs évoluent dans une galaxie imaginaire instable et hostile dans laquelle ils cherchent désespérément à survivre et/ou se construire une réputation. Leur but principal est de réussir les objectifs qu’il se sont fixé dans la vie par le biais de leur vocation spirituelle afin de maintenir leur bien-être ou Assurance au maximum.

[Système] est semblable à de nombreux autres jeux de rôle dans la mesure où les joueurs qui participent à une partie incarnent des individus imaginaires (que l'on appelle [Personnages Joueurs] alias [PJs]) chacun doté de forces ou de faiblesses, d'envies et de besoins, de récits de vie et de profils psychologiques qui leurs sont propres. Ils s'engagent à tour de rôle dans le déroulement d'un scénario ou une campagne préparée au préalable par un Maître du Jeu (alias MJ) et doivent juger de l'aboutissement de leurs actions à travers des scores déterminés par des résultats de dés.




## Metagaming


Dans la plupart des jeux de rôles, lorsque les PJs se retrouvent face à une situation qui requiert un certain degré d'effort à résoudre telle qu'un combat, une énigme ou tout autre tache nécessitant un tant soit peu de coordination entre les membres du groupe, ces derniers ont souvent tendance, avant d'entreprendre les actions et d'effectuer les jets de dés, à discuter de la stratégie optimale à entreprendre pour maximiser au mieux leurs chances de réussites. Quelles actions vont-ils entreprendre? A quelle moment et dans quel but? Certains joueurs vont même jusqu'à rappeller à d'autres qu'ils possèdent certaines capacités ou objets qu'il serait bon d'utiliser à ce moment précis, d'autres vont insister sur le fait qu'ils préfèrerainent utiliser telle ou telle technique et ainsi de suite.

Dans [ROD] les joueurs ont l'interdiction totale de communiquer à propos de quoi que se soit qui concernerait de près ou de loin à une stratégie ou tout autre chose qui se rapporterait au jeu. Bien sûr, les joueurs n'ont pas l'interdiction de parler entre eux, rien ne les empêche de faire des blagues, de discuter du match de foot de la veille ou tout autre chose n'ayant aucun rapport avec la partie de [ROD] en cours mais il leur est strictement interdit d'évoquer le moindre détail qui puisse fausser le pacte de "Conscience individuelle".

Au cours d'une partie, il est essentiel pour tous les joueurs autres que le MJ de respecter le "Pacte de mutisme élémentaire collectif". Ainsi, à la différence d'autres jeux de rôle papier, les joueurs de [ROD] n'incarnent pas directement leurs personnages mais plutôt leurs "Consciences".

***








# CHECKS / DICE MECHANICS / ABILITIES




- In the heat of an action and excitement, there should be a mechanic that makes characters forget about their "less used" means, skills or strategies?




## Design Guidelines


- Each action should combine two abilities.
- Skills shouldn't interfere with the dice mechanics. They're just here to tell players if they can perform specific actions or not.
- Actions should support multiple outcomes ("Perfect success", "failure with minor complication", "success with major setback"...)
- There should be a "push your luck" mechanic. Players can choose to roll more dice for a better chance of success, but this increases the risk of having a greater "complications" (if failure) or "setbacks" (if success).
- Ability scores should not determine how many dice a player is allowed to throw.




## New Test Rule (7?)


 - I can now say there's only two types of ability checks: *passive* and *active checks* (*targeted checks* are now just *active checks* with an added *ward* / *support* modifier).


### - Modifiers

>https://forum.rpg.net/index.php?threads/how-many-modifiers-is-too-many.746705/
>https://www.reddit.com/r/RPGdesign/comments/kd7b13/how_many_modifiers_are_too_many_and_what_are/




## Trivial Difficulty


### - Numenera

When you jump from a burning vehicle, swing a battleaxe at a mutant beast, swim across a raging river, identify a strange device, convince a merchant to give you a lower price, craft an object, use a power to control a foe’s mind, or use a heat-beam emitter to carve a hole in a wall, you make a d20 roll.

However, if you attempt something that has a difficulty of 0, no roll is needed — you automatically succeed. Many actions have a difficulty of 0. Examples include walking across the room and opening a door, using a special ability to negate gravity so you can fly, using an ability to protect your friend from radiation, or activating a device (that you already understand) to erect a force field. These are all routine actions and don’t require rolls.

Using skill, assets, and Effort, you can decrease the difficulty of potentially any task to 0 and thus negate the need for a roll. Walking across a narrow wooden beam is tricky for most people, but for an experienced gymnast, it’s routine.




## Anticipation


Une [Anticipation] remplace l'[Action] qu'un [Personnage] pourrait entreprendre et permet d'augmenter, jusqu'à la fin d'un [Roulement], la valeur de ses [SDs de Sauvegarde]. Cela dans le but d'empêcher tout agresseur de l'atteindre, un peut comme si le [Personnage] essayait de prévoir l'avenir et anticiperait les [Actions] des autres pour mieux pouvoir se défendre. Cependant, déclarer une [Anticipation] doit obligatoirement être fait pendant la [Phase de Décision]. Avant que [Personnage instigateur] ne déclare son [Action] et jamais après (ou pendant). De la même manière qu'une [Action], le [Personnage] doit choisir une quantité de [Points de Résilience] à consommer pour une [Anticipation]. Plus le nombre de points de [Résilience] consommés sera grand et plus le bonus attribué à son [SD de Sauvegarde] le sera aussi.




## NPCs


If an *active* or *targeted check* is being performed by an NPC, the GM doesn't perform *initial* or *daring* rolls. Instead, they pick 4 die: 2 for each ability. In order for the NPC to succeed, the added results of non-*stress die* should equate or overcome the difficulty score for the *active* or *targeted check*. The GM can also narrate an additional outcome depending on the number of *stress die* obtained if they want to.

>This rule should mostly be applied for important or at least well fleshed-out NPCs with active stats. Specifically during combat scenarios. Other than that, just have players perform simple *passive ability checks* instead.




## Megatraveller

If I don't want the player to know for sure whether they succeeded, then I use the "uncertain" modifier from MegaTraveller.

>Uncertain: If the result of a task attempt is largely "opinion" or if, because of the nature of the task, immediate feedback on how successful the task has been is not possible, then declare the task to be uncertain. With an uncertain task, those associated with the task have some idea of how successful the task attempt was, but they are not certain of the outcomeSensor readings, Interchanges between characters (including any task which might require a reaction roll), psionics, computer programming, repairs, and research are all good candidates for uncertain tasks.Both the player and referee roll for the attempt. The referee's roll is hidden from the player and modifies the player's roll.* If both fall, the result is no truth. The player is misled about the success of the task attempt. Erroneous information is given.* If one succeeds and one falls, the result is some truth. Some valid information is given. The player may fail the attempt and still get information, although he cannot know for sure.* If both succeed, the result is total truth. Totally valid information is given, although the player may stll not believe it.A character may know whether he has succeeded. If the player achieves exceptional success, the referee may elect to tell the player the result of the hidden roll The referee must decide if this is warranted, however, sometimes it is not.

Often I will modify the above rule and instead have the player roll one die while I secretly roll the other. That way I don't have to come up with a "some truth" response but they still can't be sure whether they succeeded (unless their one die plus mods already put them over the success threshold.)

>https://www.reddit.com/r/traveller/comments/11d71mm/do_you_tell_your_players_about_the_difficulty_of/

***








# TARGETED CHECKS / VERSUS CHECKS / MOVE LISTS




- *Focus* : Players should be allowed to add (1?) *routine stress* to their character (as long as it doesn't trigger a *breakdown*) in order to temporarily increase one (all?) of their *ward ratings*.

- Multiple instigators executing "hostile" checks against a single target should reduce the target's difficulty score

- If a player uses a NPC as *target*, the GM can choose not to bother checking the NPC's *ward/support modifiers* if they don't want to. They can just come up with a modifier* based on the circumstances of the action being described by the player.

- When doing a "scream for help vs dexterity assist" check, if there's an echo or reverberation, it applies a reduction to the Active ST of the person calling for help

- When doing a "scream for help vs dexterity assist" check, this is just to see if the target can hear the character. Deciding whether to help them or not is the choice of the player -> implement a "culpability" rule so "empathetic" (humble?) characters can't act cold/sociopathically in this kind of situation?

- "Lying" requires "effort", therefore it usually asks a player to perform an *active eloquence* + *reasoning* vs. *insight check* on a target.

- On the other hand, "telling the truth" or "being straightforward" doesn't require any "effort", but still asks the player "listening" to the character "telling the truth" to perform a *passive insight* (or *humility*?) *check*.




## Ability scaling


When dealing with creatures or monsters whose stats are much higher than those of player characters, add positive or negative modifiers. For example, a bear could maybe have *strength* (+2), which means every time they use *strength* they should get a +2 modifier when dealing with "human" characters.




## Social Actions


- "Attacking" a character on the basis of their drives/intimacies is harder (higher difficulty threshold) but directly strikes the target's morale if successful (instead of just stress)


### - Lying

If a character seeks to find/create an excuse/lie/improvise a story, they must think beforehand. Lying/justifying oneself requires energy, reflection.

>Should intentions remain secret? One character can ask another what their intention was.

>Only the characters whom the character "respects" can subject them to a "moral justification test"?

>- For example: "Yes, you won, but don't you feel like you cheated?"

>- Another example: "You had told me you were going to do this for me. So, explain to me, why didn't you do it?"




## List


### - Hostile / Confrontational

- Harm
- Insult
- Intimidate
- Mock
- Punish
- Steal


### - Deceptive / Concealed

- Barter?
- Complain
- Conceal
- Deceive
- Distract
- Manipulate
- Lie
- Provoke
- Test


### - Support

- Admire/Praise/Boast
- Comfort
- Confess
- Defend
- Entertain
- Greet
- Help
- Listen
- Respect
- Socialize
- Sympathize


### - Distress

- Request/Solicit


### - Collaborate?

- Correct
- Educate
- Explain
- Inform
- Instruct
- Obey
- Train


### - Other / Unclear

- Ask
- Charm/Flirt/Seduce
- Command
- Deny
- Eat/Drink
- Ignore
- Interrupt
- Justify
- Read
- Reason
- Recite/Pray
- Rest
- Search
- Sing
- Wait




## Old Targeted Check "subtypes"


- Action Réelle(négative) / Affichée(positive)/ - Directe: tiens, voilà ce que tu voulais maintenant ferme là

- Action Réelle(négative) / Affichée(positive)/ - Sournoise: mais bien sûr que je vais t'aider (prépare un sale coup derrière)

- Action Réelle(positive) / Affichée(négative)/ - Directe: mais t'es trop moche espèce de génie!

- Action Réelle(positive) / Affichée(négative)/ - Sournoise: je t'ai tout volé l'autre soir (c'est un mensonge mais j'attends quand même de voir ta réaction pour te dire qu'en fait j'ai retrouvé tout ce que t'avais perdu)

- Action Altruo-négative : action négative bien vue par les gens qui nous soutiennent

- Action individualo-négative : action négative mal vue par les gens qui nous soutiennent

***








# HEALTH & STRESS




- Hostile checks are the most straightforward way to inflict stress onto characters. But a character can cause stress to another character by other means, like attacking things they are attached to without ever targeting them directly.

>Any consumption of energy (intellectual or physical) triggers a remembrance of pleasure-oriented objectives in a character's consciousness.

>https://en.wikipedia.org/wiki/Psychological_resilience




## Drafts


The *stress stack* reflects a character's mental and emotional strain, increasing as they face undesired situations or experience negative events. High levels of *stress* can impair their judgment and capacity to succeed *passive checks*.

>- *Ridicule, humiliate, making fun of someone, provocation* without using a *target*'s *memory/resonance* => adds *routine stress* + decreases the *target*'s relationship towards the *instigator*?

>- *Ridicule, humiliate, making fun of someone, provocation* and using a *target*'s *memory/resonance* => adds *tough stress* but **DOESN'T** decrease the *target*'s relationship towards the *instigator*?

>*Anger breakdowns* are only triggered when *routine stress* (**NOT** *tough stress*) reaches or surpasses the value of the character's current *health pool*?




## Unwinding


Take inspiration from *"the One Ring"* rulebook p.20

>Basically once a day (and **ONLY** once a day) a character can:

>- Eat up to two meals to recover 1 stress for each meal (2 stress if it's a feast or their favourite dish)

>- Engage in some fun activity (watch a play, have a beer, listen to a bard... - can recover up to 2 stress depending on how "fun" the activity is - a full-on Mozart concert removes more stress than just drinking a beer for example)

>- Take a single short rest/nap (1 hour) to recover 1 stress

>- Take a single long rest to recover 3 (2?) stress (but can only be done after sunset)

>Keep in mind that a character can only recover from *stress* if they *choose* to take a break. *Being forced* to wait for something or someone doesn't count as stress relief. On the contrary, a character that *doesn't choose* to wait (like being imprisoned in a cell or expecting a bus that is runing late) **adds stress** to the character instead of removing it.




## Reddit Post


>The way I see is that measuring only your bodily damage is not enough, as human mind and body intrinsically linked. One doesn't work without the other, and any damage on one end hinders the function of the other.

>It became quite apparent during my military service, as often, if not always, your mind would be the first one give up, even if your body could have still pushed on.

>A maybe more concrete example for those who have not served in military, is gym. Having your coach scream shit at you will make you push through those last couple of reps. Without him/her, you wouldn't have done it cause your mind would say 'enough' in order to protect your body from over exertion. Your mind literally gives up before your body does.

>Similarly, the mindset and state of mind really effect your performance. Some of the worst scores my squads received in combined arms exercises were when they had been watching movies until late night. Having your squads tired is sure way to get them perform poorly.




## Balladry


Characters all need rest to recover from harm and exhaustion. All rests require Three *Comforts* to receive the benefits of the rest.

If a rest is interrupted by a threat, all characters must succeed on a Vigor Check after they return to and finish their rest in order to receive the benefits of the Rest.


### - Partial Rests

Takes 30 Minutes, or 3 Turns

- Replenish one lost Mana
- Recover one spent Focus


### - Full Rests

Takes a full day of rest.

- Replenish all lost Mana
- Recover all spent Focus
- Recover one lost Health
- Recover 1 lost Attribute Point


### - Comforts

Comforts could be many different things based on the character and situation, but here are some common comforts.

- A meal
- A good drink
- Sleep
- A relaxing and safe place to rest
- Personal Rituals
- Decent bedding
- Some entertainment

Some environmental conditions may impose *“Negative Comforts”* that will need to be counteracted to get a rest. These could be extreme heat or cold, weather, or the unsettling feeling one gets from exploring a tomb.

**Exhaustion** : going too long without Food, Water, or Sleep will stack as Negative Comforts and can eventually kill the suffering character.


### - Medical Attention

When a poultice (medicine) is applied to an injured character, they will restore X additional Health from resting and allows HP recovery on Partial Rests.

***








# BREAKDOWNS / TRAUMAS / FALLOUTS




>People feel really angry when they sense that they or someone they care about has been offended, when they are certain about the nature and cause of the angering event, when they are convinced someone else is responsible, and when they feel they can still influence the situation or cope with it. For instance, if a person's car is damaged, they will feel angry if someone else did it (e.g. another driver rear-ended it), but will feel sadness instead if it was caused by situational forces (e.g. a hailstorm) or guilt and shame if they were personally responsible (e.g. they crashed into a wall out of momentary carelessness).

>(REMEMBER) simple rule for breakdowns: they can be trigered wether by the GM/circumstances of the scenario or when the stress stack of a character reaches its current health pool




## Dr K. quotes


>If the challenges that you face are too easy you'll get bored and your performance will drop. If the challenges that you face are overwhelming you will get overwhelmed. Both of these things cause a decrease in performance.

>The interesting thing is that people often feel overwhelmed when they encounter challenges that were imposed on them rather than challenges they willingly chose. An active challenge is something that you chose to engage in which is difficult, a passive challenge is something that is thrust upon you which is difficult. And when your life is one where passive challenges outweight active challenges it leads to feeling overwhelmed and stopping action. It's very hard to motivate yourself, you're kinda getting bodied by life. [...] Once active challenges outweight passive challenges, once the hard things that you do in life are more based on your choice, the more motivated and succesfull you become.

>The big thing that we've sort of discovered is that when you have a purpose, you can tolerate negativity. And in the absence of purpose you're stuck seeking out pleasure and avoiding pain. And if you build a life that is seeking out pleasure and avoiding pain that life will leave to unhappiness.




## Anger


### - Draft

- **Modifier +2** : for *hostile/confrontational* (and *concealed/deceptive*?) *targeted checks* (incoming and outgoing)

- **Modifier -2** : for support and distress *targeted checks* (incoming and outgoing)

- **Other Effects** : Substitutes all current *individual quests* with a "revenge" quest where the character must cause some form of harm to what initially caused the *breakdown*. If they complete this quest they don't gain any *XP*. If they fail it they loose *XP*.




## Shame


>https://www.youtube.com/watch?v=eJ19kSHp1Ks




## Drunkenness


>https://hbr.org/2018/05/drunk-people-are-better-at-creative-problem-solving

>https://www.reddit.com/r/rpg/comments/1by9o4/mechanics_for_drunkenness/

>https://www.cracked.com/article_18786_the-5-most-inspiring-things-ever-accomplished-while-drunk.html

***








# PATHS / ARCS / XP / CHARACTER PROGRESSION / FULFILMENT




## Design Guidelines


- Character progression should embody a distinct "career *path*" for each character. Upon reaching the culmination of this *path*, they should attain a level of power that enables them to shape or influence the world's lore.

- The characters form a team of mercenaries, employed by a prosperous mogul to fulfill a designated task, which forms the pivotal scenario for the game. However, instead of actively pursuing this as a personal goal, it should represent a responsibility compelled by various circumstances – often arising from the imperative for survival driven by financial needs (take inspiartion from *Invisible Sun*'s *Desideratum* to help explain this - The Key p.202). **Characters shouldn't know each other**, the first scenario should represent the first time they meet.

- The "weight" of this responsibility is underscored by limiting character experience points (XP) gained solely from completing the scenario. Instead, XP should be earned through the accomplishment of side objectives, tailored to each character's *path*, which should run parallel to the main scenario of the game.




## Draft


- XP can go under 0 (meaning it can be negative)?

- XP is lost when failing a scenario?

- XP is lost when a charater suffers a breakdown state (similar to outcomes, the GM doesn't have to attribute or narrate a specific state to a character if they don't want to, as long as they make them loose XP)

- When XP reaches 0 (or some negative value), the character must abandon their individual quest?

>When a character suffers a breakdown, they loose all of their *XP* earned? -> if no *XP* they must abandon their current *individual quest*? -> if no *individual quest* they must abandon the game?

- xp is gained when *bonding* with other characters, completing an *individual quest* and when suceeding actions aligned with their *avatars* or motivations.

>*Individual quests*, on top of rewarding characters with XP, also rewards them with extra bonus like career advancement, new relationships or items/equipment.




## Ideas


### - Two types of XP?

- **"Main" Xp** is earned when the character succeeds checks aligned with their motivations (*avatar*, *culture*, *beliefs*...) or when finishing their *individual quest*. Leveling through "main *XP*" cannot provide "level up" points to *wisdom* abilities though.

- **"Wisdom" Xp** is earned when the character fails (or succeeds) a *daring check* or manages to bond with another character. Leveling through "*wisdom XP*" can only provide "level up" points to *wise* abilities though.

>Have a specific *wisdom*-related skill that allows a character to gain *xp* when they undergo a *failure with major complications* ? -> Explain that skills can be both "active" (used or required when performing actions) or "passive" (have a "constant effect" on the character)


### - Individual Quests

The main drive for every player is to make their characters progress on an *individual quest* they've set for themselves at the begining of each game. An *individual quest* is an objective, an ambition or a goal that must align with a characer's path and that, once achieved, will allow them to level up and improve their abilities and skills.

- Individual quests have a "purpose" (survival - power, ego, pleasure - love, duty, burden)

- Characters have the possibility to follow multiple *individual quests* at once (and thus gain more *XP*?). But this should come at the cost of adding extra *tough stress* each time they "accept" a new quest. The amount of *tough stress* added depends on the quest's "purpose":

|Purpose    |Importance |Modifier |
|:---------:|:---------:|:-------:|
|Survival   |           |         |
|Power      |           |         |
|Ego        |           |         |
|Pleasure   |           |         |
|Curiosity  |           |         |
|Love       |           |         |
|Duty       |           |         |
|Altruism   |           |         |
|Burden     |           |         |
|Absurdity  |           |         |

>survival: +0, power/love: +1, ego/duty: +2, pleasure/burden: +3?

- The first individual quest a character starts a scenario with doesn't require adding any stress


### - Drive points idea

Characters start with (5?) *drive points* (or more depending on the purposes of their current *individual quests*...?). When attempting checks (*active* or *passive*, doesn't matter) "aligned" with these *individual quests*, they can choose to spend 1 *drive point*. This should provide them with a bonus (an extra die or modifier) to their check.


### - Lore Impact

Even though the Game Master (GM) controls the lore in which a scenario takes place, players should be able to "alter" or "impose" modifications on it if they reach their maximum level (*Avatar fulfilment*?). These modifications should have an impact on the history of the world in which the characters live. And each time the GM chooses to play with them, they'll have to take into account the modifications made by the players.


### - (Old) Ability advancement

- **Physical Abilities** : increase based on the amount of effort used to undertake actions requiring *physical abilities*.

- **Social Abilities** : increase based on the difficulty score of successful actions that have the intent of *power*, *ego*, or *pleasure*.

- **Intellectual Abilities** : increase based on the difficulty score of successful actions requiring *intellectual abilities*.

- **Wise Abilities** : increase based on the amount of effort used to undertake actions that have the intent of *altruism*, *duty* or *love*.




## Inspirations


### - Chronicles of Darkness (Aspirations)

Simplest and most straightforward arc-progression system where a player sets *aspirations* for their character (find my soul mate, take over the company, become a parent...), and gain XP (*beats*) once they manage to fulfil them.

>An important thing to consider when deciding on your Aspirations is that you want them to be active goals. They have to be something that you need to do, rather than something that you need to avoid. For example, “Don’t get drunk” wouldn’t be a very good Aspiration for a character, but “Go a day without taking a drink” could be, if your character would find this to be a struggle.


### - Chuubo's Marvelous Wish-Granting Engine

>**Action XP** (aka **Group XP**) comes from taking various actions that advance the story. It goes into a group pot that will be shared among the players and you can spend your share on any quest.


### - Invisible Sun (Arcs)

Invisible Sun's *arcs* work similar to Chronicles of Darkness' *aspirations* except they are a bit more complex:

- XP (*acumen* and *crux*) is divided into two resources: *joy* and *despair*. A character gains *joy* when good things happen, and *despair* when bad things happen. 1 *joy* and 1 *despair*, together, form 1 *crux*.

- *Arcs* have *costs*, and require characters to invest an initial amount of *acumen* (usually 2) when starting them.

- *Arcs* have following parts, called story points. As the character progresses through them, they earn *acumen* (and possibly *joy* or *despair*): *opening*, *step(s)*, *climax* and *resolution*.

>Character arcs should always take at least weeks in game time, and no more than two steps in an arc should be accomplished in a game session or side scene (and most of the time, it should be one step, if any). If neither of these two things is true, then it’s not really a character arc. You can’t, for example, use the Creation arc to guide you through something you can make in an hour or two.

***








# TIES / INTIMACIES / RESONANCES




## Naming


- Affinities
- Anchors
- Aspects
- Attachments
- Aspirations
- Bonds
- Connections
- Drives
- Footings
- Foundations
- Intimacies
- Harmonies
- Heartstrings
- Links
- Opinions
- Passions
- Principles
- Purposes
- Rapports
- Relations / Relationships
- Resonances
- Ties
- Traits
- Values




## Draft


Make a similar system akin to *Exalted*'s *intimacies* (see below). Except instead of using "levels of intensity" (*minor*, *major*, and *defining*), use positive and negative values that would reflect how much a character "esteems" or "rejects" a particular *intimacy*. In the case of a *relationship*, this would be considered as how much they "love" or "hate" another character for example.

In terms of gameplay, this value would be directly translated as a modifier for *checks* that would "relate" to that *intimacy*. For instance, if a character has a *relationship* value of +2 with another character, they would get a modifier of -2 on any *hostile* or *contentious check* targeted towards them, but a modifier of +2 to any *cooperative check*.




## Memories (idea)


*Memories* are snapshots of life that encapsulate significant moments from a character's past, shaping their current identity. These *memories* can be wielded by others to inflict *stress* on the character. Additionally, they form the foundation for their backstory, and players are encouraged to enrich their experience by introducing new *memories* to their characters as the scenario unfolds. *Memories* can be *fond* (positive) or *bitter* (negative).

>Memories shouldn't provide a positive modifier (or advantage/disadvantage), they're just there for immersion and backstory purposes.


### - Bonding

*Fond memories* are conceived/formulated when a character *bonds* with another character. When *bonding*, characters have the opportunity to remove their *tough stress* and share happy intimate moments from their past. For this to work, a player must come up with a *fond memory* and have their character share it with another character.

>? A *fond memory* should be tied to one of the character's ability where they're strong at, and portray an event or situation they're proud of, where they showed impressive skill and mastery at using that ability ?


### - Alienating

*Alienation* serves as the opposite of *bonding* (and happens when a character suffers a *breakdown* or when they fail a *bond* check...?). With this mechanic, the GM (or players) are allowed to tarnish the image of another player's character by coming up with a *bitter memory* for them.

>? A *bitter memory* is brought up in game because the character slipped up or revealed something shameful about their past by accident ?

>? Just like with *bonding*, this *bitter memory* should relate to one of the character's abilties. But instead of an ability that a character is strong at, it should be an ability where they are weak. Use this list to check what type of *bitter memories* one can come up depending on the ability being chosen:

>- *Endurance* => Fragility
>- *Strength* => Weakness
>- *Agility* => Numbness
>- *Dexterity* => Sluggishness
>- *Charm* => Repulsiveness
>- *Personality* => Timidity
>- *Creativity* => Dullness
>- *Reasoning* => Lunacy/Madness
>- *Knowledge* => Ignorance
>- *Insight* => Naïvety
>- *Humility* => Arrogance/Vanity
>- *Composure* => Angst


### - Inflicting tough stress

Just like with skills, an instigator must use a target's *memory* to inflict *tough stress* on their target. If they don't have a *memory* "at hand" (aka they don't know enough about the *target*), they just can't do it.

- The simplest way to inflict *tough stress* on a character is by refering to one of their *bitter memories*

- Another way is, if a character screwed up or did something that "clashed" against one of their *fond memories* (for example they failed an ability check where they were supposed to be strong at), an instigator can use that to inflict *tough stress* on them

>Narrating actions or words where "*tough stress infliction*" happens could make players uncomfortable, therefore not going into too much detail on what is being said or done is the best way to approach this mechanic.

>**From Balladry** : keep it respectful, mature, and in the fiction. Romance is part of fiction and can be great for immersion and engaging entertainment. Don’t be afraid of it, just keep it realistic. If any mature content arises, fade to black. Leave it to the imagination and move on.




## Backstory-Timeline


```
http://www.ashami.com/rpg/background/
https://www.d20pfsrd.com/basics-ability-scores/more-character-options/character-backgrounds/background-generator
```




## Exalted


Intimacies represent loves, hatreds, ideals, and goals - the things in this world people feel strongly about. Intimacies are important to [morale management], as they help determine what kinds of influence will affect your character. They come in two basic types:

- **Ties** describe your character’s attachments to people, objects, organizations, and other concrete entities. They have an emotional context which describes your character’s feelings towards that entity. Ties are generally written as the subject of the Tie, followed by a parenthetical clarifier to establish context.

- **Principles** describe your character’s beliefs and ideals. Principles are generally written as a statement of the Principle. Examples include ideals such as “Honesty is my watchword” and “Pragmatism rules my actions,” and beliefs such as “The Immaculate Philosophy is the true guide to righteous living” or “I believe everyone looks out for number one.”

Intimacies come in three levels of intensity: Minor, Major, and Defining. Minor Intimacies are notable parts of your character’s worldview, but only come into play when the subject of the Intimacy is directly relevant to her current situation. Major Intimacies hold more influence over your character, coming into play even if the subject is only indirectly or tangentially related to the situation at hand. Finally, Defining Intimacies hold sway over every aspect of your character’s life—they’re the pillars of her worldview, and often things she would lay down her life to protect.

Characters may gain new Intimacies in several ways, all subject to Storyteller approval. The key to changing Intimacies is that the change must make sense given the roleplaying going on during the scene and in the broader context of the story. Characters can’t gain beneficial new Intimacies if it doesn’t match how they’ve been played.


### - Gaining intimacies

Whenever the player feels it is appropriate and the Storyteller agrees, the character may add a new Minor Intimacy or intensify an existing Intimacy at the end of a scene by one degree. In extraordinary situations, the character may gain a new Intimacy at Major or Defining Intensity based on the events of the story—when an Abyssal murders your brother, it’s probably acceptable to go straight to a Major or Defining Tie of hatred toward him.


### - Loosing intimacies

Losing Intimacies is similarly simple, and likewise subject to Storyteller discretion. Intimacies can be degraded by one level or removed entirely (if Minor) by the social influence of other characters.

Whenever the player feels it is appropriate and the Storyteller agrees, the character may remove a Minor Intimacy or degrade an existing Major or Defining Intimacy at the end of a scene — the character just doesn’t care about that thing as much anymore. Generally, this should follow several sessions in which the subject of the Intimacy hasn’t come up—characters should rarely drop or degrade an Intimacy right after it has been created, even if the Intimacy is undesirable. Alternately, this might follow several sessions showing the character resolving or working to conquer unwanted Intimacies, such as Intimacies of fear or hatred.

Whenever the Storyteller judges that a player hasn’t reflected an Intimacy in her roleplaying for a while, she may declare that it has degraded even disappeared completely. This is mostly to keep characters from accumulating a lot of Defining Intimacies, which should be reflected in the character’s actions at least once per story. Few characters can sustain the kind of intensity needed for more than a small handful of Defining Intimacies, and the Storyteller’s pruning helps keep down the clutter.

***








# BONDING / CHARACTER BACKSTORY / DISPOSITION / RELATIONSHIPS




## Design Guidelines


*Social abilities* should be the ones that allow characters to remove *stress* from other characters (thus also increasing their fondness towards them). Conversely, a character with weak social skills may tend to increase the *stress stack* of others.

>Watching the Ted Talks video "My philosophy for a happy life" by Sam Berns caused a slight sensation (like the beginning of a headache) in the right side of my brain (more towards the upper front). This is probably because I was seeing a 'figure so unusual expressing themselves like a human' (even though it's indeed a human; the issue lies with me – it's just my brain struggling to integrate it)."




## Draft


- **Crack a Joke** - *Passive eloquence check* : this is for recovering *routine stress* -> success = everyone listening removes *routine stress* (you included) / -> failure = everyone listening adds *routine stress* (you not included)

- **Bonding, aka helping someone recover tough stress** - *Active humility + insight* check VS *target*'s *humility assist modifier* ...?

>"Bonding Checks" should only go one way (no "distress" crap). Basically the target character should be the one "opening up" and "sharing their bond", while the instigator should be the one having to perform the active assist check (adding the target's humility's assist modifier to their Active ST). This may seem counterintuitive at first, as the target character is the one doing all the talking, but players have to keep in mind that the character instigator is the one putting all the effort in doing therapy for the target and providing emotional support by listening and understanding what they have to say.




## Inspirations


### - Apocalypse World (HX)

*Hx* ("history") represents how much a character "knows" another character. A character holds multiple *Hx* values, and these are considered as integral "character stats" (just like *cool*, *hard* or *hot*). *Hx* gives bonus to rolls where the character interacts with the other character, no matter if they're trying to help or harm them.

>Hx doesn’t say how long you’ve known them, how much you like them, how positive your history together has been, or anything else necessarily, just how well you get them. If your Hx with somebody is negative, that means that you really don’t know them and can’t predict what they’ll do. Thus, you can’t effectively help them OR screw them over. Hx is asymmetrical — My character might know yours Hx+2, but yours might know mine Hx-1, or whatever.

*Hx* is gained through different situations (or *moves*) that involve both characters. For instance, if a character inflicts harm on another character, they gain 1Hx with them for every segment of harm suffered.


### - Dungeon World (Bonds)

Simplest and most straightforward bonding system where a player just writes *bonds* for their character. Eeach bond is a simple statement that relates them to another character. The character gains *XP* when the *bond* is resolved, aka when it no longer describes accurately how the character relates to that person.


### - Invisible Sun (Bonds)

Very simple system where *bonds* are just extra character features with specific *benefits* and *drawbacks*. For example, a *housemate bond* means two characters can share expenses, but lack an additional house. *Bonded* characters can also share character arcs.


### - Smallville (Relationships)

Very intricate and comprehensive character bonding system in which *relationships* are treated as distinct "stats" (somewhat similar to Apocalypse World's *Hx*). Each *relationship* is characterized by a *statement* encapsulating a character's viewpoint towards the other individual, accompanied by a die rating reflecting how much they care about it. When engaging in actions aligned with a particular *relationship*'s *statement*, players roll the associated die to determine the outcome.

Players can challenge *relationships* when taking actions conflicting with their *statements*. When doing so, they roll three dice instead of one, but step the challenged *relationship*'s die rating back by one level (a d8 becomes a d6 for example), and this lasts until the end of a game. Challenged *relationships* can be restored to previous ratings during "tag scenes", but *statements* must be rewritten to reflect the new perspective the character holds with the other person.


### - Spire - The City Must Fall (Bonds)

Interesting bonding mechanic where each *bond* a character forms is depicted through a distinct *resistance* (different from *blood*, *mind*, *silver*, *shadow* or *reputation*). By performing a normal roll, players can ask a *bond* for a favor but may suffer *stress* to their *resistance* while doing so:

>- **1 Stress** : Give advice or access to general information within their domain; allow safe passage through space they control; offer temporary accommodation.

>- **D3 Stress** : Get you and your comrades access to a private area or event; lend you a piece of equipment; put in a good word with an authority in their domain; turn a blind eye to minor transgressions.

>- **D6 Stress** : Gift you a valuable piece of equipment; provide a safe haven for you and your comrades; betray the trust of an outsider; turn a blind eye to major transgressions; commit minor transgressions; engage in moderate-risk actions.

>- **D8 Stress** : Betray a friend; commit major transgressions; engage in high-risk actions; donate large amounts of resources to the Ministry.

And just like with any other *resistance*, *bonds* can also suffer *fallouts* specific to them. These range from the *bond* becoming less friendly (*minor fallout*) to it completely turning against the character (*major fallout*).

*Stress* can be removed from a *bond* by doing a favour for them in return; the bigger the favour, the more *stress* is removed.


### - Urban Shadows (Intimacy Moves)

Very simple system where each time a character shares an intimate moment with another character, they both trigger an effect (*intimacy move*) related to their respective "classes". For example, if an *aware* shares a moment of intimacy —physical or emotional— with someone who isn’t mortal, they must mark *corruption*.




## Old Disposition Draft


- [Culpabilité] :

- [Respect] :

En fonction de la [Disposition] qu'un [Personnage] possède avec les autres, un [Modificateur de Disposition] viendra s'ajouter à ses [Jets d'Execution] ou à ses [SDs de Sauvegarde].

Chaque [Point de Culpabilité] est lié à un autre [Personnage] (appellé la "cible"). Ils s'utilisent et se consomment en plus des [Points de Résilience] pour permettre d'entreprendre des [Actions]. ((( Cependant, ils ne se consomment QUE si une [Action] est réussie?, autrement ils restent là => BOF ))). Les [Points de Culpabilité] positifs ne peuvent être consommés que pour faire des [Actions] [Altruistes] visant la cible en question. Les [Points de Culpabilité] négatifs ne peuvent être consommés que pour faire des [Actions] [Individualistes] visant la cible en question.

Si le [Personnage] entreprend toute autre [Action] [Altruiste] (ne visant pas la cible), il reçoit un modificateur négatif à cette dernière pour chaque [Point de Culpabilité] négatif qu'il possède et un modificateur positif pour chaque [Point de Culpabilité] positif. Les [Points de Culpabilité] ne sont pas consommés ni utilisés pour l'[Action]. 

Si le [Personnage] entreprend une [Action] [Individualiste] (ne visant pas la cible), il reçoit un modificateur positif à cette dernière pour chaque [Point de Culpabilité] négatif qu'il possède et un modificateur négatif pour chaque [Point de Culpabilité] positif. Les [Points de Culpabilité] ne sont pas consommés ni utilisés pour l'[Action].

Le but est d'avoir à tout moment une quantité de [Culpabilité] nulle ou proche de 0. Si un [Personnage] possède une valeur de [Culpabilité] positive ou négative, il perd de l'[Esprit] (à un moment donné de la partie? - lorsqu'il n'a plus de [Résilience]?)

Le [Respect] pour tout [Personnage] augmente si ce dernier réussit des [Actions] au [SD] difficile.
Le [Respect] pour tout [personnage] diminue si ce dernier rate des [Actions] au [SD] facile.


### - [Action] [Individualiste] envers une cible :

- [Respect] fort du [Personnage] envers la cible: augmentation de [Culpabilité] du [Personnage] envers la cible
- [Respect] faible du [Personnage] envers la cible: pas d'augmentation de [Culpabilité] du [Personnage] envers la cible
- [Respect] fort de la cible envers le [Personnage]: pas de diminution de [Culpabilité] de la cible envers le [Personnage]
- [Respect] faible de la cible envers le [Personnage]: diminution de [Culpabilité] de la cible envers le [Personnage]


### - [Action] [Altruiste] envers une cible :

- [Respect] fort du [Personnage] envers la cible: pas de diminution de [Culpabilité] du [Personnage] envers la cible
- [Respect] faible du [Personnage] envers la cible: diminution de [Culpabilité] du [Personnage] envers la cible
- [Respect] fort de la cible envers le [Personnage]: augmentation de [Culpabilité] de la cible envers le [Personnage]
- [Respect] faible de la cible envers le [Personnage]: pas d'augmentation de [Culpabilité] de la cible envers le [Personnage]

A toute valeur de sauvegarde vient s'additionner les modificateurs de [Disposition] ([Affinité] et [Respect]) du [Personnage] qui entreprend l'[Action]. Les [Actions] à l'[Intention] de type [Altruiste] sont plus faciles (baisse du [Seuil de Difficulté]) à entreprendre si elles ciblent un [Personnage] envers qui l'[Affinité] est positive. Au contraire, les [Actions] à l'intention de type [Individualiste] sont plus faciles à entreprendre (baisse de la valeur [Sauvegarde]) si elles ciblent un [Personnage] envers qui l'[Affinité] est négative. Les [Actions] à l'intention de type [Individualiste] sont plus faciles (baisse de la valeur de [Sauvegarde]) à entreprendre si elles ciblent un [Personnage] envers qui le [Respect] est bas. Au contraire, les [Actions] à l'intention de type [Individualiste] sont plus difficiles à entreprendre si elles ciblent un [Personnage] envers qui le [Respect] est haut.


### - Evolution de la [Disposition] envers le [Personnage] :

L'[Affection] pour un individu augmente si ce dernier a utilisé des [Points de Résilience] pour entreprendre une [Action] [Altruiste] (proportionnellement à la quantité de [Points de Résilience] consommés?), peu importe qu'il aie réussi ou pas. Au contraire, l'[Affection] pour un individu diminue si ce dernier a utilisé des [Points de Résilience] pour entreprendre une action [Individualiste] (proportionnellement à la quantité de [Points de Résilience] consommés?), peu importe qu'il aie réussi ou pas.

Le [Respect] d'un [Personnage] envers un autre augmente si ce dernier réussit des [Actions] au [Seuil de Difficulté] haut. Le [Respect] d'un [Personnage] envers un autre diminue si ce dernier rate des [Actions] au [Seuil de Difficulté] bas.

- Echec : un échec provoque de la baisse de niveau de confiance dans tous les membres de l'équipe qui aiment le personnage. Si l'échec est aussi néfaste pour un autre membre, son amour diminue. Si il est bénéfique au contraire, il augmente.

***








# PARADIGMS / BELIEFS / RELIGION




PAS DE PARADIGME POSSIBLE? = NIHILISME (Psycopathes) => NON, TOUT PERSO POSSEDE UN PARADIGME MAIS LE FAIT QU'IL SOIT NIHILISTE VEUT SIMPLEMENT DIRE QUE SES ATTRIBUTS DE SAGESSE SONT BAS?

- *Configuration morale simple* : le PJ ne se base que sur les principes/commandements de sa propre religion ou des paroles d'idoles qu'il respecte

- *Configuration morale "schizophrénique"* : le PJ se base sur une notion de bien et de mal qu'il s'est formé lui-même*


Le [Paradigme] est la représentation mentale qu'un [Personnage] a de l'univers dans lequel il se trouve. Il s'agit de sa configuration spirituelle, sa projection intérieure du monde, sa religion, ses croyances. Le [Paradigme] permet d'établir l'ensemble des préceptes philosophiques et/ou moraux qui vont aider un [Personnage] à se sentir ancré dans le monde dans lequel il vit. Il dicte au [Personnage] son mode de vie, ce qu'il doit ou ne doit pas faire, dire ou manger. Un [Paradigme] peut correspondre à une croyance, une religion, une philosophie, une liste de préceptes ou toute autre forme d'idéologie définissant un mode de vie ou une perception de la réalité. Les divergences entre [Paradigmes] maintiennent les [Personnages] d'une partie en conflit permanent car chacun est constament en train d'essayer d'imposer sa propre doctrine aux autres.

Dans le cas d'une religion ou d'une philosophie, un [Paradigme] doit s'aligner avec les préceptes et écritures de cette dernière. Dans le cas d'un [Paradigme] athéiste ou agnostique, il doit proposer une représentation plus scientifique et rationnelle du monde. "Croire que la terre est plate" versus "croire que la terre est ronde" est un parfait exemple de deux [Paradigmes] qui s'opposeraient idéologiquement.

Tout [Paradigme] est également associé à une "finalité" qui correspond à l'"objectif final" de ce dernier. Telle une prophécie, cette notion fondamentale représente le point de culmination, la situation idéale vers laquelle tout [Paradigme] est censé aboutir et prendre son sens le plus absolu. C'est une illusion, un objectif inaccessible qui reste paradoxalement convaincant en raison des promesses de paix et de prospérité éternelles qu'il apporte si jamais il venait à se réaliser. Pour être plus clair, je vais essayer d'utiliser des exemples présents dans le monde réel. Gardez à l'esprit que ces exemples ne sont pas représentatifs à 100% de ce qu'ils sont réellement. Il s'agit là de mon interprétation personelle de ces idéologies, utilisée dans ce cas-ci pour tenter de mieux exposer mes propos:

- **Islam** : En Islam, l'avertissement final a été donné à toute l'humanité au travers de son messager, Muhammad Rasūl Allāh. Muhammad est le dernier prophète de l'histoire et plus personne ne peut prétendre parler au nom du divin avant l'avènement du procès final. Dans cette religion, la "finalité" correspondrait au fait qu'une croyance internationale unique égaliserait les êtres humains et leur apporterait la paix éternelle. Si jamais ils venaient tous à accepter les 5 piliers fondamentaux, l'avènement de cette idéologie permettrait d'effacer les malentendus entre individus. Tout le monde parlerait qu'une seule et même langue (l'Arabe), les nationalismes disparaîtraient et la pauvreté ne ferait partie que du passé.

- **Athéisme** : L'athéisme est une philosophie qui considère la majorité des religions comme étant de dangereux virus qu'il est necessaire d'éradiquer. Les Athéistes croient, ou du moins voient, un avenir dans lequel les religions et l'obscurantisme spirituel ont totalement disparu de la surface de la terre. Dans une "finalité" athéiste, les êtres humains (suffisament éduqués à la science et aux disciplines déductives) penseraient tous de manière rationelle et essayeraient de trouver des réponses logiques à tout problème. La supertition et la soumission à tout être divin seraient considérées comme idiot et les guerres à motif théologique n'auraient plus lieu d'être. Les expérimentations scientifiques et innovations technologiques deviendraient la priorité de tout individu et l'éducation garantirait à chacun une compréhension de la réalité optimale.

- **Communisme** : Pensé par Karl Heinrich Marx au milieu du 19e siècle, l'idéologie communiste (aussi appellée "Marxisme") voit le monde se diviser en deux catégories: la classe dominante (bourgeoisie) et la classe opprimée (le prolétariat). Dans leur "finalité", les marxistes pensent que le prolétariat est destiné à inévitablement renverser la bourgeoisie et à récupérer le contrôle des moyens de production. Un monde communiste serait une utopie égalitaire dans lequel aucun individu ne serait placé au-dessus des autres, où les ressources seraient redistribuées de manière équitable et où l'économie serait conçue pour répondre au mieux aux besoins de chaque être humain.

***








# SKILLS & LANGUAGES




## Arcanic Skills


### - Arithmysticism

- Basic Arithmysticism (*reasoning*) : (can learn basic/starting spells) - requires 2/3? *reasoning*

- Quatatonic runes (*knowledge*) : (extra spells + allows to read *Quatatonic* scrolls) - requires 2/3? *knowledge* and "Basic Arithmysticism"

- Epylleïptic runes (*knowledge*) : (extra spells + allows to read *Epylleïptic* scrolls) - requires 2/3? *knowledge* and "Basic Arithmysticism"

- Tetrahadric runes (*knowledge*) : (extra spells + allows to read *Tetrahadric* scrolls) - requires 2/3? *knowledge* and "Basic Arithmysticism"

- Advanced Arithmysticism (*reasoning*) - (end-game spells + allows to write scrolls) - requires 3/4? *reasoning* and a combination of two "runic areas of knowledge"

- Experimental Arithmysticism (*creativity*) : (allows the invention of new calciphants) - requires 3/4? *creativity* and *Advanced Arithmysticism*


### - Artifism

- Basic Artifism (*reasoning* or *dexterity*?) : (can use basic artifistic devices) - requires 2/3? *reasoning*/*dexterity*?

- Artifism theory (*knowledge*) : (allows basic artifism repair) - requires 2/3? *knowledge*, *Basic Arithmysticism* and "Basic Artifism"

- Artifism engineering (*reasoning*) : (allows basic artifism building/crafting from a template) - requires 2/3? *reasoning*, "Artifism theory" and one "runic area of knowledge"

- Advanced Artifism (*reasoning*) : (allows advanced artifism repair, crafting (if they unlocked "Artifism engineering") and can use advanced artifistic devices) - requires 3/4? *reasoning*, one "runic area of knowledge" and "Artifism theory"

- Experimental Artifism (*creativity*) : (allows artifism invention) - requires 3/4? *creativity*, a combination of two "runic areas of knowledge", "Artifism engineering" and "Advanced Artifism "




## Areas of Knowledge


https://images.webofknowledge.com/images/help/WOK/hs_research_domains.html

>From **Invisible Sun** rules : often, using Hidden Knowledge requires a bit of narrative creation on the player’s part. In other words, you need to explain what hidden knowledge you’re drawing upon when you use it. You don’t necessarily need to elaborate on the specifics (in fact, it’s often better to leave the specifics out, because leaving them to the imagination lends power to the narrative), but you do want to consider the generalities. If you’re trying to convince the leader of the Handasa to help you and you can justify using a bit of Hidden Knowledge to recall something to blackmail her with, that might help (although the risks are obvious), but you don’t have to specify what that blackmail material might be. As another example, you might attempt to convince the GM that you once read a bit of trivia on the best way to pick locks so your Hidden Knowledge can help you in that challenge (once), but you don’t have to go into the specifics of lock construction to do so. Hidden Knowledge is not restored through rest, points spent are gone for good. However, new points can be earned through various means (through research, study, conversation, and eavesdropping or snooping).


### - Arts & Humanities

- Architecture
- Art
- History & Archaeology
- Literature
- Music & Dance
- Philosophy & Religion
- Theater
- Food & Cuisine


### - Science & Technology

- Agriculture
- Anatomy & Medicine
- Biology
- Botany
- Astronomy
- (Geology?)


### - (Combat & Sports?)

- Text




## Combat


- Shield


### - Heavy Weapons

- Axe
- Hammer
- Mace
- Longsword
- Two-Handed Sword


### - Light Weapons

- Dagger
- Shortsword
- Rapier
- Staff
- Hatchet


### - Shooting

- Bow
- Throwing Knife
- Musket
- Crossbow


### - Throwing

- Javelin
- Sling
- Flail


### - Long Weapons

- Lance
- Pike
- Halberd


### - Heavy Armor

- Pauldrons
- Helmet
- Helm
- Breastplate
- Chainmail




## Crafting / Manual Labor


### - Agriculture

- Molding
- Marking
- Sowing
- Plowing
- Mowing
- Harvesting


### - Forging

- Sharpening
- Hammering
- Beating
- Grinding
- Tempering
- Finishing


### - Masonry

- Carving
- Marking
- Coating
- Setting
- Digging


### - Tools / Other

- Broom
- Oven
- Furnace
- Anvil
- Mallet
- Bellows
- Pliers
- Sharpening Stone
- Plow
- Harrow
- Hoe
- Scythe
- Sickel
- Pitchfork
- Shovel
- Rake
- Mill
- 13-knot Rope
- Plumb Bob
- Plumb Line
- Plumb Bob
- Lifting Device
- Saw
- Chisels
- Comb
- Vise
- Loom Frame
- Loom Machine
- Paintbrush
- Lockpicking
- Manipulation
- Parrying
- Dishware
- Cooking
- Massage
- Carpentry
- Weaving
- Athletics




## Navigation


- Helm
- Knots



## Arts

- Acting ( https://www.youtube.com/watch?v=RgNYgt_9O4Q )
- Public Speaking ( https://www.youtube.com/watch?v=rPvSnrejWtA )



## Sports


- Acrobatics
- Climbing
- Stealth
- Horsemanship
- Swimming
- Wrestling
- Martial Arts




## L'Intelligence par rapport à un outil/objet:


> - Savoir: le perso connait le nom et l'utilité/fonction de l'outil
> - Logique: le perso sait comment l'outil est fabriqué, comment il fonctionne et quels sont ses mécanismes
> - Expérience: le perso a l'habitude d'utiliser l'objet/l'outil et sait le manier comme un pro




## Numenera


Astronomy
Balancing
Biology
Botany
Carrying
Climbing
Deceiving
Escaping
Geography
Geology
Healing
History
Identifying
Initiative
Intimidation
Jumping
Leatherworking
Lockpicking
Metalworking
Numenera
Perception
Persuasion
Philosophy
Pickpocketing
Repairing
Riding
Smashing
Sneaking
Swimming
Woodworking




## Balladry


### - Languages

- Languages can be learned as if they were an Additional Skill. These languages are not fluent and instead call for a successful Intellect check in order to use in a given situation. Mastery in a language skill gives fluency instead of the normal benefit.

***








# NPCS


NPCs work like "stripped down" versions of player characters. They share the same 12 abilities, die classes, and targeted modifiers. When NPCs take actions, they also roll two dice, similar to regular characters. However, unlike player characters, they roll against a fixed 'ST' value (e.g. 5?), which remains unaffected by any 'Stress' value and can't perform daring rolls (+ results of 1 and 2 count as 0).

>If need be, the GM can also give NPCs a health pool and a stress stack with specific drives and weaknesses in order to make NPCs a bit more complex.

<!-- At some point, I will make a list of "generic" NPCs that characters can encounter in the world of RoD (similar to D&D's "Monster Manual") with their own sets of stats, skills and general behavior/background in the setting. Examples of archetypes: "scoundrel", "merchant", "guard", "army officer" etc... -->


### - (3 Levels of NPCs?)

- **Level 1 - Random Encounter**: NPC has no stats, no nothing. It's just a random dude made up on the go for the purpose of a single interaction in the adventure. When interacting with them, just have characters perform simple **Passive** or **Active Checks**. And depending on the circumsances and/or archetype of the NPC, the GM may also add "difficulty modifiers" depending on what characters want to do with them (example: trying to convince a beggar to give the players food may add a trying modifier of +3 to their check, because the beggar is less likely to give them food than any other person - dumb example I know, but couldn't come up with something better rn).

- **Level 2 - Recurring NPC**: A more fletched-out NPC that characters may interact with multiple times in the adventure. NPC has basic stats, a health pool and that's all (see initial paragraph for this chapter). -> "Enemies" fall into this category?

- **Level 3 - Companion**: the most fletched-out type of NPC. Has stats, a health pool, a stress stack, a set of drives and weaknesses. This NPC roughly follows the same rules as any other character in the adventure.


### - (Idea?)

Use passive checks to replace most random NPC *targeted checks* -> from *Numenera* : It’s worth noting that players make all die rolls. If a character attacks a creature, the player makes an attack roll. If a creature attacks a character, the player makes a defense roll.

>Example: a character gets approached by a beggar on the street. The beggar is in dire straits and is genuenly asking for coin or something to eat. The character performs an easy *passive humility check* and has to make a choice depending on the outcome:

>- If they succeed, they can give a coin to the beggar which in return helps them remove some *stress* or gain some XP. Or they can do nothing and nothing "bad" happens.

>- If they fail, they can give a coin to the beggar and nothing "bad" happens. Or they can do nothing but gain some *stress*.


### - Creatures

Creatures also have the same 12 abilities, but with "base modifiers" added to them. For instance, an elephant might have a "base strenght modifier of -7" because they are huge, whereas a rat might have a "base strength modifier of +3" because they are small etc...

Each creature is then subdivided into "subtypes" which determine what scores they'll have for their abilities (for example, an "adult male elephant" will have a higher strength score than a "young female elephant").

>Creatures, because of many reasons including their language barrier, can't interract with characters the same way NPCs do. Manipulation tactics or social rolls can't be performed on them or by them.

***








# COMBAT




- With my new rule where ST is based on a character's current stress stack, it might make it easier for characters to hit enemies, even if the enemies are supposed to be strong. To circumvent this, have it so damage dealt is equal to a character's strenght score (so characters with low strenght deal very little damage even if they hit) [same with "verbal combat/insults"?].

- Damage for ranged weapons depends on the type of ammo being used, whereas damage with close-combat weapons depends on the *instigator*'s strength.




## Armor

Armor reduces *health* damage. But some armors require a character to have a minimum amount of *endurance* and/or be trained in a specific skill in order to be worn. Heavy armors impose negative modifiers to *dexterity* and *agility*.

>light armor has minimal damage reduction and the character's agility remains untouched
>heavy armor has great damage reduction but the character's Agility is reduced

>https://www.therpgsite.com/pen-paper-roleplaying-games-rpgs-discussion/armor-as-damage-reduction-or-ac

>https://rpg.stackexchange.com/questions/202217/what-bonuses-are-expected-to-be-applied-to-armor-class

***








# OTHER / RANDOM




## Extended Task Resolution Mechanic (From forum.rpg.net)


>https://forum.rpg.net/index.php?threads/critique-my-extended-task-resolution-mechanics.911132/
>UPDATED VERSION : https://forum.rpg.net/index.php?threads/critique-my-extended-task-resolution-mechanics.911132/post-24894623


### - Skill challenges

An alternative method of resolving tasks is to use a skill challenge. This can be used when the GM feels that a single skill roll is too simple or anticlimactic to capture what’s going on, such as complex tasks that may take many steps to complete. Skill challenges can also bring the passage of time to the fore: either progress on a long term project or achieving a goal before a deadline.


### - Building a skill challenge

**Complexity** is the number of successful skill checks required to succeed at the challenge. It usually ranges from 3-7 depending on how involved the task is.

**Time** represents how long the character(s) have to accomplish the task. This is usually measured in a number of rolls; a starting point should be between 1.5x - 2x the complexity, depending on how loudly the clock is ticking. Having a deadline like this is optional - in which case the challenge is **open-ended**. Here the GM may want to track how long it takes to complete the task by setting the interval between checks; such as once per hour, once per day, once per month and so on.

**Threat** represents the presence of countervailing forces acting against the characters. Threat may often represent the risk of being caught in the act, or messing up in some catastrophic fashion that causes the challenge as a whole to fail. The challenge’s threat rating is the number of failed skill checks that result in the failure of the overall challenge. As with Time, not all challenges need a threat rating.


### - Resolving a skill challenge

At its simplest you resolve a skill challenge by continuing to make skill checks until the challenge succeeds, fails, or runs out of time. As with combat, a skill challenge is divided into **rounds**.

Threat and time are obviously linked, but the players can play one against the other by choosing an **approach**. These are:

- *Neutral* - no special effect; use the rules as described above.
- *Careful* - the check takes twice the amount of Time, but failed checks do not count for Threat.
- *Quick* - the check takes half the Time, but the difficulty increases by one step.
- *Reckless* - the difficulty decreases by one step, but the check automatically generates one threat regardless of the outcome. If the challenge doesn’t have a threat rating, treat this as an automatic **complication** instead.

The difficulty and skills used for the task can vary from round to round as the challenge progresses. At each step the player should say what they’re attempting to do and the GM should describe the progress they’re making towards their goal - or not.


### - Multiple characters

Often a group of characters will be co-operating on a task. In this case, each player should decide what their character is going to do and what approach to take, and each character rolls normally.

If **any** of the characters succeed, that counts as one success towards the overall complexity rating of the skill challenge.

If **more** of the characters fail than succeed, that counts as one threat.

Not every character is obligated to roll in this way; instead they can just sit out a round or use the **helping others** rule to assist another character. Failing at the help check does not run the risk of generating threat.

If the characters are using approaches that take different amounts of time, adjust the length of the rounds compared to the challenge’s interval and which characters roll in each round to suit.


### - More Inspirations

"Progress clocks" representing long-term/ongoing effort (crafting, writing a book, etc...) -> like in Blades in the Dark or Exalted




## Keeping Track Of Resources (ammo, coins, rations, etc...)


>https://www.reddit.com/r/rpg/comments/pnldjk/how_do_you_like_to_keep_track_of_resources/

>I love usage dice. Starting with a d10, d8, d6, d4. On a roll of 3 or lower, the die goes down a step. Anything other than a 4 on a d4 means the resource is gone. It make things so simple to keep track of and abstract enough to habe you count every little bit.

>I like dice rolls. Instead of individual resources you simply assign a die (like 1d6 or 1d8) whenever the resource is used you roll that die type. On a 1 the resource is consumed and the die either drops one size or is totally consumed.




## Initiative


### - Balladry

- **Using Initiative** : Rather than simply using a clockwise order for declaring player actions, the Gamemaster may instead have the players all roll a D10 adding their Reflex Bonus to determine the order (that would be a *passive dexterity check* for me -> + only compare results that did not fail the *check*?).




## Hex-Crawling (Balladry)


### - Searching a Hex

Groups may attempt to search for something within a Hex, such as the entrance to a cave, evidence of bandits, or hunting for food. Doing so calls for the group to dedicate a number of Miles worth of travel to the search and a roll of 2Dx.

If the roll is less than or equal to the Miles spent, the groups finds what they were looking for if it was there to be found. The size of the Die rolled is based off of the Hex’s scale; D8 for Local, D12 for Provincial, and D20 for Regional.

One Character with an applicable skill may add their Skilled Bonus to the number of dedicated miles per attempt.


### - (Reminder) Dx Decider

Used by Gamemasters to expedite the process of determining the outcome of something that the players have little to no influence over or when they would have to make many checks that would slow things down and to just combine them all into a single roll.

This mechanic is expressed as such; X-in-Y. Where Y is a Die size, such as 4, 6, or 8. Rolls of X or less of a Y sided-die result in whatever the condition or event to occur.

***








# DESPAIR


**Despair** represents a character's loss of morale and willpower. It arises as a character suffers a **Breakdown**, or when specific events affect their core identity or sense of self-worth. A character may suffer **Despair** at the GM's discretion, and based on the unfolding of the narrative. Here are some examples:

- Successfully challenging or undermining a character's core motivations, identity or beliefs may increase their **Despair**. For example, a character missing a deadline for their personal **Quest**, a vegetarian being forced to eat meat, or a character deeply invested in the outcome of a war experiencing sorrow when their side loses an important battle.

- Similarly, successfully hurting a character's family, friends or livelihood can shatter their optimism, increasing their **Despair**; as they grapple with feelings of helplessness. This could also happen due to an antagonist's deliberate actions or through an unrelated event.

>*For example, if a character experiences the death of a close friend or parent a profound sense of dispossession, stemming from significant material loss or .*

<!-- This could happen due to an antagonist's deliberate actions or through an unrelated event that indirectly affects the character. -->

<!-- Material loss : a character experiences a significant loss of possessions or resources that hold personal value or importance to them. This can include strong financial setbacks, damage to important tools or loss of cherished belongings. -->

<!-- Death of a loved one : the loss of a close friend or parent has a profound impact on the character's emotional well-being. Grief and sadness can accumulate significant permanent stress, potentially affecting the character's ability to function optimally or make rational decisions. -->

<!-- they experience a profound dispossession that shakes their confidence and sense of self-worth. -->

<!-- Betrayal of trust : a close friend or ally betrays a character's trust, either by revealing sensitive information or turning against them for personal gain. This can be emotionally devastating, causing the character to question their judgment and potentially leading to a loss of friendship. -->

The maximum quantity of **Despair** a character can hold is inherently tied to their **Health**. If a character suffering a **Breakdown** has their level of **Despair** reach or overcome their current **Health**, they are taken out of action. They cannot continue as an adventurer and must retire to a different life.

>*When a character's Health is in a good state, their resilience allows them to withstand and cope with low morale. However, as their Health diminishes, their ability to mitigate Despair becomes increasingly limited.*

### - Regaining Hope

There are a few ways for a charater to regain hope and remove some of their **Despair** if needed:

#### Quest Fulfillment

A character removes all of their **Despair** (along with their **Emotional Strain**, and any **Breakdown** they're suffering) if they manage to fulfill their current **Quest**. This also gives them a *+3* **Condition Modifier** on most rolls until the end of the adventure.

>*When a character completes their personal Quest, they experience strong catharsis, allowing them to continue their adventures with a refreshed mindset.*

***
