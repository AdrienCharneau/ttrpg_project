- Malazan Book of the Fallen / Gardens of the Moon (1999, Steven Erikson)
- Bas-Lag - Perdido Street Station (2000, China Miéville)
- Mistborn (6 books)

- Ted Peterson (The Elder Scrolls' original loremaster) book recommendations : P.G. Wodehouse, Paul Bowles, Jorge Luis Borges, Evelyn Waugh, Edward Gorey, Meryn Peake, Ruth Rendell, Flannery O’Connor, Anthony Powell, Truman Capote, Nikolai Gogol