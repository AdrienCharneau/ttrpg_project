# Notes

***




## How to Start Worldbuilding: PART 1


>https://www.youtube.com/watch?v=s2mmglMpuak

- Come up with a list of "races" and "cultures" and draft a little bit of their histories

- Produce a first draft of the world map ( https://www.youtube.com/watch?v=cWINCJdVUaw ) -> Gimp tutorials...? ( https://www.patreon.com/2minutetabletop )

- Minor worldbuilding, like average money income, general food prices, weather or environment conditions... stuff like that

#### Theme

A theme in worldbuilding refers to the big picture concept that pulls everything together. These are the aspects to consider:

- How things work (technology level, whether there is magic, etc)

- What things look like (aesthetics, style, general trappings)

- What things feel like (tone, atmosphere)

>*Lord of the Rings is a classic example of a widely copied theme: Medieval European fantasy. We have low technology levels and specific magic items and magic users, we have a medieval European flavor of aesthetics, and we have an questing adventure tone that is sometimes serious but includes some lighthearted moments.*

>*If a spaceship or a gun appeared in the story, that would be out of place. If there were absurdist or horror elements introduced, those would also be hard to explain.*

#### Setting

Setting is the time and place (or when and where) of the story. It includes the environment of the story, which can be made up of the physical location, climate, weather or social and cultural surroundings.

<!-- https://literaryterms.net/setting/ -->

#### Conflict

At its most basic, conflict is the clash of opposing forces against a character’s own pursuit of a goal.




## Theme


### - How things work

- **Living tectonic plates** : The world is made of "living tectonic plates" called The Great Walkers, who are responsible for its volcanic and seismic activity.

- **Flying Mounts** : "Flying creatures" that were used as mounts for war and travel before the advent of airships. These live in giant "floating rocks" (think of "Baar Dau" in Morrowind) and form "colonies" in which they breed. There existed a form of old "flying/floating charriot" that was pulled by these creatures and used proto-artifistic technology to transport small amounts of goods or weapons before the advent of airships.

- **Whales** are giant (and generally) friendly sea creatures that can be domesticated and used as cargo ships.

- **Nectar** is an incredibly rare and sought out substance, coveted by all who seek power and prosperity. Harvested from the rock-formed levitating hives (think of "Baar Dau" in Morrowind) of large insectoid creatures, it is a versatile resource with properties that make it unparalleled in value. In its raw form, nectar is a viscous liquid with a shimmering golden hue, exuding an aroma that is both intoxicating and alluring. When heated, this precious substance undergoes a remarkable transformation, solidifying into one of the strongest materials known to the inhabitants of Atouhm. This hardened nectar is not only prized for its unparalleled durability, used in the crafting of superior weaponry and armor, but it also serves as the primary form of currency across the realm. The pursuit of nectar drives explorers and merchants alike to venture into dangerous territories where hives are found, often guarded by swarms of insectoids.

- Drinking water can be harvested on a specific tree that releases humidity during a particular season (thus creating large puddles of waters next to their roots). People have learned that attaching a device or creature that "harvests" the water directly from the tree is more efficient.

#### Four tidal seasons

Time is defined by the influence of four tidal seasons controlled by five suns. The tidal patterns create a dynamic landscape where islands and continents are connected and separated cyclically. For instance, communities must carefully manage resources, particularly during the High Season when isolation necessitates self-sufficiency.

Each sun orchestrates specific seasons that regulate ocean levels. During the low season, marked by the simultaneous appearance of the first two suns (*Vār* and *Pher*), the oceans recede, unveiling land and establishing connections between some islands and continents. The rising season sees the tide returning, making gaps between islands gradually more flooded and leading to the high season where the sea reaches its peak. Finally, the cycle ends with the dipping season where tides start to slowly recede until the low season is reached once again.


### - What things look like

Alien but somewhat grounded and realistic?


### - What things feel like

Culture clash, politics, colonialism, survival, guerrilla, rebellion, war...




## Setting

A province/colony conquered and ruled by a large foreign empire.

- **The Rhydian "Status Quo"** : An empire emerged from the remnants of a civil war between two factions, one favoring 'monarchy' and the other 'popular democracy.' Despite the absence of an official resolution to the conflict, the society adapted to this division, ultimately transforming into a powerful empire that expanded its influence by conquering other lands.

- **Hīrīs** are semi-nomadic and ruled by their **Kraäls** or leaders. In traditionnal **Hīrī** culture, only **Kraäls** are allowed to live a sedentary lifestyle, and they are the only ones that can chose who can settle a land or live in a town/city.




## Conflict


- Do a Morrowind's "Dwemer disappearance mystery", but instead of an entire civilization or people disapperaing, it's a huge chunk of the land that has disappeared/changed and nobody knows why it happened?

#### Factions

- *Rhydian Authority* : the official imperial governance that administers the province, collects taxes and guarantees it security.

- *Local Establishment* : the local elite composed of high priests and nobles. They represent the local cultural and religious establishment and have made deals with the rhydians to rule over the province and its population.

- *Paramilitaries* : an important rhydian millitary faction that is known for acting rogue, not listening to the orders coming from Bálora/Tïhras/Solgard (the imperial capital) and commiting war crimes against the local population. They are run by a charismatic general that wants to crush all rebel factions, end the administrative corruption and take the lead of the province. *Inspirations : New Vegas' Caesar's Legion, Franco's military junta etc...*

- *Rebels* : a group of local insurgents opposing the rhydian establishment and its allies. They hide in the furthest reaches of the province, are backed by powerful foreign enemies of the empire and tend to be fanatic/traditionalistic in nature. They are known for being ruthless and scaring civilians that are too cooperative with rhydians. They have commited numerous terror attacks against imperial military bases and local institutions, particularly in cities. *Inspirations: Hamas, Hezbollah, FARC, Mujahideen, etc...*

#### Development

In the early history of Atouhm, as recounted by the people of Dranhir, a formidable civilization originating from the earth's core dominated the world, enslaving all humans under their oppressive rule. Known for their unparalleled technological and mystical prowess, they seemed invincible. However, over the centuries, their power waned, and humanity revolted, ultimately eradicating their overlords and leaving only enigmatic ruins behind. According to legend, before their complete disappearance, this civilization discovered space travel and some escaped to the moon, where they are said to reside, watching over the world they once ruled.

Players are tasked to find  a paramilitary group that defected from the Rhydian army and is causing trouble in the region (Apocalypse Now style). They will learn that the commandant of this group aims to overthrow the Rhydian Emperor and lead the empire themselves. To achieve this, his soldiers have captured what they believe to be the last survivor of the ancient civilization, hoping to use them to travel to the moon. However, that survivor has now escaped and is rallying tribes to rebel against the empire too, gathering popular support. In the end, players will have the chance to speak with this individual but will remain uncertain about his origin or purpose. Is he truly the last descendant of the ancient civilization, someone capable of moon travel? Or are they just a charlatan, a prophet or both?

<!-- Alternative twist : characters are prisonner slaves sent to the province to perform forced labor. They are actually secret agents sent there for a very important task. But their minds/soul have been erased so they can't give out their true identity to anybody. Someone is supposed to pick them up/buy them on arrival and free them plus restore their minds/souls back so they are able to complete their task. Unfortunatelly, the vehicle transporting the players has an incidient during the trip and crashes in the middle of nowhere. The players are stranded and don't have access to their minds/souls anymore, they don't know their true identity/past and forgot all of their skills. They'll be able to recover them later on in the adventure but will have to make a choice between going back to their true identity or keep the new one they've been playing with the whole game (all the while accepting that it's a fake identity). -->

#### Epilogues

- *Coup* : if the paramilitaries win, they end up seizing total control over the province. Opponents are incarcerated, rebels get massacred, land is confiscated from the local population and some form of genocide ensues. *Paramilitaries* now have their eyes on Bálora/Tïhras/Solgard (the imperial capital), ready to march and establish their dominance over the whole Rhydian Empire.

- *Disintegration* : the Rhydian power is outmatched and the paramilitaries get defeated. The empire has to abandon the province and rebel factions take over. This is not the end of the civil war, as rebels now fight with each other to assert dominance. The province is fragmented and warlords rule the land. Mass migration from the local population ensues.

- *Consolidation* : somehow paramilitaries and rebels get defeated and peace returns to the province. Things return to what they were before, except Rhydian imperial rule is now strengthened.

- *Independence* : somehow paramilitaries and rebels get defeated and peace returns to the province. But the conflict has weakened the rhydians, and enlightened local nobles have re-negociated their deal with imperial authorities. They've managed to make the province independent, but still retain the corruption and some of the previous ties they had with Bálora/Tïhras/Solgard (the imperial capital).

***

***

***

***

***

***

***

***

***








# Introduction

***




<!-- ## Game Titles


- Rise of Dissent
- Faith and Rebellion (Foi et Sédition ?)
- Birth of Dissent
- Shadow of Rebellion
- Warlords
- The Province
- Banner of Faith
- Bloodstained Banners
- Dominion's Fall
- Echoes of Struggle
- Faith's Descent
- Colony of Strife -->




<!-- ## Engine Names




- Adversity Engine -->




## Core Ideas


<!-- https://screenrant.com/dnd-morrowind-rpg-setting-worldbuilding-elder-scrolls-tabletop/ -->

<!-- https://www.reddit.com/r/Morrowind/comments/92udt1/what_are_the_influences_of_morrowinds_setting/ -->

<!-- https://en.wikipedia.org/wiki/Gandahar_(film) -->

***








# Time

***




Time is defined by the influence of four tidal seasons controlled by five suns. The tidal patterns create a dynamic landscape where islands and continents are connected and separated cyclically. For instance, communities must carefully manage resources, particularly during the High Season when isolation necessitates self-sufficiency.

#### Four tidal seasons

Each sun orchestrates specific seasons that regulate ocean levels. During the low season, marked by the simultaneous appearance of the first two suns (*Vār* and *Pher*), the oceans recede, unveiling land and establishing connections between some islands and continents. The rising season sees the tide returning, making gaps between islands gradually more flooded and leading to the high season where the sea reaches its peak. Finally, the cycle ends with the dipping season where tides start to slowly recede until the low season is reached once again.

<!-- Tidal Harvesting: The receding tides during dipping season reveal rich mineral deposits and fertile land. Communities plan their economy around these cyclical opportunities. -->

<!-- Resource Scarcity: The isolation during rising and high seasons forces efficient use of resources. Trade is essential during low Season to gather what is needed for the rest of the year. -->

<!-- Innovative Agriculture: Adapted farming techniques to utilize the temporary fertile lands, including floating gardens and cliffside terraces. -->

>*Indigenous Australian seasons* : https://en.wikipedia.org/wiki/Indigenous_Australian_seasons

#### Rhydian calendar

<!-- 24 hours per day, 360 days per year? -->

***








# Geography / Biology

***




<!-- Define the shape and limits of the world, its continents, landmasses known areas and what people generally know about it. -->

#### Living tectonic plates

The world is made of "living tectonic plates" called The Great Walkers, who are responsible for its volcanic and seismic activity.

>https://youtu.be/4d8ySaOmkDw?si=POYx68Quicpa-VN9&t=287

#### Oceans

#### Landmasses




## Regions


### - Sunfire Drylands

A rough scorching environment, stretching endlessly under the heat of the *five suns*. This hot terrain is characterized by vast stretches of arid plains covered in low, resilient vegetation adapted to thrive in the intense heat. Under the sun-scorched surface, lies a complex network of subterranean tunnels that provide shelter for small creatures. These tunnels allow them to stay active and shielded from the searing temperatures found above. During the colder parts of the year, a multitude of unique species travel to this land to nest, adding a fleeting burst of life and activity to this otherwise desolated landscape.

#### Climate

#### Fauna & Flora


### - Hellhaven

The *Hellhaven* stands as the largest terrestrial biome in *Atouhm*, a lush and vibrant environment filled with tropical vegetation. Towering trees, adorned with iridescent, oversized fronds, dominate the landscape. These colossal flora, known as *Solar Scepters*, absorb the radiant energy from the *five suns* to [?]. Beneath the foliage, the ground is a lush carpet of moss and ferns. Creatures of all shapes and sizes navigate the terrain, displaying an array of vivid colors and patterns.

#### Climate

#### Fauna & Flora


### - High Glades

A convergence of temperate grasslands and towering coniferous giants. The ground is blanketed in a mosaic of grasses, interspersed with wildflowers that sway in the gentle breeze. Streams and rivers weave through the land, nourishing the roots of majestic conifers, and their banks serve as the chosen haven for the rare arboreal organisms that thrive in the cool shade.

#### Climate

#### Fauna & Flora


### - Bladerifts

Sharp, crystalline fields that catch the light, creating stunning but dangerous landscapes. Formed from volcanic glass, they are treacherous to cross without proper equipment. Beneath, expansive cave systems harbor unique species that are bioluminescent and adapted to complete darkness.

#### Climate

#### Fauna & Flora




## Fauna / Bestiary


- A large parasitic plant that grows beneath houses, burrows, or even castles, eventually taking control of the entire structure with its vines. It releases toxic spores that transform its inhabitants (human or creature) into subservient zombies known as "infected" (these are the zombies of my world...?).

- A scavenger creature that assembles a protective shell from bits of ancient man-made equipment (found in "dwemer" ruins for example), much like how hermit crabs use shells for protection in the real world.

- "Flying creatures" that were used as "mounts" for war and travel before the advent of airships. These live in giant "floating rocks" (think of "Baar Dau" in Morrowind) and form "colonies" in which they breed. There existed a form of old "flying/floating charriot" that was pulled by these creatures and used proto-artifistic technology to transport small amounts of goods or weapons before the advent of airships.

<!-- Viaticum : The only intelligent species is humankind, but many believe in the presence of other soul carriers, such as demons, ghosts, or people from the stars. Talking animals live only in imagination, rumors, and legends. Dragons, were-beasts, goblins, and huntlords and huntqueens are often considered to be self-aware and near human intelligence. -->

<!-- How Ants Learned to Control Fungus : https://www.youtube.com/watch?v=-MQkjCq9D4U -->

#### Europa / Deep Sea Hydrothermal Vent Biology

<!-- Scientists Think There Could Be LIFE on Europa and It’s Even Weirder Than We Thought : https://www.youtube.com/watch?v=UqL-lRRgsN0 -->

<!-- https://en.wikipedia.org/wiki/Hydrothermal_vent#Biology_of_hydrothermal_vents -->

<!-- https://en.wikipedia.org/wiki/Spanish_dancer -->

<!-- https://en.wikipedia.org/wiki/Enypniastes -->

<!-- https://oceaninfo.com/list/mariana-trench-animals/ -->

***








# History

***




## Inspirations


#### Real

- **Rwandan genocide** : https://en.wikipedia.org/wiki/Rwandan_genocide

- **Mithridatic Wars** : https://en.wikipedia.org/wiki/Mithridatic_Wars

- **Song–Đại Việt war** : https://en.wikipedia.org/wiki/Song%E2%80%93%C4%90%E1%BA%A1i_Vi%E1%BB%87t_war

- **Siege of Paris (1870–1871)** : https://en.wikipedia.org/wiki/Siege_of_Paris_(1870%E2%80%931871)

- **Sabra and Shatila massacre** : https://en.wikipedia.org/wiki/Sabra_and_Shatila_massacre

- **French Algeria** : https://en.wikipedia.org/wiki/French_Algeria

- **Mukden incident** : https://en.wikipedia.org/wiki/Mukden_incident

- **Mau Mau rebellion** : https://en.wikipedia.org/wiki/Mau_Mau_rebellion

-**Glorious Revolution** : https://en.wikipedia.org/wiki/Glorious_Revolution

#### Imaginary

- **Ancient civilization of Eftal** (from Nausicaa): https://nausicaa.fandom.com/wiki/Eftal




## Timeline of Rhydium


- An empire emerged from the remnants of a civil war between two factions, one favoring 'monarchy' and the other 'popular democracy.' Despite the absence of an official resolution to the conflict, the society adapted to this division, ultimately transforming into a powerful empire that expanded its influence by conquering other lands.




## Timeline of Dranhir


- The setting takes place in the region of **Dranihr**. **Dranihr** holds many kingdoms and people. It used to be ruled by the now long gone **Great Ankat Empire** from the east (capital: **Aashiam**).

- Among them are the **Hīrī People**, with their ancient culture. **Hīrīs** are semi-nomadic and ruled by their **Kraäls** or leaders. In traditionnal **Hīrī** culture, only **Kraäls** are allowed to live a sedentary lifestyle, and they are the only ones that can chose who can settle a land or live in a town/city.

- **Hīrīs** are rivals with the **Samahri Kingdom**, their closest neighbour, also with its own people and ancient culture. They have a dispute over the **Manj territories**.

- **Buadach**, also called **The Tyrant of Ankod** , tried to fill the power vacuum left by the **Ankat Empire** by trying to conquer **Dranihr** for himself. In this process, he started a war with the new emerging and powerful **Rhydian Empire** from the west.

<!-- Get some inspiration from Assyrians for Buadach, and how they were hated by everyone because of their shitty behavior) -->

- After defeating **Buadach** (with great help from the **Samahri Kingdom** and the **Hīrīs**), the **Rhydian Empire** annexes the entire region of **Dranihr** and gives preferential treatment to **Samahris** (because of their closer ties with **Rhydians** or something).

- The **Samahrin Kingdom** gets some control over the **Manj territories** and **Samahri** colonists start settling their portion of the land, a region mostly populated by **Hīrī People**.

- Turns out the land of **Hīrīs** is also rich in **Aurilith** and **Mythorium**, natural resources that the **Rhydian Empire** seeks. Contracts are signed with the **Kraäls** to exploit the land and **Rhydians** break local religious customs in the process.

- All of this angers **Hīrīs**, who feel like they got the short hand of the bargain and are being sold by their **Kraäls**. They get radicalized and start engaging in armed resistance/terrorist acts.

- Meanwhile, the new **Hithimaal Empire** is rising in the east, wanting to re-established the power and glory of the old **Ankat Empire**. They are **Rhydia**'s biggest threat and are known to support **Hīrī** resistance groups and millitias.

<!-- Originally the sand elves (along with some humans) from the central desert were the most advanced civilization(s) (pbly invented arithmysticism). -->

<!-- The central desert was a lush forest but it became dry overtime pbly because the elves (now sand elves) living there went too far with their arithmystic practices (aka Morrowind's dwemer disappearance mystery but with the land instead of its people) - after the "great collapse" of the ancient elven civilizations - creation of a new religion and culture for the sand elves to limit their use of arithmysticism. -->

<!-- The "western empire" (combination of "western humans" and "island elves" - aka "romans" + "greeks/europeans") have taken over as the new world power on the block. -->

<!-- There's also a large "new" "chinese/japanese dwarf + gnome elf" empire to the east, and it's the "western empire"'s biggest enemy -->

<!-- North of the "western empire" live the "alien tribes"? (that usually get enslaved by the western empire) - interchange "greek island elfs" with "alien tribes"? -->

***








# Society & Culture

***




>https://www.reddit.com/r/Destiny/comments/1d142zy/a_heads_up_about_israeli_culture_before_your_trip/

- Drinking water can be harvested on a specific tree that releases humidity during a particular season (thus creating large puddles of waters next to their roots). People have learned that attaching a device or creature that "harvests" the water directly from the tree is more efficient.

- The substance required to propel or activate artifistic devices should be "mined" from some type of underground "ant colonies" or something (but try to differentiate yourself from kwama eggs in Morrowind)

- A culture where the whole dialect you're taught to speak is different depending if you're a man or a woman (it is even forbidden to speak the dialect from the other gender)

- Many scammers (in cities) claim to be part of or know of the thieves guild. But there are many fake "unofficial" thieves guilds that are actually just groups of bandits that like to take advantage of people.

- Traditional cultural trait : fighting on hills or taking good positions on battle is dishonorable (or even fight with weapons)

- Law : it is FORBIDDEN not to carry a weapon if you go out of the city.

- Trial by combat : lawyers are now mercenaries

- Old civilizations used to have airships too, but their technology was lost to time.

- Rhydian soldiers should be wearing colonial British pith helmets? `https://en.wikipedia.org/wiki/Pith_helmet`

<!-- Races don't form societies or cultures. Societies are composed of people from multiple races who live together as a unified group. These societies may conflict with other societies made up of the same races but differing in language, religion, and history. -->

<!-- I still have to explain how these races interbreed with each other though. Also, having different playable races will cause an issue whith the way Ability scores are handled. -->

<!-- One of the races appeared after a great plague? -->

<!-- Aliens : don't have a culture or territory, they're just found in small minorities accross all populations, they're usually found in accounting positions, mages or scientists -->

<!-- Idea 1: Aliens came from another planet and brought magic with them a long time ago. They slowly became a minority and blended with the local population now living with them -->

<!-- Idea 2: Aliens are just powerful arcanists (humans, elves, dwarves...) that got "transfigured" because of the effects of arcanism in society? -->

#### Nectar

Nectar is an incredibly rare and sought out substance, coveted by all who seek power and prosperity. Harvested from the rock-formed levitating hives of large insectoid creatures, it is a versatile resource with properties that make it unparalleled in value. In its raw form, nectar is a viscous liquid with a shimmering golden hue, exuding an aroma that is both intoxicating and alluring. When heated, this precious substance undergoes a remarkable transformation, solidifying into one of the strongest materials known to the inhabitants of Atouhm. This hardened nectar is not only prized for its unparalleled durability, used in the crafting of superior weaponry and armor, but it also serves as the primary form of currency across the realm. The pursuit of nectar drives explorers and merchants alike to venture into dangerous territories where hives are found, often guarded by swarms of insectoids.

#### Morrowind slavery

Come up with a "barbaric" ancestral cultural practice that is banned by the Empire (like slavery in Cyrodiil). Take inspiration from ancient celtic druids, who allegedly practiced human sacrifice and cannibalism.

- https://www.nationalgeographic.com/culture/article/druids-sacrifice-cannibalism

- https://imperiumromanum.pl/en/curiosities/romans-and-druids/

- https://en.wikipedia.org/wiki/Religious_persecution_in_the_Roman_Empire#Druids

- https://en.wikipedia.org/wiki/Wicker_man




## Ethnicities


<!-- "Ilians" from "Ilia" -->


#### Arthusians

The people of *arthusia* (also known as *Thïryans*) are a blend of "europe/renaissance-steampunk" humans and western elves, and form the dominant culture within the Western Empire. Their capital city, known as *Tïhras*, is a sprawling metropolis where commerce and *artifistic* advancements flourish.

#### Rahmaris

The Province is a diverse realm, comprising ancient humans and elves. Known as the "stone people" or *Rahmari*, they were once an advanced "Anssyrian/Persian" civilization dwelling in a central water archipielago. But their misuse of *arcana* led to the land's transformation into a barren wasteland.

To prevent further devastation, the *Rahmari* formed a new religion called the *Sancora*, which emphasizes the careful and responsible use of *arithmysticism*. They now live harmoniously with the rest of the Province and its wildlife.

#### Xi'Lorins and Ta'syrs

The *Xi'Lorin*, a race of industrious "chinese-looking" dwarfs, have formed a great empire in the east. Their capital city, *Xan'Torin*, is renowned for its intricate architecture, *arcanic*-powered marvels, and towering pagodas. The *Ta'syr*, also known as the "long-eared gnomes", are elegant and enigmatic elves who share a strong bond with nature.

While the *Xi'Lorin* and *Ta'syr* coexist peacefully, tensions have risen with the encroaching Western Empire, leading to a simmering conflict.

#### Skal'Draak

The *Skal'Draak* are a tribal race of "alien" looking beings with reptilian features. They have a distinct blend of Viking and Zulu cultures. Originating from the frozen lands in the north, the *Skal'Draak* were once a proud and independent people until they were conquered and enslaved by the Western Empire. They live in traditional mudbrick buildings, establishing close-knit communities within the empire's territories.


### - Drafts

Short and slender with pale, sun-sensitive skin, adapted for cooler underground environments. Their large eyes are well-suited for dim lighting, helping them see clearly around caves and tunnels. Their skin tones are earthy, ranging from dark brown to deep greenish hues.

The [name] are highly skilled in husbandry, particularly in raising livestock such as [name] and [name]. Some communities specialize in irrigation and subterranean farming, using distinctive methods to grow food in underground environments. Others are nomadic in nature, living in mobile, spelunking communities that move with the [seasons].

<!--

Tall and slender with pale, sun-sensitive skin, adapted for cooler, high-altitude environments. Their large eyes are well-suited for dim lighting, helping them see clearly during early morning or dusk. Their hair is typically light in color, ranging from blond to white.


Stocky, with dark, ash-colored skin that has adapted to the dry, volcanic landscapes where they live. Their compact bodies are built for strength, and they are highly resistant to heat and dehydration.

The Drukari are physically robust and capable of enduring tough environments, including extreme heat and minimal water supply. They are known for their resilience and ability to work in physically demanding jobs like mining, construction, or forging metal.

Living in volcanic or desert regions, the Drukari have developed a culture centered around self-reliance and craftsmanship. They are highly skilled metalworkers and builders, constructing large fortresses and tools that help them thrive in their harsh environment. Honor and physical strength are deeply respected values in their society.


Slender and light-skinned, with hair that ranges from white to light gray. Their eyes are slightly larger than average, adapted for nocturnal activity and low-light conditions. They are nimble and lightly built, allowing for agility and endurance in forested or overgrown environments.

Elantiri are skilled in tracking and navigating through dense forests or wilderness. They excel in activities that require fine motor skills, such as crafting, hunting, and tool-making. Their nocturnal nature allows them to be active during twilight or nighttime, giving them a tactical advantage in certain environments.

The Elantiri live in close-knit, forested communities where their society revolves around preservation of the environment. They are skilled hunters and gatherers, making efficient use of available resources while maintaining a deep respect for nature. Their cultural practices often include rituals linked to seasonal changes, and they prefer to live in harmony with their surroundings.


Medium height with lean, athletic builds. Their skin tends to be slightly oily, with tones ranging from olive to gray, helping them retain moisture in humid or aquatic environments. Their limbs are strong and slightly elongated, making them excellent swimmers.

Zhar'ki are adapted to life near or in water, with strong lung capacity for diving and swimming. They are skilled at fishing, crafting boats, and creating water-based infrastructure like bridges and dams. They can survive long periods in humid, coastal climates, and their diet is heavily based on seafood and aquatic plants.

Living in coastal or marshland areas, the Zhar'ki build settlements that interact closely with water. Their economy is based on fishing, trade, and boat-building, and they have developed a deep respect for the sea as a source of sustenance. Socially, they tend to form cooperative communities, where resources are shared, and collaboration is key to survival in their watery environments.


Short and sturdy, with thick, calloused skin that has adapted to living in rocky or underground environments. Their skin tones are earthy, ranging from dark brown to deep greenish hues, reflecting their environment. They have broad, strong hands suited for manual labor.

The Grimka are excellent builders and farmers, with a knack for underground construction and sustainable agriculture. Their communities often specialize in irrigation and subterranean farming, using innovative methods to grow food in difficult environments. Their strong physical endurance makes them capable of handling tough, labor-intensive tasks.

The Grimka live in self-sustained, underground or semi-subterranean cities, valuing communal living and resource-sharing. They have a strong focus on sustainability, practicing rotational farming and careful management of natural resources. Their society revolves around cooperation, with each member contributing to the well-being of the community, especially in engineering and agriculture.


Medium height with tanned or bronze skin, adapted to life in arid savannas and grasslands. Their bodies are lean but muscular, built for endurance and long-distance movement. They often have tightly curled hair and sharp, angular facial features.

The Arkhani are highly skilled in animal husbandry, particularly in raising livestock such as cattle and horses. They are exceptional at navigating open plains and have a deep knowledge of migratory patterns and seasonal changes. Their endurance allows them to travel long distances on foot or horseback, making them adept at long-range trade and communication.

Nomadic in nature, the Arkhani live in mobile, tented communities that move with the seasons. Their society revolves around the care of animals and the seasonal migration of herds. They value resourcefulness, mobility, and oral tradition, passing down knowledge and history through storytelling and songs. Trade and diplomacy with neighboring peoples are crucial to their way of life.


Short and compact, with olive to dark brown skin tones. Their thick, coarse hair is often worn short for practicality in their humid, jungle environment. Their wide noses and large, dark eyes are adapted to the warm, moist climate, helping them avoid heat exhaustion and irritation from intense sunlight.

Kolvari are skilled in agriculture, particularly in cultivating tropical crops like fruits, nuts, and medicinal plants. They are adept at constructing sturdy homes from natural materials, using bamboo, palm, and clay to create elevated structures that withstand heavy rains and flooding. Their expertise in herbal medicine is renowned, with knowledge passed down over generations.

The Kolvari live in densely forested areas, often near rivers or in jungle clearings. Their society is community-focused, with a deep respect for the land and its resources. They practice sustainable farming and fishing, and their religious or spiritual beliefs are closely tied to the natural world. Kolvari culture emphasizes collective well-being, with strong communal ties and shared labor for agriculture and construction.

-->


### - Research

#### IBD fragments in world population

- AFE (East Africa)
- AFW (West Africa)
- AMR (America)
- ARC (Arctica - N Eurasia + N America)
- EAS (East Asia)
- EUR (North Europe)
- SAS (Hindustan Peninsula)
- OCE (Oceania + Australia)
- MDE (Middle East)

<!-- source : https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7696950/ -->




## Food and Agriculture


>https://en.wikipedia.org/wiki/Famine_food

>https://en.wikipedia.org/wiki/Bark_bread




## Social Structure




## Traveling




## Commerce




## Architecture




## Clothing




## Entertainment




## Vocabulary


- *Lorth* : northern city

- *Grapple* : common fruit/staple food

- *Greatwaters* : ocean

- *Ankat* : staple food? (like rice, wheat, oat, maize, etc...)

- *Flamefruit*, *Skyroot*, *Mistroot*, *Mythshade*, *Willowgrain*

- *Fitah*

- *Anjara* :

- *Sahafa* :

- *Tahot* :

- *Jovah* :

***








# Magic & Arcana

***




#### Tips

- 7 Stage process for building magic systems : https://www.youtube.com/watch?v=W4Czzz7NX7E
- Keep it specific : https://youtu.be/B0kMhTmebsA?t=742
- 5 Questions to ask about magic and power : https://www.youtube.com/watch?v=Cs_kozJYN6U

#### Brainstorming

>Magic **categories** or **verbs** ? = assemble, disassemble, move, stop, transform, reset

>**Reality Anchors** : machines capable of imposing other systems of physical laws on the surrounding reality.

>I might write another very soft and irrational magic system that could run parallel to this one if I want to implement "supernatural" concepts like necromancy, shamanism or divination) -> https://www.youtube.com/watch?v=uJlIcCsJe70

***








# Religion & Philosophy

***




- https://en.wikipedia.org/wiki/List_of_creation_myths
- https://en.wikipedia.org/wiki/Timeline_of_cosmological_theories
- https://en.wikipedia.org/wiki/Paradigm

<!-- https://www.youtube.com/watch?v=dg2qy_AUPNo -->

- There's a precept in a religion that says that the only people that go to heaven are the ones that achieve in life what their fathers wanted them to do

- "Imperial Religion" : worships the three gods of "Prosperity", "Law" and "Conquest/Defense"? Strictly forbids to attack, can only fight in self-defense (see Roman casus belli: https://history.stackexchange.com/questions/45383/was-roman-expansion-casus-belli-as-just-as-they-claimed)

- Arvahnism : religion/philosophy that follows the teachings of the prophet *Arvahna*, worshipping 7 gods and 7 demons -> taoism (yin/yang), christian's "Seven deadly sins" ( https://en.wikipedia.org/wiki/Seven_deadly_sins ), hinduism, Kabbalah...

- Zoroastrianism : https://en.wikipedia.org/wiki/Zoroastrianism

- Aztec Religion : https://en.wikipedia.org/wiki/Aztec_religion

- Ojibwe religion : https://en.wikipedia.org/wiki/Ojibwe_religion

- Inuit religion : https://en.wikipedia.org/wiki/Inuit_religion

- Anishinaabe religion : https://en.wikipedia.org/wiki/Anishinaabe_traditional_beliefs / https://en.wikipedia.org/wiki/Teachings_of_the_Seven_Grandfathers

#### Catharism

- https://en.wikipedia.org/wiki/Catharism

Followers were known as Cathars or Albigensians, (after the French city Albi where the movement first took hold), but referred to themselves as Good Christians. They famously believed that there were not one, but two Gods—the good God of Heaven and the evil god of this age. According to tradition, Cathars believed that the good God was the God of the New Testament faith and creator of the spiritual realm; many Cathars identified the evil god as Satan, the master of the physical world. The Cathars believed that human souls were the sexless spirits of angels trapped in the material realm of the evil god. They thought these souls were destined to be reincarnated until they achieved salvation through the "consolamentum," a form of baptism performed when death is imminent. At that moment, they believed they would return to the good God as "Cathar Perfect." Catharism was initially taught by ascetic leaders who set few guidelines, leading some Catharist practices and beliefs to vary by region and over time.

#### Demonology

- https://en.wikipedia.org/wiki/Demonology
- https://en.wikipedia.org/wiki/The_Lesser_Key_of_Solomon
- https://en.wikipedia.org/wiki/Pseudomonarchia_Daemonum

<!-- https://www.youtube.com/watch?v=eYn-YY8hz94 -->

<!-- https://www.youtube.com/watch?v=nZmEro_ODqc -->

***








# Campaign/Module

***




## Premise


#### Background

- Characters are thrown into a province of the Rhydian Empire.

- The province is a colony. Locals don't share the same culture as the rhydians and see the occupation with resentment.

- Because of corruption and mismanagement, civil unrest and general mistrust against rhydians, the imperial establishment and the local elites increases.

- New guerrila factions start to appear and rumors of a civil war loom on the horizon.

#### Factions

- *Rhydian Authority* : the official imperial governance that administers the province, collects taxes and guarantees it security.

- *Local Establishment* : the local elite composed of high priests and nobles. They represent the local cultural and religious establishment and have made deals with the rhydians to rule over the province and its population.

- *Paramilitaries* : an important rhydian millitary faction that is known for acting rogue, not listening to the orders coming from Bálora/Tïhras/Solgard (the imperial capital) and commiting war crimes against the local population. They are run by a charismatic general that wants to crush all rebel factions, end the administrative corruption and take the lead of the province. *Inspirations : New Vegas' Caesar's Legion, Franco's military junta etc...*

- *Rebels* : a group of local insurgents opposing the rhydian establishment and its allies. They hide in the furthest reaches of the province, are backed by powerful foreign enemies of the empire and tend to be fanatic/traditionalistic in nature. They are known for being ruthless and scaring civilians that are too cooperative with rhydians. They have commited numerous terror attacks against imperial military bases and local institutions, particularly in cities. *Inspirations: Hamas, Hezbollah, FARC, Mujahideen, etc...*

#### Goal

- Players have been hired by some imperial (commercial) faction to perform a series jobs in the province.

- In the process of accomplishing different contracts, they get dragged into the politics of the region and come into contact with the various factions fighting for power.

- Once finished, the campaign ends with a different *epilogue* depending on the character's choices and factions they chose to align themselves with.

<!-- Alternative twist : characters are prisonner slaves sent to the province to perform forced labor. They are actually secret agents sent there for a very important task. But their minds/soul have been erased so they can't give out their true identity to anybody. Someone is supposed to pick them up/buy them on arrival and free them plus restore their minds/souls back so they are able to complete their task. Unfortunatelly, the vehicle transporting the players has an incidient during the trip and crashes in the middle of nowhere. The players are stranded and don't have access to their minds/souls anymore, they don't know their true identity/past and forgot all of their skills. They'll be able to recover them later on in the adventure but will have to make a choice between going back to their true identity or keep the new one they've been playing with the whole game (all the while accepting that it's a fake identity). -->




## Development


#### Causes for rebellion

- Oppressive rule of **Rhydian** governors

- Widening gaps between the wealthy aristocracy and the downtrodden masses

- Religious tensions

- Anti-taxation protests

- Clashes between **Hīrīs** and **Samahris** in mixed cities


#### Main Story

>*In the early history of Atouhm, as recounted by the people of Dranhir, a formidable civilization originating from the earth's core dominated the world, enslaving all humans under their oppressive rule. Known for their unparalleled technological and mystical prowess, they seemed invincible. However, over the centuries, their power waned, and humanity revolted, ultimately eradicating their overlords and leaving only enigmatic ruins behind. According to legend, before their complete disappearance, this civilization discovered space travel and some escaped to the moon, where they are said to reside, watching over the world they once ruled.*

Players are tasked to find  a paramilitary group that defected from the Rhydian army and is causing trouble in the region (Apocalypse Now style). They will learn that the commandant of this group aims to overthrow the Rhydian Emperor and lead the empire themselves. To achieve this, his soldiers have captured what they believe to be the last survivor of the ancient civilization, hoping to use them to travel to the moon. However, that survivor has now escaped and is rallying tribes to rebel against the empire too, gathering popular support. In the end, players will have the chance to speak with this individual but will remain uncertain about his origin or purpose. Is he truly the last descendant of the ancient civilization, someone capable of moon travel? Or are they just a charlatan, a prophet or both?




## Epilogues


- *Coup* : if the paramilitaries win, they end up seizing total control over the province. Opponents are incarcerated, rebels get massacred, land is confiscated from the local population and some form of genocide ensues. *Paramilitaries* now have their eyes on Bálora/Tïhras/Solgard (the imperial capital), ready to march and establish their dominance over the whole Rhydian Empire.

- *Disintegration* : the Rhydian power is outmatched and the paramilitaries get defeated. The empire has to abandon the province and rebel factions take over. This is not the end of the civil war, as rebels now fight with each other to assert dominance. The province is fragmented and warlords rule the land. Mass migration from the local population ensues.

- *Consolidation* : somehow paramilitaries and rebels get defeated and peace returns to the province. Things return to what they were before, except Rhydian imperial rule is now strengthened.

- *Independence* : somehow paramilitaries and rebels get defeated and peace returns to the province. But the conflict has weakened the rhydians, and enlightened local nobles have re-negociated their deal with imperial authorities. They've managed to make the province independent, but still retain the corruption and some of the previous ties they had with Bálora/Tïhras/Solgard (the imperial capital).

***
