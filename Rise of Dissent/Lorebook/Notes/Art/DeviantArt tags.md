# MAIN


>https://linktr.ee/riseofdissent

>https://www.deviantart.com/kidinnu/gallery/88219573/rise-of-dissent

>AI generated sketch art | Concept for a pen&paper RPG project called `Rise of Dissent` ( https://linktr.ee/riseofdissent )

>hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

>watercoloured sketch art Michael Kirkbride Hugo Pratt Rise Dissent AI generated


### - Literature

>Concept for a pen&paper RPG project called `Rise of Dissent` ( https://linktr.ee/riseofdissent )

>dissent paper pen rise rpg rules ttrpg

>dissent lore paper pen rise rpg ttrpg

***







# LOCATIONS


bustling market fantasy alien assyrian city steampunk architecture hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

fantasy alien assyrian airport city steampunk architecture renaissance airship docking hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

fantasy assyrian temple steampunk architecture alien desert pilgrims hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

fantasy outlandish alien natural landscape weird vegetation hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

viking northern african mudbrick architecture winter setting giant mushrooms hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

***








# CREATURES


alien bug dinosaur creature Morrowind fantasy outlandish natural landscape hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

winged alien bug reptilian dragon creature drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated Rise Dissent AI generated

***







# PEOPLE


fantasy steampunk warrior soldier steampunk armor holding spear hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated Rise Dissent AI generated

fantasy steampunk roman steampunk centurion armor hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated Rise Dissent AI generated

fantasy warrior knight soldier armor riding steampunk creature swamp hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

young fantasy steampunk skinny dwarf woman thin traditional chinese clothing hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated Rise Dissent AI generated

old fantasy steampunk dwarf woman traditional chinese clothing hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated Rise Dissent AI generated

fantasy steampunk warrior dwarf traditional chinese armor clothing hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

fantasy steampunk persian assyrian woman exotic tribal holding staff sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

fantasy renaissance alien mage reading sci-fi book scroll assyrian steampunk tavern hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

fantasy renaissance noble steampunk elven lady hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

crouching renaissance steampunk mech woman holding spear hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

noble fantasy viking zulu man alien reptilian hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

noble fantasy viking zulu lady alien reptilian hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

***








# MACHINES


fantasy chinese steampunk airship hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

fantasy renaissance steampunk mech holding a spear hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

fantasy renaissance steampunk mech holding spear hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated

***








# ITEMS


old steampunk clay tablet inscribed magic cuneiform runes hand drawing sketch black white ink Michael Kirkbride Hugo Pratt Rise Dissent AI generated