# APOCALYPSE WORLD




>*Moves* p.189




## Basic Moves


### - Act Under Fire

You can read “under fire” to mean any kind of serious pressure at all. Call for this move whenever someone does something requiring unusual discipline, resolve, endurance or care.


### - Go Aggro

Going aggro means using violence or the threat of violence to control somebody else’s behavior, without (or before) fighting. If the character has the drop on her enemy, or if the enemy won’t fight back, or if the character is making a show of force but isn’t disposed to really fight, it’s going aggro.


### - Seize by Force

Read “seize something” broadly — a character can seize the upper hand, seize momentum, or even seize the moment — but “by force” is strict. This is a move for when the guns and knives and crowbars are already out on both sides. If she doesn’t choose to take definite hold of it, she still gets her hands on it, but it’s not in her exclusive power. You can (and probably should) try to take it away from her again. If the thing’s abstract, like “the moment,” you’ll need to figure out case-bycase just what it means to have it but not have it definitely.


### - Seduce or Manipulate

Seducing someone, here, means using sex to get them to do what you want, not (or not just) trying to get them to fuck you. Asking someone straight to do something isn’t trying to seduce or manipulate them. To seduce or manipulate, the character needs leverage — sex, or a threat, or a promise, something that the manipulator can really do that the victim really wants or really doesn’t want. Absent leverage, they’re just talking, and you should have your NPCs agree or accede, decline or refuse, according to their own self-interests.


### - Read a Sitch

Reading a situation can mean carefully checking things out, studying and analyzing, or it can mean a quick look over the wall and going by gut. Depends on the character. As MC, sometimes you’ll already know the answers to these and sometimes you won’t. Either way, you do have to commit to the answers when you give them. The +1 is there to make it concrete. Spring sudden unhappy revelations on people every chance you get. That’s the best. A character can’t read the same charged situation more than once.


### - Read a Person

Reading a person is an investment in time. It means studying them carefully through the whole conversation, noticing changes in their tone, the movements of their eyes and hands, their most fleeting expressions. In play, have the player roll this move only (a) when the interaction is genuinely charged, and (b) when you’re going to play the interaction through.


### - Open your Brain

When you open your brain to the world’s psychic maelstrom, roll+weird. On a hit, the MC will tell you something new and interesting about the current situation, and might ask you a question or two; answer them. On a 10+, the MC will give you good detail. On a 7–9, the MC will give you an impression. If you already know all there is to know, the MC will tell you that.


### - Help or Interfere

When you help or interfere with someone who’s making a roll, roll+Hx. On a hit, they take +1 (help) or -2 (interfere) now. On a 7–9, you also expose yourself to fire, danger, retribution or cost. Always ask how! To do it, you’ve got to do it. “I’m helping” is the same kind of unacceptably vague as “I’m going aggro” — you answer them both with “cool, what do you do?”


### - Mutual or Interference

A character’s allowed to interfere with someone making a roll against her. That’s as close as this game gets to opposed rolls. Here’s an example that’s likely to come up: one character pulls out her smg and opens up on another. “Jesus! I dive behind, like, the old tractor, and try to make my way around behind you.”

>MC: “Roll+cool to do something under fire.” “Like I just stand here and wait for you to get away? No way. I chase after you, I jump up on top of the tractor, I rain hell down—” MC: “Roll+Hx to interfere.”




## Peripheral Moves


### - Harm & Healing Moves

>There's no real description for this move but you get the idea


### - Barter Moves

When you go into a holding’s bustling market, looking for some particular thing to buy, and it’s not obvious whether you should be able to just like go buy one like that, roll+sharp. On a 10+, yes, you can just go can buy it like that. On a 7–9, the MC chooses one of the following:

- it costs 1-barter more than you’d expect

- it’s available, but only if you meet with a guy who knows a guy

- damn, I had one, I just sold it to this guy named Rolfball,
maybe you can go get it off him?

- sorry, I don’t have that, but maybe this will do instead?


### - Augury

This move is big crazy fun, and a fantastic opportunity for you to barf forth apocalyptica. It may call for you to make snap decisions about the workings of the world’s psychic maelstrom.


### - Insight

When you use your followers for insight, ask your followers what they think your best course is, and the MC will tell you. If you pursue that course, take +1 to any rolls you make in the pursuit. If you pursue that course, but don’t accomplish your ends, you mark experience.




## Optional Battle Moves


- provide covering fire for someone

- maintain an untenable position or course

- stay the fuck down

- follow through on someone else’s move

- incidental and concentrated fire

***








# BLADES IN THE DARK




## Attune


When you Attune, you open your mind to the ghost field or channel nearby electroplasmic energy through your body. You might communicate with a ghost or understand aspects of spectrology. You could try to perceive beyond sight in order to better understand your situation (but Surveying might be better).


### - GM questions

- How do you open your mind to the ghost field? What does that look like?

- What energy are you attuning to? How are you channeling that energy? What do you hope the energy will do?


### - Controlled

*I Attune to the ghost field to see if there are any magical wards on the door.*

- **Reduced Effect** : You sense a buzzing ward in the ghost field, but you can’t quite tell how far it extends—to the windows and roof, too? Maybe so.

- **Lesser Harm** : There’s a sharp electrical pop, and a spark burns across your cheek. Level 1 harm, “Scorched.”


### - Risky

*I reach into the ghost field for the death echoes of Trennet. He died on this very spot, so I’ll try to catch a vision of it.*

- **Reduced Effect** : You see the faint glimmer of Trennet’s ghost echo, thrashing on the floor with a large figure atop him, bearing down as if choking him. It sort of looks like Ulf Ironborn, but you can’t be sure.

- **Harm** : Trennet’s death agony washes over you. You see Ulf Ironborn’s face and feel his vice-like grip on your throat, crushing the life out of you. Take level 2 harm, “Choked".


### - Desperate

*As the ghost tries to possess me, I smash my hand through the glass over the electric light. I’ll Attune the electricity through myself to fry the ghost!*

- **Serious Complication** : The electrical surges overload the spirit. As it’s driven off, crackling bolts of energy lash out from your hands and eyes, setting the room ablaze.

- **Severe Harm** : The ghost rides the energy directly into your mind. You suffer two types of harm: level 2 “Burned” from the energy, and also level 3 “Possessed.” Which would you like to resist first?




## Finesse


`Blades in the Dark.pdf` p.181




## Hunt


Text




## Prowl


Text




## Skirmish


Text




## Study


Text




## Survey


Text




## Sway


Text




## Tinker


Text




## Wreck


Text

***








# THE BURNING WHEEL




>*Skills* p.15

***








# CHRONICLES OF DARKNESS




>p.31, p.71, p.81




## Attributes


### - Mental attributes

- **Intelligence** : Intelligence is your character’s book smarts and ability to process data. It represents memory, general knowledge, and ability to solve complex and difficult problems. *Memorizing (Intelligence + Composure, instant action)*

- **Wits** : Wits represents quick thinking and improvisation. A character with a high Wits responds quickly to new information and situations. It also represents perception and the ability to notice details and subtle tells. *Perception (Wits + Composure, reflexive action), Catching a dropped object (Wits + Dexterity, instant action)*

- **Resolve** : Resolve represents your character’s patience, concentration, and determination. A high Resolve allows a character to focus despite distractions or discouragement *Meditation (Resolve + Composure, extended action), Staying Awake (Resolve + Stamina, instant action)* 


### - Physical attributes

- **Strength** : Strength is a character’s muscular power and ability to use the force of her body. It is used for a large number of physical tasks, and is important for most applications of violence. *Holding up a heavy object (Strength + Stamina, extended action)*

- **Dexterity** : Dexterity represents hand-eye coordination, agility, and physical speed. A character with a high Dexterity has fast reactions, a good sense of balance, and accurate control of physical force. *Maintain balance (Dexterity + Composure, reflexive action)*

- **Stamina** : Stamina is your character’s general fitness and health. It is used for sustained effort, as well as determining how much physical punishment your body can take. *Resisting Interrogation (Stamina + Resolve, reflexive action)*


### - Social attributes

- **Presence** : Presence is a character’s raw charisma, assertiveness, and ability to command. Characters with a high Presence dominate a room and are adept at changing people’s thoughts and moods. *Air of authority (Presence + Intelligence, instant action)*

- **Manipulation** : Manipulation represents your character’s ability to choose his words, mask his intentions, and convince others to go along with his ideas. *Poker Face (Manipulation + Composure, reflexive action)*

- **Composure** : Composure is a character’s ability to keep control of her emotions and resist others’ manipulations. It also represents poise, dignity, and the ability to keep a level head when under fire (metaphorically or literally). *Perception (Wits + Composure, reflexive action*




## Skills


### - Physical skills

- Athletics - sports and movement
- Brawl - unarmed combat
- Drive - operating automobiles under rough conditions
- Firearms - identifying, using, and maintaining shooting weapons.
- Larceny - picking locks, burglary, bypassing security, sleight of hand, etc
- Stealth - moving silently and unnoticed
- Survival - enduring harsh environments
- Weaponry - identifying, using, and maintaining non-firearm weapons


### - Mental skills

- Academics - knowledge in Arts and Humanities
- Computer - computer programs and operating systems
- Crafts - constructing, crafting, and repairing objects
- Investigation - solving riddles and finding evidence
- Medicine - physiology, anatomy, and medical treatments
- Occult - lore about the supernatural
- Politics - political processes and bureaucratic maneuvers
- Science - physical and natural sciences


### - Social skills

- Animal Ken - understanding animal minds and behaviors
- Empathy - observing emotions and understanding viewpoints
- Expression - art of communication and entertainment
- Intimidation - coercing via force and threat
- Persuasion - convincing others and inspiring their emotions
- Socialize - social interaction in various situations
- Streetwise - navigating urban streets and gathering intel there
- Subterfuge - deceiving others and noticing it




## Instant Actions


A task that takes place within a single turn. A character can perform only one instant action per turn, unless he has a Merit or power that lets him do otherwise.




## Reflexive Actions


An instinctual task that takes no appreciable time, such as reacting to surprise or noticing something out of the corner of your eye. Performing a reflexive action does not prevent a character from performing another action within a turn. 




## Extended Actions


An extended action is an attempt to complete a complex task. You roll your dice pool multiple times. Each roll takes a certain amount of time, and represents a step in the process — your character either makes significant progress or faces a setback.




## Contested Actions


When two characters are competing, and you only need to know who comes out on top, use a contested action. In a contested action, each player rolls a pool for their character (such as an Attribute + Skill pool), and the character for whom the most successes are rolled wins the contest. Here are some sample ways you can apply your Skills. Remember, you can invent your own at any time.


### - Argument

*Intelligence + Expression vs. victim’s Resolve*

You try to sway someone with a rational argument. (If arguing with a crowd, use the highest Resolve in the crowd.) (See also Social Maneuvering, p.81.)

- *Dramatic Failure* : You convince them of quite the opposite.
- *Failure* : They listen, but are ultimately unaffected.
- *Success* : They accept the truth (or apparent truth) of your words.
- *Exceptional Success* : They’re convinced, and become a recruit to your point of view, though they might change their minds if they find themselves at risk.


### - Fast-talk

*Manipulation + Subterfuge vs. victim’s Composure*

You may not be able to win the argument with facts, but you can try to get out of trouble with a little judicious spin.

- *Dramatic Failure* : The other party has a good idea what the truth is.
- *Failure* : The other party doesn’t believe you.
- *Success* : The other party swallows your story.
- *Exceptional Success* : The other party believes you so thoroughly that they’re even willing to offer a little aid… though they won’t put themselves at any kind of risk.


### - Interrogation

*Manipulation + Empathy or Intimidation vs. victim’s Resolve*

You try to dig secrets out of a reluctant informant. (See also Social Maneuvering, p. 81.)

- *Dramatic Failure* : The informant is so alienated or injured that he will no longer reveal information.
- *Failure* : The informant blabs a mix of truth and falsehood — even he may not know the difference.
- *Success* : You get the information you were looking for.
- *Exceptional Success* : You get the information you were looking for, and the informant is willing to continue cooperating.


### - Intimidation

*Strength or Manipulation + Intimidation vs. victim’s Composure*

You try to get someone to do what you want by making them afraid of you.

- *Dramatic Failure* : They don’t take you seriously, even if you knock them around a bit. They won’t be doing what you want.
- *Failure* : They’re unimpressed with your threats.
- *Success* : They’re coerced into helping you.
- *Exceptional Success* : They develop a lasting fear of you, which could make them easier to coerce in the future.


### - Shadowing a mark

*Wits + Stealth or Drive vs. victim's Wits + Composure*

You follow someone, perhaps in the hopes of ambushing them, or of finding out their destination.

- *Dramatic Failure* : You’re caught, either by the mark or some observer that’s become suspicious of you.
- *Failure* : The mark senses he’s being followed, and manages to lose you.
- *Success* : You follow the mark to his destination.
- *Exceptional Success* : You find some means by which you can continue following the mark, such as an unlocked entrance into the building at which he arrived.


### - Sneaking

*Dexterity + Stealth vs. victim's Wits + Composure*

You’re trying to avoid notice by someone… or multiple someones. Maybe you want to get into a place undetected. Maybe you’re trying to break out.

- *Dramatic Failure* : You attract a lot of attention… enough that now it’s going to be hard to get out.
- *Failure* : You’re noticed, but still have the chance to slip away.
- *Success* : You avoid notice and get closer to your goal.
- *Exceptional Success* :  You avoid notice and get away before anyone has another chance to catch you.

***








# DUNGEON WORLD




>*Moves* p.56

***








# EXALTED




>*Social Actions* p.214

- **Lore Facts** : Lore knowledge mechanic. "Lore facts" can be introduced or "challenged" by characters ("once a fact about the setting has been introduced, it becomes concrete") p.238

***








# FATE




- Four types of actions: *overcome*, *create an advantage*, *attack* and *defend* (and explanations for critical failures or successes for each) p.142

***








# MONSTERHEARTS




- List of action types with strong emphasis on social actions p.18, p.54

***








# URBAN SHADOWS




>*Moves* p.39, *Faction moves* p.61, *Session moves* p.78, *Moves upon moves* p.205

***