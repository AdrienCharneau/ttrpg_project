# General




>https://en.wikipedia.org/wiki/History_of_Algeria

>https://en.wikipedia.org/wiki/French_Algeria

>https://en.wikipedia.org/wiki/French_conquest_of_Algeria

>https://en.wikipedia.org/wiki/S%C3%A9tif_and_Guelma_massacre

>https://en.wikipedia.org/wiki/1934_Constantine_riots

>https://en.wikipedia.org/wiki/Algiers_putsch_of_1961

>https://fr.wikipedia.org/wiki/Exode_des_pieds-noirs

***








# Sources




Text

***








# Geography-Locations




- [Map 1](https://ontheworldmap.com/algeria/large-detailed-road-map-of-algeria.jpg)

- [Map 2](https://www.guideoftheworld.com/wp-content/uploads/2021/01/physical_road_map_of_algeria.jpg)

>https://en.wikipedia.org/wiki/Geography_of_Algeria

>https://en.wikipedia.org/wiki/Barbary_Coast

>https://en.wikipedia.org/wiki/Kabylia




## Landmarks


Text




## Cities


>https://en.wikipedia.org/wiki/List_of_cities_in_Algeria

- **Algiers** :

>https://en.wikipedia.org/wiki/Algiers

- **Oran** :

>https://en.wikipedia.org/wiki/Oran

- **Constantine** :

>https://en.wikipedia.org/wiki/Constantine,_Algeria

- **Sétif** :

>https://en.wikipedia.org/wiki/S%C3%A9tif

***








# Entities-Factions




>https://en.wikipedia.org/wiki/Kingdom_of_Beni_Abbas

>https://en.wikipedia.org/wiki/Kel_Ahaggar



## France


>https://en.wikipedia.org/wiki/List_of_French_governors_of_Algeria

- **Charles X** :

>https://en.wikipedia.org/wiki/Charles_X_of_France

- **Louis-Auguste-Victor** :

>https://en.wikipedia.org/wiki/Louis-Auguste-Victor,_Count_de_Ghaisnes_de_Bourmont

- **Guy-Victor Duperré** :

>https://en.wikipedia.org/wiki/Guy-Victor_Duperr%C3%A9




## Ottoman Empire


- **Mahmud II** :

>https://en.wikipedia.org/wiki/Mahmud_II


### - Regency of Algiers

>https://en.wikipedia.org/wiki/Regency_of_Algiers

>https://en.wikipedia.org/wiki/List_of_governors_and_rulers_of_the_Regency_of_Algiers

- **Aruj Barbarossa** :

>https://en.wikipedia.org/wiki/Aruj_Barbarossa

- **Hayreddin Barbarossa** :

>https://en.wikipedia.org/wiki/Hayreddin_Barbarossa




## Algerian Resistance


>https://en.wikipedia.org/wiki/Emirate_of_Abdelkader

>https://en.wikipedia.org/wiki/Young_Algerians

- **Emir Abdelkader** :

>https://en.wikipedia.org/wiki/Emir_Abdelkader

- **Messali Hadj** :

>https://en.wikipedia.org/wiki/Messali_Hadj

- **Ferhat Abbas** :

>https://en.wikipedia.org/wiki/Ferhat_Abbas

- **Mohamed Boudiaf** :

>https://en.wikipedia.org/wiki/Mohamed_Boudiaf

***








# Commerce-Society



## Cultures-Ethnicities


>https://en.wikipedia.org/wiki/Kabyle_people




## Personalities


- **Charles Louis Alphonse Laveran** :

>https://en.wikipedia.org/wiki/Charles_Louis_Alphonse_Laveran

- **Mohammed Arkoun** :

>https://fr.wikipedia.org/wiki/Mohammed_Arkoun

***








# Timeline




Text

***
