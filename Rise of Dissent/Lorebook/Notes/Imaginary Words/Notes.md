# LOCATIONS/CULTURES




## The Empire


- **Name** = Rhydian, Thïryan, Ríshlí, Ardashir, Assyrium

- Kuniens/Kunyens? capitale: Kun (empire marin, moines spécialisés dans l'arithmétologie du vent pour mieux naviguer?)


### - Prompts

>Generate a list of 50 random imaginary words that sound like a blend of Latin and Old English

>Generate a list of 50 random imaginary words that sound like a blend of Gaelic and Avestan


### - Regions

- **Names** = Ingrisil, Ardóir, Whinmoyr, Ahuravar, Künya, Fálbor, Aryanáth, Órga, Corvelia, Firinneach, Ardóchas, Ardavach


### - Cities/Towns

- **Names** = Bálora, Tïhras, Solgard, Sveshr, Nuáll, Lorga, Achaemenar, Mithrakhan, Mithravar, Aithríoch, Rúnach, Firúth, Lúdraí, Caeliber, Síodán, Siorvágh, Ríspahr, Críostár, Crúinnar, Síorghrá, Fíreann, Azvh, Ēhras


### - Culture

- **Official Imperial Symbol** = `ƕ`, `Ɣ`, `Ō̱`, `Ǫ́`, `Ƕ`, `Ē`, `Ð`

- **Languages/Dialects** = Ardashian, Críosyán, Kunyan, Gaisgeir, Orlactan, Bistokhan

- **Food/Drinks** = Qalipre, Gráinneach

- **Events/Festivals** = Roshaidh, Solasáilte




## The Province


### - Prompts

>Generate a list of 50 random imaginary words that sound like a blend of Somali and Bulgarian

>Generate a list of 50 random imaginary words that sound like a blend of sounds and syllables that can be found in the Sanskrit and Khalkha Mongolian vocabularies


### - Regions

- **Names** = Qrashwam, Manahr, Thü-uk, Kalapal, Blyapal, Ahurashan, Dirliqsa, Swariksha, Dheeksha


### - Cities/Towns

- **Names** = Ujr, Qalpal, Qrashwar, Hithifaa, Öhras, Dhiipal, Samumaya, Jyāeta


### - Places

- **Famous Mountain** = Khuramja


### - Culture

- **Languages/Dialects** = Tuksī, Samarthi, Tahü-ksī

- **Food/Drinks** = Qalayot (soup), Naznyë (bread), Chithikā, Kalijwal, Prasaglam

- **Money/Currency** = Eth, Khandram

- **Events/Festivals** = Sapumara, Samupalli, Kalijwal


### - Vocabulary

- **Land/Homeland** = Praētaä

- **People of Yyē** = Öhryyē, Yyēnite (imperial translation)

- **King/Leader** = Kraäl, Wöder

- **King/Warlord** = Khaal

- **Queen/Spouse** = Ghoryka, Wyal

- **Prostitute** = Wöon

***








# MAGIC




- **Science (magic/arcana)** = Arcanium? Arcanora? Auranthia? Mythosine?

- **Scientist (spellcaster/mage)** = Arithmystic? Arithmist? Arithmyst? Empyrealis?

- **Mage classes/types** = Zephyralith?

- **Name of the esoteric language** = Arithmythium? Arithmilian? Aetherium? Ignium? Sylvaris? Crysallis? Aquilonis?

- https://en.wikipedia.org/wiki/Transitus_Fluvii

- **Algorithm** = Calcitron? Calcisurge? Algorithmium? Quantarith?

- **Rune types** = Quatatonic, Epylleïptic, Tetrahadric

- **Mages Guild** =

- **Magic technology** = glyphomancy, Runecrafting, technomancy

- **Magic beacon** = sigil stone

- **Spell Names** = Astrafyre, Astrafall, Solareon, Lunariel, Pyroscryph, Fiaraham, Imríomh

***








# RELIGION/SPIRITUALITY




## The Empire


- **Angel/Demigod** = Aiséirí

- **Demon** = Dúthúil




## The Province


- **Faith** = Dyanae ("Dyanist"), Dyana-thë

- **Sacred/Divine** = Shaēb

- **Sacred Law** = Ëdnya

- **Profane Law** = Veätya

- **God** = Shaēb-Ujr

- **Godess** = Shandrika

- **Angel/Good Spirit** = Aän-ehlil

- **Prayer** = Äht

- **Temple** = Qäader

- **Demon/Spectre** = Aän-Turxan, Tür-Bek, Wöqorka

***








# PEOPLE/FIGURES




## The Empire


- **Famous Kings** = Emperor Tholomon 

- **Famous Queens** = Luciodora, Zenithra

- **Famous Heroes/Saints** = Lorgoir, Iomlanach




## The Province


- **King Orghô** *(son of dragon)* = Orghô Aht-Räzam

***