# General

***




The Mithridatic Wars were three conflicts fought by Rome against the Kingdom of Pontus and its allies between 88 – 63 BC.




## Pointers

***








# Sources

***




- [History of Rome](https://en.wikipedia.org/wiki/History_of_Rome_(Livy)) by [Livy](https://en.wikipedia.org/wiki/Livy)




## Wikipedia


- [Asiatic Vespers](https://en.wikipedia.org/wiki/Asiatic_Vespers)

- [Mithridatic Wars](https://en.wikipedia.org/wiki/Mithridatic_Wars)

- [Parthian Empire](https://en.wikipedia.org/wiki/Parthian_Empire)

***








# Cartography

***




- [Map 1](https://wikiwandv2-19431.kxcdn.com/_next/image?url=https:%2F%2Fupload.wikimedia.org%2Fwikipedia%2Fcommons%2Fthumb%2F5%2F58%2F1stMithritadicwar89BC.svg%2F1500px-1stMithritadicwar89BC.svg.png&w=1920&q=50)

- [Map 2](https://i.pinimg.com/originals/94/ce/bf/94cebf642a461b5748905be8b9076db9.png)

<!-- https://en.wikipedia.org/wiki/Geography_of_Turkey -->

<!-- https://en.wikipedia.org/wiki/Anatolia -->

<!-- https://en.wikipedia.org/wiki/Asia_(Roman_province) -->

<!-- https://en.wikipedia.org/wiki/Aegean_Region -->

<!-- https://en.wikipedia.org/wiki/Phrygia -->

<!-- https://en.wikipedia.org/wiki/Galatia -->

<!-- https://en.wikipedia.org/wiki/Bithynia -->

<!-- https://en.wikipedia.org/wiki/Bithynia_and_Pontus -->

<!-- https://en.wikipedia.org/wiki/Pontus_(region) -->

<!-- https://en.wikipedia.org/wiki/Black_Sea_Region -->




## Environment


#### Anatolian Plateau

<!-- https://en.wikipedia.org/wiki/Anatolian_Plateau -->

#### Pontic–Caspian steppe

<!-- https://en.wikipedia.org/wiki/Pontic%E2%80%93Caspian_steppe -->


### - Oceans-Seas

#### Aegean Sea

<!-- https://en.wikipedia.org/wiki/Aegean_Sea -->

#### Bosporus Strait

<!-- https://en.wikipedia.org/wiki/Bosporus -->

#### Black Sea

<!-- https://en.wikipedia.org/wiki/Black_Sea -->

#### Mediterranean Sea

<!-- https://en.wikipedia.org/wiki/Mediterranean_Sea -->


### - Mountains

#### Taurus Mountains

#### Mount Erciyes


### - Rivers-Lakes

<!-- https://en.wikipedia.org/wiki/List_of_rivers_of_Turkey#Ancient -->

#### Euphrates

<!-- https://en.wikipedia.org/wiki/Euphrates -->

#### Tigris

<!-- https://en.wikipedia.org/wiki/Tigris -->

#### Lake Egirdir

<!-- https://en.wikipedia.org/wiki/Lake_E%C4%9Firdir -->




## Cities


<!-- https://vividmaps.com/roman-empire-cities/ -->


- **Ephesus** :

<!-- https://en.wikipedia.org/wiki/Ephesus -->

- **Sinope** :

<!-- https://en.wikipedia.org/wiki/Sinop,_Turkey -->

- **Amasya** :

<!-- https://en.wikipedia.org/wiki/Amasya -->

- **Nicomedia** :

<!-- https://en.wikipedia.org/wiki/Nicomedia -->

- **Pergamon** :

<!-- https://en.wikipedia.org/wiki/Pergamon -->

- **Ilion (Troy)** :

<!-- https://en.wikipedia.org/wiki/Troy -->

- **Miletus** :

<!-- https://en.wikipedia.org/wiki/Miletus -->

- **Sardis** :

<!-- https://en.wikipedia.org/wiki/Sardis -->

- **Halicarnassus** :

<!-- https://en.wikipedia.org/wiki/Halicarnassus -->

- **Side** :

<!-- https://en.wikipedia.org/wiki/Side,_Turkey -->

- **Tarsus** :

<!-- https://en.wikipedia.org/wiki/Tarsus,_Mersin -->

- **Gordion** :

<!-- https://en.wikipedia.org/wiki/Gordion -->

- **Rhodes** :

<!-- https://en.wikipedia.org/wiki/Rhodes -->

- **Antioch** :

<!-- https://en.wikipedia.org/wiki/Antioch -->

- **Amorium** :

<!-- https://en.wikipedia.org/wiki/Amorium -->

***








# Politics-Military

***




## Roman Republic


<!-- https://en.wikipedia.org/wiki/Roman_Republic -->


### - Personalities

#### Sulla

<!-- https://en.wikipedia.org/wiki/Sulla -->

#### Lucullus

<!-- https://en.wikipedia.org/wiki/Lucullus -->

#### Pompey

<!-- https://en.wikipedia.org/wiki/Pompey -->




## Parthian Empire


The Parthian Empire, also known as the Arsacid Empire, was a major Iranian political and cultural power centered in ancient Iran from 247 BC to 224 AD. At its height, it stretched from the northern reaches of the Euphrates, in what is now central-eastern Turkey, to present-day Afghanistan and western Pakistan. The empire, located on the Silk Road trade route between the Roman Empire in the Mediterranean Basin and the Han dynasty of China, became a center of trade and commerce.

The Arsacid rulers were titled the "King of Kings", as a claim to be the heirs to the Achaemenid Empire; indeed, they accepted many local kings as vassals where the Achaemenids would have had centrally appointed, albeit largely autonomous, satraps. The court did appoint a small number of satraps, largely outside Iran, but these satrapies were smaller and less powerful than the Achaemenid potentates.

>*A satrap was a governor of the provinces of the ancient Median and Persian Empires and in several of their successors.They served as a viceroy to the king, though with considerable autonomy. The word came to suggest tyranny or ostentatious splendour, and its modern usage is a pejorative and refers to any subordinate or local ruler, usually with unfavourable connotations of corruption.*

The earliest enemies of the Parthians were the Seleucids in the west and the Scythians in the north. However, as Parthia expanded westward, they came into conflict with the Kingdom of Armenia, and eventually the late Roman Republic. Rome and Parthia competed with each other to establish the kings of Armenia as their subordinate clients.

>*Native Parthian sources, written in Parthian, Greek and other languages, are scarce when compared to Sasanian and even earlier Achaemenid sources. Aside from scattered cuneiform tablets, fragmentary ostraca, rock inscriptions, drachma coins, and the chance survival of some parchment documents, much of Parthian history is only known through external sources.*


### - Personalities

#### Sinatruces of Parthia

<!-- https://en.wikipedia.org/wiki/Sinatruces_of_Parthia -->

#### Phraates III

<!-- https://en.wikipedia.org/wiki/Phraates_III -->




## Seleucid Empire


<!-- https://en.wikipedia.org/wiki/Seleucid_Empire -->




## Kingdom of Pontus


<!-- https://en.wikipedia.org/wiki/Kingdom_of_Pontus -->


### - Personalities

#### Mithridates VI Eupator

<!-- https://en.wikipedia.org/wiki/Mithridates_VI_Eupator -->




## Kingdom of Armenia


<!-- https://en.wikipedia.org/wiki/Kingdom_of_Armenia_(antiquity) -->


### - Personalities

#### Tigranes the Great

<!-- https://en.wikipedia.org/wiki/Tigranes_the_Great -->




## Kingdom of Cappadocia


<!-- https://en.wikipedia.org/wiki/Kingdom_of_Cappadocia -->

<!-- https://en.wikipedia.org/wiki/Cappadocia#History -->


### - Personalities

#### Ariobarzanes I

<!-- https://en.wikipedia.org/wiki/Ariobarzanes_I_of_Cappadocia -->




## Other

#### Cilician pirates

<!-- https://en.wikipedia.org/wiki/Cilician_pirates -->

***








# Timeline

***




Text

***
