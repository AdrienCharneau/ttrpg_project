# NOTES/IDEAS




## Aims


### - Gameplay

- I must playtest the game using a basic module/scenario.

- I want to write a small module/scenario based on Roadwarden.

- I need to create basic a combat system to handle some of the wild encounters.

- I need to come up with a satisfying and intuitive shield mechanic for the combat system.


### - Lore

- I must decide if whether or not I'm going to implement different races, as "racial/nature modifiers" could affect how the game plays.

- To see if "racial/nature modifiers" can work using **Condition Modifiers**, I want to check if "disposition modifiers" can work using **Means Modifiers**.

- It seems like "disposition modifiers" synergize pretty well with **Sympathy**, but I need to find a better use for **Respect**.

***








# Financing




- Set up a Patreon (or Kickstarter?) with Tier subscriptions/rewards (create new gmail?)
- Find a "company name" -> Moonloop games?
- Send work to publishers? -> `The One Ring` -> `https://en.wikipedia.org/wiki/Cubicle_7`

>Examples (Kickstarter)
> - https://www.kickstarter.com/discover/advanced?term=pen+%26+paper+rpg
> - https://www.kickstarter.com/projects/scottwegener/home-a-toolkit-for-towns

>Examples (Patreon):
> - https://www.patreon.com/bohgames?l=fr

#### Patreon Tiers

- **Tier 1** (1$/month?): your name in the credits, receive a free pdf copy of the game rules (and lorebook?)

***








# BALLADRY - TAMRIELIC GAZETEER




## Research


- Do research and take notes about the Bitter Coast & Seyda Neen's area

- Don't forget to research in the *Tamriel Rebuilt Handbook* and *Project Tamriel Wiki*

>- https://www.tamriel-rebuilt.org/tr-handbook
>- https://wiki.project-tamriel.com/wiki/Main_Page

- Do research on how "colonial / frontier towns" opperated in the Americas

>https://es.wikipedia.org/wiki/Arquitectura_colonial_espa%C3%B1ola

>https://fr.wikipedia.org/wiki/Fort_Caroline

>https://es.wikipedia.org/wiki/Colonizaci%C3%B3n_espa%C3%B1ola_de_Am%C3%A9rica

>https://en.wikipedia.org/wiki/American_frontier

- Read and write down Tuna's notes on Morrowind:

>https://docs.google.com/document/d/1AA30wQfKJPeDl5ZUvxNKVzSBYNNMEYMD4O7SaIq5XG8

- Watch THLMR : https://www.youtube.com/playlist?list=PLIwkFxfJgtEGqmT2gnJ5uq3EIjfo1IEq1




## Production


- Make a rough map of Seyda Neen (or Seyda Neen's area)

- Write descriptions in the style of tutorialtuna for each location in Seyda Neen's region and showcase it to him (ask him about his guidelines on google docs page formatting)




## Decisions


- Wait for Tuna to take a decision on the Odai river (and the Hlormaren fortress?)

- Is Fort Firemoth already overrun by Grurn? Or is the imperial garrison still operating there (and mining ebony)?

- Let tuna define wind speeds and directions for the region...? (necessary for the Balladry *sailing* rules) + general weather conditions...?




## Balladry rules

***
