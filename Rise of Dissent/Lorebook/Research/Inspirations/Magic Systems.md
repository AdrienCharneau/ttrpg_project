- https://en.wikipedia.org/wiki/Magic_in_fiction
- http://brandonsanderson.com/sandersons-first-law/
- http://tsdcv3.proboards.com/thread/5485/best-magic-systems-uses-fiction

- http://fantasy-faction.com/2011/magic-in-fantasy
- http://fantasy-faction.com/2013/real-magic
- http://fantasy-faction.com/2015/ancient-magic
- http://fantasy-faction.com/2014/alternative-magic-systems




## The Dying Earth (Jack Vance)

In the far future world of the Dying Earth, magicians use spells, but only 100 spells remain to human knowledge.  These spells are complex and very difficult to commit to memory, so a magic user can only carry so many around in his memory at one time, and they are immediately forgotten upon use.  Wizards like Turjan of Miir and Mazirian the Magician, therefore, face the interesting challenge of having to predict what obstacles they might face on any given adventure and memorize the appropriate spells accordingly � and when they use up the ones they�ve remembered for each trip out into the wilderness of the dying earth, they�re out of luck, which makes for entertaining dilemmas.




## The Memory, Sorrow, and Thorn trilogy (Tad Williams)

�The Art,� as it�s called in Osten Ard, is a secretive, scientific ability the use of which is limited to a very select, very educated few � and using it generally causes more problems than it solves.  The story of magic in this trilogy is a cautionary tale, the story of a powerful tool that is too dangerous for any but the most disciplined to study.  And when those who for one reason or another have lost self-control abuse its power, bad things happen that affect not only themselves but the world at large.




## The Earthsea Series (Ursula K. LeGuin)

In Earthsea, everyone and everything has two names: an everyday, descriptive name, and a true name, in the Old Speech, the ancient language of dragons, which, if revealed, provides skilled wizards the ability to control the person or thing so named.  To protect oneself against magic, one must conceal one�s true name at all costs; consequently, divulging your true name to another is the sincerest sign of trust.  The idea that names have power is as old as language, but LeGuin was arguably the first to introduce it to popular fiction.  She was not the last, however.  Christopher Paolini purloined the Earthsea magic system wholesale for his Inheritance series.  Like in Memory, Sorrow, and Thorn, LeGuin�s classic series places a lot of emphasis on the relationship between power and responsibility.




## The Long Price Quartet (Daniel Abraham)

Probably the most original of all the magical systems in this list, in Daniel Abraham�s vaguely Asian-inspired tetralogy, �Poets� can snare gods with verse.  The Poets are sorcerors who, through magical poetic description, can bind to themselves godlike powers called andat.  One, called Seedless, is the personification of a natural force controlling the destruction or removal of that which makes things grow � and as such has the power to cause abortions, or, more usefully, to remove the seeds from cotton with absolutely no labor, allowing it to be sold at a much more competitive price and consequently greatly increasing the economic and political power of Saraykeht, its influential host city-state.




## The Mistborn Trilogy (Brandon Sanderson)

It has not one magic system, but three: allomancy, the magical ability to �burn� ingested metals, granting the allomancer a variety of abilities; feruchemy, the ability to enhance one�s natural abilities with �metalminds�; and hemalurgy, the ability to steal allomantic powers by driving metal through the body of another. 

- https://en.wikipedia.org/wiki/Mistborn_series#Allomancy




## The Wheel of Time




## One Power

- https://en.wikipedia.org/wiki/One_Power