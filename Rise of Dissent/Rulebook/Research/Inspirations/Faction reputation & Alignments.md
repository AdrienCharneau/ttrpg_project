# 13th Age

***




<!-- https://www.13thagesrd.com/ -->

This game uses the concept of "Icons" which are powerful NPCs that are central to the game world. Characters can have positive or negative relationships with these NPCs, and the GM can use these relationships to create interesting situations for the players.

The "Icons" mechanic in 13th Age is a system for representing powerful NPCs (non-player characters) and organizations that have a significant impact on the game world. The Icons are powerful figures such as kings and queens, powerful wizards, ancient dragons, and other beings of great influence and authority.

Each Icon has a set of values, relationships, and goals that the players can interact with, similar to how players interact with their own characters. These values, relationships and goals can be used by the GM to create plot hooks, introduce complications, and add depth to the game world.

Players can also interact with Icons in a variety of ways. They can build relationships with them, gain their favor or earn their enmity. Building a positive relationship with an Icon can provide a player character with access to powerful allies or resources, while earning the enmity of an Icon can make life much more difficult for a character.

The Icons are also divided into three categories: personal, institutional and elemental. The personal Icons represent individuals, the institutional Icons represent organizations and the elemental Icons represent the forces of nature.

At the beginning of the game, each player chooses one Icon that is particularly important to their character. This Icon represents a powerful figure or organization that the character has a personal connection to, and it can be used to create interesting plot hooks and to add depth to the character's backstory.

Players can also gain bonuses on certain rolls by invoking the Icon's name, by making a roll against their relationship with the Icon, or by using the relationship with the Icon to create an advantage.

The Icon mechanic in 13th Age helps create a rich and dynamic game world, in which powerful NPCs and organizations have a significant impact on the game's events and the players' characters' lives. It also adds depth and complexity to the characters, by giving them personal connections to the game world's most powerful figures.

***








# A Song of Ice and Fire Roleplaying - House & Lands

***




>P.97

***








# Blades in the Dark - The Faction Game

***




>Blades in the Dark - core rulebook p.53




## Summary


<!-- https://bladesinthedark.com/faction-game -->

Each notable faction is ranked by Tier—a measure of wealth, influence, and scale. At the highest level are the Tier V and VI factions, the true powers of the city. Your crew begins at Tier 0.

You’ll use your Tier rating to roll dice when you acquire an asset, as well as for any fortune roll for which your crew’s overall power level and influence is the primary trait. Most importantly, your Tier determines the quality level of your items as well as the quality and scale of the gangs your crew employs—and thereby what size of enemy you can expect to handle.

#### Gang scale by tier

- Tier V. Massive gangs. (80 people)
- Tier IV. Huge gangs. (40 people)
- Tier III. Large gangs. (20 people)
- Tier II. Medium gangs. (12 people)
- Tier I. Small gangs. (3-6 people)
- Tier 0. 1 or 2 people


### - Hold

On the faction ladder next to the Tier numbers is a letter indicating the strength of each faction’s hold. Hold represents how well a faction can maintain their current position on the ladder. W indicates weak hold. S indicates strong hold. Your crew begins with strong hold at Tier 0.


### - Development

To move up the ladder and develop your crew, you need rep. Rep is a measure of clout and renown. When you accrue enough rep, the other factions take you more seriously and you attract the support needed to develop and grow.

When you complete a score, your crew earns 2 rep. If the target of the score is higher Tier than your crew, you get +1 rep per Tier higher. If the target of the score is lower Tier, you get -1 rep per Tier lower** (minimum zero).

You need 12 rep to fill the rep tracker on your crew sheet. When you fill the tracker, do one of the following:

- If your hold is weak, it becomes strong. Reset your rep to zero.

- If your hold is strong, you can pay to increase your crew Tier by one. This costs coin equal to your new Tier x 8. As long as your rep tracker is full, you don’t earn new rep (12 is the max). Once you pay and increase your Tier, reset your rep to zero and reduce your hold to weak.


### - Turf

Another way to contribute to the crew’s development is by acquiring turf. When you seize and hold territory, you establish a more stable basis for your rep. Each piece of turf that you claim represents abstracted support for the crew (often a result of the fear you instill in the citizens on that turf).

Turf is marked on your rep tracker (see the example below). Each piece of turf you hold reduces the rep cost to develop by one. So, if you have 2 turf, you need 10 rep to develop. If you have 4 turf, you need 8 rep to develop. You can hold a maximum of 6 turf. When you develop and reset your rep, you keep the marks from all the turf you hold.

>*If you hold 3 pieces of turf, you need only 9 rep to develop, instead of 12.*

>*When you develop, you’ll clear the 9 rep marks, but keep the 3 turf marks. Mark turf on the right side, to show the “cap” on how much rep is needed.*

Also, when you acquire turf, you expand the scope of your crew’s hunting grounds.


### - Reducing hold

You may perform an operation specifically to reduce the hold of another faction, if you know how they’re vulnerable. If the operation succeeds, the target faction loses 1 level of hold. If their hold is weak and it drops, the faction loses 1 Tier and stays weak.

When a faction is at war, it temporarily loses 1 hold.

Your crew can lose hold, too, following the same rules above. If your crew is Tier 0, with weak hold, and you lose hold for any reason, your lair comes under threat by your enemies or by a faction seeking to profit from your misfortune.


### - Faction Status

Your crew’s status with each faction indicates how well you are liked or hated. Status is rated from -3 to +3, with zero (neutral) being the default starting status. You track your status with each faction on the faction sheet.

When you create your crew, you assign some positive and negative status ratings to reflect recent history. The ratings will then change over time based on your actions in play.

#### Faction status changes

When you execute an operation, you gain -1 or -2 status with factions that are hurt by your actions. You may also gain +1 status with a faction that your operation helps. (If you keep your operation completely quiet then your status doesn’t change.) Your status may also change if you do a favor for a faction or if you refuse one of their demands.

#### Faction status levels

- +3: Allies. This faction will help you even if it’s not in their best interest to do so. They expect you to do the same for them.

- +2: Friendly. This faction will help you if it doesn’t create serious problems for them. They expect you to do the same.

- +1: Helpful. This faction will help you if it causes no problems or significant cost for them. They expect the same from you.

- 0: Neutral

- -1: Interfering. This faction will look for opportunities to cause trouble for you (or profit from your misfortune) as long as it causes no problems or significant cost for them. They expect the same from you.

- -2: Hostile. This faction will look for opportunities to hurt you as long as it doesn’t create serious problems for them. They expect you to do the same, and take precautions against you.

- -3: War. This faction will go out of its way to hurt you even if it’s not in their best interest to do so. They expect you to do the same, and take precautions against you. When you’re at war with any number of factions, your crew suffers +1 heat from scores, temporarily loses 1 hold, and PCs get only one downtime action rather than two. You can end a war by eliminating your enemy or by negotiating a mutual agreement to establish a new status rating.

>*If your crew has weak hold when you go to war, the temporary loss of hold causes you to lose one Tier. When the war is over, restore your crew’s Tier back to its pre-war level.*


### - Claims

Each crew sheet has a map of claims available to be seized. The claim map displays a default roadmap for your crew type. Claims should usually be seized in an orderly sequence, by following the paths from the central square, the crew’s lair.

However, you may attempt to seize any claim on your map, ignoring the paths (or even seek out a special claim not on your map) but these operations will always be especially difficult and require exceptional efforts to discover and achieve.

#### Seizing a claim

Every claim is already controlled by a faction. To acquire one for yourself, you have to take it from someone else. To seize a claim, tell the GM which claim on your map your crew intends to capture. The GM will detail the claim with a location and a description and will tell you which faction currently controls that claim. Or the GM might offer you a choice of a few options if they’re available.

If you choose to ignore the roadmap paths when seizing a claim, the GM might tell you that you’ll need to investigate and gather information in order to discover a claim of that type before you can attempt to seize it.

Execute the operation like any other score, and if you succeed, you seize the claim and the targeted faction loses the claim.

Seizing a claim is a serious attack on a faction, usually resulting in -2 faction status with the target, and potentially +1 status with its enemies.

As soon as you seize a claim, you enjoy the listed benefit for as long as you hold the claim. Some claims count as turf. Others provide special benefits to the crew, such as bonus dice in certain circumstances, extra coin generated for the crew’s treasury, or new opportunities for action.

#### Losing a claim

An enemy faction may try to seize a claim that your crew holds. You can fight to defend it, or negotiate a deal with the faction, depending on the situation. If you lose a claim, you lose all the benefits of that claim. If your lair is lost, you lose the benefits of all of your claims until you can restore your lair or establish a new one. To restore or establish a new lair, accomplish a score to do so.




## Tier


Take a look below. Each notable faction of the city is ranked by Tier -a measure of wealth, influence, and scale. At the highest level are the Tier V and VI factions, the true powers of the city. Your crew begins at Tier 0.

You’ll use your Tier rating to roll dice when you acquire an asset, as well as for
any fortune roll for which your crew’s overall power level and influence is the
primary trait. Most importantly, your Tier determines the quality level of your
items as well as the quality and scale of the gangs your crew employs—and
thereby what size of enemy you can expect to handle.

#### Gang Scale by Tier

- **Tier V** : Massive gangs. (80 people)
- **Tier IV** : Huge gangs. (40 people)
- **Tier III** : Large gangs. (20 people)
- **Tier II** : Medium gangs (12 people)
- **Tier I** : Small gangs (3-6 people)
- **Tier 0** : 1 or 2 people




## Hold


On the faction ladder next to the Tier numbers is a letter indicating the strength of each faction’s hold. Hold represents how well a faction can maintain their current position on the ladder. W indicates weak hold. S indicates strong hold. Your crew begins with weak hold.

<!-- Blades in the Dark - core rulebook p.53 -->




## Factions


>Blades in the Dark - core rulebook p.292

Each faction of Duskwall is briefly described below, with detailed entries following for the criminal underworld and the other most significant factions of the city.

- **The Billhooks (ii)** : A tough gang of thugs wielding hatchets and meat hooks.

- **The Brigade (ii)** : The fire-fighters of the city. Beloved for their life-saving heroism, or reviled for their looting and extortion rackets. Also known as “Sallies” (from “salamanders,” their ancient name).

- **Bluecoats (iii)** : The City Watch of Duskwall, tasked with upholding the law. Known as the meanest gang in the city. Corrupt, violent, and cruel.

- **Cabbies (ii)** : The public coach operators. They also breed the large Akorosian goats used to pull the carriages. An impressive gossip network.

- **The Church of the Ecstasy of the Flesh (iv)** : The “state religion,” if there is such a thing. They honor the life of the body and abhor the corrupted spirit world. Essentially a secret society.

- **The Circle of Flame (iii)** : A secret society of antiquarians and scholars; cover for extortion, graft, vice, and murder.

- **Citizenry** : The ordinary citizens of a district can be represented by a faction, if you want to track status with them in the game. The GM will set their Tier according to the wealth and power of the citizens in that district (Whitecrown might be Tier V, The Docks Tier II, Charhollow Tier 0).

- **City Council (v)** : The elite noble rulers of the city government.

- **The Crows (ii)** : An old gang with new leadership. Known for running illegal games of chance and extortion rackets.

- **Cyphers (ii)** : The messenger guild of the city. Cyphers swear sacred oaths of secrecy—never revealing the contents of their messages or the identities of their clients -or so they claim.

- **Deathlands Scavengers (ii)** : Convicts from Ironhook and desperate freelancers who roam the wasteland beyond the lightning barriers.

- **The Dimmer Sisters (ii)** : Housebound recluses with an occult reputation.

- **Dockers (iii)** : The hard-bitten laborers who work the docks.

- **The Fog Hounds (i)** : A crew of rough smugglers looking for a patron.

- **The Forgotten Gods (iii)** : Cults that attempt to follow the old ways from before the cataclysm, doing the bidding of demons and darker things. There are many cults, who rarely organize together. An individual cult is usually Tier I or Tier II.

- **The Foundation (iv)** : The powerful ancient order of architects and builders. Many of their enemies have disappeared behind the brick and mortar of Doskvol.

- **Gondoliers (iii)** : The guild of canal boat operators. Venerated by ancient tradition. Said to know occult secrets (many things are submerged in the Dusk).

- **The Gray Cloaks (ii)** : Former Bluecoats who turned to crime.

- **The Grinders (ii)** : A vicious gang of former dockers and leviathan blood refinery workers from Skovlan.

- **The Hive (iv)** : A guild of merchants who secretly trade in contraband. Named for their symbol, a golden bee.

- **The Horde (iii)** : A mass of hollows, all united in some fell purpose, controlled by an unknown power.

- **Imperial Military (vi)** : The armed forces of the Imperium stationed in Doskvol. Garrisons are posted at Gaddoc Rail Station, aboard the naval destroyer Paragon, and at the Lord Governor's stronghold (about 250 troops in total).

- **Ink Rakes (ii)** : The journalists, muckrakers, and newspaper publishers of Doskvol.

- **Inspectors (iii)** : The criminal investigators of the City Watch. They have a reputation for ethics and integrity (no one likes them). They present evidence for prosecutions to the city magistrates.

- **Ironhook Prison (iv)** : Where many scoundrels spend the bulk of their lives. Several criminal organizations are run by convicts inside its walls.

- **The Lampblacks (ii)** : The former lamp-lighter guild, turned to crime when their services were replaced by electric lights.

<!-- Blades in the Dark - core rulebook p.294 -->

***
