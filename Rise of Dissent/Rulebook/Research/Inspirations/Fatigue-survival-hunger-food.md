# ARS MAGICA

burden, short-term and long-term fatigue p.178

***


# CHIVALRY & SORCERY

Fatigue levels managed through character constitution ('1e Red Book - 7th-print-edtion' p.49, p.59)

***


# CONAN

You have Mental Stress points (like hit points) called Resolve. Once your Resolve is worn down (by displays of violence, superior reputation or skill, threats, horror, supernatural, sorcery etc...), or if something extra shocking happens all at once (say enemy cuts off your buddies head), then you take serious mental harm called Trauma which increases difficulty for all your actions and escalates from there until you're a gibbering wreck in need of counseling, comatose or so mentally destroyed you become an NPC.

When a character takes damage from any source, it is marked off from a particular form of Stress. Physical attacks reduce Vigor, while mental attacks reduce Resolve. If this attack causes 5 or more damage, or reduces Vigor or Resolve to 0, the character suffers a point of Harm. If both events occur, the character suffers two Harms. Harm has different names and effects depending on what caused the Harm. Mental damage inflicts Trauma, which increases the Difficulty of Awareness, Intelligence, Personality, and Willpower tests by 1. Physical damage inflicts Wounds, which increases the Difficulty of Agility, Brawn, and Coordination tests by 1. The effects of these Harms are cumulative.

***


# TORCHBEARER

In the Torchbearer RPG, Hunger Points are a game mechanic used to represent the physical and mental stress that a character experiences as they go without food and water. Characters begin each adventure with a certain number of Hunger Points, and they lose one Hunger Point each day they go without food and water. When a character's Hunger Points reach zero, they are considered to be starving and will start to suffer penalties to their abilities and skill checks. In addition, if a character's Hunger Points reach a negative total, they will begin to take damage and may even die from starvation. In order to regain Hunger Points, a character must find and consume food and water. The rate at which Hunger Points are regained can vary depending on the quality and quantity of the food and water, and some rare or special foods may even grant additional benefits. This mechanic is designed to add a sense of realism and survival to the game, and to encourage players to prioritize finding food and water when they are on adventures in a wilderness setting.

## Copypasta:

https://mythcreants.com/blog/five-rpg-systems-with-downtime-mechanics/

If you’re a Mythcreants regular, you’ve probably realized by now that I play a lot of Torchbearer, and it would be remiss of me not to mention this game’s excellent downtime rules: the town phase. The idea is simple. After adventurers have delved deep beneath the earth to slay foul monsters and recover glittering treasure, they return to town where they rest and sell their loot.

The execution is more complex, but for the most part, it works. PCs build up a “lifestyle cost” as they do things in town, meaning that the more they take on, the more they’ll have to pay at the end. This introduces an interesting set of choices: Should the players make do with lodging in the stables, or should they spring for an inn? The inn is expensive, but they’re pretty banged up from that last dungeon and could use the extra R&R. Further, can they afford the time and money to research their next dungeon, or should they save the coin and risk being unprepared?

The ability to do more than one thing gives the players plenty of control, and it creates natural points where PCs can process their experiences through roleplaying.* After an adventure of constant danger and life-or-death choices, it just feels good to stop and focus on some more logistical problems for a while. Of course this is still Torchbearer, so money is tight, and the townsfolk are mistrustful, but at least nothing in town will try to eat you. Probably.

The town phase feels like a more successful version of what Delta Green’s Home phase was trying to do. Its rules mesh better with the intended mood, and there are fewer mechanical traps to snag an unwary PC. It gives players the opportunity to plan and prepare without overwhelming them with options.

All that said, no system is perfect, and the town phase has a few problems of its own. Most notably, it requires players to make a lot of recovery and purchasing tests. Adventurers need a lot of supplies, and they suffer a lot of injuries,* all of which must be rolled separately. This gets so tiresome that I invented multiple sets of house rules for how to deal with it.

Another problem with the town phase is that it depends on the GM’s being able to properly balance how much treasure the party gets. Too much isn’t a problem, it just means the PCs will be extra prepared for their next adventure. But showing up to town with too little treasure can be a real problem, as the party enters a death spiral of embarking on a new adventure without being recovered from the previous ones. There are ways around this problem, of course – a stranger offering needed coin in exchange for a price is always effective – but it’s something the GM needs to be aware of.

Despite its problems, the town phase works remarkably well and is a major boon to Torchbearer.

***


# GURPS

Fatigue management system using "Fatigue Points" (FP). There's even a short list of actions/tasks that show how much 'FP' they consume + how environmental hazards affect this resource ('GURPS 4e - Lite.pdf' p. 31)

***


# ROLEMASTER

Exhaustion points with penalties and charts ('Rolemaster FRP - CORE Rules - OCR PDF' p.58)