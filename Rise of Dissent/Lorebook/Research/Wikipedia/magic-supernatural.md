# MAGIC




The English words "magic", "mage" and "magician" come from the Latin term *magus*, through the Greek *μάγος*, which is from the Old Persian *maguš*. The Old Persian is derived from the Proto-Indo-European *magh* ("be able"). The Persian term may have led to the Old Sinitic *Mγag* ("mage" or "shaman"). The Old Persian form seems to have permeated ancient Semitic languages as the Talmudic Hebrew *magosh*, the Aramaic *amgusha* ("magician"), and the Chaldean *maghdim* ("wisdom" and "philosophy"). From the first century BCE onwards, Syrian *magusai* gained notoriety as magicians and soothsayers.

- https://en.wikipedia.org/wiki/Magic_(supernatural)
- https://en.wikipedia.org/wiki/History_of_magic




## High & Low Magic


Historians and anthropologists have distinguished between practitioners who engage in *high magic*, and those who engage in *low magic*. *High magic*, also known as *ceremonial magic* or *ritual magic*, is more complex, involving lengthy and detailed rituals as well as sophisticated, sometimes expensive, paraphernalia. *Low magic*, also called *natural magic*, is associated with peasants and folklore and with simpler rituals such as brief, spoken spells. *Low magic* is also closely associated with witchcraft.




## Sympathetic Magic


*Sympathetic magic*, also known as *imitative magic*, is a type of magic based on imitation or correspondence.

>If we analyze the principles of thought on which magic is based, they will probably be found to resolve themselves into two: first, that like produces like, or that an effect resembles its cause; and, second, that things which have once been in contact with each other continue to act on each other at a distance after the physical contact has been severed.

>The former principle may be called the *Law of Similarity*, the latter the *Law of Contact* or *Contagion*. From the first of these principles, namely the *Law of Similarity*, the magician infers that he can produce any effect he desires merely by imitating it: from the second he infers that whatever he does to a material object will affect equally the person with whom the object was once in contact, whether it formed part of his body or not.

- https://en.wikipedia.org/wiki/Sympathetic_magic


### - Imitation

*Imitation* involves using effigies, fetishes or poppets to affect the environment of people, or people themselves. Voodoo dolls are an example of fetishes used in this way: the practitioner uses a lock of hair on the doll to create a link (also known as a "taglock") between the doll and the donor of this lock of hair. In this way, that which happens to the doll will also happen to the person.


### - Correspondence

*Correspondence* is based on the idea that one can influence something based on its relationship or resemblance to another thing. This include beliefs that certain herbs with yellow sap can cure jaundice, that walnuts could strengthen the brain because of the nuts' resemblance to brain, that red beet-juice is good for the blood, that phallic-shaped roots will cure male impotence, etc

- https://en.wikipedia.org/wiki/Doctrine_of_signatures

Many societies have been documented as believing that, instead of requiring an image of an individual, influence can be exerted using something that they have touched or used. Consequently, the inhabitants of Tanna, Vanuatu in the 1970s were cautious when throwing away food or losing a fingernail, as they believed these small scraps of personal items could be used to cast a spell causing fevers.


### - Cave paintings

*Sympathetic magic* has been considered in relation to Paleolithic cave paintings such as those in North Africa and at Lascaux in France. The theory, which is partially based on studies of more modern hunter-gatherer societies, is that the paintings were made by magic practitioners who could potentially be described as shamans.

The shamans would retreat into the darkness of the caves, enter into a trance state and then paint images of their visions, perhaps with some notion of drawing power out of the cave walls themselves. This goes some way towards explaining the remoteness of some of the paintings (which often occur in deep or small caves) and the variety of subject matter (from prey animals to predators and human hand-prints). 




## Mesopotamia


Magic was invoked in many kinds of rituals and medical formulae, and to counteract evil omens. Defensive or legitimate magic in Mesopotamia (*asiputu* or *masmassutu* in the Akkadian language), were incantations and ritual practices intended to alter specific realities.

- https://en.wikipedia.org/wiki/A%C5%A1ipu


### - Incantation bowls

A common set of shared assumptions about the causes of evil and how to avert it are found in a form of early protective magic called *incantation bowls* or *magic bowls*. The bowls were produced in the Middle East, particularly in Upper Mesopotamia and Syria, what is now Iraq and Iran, and fairly popular during the sixth to eighth centuries.

The bowls were buried face down and were meant to capture demons. They were commonly placed under the threshold, courtyards, in the corner of the homes of the recently deceased and in cemeteries. 

- https://en.wikipedia.org/wiki/Incantation_bowl




## Egypt


In ancient Egypt (*Kemet* in the Egyptian language), Magic (personified as the god *heka*) was an integral part of religion and culture which is known to us through a substantial corpus of texts which are products of the Egyptian tradition.

The Coptic term *hik* is the descendant of the pharaonic term *heka*, which, unlike its Coptic counterpart, had no connotation of impiety or illegality, and is attested from the Old Kingdom through to the Roman era. Heka was considered morally neutral and was applied to the practices and beliefs of both foreigners and Egyptians alike.

The interior walls of the pyramid of Unas, the final pharaoh of the Egyptian Fifth Dynasty, are covered in hundreds of magical spells and inscriptions, running from floor to ceiling in vertical columns. These inscriptions are known as the *Pyramid Texts* and they contain spells needed by the pharaoh in order to survive in the afterlife.

The Pyramid Texts were strictly for royalty only; the spells were kept secret from commoners and were written only inside royal tombs. During the chaos and unrest of the First Intermediate Period, however, tomb robbers broke into the pyramids and saw the magical inscriptions. Commoners began learning the spells and, by the beginning of the Middle Kingdom, they began inscribing similar writings on the sides of their own coffins, hoping that doing so would ensure their own survival in the afterlife. These writings are known as the *Coffin Texts*.


### - Amulets

The use of amulets, (*meket*) was widespread among both living and dead ancient Egyptians. They were used for protection and as a means of *"...reaffirming the fundamental fairness of the universe"*. The oldest amulets found are from the predynastic Badarian Period, and they persisted through to Roman times.




## Israel-Judah


In the Mosaic Law, practices such as witchcraft, being a soothsayer, a sorcerer, one who conjures spells or one who calls up the dead are specifically forbidden as abominations to the Lord. Halakha (Jewish religious law) forbids divination and other forms of soothsaying, and the Talmud lists many persistent yet condemned divining practices.


### - Practical Kabbalah

Practical Kabbalah in historical Judaism, is a branch of the Jewish mystical tradition that concerns the use of magic. It was considered permitted white magic by its practitioners, reserved for the elite, who could separate its spiritual source from *Qliphoth* (realms of evil) if performed under circumstances that were holy and pure. The concern of overstepping Judaism's strong prohibitions of impure magic ensured it remained a minor tradition in Jewish history.

- https://en.wikipedia.org/wiki/Practical_Kabbalah


### - Incantation bowls and papyri

A subcategory of Mesopotamian incantation bowls are those used in Jewish magical practice. Aramaic incantation bowls are an important source of knowledge about Jewish magical practices.

- https://en.wikipedia.org/wiki/Jewish_magical_papyri


### - Sefer-ha-Razim

The idea that magic was devised, taught, and worked by demons would have seemed reasonable to anyone who read the Greek magical papyri or the *Sefer-ha-Razim* and found that healing magic appeared alongside rituals for killing people, gaining wealth, or personal advantage, and coercing women into sexual submission.

- https://en.wikipedia.org/wiki/Sefer_HaRazim




## Islamic World


The Islamic reaction towards magic did not condemn magic in general and distinguished between magic which can heal sickness and possession, and sorcery. The former is therefore a special gift from God, while the latter is achieved through help of *Jinn* and devils. Ibn al-Nadim held that exorcists gain their power by their obedience to God, while sorcerers please the devils by acts of disobedience and sacrifices and they in return do him a favor. According to Ibn Arabi, Al-Ḥajjāj ibn Yusuf al-Shubarbuli was able to walk on water due to his piety. According to the Quran 2:102, magic was also taught to humans by devils and the fallen angels *Harut* and *Marut*.




## Western World


### - Ancient Greece

*Katadesmoi* (Latin: *defixiones*), curses inscribed on wax or lead tablets and buried underground, were frequently executed by all strata of Greek society, sometimes to protect the entire polis. Communal curses carried out in public declined after the Greek classical period, but private curses remained common throughout antiquity.

A large number of magical papyri, in Greek, Coptic, and Demotic, have been recovered and translated. They contain early instances of:

- the use of magic words said to have the power to command spirits
- the use of mysterious symbols or sigils which are thought to be useful when invoking or evoking spirits

The practice of magic was banned in the late Roman world and during the medieval period. Divination, interpretation of omens, sorcery and use of charms were specifically forbidden.


### - Key of Solomon

Medieval Europe saw magic come to be associated with the Old Testament figure of Solomon; various grimoires, or books outlining magical practices, were written that claimed to have been written by Solomon, most notably the *Key of Solomon*.

The corpus is divided into two books. It describes the necessary drawings to prepare each *experiment* or, in more modern language, magical operations. Elaborate preparations are necessary, and each of the numerous items used in the operator's *experiments* must be constructed of the appropriate materials obtained in the prescribed manner, at the appropriate astrological time, marked with a specific set of magical symbols, and blessed with its own specific words. All substances needed for the magic drawings and amulets are detailed, as well as the means to purify and prepare them. Many of the symbols incorporate the *Transitus Fluvii* occult alphabet.

- **Book I** : contains conjurations, invocations, and curses to summon and constrain spirits of the dead and demons in order to compel them to do the operator's will. It also describes how to find stolen items, become invisible, gain favour and love, and so on.

- **Book II** : describes various purifications which the operator (termed *exorcist*) should undergo, how they should clothe themselves, how the magical implements used in their operations should be constructed, and what animal sacrifices should be made to the spirits.

>*Transitus Fluvii* ("passing through the river" in Latin) or *Passage Du Fleuve* (in French) is an occult alphabet consisting of 22 characters described by Heinrich Cornelius Agrippa in his Third Book of *Occult Philosophy* (Cologne, 1533, but written around 1510). It is derived from the Hebrew alphabet and is similar to the *Celestial* and *Malachim* alphabets. The name may refer to the crossing of the Euphrates by the Jews on their return from Babylon to rebuild the Temple.

- https://en.wikipedia.org/wiki/Key_of_Solomon
- https://en.wikipedia.org/wiki/Transitus_Fluvii
- https://en.wikipedia.org/wiki/Malachim
- https://en.wikipedia.org/wiki/Celestial_Alphabet


### - Chaos Magic

*Chaos magic*, also spelled *chaos magick*, is a modern tradition of magic. Emerging in England in the 1970s as part of the wider neo-pagan and esotericist subculture, it drew heavily from the occult beliefs of artist Austin Osman Spare, expressed several decades earlier.

The central defining tenet of *chaos magic* is arguably the idea that belief is a tool for achieving effects. In *chaos magic*, complex symbol systems like Qabalah, the Enochian system, astrology or the I Ching are treated as maps or "*symbolic and linguistic constructs*" that can be manipulated to achieve certain ends but that have no absolute or objective truth value in themselves.

- https://en.wikipedia.org/wiki/Chaos_magic

***