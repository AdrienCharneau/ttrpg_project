# GENERAL BRAINSTORMING




- Realistic setting where everything works like in our universe (mathematics, physics, chemistry, biology) but the cultures and societies are completely made up. Maybe different/alien creatures too?

- Realistic setting where everything works like in our universe (mathematics, physics, chemistry, biology) but the cultures and societies are completely made up. Maybe different/alien creatures too?

- Gravity is inverted in this world. Over time, living creatures have adapted to this change by developing certain biological abilities. For example, just as terrestrial animals and humans on Earth are able to stand on the ground, in this inverted world, animals have evolved to maintain their footing. The same way as birds have learned to fly, which is equivalent to "overcoming gravity" in our world.

- The history of [Game Name] takes place in the universe of [X], specifically on the planet [Y]. The period during which the adventure unfolds is chosen by the players themselves, influencing various aspects such as technological advancements, political events, or the wealth/power of certain factions.