# Arithmysticism

***




Arcana (or magic) represents the fundamental constituents, motions and behavior of entities, energies and natural forces present in the world of Rise of Dissent. Arcanism (the study of arcana) is the method used to reveal knowledge about it and arithmysticism (spellcasting) is the utilization of that knowledge for practical purposes.

Arithmysticism is used as a way to bend reality or transform environments, and can only replicate natural phenomenons that are found or studied within the world of Rise of Dissent. For instance, if a character wants to start a fire with a spell, they still need to find combustible material such as wood or oil for it work.

>As long as arcanists have not discovered a way to perform some unconventional or outlandish effect, magic cannot be used to do things like *summoning spirits*, *reading minds* or *resurrecting the dead*.

#### Arithmyum

Arcana is channeled by mastering the intricacies of an ancient non-spoken esoteric language called arithmyum. Understanding it enables arcanists to invoke calciphants (magic algorithms) with their thoughts.

>By mentally projecting the appropriate patterns using arithmyum, arcanists manifest specific spells or arcanic effects directly into reality.




## Calciphants


Calciphants work like magic formulas, but should not be seen as ready-made spells or on-command effects. They should be interpreted as *blueprints* for magical occurrences that must be tailored or arranged to specific situations. When casting arcana, users need to carefully consider, adapt, and modify a unique calciphant that they've learned (or that they're reading from a tablet) to invoke its effect into a given circumstance.

>A calciphant that works as intended is considered *arithmystically sound*, whereas a flawed or illogic calciphant that produces incorrect results is deemed *dissonant*.

Arithmysticism rarely involves instantaneous spells and demands focus and preparation to achieve desired outcomes. It is a deliberate process that requires users to analyze a situation and determine the most suitable approach for casting a spell.

>For example, if a caster intends to ignite a piece of wood, they must take into account factors such as the size of the object, its weight and the strength of the wind in order to adapt their burning calciphant accordingly. This is why direct combat spells, relying on swift reflexes and reactions, barely exist in Rise of Dissent.

Elementary arithmystic practices are for the most part well understood by the general population. Many people can perform basic calciphants like *extracting water from mud* or *levitating a piece of paper*, the same way they would learn a cooking recipe. On the other hand, learning advanced calciphants requires much more knowledge and training.

#### Equipment

The ability to create or replicate calciphants necessitates specific equipment such as vessels, bottles or oils. In this context, arcanists carry flasks and potions specifically for this purpose.




## Cultural ramifications


*Arithmyum* can be transcribed into spoken words or written tablets, assisting *arcanists* in sharing their knowledge. Additionally, written *arithmyum* relieves them from the need to memorize specific *calciphants*. However, each civilization has its own unique way of representing *arithmyum* through distinct words, patterns, runes and symbols. Consequently, an *arcanist* from one culture may not be able to read and comprehend tablets and *calciphants* from another one, even though they're spoken or written in the same "universal" language.

>Appart from tablets, I must introduce a "papyrus-like" scroll (I need to make up an imaginary word for the material, not just use "papyrus"). It should have less powerful calciphants than what can be found on tablets but much easier to carry.

Because of this barrier, some cultures may have discovered functional *calciphants* that remain unknown to others. For instance, in most ancient traditions, the use of *arcana* is directly tied to religion and divinity. In these societies, *arithmysticism* is seen as a way to communicate with sacred forces and perceived as a hidden form of knowledge. Therefore the concepts and intricacies found in these *arithmystic* systems are usually not shared with the outside world. This is why *arcanic* effects that are not well known, studied or understood by experts (and the current *arcanic establishment*), are refered to as "sorcery" or "witchcraft".

Finally, another form of "sorcery" or "miracle" (although exceedingly rare) can be found when individuals conceive their own *calciphants* with unprecedented *arcanic* effects but refrain from documenting or sharing them with others. This could be due to them being mute or not knowing how to write. As *arithmyum* is directly solicited from a caster's mind, this makes it look like such individuals are gifted with supernatural abilities and are able to naturally perform feats that are thought impossible or never seen before by most *arcanic* experts.

#### Calciphant Degenerescence

There is a poorly understood phenomenon in *arcanism* where *calciphants*, once functional, gradually lose their effectiveness over long periods of time. This decline is likely attributed to the shifting positions of stars and constellations through the ages.

This process elucidates why old *arithmystic* systems have in their repertoires *calciphants* that lost their use. Mythical spells like "*reviving the dead*" or "*fortune telling*" are well-known *arcanic* effects that have appeared in multiple ancient traditions. Whether these truly functioned during their inception or were just whims of charlatans remains unclear. Nonetheless, the phenomenon of *degeneration* has been observed and studied by *arcanists*, despite the absence of a definitive explanation for its occurrence.




## Artifism


In recent times, advancements in *arcanism* have revolutionized the integration and application of *calciphants* directly into artifacts and machines, resulting in a streamlined process for casting *arcana*. This approach (called *artifism*) treats *arithmyum* as akin to computer code. Runes are inscribed directly onto contraptions, empowering them with specific *arcanic* functionalities and automating the tedious process of having to prepare and ponder *calciphants* before casting a spell.

While *arcanists* still need to master the fundamental principles of *arithmyum*, their capacity in performing *arithmysticism* has been significantly enhanced thanks to this progress. However, they've also become too reliant on it, and this can limit their abilities in certain situations. For instance, certain *artifistic* devices necessitate a caster's proximity to a "magic beacon" (an infrastructure provided by either a guild or a government), for their *arcanic* effects to function as intended.

#### Airships & Mech riding

*Artifism* has allowed the development of powerful technology like *airships*, *mechs* and "mechanical riding creatures" (they work like horses but are upkeeped sort of like motorbikes - with garages, technicians and so on). These technologies have become a key feature in modern warfare.

***








# Geography

***




## Sunfire Drylands


The Sunfire Drylands is an expansive, unforgiving desert, dominated by the relentless heat of the five suns. This inhospitable land is a maze of vast, arid plains and rugged hills, where the earth is cracked and life clings tenaciously to survival. The ground is littered with jagged rock formations, sun-bleached bones and sparse, heat-resistant flora and scrub bushes that defy the scorching sun.

<!-- A rough scorching environment, stretching endlessly under the heat of the five suns. This hot terrain is characterized by vast stretches of arid plains covered in low, resilient vegetation adapted to thrive in the intense heat. Under the sun-scorched surface, lies a complex network of subterranean tunnels that provide shelter for small creatures. These tunnels allow them to stay active and shielded from the searing temperatures found above. During the colder parts of the year, a multitude of unique species travel to this land to nest, adding a fleeting burst of life and activity to this otherwise desolated landscape. -->

<!-- The Drylands are sparsely populated, with few permanent settlements. Nomadic tribes, known for their resilience and resourcefulness, traverse the region, following the migratory patterns of wildlife and the scarce water sources. Occasionally, small outposts and trade posts can be found near the oases, serving as temporary refuges for travelers and merchants daring enough to cross this unforgiving land. These outposts are fortified against both the elements and the bandits who prey on those unprepared for the harshness of the Drylands. -->

#### Cave Network

Beneath the sun-baked surface lies an intricate network of subterranean tunnels and caverns. These underground passages, carved by long-dried rivers, provide refuge for both creatures and nomadic tribes seeking shelter from the oppressive heat. Small animals such as tsimb lizards, burrowing insects and predators like the shaft dragon make their homes here.

<!-- This place also hold secrets from an ancient civilization, with hidden ruins and forgotten relics buried deep within the earth. -->

#### Tribes

Most tribes of the Sunfire Drylands are deeply rooted in their ancient traditions, choosing to remain underground rather than settle on the surface. Each tribe holds secretive knowledge of the vast tunnel network, with specific territories claimed and fiercely protected. Their allegiances vary greatly: some maintain strong ties with the outside world, embracing modern religions and technologies, while others remain isolated, shunning all contact.

<!-- Settlements : because of the Drylands' harsh sunlight, the sedentary people of the surface live in settlements built under gigantic tents (similar to Kazakhstan's Khan Shatyr Entertainment Center). The silk used to make these tents is harvested from colonies of alien-like creatures, similar to how eggs are harvested from kwamas in Morrowind. -->

<!-- City/Town names : Qrashwar, Gyzybzam, Kalbit, Oqalap, Lalamby, Hithimaal, Foko -->

<!-- Real Underground Cities You Didn’t Know Exist : https://www.youtube.com/watch?v=C-UPTs0zMxc -->




## High Glades


A convergence of temperate grasslands and towering coniferous giants. The ground is blanketed in a mosaic of grasses, interspersed with wildflowers that sway in the gentle breeze. Streams and rivers weave through the land, nourishing the roots of majestic conifers, and their banks serve as the chosen haven for the rare arboreal organisms that thrive in the cool shade.

<!-- Inspiration : Conguillío National Park (Chile) https://en.wikipedia.org/wiki/Conguill%C3%ADo_National_Park -->




## Radovan Outreefs


<!-- Coastal region sparsed with out of water reefs (check out [0 - Lorebook]/[0 - Resources]/Concept Art/Outreefs/ ) -->

<!-- "Radovan" stands for the people/culture living there? -->




## Eagnai Ocean

***
