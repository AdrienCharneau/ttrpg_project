# TO DO




- re-read / re-organise / format my notes

- fleshed out Mathom House map

- rat fight in the basement

- flesh out Michel Dwelving side quests

***








# GENERAL




- 1 Mile = 1.6 Km

- 1 League = 3 Miles (4.83 Km)

- Printemps 2960 du Tiers-Âge (ou 1360 d'après le calendrier de la Comté)

- Cottage (Chaumière)

- To stir (s'agiter)

- skylight opening (ouverture de lanterneau)

- cobwebs (toiles d'araignées)

- golf

- fêtes d'anniversaire

***








# POLITICS




- Thain (largely honorary title of the military leader of the Shire)

- Hobbiterie-en-armes (ancienne milice locale devenue compagnie de cérémonie dirigée par le Thain)

- The Watch (Guéteurs)

- 12 Shirriffs

- Bounders (Garde-frontières) -> Patrouille de la Comté - "state your purpose" -> "Exprimez votre raison"

***








# CHARACTERS




- Marcho et Boffine (fondateurs de la Comté)

- Gerontius Took (Gerontius Touc - Grand père de Bilbo, Thain connu)

- Mayor Pott Whitfoot (Maire de Grande-Creusée depuis 3 ans)

- Malva Slowfoot (Malva Piedlent)

- Baldo Bunce (Baldo Bonçe)

- Ada Burrows (Ada Baudry)

- Rorimac Brandebouc?

- Firework (Feu d'Artifice) -> chien de garde (terrier à fourrure grise et brune)

***








# ITEMS




- Mathom : mot typiquement hobbit servant à désigner des objets sans utilité quelconque dont on ne souhaite pas pour autant se séparer

- Carte du Vieux Touc

- Pipe-weed (Herbe à Pipe)

***








# LOCATIONS




- Eriador

- La Comté (Quartier Nord, Est, Sud, Ouest)

- Route de l'Est




## Quartier Ouest


### - Centre

- L'eau (rivière - possibilité de pêcher ou de rencontrer des pêcheurs)

- Bywater (Belleau)

- Bywater Pool (Bassin de Belleau)

- Auberge du Dragon Vert

- Three-Farthing Stone (Pierre des Trois Quartiers)

- Frogmorton (Grenouillers)


### - Hobbiton (Hobbitebourg)

- Underhill (Souscolline)

- Bag End (Cul-de-Sac)

- Overhill (Suscolline)


### - Nord-Ouest

- Bindbole Wood (Forêt de Bindbole)

- Needlehole (Trou de l'Aiguille - chasseurs des marais, très bons constructeurs de pipe)

- Rushock Bog (Marais d'Herberesque - chasseurs de canards)

- Nobottle (Bourdeneuve - mauvaise réputation, pas chaleureux, plutôts des pêcheurs)

- Little Delving (Petite-Creusée)

- Tighfield (Champfunel)


### - Waymeet (Passe-Croisé)

- Walking Party Inn (Auberge du Pas-Groupé)


### - Tookland (Pays-de-Touc)

- Whitwell (Fontblanche)

- Tookbank (Côte-aux-Touc)

- Tuckborough (Tocquebourg)


### - Michel Delving (Grande-Creusée)

- White Downs (Côtes-Blanches)

- Mathom House (Maison des Mathoms - musée de bibelots)




## Quartier Sud


- Green Hill Country (Pays des Côtes Vertes)

- Longbottom (Fondreaulong - tout au sud)

***
