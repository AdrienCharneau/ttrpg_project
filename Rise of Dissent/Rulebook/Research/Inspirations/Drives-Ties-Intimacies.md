# BLADES IN THE DARK - VICES




>p.165




Every scoundrel is in thrall to some vice or another, which they indulge to deal with stress. Choose a vice from the list, and describe it on the line above with the specific details and the name and location of your vice purveyor in the city:

- Faith: You’re dedicated to an unseen power, forgotten god, ancestor, etc.

- Gambling: You crave games of chance, betting on sporting events, etc.

- Luxury: Expensive or ostentatious displays of opulence.

- Obligation: You’re devoted to a family, a cause, an organization, a charity, etc.

- Pleasure: Gratification from lovers, food, drink, drugs, art, theater, etc.

- Stupor: You seek oblivion in the abuse of drugs, drink to excess, getting
beaten to a pulp in the fighting pits, etc.

- Weird: You experiment with strange essences, consort with rogue spirits,
observe bizarre rituals or taboos, etc.

***








# EXALTED - INTIMACIES


Intimacies represent loves, hatreds, ideals, and goals - the things in this world people feel strongly about. Intimacies are important to [morale management], as they help determine what kinds of influence will affect your character. They come in two basic types:

- **Ties** describe your character’s attachments to people, objects, organizations, and other concrete entities. They have an emotional context which describes your character’s feelings towards that entity. Ties are generally written as the subject of the Tie, followed by a parenthetical clarifier to establish context.

- **Principles** describe your character’s beliefs and ideals. Principles are generally written as a statement of the Principle. Examples include ideals such as “Honesty is my watchword” and “Pragmatism rules my actions,” and beliefs such as “The Immaculate Philosophy is the true guide to righteous living” or “I believe everyone looks out for number one.”

The basic mechanical function of Ties and Principles is identical, but some Charms and other special rules may differentiate between them.

Intimacies come in three levels of intensity: Minor, Major, and Defining. Minor Intimacies are notable parts of your character’s worldview, but only come into play when the subject of the Intimacy is directly relevant to her current situation. Major Intimacies hold more influence over your character, coming into play even if the subject is only indirectly or tangentially related to the situation at hand. Finally, Defining Intimacies hold sway over every aspect of your character’s life—they’re the pillars of her worldview, and often things she would lay down her life to protect.

Characters may gain new Intimacies in several ways, all subject to Storyteller approval. The key to changing Intimacies is that the change must make sense given the roleplaying going on during the scene and in the broader context of the story. Characters can’t gain beneficial new Intimacies if it doesn’t match how they’ve been played.

>Intimacies can be created at Minor intensity or strengthened by one level by the social influence of other characters.

Whenever the player feels it is appropriate and the Storyteller agrees, the character may add a new Minor Intimacy or intensify an existing Intimacy at the end of a scene by one degree.

>In extraordinary situations, the character may gain a new Intimacy at Major or Defining Intensity based on the events of the story—when an Abyssal murders your brother, it’s probably acceptable to go straight to a Major or Defining Tie of hatred toward him.

Losing Intimacies is similarly simple, and likewise subject to Storyteller discretion.

>Intimacies can be degraded by one level or removed entirely (if Minor) by the social influence of other characters.

Whenever the player feels it is appropriate and the Storyteller agrees, the character may remove a Minor Intimacy or degrade an existing Major or Defining Intimacy at the end of a scene — the character just doesn’t care about that thing as much anymore. Generally, this should follow several sessions in which the subject of the Intimacy hasn’t come up—characters should rarely drop or degrade an Intimacy right after it has been created, even if the Intimacy is undesirable. Alternately, this might follow several sessions showing the character resolving or working to conquer unwanted Intimacies, such as Intimacies of fear or hatred.

Whenever the Storyteller judges that a player hasn’t reflected an Intimacy in her roleplaying for a while, she may declare that it has degraded even disappeared completely. This is mostly to keep characters from accumulating a lot of Defining Intimacies, which should be reflected in the character’s actions at least once per story. Few characters can sustain the kind of intensity needed for more than a small handful of Defining Intimacies, and the Storyteller’s pruning helps keep down the clutter.

***




# FATE - ASPECTS


An aspect is a phrase that describes something unique or noteworthy about whatever it’s attached to. They’re the primary way you spend and gain fate points, and they influence the story by providing an opportunity for a character to get a bonus, complicating a character’s life, or adding to another character’s roll or passive opposition.

Every game of Fate has a few different kinds of aspects: game aspects, character aspects, situation aspects, consequences, and boosts. They mainly differ from one another in terms of what they’re attached to and how long they last.


## Example: Character Aspects

Character aspects are just as permanent, but smaller in scope, attached to an individual PC or NPC. They describe a near-infinite number of things that set the character apart, such as:

- Significant personality traits or beliefs (Sucker for a Pretty Face, Never Leave a Man Behind, The Only Good Tsyntavian Is a Dead Tsyntavian).

- The character’s background or profession (Educated at the Academy of Blades, Born a Spacer, Cybernetic Street Thief).

- An important possession or noticeable feature (My Father’s Bloodstained Sword, Dressed to the Nines, Sharp Eyed Veteran).

- Relationships to people and organizations (In League with the Twisting Hand, The King’s Favor, Proud Member of the Company of Lords).

- Problems, goals, or issues the character is dealing with (A Price on My Head, The King Must Die, Fear of Heights).

- Titles, reputations, or obligations the character may have (Self-Important Merchant Guildmaster, Silver-Tongued Scoundrel, Honor-Bound to Avenge My Brother).

You can invoke or call for a compel on any of your character aspects whenever they’re relevant. GMs, you can always propose compels to any PC. Players, you can suggest compels for other people’s characters, but the GM is always going to get the final say on whether or not it’s a valid suggestion.

The primary way you’re going to use aspects in a game of Fate is to invoke them. If you’re in a situation where an aspect is beneficial to your character somehow, you can invoke it.

The group has to buy into the relevance of a particular aspect when you invoke it; GMs, you’re the final arbiter on this one. The use of an aspect should make sense, or you should be able to creatively narrate your way into ensuring it makes sense.

Precisely how you do this is up to you. Sometimes, it makes so much sense to use a particular aspect that you can just hold up the fate point and name it. Or you might need to embellish your character’s action a little more so that everyone understands where you’re coming from. (That’s why we recommend making sure that you’re on the same page with the group as to what each of your aspects means—it makes it easier to justify bringing it into play.)

Finally, aspects have a passive use that you can draw on in almost every instance of play. Players, you can use them as a guide to roleplaying your character. This may seem self-evident, but we figured we’d call it out anyway—the aspects on your character sheet are true of your character at all times, not just when they’re invoked or compelled.

***




# PENDRAGON - TRAITS AND PASSIONS


Traits and passions are game mechanics for quantifying your character’s inner self, recording both repute and propensity. They help you run your character in a consistent manner and according to his actual play activity. A value of 16 or more in a particular trait or passion indicates tremendous interest and activity in that quality, perhaps bordering on the fanatical. The behavior is very obvious to everyone, and is thus significant in roleplaying terms as well as in game terms.


## Traits

The personality traits used in Pendragon consist of thirteen opposed pairs of virtues and vices. Note, though, that what is a virtue in one culture is sometimes a vice in another. Thus, Christian and pagan cultures, for instance, view the contrast between Modesty and Pride in a very different light. All initial characters begin with a moral base derived from either British or Roman Christianity or from Paganism:

- Roman Christian Virtues: Chaste, Forgiving, Merciful, Modest, Temperate.
- British Christian Virtues: Chaste, Energetic, Generous, Modest, Temperate.
- Pagan Virtues: Generous, Energetic, Honest, Lustful, Proud.


## Passions

Passions are strong emotional and psychological propensities within any individual. These include Love, Hate, Loyalty, Envy, and Honor, among others. Beginning characters all begin with five passions: Loyalty (to their lord), Love (of family), Hospitality, Honor, and Hate (of Saxons).

- Loyalty is the prime virtue and passion of the medieval world — without it the feudal system could not exist. Most knights believe in the axiom “King before God.” Showing obedience to one’s immediate overlord is always correct behavior, and disobedience to a lord is shocking to all true knights.

- Love of family is a natural emotion common to humankind in any age or culture. To most people, family equates to society in microcosm, which in turn represents their world. Further, the travel restrictions of the Arthurian era emphasize family closeness.

- Everyone learns Hospitality at his or her mother’s knee, and it is so ingrained that it is almost subconscious.

- Honor is the knight’s special passion. All knights must retain a value of 4 or higher in Honor, or they risk losing their knighthood. (See Honor in Chapter 4 for more information.)

- Hatred of the Saxons is an inherited passion of all Salisbury residents.

During play, character behavior is often challenged by the Gamemaster. Temptations are paraded forth, moral crises heaped up, and critical judgments and actions thrown forward. Since traits and passions define character personality, they must be consulted whenever the Gamemaster feels them necessary. In crises, it is assumed, individuals act according to their character, not spontaneous and ambiguous choices.

The Gamemaster should request trait rolls only when a trait is tested in an important situation. In general, trait rolls simulate situations in which a crisis forces the character to act unconsciously.

***




# SMALLVILLE - DRIVES AND VALUES


The first half of your Lead sheet lists your character’s Drives: his Relationships with the people important to him and the Values that he holds dear. Each one has a statement (in italics) and a die rating. The statement describes his impression of each person or value. The rating scores how much he cares about that Drive. You roll in dice for Drives when your character’s actions advance or assume the truth of the statement.

Unlike other roleplaying games where Attributes or Skills are the first dice you pick up, the Smallville RPG relies on this initial approach of, “Why am I doing this, and for whom?”. It might be hard to wrap your brain around at first if you’re not used to thinking in that way, but trust me: you’ll get the hang of it, and once you’ve done it a few times you’re going to start thinking of new ways to describe your Lead’s actions or the actions of a Feature.

During Lead creation, you assign dice to six Values: Duty, Glory, Justice, Love, Power, and Truth. Your Pathway choices and backstory help prioritize these for you. The ratings you give them and, more importantly, how you define them with respect to your Lead, affects how your Lead succeeds or fails, and this is where the drama comes in. Your Values are going to go head-to-head with those of your opponents—and your friends.


## Statements

Anyone can go to a dictionary and read the definitions for justice, truth, love, etc., but what we really want to know is how your Lead defines these Values. Each character has a different idea of what Justice means to him or what Love really is, and these differences get to the heart of why our Leads behave the way they do—why they stand up and fight, what path they’re walking into the future.

In Smallville, even the most seemingly mundane Lead is secretly amazing. They can all do marvelous and interesting things, or they have access to powerful and remarkable resources. But the most important thing that differentiates them is how they qualify—not quantify—their Values.

**Example** : Let’s have another look at Clark’s Values, this time with his Value statements. You can see here how Values are presented, too, with the name of the Value in small caps, its statement in italics like this, and the die rating.

- Duty : I must fulfill my parents’ hopes for me d8
- Glory : The Blur gives people hope d6
- Justice : I must protect the innocent d10
- Love : I must safeguard my family and friends d8
- Power : Power corrupts d6
- Truth : The truth is often dangerous d4

All of these statements reflect both the weight given to the Value and the personal worldview of Clark at this point in his life.

The statements for your own Smallville Lead reflect a particular view of each Value—and your options are endless. If Glory is your most important Value at a d10, how does your Lead live up to this? Is he an attention seeker who believes even bad PR is good PR? Or does he, paladin-like, willingly martyr himself in the quest for renown?

Of course, everyone’s Values change and grow as you move through life. The same is true for your Lead. You will have opportunities in the game to change the die rating of a Value and to rewrite your Value statements as appropriate.


## Using Values

Once you’ve established what motivates your Lead—you’ve settled on your die rating and polished up your Value statements—you’re ready to show off your own brand of heroism or villainy.

It’s time for a Test. Watchtower declares it’s time to pick up some dice and resolve a situation; this is your cue to assemble your dice pool. Start with Values. Declare the action (or reaction) you wish to make and begin by asking, Why am I doing this? Is there a Value that speaks to this deed more than the others? By committing this action, will you live up to your Value statement?

**Example **: Let’s say Clark’s trying to stop Plastique from blowing up a Metropolis building. The first question is, “Why is Clark doing this?” We can argue a few things:**

- Duty : Clark knows his parents—both Kryptonian and human—want him to do the right thing.
- Glory : If people see the Blur saving a building, they
will have hope.
- Justice : By stopping Plastique, Clark is protecting
innocent people.
- Love : If some of Clark’s friends are in the building, he can argue that he’s protecting his friends.
- Power : If Plastique is corrupted by her power, she needs to be stopped.
- Truth : By blowing up this building, Plastique could expose some dangerous truth to the world that will cause even greater chaos and harm.

You should make sure that you’re not inventing story elements just so you can use a Value: if Clark doesn’t have friends in the building, then he can’t use Love, for example. But if it’s already been established, then it’s totally appropriate. The point is that you can make a lot of different arguments depending on the situation. In this case, rolling Justice d10 is the best choice because it a) is the most plausible answer, b) speaks directly to Clark’s most important Value of Justice, and c) gives the player the strongest die to roll.


## Acting against your values

What happens when one of your Value statements conflicts with what you think your Lead would do? That’s okay; in fact, that’s good and is likely to happen often. Not all of your Lead’s actions will fit seamlessly with your Value statements. When they don’t, this is an opportunity for your Lead to grow. You can challenge a Value when the action you wish to take is in conflict with your declared Value statement. p.87


## Duty

Obligation, service, responsibility, and commitment—but to whom? For what reasons? To what extent? If Duty is important to your Lead—or if it really isn’t—you need to define why.

- I’m the only one that can save the world.
- I will live up to my family name.
- The people I care about depend on me. I won’t let them down.
- Make mom proud.
- I won’t be told what to do.


## Glory

Honor, distinction, credit, renown, and fame. Do you demand to be the center of attention or are you constantly in the headlines against your will? Perhaps you have always worked behind the scenes and now want your due. Or is Glory just something that threatens to get in your way?

- I will be famous!
- I’m one of the world’s best.
- Being a Luthor has its advantages.
- I can give people hope.
- Credit for my deeds is unimportant.


## Justice

Fairness, impartiality, righteousness, and integrity. Is the world fair? Is there something you can do to make it more so? Does the Universe owe you? Or are you out there to make sure evildoers get their just deserts?

- Protect the innocent.
- Rid the world of sin.
- Help those less fortunate.
- I try to do what I think is right.
- Life is unfair; that’s just the way it is.


## Love

Affection, caring, kinship, and devotion. Is your life full of the devotion of friends and family or have you been searching for it your whole life? Maybe you Love only yourself or have no time or inclination to commit to such a trite emotion.

- What could possibly be greater than Love?
- I will protect my friends and family no matter what.
- I have been through the ringer, but I think I’m getting better at this.
- Love happens or it doesn’t. Don’t sweat it.
- I have no room in my life for Love.


## Power

Control, influence, command, and clout. Do you hold the reins of mighty empires, giant corporations, influential countries, or a family dynasty? Perhaps you see Power as a way of getting something you want. Or are you always on the short end when it comes to influence?

- I will control the world.
- I’m the one people come to when they need something, and I decide what they get.
- It’s better to have at least some power than none.
- Power is dangerous.
- I am powerless.


## Truth

Fact, certainty, legitimacy, and sincerity. Are secrets and lies the real enemies? Do you decide what is true and who gets the facts? Has your entire life been built around tall tales that are only now surfacing? Are you just beginning to see the Truth about what’s really going on around you?

- I will uncover the real Truth, no matter who it destroys.
- Something’s not right, and I’m going to get to the bottom of it.
- There’s a lot for me to learn.
- The world is full of too many secrets.
- I believe what I want to believe.

***