# chatGPT: Mechanics similar to action purposes and resolve points


In the World of Darkness RPG, Willpower is a characteristic that represents a character's inner strength and determination. It is used to resist temptations, overcome fear and push through physical and mental challenges. Willpower points are often spent to activate certain powers or abilities, and also to resist interference from supernatural powers. Characters can regain Willpower points by succeeding at actions that align with their moral or personal beliefs, or by taking time to meditate or reflect.

Willpower can also play a role in certain mechanics such as Frenzy. Frenzy is a state of mind in which a character's emotions and instincts take over, and their actions are driven by their primal desires. Characters with low Willpower are more prone to Frenzy, and in some cases spending Willpower can help a character resist it.

Another example is the mechanic of Humanity, which represents the character's connection to their own humanity. Characters with low humanity are more likely to lose control of themselves and act in ways that are monstrous or inhuman. Willpower can be spent to resist such impulses and maintain control.

Additionally, in some of the World of Darkness games such as Vampire: the Masquerade, Willpower can also be used to resist the influence of other supernatural creatures. Willpower can be spent to resist a vampire's Dominate power, for example.

***


In the Numenera RPG, the Effort mechanic is used to represent the extra effort a character is willing to put into a task in order to increase their chances of success. When a character attempts a task, they may choose to spend Effort points in order to boost their roll. The number of Effort points a character can spend is based on their Effort stat, which ranges from 1 to 10.

Effort can be applied to both physical and mental tasks, such as combat or using a complex device. Each point of Effort spent increases the difficulty level of the task by one step, making it more challenging to accomplish. However, by increasing the difficulty, the character also increases their chances of success.

The idea behind this is that by putting in more effort, the character is essentially pushing themselves harder and taking greater risks, which can lead to a greater payoff if they are successful.

It is similar to the concept of "burning" energy or resources in order to have a better chance of success. The difficulty level of the task is increased, so it is harder to achieve, but the player's character is more motivated and determined to succeed, so they have a better chance of succeeding despite the increased difficulty.

The amount of Effort points a player can spend on a task is limited, and the more points spent, the greater the bonus to the task roll. However, spending too many Effort points can also result in a negative consequence, such as fatigue or injury.

This allows players to have more control over the outcome of their actions, but also adds an element of risk and decision-making, as players must weigh the potential benefits of investing more Effort against the potential drawbacks.


***

In The Riddle of Steel RPG, the Momentum mechanic is used to represent a character's level of control and mastery over a situation. It is a measure of a character's ability to dominate a fight, outmaneuver their opponents, and gain the upper hand in combat. Momentum is gained by successfully hitting an opponent, making them miss, or using special maneuvers or techniques. As a character gains Momentum, they can use it to perform special maneuvers, increase the damage dealt, or gain other benefits in combat. However, Momentum can also be lost if a character misses an attack, gets hit, or loses control of the situation. The use of Momentum is an important part of the combat system in The Riddle of Steel, as it adds an extra layer of strategy and decision-making for players and GMs to consider during encounters.

Example: A player's character is engaged in a sword fight with an enemy NPC. The player's character is currently at a disadvantage and is struggling to land a hit. However, the player decides to spend some Momentum to gain an advantage in the fight. By spending Momentum, the player can increase the chances of their character landing a hit, make it more difficult for the enemy to defend themselves, or even add extra damage to their attacks. This allows the player to turn the tide of the battle and gain an upper hand. The cost of spending Momentum may come with a risk, such as leaving the character open to counterattacks or losing the momentum if the action is unsuccessful.

A character can regain momentum by successfully performing a difficult task, or by using certain abilities or skills. For example, a character may regain momentum by successfully landing a powerful blow in combat, or by making a successful roll using a skill that is particularly challenging for them. Additionally, certain abilities or special moves may allow a character to regain momentum more easily or quickly.


***

The Burning Wheel: This game has a concept called "Beliefs" which are statements that describe what a character holds as true, what they value and what they are willing to risk everything for. These Beliefs are used to guide the character's actions and decisions.

In The Burning Wheel roleplaying game, "Beliefs" are a key aspect of character creation and development. They are statements that describe what a character believes in or wants to achieve, and they serve as a guide for the character's actions and decisions throughout the game.

When creating a character, players are asked to come up with a set of Beliefs that describe what their character values and desires. These Beliefs can be anything from "I will prove myself to my father" to "I will rid the land of the undead" to "I will find a way to cure my wife's illness". These Beliefs should be specific, personal, and meaningful to the character.

During the game, players can use their Beliefs to gain bonuses on rolls by invoking them. For example, if a character with the Belief "I will prove myself to my father" is trying to impress his father, they can invoke that Belief to gain a bonus on a roll.

The GM can also use Beliefs to create challenges for the characters by testing them. For example, if a character with the Belief "I will rid the land of the undead" encounters a group of undead, the GM can test that Belief by making it difficult for the character to accomplish their goal.

In this way, the Beliefs mechanic in The Burning Wheel helps to create a rich and dynamic game world, where characters are driven by their own motivations and desires, and where their actions and decisions have meaningful consequences. It also helps players to roleplay their characters in a more personal and meaningful way.

It's important to note that Beliefs mechanic is one of the core mechanics of the game, and it's very important for the players to roleplay their characters and to make their characters act in accordance with the beliefs they set.

***


The One Ring: This RPG is set in J.R.R. Tolkien's Middle-earth, and uses a system called "Hope, Fear and Duty" which are the three main motivations that drive characters in the game. Characters can spend points from these pools to gain bonuses on rolls or to boost their abilities in certain situations.

In The One Ring roleplaying game, "Hope, Fear, and Duty" are a key aspect of character creation and development. They are three aspects that help define a character's motivations, beliefs, and goals, and serve as a guide for the character's actions and decisions throughout the game.

When creating a character, players are asked to come up with a statement for each of these three aspects that describe what their character hopes for, fears, and feels duty-bound to do or achieve. These statements should be specific, personal, and meaningful to the character.

Hope represents a character's dreams and aspirations, it's something they long for and look forward to. It's a driving force that keeps characters going even in the face of adversity.

Fear represents a character's worst nightmares, it's something they dread and try to avoid. It can be a source of weakness for the characters, but it can also be a source of strength, as it can make them more cautious and prepared.

Duty represents a character's sense of responsibility, it's something they feel compelled to do, regardless of the cost. It can be a sense of obligation to family, friends, community, or a higher power, it's what they consider to be their duty or calling.

During the game, players can use their Hope, Fear, and Duty to gain bonuses on rolls by invoking them. For example, if a character with the Hope of "I will find my lost family" is trying to find information about them, they can invoke that Hope to gain a bonus on a roll.

The GM can also use Hope, Fear, and Duty to create challenges for the characters by testing them. For example, if a character with the Fear of "I will be corrupted by the power of the ring" is offered the ring, the GM can test that Fear by making it difficult for the character to resist the temptation.

In this way, the Hope, Fear, and Duty mechanic in The One Ring helps to create a rich and dynamic game world, where characters are driven by their own motivations, beliefs, and goals, and where their actions and decisions have meaningful consequences. It also helps players to roleplay their characters in a more personal and meaningful way.

It's important to note that Hope, Fear and Duty are core mechanics of the game, and it's very important for the players to roleplay their characters and to make their characters act in accordance with the Hope, Fear and Duty they set.


***



Star Wars: Edge of the Empire: This system has obligations and duties that characters have, which can be invoked for bonuses or complications on rolls.

In Star Wars: Edge of the Empire, "obligation and duties" are mechanics that help to define a character's backstory and motivations, as well as to create opportunities for adventure and conflict within the game. Obligation is a measure of a character's debts, responsibilities, and other ties to the galaxy, while duties are specific tasks that the character is responsible for completing.

Obligation can be both positive and negative, representing both the debts a character owes, and the debts that are owed to them. The game includes tables that help to generate obligation, randomly, or the players can determine them during the character creation. Each character starts with a certain level of obligation, which can be increased or decreased as the game progresses.

Duties, on the other hand, are specific tasks that a character must complete in order to fulfill their obligations. These can be as simple as paying off a debt, or as complex as completing a mission for a powerful organization. Duties can also be related to a character's profession, such as a bounty hunter's duty to capture or kill a specific target.

Each character has an Obligation score, which represents a debt or responsibility they owe to someone or something. This can be a personal debt, a debt to a crime lord, or a duty to a noble cause. The Obligation score is determined at character creation and can be increased or decreased throughout the game.

When a character takes actions that align with their Obligation, they may gain bonuses to their dice rolls or other in-game benefits. On the other hand, if a character acts against their Obligation, they may suffer penalties to their rolls or other consequences.

Additionally, players can choose to trigger an "Obligation" roll, which is a special roll made with a pool of Destiny Points, a mechanic that allows characters to influence the game's outcome. The roll can be used to gain a benefit and help them accomplish a task related to their obligation, but if the roll fails, it can increase the character's Obligation score and make their debts harder to pay off.

Duties are specific tasks or responsibilities that a character has taken on as part of their occupation or role in the galaxy. Examples of duties could include a bounty hunter's duty to capture their quarry, or a smuggler's duty to deliver their cargo safely. Characters can gain benefits, such as extra money or equipment, for fulfilling their duties, but they may also be penalized if they fail to do so.

In gameplay, both obligations and duties can be used to generate story hooks and complications for the characters. For example, a character with a high obligation may be pursued by creditors or have to undertake dangerous missions to pay off their debts. A character with a high duty, on the other hand, may have to choose between fulfilling their duty and following their own goals or desires. Both obligations and duties can also affect the outcome of skill checks, as the GM can call for a character to make an obligation or duty roll to reflect the added pressure and complications of fulfilling their responsibilities.


***


In the Starfinder role-playing game, "Resolve Points" (often shortened to just "Resolve") is a mechanic that represents a character's ability to push through difficult challenges and stay in the fight even when they're injured or under duress.

Characters start each encounter with a certain number of Resolve Points (usually equal to their level + their Wisdom modifier), and can spend these points to gain various benefits. Some examples of things that can be done with Resolve Points include:

Stabilizing when dying: A character can spend 1 Resolve Point to stop themselves from bleeding out, becoming stable and no longer dying
Stand up: A character that has been knocked prone can spend 1 Resolve Point to stand up as a move action
Ignore critical hit effects: A character can spend 1 Resolve Point to ignore the extra damage and any special effects of a critical hit
Second wind: A character can spend 1 Resolve Point to regain a number of hit points, as well as gaining bonuses to attack rolls, damage rolls and saving throws
Characters regain all of their expended Resolve Points after an 8 hours rest and at the end of each day. This makes Resolve a valuable resource for players, allowing them to push themselves harder and survive longer in combat, but also something that needs to be used strategically.

***