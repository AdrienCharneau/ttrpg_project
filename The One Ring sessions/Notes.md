# NOTES


Prochaine session 6 Août 2023


-> Le groupe est arrivé à la Grande-Creusée et a passé la nuit à la *Taverne du Bon Vivre*

***








# CHARLES (Paladin Took)




- Invitation random d'un hobbit sur la route entre la Passe-Croisée et la Grande-Creusée. Ce dernier l'avait confondu avec quelqun de sa famille. Invitation pour un thé ou autre chez lui

- A réussi à effrayer avec excellence la chouette qui dérangeait la mule dans l'étable où ils ont dormi à la Passe-Croisée. Les animaux de la région sont contents de son action

***








# MANU (DROGO BAGGINS)

***








# JOSEPH (LOBELIA BRACEGIRDLE)




- Sera inscrite dans l'héritage de Bilbo si jamais le groupe réussit à ramener la carte du Vieux Touc

***








# AVENTURE




## Introduction


Histoire de la comté: en général tranquile mais guerre contre des gobelins + grand hiver + famine (environ 200-250 ans avant l'époque actuelle).

>Grand hiver cause une invasion de loups blancs que Gandalf aide à repousser

Bilbo disparait puis revient. Respecté avant son départ, les communautés avoisinantes le regardent aujourd'hui d'un oeil méfiant (et est maintenant sujet aux ragots du coin). Gandalf (aprécié pour ces feux d'artifice) visite fréquemment le pays (s'accomode surtout avec les Toucs) est néanmoins vu d'un mauvais oeuil car il est tenu résponsable de l'arrivée de plus en plus fréquente de nains et d'étrangers dans le pays. Le travail des guéteurs, shirriffs et autres garde-frontières s'est animé récemment, et certaines messes basses disent qu'il faudrait réabiliter l'Hobbiterie-en-armes




### - La Pierre des Trois Quartiers


Que ce soit érigé par le haut roi à Norbury, par les frères Marcho et Boffine, ou par quelque autre Hobbit réfléchi d'un sens surpassant, la Pierre des Trois Quartiers se dresse au milieu de la Comté. Grande comme deux hobbits et peu usée par l'âge ou le temps, cette colonne sert à marquer l'endroit où les frontières du Quartier Ouest, Quartier Est et Quartier Sud se rejoignent. En effet, elle est facilement visible par tout hobbit ou le rare étranger qui voyage sur la Route Est. Bien qu'elle ne serve aucun but formel au-delà d'indiquer le véritable et propre centre de la Comté pour que tous le voient, la Pierre est tenue en grande estime par tous les habitants du pays

>Faire une introduction des personnages autour de la Pierre des Trois Quartiers avant de commencer l'aventure. (pendant que des enfants jouent au "roi de la pierre" et qu'un vieux hobbit leur crie dessus "vous allez la faire tomber!")


### - Belleau

Passage par Belleau et son grand bassin gris puis par l'Auberge du Dragon Vert (last place where Bilbo Baggins was seen before he disappeared. Ruthie Overwater, a local gossip, claims to have witnessed the whole affair). De nombreuses maisons se regroupent autour du Bassin. Une longue ligne de véritables trous de hobbit marque sa rive nord, tous dotés de petits jardins qui descendent jusqu'au bord de l'eau. Une agréable avenue bordée d'arbres forme la section de la route où elle longe l'eau, lui donnant le nom de "Côté du Bassin".

>GROSSE PLUIE -> Auberge du Dragon Vert


### - Hobbitebourg

Possiblement le plus ancien de tous les villages de la Comté, il repose tranquillement contre le côté sud de la Colline, et les courants de l'Eau le traversent en ligne droite. L'endroit où la route traverse l'Eau sur un pont de pierre est le bâtiment le plus remarquable de la région, le vieux moulin à eau, propriété de M.Sablonnier, qui se dresse à côté de la Vieille Grange.

En arrivant près de Souscolline, vous croisez Hamfast Gamgee, probablement l'un des seul Hobbits qui est resté loyal envers Bilbo. Il s'était occupé e son jardin pendant son absence. Il salue votre passage.


### - Cul-de-Sac

Après une longue promenade sous le ciel qui a éclaté après une après-midi de pluie intense, vous êtes enfin arrivé(e) devant la porte ronde et verte de Cul-de-Sac, la demeure du curieux et célèbre Bilbo Baggins.

Juste au moment où l'un de vous s'apprête à sonner la cloche, la porte s'ouvre et vous voyez se tenir dans le hall d'entrée nul autre que le Maître Baggins lui-même. Il vous adresse à chacun un salut énergique et vous fait rapidement entrer en vous accueillant avec une multitude de bienvenues, de remerciements et de ravies de vous voir.

Avant que vous ne puissiez reprendre vos esprits, vous êtes entraîné(e) dans l'étreinte chaleureuse de Cul-de-Sac, et conduit(e) dans le salon où un beau feu, de nouvelles chopes de bière, des assiettes de friandises et plusieurs fauteuils et canapés rembourrés offrent un lieu de repos et de détente bien nécessaire. Bilbo, un éclat malicieux dans les yeux, vous guide chacun(e) vers un siège mais n'en prend aucun pour lui-même. Quelques instants après avoir fini la première assiette de nourriture, le Maître Baggins, visiblement excité, ne peut plus se contenir. Il place une main derrière son dos et une autre dans sa poche de gilet, se tenant droit pour s'assurer d'avoir toute votre attention.

"Sans aucun doute, vous êtes tous curieux de savoir pourquoi je vous ai convoqués ici ce soir. Eh bien," dit-il en se penchant, la lumière du feu faisant danser joyeusement l'éclat dans ses yeux. Sa main se déplace de derrière son dos vers la fenêtre du salon. "J'espère que vous accepterez de participer à une petite aventure ! Rien d'aussi grandiose que la mienne, j'en ai bien peur, mais en échange de votre aide, je vous offrirai à chacun(e) une place dans mes mémoires et ma reconnaissance éternelle."

Il saisit un verre de vin sur la table et prend une gorgée, faisant une pause pour l'effet. "Maintenant, je sais ce que vous pensez : 'Voilà le vieux Bilbo qui s'y met encore, prenant exemple sur ce sorcier Gandalf et envoyant les Hobbits à l'aventure. Eh bien, je peux vous assurer que ce n'est rien de tout ça. Juste un petit voyage à la Grande-Creusée et de retour pour récupérer ce qu'on pourrait appeler un héritage familial de la Maison des Mathoms : une carte de la Comté soi-disant faite par le Vieux Took lui-même, avec toutes sortes d'annotations précieuses. J'ai envoyé de nombreuses lettres à la gardienne, Malva Piedlent, en lui demandant si je pouvais la récupérer, proposant même des dons assez généreux : mais pour une raison quelconque, elle n'a jamais répondu. Alors, j'ai pensé que nous n'avions pas besoin de lui dire, ni à son mari - après tout, nous parlons de quelque chose qui appartenait à mon grand-père et qui ne détonnerait pas ici, à Cul-de-Sac.

Surtout, ne pas alerter ce fichu chien que les Piedlents ont mis pour garder les lieux! Vif comme l'éclair, cette bête, et il me repère à chaque fois que je m'approche. En outre, je sais que certains d'entre vous attachent plus d'importance aux potins locaux que d'autres - et même être vu(e) la nuit en compagnie de 'Fou Baggins' pourrait suffire à ruiner votre réputation pour toujours. Mais pour ceux et celles d'entre vous qui ont de l'énergie - ce que je sais que vous avez tous en abondance, sinon je ne vous aurais pas convoqués ici ce soir - ne seriez-vous pas prêt(e)s à me venir en aide dans cette mission d'une importance particulière, mes chers conspirateurs ?

>Concours de ronds de fumée avant d'aller dormir

>Tradition holds that the winner gives as a gift to those who have lost a pouch of their finest tobacco, so that they might further study the art. However, the winner is still held in high regard, and can expect a filled tankard and a free meal at the losers’ table within the next few days.


### - Suscolline

Juste au-dessus de la pente nord de la Colline, se trouve la petite communauté de Suscolline. La plupart des maisons y sont construites surélevées pour laisser plus de terre pour l'agriculture et la culture. Ses habitants sont bien connus pour leur habitude de se rendre au nord pour cueillir des champignons et chasser.

En fait, Gerda Boffin, dont la réputation est liée à sa capacité étonnante à trouver une véritable richesse de champignons, s'est fait une véritable fortune en vendant ses trouvailles à l'Auberge du Dragon Vert (renommée dans toute la Comté pour sa soupe aux champignons et au thym, affectueusement appelée "Le Plat de Boffin").


### - Passe-Croisé

Une promenade joyeuse à l'est de la Grande-Creusée le long de la Route de l'Est mènera un hobbit errant aux carrefours de Passe-Croisé. À juste titre nommé, c'est non seulement un carrefour, mais aussi l'endroit où les hobbits de tout le Quartier Ouest se retrouvent pour échanger des marchandises et des ragots en quantités égales. En fait, beaucoup de maisons ont des abris adjacents ou attenants servant à la fois de stockage et de vitrine pour des babioles et des marchandises facilement accessibles à tous les passants qui voyagent le long de la Route de l'Est.

Un ensemble de tables est situé juste le long de la route au milieu de Passe-Croisé, devant les portes de l'auberge du Pas-Groupé. Cela sert en quelque sorte de taverne en plein air, où les visiteurs peuvent étancher leur soif avant de vaquer à leurs affaires et peut-être acheter tout ce qui attire leur attention dans les petites boutiques qui bordent la voie principale. Plus d'un nain traversant la Comté s'est arrêté aux tables de Passe-Croisé pour une pinte et une partie de marl à neuf pence, tout en étant forcément mis au courant des derniers ragots offerts par les joueurs et les commerçants oisifs.



### - Route de l'Est


**Rencontre 1**

Rencontre avec un "Hommes de l'Eriador" en cape élimée, qui leur parle avec exubérance (bouré il cherche à arnaquer les Hobbits):

"Bien le bonsoir, mes braves hobbits ! Vous voilà en bien étranges terres, n'est-ce pas ? Haha ! Dites-moi, qu'amène de si vaillants voyageurs ? Vous ne seriez pas en train de chercher quelque trésor caché, par hasard ? Haha !

Voyez-vous, j'ai entendu des rumeurs qui traînent dans ces contrées, et je sais parfois où dénicher des secrets bien enfouis. Peut-être pourrais-je vous aider dans votre quête, si vous voyez ce que je veux dire. Mais bien sûr, mes bons hobbits, tout service a un prix, n'est-ce pas ? Une pièce ou deux, et je pourrais vous donner une petite indication. Ou bien peut-être quelque chose de plus... précieux ? Haha !

Quoi qu'il en soit, je suis sûr que nous pourrions devenir de bons amis, n'est-ce pas ? Un petit échange d'informations, quelques verres ensemble, et qui sait ce que l'avenir nous réserve ! Alors, qu'en dites-vous, mes chers hobbits ? Sommes-nous prêts à partager un moment d'amitié et de découvertes ? Haha !"


>Rolls (2-3 successses and he leaves you alone): Awe (only on first impression), Courtesy (-1 dice coz drunk), Persuade (-1 dice coz drunk), Riddle (-1 dice coz drunk), Song (+1 dice coz drunk -> falls asleep on success)

>Combat: END 24 (falls unconscious below 12) / PARRY (-1 : -2 coz drunk, +1 coz holding Buckler) / Leather Shirt (1d protection) / Short Sword (3 damage - only 1d6 coz drunk)

>Loot (broken?): Buckler, Leather Shirt, Short Sword



**Rencontre 2**

les joueurs sont approchés par un hobbit qui leur demande de l'aide face à un groupe de nains voleurs

Cloverdale farm

personnages: Andy Roper (mène le groupe de hobbits furieux), Ergi Broadbeam (+ 2 nains), Birba Muggins, Bobbin Muggins

quète des nains p.13

reward if mystery gets solved -> Parfum Sindarin: Une bouteille en cristal taillé de six pouces de hauteur, avec un bouchon d'un pouce, scellée avec de la cire, qui s'élargit depuis sa base jusqu'à sa facette plus large, puis se rétrécit jusqu'à un col fin, contenant un parfum inconnu, musqué mais agréable et très séduisant, doux et sensuel.








### - Grande-Creusée

Vous passez à l'endroit où un chemin vers le sud se détache de la route principale de la route de l'Est, juste au moment où le soleil se couche et que les premières étoiles commencent à se révéler. Devant vous, vous avez une bonne vue sur la Grande-Creusée, perchée sur les Côtes-Blanches. Beaucoup plus grand que Hobbiton, c'est un ensemble densément peuplé de maisons construites en bois, en brique et en pierre, avec parfois un trou de hobbit.

Mais tandis que les derniers habitants vaquent à leurs occupations du soir, votre regard ne peut s'empêcher d'être attiré par l'immense smial situé sur le côté de la grande artère pavée qui traverse le centre-ville : le Trou de Ville, siège du Maire de la Comté.










- **Rosie Mainverte** : Une charmante hobbit fleuriste qui possède le plus beau jardin du Shire. Ses arrangements floraux sont connus pour répandre la joie et la couleur dans toute la ville.

- **Grifon Rondouillard** : L'aubergiste jovial de la *Taverne du Bon Vivre*. Il est réputé pour ses festins délicieux et ses boissons revigorantes qui attirent les hobbits de toute la Comté.

- **Grispoil Tuc** : Un hobbit passionné d'agriculture et de jardinage. Il gère une ferme prospère qui fournit les légumes les plus frais et les plus savoureux à la ville.

>Gandalf gossip table p.19




**La Chocolaterie Feuilledor** : Tenue par le hobbit Pippin Feuilledor, ce magasin est renommé pour ses délicieuses friandises au chocolat, des truffes fondantes aux tablettes les plus fines.

**La Forge des Marteliers** : Ce magasin est géré par les frères Marteliers, Edmond et Fergus, qui sont d'excellents forgerons. Ils créent des outils et des armes de haute qualité pour les hobbits de la Comté.

**La Librairie Sagefeuilles** : Tenu par Iolanthe Sagefeuilles, ce magasin est une mine d'informations avec une vaste collection de livres et contes sur les aventures passées et les lieux éloignés.


**Le Trou Sacquet** : Une colline verdoyante avec une grande porte circulaire, demeure de la famille Sacquet. Ce grand terrier est réputé pour ses fêtes joyeuses et ses rassemblements conviviaux.

**Le Bosquet d'Emeraude** : Un magnifique bosquet ombragé où les hobbits aiment se retrouver pour des pique-niques, des lectures tranquilles et des séances de musique.

**La Colline aux Tours** : Un point de vue pittoresque au sommet d'une colline qui offre une vue panoramique sur toute la Comté. C'est un endroit de choix pour les mariages et autres célébrations.





John Grubbins
Marigold Burrows
Bramble Proudfoot
Clover Bramblebrook
Rorimac Honeypuddle
Otho Greenmeadow
Elanor Thumbleburr







**La Recette Disparue** : Un boulanger local a perdu sa recette secrète pour la tarte la plus délicieuse de la Comté. Aidez-le à retracer ses pas et à rassembler les ingrédients pour recréer la recette.

**Vente de Pâtisseries de Bienfaisance** : Aidez à organiser une vente de pâtisseries de bienfaisance pour collecter des fonds pour les hobbits moins fortunés de la ville.

**Égaré** : Une chèvre espiègle s'est échappée d'une ferme et provoque le chaos en ville. Aidez le fermier débordé à attraper la chèvre et à la ramener en sécurité.

**Préparatifs du "Free Fair"** : La ville se prépare pour le "Free Fair" annuel qui a lieu aux Côtes-Blanches (election d'un nouveau maire tous les 7 ans). Assistez à la décoration de la place du marché, à la préparation des étals de nourriture et à l'organisation de jeux pour l'événement.

**Livraison Florale** : Un hobbit timide souhaite exprimer ses sentiments à sa bien-aimée en lui envoyant un bouquet de fleurs. Aidez-le à rassembler les plus belles et rares fleurs de Michel Delving.

**Inspiration Artistique** : Un artiste en herbe peine à trouver l'inspiration pour sa prochaine œuvre magistrale. Parcourez la ville avec lui, partagez des histoires et des moments qui pourraient susciter sa créativité.

**Rivalité Amicale** : Michel Delving accueille une compétition amicale de tir à l'arc entre deux fermes voisines. Aidez à mettre en place les cibles et à tenir les scores pendant la compétition.

**Chaton Perdu** : Un jeune hobbit a perdu son chaton quelque part en ville. Cherchez le félin joueur et rendez-le à sa propriétaire soulagée.

**Cueillette d'Herbes** : Un guérisseur de la ville est à court de certaines herbes pour ses remèdes. Aventurez-vous dans la campagne environnante pour rassembler les herbes nécessaires tout en profitant des paysages paisibles.

**Échange de Livres** : La Librairie Sagefeuilles organise un événement d'échange de livres. Aidez à organiser et à catégoriser les livres, et assurez-vous que chacun trouve quelque chose d'intéressant à lire.

**Main Secourable** : Aidez un vieux hobbit dans ses tâches quotidiennes, comme ramasser du bois de chauffage, s'occuper du jardin ou réparer un toit qui fuit.

**Construction de Nichoirs** : Travaillez avec les enfants locaux pour construire et décorer des nichoirs pour attirer plus d'oiseaux en ville et ajouter une touche de nature dans les rues.






### - Maison des Mathom

Mais ce qui est encore plus important pour votre affaire se trouve au sud du Trou de Ville, sur une colline adjacente, moins haute et moins verdoyante. Il s'agit d'un grand bâtiment en bois avec une imposante porte ronde rouge, reliée à la route principale par un petit chemin en pierre. C'est la Maison des Mathom, le musée de la Comté, votre destination.


- tenue par Malva Piedlent




>Chers amis,

C'est avec une grande joie que nous vous accueillons dans notre cher musée dédié aux trésors et curiosités de notre chère Comté. Cependant, aujourd'hui, nous devons vous annoncer que le Mathom House est fermée pour la journée. Nos fidèles gardiens de mathoms ont décidé de prendre une petite pause pour prendre soin des jardins et se ressourcer.

Ne vous inquiétez pas, nous rouvrirons nos portes demain matin avec encore plus de merveilles à partager. Nous espérons que vous passerez une agréable journée en attendant notre réouverture.

Malva Piedlent





Alors que les bougies s'allument derrière les fenêtres et que les habitants se préparent pour la soirée, vous remarquez quelques Hobbits se promenant avec des massues solides et de petites lanternes. Ce sont sans aucun doute des Garde-frontières, faisant leur tournée habituelle. Il vaut mieux les éviter, au cas où ils découvriraient et entravaient vos méfaits.



>Tous les joueurs doivent réussir un lancé de discretion si ils ne veulent pas se faire repérer par un garde frontières

>Si repéré: la première garde qui s'approche des conspirateurs est Ada Burrows, une jeune et enthousiaste Hobbit de Little Delving. C'est l'une de ses premières missions, et elle prend très au sérieux son devoir de poser des questions aux voyageurs trouvés à l'extérieur après la tombée de la nuit. -> Persuade / Riddle (échec = fermeture à clef de la Maison des Mathoms)






En inspectant les terrains entourant la Maison des Mathoms pour trouver d'autres façons d'entrer, on découvre que, bien que la porte soit verrouillée et les fenêtres barricadées, il y a une petite ouverture de lanterneau sur le toit. De plus, à quelque distance de la Maison des Mathoms et en saillie sur le flanc de la colline sur laquelle le musée est situé, se trouve une vieille porte qui pourrait conduire au sous-sol de l'établissement.

>Monter sur le toit nécessite un jet d'ATHLETICS. Un échec entraîne une chute qui fait perdre 3 points d'Endurance. Si deux joueurs-héros ou plus échouent au jet, ils se retrouvent empêtrés dans un désordre et tout le monde atterrit avec un bruit fort (qui alerte l'invité surprise qui attend à l'intérieur).

>Une fois là-haut, les conspirateurs découvrent que le lanterneau n'est pas verrouillé. Atterrir en sécurité sur le sol à l'intérieur de la salle principale de la Maison des Mathoms nécessite un deuxième jet d'ATHLETICS (pour les conséquences en cas d'échec, voir ci-dessus).


Les personnages choisissant d'essayer la porte sur le flanc de la colline constatent qu'elle mène à une chambre souterraine étroite et encombrée, un sous-sol rempli de toutes sortes de babioles oubliées et de toiles d'araignées épaisses. En fait, une petite troupe de rats y a élu domicile, craignant le gardien qui vit au rez-de-chaussée. Effrayer les rats les fait fuir bruyamment et réveille le chien de garde : éviter cela nécessite un jet de STEALTH réussi.

>COMBAT AC RATS

Un trou de trappe menant à la Maison des Mathoms s'ouvre au plafond de la cave, au-dessus d'un amas massif de pots et de casseroles antiques. Pour les déplacer avec précaution, il faut réussir un autre jet de STEALTH, de peur qu'ils ne tombent au sol et ne fassent tout un vacarme (et réveillent le gardien).








### - Intérieur

Les conspirateurs se retrouvent à l'intérieur d'une série de grandes pièces aux hauts plafonds, contenant une impressionnante collection d'artefacts représentant toute l'histoire de la Comté. Des étagères de livres racontant les généalogies familiales et les recettes transmises de génération en génération bordent les murs, tandis qu'un ensemble de boutons de diamant parfaitement polis est exposé sur un piédestal sous verre, et une paire de bâtons de marche étrangement croisés est accrochée à un mur. D'innombrables bibelots et mathoms sont exposés. Des vieux boutons en laiton montés sur un panneau de velours à un manteau étincelant de bagues en argent reposant sur un socle dans le coin. Bien que merveilleux à contempler, il va falloir un peu d'effort pour trouver la carte du Vieux Touque dans cette collection !



- Feu d'artifice (Border Terrier)

>https://fr.wikipedia.org/wiki/Border_Terrier

>END 6 / PARRY 0 / (3 damage - 1d6)

>Parfum Sindarin pour adoucir feu d'artifice: +1d6 (jusqu'à 2/3 utilisations)

>attaquer feu d'artifice (uniquement "brawl") = -3 espoir / rate une seule attaque et le perso **se fait attrapper**



Le Maitre du jeu est encouragé à rappeler à tout joueur qui évoque l'idée de dérober l'un des mathoms les plus précieux de la Maison des Mathom que ce comportement est indigne d'un Hobbit respectable. C'est une chose de prendre quelque chose lors d'une mission parrainée par Bilbo Baggins lui-même, mais tout à fait autre chose que de profiter de la situation ! Les personnages qui évoquent même un tel acte devraient avoir honte d'eux-mêmes.



- voler un mathom = -2 espoir

>*Cuisine des Semi-Hommes : 101 façons d'égayer vos repas, écrit par Gerontius Took."* : load +3

>1d6 *"Carreaux Dúnedain en argile cuite et émaillée*, peints avec des scènes de poissons nageant, et avec des perles blanches incrustées comme yeux, un poisson par carreau : load +1 chaque carreau



- voler un mathom (scan roll -> echec = perso **se fait attrapper**) = -2 espoir

>Elven scarf : +1d6 courtesy rolls

>Arc de Chasse : damage 3 (+1 for each *hunting* point - load +2)

>1d4 Flêches du Dúnedain: +2 bow damage (si échec du "scan roll" des Garde-frontières arrivent et récupèrent tous les objets trouvés par les hobbits)




**se faire attraper** = prison (perso éliminé de la partie - ne peut plus participer aux futurs scenarios), les autres doivent réussir un *athletics* roll puis un *stealth* roll pour réussir à fuir

***