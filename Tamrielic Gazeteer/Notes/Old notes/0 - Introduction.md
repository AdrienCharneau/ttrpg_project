```
 _ _|   \ | __ __| _ \   _ \  _ \  |  |  __| __ __| _ _|   _ \   \ | 
   |   .  |    |     /  (   | |  | |  | (       |     |   (   | .  | 
 ___| _|\_|   _|  _|_\ \___/ ___/ \__/ \___|   _|   ___| \___/ _|\_| 
                                                                     
```

***








>*The first ones were brothers: Anu and Padomay. They came into the Void, and Time began.*

>*As Anu and Padomay wandered the Void, the interplay of Light and Darkness created Nir. Both Anu and Padomay were amazed and delighted with her appearance, but she loved Anu, and Padomay retreated from them in bitterness.*

>*Nir became pregnant, but before she gave birth, Padomay returned, professing his love for Nir. She told him that she loved only Anu, and Padomay beat her in rage. Anu returned, fought Padomay, and cast him outside Time. Nir gave birth to Creation, but died from her injuries soon after. Anu, grieving, hid himself in the sun and slept.*

>*Meanwhile, life sprang up on the twelve worlds of creation and flourished. After many ages, Padomay was able to return to Time. He saw Creation and hated it. He swung his sword, shattering the twelve worlds in their alignment. Anu awoke, and fought Padomay again. The long and furious battle ended with Anu the victor. He cast aside the body of his brother, who he believed was dead, and attempted to save Creation by forming the remnants of the 12 worlds into one -- Nirn, the world of Tamriel. As he was doing so, Padomay struck him through the chest with one last blow. Anu grappled with his brother and pulled them both outside of Time forever.*

> ~ *The Annotated Anuad*

***








# WELCOME




[PRIORITY] Talk about early 2000s era Elder Scrolls

>Why am I doing this? -> (Rise of Dissent?)

[PRIORITY] DISCLAIMER THIS IS WIP YES I KNOW MANY THINGS AND HISTORICAL EVENTS ARE MISSING THEY'LL BE INCLUDED EVENTUALLY

***








# UESRPG 3e




[PRIORITY] This uses the UESRPG 3e rules

***








>*Before the rule of Tiber Septim, all Tamriel was in chaos. The poet Tracizis called that period of continuous unrest "days and nights of blood and venom." The kings were a petty lot of grasping tyrants, who fought Tiber's attempts to bring order to the land. But they were as disorganized as they were dissolute, and the strong hand of Septim brought peace forcibly to Tamriel. The year was 2E 896. The following year, the Emperor declared the beginning of a new Era-thus began the Third Era, Year Aught.*

> ~ *Brief History of the Empire v 1*

***








# THE THIRD ERA




The Third Era is the time period in which this adventure takes place, and roughly mirrors the timeline of the Third Empire of Tamriel. Starting right after the chaos of the Interregnum and ending (with the Oblivion Crisis?), it was spearheaded by Tiber Septim , and was forged through a decades-long conflict to unite Tamriel known as the Tiber Wars.

The Adventure actually take place at the end of The Third Era, roughly before **3E 427**, which means right before the events of *The Elder Scrolls III: Morrowind*.




## Before the Third Era


### - The Second Empire of Cyrodiil

Text

>https://en.uesp.net/wiki/Lore:Akaviri_Potentate


### - The Interregnum & The Time of Three Banners

*2E 430 - 2E 854*

The Interregnum began when the last of the Akaviri Potentates, Savirien-Chorak, and all of his heirs were murdered in 2E 430. The resulting collapse of central authority led to five centuries of bickering between racial alliances, small kingdoms, and petty states. During this time, Tamriel's provinces reasserted their independence, and the remnants of the Second Empire nominally survived as the Empire of Cyrodiil, a rump state confined to Cyrodiil that was ruled by a succession of warlords.

Perhaps one of the most pivotal stretches of the Interregnum occurred in the sixth century of the Second Era, when Tamriel became engulfed in a series of crises as well as a continent-wide conflict between three alliances known as the Three Banners War. The first of these alliances was precipitated by a Reachman invasion of High Rock under the command of Durcorach the Black Drake, the first Longhouse Emperor of Cyrodiil, in 2E 541. In 2E 572, northeastern Tamriel faced a similar threat in the form of an Akaviri invasion under the command of Ada'Soom Dir-Kamal, who ravaged the lands of Eastern Skyrim and Morrowind. The third alliance was established in southwest Tamriel in 2E 580 with the ascension of Ayrenn to the throne of the Summerset Isles.

During the entirety of Interregnum, petty warlords had attempted to seize control of Cyrodiil's capital and to reestablish the Empire. None of them lasted for long, until, in the year 2E 852 the king of Falkreath, Cuhlecain, sought to proclaim himself "Emperor of All Cyrodiil". Two years later, with the help of his general Talos, he took the Imperial City from the battlemages of the Eastern Heartland, but was assassinated shortly after. It is uncertain whether or not Cuhlecain was ever crowned Emperor.

>https://en.uesp.net/wiki/Lore:Interregnum


### - Tiber Wars & Conquest of Tamriel

*2E 896*

Tiber Septim, also known as Talos Stormcrown, Hjalti Early-Beard, Tiberius Imperator, and many other titles, was a military leader who became one of the most famous figures in Tamrielic history, reigning as Emperor Tiber Septim from **2E 854** to **3E 38**. Tiber Septim started his career in service under the Cyrodilic king Cuhlecain, where he was known as General Talos. In this role, he fought to unify Cyrodiil and finally all of Tamriel, an effort that culminated in **2E 896** with the birth of the Third Empire. He declared the onset of the Third Era at the end of the same year.

>https://en.uesp.net/wiki/Lore:Tiber_Septim




## The Septim Dynasty


Tiber Septim ruled for 81 years and is considered by many to be the greatest emperor in history. He gave his name to the lineage of Cyrodilic Emperors known as the Septims. In the centuries following his reign, Tiber became revered as a god and saint, and is worshipped as one of the Nine Divines under the name of Talos, meaning "Stormcrown".


|Name                |Crowned |
|:------------------:|:------:|
|Tiber Septim        |3E 000  |
|Pelagius Septim I   |3E 038  |
|Kintyra Septim I    |3E 040  |
|Uriel Septim I      |3E 053  |
|Uriel Septim II     |3E 064  |
|Pelagius Septim II  |3E 082  |
|Antiochus Septim    |3E 099  |
|Kintyra Septim II   |3E 120  |
|Uriel Septim III    |3E 121  |
|Cephorus Septim I   |3E 127  |
|Magnus Septim       |3E 140  |
|Pelagius Septim III |3E 145  |
|Katariah Ra'Athim   |3E 153  |
|Cassynder Septim    |3E 200  |
|Uriel Septim IV     |3E 202  |
|Cephorus Septim II  |3E 247  |
|Uriel Septim V      |3E 268  |
|Uriel Septim VI     |3E 290  |
|Morihatha Septim    |3E 313  |
|Pelagius Septim IV  |3E 339  |
|Uriel Septim VII    |3E 368  |

>https://en.uesp.net/wiki/Lore:Third_Empire

***








>*The Daedra are powerful ancestor spirits, similar in form and substance to the Tribunal (Blessed Be Their Holy Names), but weaker in power, and more arbitrary and removed from the affairs of mortals. In old times, the Chimer worshiped the Daedra as gods. But they did not deserve this veneration, for the Daedra harm their worshippers as often as help them.*

>*The Advent of the Tribunal (Blessed Be Their Holy Names) changed this unhappy state. By the Apotheosis, the Tribunal (Blessed Be Their Holy Names) became the Protectors and High Ancestor Spirits of the Dunmer, and bade the Daedra to give proper veneration and obedience. The Three Good Daedra, Boethiah, Azura, and Mephala, recognized the Divinity of the Triune Ancestors (Blessed Be Their Holy Names). The Rebel Daedra, Molag Bal, Malacath, Sheogorath, and Mehrunes Dagon, refused to swear fealty to the Tribunal (Blessed Be Their Holy Names), and their worshippers were cast out.*

> ~ *The Anticipations*

***








# THE MORROWIND PROVINCE




## History


- ### Beginnings

In the **Dawn Era** or **Late Middle Merethic Era**, Veloth the Prophet abandons the Summerset Isle and leads the Chimers into Resdayn. According to some legends, the Dwemer originated from the same group as the Chimer, and were treated as another tribe, and later, as a secular Great House. Other sources however claim they already occupied the northeast of Tamriel when Veloth and his people arrived there.

>https://en.uesp.net/wiki/Lore:Dwemer


- ### First Era

- **1E 240** : Nordic invasion led by King Vrage

- **1E 668** : War of the First Council, first eruption of Red Mountain

- **1E 700** : Battle of Red Mountain, disappearance of the Dwemer and formation of the Tribunal Temple

- **1E 2703** : First Akaviri Invasion


- ### Second Era

- **2E 572** : Second Akaviri Invasion

- **2E 582** : Vvardenfell is saved from the machinations of Clavicus Vile and Barbas

- **2E 582** : A plot by Nocturnal to take over the Clockwork City is foiled

- **2E 882** : Dagoth Ur and The Sixth House reawaken and drive off the Tribunal from Red Mountain (after a great erruption). Tribunal is now cut off from the Heart chamber

- **2E 893** : Barenziah, future Queen of Morrowind, and later Wayrest, is born

- **2E 896** : Tiber Septim conquers Morrowind and the Treaty of the Armistice is signed (with Vivec?)


- ### Third Era

- **3E 006** : Hlaalu Athyn Llethan becomes Duke of Mournhold

- **3E 376** : Hlaalu Helseth is born to Queen Barenziah and Symmachus

- **3E 391** : Queen Barenziah goes into exile and her successor, Hlaalu Athyn Llethan, is crowned king of Morrowind

- **3E 396** : Arnesian War. What started as a simple slave revolt in the House Dres lands to the south escalated into a full war between Morrowind and Black Marsh

- **3E 400** : The Sixth House expands substantially, takes Kogoruhn and bases are founded in various places around Vvardenfell

- **3E 411** : Tribunal Temple begins a crusade to stamp out dissidents in their ranks

- **3E 414** : Vvardenfell Territory opened for settlement by King Llethan

- **3E 415** : Sixth House has at least a small cell in every town in Vvardenfell

- **3E 417** : The Tribunal lose Kagrenac's Tools (during a battle at Red Mountain) and stop appearing in public

- **3E 426** : Due to high Imperial tax rates and tariffs on trade, the people of Balmora revolt. The revolt is forcefully put down, but accounts of the event differ

- **3E 426** : Assassinations by the Sixth House of Imperial and Hlaalu supporters increase significantly. So do sudden attacks by the cultists and deranged victims of soul sicknes




## Events


### - War of the First Council

**1E 668** - **1E 700**

Also called *The War of the Red Mountain*. The secular Dwemer waged war against the Chimer Great Houses, as well as the nomadic Ashlanders of Vvardenfell, who had united under General Indoril Nerevar. The Nords of Skyrim under High King Wulfharth also entered the fray, seeking to reclaim Morrowind amidst the chaos and restore the First Empire of the Nords.

- **Battle of Red Mountain** : Text

- **Disappearance of the Dwemer** : Text

- **The Tribunal** : Text


### - The Armistice

**2E 896**

*The Armistice*, also known as *The Treaty of the Armistice*, was signed sometime in the final years of the Second Era. It created a lasting peace between Morrowind and the Cyrodilic Empire, furthering Emperor Tiber Septim's dream of a unified Tamriel in the final years of the Second Era. It was signed as the result of a personal meeting between Tiber Septim and the Dunmer god Vivec, which took place after several Empire-won battles, one of which laid waste to Mournhold.




## Politics


### - Great Houses

[PRIORITY] Text

- **House Hlaalu** :

- **House Redoran** :

- **House Telvanni** :


### - Tribunal Temple

[PRIORITY] Text


### - Septim Empire

[PRIORITY] Text

- **Imperial Legion** :




## Vvardenfell


Vvardenfell, also called the *Black Isle*, is a large island located inside the bay-like Inner Sea, and is surrounded by mainland Morrowind with the exception of its northern coast, which meets the Sea of Ghosts. The island is dominated by the great volcano, Red Mountain. The island itself is named after the original Dwemeri name of Red Mountain, literally translating to "City of the Strong Shield". It is characterized by arid wastes, rocky highlands and coastal wetlands filled with unusual and unique flora and fauna.

Red Mountain’s lava flows, eruptions, and ash-fall cause Vvardenfell’s landscape to undergo a constant cycle of death and rebirth. The soil becomes greatly fertilized thanks to the volcanic activity, and new plants and fungi emerge from the cooling ashes. It also causes the island to expand as the lava hardens.


- **Ascadian Isles** : Text

- **Ashlands** : Text

- **Azura's Coast** : Text

- **Bitter Coast** : Text

- **Grazelands** : Text

- **Molag Amur** : Text

- **Red Mountain** : Text

- **Sheogorad** : Text

- **West Gash** : Text

- **Zafirbel Bay** : Text

***