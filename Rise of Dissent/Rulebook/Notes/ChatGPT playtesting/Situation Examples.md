# NALCARYA THE ALCHEMIST

>*Eirnian is looking for a shipment of rare ingredients that were stolen near a town called Culcheth. She will receive 500 gold for the safe return of the stolen goods, and an additional 250 gold if she can deliver them before a specific deadline. She's in a bit of a hurry and hoping she can recover the ingredients to complete the delivery as soon as possible.*

>*She recalls her contractor mentioning a local alchemist named Nalcarya who may know about the rare ingredients she's looking for. Once in Culcheth, Eirnian makes her way to Nalcarya's shop and enters the door. The place is filled with shelves of potions, ingredients, and various alchemical tools. A woman behind the counter greets her with a smile and asks how she can assist her.*

>*Eirnian explains that she has been sent to find some rare ingredients that she needs for a special order. Nalcarya nods and begins to rummage through her inventory.*

>*"I think I may have what you're looking for," she says, pulling out a small pouch of rare mushrooms. "But these are quite valuable, so I hope you have the coin to pay for them."*

>*"I'm not really here to buy the goods... I'm looking for stolen ingredients. Apparently their hideout should be in the region.*

>*Nalcarya raises an eyebrow. "Stolen, you say? I'm sorry to hear that. I haven't heard of any thefts in the area, but I'll keep an eye out for you," she says sympathetically.*

>*She takes a closer look at the mushrooms in her hand and then looks back at you. "If these ingredients are part of what was stolen, I'd be happy to help return them to their rightful owner. Just let me know what you need me to do," she offers.*

>*why? where did you get these ingredients from?*

>*Nalcarya frowns slightly. "I acquired them from a supplier who had traveled all the way from Solstheim. It wasn't cheap, but I'm always on the lookout for rare ingredients to add to my inventory," she explains. She looks at you with a hint of concern. "Why do you ask?" she adds, sensing your suspicion.*

>*[GM] I tried to ask her the name of the supplier but I failed and she refused to tell me. So I just wave off her comment and tell her "nothing, just curious".*

>*Nalcarya nods, accepting your response. "Well, let me know if you need any help finding the hideout. The wilderness can be dangerous, so be careful," she advises. With that, she returns to her shop counter and resumes tending to her wares.*

***




# OLD ACTION RULE EXAMPLES

Here are a few examples of this mechanic at play. We'll use this dummy character as a template:

```
Skills: swimming, climbing, arithmetic, pickpocketing, surgery, painting

Endurance		9
Strength		15
Agility			8
Dexterity		15
Charm			13
Personality		4
Creativity		8
Reasoning		10
Knowledge		7
Experience		12
Empathy			10
Humility		6
```


### - Jumping over a chasm

The character is in a dire situation as they are being pursued by a dangerous enemy. However, their escape route is blocked by a chasm, causing them to stop in their tracks. With the urgency of the situation, the character decides to attempt jumping over the chasm to continue their escape. The stats required for this action are *agility* and *endurance*.

#### Setup:

> - **Skills required**: none - there's no specific skill or training required for jumping
> - **Difficulty Threshold** : 5 - represents the physical challenge of jumping over a chasm
> - **Aptitude** : 17 - the character possesses 8 *agility* and 9 *endurance* (8 + 9 = 17)
> - **Purpose** : 20 - the character is trying to escape imminent danger and save their own life (survival)
> - **Commitment** : 8 - the player decides to commit 8 stamina points, therefore rolls 8d20

#### Resolution:

> - **Dice rolls** : 17, 20, 2, 10, 9, 16, 2, 19
> - **Outcome** : success - 6 of these rolls are below or equal to the aptitude value of the action (17) - 6 is superior to the difficulty threshold of 5, therefore the action is successful
> - **Resolve points lost**: 0 - no roll is strictly superior to the purpose value of the action (20)


### - Persuading a guard to let the character pass

After a long day of work, the character is trying to gain entry to a private party and decides to attempt to persuade the guard to let them pass. The guard is the obstacle standing between the character and their goal of unwinding themselves. The stats required for this action are *personality* and *humility*.

#### Setup:

> - **Skills required**: none - there's no specific skill or training for persuasion
> - **Difficulty Threshold** : 3 - represents the challenge of convincing the guard to let the character pass
> - **Aptitude** : 10 - the character possesses 4 *personality* and 6 *humility* (4 + 6 = 10)
> - **Purpose** : 5 - the character is trying to gain entry to a private party because they want to have fun (pleasure)
> - **Commitment** : 5 - the player decides to commit 5 stamina points, therefore rolls 5d20

#### Resolution:

> - **Dice rolls** : 16, 4, 2, 16, 14
> - **Outcome** : failure - 2 of these rolls are below or equal to the aptitude value of the action (10) - 2 is inferior to the difficulty threshold of 3, therefore the action is unsuccessful
> - **Resolve points lost**: 3 - 3 rolls are strictly superior to the purpose value of the action (5)


### - Climbing a tall cliff to save a survivor

The character is on a mission to rescue a stranded companion trapped upside a cliff. The character decides to attempt to climb the escarpment in order to reach the survivor and bring them to safety. The stats required for this action are *agility* and *endurance*.

#### Setup:

> - **Skills required** : climbing - The character has training in that skill
> - **Difficulty Threshold** : 5 - represents the physical challenge of climbing a medium-sized cliff
> - **Aptitude** : 17 - the character possesses 8 *agility* and 9 *endurance* (8 + 9 = 17)
> - **Purpose** : 10 - the character is trying to save a companion (duty)
> - **Commitment** : 7 - the player decides to commit 7 stamina points, therefore rolls 7d20

#### Resolution:

> - **Dice rolls** : 16, 20, 6, 20, 8, 2, 19
> - **Outcome** : failure - 4 of these rolls are below or equal to the aptitude value of the action (17) - 4 is inferior to the difficulty threshold of 5, therefore the action is unsuccessful
> - **Resolve points lost** : 4 - 4 rolls are strictly superior to the purpose value of the action (10)


### - Solving a complex mathematical equation

The character is competing for a prize in a math contest and is tasked with solving a difficult equation. The stats required for this action are *reasoning* and *creativity*.

#### Setup:

> - **Skills required** : arithmetic - The character has training in that skill
> - **Difficulty Threshold** : 4 - represents the intellectual challenge of solving a complex mathematical equation
> - **Aptitude** : 18 - the character possesses 8 *reasoning* and 9 *creativity* (10 + 8 = 18)
> - **Purpose** : 10 - the character is trying to solve the equation to win a prize in a competition (ego)
> - **Commitment** : 6 - the player decides to commit 6 stamina points, therefore rolls 6d20

#### Resolution:

> - **Dice rolls** : 4, 16, 8, 11, 11, 13
> - **Outcome** : success - all the 6 rolls are below or equal to the aptitude value of the action (18) - 6 is superior to the difficulty threshold of 4, therefore the action is successful
> - **Resolve points lost** : 4 - 4 rolls are strictly superior to the purpose value of the action (10)


### - Pickpocketing a wealthy patron

The character needs money and decides to take advantage of an opportunity to pickpocket a wealthy patron. The stats required for this action are *dexterity* and *experience*.

#### Setup:

> - **Skills required** : pickpocketing - The character has training in that skill
> - **Difficulty Threshold** : 3 - represents the physical and mental challenge of stealing from a noble or wealthy individual
> - **Aptitude** : 27 - the character possesses 15 *dexterity* and 12 *experience* (15 + 12 = 27)
> - **Purpose** : 15 - the character is poor and trying to acquire money for their own needs (power)
> - **Commitment** : 5 - the player decides to commit 5 stamina points, therefore rolls 5d20

#### Resolution:

> - **Dice rolls** : 17, 3, 4, 10, 18
> - **Outcome** : success - all the 5 rolls are below or equal to the aptitude value of the action (27) - 5 is superior to the difficulty threshold of 3, therefore the action is successful
> - **Resolve points lost** : 2 - 2 rolls are strictly superior to the purpose value of the action (15)


### - Performing a surgery

The character is faced with the task of performing a complex surgery to save a close relative from death. The stats required for this action are *empathy* and *knowledge*.

#### Setup:

> - **Skills required** : surgery - The character has training in that skill
> - **Difficulty Threshold** : 5 - represents the physical and mental challenge of performing a difficult surgery
> - **Aptitude** : 17 - the character possesses 10 *empathy* and 7 *knowledge* (10 + 7 = 17)
> - **Purpose** : 15 - the character is trying to save a close relative's life (love)
> - **Commitment** : 8 - the player decides to commit 8 stamina points, therefore rolls 8d20

#### Resolution:

> - **Dice rolls** : 9, 14, 12, 13, 18, 4, 19, 5
> - **Outcome** : success - 6 of these rolls are below or equal to the aptitude value of the action (17) - 6 is superior to the difficulty threshold of 5, therefore the action is successful
> - **Resolve points lost** : 2 - 2 rolls are strictly superior to the purpose value of the action (15)

***