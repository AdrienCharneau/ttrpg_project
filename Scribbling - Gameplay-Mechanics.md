```
   __|   _ \  _ \  __|     \  |  __|   __|  |  |    \     \ | _ _|   __|   __|
  (     (   |   /  _|     |\/ |  _|   (     __ |   _ \   .  |   |   (    \__ \
 \___| \___/ _|_\ ___|   _|  _| ___| \___| _| _| _/  _\ _|\_| ___| \___| ____/
```

***








# **Action Rolls (Part 1)**




## Steps


### - Resolution

#### Raising Emotional Strain

<!-- Make it so stressful situations raise 2 points of Emotional Strain? This could pair well with the Order/Threaten Social Actions, and the possible implementation of a single roll combat sytem (where entire fights are resolved with a single Action Roll). -->

***

***








# **Character State**

***




## Emotional Strain


### - Recovery

#### Resting

- (see Social Influence/Disposition) Emotional recovery system where, if you have positive Sympathy with a character and rest with them you recover Emotional Strain. But if you don't (aka you hate them) then you lose Emotional Strain instead?
- Remove "Rewarding Event" recovery option?

***

***

***








```
    \        |                                   |     \  |             |                 _)
   _ \    _` | \ \ /  _` |    \    _|   -_)   _` |    |\/ |   -_)   _|    \    _` |    \   |   _| (_-<
 _/  _\ \__,_|  \_/ \__,_| _| _| \__| \___| \__,_|   _|  _| \___| \__| _| _| \__,_| _| _| _| \__| ___/
```

***








# Project Clocks

***




A **Progress Clock** is a gauge (or circle) divided into sections used to trace almost anything. This could be plot progression, time management, NPC health or any other variable essential to the unfolding of the narrative. It can be compared to a "progress bar" found in video games, but adapted to suit a variety of situations.

>*Progress Clocks transform narrative outcomes into something tangible that players can grasp, whether it's overcoming an enemy, escaping danger or fulfilling an objective.*

The greater the **Progress Clock**, the more sections it holds:

|Progress Clock |Sections |
|:-------------:|:-------:|
|Standard       |2        |
|Large          |4        |
|Huge           |6        |
|Mighty         |8        |

A **Progress Clock** should always reflect what's happening in the fiction, allowing players to check how they’re doing. For example:

- **Event Clocks** are used to track the progression of time. They can represent the countdown to an event, the arrival of reinforcements or the duration of a spell. Each segment should mark a specific time interval, enabling players to anticipate future developments or prepare accordingly.

- **Obstacle Clocks** are used to track ongoing effort against a challenge. Characters tick segments when succeeding **Action Rolls** aiming at overcoming the challenge, until the clock is filled and the obstacle is no more. Unless it's a *critical success*, a single action cannot fill more than one segment at a time.

<!-- When you create a clock, make it about the obstacle, not the method. The clocks for an infiltration should be “Interior Patrols” and “The Tower,” not “Sneak Past the Guards” or “Climb the Tower.” The patrols and the tower are the obstacles— the PCs can attempt to overcome them in a variety of ways. -->

- **Threat Clocks** are used to monitor impending trouble or danger. They can symbolize growing suspicion, the proximity of pursuers or the alert level of guards. Segments are ticked when complications arise or characters mess up, like when an action ends on a *limited success* or a *failure* (with *critical failures* ticking more than one segment at a time).

- **Influence Clocks** are used to represent the effort required to sway an opinion, negotiate a deal or achieve an objective through persuasion, intimidation or manipulation. Segments can be ticked based on the narrative, or through the success of social actions aimed at influencing the target.

- **Project Clocks** are used to monitor the advancement of a long-term task or endeavor. This could be building a stronghold, learning a skill, writting a book or crafting an artifact. Segments should represent the various stages of the project, with each tick marking a significant milestone towards completion.

>*Progress Clocks are not required for every situation. The GM should use them for challenges where tracking advancement is essential. Otherwise, they should opt for resolving outcomes with a single Action Roll.*

<!-- **Combat Clocks** are used to track the progression of combat encounters. They can represent a number of enemies, their morale or even their armor. Segments  are ticked when characters perform successful attacks on their adversaries and, unless there's a *critical success*, a single attack cannot fill more than one segment at a time. -->

<!-- Faction Clock : each faction has a long-term goal. When the PCs have downtime, the GM ticks forward the faction clocks that they’re interested in. In this way, the world around the PCs is dynamic and things happen that they’re not directly connected to, changing the overall situation in the city and creating new opportunities and challenges. The PCs may also directly affect NPC faction clocks, based on the missions and scores they pull off. Discuss known faction projects that they might aid or interfere with, and also consider how a PC operation might affect the NPC clocks, whether the players intended it or not. -->

<!-- Spellcasting Clock -->

<!-- When you create a clock, make it about the obstacle, not the method. The clocks for an infiltration should be “Interior Patrols” and “The Tower,” not “Sneak Past the Guards” or “Climb the Tower.” The patrols and the tower are the obstacles —the PCs can attempt to overcome them in a variety of ways. -->

<!-- https://www.reddit.com/r/bladesinthedark/comments/u2na85/help_with_understanding_progress_clocks -->

<!-- https://www.reddit.com/r/rpg/comments/bft68t/using_clocks_world_bitd_in_any_game/ -->

***








# Social Influence (Part 1)

***




Influence is used for resolving situations where characters aim to manipulate, persuade or coerce each other. Successful influence rolls can convince characters or NPCs to act a certain way, alter their opinions or extract benefits from them.

>*This can range from*

When engaging in social influence, the first step  is to declare a **Target** and articulate a desired outcome. Simply put, state what the character wants their subject to do or say, along with a rough idea on how they aim to achieve it.

>*However, certain limitations apply. No amount of conviction can compel someone to commit suicide or give up their entire wealth. Similarly, absurd requests or things that could make a player uncomfortable should be avoided, while attempting to seduce someone contrary to their sexual orientation are deemed unacceptable.*

Second, only roll if the target doesn't want to be influenced.

<!-- Chronicles of Darkness - Social Maneuvering : (Notes/Research/Inspirations/Persuasion-Social Combat.md) + (Notes/Research/Inspirations/Action Lists-Saving Throws.md)
Exalted - Social Influence : (Notes/Research/Inspirations/Persuasion-Social Combat.md)
Roadwarden - Attitudes : (Notes/Research/Inspirations/Dialogue Trees.md) -->

<!-- My Method for Winning Over Conservatives : https://www.youtube.com/watch?v=PojRfPEH2mY -->

<!-- Read a more into Exalted :

"Social Complications":

- One Instigator vs. Many Targets

- Written Social Actions

- Gestures and Body Language

- Overturning Influence

- Retrying Social Actions

- Social Actions in Combat

-->




## Influence Clocks


Most times, social influence involves applying persuasion over time, whether through direct demands or subtle suggestions. This is represented with **Influence Clocks** (a type of **Progress Clock**), where each segment ticked gradually molds the target's mindset or behavior, eventually leading to compliance with the desired outcome:

<!-- Influence Clocks also feature intervals between rolls, representing the time required for each subsequent action -->

|  Demand  | Clock Sections | Time Per Roll | Description                                                                                      |
| :------: | :------------: | :-----------: | ------------------------------------------------------------------------------------------------ |
| Average  |       2        |   One Hour    | Demand entailing some form of risk or inconvenience, potentially impacting resources.            |
|  Major   |       4        |    One Day    | Demand entailing significant risk of harm or impediment, or some form of strong commitment.      |
| Integral |       8        |   One Week    | Requests entailing profound life-altering sacrifices, often involving matters of utmost gravity. |

<!--

- Demands involving mild risks or inconveniences, potentially impacting personal time, resources, or commitments, without posing existential threats or profound changes to the target's life. [or demands aligned with a target's Drives or Quest]  

- Demands that entail significant risk of harm or impediment, or some form of strong commitment, such as significant financial contributions, personal sacrifices or long-term undertakings.

- Requests entailing profound life-altering sacrifices, often involving matters of utmost gravity, such as relinquishing a core belief or religion, reshaping one's lifestyle and priorities or giving up essential resources or large portions of wealth. [or demands that go against a target's Drives or Quest]

-->

When gauging the size of an **Influence Clock**, the target's **Drives** and **Quest** should be taken into consideration. For instance, always ask: "how likely is the character to accept the request?" The less likely, the larger the clock.

>*Context should not be forgotten. Requesting a small sum from a beggar may carry significant weight, considering it could be crucial for their survival. Conversely, a general asking a soldier to risk their life in battle may carry less weight, given the inherent agreement to such risks upon joining the army.*

<!-- Events or changing circumstances can reduce the size of an Influence Clock before it is completed. For example, someone reluctant to sell their house might reconsider after learning about a significant market downturn -or discovering it’s built atop an ancient undead graveyard. -->

<!--  From Chronicles of Darkness : Once your character opens the final Door, the subject must act. Storyteller characters abide by the intended goal, and follow through as stated. If you allow players’ characters to be the targets of social maneuvering, resolve this stage as a negotiation with two possible outcomes. The subject chooses to abide by the desired goal, or offer a beneficial alternative. -->

<!-- #### Examples -->

<!-- These have been copy-pasted from Exalted: -->

<!-- - **Minor Demand** : *“I need you to deliver this parcel to that big house in Cinnabar District, with the red jade lion statues by the door. If the man on the door has a scorpion tattoo, don’t leave it with him —insist to see the master of the house.”*

- **Major Demand** : *“Just because he’s your father doesn’t make you his slave —why should his fear deny you a place in AnTeng’s glorious uprising against its oppressors? This nation needs heroes; men like you!”*

- **Pivotal Demand** : *“I know the old scrolls said the heart of this temple is guarded by a fearsome beast of brass and flame. I know it’s frightening, but isn’t this why we came so far and spent our fortunes, to be the first ones to scavenge the Great Ziggurat of Lost Zarlath? I’ll never make it into the final chambers with my leg like this—you’ll have to dare it for both of us!”* -->




## Disposition


**Disposition** refers to the attitudes and feelings the **Target** holds towards the character. It is represented by two values (*sympathy* and *respect*), which vary depending on the alignment, perception of actions and choices made by them through the story.


### - Sympathy

*Sympathy* indicates the level of compassion or trust that the target extends towards the character. It signifies empathy and emotional connection, influenced by their behavior or choices within the game world:

<!-- Explain how Sympathy is raised and dropped. Include an example. -->

| Modifier | Name     | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| :------: | :------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|    -4    | Hatred   | <!-- visceral, harm, affront, betrayal, wronged, despise, unresolved resentment, traumatic event, deep jealousy, spite<br><br>The character’s disdain for the other is palpable, their every word dripping with venom. They might go out of their way to undermine or harm the other, driven by a deep-seated loathing that colors every interaction. Even the sight of the other person stirs a visceral reaction, as if their very presence is an affront.<br><br>This intense animosity could stem from a deep betrayal, a history of conflict, or a perceived injustice. Perhaps the other character wronged them in the past, harmed someone they care about, or represents something they despise (e.g., an ideology, race, or faction). Hatred might also be fueled by envy, unresolved resentment, or a clash of values so severe that reconciliation feels impossible. -->                                                                                                               |
|    -3    | Aversion | <!-- avoidance, resentment, grudge, personality clash, negative encounter, lasting impression, dislike, witnessed behavior, envy, bitterness<br><br>The character feels a strong discomfort around the other, avoiding them whenever possible. They might not actively seek to harm them, but they recoil at the thought of closeness, as if the other person carries an invisible weight they can’t bear. Their body language is tense, and their words are clipped, betraying their unease.<br><br>Aversion often arises from discomfort with the other character’s behavior, appearance, or reputation. Maybe the other person reminds them of a traumatic event, embodies something they fear, or has a personality that clashes with their own. It could also stem from a single negative encounter that left a lasting impression, making them wary of further interaction. -->                                                                                                             |
|    -2    | Unease   | <!-- discomfort, wariness, distrust, treading carefully, heard rumors, sensing something “off”, lack of familiarity <br><br>The character is wary of the other, unsure of their intentions or presence. They might not dislike them outright, but there’s a lingering sense of distrust or discomfort. They tread carefully in conversations, always on guard, as if waiting for the other shoe to drop. Their tone is cautious, and their actions measured.<br><br>Unease might be caused by uncertainty about the other character’s motives or nature. Perhaps they’ve heard rumors about the other person’s dark deeds, witnessed their unpredictable behavior, or sensed something “off” about them. It could also stem from a lack of familiarity, especially if the other character belongs to a group or culture the character doesn’t understand or trusts. -->                                                                                                                           |
|    0     | -        | -                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
|    +2    | Openness | <!-- curiosity, relaxed, inviting, optimistic, empathetic, kindness, willingness, enjoying each other’s company, common goal<br><br>The character is willing to give the other a chance, approaching them with curiosity and a lack of preconceived judgment. They listen attentively, their body language relaxed and inviting. There’s a sense of optimism in their interactions, as if they believe in the possibility of connection or understanding.<br><br>Openness often comes from a place of curiosity, optimism, or a desire for connection. The character might see potential in the other person, whether as an ally, friend, or kindred spirit. They could be naturally empathetic, or perhaps they’ve experienced kindness from the other character in the past, sparking a willingness to engage without judgment. -->                                                                                                                                                             |
|    +3    | Fondness | <!-- camaraderie, connection, understanding, affection, joy, trust, shared experiences, sharing moments of vulnerability, shared hardships<br><br>The character holds a genuine affection for the other, finding joy in their presence. They might go out of their way to help or support them, their actions guided by a warm regard. Their tone is kind, their smiles frequent, and they often seek out opportunities to spend time together.<br><br>Fondness typically grows from positive shared experiences, such as working together toward a common goal, sharing moments of vulnerability, or simply enjoying each other’s company. The character might admire the other’s qualities, such as their humor, bravery, or kindness, and feel a sense of camaraderie or affection as a result. -->                                                                                                                                                                                            |
|    +4    | Love     | <!-- passion, care, nurture, loyalty, warmth, tender, comfort, develops over time, bond, family, profoundly changed their life for the better<br><br>The character’s feelings for the other run deep, encompassing care, loyalty, and a desire to protect or nurture. They prioritize the other’s well-being, often putting their needs above their own. Their actions are selfless, their words tender, and their presence is a source of comfort and strength. Love drives them to great lengths, even at personal cost.<br><br>Love often develops over time, rooted in deep emotional bonds, shared hardships, or acts of profound kindness and sacrifice. It could be romantic, platonic, or familial, but it’s always marked by a selfless desire to see the other person thrive. Love might also arise from a sense of destiny or connection, such as recognizing the other as a soulmate, a long-lost family member, or someone who has profoundly changed their life for the better. --> |

<!--

- **Hatred** : 

- **Aversion** : 

- **Unease** : 

- **Openness** : 

- **Fondness** : 

- **Love** : 

-->

<!-- "You begin cordially. Civility is not earned; it is lost." : https://www.reddit.com/r/Destiny/comments/1ggptci/forced_civility -->

<!-- Example idea : a character addresses their anger to get rid of a Breakdown state, and does something that pisses the rest of the group. Now all teammates have negative Sympathy towards the chracter. However, affected characters may still resist their Disposition change with a humility Resistance Roll (raising their anger and using a Pressure rating set by the GM). -->

<!-- Sympathy should go straight to -1 if the relationship performs a contentious check targeting the character (doesn't matter if they succeed or not?). -->

<!-- Sympathy should decrease by 1 when the relationship does something the character really disagrees with or finds reprehensible. -->

<!-- Sympathy should go straight to +1 if the relationship performs a successful cooperative check targeting the character (and the character was in need of help). -->

<!-- Sympathy should increase by 1 when the relationship does something the character really agrees with or finds commendable. -->


<!-- ## Bond score -->

<!-- - Increases : when the relationship succesfully performs a cooperative check targeting the character. -->

<!-- - Decreases* : when the character triggers a bond or when the relationship does something the character disagrees with or finds reprehensible. -->

<!-- When a character attempts a cooperative check aimed at a specific disposition, they can trigger a bond. To trigger a bond, the disposition's bond score must be strictly higher than its matching grudge score. Once triggered, a bond provides the player with an easing modifier equal to its score (for the current cooperative check), after which the bond score is decreased by 1. -->

<!-- When a character dies, inflict an amount of stress to other characters equal to the amount of bond they had with them. -->


<!-- ## Grudge score -->

<!-- - Increases : when the relationship performs a contentious check targeting the character (doesn't matter if they succeed or not?). -->

<!-- - Decreases : when the character triggers a grudge or when the relationship does something the character agrees with or finds commendable. -->

<!-- When a character attempts a contentious check aimed at a specific disposition, they can trigger a grudge. To trigger a grudge, the disposition's grudge score must be strictly higher than its matching bond score. Once triggered, a grudge provides the player with an easing modifier equal to its score (for the current contentious check), after which the grudge score is decreased by 1. -->


<!-- ALTERNATIVE RULE : reduce the amount of bond and grudge a character can accumulate up to 3 instead of 5. Now, consuming either of these resources doesn't provide an easing modifier, but instead works as if the character is performing a check "aligned" with one of its drives, thus giving them a chance to remove all of their stress if they succeed it. -->


### - Respect

*Respect* reflects the perceived value or status the target holds over the character. It is based on their accomplishments, recognized skill or significance within the game world:

<!-- Explain how Respect is raised and dropped. Include an example. -->

| Modifier | Name         | Description                                                                                                                                                                                                                                                                                                                                                                                                                     |
| :------: | :----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|    -4    | Neglect      | <!-- The character doesn’t even consider the other worthy of attention because they’ve shown no evidence of skill, accomplishment, or significance. Perhaps the other person has consistently failed to prove themselves, committed embarrassing mistakes, or remained entirely unnoticed in the world. The character sees them as irrelevant, not even worth the effort of forming an opinion. -->                             |
|    -3    | Disregard    | <!-- The character is aware of the other but actively dismisses their value because of their proven incompetence or lack of tenure. Maybe the other person has a history of hard fumbles, poor decisions, or failures that have undermined their credibility. The character sees them as a liability or a joke, unworthy of respect or serious consideration. -->                                                               |
|    -2    | Indifference | <!-- The character acknowledges the other’s existence but feels no particular respect for them. This might be because the other person’s actions have been consistently mediocre, their accomplishments unremarkable, or their role in the world negligible. They haven’t done anything to earn disdain, but they also haven’t done anything to earn admiration. The character simply doesn’t care. -->                         |
|    0     | -            | -                                                                                                                                                                                                                                                                                                                                                                                                                               |
|    +2    | Notice       | <!-- The character begins to see the other as someone of potential value, though their respect is tentative. Perhaps the other person has shown flashes of skill, overcome a minor challenge, or demonstrated a willingness to improve despite past missteps. The character is paying attention, curious to see if they can prove themselves further. -->                                                                       |
|    +3    | Recognition  | <!-- The character acknowledges the other’s skill, accomplishments, or significance as noteworthy. This respect is earned through consistent performance, overcoming significant challenges, or demonstrating expertise in their field. The character sees them as a competent and reliable individual, someone who has proven their worth and earned their place in the world. -->                                             |
|    +4    | Awe          | <!-- The character holds the other in the highest regard, seeing them as extraordinary or even legendary. This level of respect is reserved for those whose skills, accomplishments, or significance are unparalleled. The other person has achieved feats that defy expectation, overcome impossible odds, or shaped the world in profound ways. The character views them with admiration, inspiration, or even reverence. --> |

<!--

- **Neglect** : 

- **Disregard** : 

- **Indifference** : 

- **Notice** : 

- **Recognition** : 

- **Awe** : 

-->




## Social Actions


Once the player has stated their intent and is ready to roll, the GM chooses the social action that best fits the situation (*persuade*, *deceive*, *plead* or *threaten*).

>*Social Actions are a type of Action Roll that apply specifically to this system.  While they resolve in the same manner, they also factor in the target's Disposition toward the character both before and after the roll.*

Social actions also require the combination of  two abilities, with *eloquence* being the most commonly used. However, there can be exceptions where a different ability may come into play. For example, *agility* may be used for seducing with dance moves, or *strength* for impressing with physical prowess.

<!-- These are just Action Rolls used to tick segments on an Influence Clock. -->

#### Assessing Challenge & Risk

**Challenge** should be evaluated based on the target's acumen and general social skills. Are they perceptive enough to uncover the character's lies? Do they possess the ability to notice subtle hints in the character's behavior? This choice is at the GM's discretion, but for fully-fledged NPCs, a designated ability score could serve as the baseline for this value:

|Ability Score |Challenge Rating |
|:------------:|:---------------:|
|1-2           |2                |
|3-4           |3                |
|5             |4                |

>*To get an idea of which ability to pick, check the 'Resisting Influence' section below.*

<!-- Include the unused "positive sympathy" and "positive respect" here...? -->

#### Choosing Abilities

#### Disposition Modifiers

<!-- Character Disposition is used as a Means Modifier, aligned with the Target's Disposition towards the Instigator. -->


### - Persuade

<!-- If positive Sympathy, more likely to succeed. If negative Sympathy, more likely to fail. -->

*Persuading* is the most straightforward way of exerting influence, allowing characters to sway others through argumentation, logic or even emotions (depending on the situation and their approach).

>*This action shouldn't involve trickery, coertion or begging, and cannot be performed if the target harbors negative sympathy towards the character.*

Abilities used for *persuading* depend on the goal, context and methods used. For instance, convincing someone to lend a piece of equipment might require *eloquence* + *insight* (to tailor one's words accordingly), or *eloquence* + *humility* (if the character wants to demonstrate reliability and assure the safe return of the item). Similarly, *eloquence* + *memory* (or *reasoning*) could be used for swaying opinions in a debate or persuading NPCs to change their political views.

<!-- Default Social Action. Include Ability and/or Modifier examples. -->

<!-- Frustration Breakdown : roll with Disadvantage for Persuade rolls? etc... -->

<!-- Provide example -->

<!-- Aristotle's Rhetoric (Modes of persuasion) : https://en.wikipedia.org/wiki/Modes_of_persuasion -->

<!-- Ethos (appeal to authority), Pathos (appeal to emotion), Logos (appeal to logic) -->

<!-- Persuation shouldn't work on a character suffering an anger breakdown -->


### - Deceive

<!-- If positive Sympathy, more likely to succeed. If negative Sympathy, more likely to fail. -->

<!-- Reduces Target's Sympathy towards the Instigator as soon as they realize they're being deceived. Include Ability and/or Modifier examples. -->

*Deceiving* works like *persuading*, except the character is hiding their true intentions, and entails an outcome that hurts or doesn't benefit the target. This action should involve manipulation, trickery or cunning (including fabricating lies, using misleading information or exploiting vulnerabilities for personal gain).

>*This action, akin to persuasion, cannot be performed if the target harbors negative sympathy toward the character. And regardless of outcome, as soon as the victim realizes they have been (or are being) deceived, they should decrease their sympathy towards the character by one level.*

<!-- cannot deceive someone you have sympathy towards as well? -->

Abilities used for *deceiving* encompass *charm*, *creativity* and sometimes *dexterity*. For example, along with *eloquence*, weaving a convincing lie might require a combination of *charm* to disarm suspicion, *creativity* to fabricate a believable narrative, or *dexterity* to subtly conceal or manipulate objects. Additionally, *insight* could also be crucial for gauging the target's reactions and adjusting the deception accordingly.

<!-- Deceive : Once an Influence Clock is beaten, and the trick action was used at least once, the victim should recognize they've been duped in some way or another and gain negative sympathy towards the character. -->

<!-- Provide example -->


### - Order

<!-- If positive Respect, more likely to succeed. If negative Respect, more likely to fail. -->

<!--- Reduces Target's Respect towards the Instigator if a Daring Roll was performed. Include Ability and/or Modifier examples. -->

<!-- If successful, these double the Progress Clock step increase (2 steps for normal success, 4 steps for critical success). -->


### - Threaten

<!-- If positive Respect, more likely to succeed. If negative Respect, more likely to fail. -->

<!-- Reduces Target's Sympathy towards the Instigator and Respect if a Daring Roll was performed. Include Ability and/or Modifier examples. -->

<!-- If successful, these double the Progress Clock step increase (2 steps for normal success, 4 steps for critical success). -->

*Threatening* is the act of using intimidation or coercion to force a target to comply with the character's demands. This action can range from subtle implications of punishment to explicit threats of violence or harm.

>*This action cannot be performed if the target harbors negative respect toward the character. And once this action is performed, regardless of outcome, the target should decrease their sympathy towards the character by one level.*

This action relies on the character's capacity to instill fear, assert dominance, and communicate consequences effectively. Abilities used for *threatening* usually involve *eloquence* to articulate the threat convincingly and *strength* to convey physical prowess or readiness to enforce it.

<!-- Anger Breakdown : roll with Advantage for Threaten rolls? -->

<!-- MORE SOCIAL ACTIONS -->

<!-- Bluff : works like Deceive but for threats; and uses Respect instead of Sympathy. It decreases the target's Respect towards the character (once they realize the threat was hollow). -->

<!-- Vent/Rant/Spill: forces another character to 'agree' with you, reducing your Emotional Strain or possibly removing some Despair. It can also be used to change the target's views and opinions, but only if they don't have negative respect towards you. -->

<!-- - Detect? Scout? Survey? Reveal? Uncover? : -->

<!-- From Exalted: The read intentions action allows a character to discern what another character wants to achieve in a scene of interaction. On a success, the Storyteller should give a brief description of what the character wants out of the interaction: “He’s seducing you to get you alone,” or “She’s making polite small talk and waiting for this party to end." -->

<!-- Alternatively, this action can be used in order to determine what Intimacies a character has. Before rolling for the action, the player should generally describe what kind of Intimacy he wants to discern (“Does he love anyone?” “How does he feel about me?” “Is he a devout Immaculate?”). On a success, the Storyteller chooses and reveals one of the target’s Intimacies that fits those criteria. If there is no Intimacy that does, the Storyteller should let the player know this. -->

<!-- The read intentions action is not a form of magic. The character using it is analyzing the target’s words and behavior to get a better feel for his motives and Intimacies, and the Storyteller should use common sense in deciding how much information can be gleaned from a character’s behavior and appearance. You might deduce that a young princeling is in love from a look of longing in his eyes or a wistful sigh, but discerning his paramour’s identity might be impossible unless she’s physically present or if he’s carrying some evidence of her identity. -->

<!-- Reading someone’s intentions is not an influence roll—instead, it is a (Perception + Socialize) roll against the target’s Guile. -->

<!-- - Read Intention : a character who is unaware he’s being observed suffers a -2 penalty to his Guile. -->

<!-- The Guile trait represents a character’s ability to conceal his thoughts and innermost feelings. A character with high Guile reveals little about himself through his expression, posture, and speech, while a character with low Guile wears his heart (and Intimacies) on his sleeve. Guile is typically used to defend against the read intentions action. -->

<!-- Eloquence should be used to resist a read intention action? Here's a good example of what I'm thinking about: https://www.youtube.com/watch?v=J4TtF4LffI4&t=1445s -->

<!-- Provoke : -->

<!-- Taunt : explain this in combat rules? -->

<!-- Raise a target's emotional strain (Taunt = Anger, Harass = Frustration, Ridicule = Insecurity, Frighten = Fear, Reprimand = Guilt). This action can only be performed if the target harbors positive respect toward the character. -->

<!-- From Exalted - Inspire : The inspire action is used to incite emotions and strong passions in the hearts of others, usually with the Performance Ability, which those others then act on. When a player takes an inspire action, he chooses which emotion he is attempting to inspire—anger, sorrow, hope, lust, hatred, delight, or any other. On a successful inspire roll, the target is impassioned according to the emotion evoked—but the target’s player chooses what form that passion takes. -->

<!-- Comfort : -->

<!-- Help a target recover from Stress. This action can only be performed if the target harbors positive sympathy toward the character. -->




## Resisting Influence


Characters can also be the targets of influence rolls made by others, and it's up to the player to decide whom to trust and whom to reject. To resist an influence attempt, the charater performs a **Resistance Roll** using an ability chosen by the GM that best suits the situation:

- **Composure** is used to resist impressions or emotional manipulation, like intimidation, seduction or temptation.

- **Insight** is used to call bluffs, detect lies or assess leverage.

- **Reasoning** or **Memory** : are used to identify inaccurate information, inconsistencies or pseudoscientific babble.

<!-- Humility -->

>*The Pressure rating for the roll is also left at the GM's discretion, and should be roughly based on the NPC's persuasive or social skills.*

<!-- ## Example -->

<!-- From Chronicles of Darkness: Stacy wants Professor Erikson to loan her a book from his private library. She intends to use the book’s contents to summon a demon, but Erickson doesn’t know that. Erickson is protective of his books, but he’d be willing to loan one out under the right circumstances. Erickson has Resolve 3 and Composure 4, so the base number of Doors Stacy needs to open is 3 (the lower of the two). Loaning out of a book wouldn’t be a breaking point, nor does it prevent him from achieving an Aspiration, but it does work against his Virtue (Cautious), so the total number of Doors Stacy needs to open to get the book is 4. -->

<!-- The Storyteller decides that the first impression is average; the two know one another (Stacy is a former student of Erickson’s), but they aren’t close. Stacy arranges to find Erickson at a conference and impresses him with her knowledge of esoteric funerary rites. This requires an Intelligence + Occult roll, plus whatever effort Stacy had to put forth to get into the conference, but changes the impression level to “good.” Now, Stacy can make one attempt to open Doors per day. At the conference, Stacy’s player rolls Manipulation + Persuasion and succeeds; one Door opens. Stacy mentions the book to Erickson and lets him know she’d like to borrow it. He’s not immediately receptive to the idea, but Stacy’s in a good place to continue. -->

<!-- The next day, Stacy emails the professor about a related work (Manipulation + Academics), but fails. Future rolls will have a -1 penalty. The Storyteller decides that the impression level slips to average. -->

<!-- Stacy still has to overcome three Doors. She spends the next week doing research into Erickson and discovers that he wants to become a respected academic. She tells Erickson that she has a colleague who can help break the cipher in which the book is written. This removes one Door without a roll. Now she must overcome two more before he’ll agree. (Note that even if Stacy has no intention of helping Erickson in his quest toward academic glory, as long as he reasonably believes that lending her the book will help him achieve his Aspiration, it opens the Door.) -->

<!-- During her research into the professor’s personality, she also learns that his Vice is Vanity; he likes to see himself as the hero. Stacy goes to his office in tears, saying that she is in danger of being accused of plagiarism for copying a paper, and asks if he can help authenticate her work. Doing this allows him to come to her rescue, which in turn lets him soak up some praise; this would allow him to regain Willpower through his Vice, and as such is enough of a temptation to raise the impression level back to good. Stacy’s player rolls Manipulation + Expression for Stacy to compose a letter of thanks to him, and achieves an exceptional success. The last two Doors open, and Erickson offers to let Stacy borrow the book for a weekend. He probably even thinks it was his idea. -->

<!-- On the other hand, if Erickson is a player-controlled character, his player might decide he really doesn’t want to let that book out of his sight. He might offer an alternative — he’ll bring the book to Stacy, and let her use it for an afternoon. That, of course, might complicate her intended demon summoning, but she does get to put the Flattered Condition on Erickson. -->

<!-- - With Hard Leverage : In the example above, assume Stacy really needs that book now. She goes to Erickson and threatens him at gunpoint to give up the book. Doing this is definitely a breaking point for Stacy. She applies a modifier for her Integrity, and then a modifier based on the severity of the action and the harm it does to her self-image and psyche. She’s not in the habit of committing violent acts and Erickson is obviously terrified, so the Storyteller assigns a -2 modifier to the breaking point roll. This being the case, one Door is removed. If she’d shot him the leg to let him know she was serious, the breaking point modifier would have been at least -3, which would have removed two Doors. In either case, her player rolls Presence + Intimidation plus any bonus for the gun, minus the appropriate penalty. -->

***








# Social Influence (Part 2)

***




## General Modifiers


Depending on which social action is being performed by the character, **Modifiers** don't always have the same utility. Here are some examples:

- **Environment** reflects the suitability of the location in which the influence is being performed. For instance, a dark alley or any place that feels unsafe would yield a positive *environment* to a *threaten* roll, while it would impose a negative *environment* to a *persuade* roll. Similarly, being in a hospital or a church would benefit a *plead* roll, but hinder a *deceive* roll.

- **Means** represent leverage or bargaining power. When attempting to *persuade*, this means offering something in exchange for the request. This can include bribes, goods, services or information tailored to the target's needs, provided it's perceived as worth the sacrifice being asked. When attempting to *threaten*, this means leveraging fear and coercion. Instead of offering something desirable, the target is presented with something undesirable, like hurting someone or something they care about, social blackmail, financial ruin etc... This should work as long as the consequences of refusal are perceived as worse than complying with the request.

- **Condition** encompasses various factors such as fatigue, substance intoxication or general appearance. Typically, a positive *condition* should be granted if the character is well dressed, clean or presentable, while a negative *condition* should be applied if they're drunk, wounded, tired or dirty. However, in the case of a *plead* roll, being in a poor state (such as being injured or wounded) should result in a positive *condition*, as it may evoke sympathy or elicit a compassionate response from the target.




## Acting against Disposition


- Option 1 : roll with Disadvantage.

- Option 1 : Daring Rolls raise 2 Emotional Strain instead of 1.




## Affecting Goals and Beliefs


<!-- Change/Give up Drive/Quest : -->

<!-- Make a character just give up their Quest? (without trying to replace it). This would make them enter a specific Breakdown state where they are depressed or aimless. -->

<!-- From Exalted: The instill action is used to change the feelings and beliefs of others. When a player takes an instill action, he declares what he wants to make his target feel or believe. The Storyteller may apply penalties to the roll if this belief is particularly implausible or hard to accept, up to a maximum penalty of -5 for truly unbelievable claims. On a successful roll, the target forms an Intimacy towards that belief. However, there are limits to what someone will believe when they already have strong opinions to the contrary. -->

<!-- This is basically a "Persuade" + "Pivotal Influence Clock" -->




## Affecting Emotions


- Explanation

- This can be used to force the Target to address their Emotion if they're in a Breakdown State. Stress cannot be inflicted this way (unless the Target decides to resist).


### - Learn / Detect / Scout / Survey


### - Affecting Sympathy

- Sympathy can be raised through Persuasion?

#### Jokes/Humor

- Help restore some Emotional Strain?


### - Triggering Emotions / Provocation / Incitement

- Only uses Respect modifier?

- May cause the Target to lose Sympathy.

#### Anger

- Taunting, Provoking, Deriding, Mocking, Insulting

#### Anxiety

- Scaring, Troubling

#### Sorrow

- Tormenting, Distressing, Afflicting

#### Guilt

- Reprimanding, Admonishing, Criticizing, Lecturing

***








# Healing & Recovery

***




## Therapy/Stress recovery


- Counts as a Social Action that uses the Sympathy Modifier.
- Rules about acting against your own Disposition apply here too.

***








# Character Progression

***




- Explain all the Ability XP rules here?

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***

***








# Random/General




- Replace "wise abilities" with "spiritual abilities"

- Make printable **Breakdown** state cards?

- Stealth mechanics inspiration : https://ftd-srd.opengamingnetwork.com/home/running-the-game/stealthy-options/

#### Character Sheet

>https://www.rocketorca.com/post/designing-better-character-sheets-part-1

>https://www.youtube.com/watch?v=vcvK1oUszsA

<!-- Remember to add a Fatigue counter somewhere on the character sheet. -->

#### Nature/Race Modifiers

- *Night Eye* : the character ignores any Trying Environment Modifier imposed by obscurity or night time.

- *Aquatic Affinity* : the character gains a minor Condition Modifier (+2) when performing actions where underwater movement is involved.

<!-- https://en.uesp.net/wiki/Category:Morrowind-Races -->

<!-- https://wiki.travellerrpg.com/Race/summary -->

#### Social Circles

- Six degrees of separation : https://en.wikipedia.org/wiki/Six_degrees_of_separation

- Burning Wheel : https://rpg.stackexchange.com/questions/26636/how-do-i-meet-people-outside-my-circles

<!-- include character bonding mechanics? (Bonds-Relationships.md) -->

#### Character Identity (Culture, Faith, Religion...)

- Remove "liberal" vs traditional" cultures. Have players choose if they want their characters to be "liberal" or "traditional" instead. And just say that some cultures may have more "liberal" or "traditional" people in them than others.

<!-- The Riddle of Steel : Faith reflects the bond between man and deity. Belief is an important issue in Weyrth, for a man without beliefs is not a man. Any character may posses a degree of Faith—even atheists, if they hold strongly to their atheism. -->

#### Skills

- New characters shouldn't be able to cast spells. They should go through some form of training and learn how to perform magic first.

<!-- The Riddle of Steel : p.33, p.54 -->

<!-- Learning Skills : "Hungry rats learn faster - Dr K & Destiny" : https://www.youtube.com/watch?v=JHNCPgcfqfQ&t=8295s - https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4482359/ -->

#### Blessings / Favors / Gifts ?

Implement these for characters who complete a Quest without a tangible or material reward (e.g., fulfilling a revenge, achieving peace in a conflict, or participating in a significant artistic event). These should function as 'special powers' (similar to those in Morrowind) that can be useful in specific situations.

>*For example, a character could gain the Blessing "Satisfied" after fulfilling a desire in a hedonistic quest. This Blessing would allow them to recover 1 Stress per day/week after succeeding a simple Humility roll.*

<!-- "Curses" / "Afflictions" would obviously work as the opposite of "Blessings". -->

***








# Inspirations




## To Buy


- [**13th Age**](https://www.drivethrurpg.com/en/product/118994/13th-Age-Core-Book)

- [**Forbidden Lands**](https://freeleaguepublishing.com/games/forbidden-lands)

<!-- 7 reasons why Forbidden Lands is one of the best RPG settings of all time : https://www.youtube.com/watch?v=xvDn53uBtZY -->

- [**Burning Wheel**](https://www.drivethrurpg.com/en/product/448187/burning-wheel-gold-revised)

- [**Torchbearer**](https://www.burningwheel.com/torchbearer-2e-core-set-pdf-bundle)

- [**Mouse Guard**](https://www.mouseguard.net/rpg)

- [**Glitter Hearts**](https://www.leatherman.games/games/glitter-hearts)

- [**Thirsty Sword Lesbians**](https://evilhat.com/product/thirsty-sword-lesbians/)

- [**Traveller**](https://www.mongoosepublishing.com/products/traveller-core-rulebook-update-2022)

<!-- https://online.anyflip.com/lqark/xtim/mobile/index.html -->




## Year Zero Engine - Very similar game mechanics as mine


>https://forum.rpg.net/index.php?threads/resolution-mechanic-for-a-ttrpg-new-revised-version.911082/page-3

"One thought that might provide grist for your mill is that this has ended in a similar design space to a couple of the Year Zero Engine games. The push/daring & stress interaction has similarities to Alien and the combination of two things dice-step is in a space with Twilight 2000. This is not a criticism, they're both really good games. But you might get some insight from how they implemented those features."

- **Year Zero Engine**
>Check `DATA/Resources/GAMEPLAY/TTRPG inspirations/[TO READ]/Year Zero Engine/`
>https://rpggeek.com/rpgsystem/43636/year-zero-engine
>https://www.youtube.com/watch?v=e47dWAOlO-U

- **Alien RPG**
>Check `DATA/Resources/GAMEPLAY/TTRPG inspirations/[TO READ]/Year Zero Engine/`
>https://online.anyflip.com/aeoqf/frgt/mobile/index.html
>https://www.drivethrurpg.com/product/293976/ALIEN-RPG-Core-Rulebook?cPath=27806_34036
>https://www.legrog.org/jeux/alien

- **Twilight 2000**
>Check `DATA/Resources/GAMEPLAY/TTRPG inspirations/[TO READ]/Year Zero Engine/`
>https://www.youtube.com/watch?v=RhJEiJ_4260




## Genesys - Social Encounters


>genesys-core-rules p.119

***








# Social Influence




## Social Actions


#### Sympathy Modifier

- **Persuade** : 

- **Deceive** : reduces **Target**'s *sympathy* towards the **Instigator** as soon as they realize they're being deceived.

#### Respect Modifier

These double the Progress Clock increase step?

- **Order** : reduces **Target**'s *respect* towards the **Instigator** if a **Daring Roll** was performed.

- **Threaten** : reduces **Target**'s *sympathy* towards the **Instigator** and *respect* if a **Daring Roll** was performed.

<!--- **Implore** : reduces **Target**'s *respect* towards the **Instigator**. -->




## Disposition


### - Test 1

#### Values

- Character **Disposition** can be used as a *means* **Modifier**, aligned with the **Target**'s **Disposition** towards the **Instigator**:

| Sympathy | Modifier    | Description |
| :------: | :---------- | :---------- |
|    -3    | Decisive    | Hatred      |
|    -2    | Significant | Aversion    |
|    -1    | Minor       | Wariness    |
|    0     | -           | -           |
|    +1    | Minor       | Fondness    |
|    +2    | Significant | Trust       |
|    +3    | Decisive    | Love        |

| Respect | Modifier    | Description                    |
| :-----: | :---------- | :----------------------------- |
|   -3    | Decisive    | Neglect                        |
|   -2    | Significant | Disregard                      |
|   -1    | Minor       | Indifference                   |
|    0    | -           | -                              |
|   +1    | Minor       | Notice <!-- Acknowledgment --> |
|   +2    | Significant | Recognition                    |
|   +3    | Decisive    | Awe                            |

<!-- Remove *environment* **Modifiers** completelly. Environment should be accounted for when setting **Difficulty** or **Pressure**? -->

<!-- Combine *environement* and *actors* modifiers into *situational* modifiers...? -->

<!-- Add *disposition* **Modifiers**. These should apply to situations other than social influence, like when trying to help or hurt other people. Only take into consideration the *disposition* of the target though, not the instigator (as in, persuading or attacking somebody that likes you should be easier than someone that doesn't). -->

#### Emotional Rolls

<!-- Perform **Emotional Rolls** on the instigator if they act against their inner *dispostions* (tricking someone you like = *guilt* roll, intimidating someone you respect = *anxiety* roll etc...). The **Strain** value should be based on the *disposition* level of the instigator. -->

#### Resting/Daily Roll

- Recovery system where, if you like a character and take a break with them you recover emotional strain, but if you don't (aka you hate them) then you lose emotional strain instead?

#### Problems/Shortcomings

- Right now, having good *sympathy* towards someone mostly brings drawbacks with no benefits.

- There's also no much use for *respect* either (it's only relevant when trying to intimidate). SOLUTION => remove *respect* entirely?

- Stealing a bag of chips from a friend causes the same amount of *guilt* as breaking their leg on purpose.


### - (Reminder) Bonus/Malus that affect rolls

- Base Modifiers (Environment, Actors, Means, Condition)

- Nature Modifier (race, sex etc...)

- Character disposition

- Drive Actions

- Beliefs?

- Breakdown state (Advantage/Disadvantage)

***








# Emotions

***




- Replace "Guilt" with "Shame"?

- Insecurity?




## New Categorization


- Change **Despair** back to **Stress**? (or introduce a new term, like **Struggle** maybe...?)

<!-- https://x.com/ybarrap/status/1827893533694374208 -->

<!--

|Emotion |        |         |
|--------|--------|---------|
|Anger   |Dynamic |Agency   |
|Anxiety |Dynamic |Weakness |
|Sorrow  |Dull    |Weakness |
|Guilt   |Dull    |Agency   |

-->

#### Action Rolls

- **Anger** : The character acts out of frustration, resentment, or animosity. They feel wronged or irritated, and believe they can resolve the issue through assertiveness or confrontation.

- **Anxiety** : The character is acting with apprehension, nervousness or insecurity. They feel threatened or in danger, plagued by fear or second-guessing their decision.

- **Sorrow** : The character is acting from a place of sadness, pessimism or half-heartedness. They may feel burdened or crushed, they don't like what they're doing or don't understand the purpose of it.

- **Guilt** : The character is acting against their values or principles, or feeling responsible for their action. They believe they shouldn't be doing it or fear hurting someone.

#### Resistance Rolls

- **Anger** : The character feels irritated or wronged by the event.

- **Anxiety** : The character feels threatened, vulnerable or in danger.

- **Sorrow** : The character feels jaded, overwhelmed or powerless.

- **Guilt** : The character feels remorse or responsibility for the event.

#### Addressing the Emotion

- **Anger** : The character must hurt someone or break something important. <!-- or fling themselves into some form of cheap relief? -->

- **Anxiety** : The character must desert the group at a critical moment. <!-- or take a foolhardy action that puts them in a difficult situation? -->

- **Sorrow** : The character must take a temporary leave from the group. <!-- or give up an important task? -->

- **Guilt** : The character must make an important sacrifice to redeem themselves.

#### Fallouts

- **Anger** :

- **Anxiety** :

- **Sorrow** :

- **Guilt** :




## Emotional Rolls


- "nice" events that aren't directly related to the character's goals : recover Emotional Strain?

- "nice" events where the character has a strong personal stake or involvement : recover Stress?

***








# Therapy

***




A character may remove some of their **Despair** if another character provides some form or therapy or guidance for them. **Despair** can be alleviated by forming bonds, sharing intimate moments, and seeking counseling, which helps restore emotional balance and build confidence.

<!-- Characters have the opportunity to remove their permanent stress by forming bonds with other characters and share intimate moments with them. They can seek counseling and advice from friends to regain their emotional equilibrium and build back their confidence. Opening up and sharing life stories with people can create a sense of trust and understanding, and this emotional connection facilitates the mitigation of sadness and sorrow. -->

<!-- Players are encouraged to develop their character's backstories throughout this system, rather than solely relying on it at character creation. Players have the opportunity to shape their characters' narratives as they progress through a game, usually during moments of downtime and relaxation. By forming bonds with other characters and removing permanent stress, players actively participate in the growth and development of their own character's backstories. -->

<!-- A character can only help another character recover from stress (aka perform "therapy") if they have positive sympathy towards the target (and if they share the same beliefs/political positions as them?). -->








# Combat

***




## Aim/Goals


- The problem with most TTRPGs is that it’s often unrealistically difficult for combatants to land strikes on each other. It would be more logical for two fighters using similar weapons to hit each other rather than both missing. The real challenge should be striking without being hit in return.

- Shields will absorb X damage from incoming attacks, until they take more damage than X, at which point they break.




## To Do


- **Time Management** : *rounds*, *turns*, etc...

- **Space Management** : *zones*, *distance*, *movement*, etc...

- **Ranged Combat**

<!-- https://www.youtube.com/watch?v=8hKokL1PdC4 -->

- **Mounted Combat**?




## Brainstorming/Ideas


- Remove **Combat Clocks** and just use **HP** for enemies.


### - Challenge & Risk

- **Challenge** : roughly measures how effective the weapon is at dealing with the target (warhammer/double-axe vs big monster = lower challenge, warhammer/double-axe vs small monster = higher challenge, knife/dagger vs big monster = higher challenge, knife/dagger vs small monster = lower challenge)

- **Risk** : roughly measures how far/how much range the character's weapon has over their opponent (bare fists = precarious, regular weapon = risky, pike/polearm = safe etc...)

<!-- Challenge is defined by the type of weapon used in the fight. For instance, hitting or hurting a strong or large enemy with a polearm is easier than with a shortsword. Similarly, a swift and agile opponent might be easier to strike with a dagger than with a heavy mace. -->

<!-- Risk is influenced by several factors. For example, using a weapon without proper training, being too close to the target or fighting without a shield exposes the character to greater danger. -->


### - Abilities used

- **Light Weapons** (knife, dagger, shortsword...) : *dexterity* + *agility*

- **Medium Weapons** (longsword, spear, mace...) : *dexterity* + *strength*

- **Heavy Weapons** (greataxe, claymore, warhammer...) : *dexterity* + *endurance*


### - Abilities used (alternative)

- **Light Weapons** (knife, dagger, shortsword...) : *dexterity* + *agility*

- **Heavy Weapons** (longsword, spear, mace, greataxe, claymore, warhammer...) : *dexterity* + *strength*

>*Heavy Weapons require the character to have a minimum score of endurance in order to be used (2 endurance for longswords and claymores, 3 for greataxes and warhammers etc...*




## Rule Draft 0 - Exchange


Melee combat doesn't work in "turns". Instead, characters perform "exchange" rolls (aka **Action Rolls**):

- **Success** : the character lands a hit and their opponent misses.

- **Limited Success** : the character lands a hit, but so does their opponent.

- **Limited Failure** : both the character and their opponent miss. If no **Daring Roll** was performed, the opponent still gets an opportunity to strike, and the character has the option to dodge or block the attack.

- **Failure** : the character misses and their opponent lands a hit.

<!-- Failure (without Daring Roll) : opponent doesn't land a hit but something bad happens? Character loses weapon, falls down etc... -->

>*The damage a character receives from an opponent's landed hit depends on the "weapon/attack dice" of the opponent.*

>*Worn armor reduces damage taken.*

<!-- Implement armor condition? -->

<!-- ALTERNATIVE? -->

<!-- Success : the character lands a hit and their opponent misses. -->

<!-- Limited Success : the character lands a hit, but so does their opponent. If no Daring Roll was performed, the character can perform an evasion or a block. -->

<!-- Limited Failure : the character misses. If no Daring Roll was performed, the opponent also lands a hit, and the character can perform an evasion or a block. -->

<!-- Failure : the character misses and their opponent lands a hit. -->


### - Damage

|Damage Class |Dice |Example |
|:-----------:|:---:|:-------|
|1            |d4   |Dagger, Staff, Wolf bite |
|2            |d6   |Mace, Shortsword, Spear |
|3            |d8   |Axe, Longsword |
|4            |d10  |Pike, Halberd, Claymore |
|5            |d12  |Great Axe, Warhammer |
|6            |d20  |Huge damage (elephant, dragon, catapult etc...) |

<!-- D&D Weapons Table (scroll down) : https://roll20.net/compendium/dnd5e/Weapons#content -->

- **Notation** : *d6:1* means the player rolls 1d6. *d4:2+* means the player rolls 2d4 and takes the highest result. *d10:3-* means the player rolls 3d10 and takes the lowest result etc...

- When a player scores a **Critical Success**, they add the maximum value of their damage die (e.g. 6 for a d6, 8 for a d10...) to their damage roll.

- Worn **Armor** (chainmail, cuirass...) simply reduces the amount of damage taken.

<!-- D&D Character HP calculator : https://www.5ehp.com/ -->


### - Enemy Opportunity

If an exchange (performed without **Daring Roll**) ends in a *limited failure*, the opponent gets a chance to strike. They attack, but the character also gets the option to **Evade**, **Block** or **Parry** (aka nullify) any incoming damage.

<!-- ALTERNATIVE? During an exchange, if the character didn't perform a Daring Roll and their opponent landed a hit, the character has the option to evade, block or parry the blow. -->

#### Evading

When **Evading**, the character simply performs a **Resistance Roll** using their **Agility**. The **Pressure** for the roll is set by the GM, depending on the power of the incoming attack.

#### Blocking

If the character is holding a shield, they perform **Block** instead. Shields have two stats, *protection* and *resistance*:

- **Protection** measures how effectively the shield defends its bearer. Shields with higher *protection* (e.g. tower shields, pavise shields...) are larger and more cumbersome, while shields with lower *protection* (e.g. bucklers, round shields...) are smaller and easier to carry around.

- **Resistance** measures a shield's durability and how much damage it can withstand before breaking. Shields with higher *resistance* (e.g. metal shields) are typically heavier, while those with lower *resistance* (e.g. wooden or leather shields) are generally lighter.

<!-- Types of shields : Round Shield, Buckler Shield, Kite Shield, Heater Shield, Tower Shield, Pavise Shield -->

<!-- https://medievalbritain.com/type/medieval-life/weapons/medieval-shield -->

<!-- https://www.medievalchronicles.com/medieval-armour/medieval-shields/medieval-shield-parts/ -->

<!-- https://www.themedievalguide.com/medieval-shield-types -->

<!-- https://neutralhistory.com/how-were-medieval-shields-made -->

When **Blocking**, the character performs a **Resistance Roll** using their **Endurance**. The **Pressure** for the roll is set by the GM, depending on the power of the incoming attack:

- If the number of **Trouble Die** matches or exceeds the shield's *protection*, the character staggers or flinches, allowing the opponent's attack to go through. The character takes damage as if they were directly hit.

- If the number of **Trouble Die** is less than the shield's *protection*, the character successfully blocks the attack, negating all incoming damage. However, if the damage class of the attack is equal or greater than the shield's *resistance*, the shield also breaks and cannot be used until repaired.

<!-- Low/High Protection : 1/4 or 2/4? -->

<!-- Low/High Resistance : 2/5 or 2/4? -->

<!-- SIMPLER RULE? When Blocking, the character performs a Resistance Roll using die based on their shield's protection. The GM sets the Pressure based on the power of the incoming attack. The shield then gains 1 deterioration point for each Trouble Die rolled. If deterioration meets or exceeds the shield's resistance, the shield breaks and becomes unusable until repaired. -->

<!-- Once a shield's deterioration hits its maximum, it cannot be used nor repaired anymore (although it can still be used for scrap parts)? -->

<!-- Low/High Protection : d4/d12 -->

<!-- Low/High Resistance : 2/6 -->

>*Regardless of the outcome, no Emotion is raised when a character chooses to block an attack.*

<!-- From Reddit - my shield mechanic: melee is an opposed roll, and if you lose, your opponent has to beat your roll by your shield rating (1 to 5) or your shield takes damage. 3 to 5ish hits, depending on the weapon, will usually break a shield. -->

<!-- Shields are wonderfully useful, but they are both perishable and exhausting. They have a high vigor requirement, and if you tire beyond that amount you can no longer lift the shield. -->

#### Parrying

**Parrying** works exactly like **Blocking** but instead of using a shield, the character uses their hand weapon. This only works if the opponent is also fighting with a weapon (they're not a wild creature for instance), and if the character is trained in a skill like *parrying* or *swordfighting*.


<!-- ### - Duels

When two characters scuffle with each other, they perform two opposed **Action Rolls** (both performing their initial roll and optional **Daring Roll** at the same time):

- The **Challenge** to hit the opponent is equal to 20 minus 5 for each **Trouble Die** they roll. **Modifiers** still apply. -->

<!-- Both players should roll their standard roll and Daring Roll at the same time. -->

<!-- having momentum, being on the backfoot -->




## Rule Draft 0 - Bestiary


<!-- D&D Creature HP : https://blackcitadelrpg.com/creature-size-5e/ -->

- **Wolf** : HP 8 / ATK d4

<!-- (D&D) HP 11 / ATK 2d4+2 (bite) : https://www.dndbeyond.com/monsters/17062-wolf -->

- **Wild Boar** : HP 12 / ATK d6

<!-- (D&D) HP 11 / ATK 1d6+1 (tusk) : https://www.dndbeyond.com/monsters/16812-boar -->

- **Black Bear** : HP 14 / ATK d6

<!-- (D&D) HP 19 / ATK 1d6+2 (bite) 2d4+2 (claws) : https://www.dndbeyond.com/monsters/16806-black-bear -->

- **Brown Bear** : HP 24 / ATK d10

<!-- (D&D) HP 34 / ATK 1d8+4 (bite) 2d6+4 (claws) : https://www.dndbeyond.com/monsters/16816-brown-bear -->

- **Elephant** : HP 56 / ATK d20

<!-- (D&D) HP 76 / ATK 3d8+6 (gore) 3d10+6 (stomp) : https://www.dndbeyond.com/monsters/16855-elephant -->




## Equipment


#### Melee Weapons

- **Dagger** :

- **Shortsword** :

- **Longsword** (Broadsword?):

<!-- https://en.wikipedia.org/wiki/Classification_of_swords -->

<!-- https://en.wikipedia.org/wiki/Longsword -->

- **Claymore** (Greatsword? Doppelhander?) :

<!-- https://en.wikipedia.org/wiki/Claymore -->

<!-- https://en.wikipedia.org/wiki/Zweih%C3%A4nder -->

- **Battle Axe** (War Axe?):

<!-- https://en.wikipedia.org/wiki/Axe -->

<!-- https://en.wikipedia.org/wiki/Battle_axe -->

- **Great Axe?** :

<!-- https://en.wikipedia.org/wiki/Labrys -->

- **Staff** :

<!-- https://en.wikipedia.org/wiki/Quarterstaff -->

- **Mace** (Club? Bludgeon?) :

<!-- https://en.wikipedia.org/wiki/Mace_(bludgeon) -->

- **Warhammer** (Maul?):

<!-- https://en.wikipedia.org/wiki/War_hammer -->

- **Spear** (Polearm? Pike?) :

<!-- https://en.wikipedia.org/wiki/Polearm -->

<!-- https://en.wikipedia.org/wiki/Pike_(weapon) -->

<!-- https://en.wikipedia.org/wiki/Bardiche -->

<!-- https://en.wikipedia.org/wiki/Halberd -->

#### Ranged Weapons

- **Sling** :

- **Bow** (Short Bow?) :

<!-- https://en.wikipedia.org/wiki/Bow_and_arrow -->

- **Crossbow** (Arbalest?):

<!-- https://en.wikipedia.org/wiki/Crossbow -->

<!-- https://en.wikipedia.org/wiki/Arbalest -->

- **Longbow** :

<!-- https://en.wikipedia.org/wiki/Longbow -->

#### Shields & Armor

<!-- https://en.wikipedia.org/wiki/List_of_medieval_armour_components -->




<!-- ## Roadwarden Module


### - Weapons

- **Battle Axe** :

- **Bronze Axe** :

- **Bow** :

- **Crossbow** :

- **Goblin Spear** :

- **Simple Battle Axe** :

- **Spear** :

- **Well-Crafted Battle Axe** :


### - Bestiary

|Creature                                                                                                              |Combat Clock|Challenge |Risk |Damage Roll |Pressure |
|:---------------------------------------------------------------------------------------------------------------------|:-----------|:---------|:----|:-----------|:--------|
|*Gnoll* (Beastfolk), *Goblin*, *Griffon* (Scrub), *Harpy*, *Howler* (Monkey), *Kobold* (Dragonling), *Spider* (Small) |Single Roll |2         |4    |1d4         |2        |
|*Frightape* (Ape), *Dragonling*, *Gargoyle*, *Ghoul* (Corpse Eater), *Runner*, *Undead*, *Wolf*                       |2           |3         |3    |1d6         |4        |
|*Griffon* (Noble), *Large Cat*, *Saurian*, *Spider* (Large)                                                           |4           |3         |3    |1d8         |4        |
|*Huntlord*, *Troll*, *Unicorn*                                                                                        |6           |4         |2    |1d10        |6        |
|*Golem*, *Pebbler*                                                                                                    |8           |4         |2    |1d12        |6        | -->

<!-- #### Combat Clocks

- **Single Roll** : *Gnoll* (Beastfolk), *Goblin*, *Griffon* (Scrub), *Harpy*, *Howler* (Monkey), *Kobold* (Dragonling), *Spider* (Small)

- **2** : *Frightape* (Ape), *Dragonling*, *Gargoyle*, *Ghoul* (Corpse Eater), *Runner*, *Undead*, *Wolf*

- **4** : *Griffon* (Noble), *Large Cat*, *Saurian*, *Spider* (Large)

- **6** : *Huntlord*, *Troll*, *Unicorn*

- **8** : *Golem*, *Pebbler* -->

<!-- https://roadwarden.fandom.com/wiki/Category:Bestiary -->




<!-- ## Old


#### Player Character vs Player Character 2

Do a variation on opposed rolls, where both characters roll against their own Challenge ratings (determined by context -like weapons used, environment etc...). Trouble Die reduce the opponent's Challenge, and if a character overcomes or matches their own total Challenge, they land a hit.

#### Player Character vs Player Character 1

Do a variation on opposed rolls, where both characters roll against a Challenge set by the opponent's Agility score:

|Agility Score |Challenge |
|:------------:|:--------:|
|1             |2         |
|2             |2         |
|3             |3         |
|4             |3         |
|5             |4         |

Trouble Die reduce a character's Challenge, and if a character overcomes or matches the opponent's total Challenge, they land a hit.

#### Approaches

When attacking, a character can choose between 3 approaches: reckless, neutral or controlled.

- **Reckless** : the attack has a challenge rating of 2 and a risk rating of 2.

- **Neutral** : the attack has a challenge rating of 3 and a risk rating of 3.

- **Cautious** : the attack has a challenge rating of 4 and a risk rating of 4.

>*I like this idea, but I feel like it can only be implemented if I use context die/modifiers.*

#### Enemy strikes

When an enemy (controlled by the GM) attacks a character, that character has the possibility to perform a **Resistance Roll** and completelly negate the attack by raising an **Emotion** (unless they are in a **Breakdown** state, in which case this only happens if they manage to succeed the roll).

If the player chooses to not resist the attack, the GM rolls a die aligned with the enemy's **Attack Die** (this reflects how "strong" the enemy is):

|Attack Die |Example        |
|:---------:|:-------------:|
|d4         |Text           |
|d6         |Text           |
|d8         |Text           |
|d10        |Text           |
|d12        |Text           |

The character loses an amount of **Health** equal to the result of the roll (unless the result is 1 or 2, in which case the character doesn't loose any **Health**). -->

***








# Character Progression

***




## Brainstorming/Ideas


**XP** = *Destiny Points*...? These points allow a player to decide the outcome of specific events in the lore (e.g., a faction wins a battle, something happens to a character). If the event aligns with a character's goals or beliefs, it helps them recover from Stress. Conversely, it induces Stress in any character opposed to the event.

>*Similarly, this resource can be used to gain rank in factions, learn skills, expand social circles etc...?*

<!-- "Story Points" in DnD have a serious problem : https://www.youtube.com/watch?v=NU0W_Ze6NRM -->

<!-- https://www.traveller-srd.com/core-rules/careers/ -->

#### Hope

- Gain **Hope** : when finishing a scenario, when adressing an **Emotion**, when succeeding a **Drive Action** (up to once per day) or when fulfilling a personal **Quest**. <!-- and when bonding? -->

- Spend **Hope** : change drives and archetypes, learn new skills, train an ability, join a faction or increase rank in a faction, gain power or wealth, increase circles and relationships, gain friends, earn recognition, fame or reputation, produce something, work on a long term project, make a scientific breakthrough or discovery, trigger historical events... <!-- gain new Feats/Achievements/Accomplishments/Triumphs/Exploits? -->

#### Idea 2

- **Ability XP** is gained when a character performs a **Daring Roll**, based on their **Ability** score and the level of the **Challenge** they are attempting<!-- (and if they roll the maximum value on a die?)-->:

|Ability Score |Challenge required |
|:------------:|:-----------------:|
|2             |2+                 |
|3             |3+                 |
|4             |4                  |

#### Idea 1

XP is a resource that can be spent outside of a scenario to "advance" a character:

- Increase/Train an Ability

- Learn a Skill

- Increase rank in a faction

>*!!! Remove "Ability XP", no more "Ability XP" !!!*

#### Historical Impact

- **Conquering an Empire** (*Authority* + *Distinction*) :

- **Scientific Discovery** (*Solution* + *Curiosity*) :

- **Breakthrough Invention** (*Solution* + *Production*) :




## Progression Points


- *(Sage)* Switch Drives and Archetypes

- *(Genius)* Learn new Skills

- *(Sage)* Gain new Feats/Achievements/Accomplishments/Triumphs/Exploits

- *(Champion)* Join a Faction/Increase rank in a Faction/Gain power

- *(Icon)* Increase circles/relationships/gain friends

- *(Icon)* Earn Recognition/Fame/Reputation

- *(Genius)* Produce something/work on a long term project/make a scientific breakthrough or discovery

<!-- Level up Abilities/Buy Ability XP -->

<!--

- Champion : you inherit some cool unique weapon or piece of armor, you master a new combat skill, you become a great king and unite or conquer other kingdoms.

- Icon : you produce a classic piece of art that will have you remembered for eternity.

- Genius : you come up with a theory that revolutionizes how people understand or practice a specific dicipline.

- Sage : you become the prophet of a new form of widespread religion or philosophy.

-->




## Old


A character gains general **XP** when:

- Attempting a **Drive Action** or generally acting in accordance with their **Drives**.

- Performing **Resistance Rolls** that challenge their **Flaws**.

- Fulfilling (or at least significantly pursuing) their current **Quest**. This is by far the method that earns the character the most general **XP**. -->


### - Using General XP

General XP is used for "buying" Skills and "Assets" for the character (or forcing events in the game world?).

Examples:

- *Rank*, *Power*

- *Material gain*, *Property*

- *Reputation*, *Social Standing*

- *Connections*, *Networking*

- *New positive relationship*

- *Discovery*, *Invention*

- *Blessing*, *Grace*

<!-- This will require a long deep dive into "Faction" and "Circles" mechanics from in other TTRPGS -->




## Paths (Old)


### - Champion path

- **Stage 1** : Find a combat trainer or join some military faction
- **Stage 2** : Defeat enemies or rise through the ranks and gain the trust of powerful leaders
- **Stage 3** : Take control of a small territory or domain
- **Stage 4** : Become the ruler of an entire chiefdom or city-state
- **Stage 5** : Become a great king and unite or conquer other kingdoms

>Examples of paths for *champions* include: becoming a king or emperor, leading a group or army, governing a territory or population, being a successful business leader, or having a high level of political or governmental influence.


### - Icon path

- **Stage 1** : Learn to play an instrument or paint
- **Stage 2** : Master your craft and gain recognition as a skilled artist
- **Stage 3** : Create a body of work that gains widespread acclaim
- **Stage 4** : Establish yourself as a leader in your artistic field
- **Stage 5** : Produce a classic piece of art that will have you remembered for eternity

>Examples of paths for *icons* include: becoming a public figure, living life to the fullest and experiencing all the pleasures and rewards it has to offer, or achieving fame and success in a particular field, such as acting, music, or sports.


### - Genius path

- **Stage 1** : Learn to read, write or get accepted for training in school
- **Stage 2** : Develop your skills and knowledge in a specific field
- **Stage 3** : Complete minor missions or objectives related to your expertise
- **Stage 4** : Establish yourself as a leader in your field
- **Stage 5** : Come up with a theory that revolutionizes how people understand or practice your dicipline

>Examples of paths for *geniuses* include: inventing a groundbreaking technology, making a significant scientific discovery, establishing a new field of study or creating a new product or service that changes the way people live and work.


### - Sage path

- **Stage 1** : Turn to a life of hermitage or asceticism
- **Stage 2** : Study and gain knowledge in a wide range of subjects
- **Stage 3** : Become a respected mentor or teacher
- **Stage 4** : Create your own school of thought
- **Stage 5** : Become the prophet of a new form religion or philosophy

>Examples of paths for *sages* include: becoming a respected spiritual leader, achieving a state of enlightenment, providing aid or assistance to those who require it, or dedicating oneself to a particular cause or philosophy.


<!-- ### - Ability XP

Trading general **XP** with **Ability XP** after a scenario is over (this counts as training). The higher the ability score, the less generous this "trade-off" becomes:

|Ability Score |Trade-off                             |
|:------------:|:------------------------------------:|
|1             |1 general **XP** for 2 **Ability XP** |
|2             |1 general **XP** for 1 **Ability XP** |
|3             |2 general **XP** for 1 **Ability XP** |
|4             |3 general **XP** for 1 **Ability XP** | -->

<!-- Training Abilities that don't correspond to the character's Archetypes should also worsen the trade-off. -->

<!-- Finally, When a character fills an **Ability XP** track entirely (its 6 slots have been marked), they increase that ability's score by 1 and clear the track back to 0.

>*A character can't mark an Ability XP track if the ability has already reached its maximum score of 5.* -->


<!-- ### - Example -->

***








# Triggering/Challenging Flaws

***




Sometimes the GM may use a character's **Flaw** to try and provoke an impulse or reaction from them (usually under a specific circumstance):

- **Greedy** : the character is tempted to steal something, or doesn't want to share an item that might be helpful to someone else. A **Resistance Roll** with *humility* is required to see if the character steals or keeps the item to themselves.

- **Vain** : the character is tempted to seduce someone or indulge in some form of pleasure.

- **Weird** : the character does something unsettling (innocuous behaviour, singing to themselves, fulfilling a strange compulsion at inappropriate times etc). A **Resistance Roll** with *charm* is required to see if this behavior drives a valuable NPC away from the group.

<!-- This is copied almost word for word from "Spire - The City must Fall" -->

<!-- - **Overinvested** : witnessing unfair treatment, seeing a helpless person being mistreated, witnessing an authority figure abusing their power... -->

<!-- *When a character fails a Resistance Roll that challenges one of their Flaws, they act upon their impulse, potentially harming themselves or the group.* -->


<!-- Spire - The City must Fall:

- **Panicked** (*Mind*) : Your heart hammers in your chest. Choose: either leave the scene and calm down, or make things difficult for everyone else around you and increase the difficulty of all actions performed nearby to you by 1.

- **Shaken** (*Mind*) : You can’t focus on what you’re doing. You may not gain dice from Domains until you take time to calm down.

- **Lash Out** (*Reputation*) : You’re pissed off. You immediately lash out at the source of stress, no matter whether or not this is a sensible idea.

- **Lie** (*Reputation*) : Trying to justify your actions, you tell a lie that will cause a problem this or next session.

- **Freak Out** (*Mind*) : You lose it, and attempt to kill (or at least drive off) whatever caused your mental break. You won’t calm down until you’re restrained, it’s destroyed or flees the scene, or you’re knocked unconscious. -->


<!-- ### - Example? -->

<!-- Bring up Modifiers? -->

***








# Factions

***




>*Implement a rule for "Circles"...?*

<!-- Remember : https://www.youtube.com/watch?v=5DKUeyAHrfM -->




## Variables


#### Sympathy

**Disposition** parameter. Measures how much the character likes/dislikes the faction. - should only be used if the character isn't part of the faction...?

- **Persuade** actions cannot be performed (by faction NPCs) on the character if *sympathy* towards the faction is negative.

- **Deceive** actions cannot be performed (by faction NPCs) on the character if *sympathy* towards the faction is negative.

#### Reputation

<!-- standing, recognition -->

**Disposition** parameter, but this time from the faction towards the character. Measures how much the average NPC within the faction likes/dislikes the character - the character should be somewhat well-known though...?

- **Persuade** actions cannot be performed on NPCS belonging to the faction if *reputation* towards the character is negative.

- **Deceive** actions cannot be performed on NPCS belonging to the faction if *reputation* towards the character is negative.

#### Rank

Position/grade parameter (only used if the character **is** part of the faction). The higher the rank, the more power/authority the character has within the faction. And the more important/influential the faction the more/the higher the ranks. For example, a small band of bandits may have 3 ranks (1 - *grunt*, 2 - *lieutenant*, 3 - *leader*), whereas a prominent guid may have 6 ranks (1 - *clerk*, 2 - *operative*, 3 - *officer*, 4 - *inspector*, 5 - *councilor*, 6 - *grand master*).

>*Rank values in a faction should determine the base respect between two characters meeting for the first time. Compare their ranks, and the character with the lower rank should have positive respect towards the higher-ranked character, regardless of the faction they belong to. This, of course, influences Plead and Threaten Social Actions.*




## Roadwarden


- **Reputation** : measures how likely the Faction is to accept a deal with the city?


### - Factions

- **Gale Rocks** (4 ranks) : Severina (*headwoman*), Photios (*fisherman*)

- **Howler's Dell** (4 ranks) : Thais (*mayor*), Elpis (*druidess*)

- **The Monastery** (3 ranks) : The prelate

- **The Tribe of The Green Mountain** (3 ranks) : Cephas, Gaiane

- **White Marshes** (3 ranks) : Helvius (*mayor*), Orentius (*priest*)

- **Creeks** (2 ranks) : Efren (*hunter*), Elah (*carpenter*)

- **Glaucia's Band** (2 ranks) : Glaucia (*leader*)

- **Pelt of the North** (2 ranks) : Iason (*owner*), Dalit (*huntress*)

<!-- Old Pagos? -->

***








# Traumas/Fallouts

***




<!-- Other names: Afflictions, Handicaps -->

<!-- https://en.wikipedia.org/wiki/Shadow_(psychology) -->

**Traumas** are negative life-changing effects or events that affect a character in the long-term. They are gained depending on the amount of **Stress** a character has accumulated by the end of a scenario.

>*Examples: physical trauma (broken limb, sickness...), social trauma (in debt, outlaw, infamy, rumour, friend loss...), psychological trauma (gain a new flaw?), spiritual trauma?*

<!-- Removing Stress at the end of a session/scenario clears the entirety of the character's Stress Stack. If there was a small amount of Stress, the character gains a new Minor Trauma. If there was a large amount of Stress, the character gains a new Major Trauma. -->

Examples:

- *Subjection*

- *Infamy*

- *Ineptitude*, *Disability*

- *New Flaw*

- *Curse*

<!-- Spire - The City must Fall:

- **Broken Arm** (*Blood*) : Your arm breaks under the strain, and splintered bone juts up through your skin. You can’t use the arm until it heals (which will take a month or so, or require powerful healing magic).

- **Broken Leg** (*Blood*) : Your leg bones splinter and crack. You can’t walk without crutches for a month or so, and you’ll automatically fail any Pursue attempts. Any action where you’d need to be quick on your feet (fighting, dancing, climbing) is either impossible or suffers from an increased difficulty.

- **Rumour** (*Shadow*) : Word of your actions gets around the area. The GM and the other players should work out three statements that are whispered about you in bars and taverns: two true, one false.

- **Debtor** (*Silver*) : During the next session or later in this one, an NPC who lent you money will call in a favour.

- **Memory Holes** (*Mind*) : You did things that you can’t quite recall. The GM and every player aside from you work together to determine what you did that you blocked out from your mind while you step outside of the room, or during downtime. These are generally pretty awful things, and they can have happened up to a year ago in game time or immediately upon suffering fallout. Your character has zero memory of the events, but everyone else involved knows what happened.

- **Phobia** (*Mind*) : You acquire a phobia of something related to the stress you suffered – a person, an item, a place, a situation, or something even more abstract. You will avoid the subject of your phobia whenever possible, and if you have to interact with it, difficulties are increased by 1.

- **Humiliated** (*Reputation*) : You make a total fool of yourself. You can’t make use of any allies associated with the actions that pushed you into fallout until you prove yourself once more in their eyes.

- **Vendetta** (*Reputation*) : During the next session, or later in this one, an NPC from this session will return with a vendetta against you.

- **Arrested** (*Shadow*) : You are caught and arrested by the guard and put in a jail cell, awaiting proper questioning for your crimes.

- **Criminal** (*Shadow*) : You are accused of criminal acts, whether true or not, and wanted posters featuring your face and name are put up throughout the district in which you committed the crime. If the crime is particularly exciting, your name and face may make it into the papers.

- **Sold** (*Silver*) : You’re forced to sell off something valuable to pay your debtors. Work out with the GM what you’re forced to sell.

- **Services Rendered** (*Silver*) : You’re forced to sell your skills to a third party to pay your debtors, and the work is not pleasant. Work out with the GM what your character doesn’t want to do but is prepared to in order to make ends meet.

- **Chosen** (*Blood*) : You pass out and awaken in a half-dream state before your god (and if you haven’t devoted your life to a god, someone else’s god) and you can strike a bargain with them in exchange for your life. If you accept their terms, you return to consciousness (changed, always changed) and if you don’t, you slip away into death.

- **Obsessed** (*Mind*) : Your mind splinters and shards until it is razor-sharp and barbed, focused on a single goal. You are now PERMANENTLY WEIRD (as above), but when you attempt to achieve your goal you roll with mastery, and the difficulty for all other actions increases by 1. Once you achieve your goal, your mind gives under the strain, and you are forcibly retired.

- **Renegade** (*Mind*) : Your mind shattered by the stress of your actions, you turn against the Ministry and all it stands for (the precise reasons why are up to you and the GM). This is not immediate; the fallout represents the first crack in the dam of your mind. Over the next few sessions, play out your descent. Should you survive, you join a rival faction, or become one unto yourself, set against the cell.

- **Reviled** (*Reputation*) : You are expelled from somewhere important to you, and you can no longer return on pain of death (or, in more civilised society, arrest). People in the district will spread stories of how utterly reprehensible you are throughout the city.

- **Burned** (*Shadow*) : The Ministry decides that you are no longer worth the risk of employing, and after feeding you some false information, sells you out to your enemies. Good luck.

- **Wrath of the Sun Gods** (*Shadow*) : Your operations are uncovered by the Solar Guard, the grand inquisitors of the aelfir church, and you are hunted. Many of your NPC bonds are dragged out into the street and shot. Your friends dare not speak your name.

- **Destitute** (*Silver*) : You are utterly without material resource, and deep in debt to several bad people. You have one last chance to gather up a vast sum of money or resources to pay back your creditors, or you’ll end up dead, mauled beyond recognition by loan sharks, or sold off into some Red Row sweatshop.

- **Turned** (*Silver/Shadow*) : You have sold out your allies in exchange for massive bribes to get you out of debt, or to lessen your own punishment. You are now working for one of your enemies as well as the Ministry, and you may be asked at any time to perform favours or rat out your team-mates. The only way out of this is to come clean (and be killed by the Ministry), completely eliminate the enemy forces who have leverage on you, or take the next skywhale out of Spire and never look back.

- **Depleted** (*Minor*) : Your bond uses up their resources, suffers an injury, is upset with you, or is otherwise unable or unwilling to help you out. Next time you ask a favour of them, the difficulty of the task they attempt is increased by 1.

- **In Trouble** (*Moderate*) : Your bond finds themselves in trouble, and can’t be used until the problem is resolved.

- **Betrayal** (*Severe*) : Mark D8 stress in Shadow. Your bond turns against you, although they will not tell you this until it is too late.

- **Made an Example of** (*Severe*) : Your bond’s connection to the resistance is uncovered, and they are made to pay the price. They are dragged into the streets and shot in public after being denounced for their crimes and declared dead. -->

***








# Decision Rolls (Advanced Mechanics?)

***




**Decision Rolls** simulate **Emotional** strain exerted on a character when they have to make a tough decision. These rolls should be made very sparingly, and only reserved for moments where characters have to make a choice where there's no clear or immediate outcome, and where the consequences of the decision may hold enough weight in shaping the narrative of the scenario.

>*Decision Rolls shouldn't (be seen as simple) be mistaken with "moral choices" or "ethical dilemmas", as these can have very apparent and instantaneous outcomes. They should be made if characters genuinely "don't know what would happen next" but still have to make a decision. If the GM sees players hesitating, not knowing what to do or discussing with each other about what is the best route to take, this might be a good time to call for a Decision Roll.*

**Decision Rolls** do not impact the choice made by the player itself, they're only here as a way to represent the mental strain exerted on the character that is forced to make the choice.

<!-- Similar to **Resistance Rolls** with **Pressure**, -->
**Decision Rolls** use **Weight** to measure their "strength". The more "perceived impact" or "importance" the choice holds, the greater its **Weight**:

|Weight     |Rating |Example |
|:---------:|:-----:|:-------|
|Average    |2      | |
|Meaningful |3      | |
|Pivotal    |4      | |


<!-- These decision rolls represent choices that, while not insignificant, don't carry substantial weight in shaping the broader narrative. They might involve everyday decisions or minor dilemmas that arise in the course of the characters' adventures. The emotional strain here is noticeable but manageable, akin to the pressure of making a moderately important decision in real life. -->

<!-- A step above the average, these decision rolls signify choices with more significant consequences, ones that could affect the characters' immediate circumstances or relationships within the game world. The emotional strain is palpable, reflecting the weight of the decision on the characters' minds and hearts as they contemplate their options. -->

<!-- At the highest level of intensity, pivotal decision rolls encapsulate moments of profound significance within the narrative. These choices have the potential to alter the course of the story in significant ways, shaping character arcs or even determining the outcome of the scenario. The emotional strain here is immense, mirroring the gravity of the decision as characters grapple with the potential ramifications of their actions. -->

- Average : the party encounters a fork in the road, unsure whether to take the well-traveled path or the mysterious, uncharted trail. Both routes seem equally viable, but each carries its own risks and potential rewards.

- Meaningful : same as before but if they take the wrong path characters may lose time and miss an important deadline, or not be able to find a shelter and be forced to sleep outdoors (a dangerous prospective).

- Pivotal : same as before but now the characters have the responsibility to lead a caravan composed of hundreds of people. The stakes are high if they take the wrong decision, this will affect the lives of many if they get lost or follow the wrong path.

- Anger (Humility) :

- Frustration (Insight) :

- Anxiety (Composure) : fear of consequences/bad outcome

- Guilt (Composure) : moral dilemma

|Emotion     |Description |
|:----------:|:-----------|
|Anger       |The character is acting out of resentment or animosity. They feel wronged or threatened, and think that they can overcome the obstacle through assertiveness or confrontation. |
|Frustration |The character is acting out of annoyance or irritation. Things aren't turning out as expected, they're dealing with an unforeseen obstacle or an unwanted task. |
|Anxiety     |The character is acting with uncertainty or apprehension. They are plagued by fear, second-guessing their decision or perceiving themselves as too weak for the task. |
|Guilt       |The character is acting against their own values or principles. They feel responsible for what they're doing or believe that they shouldn't be doing it in the first place. |

<!-- https://en.wikipedia.org/wiki/Decision_fatigue -->

<!-- https://en.wikipedia.org/wiki/Ego_depletion -->

<!-- https://en.wikipedia.org/wiki/Spoon_theory -->

***








# Inventory

***




## Coin Pouch


A character's **Pouch** is divided into **Tiers** ranging from 1 to 6, with each **Tier** consisting of 4 slots. Wages and prices are also categorized into 6 **Tiers**, from low/cheap (1) to high/expensive (6):

|Price      |Tier |Example |
|-----------|-----|--------|
|Cheap      |1    | |
|Affordable |2    | |
|Costly     |3    | |
|Expensive  |4    | |
|High-end   |5    | |
|Lavish     |6    | |

When adding or removing value to their **Pouch**, characters mark or clear slots based on the affected **Tier**.

>*For example, earning 1 coin marks 1 slot in Tier 1, while losing or spending 2 coins clears 1 slot in Tier 2.*

When a **Tier** is full and the character needs to mark an additional slot, all slots in that **Tier** are cleared, and one slot is marked in the next **Tier**.

If a character needs to clear a slot in a **Tier** with no marked slots, they must clear a slot in the next available **Tier** and mark all slots in any **Tiers** found in between, including the original **Tier**. If there are no **Tiers** available, it means that the character doesn't have enough coins to spend.

>*For example, if a character wants to spend 1 coin but only has 2 slots marked in Tier 3 and none in Tier 1 or Tier 2, they must clear 1 slot in Tier 3 and mark all slots in Tier 1 and Tier 2. After the transaction, Tier 1 and Tier 2 now have all slots marked, while Tier 3 only has 1 marked slot left.*




## Blades in the Dark (Loadout/Inventory)


- p.57

You have access to all of the items on your character sheet. For each operation, decide what your character’s load will be. During the operation, you may say that your character has an item on hand by checking the box for the item you want to use—up to a number of items equal to your chosen load. Your load also determines your movement speed and conspicuousness:

- **1-3 load** : Light. You’re faster, less conspicuous; you blend in with citizens.

- **4/5 load** : Normal. You look like a scoundrel, ready for trouble.

- **6+ load** : Heavy. You’re slower. You look like an operative on a mission.

Your maximum load is 2 greater than your heavy load. So, a normal person can carry up to 8 load. Some special abilities (like the Cutter's Mule ability) may increase your load limits. Some items count as two items for load (they have two connected boxes). Items in italics don’t count toward your load. You don’t need to select specific items now. Review your personal items and the standard item descriptions on page 88.

>https://bladesinthedark.com/planning-engagement

>https://www.reddit.com/r/bladesinthedark/comments/le1au8/how_do_items_work/

>https://www.reddit.com/r/bladesinthedark/comments/6og35q/how_equipment_works_lots_of_qs/


### - Coin

<!-- Let's Learn Blades in the Dark - Episode 13 (Coin & Stash) : https://www.youtube.com/watch?v=GGpxSoeYbXo -->

<!-- Let's Learn Blades in the Dark - Episode 14 (Payoff) : https://www.youtube.com/watch?v=wP52-1b0DwQ -->

***








<!-- # Spellcasting

*** -->








<!-- # Conditions




- Dirty, encumbered, tired, drunk...

*** -->








<!-- # Flashbacks




Blades in the Dark : core rulebook p.132

- https://www.youtube.com/watch?v=6sXdCKd0XdE&t=170s

- https://bladesinthedark.com/planning-engagement

*** -->


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***


***
