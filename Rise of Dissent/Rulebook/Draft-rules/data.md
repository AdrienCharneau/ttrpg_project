```
CURRENT CORE MECHANIC (TEST 10)
v13_dice_probabilities
no character sheet (yet)
```


Results higher than 3 are now added together to form a total **Success Score**, instead of counting as single **Sucess Die**.

#### Positive points

This new system allows me to:

- Provide more breathing room for detailed worldbuilding (by adding granularity and variety in weapons, quality tiers etc...).

- Implement "racial" **Modifiers**...? (aka "*innate*/*natural*" **Modifiers**).

- Keep *crippled* abilities.

#### Neutral points

- **Context Modifiers** are implemented in a more straightforward way, at the cost of adding more crunch/maths.

#### Negative points

Things that the older system did better:

- **Challenge** and **Risk** were symmetrical, and easier to remember.

- **Critical Successes** were more straightforward/easier to understand.

- **Targeted Checks** did not require a correspondance table.