# Apocalypse World

Zombie apocalypse setting with character relationship mechanics. Very eccentric rules - didn't understand everything. Specifically the "harm" mechanic based around a countdown clock (p.15).

## Notes:

- List of actions or "moves" characters can do in game (doing them is the only time players have to roll dice) p.86 and p.190
- Hx aka history: character relationships mechanic (it's actually a character stat p.14)
- Character "improvement" aka leveling-up p.178

***




# Blades in the Dark

Players play as a crew of assassins or thieves in a Victorian era setting. Interesting stress and action mechanics.

## Notes:

- Stress & trauma mechanic (p.22) + resistance roll (p.41)
- Progress clocks to track ongoing effort against an obstacle p.24
- Action "positions" - controlled, risky or desperate (p.28, p.32) and "effects" - great, standard, limited (p.29, p.33)
- Devil's bargain mechanic - increase chances of success but harm friends (p.30)
- Gathering information, social skills p.45
- Downtime, relaxation, vice stress relief p.154
- Consort, small character relationship mechanic p.181
- Some lore and setting with maps of the city districts p.263

***




# The Burning Wheel

Seems like a pretty standard system, mostly focused around combat and adventuring like d&d. No deep lore or any outlandish stuff.

## Notes:

- Good list of actions with their difficulty thresholds according to each stat p.15
- There's a small steel/hesitation mechanic worth checking out p.17
- Some cool test/action mechanics starting at p.19 (versus test, working quickly...)
- Skills stuff (characters collaborating, learning skills...) p.24

***




# Chronicles of Darkness

Mystery and horror investigation set in a modern urban setting.

## Notes:

- Virtues, vices p.28
- Aspirations: they're exactly the same as my "path/xp/progress" mechanic in my game p.29, p.77
- Breaking points aka trauma/fallout/breakdown mechanic p.30, p.74
- TAKE INSPIRATION -> Example list of actions (persuading, jumping, sneaking...) with their respective stats (2 stats for the instigator and one stat for the receiver, just like in my game) + their associated "dramatic failures" and "exceptional successes" p.71
- Very interesting and complex "Social Maneuvering" mechanic (with "doors", "first impressions", "coercion", "blackmail" etc... and a very good example showing off the system at play). + takes into account aspirations and breaking points just like in my game p.82

***




# The Chuubo's Marvelous Wish-Granting Engine RPG

>UNFINISHED - RULES ARE HARD TO UNDERSTAND

Diceless progressive, warm-hearted rpg that focuses on children adventures and slice-of-life stories.

## Notes:

- "Bonding" and "emoting" mechanics -> looks really in-depth and well put, except I didn't understand much of it (better go back and read the whole rules again)

***




# Dungeon World

Very generic heroic fantasy setting and mechanics like d&d.

## Notes:

- Bonds aka "character relationships" mechanic p.30 (each class has its own set of starting bonds p. 84) => resolving them allows a character to gain XP
- List of actions or "moves" characters can do in game p.56

***




# Exalted

Pretty detailed Eastern-Asian mythical setting based on "Journey to the West" or "War of the Three Kingdoms". You play as an "Exalted", sort of hero character type similar to the ones found in Dynasty Warriors (the video game).

## Notes:

- Intimacies: this is where I get the idea from for this concept p.171
- Very fleshed out influence or "social combat" mechanic based on character's intimacies. There's even a list of "social actions" and rules about things like "addressing a crowd", "unacceptable influence" or "retrying social actions" p.214
- Lore-heavy. Lots and lots of lore
- Traits and limit triggers mechanic, similar to traumas/fallout p.135
- Merits and flaws p.158
- Willpower mechanic similar to my morale/resolve mechanic (restored through resting or achieving goals etc...) p.170
- Dice roll mechanic seems to be close to my game and Burning Wheel's. There's a good description on how to perform dice rolls and quickly determine successes that I could implement as an introduction to the chapter about 'Actions' in my game p.186
- Action "complications" mechanic - dealing with actions where other characters are trying to stop you from succeeding, actions with ongoing effort or steps etc... p.189
- Lore knowledge mechanic. "Lore facts" can be introduced or "challenged" by characters ("once a fact about the setting has been introduced, it becomes concrete") p.238

***




# Fate

Generic role-playing game system based on the Fudge gaming system. It has no fixed setting, traits, or genre and is customizable. It is designed to offer minimal obstruction to role-playing by assuming players want to make fewer dice rolls.

## Notes:

- Remember/for reference only: dice mechanics (p.16) - challenges, contests and conflicts (p.154)
- Character creation: High Concept, Trouble, Phase Trio (how a character is related to other characters from past experiences/adventures) p.39
- Aspects: related to fate points, these work almost exactly like intimacies in my game except instead of gaining xp/progress when "invoking" them, players gain fate points p.20, p.64, p.76
- Stress & breakdown/trauma/fallout mechanic (with two gauges: physical and mental just like my game) that end up giving negative Aspects to the player - although there's not much on social conflicts or mental breakdowns unfortunately p.58, p.66, p.170
- Compels: interesting way of bringing obstacles and complications into the game using Aspects. Any player (not just the GM) can use this mechanic p.22, p.79
- Four types of actions: "overcome", "create an advantage", "attack" and "defend" (and explanations for critical failures or successes for each) p.142

***




# Invisible Sun

>Invisible Sun is deeply character focused. Not only are the player characters the avatars of the players of the game, but the characters and their arcs drive play as well. Character creation is involved, with many choices, but the esoteric arts are involved and require a great deal of introspection, so the intricacy and depth are an excellent primer for what it means to be a vislae (The Key p.13).

- **The Key** : rulebook, character creation

- **The Way** : how magic works in Invisible Sun (with extra lore, list of spells, enchanted items etc...)

- **The Path** : 150 pages of exposition and lore about the world of Invisible Sun

- **The Gate** : GM's handbook

## Notes:

- *Venture* aka "roll modifiers" reduce (or increase) *Target Numbers* for actions, just like in my game or Numenera (The Key p.24)

- List of skills (The Key p.34)

- Good introduction to *Paths*: "What do you do?" (The Key p.14)

- 4 *Roleplaying Styles* that sort of mirror my 4 *Avatars*: *Builder*, *Explorer*, *Attainer* & *Achiever* (The Key p.21)

- **Factions** : right at the beginning, players have to pick an *Order* for their characters (*Vances*, *Makers*, *Weavers*, or *Goetics*). *Orders* are like organizations they can advance within, and that determines the way they express their magical talents. They use ranks like in Morrowind factions. Each order has a "philosophy", "relationships", "authority" and "responsibilities" (for each rank) descriptions that I can use as inspiration for my own factions (The Key p.21, p.37, p.39)

- **Maker's Matrix** : cool "magic item crafting" system that can be used as a template for *artfism* (The Key p.45 - The Way p.58)

- **Foundations** : very in-depth character backstory mechanics with "homes", "connections"... (The Key p.22, p.147)

- **Arcs** : the *Path* mechanic I'm trying to take inspiration from (The Key p.23, p.24)

- **Bonds** : aka relationships (The Key p.23, p.200)

- **Personality Traits** : Galant, Stoic, Empath & Ardent (The Key p.67)

- **Skill Trees** : Forte (The Key p.73)

- **Day-to-day items** : home furnishing, supplies, tools, clothes... (p.188)

- **Sooth Deck** : very interesting magic "rules alteration" mechanic (The Key p.25, p.71 - The Gate p.75)

>When something significant happens in the game, the GM will turn over a card in the Sooth Deck. Among other things, the card revealed will change the way magic works, making certain spells and effects more powerful, and others less so. These changes are frequent and usually fleeting, and they reflect the fact that the magical world is a complex, ever-shifting one. Since events that cause a card turn are usually magical, this shows that the more one uses magic, the more magic gets disrupted, like a still pond into which more and more pebbles are dropped, causing ripples that then interact with other ripples, and so on.

***




# Monsterhearts

Game about "the messy lives of teenage monsters", taking place in a high school drama setting.

## Notes:

- Strings : characters relationship mechanic p.16, p.26
- List of action types with strong emphasis on social actions p.18, p.54
- Reputation mechanic aka "conditions" p.31
- Good tips and directions on how to role-play safely and set boundaries between players p.74

***




# Numenera

- **Torment: Tides of Numenera** : https://en.wikipedia.org/wiki/Torment:_Tides_of_Numenera

Direct quotes from the introduction (p.5):

>**Gameplay** : a roleplaying game system where players got to decide how much effort they wanted to put into any given action, and that decision would help determine whether their action would succeed or fail. This would be a simple but elegant system where sustained damage and physical exertion drew from the same resource (so as you became wounded, you could do less, and as you became exhausted, you were easier to take down). Where your willpower and your mental “power points” were the same thing, and as you drew on your mental resources, your ability to stave off mental attacks waned.

>**Lore** : a world that fused science fiction and fantasy, but not in the usual mixed-genre sort of way. Instead, it was a place that felt like fantasy but was actually science fiction. Or perhaps it felt like science fiction but was actually fantasy. Could I achieve both at once? The well-known quote from Sir Arthur C. Clarke that “any sufficiently advanced technology is indistinguishable from magic” seemed to lie at the heart of this concept. In my mind, I envisioned strangely garbed priests chanting well-rehearsed prayers and invocations, using sacred instruments and making precise gestures, but then we realize that the instruments are technological in nature, and some of the gestures are actually fingers playing over buttons or sections of a touchscreen.

## Notes:

- Very good lore and exposition, very very very similar to what I want to do p.13

>"The Ninth World is about discovering the wonders of the worlds that came before it, not for their own sake, but as the means to improve the present and build a future."

- Modifiers decrease (or increase) difficulty of action rolls p.16, p.85

- Applying "effort" adds positive modifiers to action rolls, but decreases a character's stat score -  p.22, death/rest/recovery p.95

- Very basic but good technology/magic system. Use it for inspiration in designing *artifistic* contraptions.

>**Example** : *Launcher* (p.307) - A metal tube with a set of winding synth tubes wrapped around it. This device launches anything about the size of a fist or smaller with great force with a range of 200 feet (61 m). An inert object, such as a rock, inflicts the artifact’s level in damage. A detonation cypher (or similar explosive) can also be launched to activate on impact.

***




# Pendragon

Medieval fantasy based on the Legend of King Arthur. Very cool semi-realistic medieval setting with fantasy elements. Feudalism, knights and customs - loads of cool british and celtic medieval lore/world-building.

## Notes:

- Very good and detailed lore dumps (right at the beginning)
- Traits and passions mechanic p.28, p.66
- Army and land management mechanics p.33, geopolitics p.45, battles p.205
- Glory aka "reputation + xp" mechanic p.101
- Religion p.139

***




# Smallville

Relationships-themed rpg in the universe of Smallville/Superman where players take the role of already existing characters: "Every character—or Lead—that you play knows or has some opinion of all of the others. You always start the game having these relationships described and written down on your sheet of notes, ready for you to engage in scenes with them". You can also play a character of your own creation (pathways). Very fetched-out and eccentric rules based on the Cortex Plus System.

## Notes:

- Drive mechanic p.10, p.85 - hard to explain but very interesting - Examples p.12 with "Contest vs. Test dice rolls"  (aka "pvp vs. pve rolls")
- Values: "Love, Duty, Glory, Power, Truth..." mechanic similar to purposes in my game p.10, p.86
- Relationships: similar to values but are related to what the character thinks of other characters p.9, p.88
- Pathways - when creating new characters, draw a pathways map (similar to "timelines" in my game) p.16
- Stress mechanic: "Afraid, Angry, Exhausted, Injured, Insecure" that can take you out of the scene - p.10, p.58
- "Scenes" management mechanic like in a tv series p.53

***




# Spire - The City must Fall

Interesting setting where players take the role of rebels fighting in secret against an oppressive regime. No "monsters", no "loot" and no barebones "moral system" like in d&d (which is good). Very good lore similar to what I want to achieve.

## Notes:

- Stress & fallout mechanic (blood, mind, silver, reputation...) p.14, p.16
- Refresh mechanic to recover from stress (tied to your character class) p.34
- Small "bonds" aka "character relationships" mechanic p.28
- Very extensive lore with cultures, traditions, districts and factions (almost 50% of the rulebook is about that)

***




# Urban Shadows

Modern "goth" urban setting based on Apocalypse World with emphasis on multicultural, ethnic and sexual diversity. Good character relationship and interaction stuff.

## Notes:

- Good introduction on how flow and narrative should work in pen & paper RPGs p.17
- Small faction relationship mechanic (p.32) with their respective faction moves (p.61) and session moves (p.78)
- List of actions or "moves" characters can do in game (doing them is the only time players have to roll dice) p.39 + "Moves upon moves" p.205
- Character relationships => "debt" system and moves p.36, p.65 + intimacy moves p.75

***