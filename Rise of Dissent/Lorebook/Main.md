# Introduction

***




The universe of Atouhm, much like our own real universe, remains an enigma to the beings that reside within it. Its origin, boundaries, and the extent of its expansion into the infinite depths are subjects of profound curiosity. Is there something beyond its tangible existence, something greater and more significant? Or perhaps it stands as the pinnacle, the essential and primordial embodiment of reality itself? These questions, brimming with doubt and uncertainty, have received only a handful of satisfying answers from the realms of philosophy and science.

Throughout the course of civilization, countless sages, thinkers, and mages have embarked on experiments and profound contemplations, seeking to unravel the mysteries surrounding the universe of Atouhm. Their efforts, while unable to fully unlock its secrets, have shed light on certain elusive aspects, building upon the discoveries of those who came before them. The vastness of Atouhm's universe defies easy comprehension, as each civilization and culture harbors its distinct calendar, unique mapping, and diverse modes of thought, which also evolve with the passage of time.

The true origins of the universe remain a mystery known to none. While various legends circulate on the subject, they often diverge, painting different narratives. The boundaries of this world elude the grasp of the ordinary, and the maps that attempt to chart the infinite expanse surrounding beings frequently prove incomplete in their depiction.




## Core Ideas


The world of Atouhm is a fantasy setting where everything is alien. Imagine the unique biology, technology, and civilizations of Nausicaä, the otherworldly architecture or cultures of Morrowind, or the strange ecosystem of Scavengers Reign. Although things may "look" different, they still "function" much the same as in our world (unless otherwise specified). People tame the land and live in societies where individuals survive by specializing in specific trades. Food is cultivated in crops and forms the backbone of any civilization. Minerals are mined from the ground and used for crafting or building materials and so on.

For all intents and purposes, and despite differences in appearance on what they reference, most english words retain their meaning. For instance, *rice* might look alien and taste like sand, but it is still a grain grown in water widely consumed as a staple food. Similarly, *dogs* may resemble giant grasshoppers, but remain domesticated four-legged animals used for herding or hunting. *Whales* are still gigantic aquatic creatures, *trees* are still large plants that can be cut down to produce *wood* etc...

There is only one humanoid, sentient species that refers to themselves as *humans*. While not identical to our world's *humans*, they share similar traits, such as bipedalism, opposable thumbs, and complex language or social structures, with racial differences among them being mostly visual. No other species or creatures in Atouhm possess rationality or spirituality, nor can they communicate with *humans* through language.

Magic replaces the concept of maths, science, chemistry or any other conventional discipline. Spellcasting is slow and tedious, necessitating elaborate rituals and thorough preparation for it to be effective. Spellcasters must gather specific materials, taylor their "magic algorithms" depending on circumstances or even adapt their incantations to celestial alignments. Besides magic, no occurrences of the unnatural, such as undead, otherworldly beings or mystical creatures manifest or is known.

***








<!-- # Geography

***

*** -->








# Arithmysticism

***




Arcana (or magic) represents the fundamental constituents, motions and behavior of entities, energies and natural forces present in the world of Rise of Dissent. Arcanism (the study of arcana) is the method used to reveal knowledge about it and arithmysticism (spellcasting) is the utilization of that knowledge for practical purposes.

Arithmysticism is used as a way to bend reality or transform environments, and can only replicate natural phenomenons that are found or studied within the world of Rise of Dissent. For instance, if a character wants to start a fire with a spell, they still need to find combustible material such as wood or oil for it work.

>As long as arcanists have not discovered a way to perform some unconventional or outlandish effect, magic cannot be used to do things like *summoning spirits*, *reading minds* or *resurrecting the dead*.

#### Arithmyum

Arcana is channeled by mastering the intricacies of an ancient non-spoken esoteric language called arithmyum. Understanding it enables arcanists to invoke calciphants (magic algorithms) with their thoughts.

>By mentally projecting the appropriate patterns using arithmyum, arcanists manifest specific spells or arcanic effects directly into reality.




## Calciphants


Calciphants work like magic formulas, but should not be seen as ready-made spells or on-command effects. They should be interpreted as *blueprints* for magical occurrences that must be tailored or arranged to specific situations. When casting arcana, users need to carefully consider, adapt, and modify a unique calciphant that they've learned (or that they're reading from a tablet) to invoke its effect into a given circumstance.

>A calciphant that works as intended is considered *arithmystically sound*, whereas a flawed or illogic calciphant that produces incorrect results is deemed *dissonant*.

Arithmysticism rarely involves instantaneous spells and demands focus and preparation to achieve desired outcomes. It is a deliberate process that requires users to analyze a situation and determine the most suitable approach for casting a spell.

>For example, if a caster intends to ignite a piece of wood, they must take into account factors such as the size of the object, its weight and the strength of the wind in order to adapt their burning calciphant accordingly. This is why direct combat spells, relying on swift reflexes and reactions, barely exist in Rise of Dissent.

Elementary arithmystic practices are for the most part well understood by the general population. Many people can perform basic calciphants like *extracting water from mud* or *levitating a piece of paper*, the same way they would learn a cooking recipe. On the other hand, learning advanced calciphants requires much more knowledge and training.

#### Equipment

The ability to create or replicate calciphants necessitates specific equipment such as vessels, bottles or oils. In this context, arcanists carry flasks and potions specifically for this purpose.




## Cultural ramifications


*Arithmyum* can be transcribed into spoken words or written tablets, assisting *arcanists* in sharing their knowledge. Additionally, written *arithmyum* relieves them from the need to memorize specific *calciphants*. However, each civilization has its own unique way of representing *arithmyum* through distinct words, patterns, runes and symbols. Consequently, an *arcanist* from one culture may not be able to read and comprehend tablets and *calciphants* from another one, even though they're spoken or written in the same "universal" language.

>Appart from tablets, I must introduce a "papyrus-like" scroll (I need to make up an imaginary word for the material, not just use "papyrus"). It should have less powerful calciphants than what can be found on tablets but much easier to carry.

Because of this barrier, some cultures may have discovered functional *calciphants* that remain unknown to others. For instance, in most ancient traditions, the use of *arcana* is directly tied to religion and divinity. In these societies, *arithmysticism* is seen as a way to communicate with sacred forces and perceived as a hidden form of knowledge. Therefore the concepts and intricacies found in these *arithmystic* systems are usually not shared with the outside world. This is why *arcanic* effects that are not well known, studied or understood by experts (and the current *arcanic establishment*), are refered to as "sorcery" or "witchcraft".

Finally, another form of "sorcery" or "miracle" (although exceedingly rare) can be found when individuals conceive their own *calciphants* with unprecedented *arcanic* effects but refrain from documenting or sharing them with others. This could be due to them being mute or not knowing how to write. As *arithmyum* is directly solicited from a caster's mind, this makes it look like such individuals are gifted with supernatural abilities and are able to naturally perform feats that are thought impossible or never seen before by most *arcanic* experts.

#### Calciphant Degenerescence

There is a poorly understood phenomenon in *arcanism* where *calciphants*, once functional, gradually lose their effectiveness over long periods of time. This decline is likely attributed to the shifting positions of stars and constellations through the ages.

This process elucidates why old *arithmystic* systems have in their repertoires *calciphants* that lost their use. Mythical spells like "*reviving the dead*" or "*fortune telling*" are well-known *arcanic* effects that have appeared in multiple ancient traditions. Whether these truly functioned during their inception or were just whims of charlatans remains unclear. Nonetheless, the phenomenon of *degeneration* has been observed and studied by *arcanists*, despite the absence of a definitive explanation for its occurrence.




## Artifism


In recent times, advancements in *arcanism* have revolutionized the integration and application of *calciphants* directly into artifacts and machines, resulting in a streamlined process for casting *arcana*. This approach (called *artifism*) treats *arithmyum* as akin to computer code. Runes are inscribed directly onto contraptions, empowering them with specific *arcanic* functionalities and automating the tedious process of having to prepare and ponder *calciphants* before casting a spell.

While *arcanists* still need to master the fundamental principles of *arithmyum*, their capacity in performing *arithmysticism* has been significantly enhanced thanks to this progress. However, they've also become too reliant on it, and this can limit their abilities in certain situations. For instance, certain *artifistic* devices necessitate a caster's proximity to a "magic beacon" (an infrastructure provided by either a guild or a government), for their *arcanic* effects to function as intended.

#### Airships & Mech riding

*Artifism* has allowed the development of powerful technology like *airships*, *mechs* and "mechanical riding creatures" (they work like horses but are upkeeped sort of like motorbikes - with garages, technicians and so on). These technologies have become a key feature in modern warfare.

***