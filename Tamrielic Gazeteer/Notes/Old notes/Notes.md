# NOTES




>https://en.uesp.net/wiki/Lore:Recreation

>"Magic" is "magicka" in the ancient Ayleid language




## Structure


### - PDF 1 : The Adventures

- Extra Rules : languages? religions? faction ties?

- Pre-generated characters?

- Adventure n°1


### - PDF 2 : Vvardenfell

- Third Era introduction -> Tamriel Rebuilt, Project Tamriel, Darkelfguy...

- (Languages & Religions?)

- Diseases

- Morrowind Timeline

- Vvardenfell Introduction (+ map)

- Factions : Census and Excise, Imperial Legion

- The Bitter Coast Introduction (+ map)

- Settlements : Seyda Neen

- Places of Interest: "Ash-hills/Range (Addamasartus?)"

- Dungeons : Abaesen-Pulu Egg Mine

***








# LORE




## Morrowind Timeline


- **Dawn Era** or **Late Middle Merethic Era** : Veloth the Prophet abandons the Summerset Isle and leads the Chimers into Resdayn. According to some legends, the Dwemer originated from the same group as the Chimer, and were treated as another tribe, and later, as a secular Great House. Other sources however claim they already occupied the northeast of Tamriel when Veloth and his people arrived there.

>https://en.uesp.net/wiki/Lore:Dwemer

- **1E 240** : Nordic invasion led by King Vrage

- **1E 668** : War of the First Council, first eruption of Red Mountain

- **1E 700** : Battle of Red Mountain, disappearance of the Dwemer and formation of the Tribunal Temple

- **2E 572** : Second Akaviri Invasion

- **2E 582** : Vvardenfell is saved from the machinations of Clavicus Vile and Barbas

- **2E 582** : A plot by Nocturnal to take over the Clockwork City is foiled

- **2E 882** : Dagoth Ur and The Sixth House reawaken and drive off the Tribunal from Red Mountain (after a great erruption). Tribunal is now cut off from the Heart chamber

- **2E 893** : Barenziah, future Queen of Morrowind, and later Wayrest, is born

- **2E 896** : Tiber Septim conquers Morrowind and the Treaty of the Armistice is signed

- **3E 006** : Hlaalu Athyn Llethan becomes Duke of Mournhold

- **3E 376** : Hlaalu Helseth is born to Queen Barenziah and Symmachus

- **3E 391** : Queen Barenziah goes into exile and her successor, Hlaalu Athyn Llethan, is crowned king of Morrowind

- **3E 396** : Arnesian War. What started as a simple slave revolt in the House Dres lands to the south escalated into a full war between Morrowind and Black Marsh

- **3E 400** : The Sixth House expands substantially, takes Kogoruhn and bases are founded in various places around Vvardenfell

- **3E 411** : Tribunal Temple begins a crusade to stamp out dissidents in their ranks

- **3E 414** : Vvardenfell Territory opened for settlement by King Llethan

- **3E 415** : Sixth House has at least a small cell in every town in Vvardenfell

> [YOU ARE HERE ?]

- **3E 417** : The Tribunal lose Kagrenac's Tools (during a battle at Red Mountain) and stop appearing in public

> [YOU ARE HERE ?]

- **3E 426** : Due to high Imperial tax rates and tariffs on trade, the people of Balmora revolt. The revolt is forcefully put down, but accounts of the event differ

- **3E 426** : Assassinations by the Sixth House of Imperial and Hlaalu supporters increase significantly. So do sudden attacks by the cultists and deranged victims of soul sicknes

> [YOU ARE HERE ?]

- **3E 427** : Beginning of the Blight Curse

> [YOU ARE HERE ?]

- **3E 427** : Hlaalu Helseth crowned king of Morrowind

***








# CHARACTERS




## Races


### - Mer

- **Dark Elf** : *Dunmer*, *Moriche*

- **High Elf** : *Altmer*, *The Cultured People*, *The Elder Ones*

- **Orc** : *Orsimer*, *The Pariah Folk*

- **Wood Elf** : *Bosmer*, *Boiche*, *The Tree-Sap People*, *The Green Ones*


### - Men

- **Breton** : *Manmer*

- **Imperial** : *Cyrodiil*, *Cyro-Nordic*

- **Nord** : *The Children of the Sky*

- **Redguard** : *The Hammerfell Men*


### - Beastfolk

- **Argonian** : *Saxhleel*, *The People of the Root*

- **Khajiit** : *Cathay-Raht*, *Senche-Tiger*




## Languages


>https://en.uesp.net/wiki/Lore:Linguistics

>https://en.uesp.net/wiki/Lore:Ehlnofex_Languages


### - Current

- **Altmeri** : *Elvish*, *High Elvish*, *High Elven*

- **Ashlander** : *Ashlander tongue*, *Ashlander dialect*

>https://www.reddit.com/r/teslore/comments/2iddfx/speak_ashland_old_elf_not_so_good_speak/

- **Bretic** :

- **Dunmeri** : *Dark Elvish*, *Dark Elf language*

- **Jel** : *Argonian*

- **Orcish** :

- **Ta'agra** :

- **Tamrielic** : *Cyrodilic*, *Imperial*, *Common tongue*


### - Old

- **Ald Chimeris** : *Chimeric tongue*

- **Ayleidoon** :

- **Daedric** :

- **Dragonish** : *Dragon Language*, *Dragon tongue*

- **Dwemeris** : *Old Dwarvish*, *Dwemer language*

- **Falmer Language** :

- **Old Cyrodilic** : *Slave's Cant*, *Alessian*

- **Old Elvish** : *Aldmeris*, *Ehlnofex*, *Direnni?*

- **Old Orcish** :

- **Yoku** : *Old Redguard*, *Old Yokudan*, *High-Yokudan*




## Purpose Combination Examples


- **Ascetic Healer** : *genius* & *sage*

- **Itinerant Bard** : *icon* & *sage*

- **Magic Scholar** : *icon* & *genius*

- **Military Battlemage** : *champion* & *genius*

- **Notorious Rogue** : *champion* & *icon*

- **Warrior Monk** : *champion* & *sage*




## Skills


### - Combat

- **Axe** :

- **Heavy Armor** :

- **Long Blade** :

- **Marksman** / **Archery?** :

- **Crossbowmanship** :

- **Short Blade** :

- **Spear** :


### - Sports

- **Acrobatics?** :

- **Climbing?** :

- **Swimming** :


### - General

- **Blacksmithing** :

- **Crafting** / **Artisanry?** :

- **Cuisine** / **Cooking**? :

- **Foraging** / **Scavenging?** :

- **Hunting** :

- **Lockpicking** / **Security?** :

- **Painting** :

- **Pickpocket** :

- **Singing** :


### - Lore

- **Botany** :

- **Cartography** : *Black Marsh*, *Cyrodiil*, *Elsweyr*, *Hammerfell*, *High Rock*, *Morrowind*, *Skyrim*, *Summerset Isle*, *Valenwood*

- **Culture** / **Customs?** :

- **Pathology?** / **Epidemiology** :

- **Geology** / **Mineralogy?** :

- **History** / **Archaeology?** :

- **Reading** / **Writting?** : *Daedric Alphabet*, *Dark Elf Script*, *Dragon Alphabet*, *Elder Alphabet*, *Elven Alphabets*, *Magic Script Alphabet*, *Runic Alphabet*...

>https://en.uesp.net/wiki/Lore:Linguistics

- **Zoology** / **Bestiary?** :


### - Magic

>https://en.uesp.net/wiki/Lore:Magic

- **Alchemy** :

- **Auramancy** :

- **Blood Magic** :

- **Dark Magic** : *Daedric magic*

- **Flesh Magic** :

- **Enchanting** :

- **Kiai** : *Akaviri magic*

- **Mycoturgy** : *Fungimancy*

- **Necromancy** : *Necromantic Arts*, *Dark Arts*, *Dark Practice*

- **Night Magic** : (obscure magical art wielded by the Nightblades)

- **Sight** :

- **Spellcasting** : *Alteration*, *Conjuration*, *Destruction*, *Illusion*, *Restoration*, *Thaumaturgy?*

- **Thaumavocalism** : 

- **Thu'um** :

- **Weather Magic** : *Storm Magic*, *Air Magic*




## Religion

>https://en.uesp.net/wiki/Lore:Gods

>https://en.uesp.net/wiki/Lore:Altmer#Religion_and_Philosophy

>https://en.uesp.net/wiki/Lore:Argonian#Religion - https://en.uesp.net/wiki/Lore:Hist

>https://en.uesp.net/wiki/Lore:Bosmer#Religion

>https://en.uesp.net/wiki/Lore:Breton#Religion

>https://en.uesp.net/wiki/Lore:Dunmer#Religion

>https://en.uesp.net/wiki/Lore:Imperial#Religion

>https://en.uesp.net/wiki/Lore:Khajiit#Religion

>https://en.uesp.net/wiki/Lore:Nord#Religion

>https://en.uesp.net/wiki/Lore:Orc#Religion

>https://en.uesp.net/wiki/Lore:Redguard#Religion

- **Ashlander Cult** : *Ashlander Religion*, *Ancestor Worship*, *Nerevarine Cult*

- **Altmer Pantheon** : *Aedric Cult*, *Altmer Religion*, *Auri-El Cult*, *Aldmeriad*

- **Daedric Princes** : *Daedric Regents*, *Demon Lords of Misrule*, *Lords and Ladies of Oblivion*, *The Second Litter* (to the Khajiit) -> variants: *Malacath Cult*, *Orcish shamanism?*

- **The Eight** : *Bosmer religion*, *Green Pact*, *Y'ffre Cult*

- **Hist** : *Argonian Shamanism?*, *Root-Whisper Cult?*

- **Khajiit Pantheon** :

- **Nordic Pantheon** : *Nordic religion*, *Shor Cult*, *Atmoran religion*

- **Nine Divines** : *Imperial Religion*, *Imperial Cult*, *The Eight and One* -> variants: *Breton Cult?*

- **Redguard Pantheon** : *Yokudan religion*

- **Tribunal Cult** : *ALMSIVI*, *Tribunal Temple*, *Dunmeri Religion*




## Morrowind Factions


### - Ashlander Tribes

- **Ahemmusa** :

- **Erabenimsun** :

- **Urshilaku** :

- **Zainab** :

etc...


### - Great Houses

- **House Dres** :

- **House Hlaalu** :

- **House Indoril** :

- **House Redoran** :

- **House Telvanni** :


### - Imperial Government

- **Census and Excise** : *Tax Collectors*

- **Imperial Knights** :

- **Imperial Legion** :

- **Imperial Navy** :


### - Imperial Guilds

- **East Empire Company** : *EEC*, *East Empire Trading Company*, *The Company*

- **Fighters Guild** :

- **Imperial Archaeological Society** :

- **Mages Guild** :


### - Religious Organizations

- **Buoyant Armigers** :

- **Imperial Cult** :

- **Ordinators** :

- **Tribunal Temple** :


### - Criminal Organizations

- **Camonna Tong** :

- **Thieves Guild** :

- **Twin Lamps** :


### - Secret Societies

- **Dark Brotherhood** :

- **Dissident Priests** :

- **Blades** :

- **Morag Tong** : *Foresters Guild*

- **Order of the Mythic Dawn?** :

- **Sixth House** :

- **Talos Cult** :


### - Vampire Clans

>https://en.uesp.net/wiki/Tamriel_Rebuilt:Factions

- **Aundae** :

- **Berne** :

- **Quarra** :

etc...

***








# LOCATIONS




https://en.uesp.net/wiki/Morrowind:Places




## Ascadian Isles


Southernmost region of Vvardenfell

>https://en.uesp.net/wiki/Morrowind:Ascadian_Isles


### - Settlements

- **Ebonheart** : *Argonian and Skyrim Missions*, *Castle Ebonheart*, *Dragon Square*, *Ebonheart Docks*, *East Empire Company Offices*, *Grand Council Chambers*, *Hawkmoth Legion Garrison*, *Imperial Chapel*

- **Pelagiad** : *Fort Pelagiad*, *Halfway Tavern*

- **Suran** : *House of Earthly Delights*, *Oran Manor*, *Suran Temple*


### - Places of Interest

- **Arano Plantation** :

- **Arvel Plantation** :

- **Dren Plantation** :

- **Fields of Kummu** :

- **Foyada Mamaea** :

- **Gro-Bagrat Plantation** :

- **Hawia Egg Mine** : Vivec's egg mine (belongs to the Temple?)

- **Lake Amaya** :

- **Lake Hairan** :

- **Lake Masobi** :

- **Norvayn Bay** :

- **Odai Plateau** :

- **Odai River** : separates the *Bitter Coast* in two

- **Omani Manor** :

- **Shulk Egg Mine** : large House Hlaalu egg mine

- **Ules Manor** :


### - Dungeons

- **Ald Sotha** : expansive daedric shrine to Mehrunes Dagon

- **Mzahnch** : dwemer ruin

- **Sarano Ancestral Tomb** : large tomb

- **Shurdan-Raplay Egg Mine** : abandoned kwama egg mine

- **Vassir-Didanat Cave** : lost ebony mine that is the subject of much speculation within House Hlaalu




## Bitter Coast


South-western region of Vvardenfell

>https://en.uesp.net/wiki/Morrowind:Bitter_Coast


### - Settlements

- **Gnaar Mok** : *Arenim Manor*

- **Hla Oad** : *Fatleg's Drop Off*

- **Seyda Neen** : *Arrille's Tradehouse*, *Census and Excise Office*


### - Places of Interest

- **Bitter Coast Trail** : footpath that traverses the marshland from north to south, passing through the *Zainsipilu Ashlands*

- **Khartag Point** : (neglected shipwreck right next to it)

- **Odai River Bridge** : small wooden bridge at the edge of the *Odai River* estuary

- **Zainsipilu Ashlands** : small ash hills (continuation of the *Foyada Mamaea*) located next to *Odai River*' south bank


### - Dungeons

- **Abaesen-Pulu Egg Mine** : wild kwama egg mine

- **Addadshashanammu** : daedric shrine to Sheogorath

- **Aleft** : small dwemer ruin

- **Ashalmimilkala** : daedric shrine to Mehrunes Dagon

>https://en.uesp.net/wiki/Lore:Ashalmimilkala

- **Ashurnibibi** : daedric shrine to Malacath

- **Band Egg Mine** : abandoned kwama egg mine

- **Hlormaren** : abandoned dunmer stronghold taken over by smugglers

- **Sunken Shrine** : sunken and ruined daedric shrine to Boethiah


### - Miscellaneous

- **Ancestral Tombs** : *Andrethi*, *Heran*, *Norvayn*, *Samarys*, *Sarys*, *Thelas*

- **Caves & Grottos** : *Abernanit*, *Addamasartus*, *Aharunartus*, *Akimaes*, *Assemanu*, *Ilunibi*, *Mallapi*, *Nimawia*, *Sennananit*, *Shal* *Shurinbaal*, *Yasamsi*, *Zainsipilu*, *Zanabi*




## Red Mountain


Central region of Vvardenfell

>https://en.uesp.net/wiki/Morrowind:Red_Mountain


### - Settlements

- **Ghostgate** : *Temple*, *Tower of Dawn*, *Tower of Dusk*


### - Places of Interest

- **Ahinanapal Camp** :

- **Ghostfence** :

- **Halit Mine** : an operational and well-staffed glass mine

- **Shrine of Pride** :

- **Yanemus Mine** : ebony mine run by ashlanders (Zainabs?)

>https://en.uesp.net/wiki/Morrowind:Yanemus_Mine

- **Yassu Mine** : an operational and well-staffed glass mine (richest deposit on Morrowind)


### - Dungeons

- **Citadel Dagoth** : *Akulakhan Chamber*

- **Citadel Endusal** :

- **Citadel Odrosal** :

- **Citadel Tureynulal** :

- **Citadel Vemynal** :

- **Mausur Caverns** : abandoned ebony mine




## Vivec City


Vvardenfell's largest city

>https://en.uesp.net/wiki/Morrowind:Vivec_(city)


### - Districts

- **Foreign Quarter** : *Guild of Fighters Headquarters*, *Guild of Mages Headquarters*, *Jobasha's Rare Books*

- **Hlaalu Canton** : *Curio Manor*, *Elven Nations Cornerclub*, *Hlaalu Vaults*

- **Redoran Canton** : *The Flowers of Gold*

- **Telvanni Canton** : *The Lizard's Head*, *Telvanni Prison Cells*, *Telvanni Tower*

- **Arena Canton** : *Arena Pit*

- **St. Olms Canton** : *Brewers and Fishmongers Hall*, *St. Olms Storage*, *St. Olms Temple*, *Tanners and Miners Hall*, *Tailors and Dyers Hall*, *Yngling Manor*

- **St. Delyn Canton** :

- **Temple Canton** : *Baar Dau/Ministry of Truth*, *Hall of Justice*, *Hall of Wisdom*, *High Fane*, *Library of Vivec*, *Palace of Vivec*, *Shrine to Stop the Moon*


### - Dungeons

- **Cantons Sewers/Underworks** : *Assernerairan*, *Ihinipalit*, *Ibishammus*

- **Puzzle Canal** :




## West Gash


North-western region of Vvardenfell

>https://en.uesp.net/wiki/Morrowind:West_Gash


### - Settlements

- **Ald Velothi** :

- **Balmora** : *Balmora Temple*, *Council Club*, *Guild of Fighters*, *Guild of Mages*, *Hlaalu Council Manor*, *South Wall Cornerclub*

- **Caldera** : *Ghorak Manor*, *Governor's Hall*

- **Gnisis** : *Arvs-Drelen*, *Fort Darius*, *Gnisis Eggmine*

- **Khuul** :


### - Places of Interest

- **Aidanat Camp** :

- **Caldera Mine** :

- **Koal Cave** :

- **Moonmoth Legion Fort** :

- **River Samsi** :

- **Shashmanu Camp** :

***








# MONSTERS




## Wild


- **Cliff Racer** :

- **Dreugh** :

- **Guar** :

- **Kwama Forager** :

- **Kwama Worker** :

- **Mudcrab** :

- **Nix-Hound** :

- **Betty Netch** : *Female Netch*

- **Bull Netch** : *Male Netch*

- **Rat** :

- **Shalk-Brother Crayfish** : rare species only found in Lake Amaya?

- **Scrib** :

- **Silt Strider** :

- **Slaughterfish** :

- **Water Dreugh** :




## Undead


- **Ancestor Ghost** :

- **Lesser Bonewalker** :

- **Skeleton** :




## Dwemer


- **Centurion Sphere** :

- **Centurion Spider** :




## Daedra


- **Clannfear** :

- **Dremora** :

- **Scamp** :

***








# DISEASES




>https://en.uesp.net/wiki/Morrowind:Diseases




- **Ataxia** :

- **Brown Rot** :

- **Chills** :

- **Collywobbles** :

- **Crimson Plague?** :

- **Dampworm** :

- **Droops** :

- **Greenspore** :

- **Helljoint** :

- **Rattles** :

- **Rockjoint** :

- **Rotbone?** :

- **Rust Chancre** :

- **Serpiginous Dementia** :

- **Swamp Fever** :

- **Witbane** :

- **Witchwither** :

- **Wither** :

- **Yellow Tick** :

******








# QUESTS



- **Missing Fisherman** : A fisherman hasn't returned from his last trip to sea. The players are asked to investigate his disappearance and find any clues about what happened to him.

- **Delivery Delay** : A courier is running late with an important package. The players are asked to find the courier and make sure the package gets delivered to its recipient.

- **Bandit Raids** : Seyda Neen has been experiencing attacks from a group of bandits. The players are asked to track down the bandit camp and put an end to their raids.

- **Ghostly Apparitions** : Some villagers claim to have seen strange ghostly figures near an old ancestral tomb at night. The players are asked to investigate the haunting and find out if there's any truth to the rumors.

- **Stray Guars** : Some guars have escaped from a nearby farm, and the players are asked to help round them up and bring them back to their owner.

- **Diseased Wildlife** : The local wildlife seems to be suffering from a mysterious illness. The players are asked to gather samples and help the town's healer find a cure.







## The Tax Records Riddle


### - Overview

Players are hired by Socucius Ergalla, an officer of the Census and Excise Office in Seyda Neen, to investigate tamperings with tax records that have been causing discrepancies in the town's revenue collection. The quest leads them on a thrilling adventure that culminates in a chase through an old abandoned Kwama egg mine.


### - Introduction

The quest begins with the players meeting Socucius Ergalla at the Census and Excise Office. He explains that someone has been tampering with the town's tax records, causing substantial losses in revenue. The players are tasked with uncovering the culprits and bringing them to justice.


### - Interviewing Suspects

Ergalla provides the players with a list of possible suspects, including town officials, merchants, and tax collectors. The players must interview these individuals, gather clues, and assess their alibis to narrow down the list of potential culprits.


### - Breaking the Code

While investigating, the players stumble upon a mysterious coded message left at the scene of one of the tampered records. They must decipher the code to gain insight into the culprits' motives and next moves.


### - Secrets of the Kwama Egg Mine

As the players follow the clues, they discover a connection to an old abandoned Kwama egg mine located on the outskirts of Seyda Neen. The mine is rumored to be haunted and dangerous, but it holds the key to the culprits' identities.


### - Exploring the Abandoned Mine

The players venture into the dark and eerie Kwama egg mine, facing dangers such as unstable tunnels, lurking creatures, and perhaps even traps set by the culprits to protect their secret.


### - The Chase

Deep within the mine, the players confront the culprits, who turn out to be a group of smugglers using the mine as a hideout. Realizing they are cornered, the smugglers flee deeper into the mine, initiating a thrilling chase sequence.


### - Reward and Resolution

With the culprits captured and the tax records recovered, the players return to Socucius Ergalla to report their success. As a reward, Ergalla offers them a share of the confiscated smuggled goods and expresses his gratitude for their diligent work.

***








## Shadows Over the Bitter Coast


### - Overview

Strange occurrences have been reported in the Bitter Coast region, and Sellus Gravius believes that some dark forces are at play. The players must unravel the mysteries shrouding the region, encountering dangerous foes and enigmatic puzzles along the way. The Bitter Coast isn't a strategic trade route, hence why the Hlaalu or the Imperial legion won't send help (they're currently dealing with unrest in Balmora).


### - Introduction

Sellus Gravius is concerned about recent events in the Bitter Coast. A string of disappearances, eerie lights in the night sky, and the sudden unexplained aggression of local wildlife have raised suspicions that something malevolent is lurking in the area.


### - Gathering Information

Gravius provides the players with a map outlining the affected areas in the Bitter Coast. The players must begin their investigation by talking to the locals, including fishermen, smugglers, and traders in Seyda Neen. These conversations may yield rumors and clues about potential supernatural occurrences.


### - The Hermit's Warning

One of the locals directs the players to a reclusive temple hermit living deep within the Bitter Coast marshes. The hermit, known as Boryn Sendas, claims to have witnessed strange rituals conducted by a group of mysterious figures near the ancient Daedric ruin of Ashalmimilkala. The players must find Sendas' secluded hut and gain his trust to uncover more about the enigmatic figures.


### - Unraveling the Daedric Connection

Boryn Sendas reveals that the figures conducting the rituals are likely cultists devoted to Mehrunes Dagon seeking to unleash a dangerous force upon the land. He provides the players with information about "The House of Troubles", the ruin and its layout.


### - Ashalmimilkala

Guided by the hermit's information, the players set out to find the daedric temple. They must navigate through treacherous swamps and fight off hostile creatures. Upon entering the ruin, they encounter cultists and daedric servants, guarding the path deeper into the mysterious underground complex.


### - Confrontation with the Cultists

As the players progress deeper into the temple, they uncover the true nature of the cult's sinister plans. The cultists aim to perform a dark ritual during an upcoming solar eclipse that would grant them power and summon a malevolent Daedric entity into the world. The players must disrupt the ritual before it's too late.


### - The Final Battle

In a climactic confrontation, the players face the cult's leader and powerful Daedric minions. They must utilize their skills and teamwork to defeat the foes and prevent the catastrophic ritual. The battle is fierce, but with determination and cunning, they emerge victorious.


### - Conclusion:

With the cult's plans foiled, the players return to Seyda Neen to report their success to Sellus Gravius. He commends their bravery and rewards them for their service to Morrowind. As they look upon the horizon, the first rays of the sun illuminate the Bitter Coast, signifying a new dawn free from the malevolent shadows that once plagued the land.



```
Title: Echoes of the Forgotten Ancestors

Quest Overview:
For new players in the land of Vvardenfell, the journey begins with an assignment from Socucius Ergalla at the Seyda Neen Census and Excise Office. Instead of facing dangerous dungeons and intense combat, this quest focuses on exploration, investigation, and interaction with the locals. The players will uncover the mysteries of an ancient ancestral tomb and learn about the history and culture of the Dunmer people.

Quest Introduction:
As the players arrive in Seyda Neen seeking their fortunes, Socucius Ergalla notices their arrival and decides to give them an essential task. He explains that there have been rumors among the locals about strange occurrences near an ancestral tomb hidden in the Bitter Coast. Ergalla suggests the players investigate the tomb to quell the superstitious fears of the nearby villagers and honor the Dunmer ancestors.

Objective 1: Gathering Information
Ergalla provides the players with a rudimentary map pointing to the general location of the ancestral tomb. Before they head out, they must talk to the villagers and fishermen in Seyda Neen, seeking any information or stories related to the tomb. The players may also encounter some friendly traders willing to offer helpful supplies for their journey.

Objective 2: The Wise Guide
One of the villagers introduces the players to a wise Dunmer elder named Nelos, who possesses knowledge about the region's history and the tomb. Nelos tells them about the significance of ancestral tombs to the Dunmer culture and shares legends of the Forgotten Ancestors buried within. He also warns them about a natural guardian spirit that watches over the tomb, advising them to approach it with respect.

Objective 3: Exploring the Ancient Tomb
As the players approach the ancestral tomb, they are greeted by a serene landscape, with ancient ruins and statues dotting the area. Inside the tomb, they find themselves solving puzzles and riddles left behind by the Dunmer ancestors. These challenges are meant to test their wit and wisdom rather than combat skills.

Objective 4: Communing with the Guardian Spirit
As the players delve deeper into the tomb, they encounter the guardian spirit of the Forgotten Ancestors. Instead of fighting it, they have an opportunity to commune with the spirit, learning about its purpose and the ancient bonds it shares with the land. By showing respect and understanding, the players gain the spirit's trust, which helps them further in their quest.

Objective 5: Unraveling the Mystery
As the players progress, they discover inscriptions and relics that reveal a forgotten chapter of Dunmer history, connecting them to the ancestral past. They learn about the struggles and triumphs of the Forgotten Ancestors and the role they played in shaping the local culture.

Objective 6: Paying Respects and Leaving a Legacy
In the heart of the tomb, the players find a chamber dedicated to honoring the Forgotten Ancestors. Here, they have an opportunity to pay their respects and leave a token of remembrance. The players gain a sense of fulfillment and connection to the rich history of Morrowind.

Quest Conclusion:
Upon returning to Seyda Neen, the players share their experiences with Socucius Ergalla. He commends them for their reverence towards Dunmer traditions and expresses his gratitude for restoring peace to the village by dispelling the rumors of dark forces. The players have now earned the respect of the locals, setting the stage for further adventures in the vast world of Morrowind.
```









