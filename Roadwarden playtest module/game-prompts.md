# Ford




A shallow crossing, broad enough for wagons, but infested by insects. This results in travel that would pass through this map location taking increased time. In order to remove this increased time, Sting Ointment must be obtained and used at the ford.

>*Merely owning the ointment is insufficient; the ford must be visited and the ointment used there in order for the travel time reduction to apply. Stinging Ointment can either be crafted at an Alchemy Table if you are a scholar, or purchased from Thyrsus at the Peat fields.*

#### Atmosphere

The creek is gentle today, but the insects are as ubiquitous as ever. Something sits down on your lips even before you reach the ford. You spot a corpse of a small monkey at the side of the creek, getting devoured by thousands of buzzing flies. Many more insects are flying nearby. You ride slowly as you notice an unusual movement at the top of the crag. A large bird is sitting among the rocks like you’re its next dinner.

A dance of flies, gnats, and mosquitoes is disturbed by the arrival of feasting dragonflies, though they present no threat to the sheer number of the blood-sucking creatures. As you get closer to the creek, you hear the pained screech of a small beast. A badger is crossing the water, trying to outrun the insects and hide between the blades of grass.


### - Insect Swarm

The road leads to a gentle creek, filled with the buzzing of flies, mosquitoes, and gnats. You don’t spot any critters among the tall meadows, tree branches, or rocky slopes. Seeing the shapeless, dark vortex of wings, Sodal snorts.

>*With access to an alchemy table, I could make a protective ointment against their bites*

#### I look for a different route

The path is barely squeezed between the hills, with no signs of settlements or game trails, and the grasses too tall and dense to allow you safely ride through the wilderness. You won’t reach the other bank without entering the swarm.

#### It will take some time, but I’ll wrap myself up with every piece of clothing I own.

>*Player wastes time wrapping himself BUT doesn't get hurt by the swarm.*

It takes you longer than you would hope for, but you manage to cover yourself with an armor of at least three layers of linen, leather, wool, and hemp. After more than half an hour, only your eyes and a single finger remain without cover.

Once you get closer to the ford, countless creatures beset you, making you instantly shut your eyes. They are striking the clothes covering your open mouth, and hitting your shell like a squishy, buzzing hail. You ride forward blindly, trying to get out of here as fast as you can, but the insects stick with you for another few minutes.

The red marks on your finger start to burn slowly. Sodal swishes its tail angrily as it turns its head to its left, observing you with one reproachful eye, pinning its ears back.

You look around to make sure nothing will try to jump on you, and once you decide you’re safe, you take off the suit. Unless you find an easier way to protect yourself, the insects will keep slowing you down. At least I kept my blood.

#### I don’t care. I just hurry Sodal.

>*Player doesn't waste any time BUT gets hurt by the swarm.*

Once you get closer to the ford, countless creatures beset you, filling your eyes, mouth, and nose, hitting you like a squishy, buzzing hail. You ride forward blindly, instantly forgetting about your preparations, trying to get out of here as fast as you can, but the insects are sticking with you for another few minutes.

The red marks on your skin start to burn slowly. Sodal swishes its tail angrily as it turns its head to its left, observing you with one reproachful eye, pinning its ears back to its head.

Unless you find a way to protect yourself, the insects will keep slowing you down. At least I didn’t waste any time.

#### I ride forward with a blazing torch

>*Player wastes time preparing the torch AND gets hurt by the swarm.*

You move away from the creek, get on your feet, and unpack the tinderbox, the wooden stave, and the set of rags, as well as the oil. After you assemble a torch, you sit down on the ground, preparing the flint and the linen char cloth. Decades ago, before The Southern Invasion, people used fire strikers made of thick steel, but all you have is a cube-like piece of harsh pyrite. It’s not ideal, but good enough for a couple of sparks. Holding the fire, you get back in the saddle. You swing your weapon left and right - there isn’t as much smoke as you expected.

Once you get closer to the ford, countless creatures beset you, filling your eyes, mouth, and nose, hitting you like a squishy, buzzing hail. You ride forward blindly, instantly forgetting about your preparations, trying to get out of here as fast as you can, but the insects are sticking with you for much longer than the smoke.

It takes a few minutes for you to escape. The red marks on your skin start to burn. You get down to put out the torch by rubbing it against the beaten track. Sodal swishes its tail angrily as it gives you a reproachful look, pinning its ears back to its head.

Well, that didn’t help much. Unless you find an easier way to protect yourself, the insects will keep slowing you down.

#### It’s time to test the ointment I carry in my bundles

>*Success condition*

You open the ointment, getting hit by the unappetizing scent of lavender, mint, and basil. You apply a thin layer of balm to your open skin, as well as to a few spread out spots on Sodal. You hope the smell won’t stay for long.

You carry on. The swarm notices you right away, gliding at you like a dark boulder, but before you collide with it, it shatters, forming a tunnel in the middle, though buzzing even louder than before. A few extraordinarily resilient gnats still sit on your horse’s back, so you try to throw them off.

Riding through this path should be much easier from now on.

***








# Hunter Cabin


An uninhabited wooden structure built in a clearing near a rock face. The chirping of insects is rarely interrupted by a cackle of a distant bird, and just as often by the thunderous roar of the woodland beasts. The only tracks in sight belong to small critters. The cabin has no window, but a stubborn beast could easily burst in through the old thatch. The gust of wind doesn’t move the door. A curious ibex observes you from the crag, but flees from your gaze.

#### Random

- Two pigeons fly off the roof and disappears above the cliff.

- The nearby flowers are crowded with humming bees.

- You notice prints of hooves and clawed paws. The pack was fleeing down the road.

- Termites are climbing the wooden beam that supports the roof. It seems to be their home.

- A loud roar comes from the west, causing the birds to burst into the sky loudly. From here, you don’t see the battling beasts.

#### I check on the cabin

You sneak toward the door and crouch down. After maybe a minute, a quiet rattle comes from the inside, something similar to the sound of cracking knuckles. You peek inside and just barely see a small part of the dusty room. A large spider web is spread among the knocked-down furniture.

#### I enter the cabin

<!-- https://roadwarden.fandom.com/wiki/The_Cabin -->

***








# Epilogue

***




#### If "fail2"

With each passing year, fewer and fewer people risked traveling North, until the forest reclaimed the valley, ending the trail for good. What happened to the tribes became a mystery, but according to the rumors, some of the villages fell, unwilling to unite despite the beasts, bandits, and undead.

#### Elif "fail1"

As time went on, fewer and fewer people risked traveling North, until the valley became a dark trail, visited only by adventurers. The tribes grew even more distant, and were present only in rumors, as the dangerous people of the woods, with mysterious beliefs, strange tongues, and never-healed grudges.

#### Elif "success1"

The flow of merchants and supplies was slow, but steady. The decades went by, but the tribes kept their distance from the cityfolk, seeing in them only trading partners, but benefitting from the safer roads, restored shelters, and new inventions.

#### Elif "success2"

The flow of merchants, soldiers, explorers, and supplies brought many changes to the North. While some of the tribes tried to stay independent, they couldn’t resist the temptation of new inventions, crops, and security. After a few generations, the sparse settlements were indistinguishable from the other regions of the province, with safe roads, strong walls, and regular visitors.

#### Elif "success3"

The sudden flow of merchants, soldiers, explorers, and supplies shook the North. While at first the tribes opposed some of the presented opportunities, they couldn’t resist the temptation of new inventions, crops, and security. After a few years, artisans, herbalists, hunters, and poor cityfolk started to look for a new home in this rich land, and just a generation later, the officials of Hovlavan cleared the wilderness in the southeast, preparing it for a new village loyal to its interests.

<!-- Everything can be found in `epilogue.rpy` -->

<!-- Below, specific slides don't appear if the player never visits their locations (unless for Pelt of the North, which is always required in order to end the adventure) -->




## Pelt of the North


### - If "fail1"

#### If players' relationship with Pelt of the North is excellent

Despite the poor state of the peninsula, Pelt of the North gained quite a lot from the roadwarden’s visit, allowing the innkeep to purchase steel traps for large game. A few years later, the inn was abandoned, to the goblins’ joy.

#### Or if players' relationship with Pelt of the North is good

Pelt of the North became a bit richer during the roadwarden’s visit, but once the roads got overgrown, further trade with the southern villages became close to impossible. After ten years, the hunters abandoned their inn, to the goblins’ joy, and disbanded soon after.

#### Or if players' relationship with Pelt of the North is average or bad

The keeper of Pelt of the North quickly forgot about the roadwarden’s visit. Once the roads got overgrown, further trade with the southern villages became close to impossible. After ten years, the hunters abandoned their inn, to the goblins’ joy, and disbanded soon after.


### - Or if "success1" OR "success2"

#### If players designate a specific village as the starting point for the guild

The trade route following the western trail greatly benefited the dwellers of Pelt of the North. After a few more years, they moved to the city, living off their savings and occasional mercenary work, forming an experienced squad. The inn, purchased by the city officials, remained one of the few shelters in the dangerous North.

- **If players are forgiving towards Tulia after learning about her involvement with Glaucia or don't know about it** : One of the new guards was Tulia, who arrived to Pelt soon after losing her squad, seeking stable work among the hunters.

#### Or if players' relationship with Pelt of the North is excellent

While the trade route was focused on the eastern trail, Pelt of the North gained quite a lot from the roadwarden’s visit, allowing the innkeep to purchase steel traps for large game. A few years later, the inn was sold to the city officials, and remained one of the few shelters in the dangerous North.

- **If players are forgiving towards Tulia after learning about her involvement with Glaucia or don't know about it** : One of the new guards was Tulia, who arrived to Pelt soon after losing her squad, seeking stable work among the hunters.

#### Or if players' relationship with Pelt of the North is good

While the trade route was focused on the eastern trail, Pelt of the North could slowly pursue its financial goals. After another decade, they moved to the city, living off their savings and occasional mercenary work, forming an experienced squad. The inn, taken over by the city officials, remained one of the few shelters in the dangerous North.

- **If players are forgiving towards Tulia after learning about her involvement with Glaucia or don't know about it** : One of the new guards was Tulia, who arrived to Pelt soon after losing her squad, seeking stable work among the hunters.

#### Or if players' relationship with Pelt of the North is average or bad

The keeper of Pelt of the North quickly forgot about the roadwarden’s visit, and needed over ten years to complete his financial goal. Finally, the hunters moved to the city, living off their savings and occasional mercenary work, forming an experienced squad. The inn, taken over by the city officials, remained one of the few shelters in the dangerous North.

- **If players are forgiving towards Tulia after learning about her involvement with Glaucia or don't know about it** : One of the new guards was Tulia, who arrived to Pelt soon after losing her squad, seeking stable work among the hunters.


### - Or if "success3"

#### If players designate a specific village as the starting point for the guild

The trade route following the western trail greatly benefited the dwellers of Pelt of the North. After five more years, they moved to the city to live off their savings, but a few of them got attached to their now-safe home. They remained in the inn and started to expand it, making room for the next generation to join them.

- **If players' relationship with Dalit is excellent** : Dalit, the leader of this new hamlet, allowed needy travelers to stay in the inn free of charge.

- **Or if players' relationship with Dalit is good** : Dalit, the leader of this new hamlet, did her best to keep good relationships with her trading partners from the other settlements.

- **Or if players' relationship with Dalit is average or bad** : Dalit, the leader of this new hamlet, put the safety of her people above all else.

- **If players are forgiving towards Tulia after learning about her involvement with Glaucia or don't know about it** : One of her most trusted friends was Tulia, who arrived to Pelt soon after losing her squad, seeking stable work among the hunters.

#### Or if players' relationship with Pelt of the North is excellent

While the trade route was focused on the eastern trail, Pelt of the North gained quite a lot from the roadwarden’s journey, and finally became an obligatory stop for most traders. After five more years, they moved to the city to live off their savings, but a few of them got attached to their now-safe home. They remained in the inn and started to expand it, making room for the next generation to join them.

- **If players' relationship with Dalit is excellent** : Dalit, the leader of this new hamlet, allowed needy travelers to stay in the inn free of charge.

- **Or if players' relationship with Dalit is good** : Dalit, the leader of this new hamlet, did her best to keep good relationships with her trading partners from the other settlements.

- **Or if players' relationship with Dalit is average or bad** : Dalit, the leader of this new hamlet, put the safety of her people above all else.

- **If players are forgiving towards Tulia after learning about her involvement with Glaucia or don't know about it** : One of her most trusted friends was Tulia, who arrived to Pelt soon after losing her squad, seeking stable work among the hunters.

#### Or if players' relationship with Pelt of the North is good

While the trade route was focused on the eastern trail, Pelt of the North gained quite a lot from the roadwarden’s journey, and finally became an obligatory stop for most traders. After another five years, the hunters moved to the city, living off their savings and occasional mercenary work, forming an experienced squad. The inn, taken over by the city officials, remained one of the most reputable shelters in the peninsula.

- **If players' relationship with Dalit is excellent** : Dalit, the new innkeep, looked after needy travelers free of charge.

- **Or if players' relationship with Dalit is good** : Dalit, the new innkeep, did her best to keep good relationships with her trading partners from the other settlements.

- **Or if players' relationship with Dalit is average or bad** : Dalit, the new innkeep, put the safety of her squad above all else.

- **If players are forgiving towards Tulia after learning about her involvement with Glaucia or don't know about it** : One of her most trusted friends was Tulia, who arrived to Pelt soon after losing her squad, seeking stable work among the hunters.

#### Or if players' relationship with Pelt of the North is average or bad

While the trade route was focused on the eastern trail, Pelt of the North gained quite a lot from its presence. After another ten years, the hunters moved to the city, living off their savings and occasional mercenary work, forming an experienced squad. The inn, taken over by the city officials, remained one of the most reputable shelters in the peninsula.

- **If players' relationship with Dalit is excellent** : Dalit, the new innkeep, looked after needy travelers free of charge.

- **Or if players' relationship with Dalit is good** : Dalit, the new innkeep, did her best to keep good relationships with her trading partners from the other settlements.

- **Or if players' relationship with Dalit is average or bad** : Dalit, the new innkeep, put the safety of her squad above all else.

- **If players are forgiving towards Tulia after learning about her involvement with Glaucia or don't know about it** : One of her most trusted friends was Tulia, who arrived to Pelt soon after losing her squad, seeking stable work among the hunters.




## Howler's Dell




## Old Pagos


### - If players don't cure the plague

#### If Gale Rocks sends help

More than half of the tribe of Old Págos lost their lives to the plague, including many children, and it would be even worse if it weren’t for the help brought by their friends from Gale Rocks.

#### Or if Gale Rocks doesn't send help

More than three-fourths of the tribe of Old Págos lost their lives to the plague, including all of the children.

#### If "fail1"

The next spring, the first few families left the peninsula for good, and in just two years there were but four elders left inside the proud, tall walls. It took many years before the scavengers dared to climb the “haunted” hill, but by that point, the place was overrun by monsters.

#### Or if "success1"

The next spring, the first few families left the peninsula for good, and in just two years there were but four elders left inside the proud, tall walls. It took many years before new settlers dared to climb the “haunted” hill, but by that point, the place was overrun by monsters.

#### Or if "success2"

The next spring, the first few families left the peninsula for good, and in just two years there were but four elders left.  A decade later, a new stone cutting hamlet was built in the shadow of the hill. Every night, the settlers heard haunting roars coming from behind the tall, proud walls.

#### Or if "success3"

The next spring, the first few families left the peninsula for good, and in just two years there were but four elders left. A decade later, the soldiers raided the walls, clearing the village of beasts and preparing the area for the next generation of settlers - but by that point, its past name was believed to be cursed.


### - Or if players cure the plague

#### If Gale Rocks sends help

The tribe of Old Págos lost many lives to the plague, but the supplies brought to them by their friends from Gale Rocks helped them overcome the harsh winter.

#### Or if Gale Rocks doesn't send help

The tribe of Old Págos lost many lives to the plague, and even more so to the harsh winter, but the village survived, as united as ever.

#### If "fail1"

Having hardly any caravans to trade with, the locals focused on farming and goat breeding, but the years to follow were lean, and with every generation more families left their homes to seek luck on the other side of Hag Hills.

- **If players tell Aeli about the copper vein** : Aeli, aware of the village’s poor state, kept the secret of the copper vein to his order.

Tertia, torn by the plague, was among the first souls to leave. She finished her duty in the distant south, buried by a sandstorm in the lands of her ancestors, where she protected those spreading Wright’s teachings.

- **If Quintus leaves the gate** : Quintus eagerly joined his friends at Pelt. His next few years were full of brave hunts and drinking, until he mixed the two a bit too much.

- **Or if Quintus doesn't leave the gate** : Quintus stayed on the wall for the first few nights of autumn, but then, lured by hunger, got caught by an unusually patient treant.

#### Or if "success1" AND players don't tell Aeli about the copper vein

- **If players designate Old Pagos or the Monastery as the starting point for the guild** : Thanks to the revitalized trade route, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. The tribe kept their savings for the darker days, which, they were sure, would come sooner rather than later.

- **Elif "endgame_oldpagos_leader_option"** : Thanks to the revitalized trade route, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. The tribe kept their savings for the darker days, which, indeed, came rather quickly.

- **Else** : The slowly awakening trade route allowed them to return to working with stone, but the rare coins and supplies always left them just on the verge of making ends meet, and the villagers faced many more lean years. With every generation, more families left their homes to seek luck on the other side of Hag Hills.

Tertia, torn by the plague, started a long journey. She finished her duty in the distant south, buried by a sandstorm in the lands of her ancestors, where she protected those spreading Wright’s teachings.

- **If Quintus leaves the gate** : Quintus eagerly joined his friends at Pelt. His next few years were full of brave hunts and drinking, until he mixed the two a bit too much.

- **Or if Quintus doesn't leave the gate** : Quintus stayed on the wall for the first few nights of autumn, but then, lured by hunger, got caught by an unusually patient treant.

#### Or if "success1" AND players tell Aeli about the copper vein

- **If players designate Old Pagos or the Monastery as the starting point for the guild** : Thanks to the revitalized trade route and the earnings from their new copper mine built by the eastern road, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. Their famous statues soon reached the houses in the city. The tribe kept their savings for the darker days, but many years would pass before they felt hunger again.

- **Elif "endgame_oldpagos_leader_option"** : Thanks to the revitalized trade route and the earnings from their new copper mine built by the eastern road, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. The tribe kept their savings for the darker days, which, they were sure, would come sooner rather than later.

- **Else** : Thanks to the revitalized trade route and the earnings from their new copper mine built by the eastern road, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. The tribe kept their savings for the darker days, which, indeed, came rather quickly.

Tertia stayed in the village, but rejected the further responsibilities of a leader, returning the power to the elders. She spent a few years patrolling the roads, until, wounded by a werebadger, she became a combat instructor.

- **If Quintus leaves the gate** : Quintus eagerly joined his friends at Pelt. His next ten years were full of brave hunts and drinking, until he mixed the two a bit too much.

- **Or if Quintus doesn't leave the gate** : Quintus stayed on the wall for the first few nights of autumn, but then, lured by hunger, got caught by an unusually patient treant.

#### Or if "success2" AND players don't tell Aeli about the copper vein

- **If players designate Old Pagos or the Monastery as the starting point for the guild** : Thanks to the revitalized trade route, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. Their famous statues soon reached the houses in the city. The tribe kept their savings for the darker days, but many years would pass before they felt hunger again.

- **Elif "endgame_oldpagos_leader_option"** : Thanks to the revitalized trade route, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. The tribe kept their savings for the darker days, which, they were sure, would come sooner rather than later.

- **Else** : Thanks to the revitalized trade route, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. The tribe kept their savings for the darker days, which, indeed, came rather quickly.

Tertia stayed in the village, but rejected the further responsibilities of a leader, returning the power to the elders. She spent a few years patrolling the roads, until, wounded by a werebadger, she became a combat instructor.

- **If Quintus leaves the gate** : Quintus eagerly joined his friends at Pelt. His next years were full of brave hunts and drinking, until he mixed the two a bit too much, barely escaping a runner. With the help of his companions, he began his path toward maturity.

- **Or if Quintus doesn't leave the gate** : Quintus stayed on the wall for the first few nights of autumn, but then, lured by hunger, got caught by an unusually patient treant.

#### Or if "success2" AND players tell Aeli about the copper vein

- **If players designate Old Pagos or the Monastery as the starting point for the guild** : Thanks to the revitalized trade route and the earnings from their new copper mine built by the eastern road, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. Their famous statues soon reached the houses in the city, and then traveled to distant provinces, unaware of the centuries they were about to witness.  The tribe kept their savings for the darker days, but they were always happy to open the gates for bards and actors, especially those portraying the grand deeds of heroes from Wright’s Tablets. After some time, another play, based on the stories of the tribe, was performed in Hovlavan - The Roadwarden Who Cured The Plague.

- **Elif "endgame_oldpagos_leader_option"** : Thanks to the revitalized trade route and the earnings from their new copper mine built by the eastern road, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. Their famous statues soon reached the houses in the city. The tribe kept their savings for the darker days, but many years would pass before they felt hunger again.

- **Else** : Thanks to the revitalized trade route and the earnings from their new copper mine built by the eastern road, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. The tribe kept their savings for the darker days, which, they were sure, would come sooner rather than later.

Tertia stayed in the village, but rejected the further responsibilities of a leader, returning the power to the elders. She spent a few years patrolling the roads, until, humbled by the selfless help she received during her struggles, she joined the monks on the mountain, guarding them from flying beasts and escorting them on their journeys to other monasteries.

- **If Quintus leaves the gate** : Quintus eagerly joined his friends at Pelt. His next years were full of brave hunts and drinking, until he mixed the two a bit too much, barely escaping a runner. With the help of his companions, he began his path toward maturity.

- **Or if Quintus doesn't leave the gate** : Quintus stayed on the wall for the first few nights of autumn, but then, lured by hunger, got caught by an unusually patient treant.

#### Or if "success3" AND players don't tell Aeli about the copper vein

- **If players designate Old Pagos or the Monastery as the starting point for the guild** : Thanks to the revitalized trade route, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. Their famous statues soon reached the houses in the city, and then traveled to distant provinces, unaware of the centuries they were about to witness.  The tribe kept their savings for the darker days, but they were always happy to open the gates for bards and actors, especially those portraying the grand deeds of heroes from Wright’s Tablets. After some time, another play, based on the stories of the tribe, was performed in Hovlavan - The Roadwarden Who Cured The Plague.

- **Elif "endgame_oldpagos_leader_option"** : Thanks to the revitalized trade route, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. Their famous statues soon reached the houses in the city. The tribe kept their savings for the darker days, but many years would pass before they felt hunger again.

- **Else** : Thanks to the revitalized trade route, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. The tribe kept their savings for the darker days, which, they were sure, would come sooner rather than later.

Tertia stayed in the village, but rejected the further responsibilities of a leader, returning the power to the elders. She spent a few years patrolling the roads, until, humbled by the selfless help she received during her struggles, she joined the monks on the mountain, guarding them from flying beasts and escorting them on their journeys to other monasteries.

- **If Quintus leaves the gate** : Quintus eagerly joined his friends at Pelt. His next years were full of brave hunts and drinking, until he mixed the two a bit too much, barely escaping a runner. With the help of his companions, he began his path toward maturity.

- **Or if Quintus doesn't leave the gate** : Quintus stayed on the wall for the first few nights of autumn, but then, lured by hunger, got caught by an unusually patient treant.

#### Or if "success3" AND players tell Aeli about the copper vein

- **If players designate Old Pagos or the Monastery as the starting point for the guild** : Thanks to the revitalized trade route and the earnings from their new copper mine built by the eastern road, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. Their famous statues soon reached the houses in the city, and then traveled to distant provinces, unaware of the centuries they were about to witness.  The tribe kept their savings for the darker days, but they were always happy to open the gates for bards and actors, especially those portraying the grand deeds of heroes from Wright’s Tablets. After some time, another play, based on the stories of the tribe, was performed in Hovlavan - The Roadwarden Who Cured The Plague.

- **Elif "endgame_oldpagos_leader_option"** : Thanks to the revitalized trade route and the earnings from their new copper mine built by the eastern road, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. Their famous statues soon reached the houses in the city, and then traveled to distant provinces, unaware of the centuries they were about to witness.  The tribe kept their savings for the darker days, but they were always happy to open the gates for bards and actors, especially those portraying the grand deeds of heroes from Wright’s Tablets. After some time, another play, based on the stories of the tribe, was performed in Hovlavan - The Roadwarden Who Cured The Plague.

- **Else** : Thanks to the revitalized trade route and the earnings from their new copper mine built by the eastern road, the locals returned to their craft, providing an essential material to the now-growing security of the northern roads and settlements. Their famous statues soon reached the houses in the city. The tribe kept their savings for the darker days, but many years would pass before they felt hunger again.

After a few years of patrolling the roads, Tertia realized that, despite the nightmares she had seen, the lives led by her friends and relatives were good, safe, and comfortable. Finally, she accepted a seat at the village council, ready to face the responsibilities of a leader until the end of her long life.

- **If Quintus leaves the gate** : Quintus eagerly joined his friends at Pelt. His next years were full of brave hunts and drinking, until he mixed the two a bit too much, barely escaping a runner. With the help of his companions, he began his path toward maturity.

- **Or if Quintus doesn't leave the gate** : Quintus stayed on the wall for the first few nights of autumn, but then, lured by hunger, got caught by an unusually patient treant.




## The Monastery




## White Marshes




## Foggy Lake




## Creeks




## Gale Rocks




## Glaucia




## Eudocia




## Tribe of The Green Mountain




## Asterion


#### Glaucia's dialogue:

- if you ask “I did enough for you already. Help me find Asterion.”

>“He was meant to bring me the treasure from High Island, the largest scrap of land within range of a boat."

- if you ask “What treasure?”

>“Dozens of blades, some enchanted, some not. Left behind by the smiths of The Tribe of The Green Mountain, long before we were born. They had refused to throw them into the sea, and hid them in a cave, at the foot of the volcano.”

>“If you find him after all, hiding in the bushes with some Spear of Immortality, I’d appreciate your tale. But even more so the spear”.

- if you ask “What was your role in all of this?”

>“It would be a priceless find. I put him in touch with a few old friends of mine, gave him a few healing potions and a crossbow, helped him with a few tasks that were pinning him to the land. All for my fair half of what he would bring.”

- if you ask about High Island

>"Your friend by trade kept me in the fog, told me he won’t let me get the treasure on my own. You know as much as I do.”

#### Asterion's goal mirrors the player's goal:

- if you ask Glaucia “But why did he want to find it in the first place?”

>PLAYER GOAL "I need to gather enough dragon bones to help someone I care about." : ANSWER “Who can know for sure? He told me something about a sick daughter", she says with mockery, but he said all that merely to get a better share, I ain’t trusting his talk.”

>PLAYER GOAL "I want to gather enough dragon bones to retire early and live in prosperity and safety." : ANSWER “Who can know for sure? He told me he planned to save a fortune for the rest of his life, but he was one of those who always had a scheme on the side.” A short pause. “I wouldn’t trust a word of his.”

>PLAYER GOAL "I want to be remembered as the soul who brought peace and order to this realm. A hero." : ANSWER “Who can know for sure? He told me something about changing the North, healing the land and suchlike. He said all that merely to get a better share, I ain’t trusting his talk.”

>PLAYER GOAL "While I’m here, I want to help people. Make this region safer for the locals and newcomers alike." : ANSWER “Who can know for sure? He told me something about helping people with these weapons, teaching them how to defend themselves, things like that. He said all that merely to get a better share, I ain’t trusting his talk.”

>PLAYER GOAL "I want to escape my difficult past." : ANSWER “Who can know for sure? He told me something about the sins of his past, people he needed to apologize to, bad blood to clean, all the usual nonsense. He said all that merely to get a better share, I ain’t trusting his talk.”

>PLAYER GOAL "While I’m in the North, I should build connections among the local leaders so I can become a major player in the merchant guild." : ANSWER “Who can know for sure? He told me something about becoming a merchant, the city council, plans for better roads, I hardly listened. He said all that merely to get a better share, I ain’t trusting his talk.”

***
