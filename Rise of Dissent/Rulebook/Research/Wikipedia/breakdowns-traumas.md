# DR.K x DESTINY QUOTES




## 1


https://www.youtube.com/watch?v=JHNCPgcfqfQ&t=1788s

If the challenges that you face are too easy you'll get bored and your performance will drop. If the challenges that you face are overwhelming you will get overwhelmed. Both of these things cause a decrease in performance.

[...]

The interesting thing is that people often feel overwhelmed when they encounter challenges that were imposed on them rather than challenges they willingly chose. An active challenge is something that you chose to engage in which is difficult, a passive challenge is something that is thrust upon you which is difficult. And when your life is one where passive challenges outweight active challenges it leads to feeling overwhelmed and stopping action. It's very hard to motivate yourself, you're kinda getting bodied by life. [...] Once active challenges outweight passive challenges, once the hard things that you do in life are more based on your choice, the more motivated and succesfull you become.

> youtube comment: "Working hard for something you don't care about is called 'stress', working hard for something we love is called 'passion'"




## 2


https://www.youtube.com/watch?v=JHNCPgcfqfQ&t=5155s

The big thing that we've sort of discovered is that when you have a purpose, you can tolerate negativity. And in the absence of purpose you're stuck seeking out pleasure and avoiding pain. And if you build a life that is seeking out pleasure and avoiding pain that life will leave to unhappiness.




## Wikipedia : Anger


People feel really angry when they sense that they or someone they care about has been offended, when they are certain about the nature and cause of the angering event, when they are convinced someone else is responsible, and when they feel they can still influence the situation or cope with it.

For instance, if a person's car is damaged, they will feel angry if someone else did it (e.g. another driver rear-ended it), but will feel sadness instead if it was caused by situational forces (e.g. a hailstorm) or guilt and shame if they were personally responsible (e.g. they crashed into a wall out of momentary carelessness).








# HEALTH / FATIGUE / PHYSICAL / MENTAL




## Strain Injury


A strain is an acute or chronic soft tissue injury that occurs to a muscle, tendon, or both. The equivalent injury to a ligament is a sprain. Generally, the muscle or tendon overstretches and partially tears, under more physical stress than it can withstand, often from a sudden increase in duration, intensity, or frequency of an activity. Strains most commonly occur in the foot, leg, or back.




## Migraine, Headache


The most common types of primary headaches are migraines and tension-type headaches. They have different characteristics. Migraines typically present with pulsing head pain, nausea, photophobia (sensitivity to light) and phonophobia (sensitivity to sound). - Causes of headaches may include dehydration; fatigue; sleep deprivation; stress; the effects of medications (overuse) and recreational drugs, including withdrawal; viral infections; loud noises; head injury; rapid ingestion of a very cold food or beverage; and dental or sinus issues (such as sinusitis). - Migraine is believed to be due to a mixture of environmental and genetic factors. About two-thirds of cases run in families.

Changing hormone levels may also play a role, as migraine affects slightly more boys than girls before puberty and two to three times more women than men. A number of psychological conditions are associated, including depression, anxiety, and bipolar disorder, as are many biological events or triggers.

Migraine may be induced by triggers, with some reporting it as an influence in a minority of cases and others the majority. Many things such as fatigue, certain foods, alcohol, and weather have been labeled as triggers; however, the strength and significance of these relationships are uncertain. Most people with migraines report experiencing triggers.

Common triggers quoted are stress, hunger, and fatigue (these equally contribute to tension headaches). Psychological stress has been reported as a factor by 50 to 80% of people. Migraine has also been associated with post-traumatic stress disorder and abuse. Migraine episodes are more likely to occur around menstruation. Other hormonal influences, such as menarche, oral contraceptive use, pregnancy, perimenopause, and menopause, also play a role.

Between 12 and 60% of people report foods as triggers. There are many reports that tyramine – which is naturally present in chocolate, alcoholic beverages, most cheeses, processed meats, and other foods – can trigger migraine symptoms in some individuals. Likewise, monosodium glutamate (MSG) is frequently reported as a trigger for migraine symptoms.

A 2009 review on potential triggers in the indoor and outdoor environment concluded that while there were insufficient studies to confirm environmental factors as causing migraine, "migraineurs worldwide consistently report similar environmental triggers". The article suggests that people living with migraine take some preventive measures related to indoor air quality and lighting.








# EMOTIONAL / STRESS / MORALE




## Guilt


Guilt is a moral emotion that occurs when a person believes or realizes—accurately or not—that they have compromised their own standards of conduct or have violated universal moral standards and bear significant responsibility for that violation. Guilt is closely related to the concept of remorse, regret, as well as shame.

Both in specialized and in ordinary language, guilt is an affective state in which one experiences conflict at having done something that one believes one should not have done (or conversely, having not done something one believes one should have done).

Guilt and shame are two closely related concepts, but they have key differences that should not be overlooked. Cultural Anthropologist Ruth Benedict describes shame as the result of a violation of cultural or social values, while guilt is conjured up internally when one's personal morals are violated. To put it more simply, the primary difference between shame and guilt is the source that creates the emotion. Shame arises from a real or imagined negative perception coming from others and guilt arises from a negative perception of one's own thoughts or actions.

An individual can still possess a positive perception of themselves while also feeling guilt for certain actions or thoughts they took part in. Contrary to guilt, Shame has a more inclusive focus on the individual as a whole. Fossum and Mason state that "While guilt is a painful feeling of regret and responsibility for one's actions, shame is a painful feeling about oneself as a person". Both shame and guilt are directly related to self-perception, only shame causes the individual to account for the cultural and social beliefs of others.

Paul Gilbert says that "The fear of shame and ridicule can be so strong that people will risk serious physical injury or even death to avoid it. One of the reasons for this is because shame can indicate serious damage to social acceptance and a breakdown in a variety of social relationships. The evolutionary root of shame is in a self-focused, social threat system related to competitive behavior and the need to prove oneself acceptable/desirable to others" Guilt on the other hand evolved from a place of Care-Giving and avoidance of any act that harms others.


### - Psychological effects

Guilt is often associated with anxiety. In mania, according to Otto Fenichel, the patient succeeds in applying to guilt "the defense mechanism of denial by overcompensation...re-enacts being a person without guilt feelings." Self-harm may be used as an alternative to compensating the object of one's transgression – perhaps in the form of not allowing oneself to enjoy opportunities open to one, or benefits due, as a result of uncompensated guilt feelings

Feelings of guilt can prompt subsequent virtuous behavior. People who feel guilty may be more likely to exercise restraint, avoid self-indulgence, and exhibit less prejudice. Guilt appears to prompt reparatory behaviors to alleviate the negative emotions that it engenders. People appear to engage in targeted and specific reparatory behaviors toward the persons they wronged or offended.

As with any other emotion, guilt can be manipulated to control or influence others. As highly social animals living in large, relatively stable groups, humans need ways to deal with conflicts and events in which they inadvertently or purposefully harm others. If someone causes harm to another, and then feels guilt and demonstrates regret and sorrow, the person harmed is likely to forgive. Thus, guilt makes it possible to forgive, and helps hold the social group together.




## Remorse


Remorse is a distressing emotion experienced by an individual who regrets actions which they have done in the past that they deem to be shameful, hurtful, or wrong. Remorse is closely allied to guilt and self-directed resentment. When a person regrets an earlier action or failure to act, it may be because of remorse or in response to various other consequences, including being punished for the act or omission.


### - Physiological effects

Deceptive descriptions of remorse were associated with positive emotions, such as happiness and surprise. Brinke and others established that participants appeared surprised because they could only raise their eyebrows when trying to appear sad, which then caused the participants to feel embarrassed, feel genuine happiness, and let a smile slip. In contrast to deceptive and falsified accounts, genuine accounts were expressed with fewer emotions. Participants showing deceptive or falsified emotions overcompensated their emotional performance. Genuine negative feelings of remorse leaked by the lower face were immediately covered up with a neutral expression. Brinke recorded a small number of body language and verbal cues for deceptive participants; instead, she recorded a large number of speech hesitations that cued deceptive and falsified accounts of remorse.


### - Psychological effects

People may express remorse through apologies, trying to repair the damage they've caused, or self-imposed punishments. Studies indicate that effective apologies that express remorse typically include a detailed account of the offense; acknowledgment of the hurt or damage done; acceptance of the responsibility for, and ownership of, the act or omission; an explanation that recognises one's role.

As well, apologies usually include a statement or expression of regret, humility, or remorse; a request for forgiveness; and an expression of a credible commitment to change or a promise that it will not happen again. Apologies may also include some form of restitution, compensation or token gesture in line with the damage that one has caused.




## Anger / Rage / Wrath


Anger, also known as wrath or rage, is an intense emotional state involving a strong uncomfortable and non-cooperative response to a perceived provocation, hurt or threat. According to cognitive consistency theory, anger is caused by an inconsistency between a desired, or expected, situation and the actually perceived situation, and triggers responses, such as aggressive behavior, with the expected consequence of reducing the inconsistency.


### - Causes

Anger can be of multicausal origin, some of which may be remote events, but people rarely find more than one cause for their anger. Disturbances that may not have involved anger at the outset leave residues that are not readily recognized but that operate as a lingering backdrop for focal provocations of anger. According to Encyclopædia Britannica, an internal infection can cause pain which in turn can activate anger.

Usually, those who experience anger explain its arousal as a result of "what has happened to them" and in most cases the described provocations occur immediately before the anger experience. Such explanations confirm the illusion that anger has a discrete external cause. The angry person usually finds the cause of their anger in an intentional, personal, and controllable aspect of another person's behavior.


### - Physiological effects

A person experiencing anger will often experience physical effects, such as increased heart rate, elevated blood pressure, and increased levels of adrenaline and noradrenaline. The rib cage tenses and breathing through the nose becomes faster, deeper, and irregular. Blood flows to the hands. Perspiration increases (particularly when the anger is intense). The face flushes (facial expressions can range from inward angling of the eyebrows to a full frown). The nostrils flare. The jaw tenses. The brow muscles move inward and downward, fixing a hard stare on the target. The arms are raised and a squared-off stance is adopted. The body is mobilized for immediate action, often manifesting as a subjective sense of strength, self-assurance, and potency. This may encourage the impulse to strike out.


### - Psychological effects

Anger causes a reduction in cognitive ability and the accurate processing of external stimuli. Drawing on an appraisal-tendency framework, we predicted and found that fear and anger have opposite effects on risk perception. Whereas fearful people expressed pessimistic risk estimates and risk-averse choices, angry people expressed optimistic risk estimates and risk-seeking choices. Moreover, estimates of angry people more closely resembled those of happy people than those of fearful people. Dangers seem smaller, actions seem less risky, ventures seem more likely to succeed, and unfortunate events seem less likely. Angry people are more likely to make risky decisions, and make less realistic risk assessments. Anger can potentially mobilize psychological resources and boost determination toward correction of wrong behaviors, promotion of social justice, communication of negative sentiment, and redress of grievances.

In contrast, anger can be destructive when it does not find its appropriate outlet in expression. In its strong form, it impairs one's ability to process information and to exert cognitive control over one's behavior. An angry person may lose their objectivity, empathy, prudence or thoughtfulness and may cause harm to themselves or others.

In inter-group relationships, anger makes people think in more negative and prejudiced terms about outsiders. Anger makes people less trusting, and slower to attribute good qualities to outsiders. Unlike other negative emotions like sadness and fear, angry people are more likely to demonstrate correspondence bias – the tendency to blame a person's behavior more on his nature than on his circumstances. They tend to rely more on stereotypes, and pay less attention to details and more attention to the superficial. In this regard, anger is unlike other "negative" emotions such as sadness and fear, which promote analytical thinking.


### - External Perception

Tiedens et al. have revealed that people who compared scenarios involving an angry and a sad character, attributed a higher social status to the angry character. Her findings clearly indicated that participants who were exposed to either an angry or a sad person were inclined to express support for the angry person rather than for a sad one.

- It was found that people were inclined to easily give up to those who were perceived by them as powerful and stubborn, rather than soft and submissive.

- Findings revealed that participants tended to be more flexible toward an angry opponent compared with a happy opponent, thus showing anger during a negotiation may increase the ability to succeed in it.


### - Coping/Healing

Conventional therapies for anger involve restructuring thoughts and beliefs to bring about a reduction in anger ("Logic defeats anger, because anger, even when it's justified, can quickly become irrational"). Taking deep breaths is regarded as the first step to calming down. Once the anger has subsided a little, the patient will accept that they are frustrated and move on. Lingering around the source of frustration may bring the rage back.

- [The Anger DEMON inside coca cola](https://www.youtube.com/watch?v=n2TEHj5e_lk)

The skills-deficit model also states that poor social skills is what renders a person incapable of expressing anger in an appropriate manner. Social skills training has been found to be an effective method for reducing exaggerated anger by offering alternative coping skills to the angry individual. Research has found that persons who are prepared for aversive events find them less threatening, and excitatory reactions are significantly reduced.

Finally, when people are in a certain emotional state, they tend to pay more attention to, or remember, things that are charged with the same emotion. So it is with anger. For instance, if a person is trying to persuade someone that a tax increase is necessary, if the person is currently feeling angry, they would do better to use an argument that elicits anger ("more criminals will escape justice") than, say, an argument that elicits sadness ("there will be fewer welfare benefits for disabled children").




## Insecurity / Low self-esteem / Self-doubt


Insecurity is a feeling of general unease or nervousness that may be triggered by perceiving of oneself to be vulnerable or inferior in some way, or a sense of vulnerability or instability which threatens one's self-image or ego. This is not to be confused with humility, which involves recognizing one's shortcomings but still maintaining a healthy dose of self-confidence.


### - Causes

Low self-esteem can result from various factors, including genetic factors, physical appearance or weight, mental health issues, socioeconomic status, significant emotional experiences, social stigma, peer pressure or bullying.


### - Psychological effects

- An insecure person lacks confidence in their own value, and one or more of their capabilities, lacks trust in themselves or others, or has fears that a present positive state is temporary, and will let them down and bring about loss or distress by "going wrong" in the future. This is a common trait, which only differs in degree between people.

- Individuals with low self-esteem tend to be critical of themselves. Some depend on the approval and praise of others when evaluating self-worth. Others may measure their likability in terms of successes: others will accept themselves if they succeed but will not if they fail. It may contribute to the development of shyness, paranoia and social withdrawal, or alternatively it may encourage compensatory behaviors such as arrogance, aggression, or bullying, in some cases. People with chronic low self esteem are at a higher risk for experiencing psychotic disorders.

- Insecurity nearly always causes some degree of isolation and withdrawal from people to some extent. The greater the insecurity, the higher the degree of isolation.

>Abraham Maslow described an insecure person as a person who "perceives the world as a threatening jungle and most human beings as dangerous and selfish; feels like a rejected and isolated person, anxious and hostile; is generally pessimistic and unhappy; shows signs of tension and conflict, tends to turn inward; is troubled by guilt-feelings, tends to be neurotic; and is generally selfish and egocentric." He viewed in every insecure person a continual, never dying, longing for security.

A person with low self-esteem may show some of the following characteristics:

- Heavy self-criticism and dissatisfaction.
- Hypersensitivity to criticism with resentment against critics and feelings of being attacked.
- Chronic indecision and an exaggerated fear of mistakes.
- Excessive will to please and unwillingness to displease any petitioner.
- Perfectionism, which can lead to frustration when perfection is not achieved.
- Neurotic guilt, dwelling on or exaggerating the magnitude of past mistakes.
- Floating hostility and general defensiveness and irritability without any proximate cause.
- Pessimism and a general negative outlook.
- Envy, invidiousness, or general resentment.
- Sees temporary setbacks as permanent, intolerable conditions.




## Sadness / Despair


Sadness is an emotional pain associated with, or characterized by, feelings of disadvantage, loss, despair, grief, helplessness, disappointment and sorrow.


### - Physiological effects

- Pupil size may be an indicator of sadness. A sad facial expression with small pupils is judged to be more intensely sad as the pupil size decreases.

- According to DIPR scientist Swati Johar, sadness is an emotion "identified by current speech dialogue and processing systems". She argues that, "when someone is sad, slow, low pitched speech with weak high audio frequency energy is produced". Likewise, "low energy state of sadness attributes to slow tempo, lower speech rate and mean pitch".

- Sadness is, as stated by Klaus Scherer, one of the "best-recognized emotions in the human voice", although it's "generally somewhat lower than that of facial expression". 


### - Psychological effects

An individual experiencing sadness may become quiet or lethargic, and withdraw themselves from others. An example of severe sadness is depression, a mood which can be brought on by major depressive disorder or persistent depressive disorder. Crying can be an indication of sadness.


### - External Perception

A person's own pupil size can become smaller when viewing sad faces with small pupils. No parallel effect exists when people look at neutral, happy or angry expressions. The greater degree to which a person's pupils mirror another predicts a person's greater score on empathy. In disorders such as autism and psychopathy, facial expressions that represent sadness may be subtle, which may show a need for a more non-linguistic situation to affect their level of empathy.


### - Coping/Healing

Some coping mechanisms include: getting social support and/or spending time with a pet or engaging in some activity to express sadness. Some individuals, when feeling sad, may exclude themselves from a social setting, so as to take the time to recover from the feeling.

While being one of the moods people most want to shake, sadness can sometimes be perpetuated by the very coping strategies chosen, such as ruminating, "drowning one's sorrows", or permanently isolating oneself. As alternative ways of coping with sadness to the above, cognitive behavioral therapy suggests instead either challenging one's negative thoughts, or scheduling some positive event as a distraction.




## Panic


Panic is a sudden sensation of fear, which is so strong as to dominate or prevent reason and logical thinking, replacing it with overwhelming feelings of anxiety and frantic agitation consistent with an animalistic fight-or-flight reaction.


### - Causes

Panic may occur singularly in individuals or manifest suddenly in large groups as mass panic (closely related to herd behavior). Though distressing, panic attacks themselves are not physically dangerous. They can either be triggered or occur unexpectedly (due to several disorders including social anxiety disorder, post-traumatic stress disorder, substance use disorder, depression and medical problems).


### - Physiological effects

A panic attack is a sudden period of intense fear and discomfort that may include palpitations, sweating, chest pain, shaking, shortness of breath, numbness, or a feeling of impending doom or of losing control. Typically, symptoms reach a peak within ten minutes of onset, and last for roughly 30 minutes, but the duration can vary from seconds to hours.


### - External Perception

Panic in social psychology is considered infectious since it can spread to a multitude of people and those affected are expected to act irrationally as a consequence. Psychologists identify different types of this panic event with slightly varying descriptions, which include mass panic, mass hysteria, mass psychosis, and social contagion.




## Paranoia / Confusion


Paranoia is an instinct or thought process that is believed to be heavily influenced by anxiety, suspicion, or fear, often to the point of delusion and irrationality. Paranoia is distinct from phobias, which also involve irrational fear, but usually no blame.

>Colby (1981) defined paranoid cognition in terms of persecutory delusions and false beliefs whose propositional content clusters around ideas of being harassed, threatened, harmed, subjugated, persecuted, accused, mistreated, killed, wronged, tormented, disparaged, vilified, and so on, by malevolent others, either specific individuals or groups (p. 518). Three components of paranoid cognition have been identified by Robins & Post: a) suspicions without enough basis that others are exploiting, harming, or deceiving them; b) preoccupation with unjustified doubts about the loyalty, or trustworthiness, of friends or associates; c) reluctance to confide in others because of unwarranted fear that the information will be used maliciously against them (1997, p. 3).

>A relevant difference can be discerned among "pathological and non-pathological forms of trust and distrust". According to Deutsch, the main difference is that non-pathological forms are flexible and responsive to changing circumstances. Pathological forms reflect exaggerated perceptual biases and judgmental predispositions that can arise and perpetuate them, are reflexively caused errors similar to a self-fulfilling prophecy.


### - Causes

Social circumstances appear to be highly influential in paranoid beliefs. Based on data collected by means of a mental health survey distributed to residents of Ciudad Juárez, Chihuahua (in Mexico) and El Paso, Texas (in the United States), paranoid beliefs seem to be associated with feelings of powerlessness and victimization, enhanced by social situations. Potential causes of these effects included a sense of believing in external control, and mistrust which can be strengthened by lower socioeconomic status. Those living in a lower socioeconomic status may feel less in control of their own lives. In addition, this study explains that females have the tendency to believe in external control at a higher rate than males, potentially making females more susceptible to mistrust and the effects of socioeconomic status on paranoia.

An investigation of a non-clinical paranoid population found that feeling powerless and depressed, isolating oneself, and relinquishing activities are characteristics that could be associated with those exhibiting more frequent paranoia.

Emanuel Messinger reports that surveys have revealed that paranoia can develop from parental relationships and untrustworthy environments. These environments could include being very disciplinary, stringent, and unstable. It was even noted that, "indulging and pampering (thereby impressing the child that they are something special and warrants special privileges)," can be contributing backgrounds. Experiences likely to enhance or manifest the symptoms of paranoia include increased rates of disappointment, stress, and a hopeless state of mind.

Discrimination has also been reported as a potential predictor of paranoid delusions. Such reports that paranoia seemed to appear more in older patients who had experienced higher levels of discrimination throughout their lives. In addition to this it has been noted that immigrants are quite susceptible to forms of psychosis. This could be due to the aforementioned effects of discriminatory events and humiliation.

Other researchers have found associations between childhood abusive behaviors and the appearance of violent behaviors in psychotic individuals. This could be a result of their inability to cope with aggression as well as other people, especially when constantly attending to potential threats in their environment. The attention to threat itself has been proposed as one of the major contributors of violent actions in paranoid people, although there has been much deliberation about this as well. Other studies have shown that there may only be certain types of delusions that promote any violent behaviors, persecutory delusions seem to be one of these.


### - Psychological effects

A common symptom of paranoia is the attribution bias. These individuals typically have a biased perception of reality, often exhibiting more hostile beliefs. A paranoid person may view someone else's accidental behavior as though it is with intent or threatening.

Paranoid thinking typically includes persecutory beliefs, or beliefs of conspiracy concerning a perceived threat towards oneself (i.e. "Everyone is out to get me"). Making false accusations and the general distrust of other people also frequently accompany paranoia. For example, a paranoid person might believe an incident was intentional when most people would view it as an accident or coincidence. Paranoia is a central symptom of psychosis.

Due to the suspicious and troublesome personality traits of paranoia, it is unlikely that someone with paranoia will thrive in interpersonal relationships. Most commonly paranoid individuals tend to be of a single status.

According to some research there is a hierarchy for paranoia. The least common types of paranoia at the very top of the hierarchy would be those involving more serious threats. Social anxiety is at the bottom of this hierarchy as the most frequently exhibited level of paranoia.

Many more mood-based symptoms, grandiosity and guilt, may underlie functional paranoia.

>At least 50% of the diagnosed cases of schizophrenia experience delusions of reference and delusions of persecution. Paranoia perceptions and behavior may be part of many mental illnesses, such as depression and dementia, but they are more prevalent in three mental disorders: paranoid schizophrenia, delusional disorder (persecutory type), and paranoid personality disorder.


### - Coping/Healing

Paranoid delusions are often treated with antipsychotic medication, which exert a medium effect size.




## Culpability / Shame


>https://en.wikipedia.org/wiki/Shame




## Depression


>https://en.wikipedia.org/wiki/Depression_(mood)

>https://en.wikipedia.org/wiki/Major_depressive_disorder




## Fear


>https://en.wikipedia.org/wiki/Fear




## Paranoia / Uncertainty / Bewilderment / Distress


>https://en.wikipedia.org/wiki/Paranoid_personality_disorder

>https://fr.wikipedia.org/wiki/D%C3%A9lire_parano%C3%AFaque

>https://en.wikipedia.org/wiki/Mental_distress

>https://en.wikipedia.org/wiki/Anxiety




## Bonus


>https://en.wikipedia.org/wiki/Cognitive_dissonance

>https://en.wikipedia.org/wiki/Disappointment
