# GENERAL-OTHER



## Books


>https://en.uesp.net/wiki/Lore:Dwemer_Inquiries_Vol_I

>https://en.uesp.net/wiki/Lore:The_Brothers_of_Strife

>https://en.uesp.net/wiki/Lore:The_Second_Akaviri_Invasion (mentions of the Inner Sea in 2E 572)

***








# THE BITTER COAST




At some point before the 2E 582 Grandmaster Rythe Verano ordered to cut down a sacred grove located somewhere in the region of the Bitter Coast in order to build a merchant warehouse. The grove was protected by Wardens: Boldekh, and his partner who died due to the Grandmaster's order.

```Source : https://en.uesp.net/wiki/Lore:Naryu%27s_Journal/Vvardenfell,_Before```


## Fauna & Flora

The Inner Sea is home to a species of bioluminescent and sentient Land Coral that grows exclusively on the land around it.

```Source : https://en.uesp.net/wiki/Lore:Bestiary_L#Land_Coral```


## Characters

>https://en.uesp.net/wiki/Lore:Boldekh

>https://en.uesp.net/wiki/Lore:Rythe_Verano

***








# SEYDA NEEN AREA

***