# Introduction




Text

#### Links

- [Roadwarden Wiki](https://roadwarden.fandom.com/wiki/Roadwarden_Wiki)

- [Console Commands](https://steamah.com/roadwarden-console-commands-guide)

<!-- https://steamcommunity.com/sharedfiles/filedetails/?id=2862967129 -->

<!-- Make specific rules for : Cooking, Fishing, Foraging, Hunting & Skinning, Night-time travel & Camping, Tracking, Wild Encounters, Working for a village...? -->

<!-- Hunting : https://en.wikipedia.org/wiki/The_Master_of_Game -->

***








# Overview

***




## Setting




- [The world of Viaticum](https://drive.google.com/file/d/1fQAMR62JVWupJj5jZDp1Z7taqMJmg-hS)




## Objectives


Players incarnate roadwardens, set out from Hovlavan to investigate an unmapped peninsula. Their goal as set by the trade guilds is to investigate the region and report their findings in preparation for an expansion in the area. The catch is that they don’t have forever. They have to leave the peninsula and return to the city by autumn, giving them 40 days to complete the task. Travelling takes time, and characters need to be somewhere safe at night if they don’t want to be attacked by monsters and beasts on the road. Safe is also not the same thing as clean or comfortable.

#### Time Clocks

Tracking the passage of time is an integral part of this module. The GM uses five 8-section **Progress Clocks** to count days, with one 8-section **Progress Clock** representing the *morning/early afternoon* period of each day, and one 6-section **Progress Clock** representing the late *afternoon/evening* period. There's no necessity to track time during the night.

Additionally, the *afternoon/evening* clock has fewer sections as the days progress, representing earlier sunset times:

|Day Count    |Late Afternoon/Evening Clock sections |
|:-----------:|:------------------------------------:|
|<=16         |6                                     |
|>16 and <=32 |4                                     |
|>32          |2                                     |

<!-- https://www.timeanddate.com/sun/poland/warsaw?month=10&year=2024 -->




## The Nameless Peninsula

![Peninsula_Map](https://steamuserimages-a.akamaihd.net/ugc/2042986382411401221/7C7856F1FE6F762D0189DE09364C53AEA58F5CA1/)

<!-- https://static.wikia.nocookie.net/roadwarden/images/e/e6/Peninsula_Map.png/revision/latest/scale-to-width-down/1000?cb=20230527211612 -->

<!-- https://roadwarden.fandom.com/wiki/Locations -->

<!-- https://steamcommunity.com/sharedfiles/filedetails/?id=2957431011 -->

<!-- https://steamcommunity.com/sharedfiles/filedetails/?id=2884391791 -->

<!-- https://roadwarden.fandom.com/wiki/Nameless_Peninsula -->

<!-- https://roadwarden.fandom.com/wiki/Locations?file=Peninsula_Map.png -->

#### Southern Path

- The **Dolmen** : a stone chapel built centuries ago to prohttps://steamah.com/roadwarden-console-commands-guide/https://steamah.com/roadwarden-console-commands-guide/vide shelter to wayfarers.

- **Pelt of the North** : an inn in the south of the Nameless Peninsula.

- The **Ruined Village** : a ruined and abandoned village on the path between Howler's Dell and Pelt of the North.

- The **Southern Crossroads** : a point connecting the deep forests of the East, the western wetlands, and the road to Hovlavan.

- **Tulia's Camp** : a military outpost surrounded by a wooden wall, set in the middle of the valley.

#### Western Wetlands

- The **Beholder** : a large tree located on the western road between the ruined village and Howler's Dell.

- The **Elders' Cave** : an old mine turned into a hamlet protected by a metal door.

- **Howler's Dell** : a prosperous village of farmers and animal breeders in the southwest.

- A **Rockslide** : a mountain pass leading to the sea, blocked by an earthquake.

- The **Western Crossroads** : a well-kept path connecting the major villages of the peninsula.

#### Name? & Northern Bogs

- The **Monastery** : a hard to reach complex of caves turned into a dwelling by an Order of Truth.

- **Old Pagos** : a village of stonemasons and farmers set on the top of a hill, right near the sea.

- The **Peat Fields** : a peatland surrounded by bogs.

- The **River Ford** : a shallow crossing, broad enough for wagons but infested by insects.

- **White Marshes** : a struggling village of peat gatherers and farmers in the northwest.

#### Northern Road

- **Creeks** : a young village of hunters and woodcutters in the northwest.

- **Foggy Lake** : an inn set at the northern crossroads.

- **Gale Rocks** : a crude, aged village of fishers and salt makers, placed among the hills.

- The **Old Tunnel** : an adit carved into a tall mountain.

- The **Ruined Shelter** : a small hamlet that could still be used as a stop for travelers.

#### Eastern Path

- The **Abandoned Watchtower** : an outpost built at the top of a hill, barely taller than the nearby tree crowns.

<!-- - The **Foraging Grounds** : a dry, rocky plain with sparse vegetation, and very few hideouts for predators. -->

- A **Hunter Cabin** : an uninhabited wooden structure built in a clearing near a rock face.

- The **Old Bridge** : a single stone slab placed over a streambed.

- The **Riverside Turn** : a blocked section of the road, impassable for carts and wagons.

- The **Secluded Residence** : a small, but luxurious hamlet, for some reason spared by wild beasts.

- The **Wanderer** : a stone statue of a woman, holding a long staff.

#### Eastern Mountains

- The **Giant** : a centuries old monument portraying a huge man with a club.

<!-- - A **Nest** : a winding, but wide path leading into the eastern mountains. -->

- The **Tribe of The Green Mountain** : an ancient settlement carved into a large, natural cave.

#### Heart of the Forest

- The **Gate** : an unusual structure guarding the path to the heart of the forest.

- The **Hideout** : an old woodcuters' hamlet at the heart of the forest.

- The **Old Forest Garden** : once tamed part of the woods, now neglected.

- A **Woodland Edge** : a path leading to the heart of the forest. The red stone tells travelers to turn back.

#### High Island




## Key People


- **Efren** & **Elah** : brothers from Creeks.

- **Foggy** : the owner of Foggy Lake.

- **Helvius** : the mayor of White Marshes.

- **Iason** : the owner of Pelt of the North.

- **Orentius** : priest of the fellowship of The Wright in White Marshes.

- **Thais** : the mayor of Howler's Dell.

- **Tulia** : lieutenant at the southern military camp.

<!-- https://roadwarden.fandom.com/wiki/Characters -->

<!-- https://roadwarden.fandom.com/wiki/Category:Characters -->




## Pre-made Characters


<!-- Classes -->

<!-- https://roadwarden.fandom.com/wiki/Fighter -->

<!-- https://roadwarden.fandom.com/wiki/Mage -->

<!-- https://roadwarden.fandom.com/wiki/Scholar -->

<!-- #### Warrior : after years of training to become a fighter... -->

<!-- - Force : while your vitality is above 0, you find ways to overcome physical struggles. -->

<!-- - Advanced Training : you have an advantage during physical interactions that involve random chance. -->

<!-- #### Mage : after years of practicing with axes and magical amulets... -->

<!-- - Spells : you can spend pneuma to overcome challenges with wands and amulets. -->

<!-- - Healing ritual : whenever you go to sleep, you can spend pneuma to restore your vitality. -->

<!-- #### Scholar : after years spent on studying old codices and training... -->

<!-- - Knowledge : you’re literate. Your education helps you solve many mysteries. -->

<!-- - Alchemy : with an access to an alchemy table and proper ingredients you can brew useful balms and potions. -->


<!-- Goals -->

<!-- I need to gather enough dragon bones to help someone I care about. A hundred coins would be a good start. I could also look for an expensive treasure, something that’s going to sell well in Hovlavan. -->

<!-- I want to gather enough dragon bones to retire early and live in prosperity and safety. A hundred coins would be a good start. I could also look for an expensive treasure, something that’s going to sell well in Hovlavan. -->

<!-- I want to be remembered as the soul who brought peace and order to this realm. A hero. -->

<!-- While I’m here, I want to help people. Make this region safer for the locals and newcomers alike. -->

<!-- While I’m in the North, I should build connections among the local leaders so I can become a major player in the merchant guild. -->

<!-- I want to escape my difficult past. I could make a place for myself by staying loyal to the merchant guild, or maybe pursue another opportunity while I'm here. -->


<!-- Religion -->

<!-- https://roadwarden.fandom.com/wiki/Religion -->

<!-- Unite - a member of The United Church : you pray to The Wright and follow The River of Order. Like most cityfolk, you believe that people should unite their strength to overcome the threats of nature and dark magic. Everyone will be judged for both good and evil deeds. -->

<!-- Seeker - a member of an Order of Truth : you pray to The Wright and follow The River of Truth. For many years you’ve supported a monastery that does its best to advance mankind’s spiritual growth, artistry, herbalism, and magical research. -->

<!-- Eremite - a member of a fellowship : you pray to The Wright and follow The River of Freedom. You’re from a small village and you believe that the freedom of shell, pneuma, and soul are the main virtues of life. Your community is unique and independent, and so are its members. -->

<!-- Pagan - the path of your ancestors : you have a strong connection to nature and spirits. Your vanishing traditions are forbidden in The Ten Cities, and seen by some as sinister and treacherous. -->

<!-- Atheist : you reject the existence of gods and spirits. -->


### - Warrior

- **Archetypes** : *Champion*, *Sage*

- **Drives** : *Wealth*, *Duty*

- **Quest** : *I need to gather enough dragon bones to help someone I care about. A hundred coins would be a good start. I could also look for an expensive treasure, something that’s going to sell well in Hovlavan.*

- **Religion** : *Eremite - a member of a fellowship. You pray to The Wright and follow The River of Freedom. You’re from a small village and you believe that the freedom of shell, pneuma, and soul are the main virtues of life. Your community is unique and independent, and so are its members.*

- **Proficient Abilities** : *Strength*, *Endurance*

- **Crippled Abilities** : *Eloquence*, *Creativity*, *Knowledge*, *Reasoning*

- **Standard Abilities** : *Dexterity*, *Agility*, *Charm*, *Insight*, *Humility*, *Composure*


### - Mage

- **Archetypes** : *Champion*, *Icon*

- **Drives** : *Authority*, *Influence*

- **Quest** : *While I’m in the North, I should build connections among the local leaders so I can become a major player in the merchant guild.*

- **Religion** : *Unite - a member of The United Church. You pray to The Wright and follow The River of Order. Like most cityfolk, you believe that people should unite their strength to overcome the threats of nature and dark magic. Everyone will be judged for both good and evil deeds.*

- **Proficient Abilities** : *Dexterity*, *Eloquence*

- **Crippled Abilities** : *Insight*, *Humility*, *Composure*, *Endurance*

- **Standard Abilities** : *Strength*, *Agility*, *Charm*, *Creativity*, *Knowledge*, *Reasoning*


### - Scholar

- **Archetypes** : *Genius*, *Sage*

- **Drives** : *Solution*, *Care*

- **Quest** : *While I’m here, I want to help people, solve issues. Make this region safer for the locals and newcomers alike.*

- **Religion** : *Seeker - a member of an Order of Truth. You pray to The Wright and follow The River of Truth. For many years you’ve supported a monastery that does its best to advance mankind’s spiritual growth, artistry, herbalism, and magical research.*

- **Proficient Abilities** : *Knowledge*, *Humility*

- **Crippled Abilities** : *Agility*, *Dexterity*, *Charm*, *Eloquence*

- **Standard Abilities** : *Strength*, *Creativity*, *Reasoning*, *Composure*, *Insight*, *Endurance*




## Inventory


- **Torch** :


### - Alchemy

- **Ointment** :


### - Weapons

- **Crossbow** :

***








# Prologue

***




## Tulia's Camp


Your first objective is to meet with a group of soldiers at a military outpost in the far south of the Nameless Peninsula, set in the middle of the valley. They should be able to give some directions

After a few days of travel, you finally reach the camp

A military outpost in the far south of the Nameless Peninsula, set in the middle of the valley. The wall surrounding it is still standing. There are no wolves around, no stench of blood. Good signs. This should be the place you’re looking for. You were supposed to meet with a group of soldiers, but you hear no voices, no sounds of labor. The gate is partially opened, but the camp isn’t safe. It may keep away the goblins and pebblers, but not beastfolk, nor trolls. And the night is near, you had a long day.

<!-- https://roadwarden.fandom.com/wiki/Tulia%27s_Camp -->




## Reaching the Crossroads


Text

<!-- there should be a combat encounter here...? -->

***








# Southern Path

***




## The Southern Crossroads


Text

***








# Western Wetlands

***




## A Ford


Players traveling north from the Western Crossroads or south from the bogs will encounter a river ford. This gentle creek, with shallow waters broad enough for a wagon, is surrounded by tall meadows, tree branches and rocky slopes. The air buzzes with insects—flies, mosquitoes and gnats swarm everywhere.

<!-- #### Traveling

|Direction                     |Duration |
|:----------------------------:|:-------:|
|Bogs, Northern Road (*North*) |         |
|Western Crossroads (*South*)  |         | -->

#### Examining

On the northern side of the creek, players may spot the corpse of a small monkey, swarmed by thousands of flies. More insects hover nearby, drawn to the gruesome scene.

<!-- #### Foraging -->

<!-- #### Fishing -->


### - Crossing the ford

Despite the creek’s gentle flow, the relentless insects make the crossing very uncomfortable. The path, squeezed between hills, shows no signs of settlements or game trails and the tall, dense grasses prevent safe riding.

#### Cutting through the grass

#### Braving the swarm

If characters decide to brave the swarm, countless insects will beset them. These will strike their clothes, cover their mouths, hit them like a buzzing hail and leave red, burning marks on their skin.

>*This counts as a 1d4 Attack on each character performing the maneuver. It can be resisted with a Pressure 2 Endurance Resistance Roll that raises Frustration.*

- *Using cover* : characters can wrap themselves in every piece of clothing they own, using at least three layers of linen, leather, wool or hemp. This allows them to brave the swarm without suffering any attacks, but takes time to set up. If players take this approach, the GM should tick at least one or two sections on the current **Time Clock**.

- *Using a torch* : although this seems like a good idea, it is ineffective and a waste of time. Characters spend time unpacking, assembling, and lighting the torch, only to realize mid-crossing that it was all for nothing. If players take this approach, the GM should tick one or two sections on the current **Time Clock**, and characters still suffer the attack from the swarm.

- *Using a sting ointment* : this is by far the most efficient way to handle the obstacle. Applying a mixture of basil, mint and lavender to the character's skin will cause the swarm to part, forming a tunnel. Although the smell of the balm is unpleasantly strong, the characters can cross the ford without harm or losing time.

***








# Hag Hills & Northern Bogs

***




Text

***








# Northern Road

***




Text

***








# Eastern Path

***




# Hunter Cabin

***








# Heart of the Forest

***




Text

***








# High Island

***




Text

***








# Epilogue

***




## Pelt of the North




## Howler's Dell




## Old Pagos




## The Monastery




## White Marshes




## Foggy Lake




## Creeks




## Gale Rocks




## Glaucia




## Eudocia




## Tribe of The Green Mountain




## Asterion

***








# Bestiary

***




<!-- https://roadwarden.fandom.com/wiki/Category:Bestiary -->




## Name?


<!-- https://roadwarden.fandom.com/wiki/Apes_and_monkeys -->


### - Apes and monkeys

<!-- https://roadwarden.fandom.com/wiki/Apes_and_monkeys -->

#### Howlers

<!-- https://roadwarden.fandom.com/wiki/Howlers -->

#### Frightapes


### - Beastfolk

<!-- https://roadwarden.fandom.com/wiki/Beastfolk -->

#### Gnolls


### - Corpse eaters

<!-- https://roadwarden.fandom.com/wiki/Corpse_eaters -->


### - Goblins

<!-- https://roadwarden.fandom.com/wiki/Goblins -->




## Bugs


### - Spiders

The most dangerous types inhabit ruins, or very lush and dark parts of the forest. They sit, all by themselves, in their webs, waiting for critters that have lost their way. Some are large like cattle, strong enough to cut through a human shell with their legs. Others use a poison-like substance that destroys their prey’s flesh. It’s impossible to list all the unique tricks they use either to strike first, or to defend themselves.

According to Dalit:

>*“How to fight them? Like a really large one? Without a crossbow? A bow should do fine as well, but aim at the abdomen. A spear would be a good choice and when it gets too close, switch to an axe, or a mace. Or do you mean the smaller ones, like head-size big? Because they often, I don’t know, spit at their targets, or something. You do not want to get hit with it, this web. Dodge it, or get blinded. Maybe jump away. Then you can smash it with whatever you have at hand.”*

<!-- https://roadwarden.fandom.com/wiki/Spiders -->


### - Worms?

<!-- https://roadwarden.fandom.com/wiki/Heart_of_the_Forest -->




## Flying Creatures


### - Gargoyles

<!-- https://roadwarden.fandom.com/wiki/Gargoyles -->


### - Griffons

<!-- https://roadwarden.fandom.com/wiki/Griffons -->


### - Harpies

<!-- https://roadwarden.fandom.com/wiki/Harpies -->




## Giants


### - Golems

<!-- https://roadwarden.fandom.com/wiki/Golems -->


### - Pebblers

<!-- https://roadwarden.fandom.com/wiki/Pebblers -->


### - Treants

<!-- https://roadwarden.fandom.com/wiki/Treants -->


### - Trolls

<!-- https://roadwarden.fandom.com/wiki/Trolls -->




## Name?


### - Huntlords

<!-- https://roadwarden.fandom.com/wiki/Huntlords -->


### - Large Cats

<!-- https://roadwarden.fandom.com/wiki/Large_cats -->


### - Wolves

<!-- https://roadwarden.fandom.com/wiki/Wolves -->

#### Furless Wolves

<!-- https://roadwarden.fandom.com/wiki/Head_of_a_Beast -->


### - Foxes

<!-- https://roadwarden.fandom.com/wiki/Foxes_and_such -->




## Reptiles


### - Dragons

<!-- https://roadwarden.fandom.com/wiki/Dragons -->

#### Dragonlings

<!-- https://roadwarden.fandom.com/wiki/Dragonlings -->


### - Runners

<!-- https://roadwarden.fandom.com/wiki/Runners -->


### - Saurians

<!-- https://roadwarden.fandom.com/wiki/Saurians -->




## Undead


<!-- https://roadwarden.fandom.com/wiki/Undead -->

***
