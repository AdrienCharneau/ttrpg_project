```
   __|  |  |    \    _ \    \     __| __ __| __|  _ \   __|
  (     __ |   _ \     /   _ \   (       |   _|     / \__ \
 \___| _| _| _/  _\ _|_\ _/  _\ \___|   _|  ___| _|_\ ____/
```

***








# **Character Sheet**

***




The character sheet is what records all essential information about a player's character. This includes their name, background, **Abilities**, inventory, equipment and any other stats or values relevant to the game:

- **Name** : the name of the character.

- **Archetypes** : the character's general tone or high concept.

- **Drives** : the character's general motivations.

- **Quest** : the character's unique goal, what they aim to achieve during the scenario.

- **Abilities** : the character's strengths and weaknesses, each associated with a dice and individual **XP** track.

- **Emotions** : the characters' psychological vulnerabilities, each with their own **Threshold**.

- **Health** : the character's physical condition, decreasing as they sustain injuries or physical damage.

- **Stress** : the character's worsening mental condition, increasing as they suffer **Breakdowns**.

>*This information serves as a reference for both player and GM, helping track the character's progress and actions throughout the scenario.*

***








<!-- # Identity




- **Name** :

- **Sex** & **Gender** : *male*/*female*, *man*/*woman*

- **Birthplace** & **Heritage** :

- **Social Status** : *bourgeois*, *proletarian*, *noble*, *peasant*

- **Belief** : *progressive*?, *orthodox*?

Include "traits"? (fat, hairy, etc...)

*** -->








# **Archetypes**

***




**Archetypes** are the central aspects of a character, setting their general tone or high concept. When creating a new character, players must choose and combine 2 **Archetypes** out of the 4 available:

- **Champions** thrive for power and specialize in physical abilities.

- **Icons** thrive for fame and specialize in social abilities.

- **Geniuses** thrive for knowledge and specialize in intellectual abilities.

- **Sages** thrive for wisdom and specialize in spiritual abilities.

**Champions** are natural-born achievers and extremely skilled fighters. They have a firm predisposition for combat, thrive in confrontations and inspire others to respect them.

<!-- With their unwavering focus and determination, they thrive when forging a path for themselves; or when ensuring the protection and prosperity of their group. -->

>*Champions have great ambition, a strong sense of pride and work tirelessly to overcome their enemies. Players that choose this Archetype may want their character to follow a criminal or military career, and specialize in some form of weapon proficiency.*

**Icons** are driven by the desire for social recognition and fame. They excel in fields such as arts, entertainment or politics, and use their skills to inspire, amuse or influence others.

<!-- Their charismatic presence allows them to seamlessly capture the public's attention and shape trends or opinions, pushing them to become influential figures in society. -->

>*Icons are natural-born performers, captivating people and leaving long lasting impressions. Players that choose this Archetype may want their character to specialize in public speaking, pursue some sort of artistic endeavor or follow a political career.*

**Geniuses** are educated individuals committed to the pursuit of knowledge. They excel in tasks like problem-solving, innovation or creative thinking, and may also carry their expertise across multiple disciplines.

<!-- Geniuses are educated individuals committed to mastering specific subjects. They can be practitioners, technicians, researchers or scholars; as long as they carry expertise across one or more disciplines (like biology, literature or archaeology). They usually possess a relentless thirst for knowledge, and excel in things like problem-solving, innovation or creative thinking. -->

>*Geniuses are driven by the desire to solve problems, understand abstract concepts or break established rules. Players that choose this Archetype may want their character to specialize in engineering, spellcasting or gain strong insight into a particular subject.*

**Sages** are wise individuals driven by self-discipline and a desire to help, educate or mentor others. They keep their head up in times of uncertainty, using their willpower to navigate hardships, and find ways to move forward without succumbing to hopelessness.

<!-- Their reflections and understanding of philosophy may help them in guiding others, by providing clarity and discernment to those who need it. -->

>*Sages make perfect healers, monks, teachers or druids. Players that choose this Archetype may want their character to specialize in some form of caretaking discipline, join a monastery, study philosophy or follow their own spiritual path.*




## Archetype Combination


When choosing its 2 **Archetypes**, a player must consider their character's general nature and behavior in order to pick the right combination. This can be illustrated with the lives of two historical figures, Julius Caesar and Marcus Aurelius:

- Julius Caesar, a charismatic leader, exemplified the combination of *champion* and *icon*. His rise to power, from successful pontiff to dictator, showcased his desire for power, influence and popularity among people. He strategically used his oratory skills and military experience to establish himself as the most prominent figure in ancient Rome.

- Marcus Aurelius inherited his role of emperor. As a military leader, his purpose centered around upholding justice, fairness, and ethical values; thus embodying the combination of *champion* and *sage*. His renowned philosophical writings, such as the collection Meditations, reflected his commitment to stoic principles and the pursuit of wisdom.

Although both characters can be classified as *champions*, their overall high-concept diverges based on their second **Archetype**: *icon* or *sage*. So despite their shared influence as roman emperors, both figures possessed distinct traits and leadership styles that illustrate this approach. For instance, here's a list of combinations that could be used for a new character:

- **Arcanic Scholar** (*Genius* & *Icon*) : this character aspires to become a well-known polymath, mastering multiple schools of magic and leaving behind a lasting legacy of famous discoveries and breakthroughs.

- **Ascetic Healer** (*Sage* & *Genius*) : this character aspires to become a reclusive yet compassionate caregiver with extensive medical knowledge, setting them apart as a trusted authority in the region.

- **Itinerant Bard** (*Icon* & *Sage*) : this character aspires to become a charismatic performer who uses their wit and musical talents to entertain or inspire others, encouraging social change and fostering peace.

- **Notorious Rogue** (*Icon* & *Champion*) : this character aspires to become a master of crime and legendary figure, able to outmaneuver authorities and strike fear in the mind of the rich and powerful.

- **War Artifist** (*Genius* & *Champion*) : this character aspires to become a skilled artisan of war machines, blending arcanic knowledge with combat expertise, harnessing their intellect to build powerful weapons able to shape the course of battles.

- **Wandering Knight** (*Champion* & *Sage*) : this character aspires to become a honorable warrior following the codes of a martial doctrine; roaming the land honing their skills, practicing self-discipline and protecting the weak.

>*These are just examples; players highly encouraged to come up with their own ideas.*

<!-- For instance, if a character has the combination of champion and icon, they might aspire to become a powerful combatant with a desire for fame and glory. Whereas a character that combines champion and sage may thrive to become a wandering knight guided by principles of wisdom and virtue. -->




## Drives


**Drives** represent a character's incentives and motivations, guiding their actions throughout the narrative. Each character must choose 2 **Drives** (out of the 12 available), with at least 1 **Drive** matching one of their **Archetypes**:

|Drive           |Archetype |Description |
|----------------|----------|:-----------|
|**Authority**   |Champion  |The character wants to assert dominance and issue commands. |
|**Revenge**     |Champion  |The character seeks justice or retribution. |
|**Wealth**      |Champion  |The character is driven to amass riches and possessions. |
|**Hedonism**    |Icon      |The character prioritizes indulgence and entertainment. |
|**Distinction** |Icon      |The character seeks to impress others and bring attention to themselves. |
|**Influence**   |Icon      |The character aims to sway people to their side and mold their opinions. |
|**Solution**    |Genius    |The character is driven by problem-solving. |
|**Production**  |Genius    |The character wants to build or conceive things. |
|**Curiosity**   |Genius    |The character is motivated by a thirst for knowledge. |
|**Care**        |Sage      |The character wants to help and protect others. |
|**Peace**       |Sage      |The character seeks to foster harmony and mend relationships. |
|**Duty**        |Sage      |The character is dedicated to fulfilling their obligations. |

<!-- - Privilege : the character seeks special attention, entitlement or validation. -->

<!-- By aligning their conduct with these motivations, characters earn XP and gain Advantage when performing actions. -->




## Quest


During a scenario, a character has the option undertake an individual **Quest** that mirrors both of their **Drives**. Players have to write this **Quest** as a concise statement, typically one or two sentences, describing a unique aspiration for their character. Here are some examples:

- **Wealth** & **Duty** : *"I want to travel to the mines of Eldoria and negotiate a contract for my father's gemstone business."*

- **Distinction** & **Solution** : *"I want to decipher the enigmatic Sandravi script and earn acclaim in academic circles."*

- **Authority** & **Care** : *"I want to rule over the city of Lüdim, where hunger and repression have plagued the inhabitants for months."*

- **Hedonism** & **Production** : *"I want to experience the serene sunset of Ignicor and compose a poem about it."*

<!-- Wealth & Duty : "I want to accumulate riches to protect my family from a powerful threat." -->

<!-- Distinction & Solution : I want to find a way to solve a scientific conundrum and gain recognition for my work". -->

<!-- Authority & Care : I want to lead by example, protecting vulnerable people and steering them to a better future. -->

<!-- Hedonism & Production : I want to create an enjoyable and innovative art piece, while indulging in life's pleasures. -->

>*Quests should be seen as active goals, involving things a character strives to accomplish rather than things they wish to avoid.*

<!-- When fulfilling (or at least significantly pursuing) their Quest, a character clears all of their their Emotional Strain and earns large amounts of XP. -->

<!-- https://www.youtube.com/watch?v=E79DDGdX62I&t=1676s -->

<!-- https://www.reddit.com/r/WhiteWolfRPG/comments/2sz9se/personal_aspirations_supernatural_aspirations/ -->

<!-- HAVE PLAYERS WRITE BELIEFS LIKE IN THE BURNING WHEEL AND MOUSEGUARD https://gamersplane.com/forums/thread/2232/ -->

<!-- Invisible Sun - The Key p.166 -->




<!-- ## Flaws


**Flaws** are negative traits that mirror a character's **Drives**. They are used by the GM to force characters into taking a specific course of action or making unexpected choices. Each **Flaw** is associated with 2 **Drives**, and players must pick 1 **Flaw** for each one of their **Drives** (2 **Flaws** in total):

- **Harsh** (*Authority*, *Revenge*) : the character is excessively severe or critical in their words and actions. Their uncompromising demeanor may create undesired rifts with NPCs or allies.

- **Reckless** (*Revenge*, *Wealth*) : the character lacks tolerance for delays, often acting impulsively without considering consequences, potentially endangering themselves and those around them.

- **Greedy** (*Wealth*, *Hedonism*) : the character is consumed by an insatiable desire for material possessions, often prioritizing personal gain over the well-being of the group.

- **Vain** (*Hedonism*, *Distinction*) : the character is excessively superficial and concerned with their ego, making them easier to be manipulated through flattery or gratification.

- **Paranoiac** (*Distinction*, *Influence*) : the character is plagued by irrational fears and suspicions, often interpreting innocent actions as signs of betrayal or danger.

- **Lazy** (*Influence*, *Solution*) : the character avoids putting in effort, preferring to coast by on minimum exertion and relying on others to solve problems or fulfill obligations.

- **Obsessive** (*Solution*, *Production*) : the character may become fixated on particular tasks, ideas, or goals to the point of detriment, neglecting relationships and other aspects of life.

- **Weird** (*Production*, *Curiosity*) : the character exhibits eccentric or unconventional behavior, unsettling others and making them feel uncomfortable in social situations.

- **Intrusive** (*Curiosity*, *Care*) : the character lacks boundaries. They frequently insert themselves where they are not welcome, prying into others' affairs and invading their privacy.

- **Overinvested** (*Care*, *Peace*) : the character is too affected by the problems and tragedies of others, often to the point of being more concerned with their well-being than their own.

- **Submissive** (*Peace*, *Duty*) : the character consistently defers to others' authority or wishes, leading to exploitation by them or inhibiting the group's ability to assert themselves.

- **Dogmatic** (*Duty*, *Authority*) : the character rigidly adheres to a set of beliefs or principles, refusing to adapt or consider alternative perspectives. -->

<!-- They may also want to impose their ideology onto others. -->

<!-- **Impatient** -->

<!-- **Bitter** (*Revenge*, *Wealth*) -->

<!-- **Superficial** -->

<!-- **Jealous** -->

<!-- - **Arrogant** (*Distinction*, *Influence*) : the character believes themselves to be superior to others. They might underestimate opponents, dismiss valuable advice or refuse to perform tasks they deem unworthy. They may reveal important information by bragging too much. -->

<!-- **Overambitious** -->

<!-- **Anxious** -->

<!-- **Eccentric** -->

<!-- **Smothering** -->

<!-- **Overprotective** -->

<!-- **Naive** (*Care*, *Peace*) -->

<!-- **Overzealous** -->

<!-- **Fanatic** -->

<!-- **Intolerant** -->


<!-- - Certain (all?) **Flaws** should have ability score requirements (for example, a character shouldn't have *arrogant* if they have a *humility* score greater than 2 or something).

- Players should feel free to come up with their own **Flaws**? (as long as these match their chosen **Drives**). -->

<!-- *** -->

***








# **Abilities**

***




**Abilities** are numerical values that represent a character's strengths, weaknesses, and overall capabilities. Characters have 12 **Abilities** that can be grouped into 4 different domains (*Physical*, *Social*, *Intellectual* & *Spiritual*):

|Ability    |Domain                   |Usage |
|:----------|:------------------------|:-----|
|Strength   |Physical                 |*Melee combat, breaking things, moving or pushing heavy loads...* |
|Dexterity  |Physical                 |*Reflexes, detection, perception, accuracy...* |
|Agility    |Physical & Social        |*Evasion, athletics, acrobatics, proficiency in sports...* |
|Charm      |Social                   |*Seduction, entertainment, persuading through looks, style or demeanor...* |
|Eloquence  |Social                   |*Public speaking, bargaining, persuading through oratory skills...* |
|Creativity |Social & Intellectual    |*Thinking outside the box, come up with lies, find solutions to problems or puzzles...* |
|Knowledge  |Intellectual             |*Understanding or recalling information, identifying something, learning spells...* |
|Reasoning  |Intellectual             |*Casting spells, computing, calculating, crafting, building or repairing...* |
|Insight    |Intellectual & Spiritual |*Bargaining, reading a situation or person...* |
|Humility   |Spiritual                |*Helping, healing, understanding or tolerating others...* |
|Composure  |Spiritual                |*Keeping calm under pressure, handling stressful situations...* |
|Endurance  |Spiritual & Physical     |*Stamina, fatigue, resisting physical or mental stress...* |

>*Abilities may be used actively as characters strive to accomplish tasks, or reactively to counteract enemy actions or evade environmental dangers.*

**Physical Abilities** relate to a character's bodily aptitude and physical prowess, encompassing their *strength*, *dexterity*, *agility* and *endurance*. These attributes not only determine their combat effectiveness but also play a vital role in athletic challenges:

- **Strength** represents a character's muscle mass, their capacity to lift, carry or move heavy loads, as well as their effectiveness in combat. It is used in melee fights, when breaking things, lifting heavy weights or intimidating others.

>*This Ability is associated with mighty characters, and is the primary attribute for bodyguards, mercenaries and warriors.*

- **Dexterity** symbolizes a character's quick reflexes and precision, measuring their capacity to perform tasks with sleight, detect things, perform quick movements or manipulate objects and devices. It also measures reaction time against surprise attacks or sudden events.

<!-- hit accurately -->

>*This Ability is associated with brisk or handy characters, and is the primary attribute for sharpshooters, assassins and thieves.*

**Social Abilities** relate to a character's social skills and capacity to communicate, encompassing their *charm*, *eloquence*, *agility* and *creativity*. These attributes are used for navigating social situations and concealing intentions, giving opportunities for information gathering, persuasion or deception:

- **Charm** expresses a character's allure, attractiveness and likability. It measures their aptitude to draw people's attention and gain favors through their looks. It plays a role in first impressions and may determine a character's artistic taste, demeanor or dressing style.

>*This Ability is associated with attractive characters, and is the primary attribute for actors, performers and courtesans.*

- **Eloquence** illustrates a character's affability, way of expression and capacity to communicate verbally. It measures their aptitude to speak in a clear and compelling manner, as well as their prowess in rhetoric or argumentation (even in writing).

>*This Ability is associated with extroverted characters, and is the primary attribute for bards, writers, diplomats and politicians.*

**Intellectual Abilities** represent a character's mental acuity and problem-solving capabilities, encompassing their *knowledge*, *reasoning*, *creativity* and *insight*. These attributes are used for magic wielding, engineering tasks or making decisions based on critical thinking:

- **Knowledge** represents the accumulation and recollection of information, reflecting a character's aptitude to remember details about history, arcana or culture. It is used when recalling magic formulas, identifying relevant facts or recognizing important symbols and names.

<!-- It is used when identifying or discerning relevant facts or pieces of information. -->

>*This Ability is associated with literate characters, and is the primary attribute for researchers, loremasters and scholars.*

- **Reasoning** expresses a character's ability to use magic, think rationally and make sound assessments. It plays a role in casting spells, solving puzzles or analyzing complex information.

>*This Ability is associated with technical characters, and is the primary attribute for engineers, craftsmen and spellcasters.*

**Spiritual Abilities** represent a character's wisdom and willpower, reflecting their *humility*, *composure*, *endurance* and *insight*. These attributes are used to withstand harm, hardships and struggles, provide guidance to others or read underlying intentions:

- **Humility** expresses a character's ability to consider the feelings of others, be tolerant or act with care and compassion. It illustrates their aptitude to relate to people, provide support or take on selfless tasks.

>*This Ability is associated with warm-hearted characters, and is the primary attribute for social activists, healers and teachers.*

- **Composure** represents a character's capacity to remain level headed and keep control of themselves. It is used in situations that require self-control or restraint, like handling unfavorable impulses and resisting temptation.

>*This Ability is associated with disciplined characters, and is the primary attribute for ascetics, monks, hermits and soldiers.*

<!-- Composure = having control of your life/having gone through millitary training -->

<!-- Equanimity : https://en.wikipedia.org/wiki/Equanimity -->

**Dual Domain Abilities** share two domains simultaneously, encompassing a character's *endurance*, *agility*, *creativity* and *insight*. These attributes provide synergistic advantages, enabling characters to blend their areas of expertise:

- **Agility** (*Social* & *Physical*) symbolizes a character's movement capabilities, encompassing their speed, body coordination and overall nimbleness. It measures their prowess in scaling, jumping, swimming or dodging attacks.

>*This Ability is associated with swift characters, and is the primary attribute for martial artists, acrobats, dancers and swashbucklers.*

- **Creativity** (*Intellectual* & *Social*) represents a character's aptitude to think outside the box, generate new ideas and come up with innovative solutions. It is used when improvising, designing spells or coming up with lies.

>*This Ability is associated with eccentric characters, and is the primary attribute for inventors, visionaries and artists.*

- **Insight** (*Spiritual* & *Intellectual*) represents a character's maturity and understanding of the world through firsthand experience. It is used for detecting lies, perceiving underlying motives or evaluating the value of goods and services.

<!-- It shows their aptitude in making astute observations or remaining skeptical in suspicious situations. -->

>*This Ability is associated with sharp characters, and is the primary attribute for philosophers, explorers and merchants.*

- **Endurance** (*Physical* & *Spiritual*) embodies a character's toughness or capacity to withstand injuries, reflecting their overall vitality and mental fortitude. It illustrates their aptitude to survive wounds and resist strong psychological pressure.

>*This Ability is associated with resilient characters, and is the primary attribute for scouts, sailors and hunters.*




## Ability Scores


To measure aptitude, each **Ability** is assigned with a score ranging from 1 to 5 and a specific dice class:

|Ability Score |Dice Class    |Description |
|:------------:|:------------:|------------|
|1             |d4            |The character is weak or inept with the **Ability**. |
|2             |d6            |The character is intermediate or has basic aptitude with the **Ability**. |
|3             |d8            |The character is above average or has good aptitude with the **Ability**. |
|4             |d10           |The character is very talented or highly capable with the **Ability**. |
|5             |d12           |The character is exceptionally accomplished or virtuosic with the **Ability**. |




## Character Setup


### - Assessing Categories

Depending on the player's choices at character creation, **Abilities** are grouped into one of three categories (*proficient*, *crippled* or *standard*):

- **Proficient Abilities** are **Abilities** that a character has an affinity with, allowing for quicker training and development compared to others. They start with a score of 3 and have a **XP** track of 6.

- **Crippled Abilities** are **Abilities** that a character struggles with, holding them back as they cannot be leveled. They always have a score of 1 and no **XP** track.

- Finally, **Standard Abilities** are those that don't fall into any of the other two categories. They start with a score of 2 and have a **XP** track of 9.

When creating a new character, a player must pick two **Proficient Abilities** and four **Crippled Abilities** for them:

- For **Proficient Abilities**, the choice should be based on the character's **Archetypes**. *Champions* may only pick between *physical* **Abilities**, *icons* between *social* **Abilities**, genius between *intellectual* **Abilities** and *sages* between *spiritual* **Abilities**.

- For **Crippled Abilities**, the choice is free as long as the character doesn't have more than one **Crippled Ability** aligned with an **Archetype**. For instance, *champions* cannot have more than one crippled *physical* **Ability**, *icons* cannot have more than one crippled *social* **Ability**, genius cannot have more than one crippled *intellectual* **Ability**, and *sages* cannot have more than one crippled *spiritual* **Ability**.

<!-- This means that new characters have a grand total of 22 Ability points. -->


### - Setting Health

Each character has a maximum **Health** value, representing their capacity to withstand physical wounds and remain conscious. This is how it's calculated (starting at 0):

|Ability Score     |Endurance         |Strength          |Agility           |
|:----------------:|:----------------:|:----------------:|:----------------:|
|1                 |total *health* +5 |total *health* +2 |total *health* +1 |
|2                 |total *health* +6 |total *health* +3 |total *health* +2 |
|3                 |total *health* +7 |total *health* +4 |total *health* +3 |
|4                 |total *health* +8 |total *health* +5 |total *health* +4 |
|5                 |total *health* +9 |total *health* +6 |total *health* +5 |

>*For instance, a character with an endurance score of 3, strength of 2, and agility of 1 would have a total Health of 11.*

<!-- Provide an example? -->


### - Setting Emotional Thresholds

**Emotions** (*anger*, *anxiety* *sorrow* and *guilt*) represent a characters' psychological vulnerabilities, influencing their behavior and responses to adversity. Each **Emotion** is assigned with a **Threshold**, which determines how much **Emotional Strain** <!-- for that particular Emotion--> a character can hold before entering a **Breakdown** state. This is how these values are established:

- First, each **Emotion** is associated with an **Ability** and an **Archetype** :

|Emotion     |Ability   |Archetype |
|:-----------|:---------|:---------|
|Anger       |Humility  |Champion  |
|Anxiety     |Insight   |Icon      |
|Sorrow      |Composure |Genius    |
|Guilt       |Composure |Sage      |

- Second, based on the character's **Ability** score associated with the **Emotion**, and if they've picked the related **Archetype**, a value is assessed:

|Ability Score |Without Archetype |With Archetype |
|:------------:|:----------------:|:-------------:|
|1             |Threshold = 3     |Threshold = 2  |
|2             |Threshold = 4     |Threshold = 2  |
|3             |Threshold = 5     |Threshold = 3  |
|4             |Threshold = 6     |Threshold = 3  |
|5             |Threshold = 7     |Threshold = 4  |

>*For instance, an icon/sage character with humility 2, insight 3, and composure 2 has the following Emotional Thresholds: anger 4, anxiety 3, sorrow 4, and guilt 2.*

Players must keep in mind that, during a game, **Ability** scores may increase and character **Archetypes** may change. Once a scenario is over, players should re-calibrate their total **Health** and **Threshold** values to reflect these changes (if any).

***








<!-- # **Skills**

***




Skills do exist, but they're just here to tell players if their character is allowed to resolve a specific task or not (eg do they have the "repair" skill? -> they can attempt a reparation, do they have the "cast" skill -> they can attempt casting a spell, etc...):

- Acrobatics

- Alchemy

- Archery

- Botany

- Cooking

- Crafting

- Disguise

- Dancing

- Drawing/Painting

- Engineering

- First Aid

- Forgery

- Healing

- Music Instrument

- Knowledge/Erudition

- Lockpicking

- Martial Arts

- Mining

- Orientation/Cartography

- Pickpocketing

- Reading/Writing

- Riding

- Sailing

- Singing

- Smithing

- Spellcasting

- Stealth/Camouflage

- Survivalism

- Swimming

- Taming

- Throwing

- Tracking

- Weapon Handling

>*Skills are different from abilities, which represent a character's innate attributes. Unlike abilities, skills do not have any inherent value or score, but are simply a representation of "knacks" or "know-hows" a character has learned through their life, that allow them do certain things.* -->

<!-- - **Combat Skills** : swordsmanship, archery...
- **Stealth Skills** : pickpocketing, lockpicking...
- **Magic Skills** : spellcasting, alchemy...
- **Survival Skills** : foraging, orientation...
- **Crafting Skills** : blacksmithing, tailoring...
- **Knowledge Skills** : history, arcana...
- **Performance Skills** : singing, instrument...
- **Athletic Skills** : climbing, swimming...
- **Medical Skills** : first aid, surgery...
- **Communication Skills** : reading, writing...
- **Artistic Skills** : painting, sculpting... -->

<!-- Reminder: swordfighting doesn't solely enable a character to "use swords" (because anyone knows how to deliver a blow with pretty much anything). Instead, this skill focuses on enabling a character to execute specific combat maneuvers such as "disarming opponents", "delivering precise strikes", "backstabbing" etc... -->

<!-- Change this chapter to "advantages" instead of just "skills"?: "advantages" would include skills, achievements (ranks, feats, etc...), assets (things owned like land or cattle) -->

<!-- *** -->








# **Template Characters**

***




## Lucian


![*Lucian*](https://i.imgur.com/eIhWYrY.jpg)


- **Archetypes** : *Genius*, *Sage*

- **Drives** : *Solution*, *Care*

- **Quest** : *"I want to develop a treatment for the grey plague, and provide relief to those afflicted by its devastating effects."*

- **Proficient Abilities** : *Reasoning*, *Humility*

- **Crippled Abilities** : *Strength*, *Agility*, *Charm*, *Eloquence*

- **Standard Abilities** : *Dexterity*, *Creativity*, *Knowledge*, *Insight*, *Composure*, *Endurance*

- **Health Pool** : 9

- **Emotional Thresholds** : *Anger* (5), *Anxiety* (4), *Sorrow* (2), *Guilt* (2)

>*Lucian is a dedicated healer with strong intellect and compassion. From a young age, he showed an affinity for herbalism and alchemy, while also helping care for the wounded at a local monastery. It wasn’t until the grey plague swept across the region that he found his true calling: discover a cure for the disease and alleviate the suffering it caused.*

>*This character possesses a sharp mind, honed through years of rigorous study and practical experience. His keen insightful nature allows him to remain grounded, recognize the limits of his understanding and the importance of collaboration. However, his strength and nimbleness pale in comparison to his acumen, and his reserved personality leads to challenges when trying to command attention.*

<!-- Lucian maintains strong resilience, having experienced first hand the many facets of human suffering. He approaches challenges with calm and confidence, viewing them as opportunities for growth rather than obstacles. But he is also prone to self-doubt, often struggling with issues he can't understand, or feeling overwhelmed by the weight of past decisions. -->




## Morwen


![*Lucian*](https://i.imgur.com/hGerJ5G.jpg)


- **Archetypes** : *Champion*, *Icon*

- **Drives** : *Revenge*, *Solution*

- **Quest** : *"I want to find the person who murdered my crew, and make them pay for what they've done."*

- **Proficient Abilities** : *Dexterity*, *Eloquence*

- **Crippled Abilities** : *Knowledge*, *Endurance*, *Humility*, *Composure*

- **Standard Abilities** : *Strength*, *Agility*, *Charm*, *Creativity*, *Reasoning*, *Insight*

- **Health Pool** : 10

- **Emotional Thresholds** : *Anger* (2), *Anxiety* (2), *Sorrow* (3), *Guilt* (3)

>*Morwen is a rogue who rose from humble beginnings in the streets of Lüdim. While growing up, she took part in various criminal operations, slowly making a name for herself and building a network of trusted allies. However, her life took a dark turn when her hideout was raided, resulting in the death of her crew. She now wants to uncover the culprit and avenge her friends.*

>*Morwen's strength lies in her dexterity and silver tongue, moving through shadows with grace and precision. However, she is not without her vulnerabilities. Beneath her charming facade, she may not always be the most wise or composed individual, often manifesting fits of anger when things don't go her way.*

<!-- Morwen is not without her vulnerabilities. If things don't go her way, her inner turmoil often manifests in fits of frustration. She also harbors a considerable fear of being judged, relentlessly seeking validation or approval from others. Every failure or criticism cuts deep, amplifying her already poor judgmental nature into cycles of anger and denial. -->

***
