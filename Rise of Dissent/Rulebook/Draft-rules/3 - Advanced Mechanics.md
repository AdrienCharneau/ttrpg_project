```
    \        |                                   |     \  |             |                 _)
   _ \    _` | \ \ /  _` |    \    _|   -_)   _` |    |\/ |   -_)   _|    \    _` |    \   |   _| (_-<
 _/  _\ \__,_|  \_/ \__,_| _| _| \__| \___| \__,_|   _|  _| \___| \__| _| _| \__,_| _| _| _| \__| ___/
```

***








# **Context Modifiers**

***




**Context Modifiers** adjust the outcome of **Action Rolls** based on various circumstances. They help create a more immersive experience by accounting for factors that may affect an action's success or failure. These **Modifiers** can be positive or negative, reflecting things like environmental conditions, character states or equipment.

>*For example, climbing a fence in a rainstorm is less likely to succeed than on a calm, sunny day. Similarly, a stealth move is more likely to succeed in a dark or noisy area but less likely if the ground is covered in crunchy leaves.*

**Context Modifiers** exist in 4 different categories: *environment*, *actors*, *means* and *condition*. When performing an **Action Roll**, each category is assigned with a value of -1 (*negative*), 0 (*neutral*), or +1 (*positive*) based on context; with all categories having a *neutral* value of 0 by default:

#### Environment

*Environment* represents the physical or social space in which the action takes place. This includes tangible aspects like physical structures or weather, as well as intangible aspects like faction territory or cultural expectations of a location. If *positive* (+1), the *environment* provides conditions that aid the character in performing their action. If *negative* (-1), the *environment* presents obstacles that hinder them. *Neutral* (0) is the default value where the *environment* doesn't significantly affect much:

- **Positive Environment (+1)** : the character is fighting from a high ground position, they are attempting a stealth maneuver at night, they are located in a social setting suitable for persuasion (like a tavern or a cosy garden), they are using snow-covered terrain to track enemies or prey etc...

- **Negative Environment (-1)** : the character is fighting in a cramped space, they are navigating an unknown area with confusing topography, they are dealing with strong headwinds while trying to shoot with a bow, they are challenging the words of a noble in their own castle etc...

#### Actors

*Actors* refer to the individuals or creatures involved, either aiding or opposing the character's action. These can include allies, enemies or bystanders whose behavior may impact the outcome of the roll. If *positive* (+1), the amount of *actors* assisting the character outnumbers the ones opposing them. If *negative* (-1), the amount of *actors* opposing the character outnumbers the ones assisting them. *Neutral* (0) is the default value where there are no *actors*, or the ones present do not influence the situation significantly:

- **Positive Actors (+1)** :

<!-- the character is supported by a group of skilled allies, they have a healer nearby providing continuous aid, they are joined by a powerful creature or summoned entity, they are surrounded by friendly townsfolk cheering them on etc... -->

- **Negative Actors (-1)** : the character is attempting to intimidate a group of thugs alone, they are fighting outnumbered, they are trying to contain a herd of animals by themselves,

#### Means

*Means* refer to the various assets available to a character, such as weapons, tools, pieces of information or any other resource that can facilitate or hinder their action. If *positive* (+1), the character has access resources, quality gear or relevant information that significantly helps them in their endeavor. If *negative* (-1), the character lacks adequate resources, uses broken gear or possesses incorrect or wrong information. *Neutral* (0) is the default value, where the *means* (or lack thereof) do not significantly affect the character's action:

- **Positive Means (+1)** : the character is using a map to navigate unfamiliar terrain, they know about an enemy's weak spot during a fight, they are attempting to bribe someone with a substantial amount of coins, they are cutting down a tree with an exceptionally good axe etc...

- **Negative Means (-1)** : the character is fighting with a broken weapon, they're attempting to convince someone using inaccurate or outdated information, they are working with improvised or makeshift tools, they are facing a language barrier in a negotiation etc...

#### Condition

*Condition* describes the overall state of the character, factoring in their fatigue, carry load, hygiene, drug influence or any other comfort or affliction that may help or hinder them. If *positive* (+1), the character's *condition* is optimal, providing them with enhanced capabilities or morale. If *negative* (-1), the character's *condition* is detrimental, reducing the effectiveness or ability to perform. *Neutral* (0) is the default value where the character is in a regular state, neither good or bad, and their *condition* does not noticeably affect them:

- **Positive Condition (+1)** : the character is well-rested and energized, they have recently received a beneficial magical blessing, they're attempting to persuade someone while bathed, well-dressed and wearing perfume, they are under the influence of a powerful and positive elixir etc...

- **Negative Condition (-1)** : the character is exhausted and sleep-deprived, they are attempting an acrobatic maneuver while burdened by heavy equipment, they are fighting an enemy while suffering from a debilitating illness, they are trying to persuade someone while drunk etc...




## Stacking Values


When performing a roll, characters may only have one **Modifier** per category. But all three categories stack with each other to form a roll's total **Modifier** rating with a value of *X* (ranging from -4 to +4).




## Altering Die


Don't implement **Context Die**. Instead, the total **Context Modifier** value "upgrades" (if positive) or "downgrades" (if negative) die classes rolled during an **Action Roll**. This applies only to the **Initial Roll**, not the **Daring Roll**.

#### Paths

- **Upgrading** : *d4* -> *d6* -> *d8* -> *d10* -> *d12*

- **Downgrading** : *d12* -> *d10* -> *d8* -> *d6* -> *d4*

#### Upgrading

- If **Context** is positive, "upgrade" a number of die equal to the total **Context**, starting with the lowest class die. A single die can only be "upgraded" once.

- If a die cannot be upgraded (i.e., all available die are already at d12), gain automatic **Successes** for each unused **Context** value.

#### Downgrading

- If **Context** is negative, "downgrade" a number of die equal to the total **Context**, starting with the highest class die. A single die can only be "downgraded" once.

- If a die cannot be downgraded (i.e., all available die are already at d4), suffer automatic **Troubles** for each unused **Context** value.

<!-- If I go with this, I should also make it so characters can earn XP with and level up their crippled Abilities - Gain Ability XP if max value on a die + Daring Roll -->




## Example


Text




<!-- ## Cooperative Rolls


A character may choose to help another character perform an **Action Roll**. If so, the character gains a +1 *actors* **Modifier**:

|Ability Score |Rule |
|:------------:|:----|
|1             |Cannot provide assistance. |
|2-3           |Raise an Emotion by 1 stage to raise the Actor Modifier by one level. |
|4-5           |Raise the Actor Modifier by one level (without raising any Emotion). | -->

<!-- Check FATE - Core System p.182 (or 174) : 2 pages of Teamwork mechanics -->

<!-- Cannot do this if negative Sympathy towards the target? -->

***








# **Ability XP**

***




As stated previously, each **Ability** is accompanied with its own **XP** track. When a character fills a track entirely, its associated **Ability** score increases by 1 and the track is reset back to 0. There are two different ways of marking **XP**:

- When performing a **Resistance Roll** and obtaining the highest number on a die. Each highest number marks 1 **XP** for the **Ability** used in the roll.

>*If a player performs a Resistance Roll using an Ability rated d6, they mark 1 XP to that Ability for each result of 6 obtained during the roll.*

- When performing an **Action Roll** with a **Daring Roll** and obtaining the highest number on a die associated with one of the two **Abilities** used in the roll. Each highest number marks 1 **XP** for the ability associated with it, regardless of the outcome of the roll.

>*If a player performs an Action Roll with a Daring Roll using Abilities rated d6 and d8, they mark 1 XP to the first Ability for each 6 obtained on a d6, and 1 XP to the second Ability for each 8 obtained on a d8.*




## Example


Finn wants to sneak behind a group of bandits and disarm their leader before a fight breaks out:

- The GM sets a **Challenge** of 3, a **Risk** of 2 and picks *dexterity* (score 2) and *agility* (score 3) for the action. This gives the player a set of d6s and d8s for their roll.

- The action is resolved and, after performing a **Daring Roll**, the results are 5, 3, and 3 on d8s (1 **Success Die**) and 6, 2 and 1 on d6s (1 **Success Die** and 2 **Trouble Die**). *Frustration* was raised as Finn sees the bandits as an annoyance more than anything.

- Since the total number of **Success Die** (2) did not match the **Challenge** but the total number of **Trouble Die** (2) did match the **Risk**, the action ends in a *failure*. As Finn tries to slip through the shadows, a twig snaps under his foot, alerting the bandits to his presence. <!-- The leader swiftly turns around, catching him in the act. -->

- Yet, because the player rolled a 6 on a d6 and decided to perform a **Daring Roll**, Finn marks 1 **XP** to the **Ability** associated with that result, here being *dexterity*.

Subsequently, the GM allows the player to resist the consequence of the action against a **Pressure** of 3<!-- (explain why) --> using Finn's *insight*<!-- (as the action was tied to frustration)-->:

- Finn's *insight* score is 2, so they roll three d6s.

- They get results of 6, 6 and 2, with 2 counting as 1 **Trouble Die**. His *frustration* is increased by 1 and Finn's *failure* is upgraded to a *limited failure*.<!-- The GM tells the player that Finn second-guesses their action, retreats and no one notices him. -->

- And because the player rolled two 6s, Finn also marks 2 **XP** to the **Ability** associated with the roll, here being *insight*.

***








# **Social Influence**

***




Social influence is the system used for resolving situations where characters aim to manipulate, persuade or coerce each other. Successful influence rolls can convince characters or NPCs to act a certain way, alter their opinions or extract benefits from them.

>*This can range from inciting war to gaining support from allies, alienating individuals or using propaganda to incite a revolt.*

When engaging in social influence, the first step  is to declare a target and articulate a desired outcome. Simply put, state what the character wants their subject to do, along with a rough idea on how they aim to achieve it.

>*Certain limitations apply. No level of charisma can compel someone to commit suicide or give up their entire wealth. Similarly, absurd requests or things that could make a player uncomfortable should be avoided, while attempting to seduce someone contrary to their sexual orientation are deemed unacceptable.*

Second, only roll if the target doesn't want to be influenced.

<!-- Chronicles of Darkness - Social Maneuvering : (Notes/Research/Inspirations/Persuasion-Social Combat.md) + (Notes/Research/Inspirations/Action Lists-Saving Throws.md)
Exalted - Social Influence : (Notes/Research/Inspirations/Persuasion-Social Combat.md)
Roadwarden - Attitudes : (Notes/Research/Inspirations/Dialogue Trees.md) -->

<!-- My Method for Winning Over Conservatives : https://www.youtube.com/watch?v=PojRfPEH2mY -->

<!-- Read a more into Exalted :

"Social Complications":

- One Instigator vs. Many Targets

- Written Social Actions

- Gestures and Body Language

- Overturning Influence

- Retrying Social Actions

- Social Actions in Combat

-->




## Disposition


**Disposition** refers to the attitudes and feelings a target hold towards the character. It is represented by two values (*respect* and *sympathy*), which vary depending on the alignment, perception of actions and choices made by them through the story.

<!-- NEW TEST RULE USING CONTEXT MODIFIERS : Dispostion provides a Means Modifier to Social Actions -->

<!-- #### Disposition Scales -->

<!-- They go from -3 to +3 and provide a Minor, Significant or Decisive Means Modifier to a Social Action roll. -->

<!-- - Respect : Disregard, Neutral, Recognition/Consideration, Respect, Deference/Reverence -->

<!-- - Sympathy : Hate, Dislike, Reservation, Neutral, Affinity, Appreciation, Endearment/Affection/Love/Adoration -->

<!-- ### - Social Actions -->

<!-- #### Persuade/Deceive -->

<!-- - Easing Modifier if the target has positive Sympathy towards the character (this also applies if the character is attempting Therapy on the target; to relieve them from Despair). -->

<!-- - Trying Modifier if the target has negative Sympathy towards the character (this also applies if the character is attempting Therapy on the target; to relieve them from Despair). -->

<!-- - Easing Modifier if the character is trying to change the target's views or opinions, and they have positive Respect towards the character. -->

<!-- - Trying Modifier if the character is trying to change the target's views or opinions, and they have negative Respect towards the character. -->

<!-- - Deceive still decreases the target's Sympathy towards the character (once they realize they have been deceived). -->

<!-- Threaten : -->

<!-- - Easing Modifier if the target has positive Respect towards the character. -->

<!-- - Trying Modifier if the target has negative Respect towards the character. -->

<!-- - This action still decreases the target's Sympathy towards the character. -->

<!-- Bluff : works like Deceive but for threats; and uses Respect instead of Sympathy. It decreases the target's Respect towards the character (once they realize the threat was hollow). -->

<!-- Plead : this action still decreases the target's Respect towards the character. And maybe add a clause where the target won't be able to gain any Respect towards the character anymore, forever. -->


### - Respect

*Respect* reflects the perceived value or status the target holds over the character. It is based on their accomplishments, recognized skill or significance within the game world:

|Respect  |Description |
|:-------:|------------|
|Negative |Indicates complete indifference, where the character is seen as unimportant or even considered inferior. |
|Neutral  |Signifies a lack of strong feelings either way, usually with the target seeing the character as their equal. |
|Positive |Reflects recognition, where the character is seen as very important or even superior due to their achievements or perceived value. |

<!-- Include examples of when respect is raised and when respect is dropped. -->


### - Sympathy

*Sympathy* indicates the level of compassion or trust that the target extends towards the character. It signifies empathy and emotional connection, influenced by their behavior or choices within the game world:

|Sympathy |Description |
|:-------:|------------|
|Negative |Signifies a lack of trust or emotional connection, with the target harboring suspicion, skepticism or even hate towards the character. |
|Neutral  |Suggests a cautious or uncertain attitude, where the target may not fully trust the character, but also hasn't developed strong negative feelings towards them. |
|Positive |Denotes empathy and trust towards the character, showing compassion, affection or the will to foster a strong emotional bond with them. |

<!-- Include examples of when sympathy is raised and when sympathy is dropped. -->

<!-- "You begin cordially. Civility is not earned; it is lost." : https://www.reddit.com/r/Destiny/comments/1ggptci/forced_civility -->

<!-- Example idea : a character addresses their anger to get rid of a Breakdown state, and does something that pisses the rest of the group. Now all teammates have negative Sympathy towards the chracter. However, affected characters may still resist their Disposition change with a humility Resistance Roll (raising their anger and using a Pressure rating set by the GM). -->

<!-- Sympathy should go straight to -1 if the relationship performs a contentious check targeting the character (doesn't matter if they succeed or not?). -->

<!-- Sympathy should decrease by 1 when the relationship does something the character really disagrees with or finds reprehensible. -->

<!-- Sympathy should go straight to +1 if the relationship performs a successful cooperative check targeting the character (and the character was in need of help). -->

<!-- Sympathy should increase by 1 when the relationship does something the character really agrees with or finds commendable. -->


<!-- ## Bond score -->

<!-- - Increases : when the relationship succesfully performs a cooperative check targeting the character. -->

<!-- - Decreases* : when the character triggers a bond or when the relationship does something the character disagrees with or finds reprehensible. -->

<!-- When a character attempts a cooperative check aimed at a specific disposition, they can trigger a bond. To trigger a bond, the disposition's bond score must be strictly higher than its matching grudge score. Once triggered, a bond provides the player with an easing modifier equal to its score (for the current cooperative check), after which the bond score is decreased by 1. -->

<!-- When a character dies, inflict an amount of stress to other characters equal to the amount of bond they had with them. -->


<!-- ## Grudge score -->

<!-- - Increases : when the relationship performs a contentious check targeting the character (doesn't matter if they succeed or not?). -->

<!-- - Decreases : when the character triggers a grudge or when the relationship does something the character agrees with or finds commendable. -->

<!-- When a character attempts a contentious check aimed at a specific disposition, they can trigger a grudge. To trigger a grudge, the disposition's grudge score must be strictly higher than its matching bond score. Once triggered, a grudge provides the player with an easing modifier equal to its score (for the current contentious check), after which the grudge score is decreased by 1. -->


<!-- ALTERNATIVE RULE : reduce the amount of bond and grudge a character can accumulate up to 3 instead of 5. Now, consuming either of these resources doesn't provide an easing modifier, but instead works as if the character is performing a check "aligned" with one of its drives, thus giving them a chance to remove all of their stress if they succeed it. -->


#### Disposition Table

The GM may use this table to enhance their description of a character's feelings and opinions towards another. This serves no mechanical function, and is intended solely for narrative purposes:

|   Disposition | - Sympathy | Neutral      | + Sympathy |
| ------------: | ---------- | ------------ | ---------- |
| **- Respect** | Contempt   | Indifference | Pity       |
|   **Neutral** | Aversion   | Neutral      | Sympathy   |
| **+ Respect** | Fear       | Respect      | Admiration |

- **Admiration** : the target feels a deep sense of appreciation towards the character, often regarding them as exemplary due to their qualities, achievements or actions.

- **Pity** : the target experiences a feeling of sorrow towards the character, usually because they see them as good but disadvantaged.

- **Fear** : the target experiences a sense of dread or apprehension towards the character, seeing them as intimidating or threatening due to their perceived power and malevolence.

- **Contempt** : the target views the character with disdain due to their perceived unethical or weak behavior, often leading to dismissive or disrespectful treatment.

<!-- Disdain -->




## Social Actions


Once the player has stated their intent and is ready to roll, the GM chooses the social action that best fits the situation (*persuade*, *deceive*, *plead* or *threaten*).

>*Social Actions are a type of Action Roll that apply specifically to this system.  While they resolve in the same manner, they also factor in the target's Disposition toward the character both before and after the roll.*

Social actions also require the combination of  two abilities, with *eloquence* being the most commonly used. However, there can be exceptions where a different ability may come into play. For example, *agility* may be used for seducing with dance moves, or *strength* for impressing with physical prowess.


### - Persuade

*Persuading* is the most straightforward way of exerting influence, allowing characters to sway others through argumentation, logic or even emotions (depending on the situation and their approach).

>*This action shouldn't involve trickery, coertion or begging, and cannot be performed if the target harbors negative sympathy towards the character.*

Abilities used for *persuading* depend on the goal, context and methods used. For instance, convincing someone to lend a piece of equipment might require *eloquence* + *insight* (to tailor one's words accordingly), or *eloquence* + *humility* (if the character wants to demonstrate reliability and assure the safe return of the item). Similarly, *eloquence* + *memory* (or *reasoning*) could be used for swaying opinions in a debate or persuading NPCs to change their political views.

<!-- Provide example -->

<!-- Aristotle's Rhetoric (Modes of persuasion) : https://en.wikipedia.org/wiki/Modes_of_persuasion -->

<!-- Ethos (appeal to authority), Pathos (appeal to emotion), Logos (appeal to logic) -->

<!-- Persuation shouldn't work on a character suffering an anger breakdown -->


### - Deceive

*Deceiving* works like *persuading*, except the character is hiding their true intentions, and entails an outcome that hurts or doesn't benefit the target. This action should involve manipulation, trickery or cunning (including fabricating lies, using misleading information or exploiting vulnerabilities for personal gain).

>*This action, akin to persuasion, cannot be performed if the target harbors negative sympathy toward the character. And regardless of outcome, as soon as the victim realizes they have been (or are being) deceived, they should decrease their sympathy towards the character by one level.*

<!-- cannot deceive someone you have sympathy towards as well? -->

Abilities used for *deceiving* encompass *charm*, *creativity* and sometimes *dexterity*. For example, along with *eloquence*, weaving a convincing lie might require a combination of *charm* to disarm suspicion, *creativity* to fabricate a believable narrative, or *dexterity* to subtly conceal or manipulate objects. Additionally, *insight* could also be crucial for gauging the target's reactions and adjusting the deception accordingly.

<!-- Provide example -->


### - Plead

*Pleading* involves making a heartfelt appeal or request, often in a vulnerable or desperate manner, to elicit empathy or assistance from the target. This action relies on the character's ability to convey sincerity and express vulnerability to persuade others to act in their favor.

>*Once this action is performed, regardless of outcome, the target should decrease their respect towards the character by one level.*

Abilities used for *pleading* involve a combination of *eloquence* to articulate the plea effectively and *humility* to convey genuine need or desperation.

<!-- Alternatively, *insight* may be useful to gauge the target's receptivenes and perspective. -->

<!-- This action relies on the emotional vulnerability or goodwill of the target, emphasizing the urgency or importance of the plea to sway their decision. -->

<!-- Imploring entails earnestly and fervently pleading or begging someone to do something or to change their course of action. It often involves appealing to emotions, empathy, or shared values to elicit a favorable response. -->


### - Threaten

*Threatening* is the act of using intimidation or coercion to force a target to comply with the character's demands. This action can range from subtle implications of punishment to explicit threats of violence or harm.

>*This action cannot be performed if the target harbors negative respect toward the character. And once this action is performed, regardless of outcome, the target should decrease their sympathy towards the character by one level.*

This action relies on the character's capacity to instill fear, assert dominance, and communicate consequences effectively. Abilities used for *threatening* usually involve *eloquence* to articulate the threat convincingly and *strength* to convey physical prowess or readiness to enforce it.

<!-- MORE SOCIAL ACTIONS -->

<!-- Vent/Rant/Spill: forces another character to 'agree' with you, reducing your Emotional Strain or possibly removing some Despair. It can also be used to change the target's views and opinions, but only if they don't have negative respect towards you. -->

<!-- - Detect? Scout? Survey? Reveal? Uncover? : -->

<!-- From Exalted: The read intentions action allows a character to discern what another character wants to achieve in a scene of interaction. On a success, the Storyteller should give a brief description of what the character wants out of the interaction: “He’s seducing you to get you alone,” or “She’s making polite small talk and waiting for this party to end." -->

<!-- Alternatively, this action can be used in order to determine what Intimacies a character has. Before rolling for the action, the player should generally describe what kind of Intimacy he wants to discern (“Does he love anyone?” “How does he feel about me?” “Is he a devout Immaculate?”). On a success, the Storyteller chooses and reveals one of the target’s Intimacies that fits those criteria. If there is no Intimacy that does, the Storyteller should let the player know this. -->

<!-- The read intentions action is not a form of magic. The character using it is analyzing the target’s words and behavior to get a better feel for his motives and Intimacies, and the Storyteller should use common sense in deciding how much information can be gleaned from a character’s behavior and appearance. You might deduce that a young princeling is in love from a look of longing in his eyes or a wistful sigh, but discerning his paramour’s identity might be impossible unless she’s physically present or if he’s carrying some evidence of her identity. -->

<!-- Reading someone’s intentions is not an influence roll—instead, it is a (Perception + Socialize) roll against the target’s Guile. -->

<!-- - Read Intention : a character who is unaware he’s being observed suffers a -2 penalty to his Guile. -->

<!-- The Guile trait represents a character’s ability to conceal his thoughts and innermost feelings. A character with high Guile reveals little about himself through his expression, posture, and speech, while a character with low Guile wears his heart (and Intimacies) on his sleeve. Guile is typically used to defend against the read intentions action. -->

<!-- Eloquence should be used to resist a read intention action? Here's a good example of what I'm thinking about: https://www.youtube.com/watch?v=J4TtF4LffI4&t=1445s -->

<!-- Provoke : -->

<!-- Taunt : explain this in combat rules? -->

<!-- Raise a target's emotional strain (Taunt = Anger, Harass = Frustration, Ridicule = Insecurity, Frighten = Fear, Reprimand = Guilt). This action can only be performed if the target harbors positive respect toward the character. -->

<!-- From Exalted - Inspire : The inspire action is used to incite emotions and strong passions in the hearts of others, usually with the Performance Ability, which those others then act on. When a player takes an inspire action, he chooses which emotion he is attempting to inspire—anger, sorrow, hope, lust, hatred, delight, or any other. On a successful inspire roll, the target is impassioned according to the emotion evoked—but the target’s player chooses what form that passion takes. -->

<!-- Comfort : -->

<!-- Help a target recover from Stress. This action can only be performed if the target harbors positive sympathy toward the character. -->

#### Assessing Challenge

**Challenge** should be evaluated based on the target's acumen and general social skills. Are they perceptive enough to uncover the character's lies? Do they possess the ability to notice subtle hints in the character's behavior? This choice is at the GM's discretion, but for fully-fledged NPCs, a designated ability score could serve as the baseline for this value:

|Ability Score |Challenge Rating |
|:------------:|:---------------:|
|1-2           |2                |
|3-4           |3                |
|5             |4                |

>*To get an idea of which ability to pick, check the 'Resisting Influence' section below.*

<!-- Include the unused "positive sympathy" and "positive respect" here...? -->

#### Applying Modifiers

Depending on which social action is being performed by the character, **Modifiers** don't always have the same utility. Here are some examples:

- **Environment** reflects the suitability of the location in which the influence is being performed. For instance, a dark alley or any place that feels unsafe would yield a positive *environment* to a *threaten* roll, while it would impose a negative *environment* to a *persuade* roll. Similarly, being in a hospital or a church would benefit a *plead* roll, but hinder a *deceive* roll.

- **Means** represent leverage or bargaining power. When attempting to *persuade*, this means offering something in exchange for the request. This can include bribes, goods, services or information tailored to the target's needs, provided it's perceived as worth the sacrifice being asked. When attempting to *threaten*, this means leveraging fear and coercion. Instead of offering something desirable, the target is presented with something undesirable, like hurting someone or something they care about, social blackmail, financial ruin etc... This should work as long as the consequences of refusal are perceived as worse than complying with the request.

- **Condition** encompasses various factors such as fatigue, substance intoxication or general appearance. Typically, a positive *condition* should be granted if the character is well dressed, clean or presentable, while a negative *condition* should be applied if they're drunk, wounded, tired or dirty. However, in the case of a *plead* roll, being in a poor state (such as being injured or wounded) should result in a positive *condition*, as it may evoke sympathy or elicit a compassionate response from the target.

<!-- Anger Breakdown : roll with Advantage for Threaten rolls? -->

<!-- Frustration Breakdown : roll with Disadvantage for Persuade rolls? etc... -->




## Influence Clocks


Most times, social influence involves applying persuasion over time, whether through direct demands or subtle suggestions. This is represented with **Influence Clocks** (a type of **Progress Clock**), where each segment ticked gradually molds the target's mindset or behavior, eventually leading to compliance with the desired outcome:

|Demand   |Clock Sections |Description |
|:-------:|:-------------:|:-----------|
|Average  |2              |Demands involving mild risks or inconveniences, potentially impacting personal time, resources, or commitments, without posing existential threats or profound changes to the target's life. <!-- or demands aligned with a target's Drives or Quest --> |
|Major    |4              |Demands that entail significant risk of harm or impediment, or some form of strong commitment, such as significant financial contributions, personal sacrifices or long-term undertakings. |
|Integral |8              |Requests entailing profound life-altering sacrifices, often involving matters of utmost gravity, such as relinquishing a core belief or religion, reshaping one's lifestyle and priorities or giving up essential resources or large portions of wealth. <!-- or demands that go against a target's Drives or Quest --> |

**Influence Clocks** also feature intervals between rolls, representing the time required for each subsequent action:

|Demand   |Time Per Roll |
|:-------:|:------------:|
|Average  |One Hour      |
|Major    |One Day       |
|Pivotal  |One Week      |

When gauging the size of an **Influence Clock**, the target's **Drives** and **Quest** should be taken into consideration. For instance, always ask: "how likely is the character to accept the request?" The less likely, the larger the clock.

>*Context should not be forgotten. Requesting a small sum from a beggar may carry significant weight, considering it could be crucial for their survival. Conversely, a general asking a soldier to risk their life in battle may carry less weight, given the inherent agreement to such risks upon joining the army.*

<!--  From Chronicles of Darkness : Once your character opens the final Door, the subject must act. Storyteller characters abide by the intended goal, and follow through as stated. If you allow players’ characters to be the targets of social maneuvering, resolve this stage as a negotiation with two possible outcomes. The subject chooses to abide by the desired goal, or offer a beneficial alternative. -->

<!-- #### Examples -->

<!-- - **Minor Demand** : *“I need you to deliver this parcel to that big house in Cinnabar District, with the red jade lion statues by the door. If the man on the door has a scorpion tattoo, don’t leave it with him —insist to see the master of the house.”*

- **Major Demand** : *“Just because he’s your father doesn’t make you his slave —why should his fear deny you a place in AnTeng’s glorious uprising against its oppressors? This nation needs heroes; men like you!”*

- **Pivotal Demand** : *“I know the old scrolls said the heart of this temple is guarded by a fearsome beast of brass and flame. I know it’s frightening, but isn’t this why we came so far and spent our fortunes, to be the first ones to scavenge the Great Ziggurat of Lost Zarlath? I’ll never make it into the final chambers with my leg like this—you’ll have to dare it for both of us!”* -->

<!-- These have been copy-pasted from Exalted. -->

<!-- Change/Give up Drive/Quest : -->

<!-- Make a character just give up their Quest? (without trying to replace it). This would make them enter a specific Breakdown state where they are depressed or aimless. -->

<!-- From Exalted: The instill action is used to change the feelings and beliefs of others. When a player takes an instill action, he declares what he wants to make his target feel or believe. The Storyteller may apply penalties to the roll if this belief is particularly implausible or hard to accept, up to a maximum penalty of -5 for truly unbelievable claims. On a successful roll, the target forms an Intimacy towards that belief. However, there are limits to what someone will believe when they already have strong opinions to the contrary. -->

<!-- This is basically a "Persuade" + "Pivotal Influence Clock" -->

<!-- Deceive : Once an Influence Clock is beaten, and the trick action was used at least once, the victim should recognize they've been duped in some way or another and gain negative sympathy towards the character. -->




## Resisting Influence


Characters can also be the targets of influence rolls made by others, and it's up to the player to decide whom to trust and whom to reject. To resist an influence attempt, the charater performs a **Resistance Roll** using an ability chosen by the GM that best suits the situation:

- **Composure** is used to resist impressions or emotional manipulation, like intimidation, seduction or temptation.

- **Insight** is used to call bluffs, detect lies or assess leverage.

- **Reasoning** or **Memory** : are used to identify inaccurate information, inconsistencies or pseudoscientific babble.

<!-- Humility -->

>*The Pressure rating for the roll is also left at the GM's discretion, and should be roughly based on the NPC's persuasive or social skills.*

<!-- ## Example -->

<!-- From Chronicles of Darkness: Stacy wants Professor Erikson to loan her a book from his private library. She intends to use the book’s contents to summon a demon, but Erickson doesn’t know that. Erickson is protective of his books, but he’d be willing to loan one out under the right circumstances. Erickson has Resolve 3 and Composure 4, so the base number of Doors Stacy needs to open is 3 (the lower of the two). Loaning out of a book wouldn’t be a breaking point, nor does it prevent him from achieving an Aspiration, but it does work against his Virtue (Cautious), so the total number of Doors Stacy needs to open to get the book is 4. -->

<!-- The Storyteller decides that the first impression is average; the two know one another (Stacy is a former student of Erickson’s), but they aren’t close. Stacy arranges to find Erickson at a conference and impresses him with her knowledge of esoteric funerary rites. This requires an Intelligence + Occult roll, plus whatever effort Stacy had to put forth to get into the conference, but changes the impression level to “good.” Now, Stacy can make one attempt to open Doors per day. At the conference, Stacy’s player rolls Manipulation + Persuasion and succeeds; one Door opens. Stacy mentions the book to Erickson and lets him know she’d like to borrow it. He’s not immediately receptive to the idea, but Stacy’s in a good place to continue. -->

<!-- The next day, Stacy emails the professor about a related work (Manipulation + Academics), but fails. Future rolls will have a -1 penalty. The Storyteller decides that the impression level slips to average. -->

<!-- Stacy still has to overcome three Doors. She spends the next week doing research into Erickson and discovers that he wants to become a respected academic. She tells Erickson that she has a colleague who can help break the cipher in which the book is written. This removes one Door without a roll. Now she must overcome two more before he’ll agree. (Note that even if Stacy has no intention of helping Erickson in his quest toward academic glory, as long as he reasonably believes that lending her the book will help him achieve his Aspiration, it opens the Door.) -->

<!-- During her research into the professor’s personality, she also learns that his Vice is Vanity; he likes to see himself as the hero. Stacy goes to his office in tears, saying that she is in danger of being accused of plagiarism for copying a paper, and asks if he can help authenticate her work. Doing this allows him to come to her rescue, which in turn lets him soak up some praise; this would allow him to regain Willpower through his Vice, and as such is enough of a temptation to raise the impression level back to good. Stacy’s player rolls Manipulation + Expression for Stacy to compose a letter of thanks to him, and achieves an exceptional success. The last two Doors open, and Erickson offers to let Stacy borrow the book for a weekend. He probably even thinks it was his idea. -->

<!-- On the other hand, if Erickson is a player-controlled character, his player might decide he really doesn’t want to let that book out of his sight. He might offer an alternative — he’ll bring the book to Stacy, and let her use it for an afternoon. That, of course, might complicate her intended demon summoning, but she does get to put the Flattered Condition on Erickson. -->

<!-- - With Hard Leverage : In the example above, assume Stacy really needs that book now. She goes to Erickson and threatens him at gunpoint to give up the book. Doing this is definitely a breaking point for Stacy. She applies a modifier for her Integrity, and then a modifier based on the severity of the action and the harm it does to her self-image and psyche. She’s not in the habit of committing violent acts and Erickson is obviously terrified, so the Storyteller assigns a -2 modifier to the breaking point roll. This being the case, one Door is removed. If she’d shot him the leg to let him know she was serious, the breaking point modifier would have been at least -3, which would have removed two Doors. In either case, her player rolls Presence + Intimidation plus any bonus for the gun, minus the appropriate penalty. -->

***
