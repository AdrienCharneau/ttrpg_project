The Storypath System: This system uses character aspects, which are phrases that describe a character's abilities, personality traits, and background. These can be invoked for bonuses or complications on rolls, as well as for other benefits and drawbacks.

The Storypath System is a role-playing game system developed by Onyx Path Publishing. One of its main features is its GM toolkit, which provides a set of guidelines and tools for game masters (GMs) to create and run adventures and campaigns.

Some examples of the elements included in the GM toolkit are:

Adventure Creation: The system includes a set of guidelines for creating adventures and campaigns, including tips for crafting compelling plots, building interesting NPCs, and creating memorable locations.

NPC creation: It provides a set of tools for creating and managing non-player characters (NPCs) in the game, including guidelines for determining their motivations, goals, and actions, as well as mechanics for tracking their relationships with the players' characters.

Encounter building: The system includes guidelines for building encounters and scenes, including tips for creating interesting and challenging encounters, and guidelines for adjusting the difficulty of encounters based on the players' characters' abilities and the players' preferences.

Worldbuilding: It provides tools and guidelines for building and fleshing out the game world, including advice on creating a consistent and believable setting, and guidelines for creating and managing factions, organizations, and other groups within the world.

Improvisation: It offers tools and techniques for improvisation and running the game on the fly, including advice for handling unexpected events and player choices, and tips for creating interesting and engaging encounters and scenes quickly.

Overall, the Storypath System's GM toolkit is designed to provide a set of resources and guidelines that help GMs create engaging and immersive adventures and campaigns, while also providing them with the flexibility to adapt and adjust the game to fit the players' preferences and playstyle.