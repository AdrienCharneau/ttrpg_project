# GENERAL-OTHER




## Smuggling


Smuggling is big business in both Elsweyr and Morrowind. The Empire forbids trade in Moon Sugar, Skooma, Ebony, slaves, and Dwemer artifacts, weapons, and armor. Smugglers also deal in greef, shein and sujamma to avoid high Imperial taxes on liquor. Main smuggling areas are the Bitter Coast controlled by the Camonna Tong and the port of Senchal by Khajiit pirates respectively. Smugglers are hunted by Census and Excise agents. Punishment for smuggling is often severe and anyone is free to kill smugglers on sight.

```no source```

```vibe : https://en.uesp.net/wiki/Morrowind:Smuggler%27s_Island```

***








# THE BITTER COAST




## General-Presentation


>The Bitter Coast is named for the salt marshes along the coast northwest of Seyda Neen. From here to the Odai River, it's rugged coastal hills, then it's roadless, uninhabited swamp all the way north to the West Gash and the Sea of Ghosts. Hla Oad is a fishing settlement north of the Odai, and the fishing village of Gnaar Mok is even farther, and both places are small, isolated, and poor. Hunting is fair, and some folks gather mushrooms, pods, and flowers for alchemists.

The Bitter Coast is dominated by humid, putrid swamps and salt marshes. The giant bull netch floats gently through the air, fearing no cliff racer or mudcrab, and hunting is considered fair.

Off the coast, divers find kollops with pearls, but even more slaughterfish and man-like dreugh with their hard shells and tentacles. Herbalists come to this region to harvest the various mushrooms, pods, and flowers which thrived in the marshy environment.

```source : https://en.uesp.net/wiki/Morrowind:Fargoth#Dialogue```

>The western coast of Vvardenfell from Seyda Neen north to Gnaar Mok is called the Bitter Coast. The salt marshes and humid swamps of this region are uninhabited, with the only settlements found at the good harbors of Gnaar Mok, Hla Oad, and Seyda Neen. Also called 'the Smuggler's Coast', the region's secluded coves and islands provide refuge for criminal trade, and the frequent rain and fog hides small boats from Excise cutters.

>The salt marshes and hot swamps of Bitter Coast are uninhabited, with the only settlements found at the good harbors of Gnaar Mok, Hla Oad, and Seyda Neen.

>The Bitter Coast is named for the salt marshes and swamps along the southwest coast. The water is bitter, and so is life, generally. It's mostly uninhabited wilderness from Seyda Neen in the south all the way north to the West Gash and the Sea of Ghosts. Hla Oad is a fishing settlement on the mainland near the mouth of the Odai River; there's a fair track along the river inland to Balmora. Gnaar Mok is a fishing village on an island farther north, but both places are small, isolated, and poor.

```source : https://en.uesp.net/wiki/Morrowind:Generic_Dialogue_B```

The region is also known as The Smuggler's Coast, since the secluded coves and islands are littered with smugglers operating out of their hidden caves. The ever-present fog and rain provides additional cover for their boats. No major action had been taken to curtail their actions, as many smugglers count on support from locals unhappy with high taxes on liquor.

The northern boundary of the region is generally considered to be marked by Khartag Point just north of Gnaar Mok. The Firemoth region is sometimes considered part of the Bitter Coast.

```no source```




## The Inner Sea

The Inner Sea (also called the Inland Sea(1), the Narrow Sea,(2) and the Vvardenfell Rift(3)) is a body of water that separates the mainland of the Imperial province of Morrowind from the large island of Vvardenfell. It stretches from the Bitter Coast and Ascadian Isles regions in western Vvardenfell all the way to Azura's Coast in the east. It meets the Sea of Ghosts twice: the two meld together near Azura's Coast, and again near the large and icy island of Solstheim northwest of Vvardenfell.

```Source (1) : https://en.uesp.net/wiki/Morrowind:Elone#Dialogue```

```Source (2) : https://en.uesp.net/wiki/Lore:A_Feast_Among_the_Dead,_Chapter_I```

```Source (3) : https://en.uesp.net/wiki/Lore:Ruins_of_Kemel-Ze```

There are conflicting reports on the origins of the Inner Sea. *{Some sources accredit an eruption of Red Mountain to its creation (1) ???}*, whereas Vivec's Sermons point to the usage of the Numidium circa 1E 700 (2). *{However, some sources attest that the Inner Sea was present before both the first major eruption of Red Mountain and the activation of the Numidium (3) ???}*. Furthermore, Topal the Pilot left behind maps of the Inner Sea dating to the Middle Merethic Era, far before the first Dwemer colonies (4-5).

```Source (1) : https://en.uesp.net/wiki/Lore:Ruins_of_Kemel-Ze```

```Source (2) : https://en.uesp.net/wiki/Lore:36_Lessons_of_Vivec,_Sermon_36```

```Source (3) : https://en.uesp.net/wiki/Lore:Pocket_Guide_to_the_Empire,_1st_Edition/Morrowind```

```Source (4) : https://en.uesp.net/wiki/Lore:Father_Of_The_Niben```

```Source (5) : https://en.uesp.net/wiki/Lore:Before_the_Ages_of_Man - mentions of "Vvardenfell Island" and "coasts of Vvardenfell"```

The coast of Vvardenfell is rugged and treacherous, with shipwrecks scattered among the menhirs that protrude from the water. It has been a major center of smuggling operations by the Camonna Tong and other groups. The main port of the Inner Sea on Vvardenfell is Ebonheart, with minor ports being Hla Oad, Gnaar Mok, Molag Mar, Tel Branora and the Imperial outpost of Seyda Neen with its lighthouse Grand Pharos guiding mariners.

```no source```




## Flora


The Bitter Coast is a swamp, plain and simple. Great, ragged trees provide a canopy beneath which pools of stagnant water grow a steady incursion of green-brown moss. Local flora is replete with fungi including bungler's bane, hypha facia and violet coprinus, and the stagnant pools can reward the strong-stomached with glowing ampoule pods.

```no source```


### - Bungler's Bane

Bungler's Bane is a shelf-fungus found growing on trees (and occasionally cave walls) throughout the Bitter Coast region. It is often confused for Hypha Facia, which is similar in appearance and often grows in the same places. Hypha Facia is the slightly more useful of the two, though neither is particularly valuable for alchemical purposes.

```no source```

>Bungler's Bane is a mottled brown-and-orange shelf fungus collected from the trunks of trees in the Bitter Coast region.

```source : https://en.uesp.net/wiki/Morrowind:Generic_Dialogue_B```

[*Effects : Drain Speed, Drain Endurance, Dispel Dispel, Drain Strength*]


### - Draggle Tail

The primitive Draggle Tail reeds are found in the swamps of the Bitter Coast region of Vvardenfell. The immature state of the plant's fruiting body are called ampoule pods, which bloom into the luminescent coda flower. Draggle Tails are prized by alchemists for their modest magical properties.

```no source```

- **Ampoule Pod** : Ampoule Pods are harvested from draggle-tail plants (the smaller kind). These are very common in the Bitter Coast region.

>"The immature state of the fruiting body of the primitive draggle-tail plant of the Bitter Coast is called the ampoule pod. This pod has modest magical properties prized by alchemists."

```source : https://en.uesp.net/wiki/Morrowind:Generic_Dialogue_A```

[*Effects : Water Walking, Paralyze, Detect Animal, Drain Willpower*]

- **Coda Flower** : Coda Flowers grow in Draggle-Tails (the larger ones) found in shallow swamp-water, most common in the Bitter Coast region. They are also found in indoor planters in various locations. Coda Flowers have an interesting property in that they glow in the dark. The plants actually emit light that illuminates things around them. Picked flowers do not emit light, but they can still be easily seen in the dark, and make good way-markers if you wish to use them as such.

```no source```

[*Effects : Drain Personality, Levitate, Drain Intelligence, Drain Health*]


### - Hypha Facia

Hypha facia is a light brown shelf fungus collected from the trunks of trees (and occasionally cave walls) in the Bitter Coast Region. While similar in appearance to Bungler's Bane, Hypha Facia grows much lower down on trees and may sometimes be found just inches above the ground.

```no source```

[*Effects : Drain Luck, Drain Agility, Drain Fatigue, Detect Enchantment*]




## Fauna


Local fauna is mostly predatory, but not all that dangerous to the traveler. The few truly dangerous creatures on the Bitter Coast are kwama foragers and nix-hounds, the latter of which are delicious and useful for making potions. The unwary traveler venturing into the open waters off the coast is in another league, however. The eternally persistent slaughterfish is thought by some to be a distant cousin to the cliff racer, as their temperament is very similar.

```no source```


### - Dragonfly

Dragonflies, sometimes called Dovah-Flies, are small flying insects commonly found in southern Skyrim and around bodies of water in the Azura's Coast, Bitter Coast, and West Gash regions of Vvardenfell. A number of varieties exist, such as the Blue and Orange Dartwings, but it is unknown whether the fly variants are different breeds or merely color variations.

```source : https://en.uesp.net/wiki/Lore:Herbalist%27s_Guide_to_Skyrim```

Dartwings are prized for their use in alchemy.

```no source```




## Swamp Fever


Swamp fever is a mild common disease affecting the victim's strength and behavior. It may be contracted from mudcrabs. The symptoms include high body temperature and delirium, but there are no easily visible signs. The Bitter Coast region of Vvardenfell is infamous for the abundance of Swamp Fever cases.

```source : https://en.uesp.net/wiki/Lore:Disease```

***








# SEYDA NEEN AREA

***








# HLA OAD AREA




## Odai River


The Odai River is a river that flows through Balmora. It lies in the southernmost reaches of the West Gash region of Vvardenfell. Originating just north of Balmora, it runs south to the Odai Plateau, and empties into the Inner Sea at the eastern edge of the Bitter Coast region, just south-southeast of Hla Oad. The terrain surrounding the Odai River is mountainous in nature, and dotted with mines and plants.

```no source```


### - Odai track

>A rough track along the western shore of the river connects Hla Oad with Balmora.

```source : https://en.uesp.net/wiki/Lore:Guide_to_Vvardenfell```

```source : https://en.uesp.net/wiki/Lore:Guide_to_Balmora```

>Hla Oad is a fishing settlement on the mainland near the mouth of the Odai River; there's a fair track along the river inland to Balmora.

```source : https://en.uesp.net/wiki/Morrowind:Generic_Dialogue_B```

***








# FIREMOTH




Firemoth (also known as Firemoth Island or the Firemoth Region) is a region encompassing a ring of barren islands located in the Inner Sea.

```source : in-game content in Morrowind```




## Environment


The islands often suffer heavy storms, and the bare volcanic rock provides little nourishment for flora.

```source : in Morrowind, the archipelago and fort are technically in their own region named "Firemoth Region" rather than in the Bitter Coast. The weather in the region is set to be thunder 100% of the time.```

A fair number of hardy trama shrubs and stark, leafless trees can survive the ashen, bitter soil. Marshmerrow reeds flourish along the sandy beaches, but otherwise the islands are devoid of life.

```source : in-game content in Morrowind```




## Fort Firemoth


By the Third Era, the Imperial Legion built a Fort made of stone, housed on the main island. The fort and its defenses are the only structures on the island.

```source : in-game content in Morrowind```

Once a strategic point guarding the Inner Sea from smugglers, it was built on top of a rich ebony mine, which broke through into flooded natural caverns and an ancient Velothi tomb.

```source : in-game, there is some evidence of abandonned mining operations ( https://youtu.be/LFEG_MnF1g8?t=936 ) - And the bottom area of the mine (where the entrance for the Ancestral Tomb is located) is clearly flooded ( https://youtu.be/LFEG_MnF1g8?t=1027 )```

The fort was overrun by an undead army led by a lich named Grurn who captured the garrison, leaving few survivors. While technically still Imperial territory, the islands remained occupied by skeletons and were not reclaimed.

The fort itself was ransacked, but an array of Imperial equipment and a few valuable items remained. The lower levels (the Mine, Lower Caverns and Upper Caverns) have veins of raw ebony and a few loose samples.

A door from the Lower Caverns to the outside offers a more direct way of extracting raw ebony from the lower sections of the fort.

There is a hidden door within the Guard Quarters of the Fort found along the right wall. Searching beyond this door reveals various loot including a large amount of gold found on a table and in several sacks.

```source : in-game content in Morrowind```




## Characters


### - Grurn

Grurn is a lich leading the skeleton army currently occupying Fort Firemoth.

>This Imperial fort offshore to the west was captured years ago by the undead horde of Grurn, who literally marched out of the ocean and overran the garrison there. The survivors were forced to leave behind the Ward of Akavir.

```source : https://en.uesp.net/wiki/Morrowind:Sellus_Gravius#Dialogue```




## Items


### - Ward of Akavir

The Ward of Akavir is a priceless artifact treasured by the Imperial Legion. It is a tower shield with an Imperial Dragonscale design, made from the actual skin of a dragon. It bears a powerful enchantment which makes the wielder far luckier. Its origins are unknown, but its name implies that the artifact is related to the continent of Akavir in some way.

```no source```

***