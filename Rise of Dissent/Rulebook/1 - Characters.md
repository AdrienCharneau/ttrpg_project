```
   __|  |  |    \    _ \    \     __| __ __| __|  _ \   __|
  (     __ |   _ \     /   _ \   (       |   _|     / \__ \
 \___| _| _| _/  _\ _|_\ _/  _\ \___|   _|  ___| _|_\ ____/
```

***








# **Archetypes**

***




**Archetypes** are the central aspects of a character, setting their general tone or high concept. When creating a new character, players must choose 2 **Archetypes** out of the 4 available:

- **Champions** thrive for power and specialize in physical abilities.

- **Icons** thrive for fame and specialize in social abilities.

- **Geniuses** thrive for knowledge and specialize in intellectual abilities.

- **Sages** thrive for wisdom and specialize in wise abilities.

**Champions** are natural-born achievers and extremely skilled fighters. They have a firm predisposition for combat, thrive in confrontations and inspire others to respect them. With their unwavering focus and determination, they thrive when forging a path for themselves or when ensuring the protection and prosperity of their group.

>*Champions have great ambition, a strong sense of pride and work tirelessly to defeat their enemies and achieve their goals. Players that choose this Archetype may want their characters to specialize in military expertise or some type of weapon proficiency.*

**Icons** are driven by the desire for social recognition and fame. They excel in fields such as arts, entertainment or politics, and use their skills to inspire, amuse or influence others. Their charismatic presence allows them to effortlessly capture the public's attention, potentially leading them to become influential figures in society.

>*Icons are natural-born performers, captivating people and leaving long lasting impressions wherever they go. Players that choose this Archetype may want their characters to specialize in public speaking, some type of artistic endeavor or being able to entertain people.*

**Geniuses** are educated individuals committed to mastering specific subjects. They can be practitioners, technicians, researchers or scholars, carrying expertise across various disciplines including biology, literature or archaeology. They possess a relentless thirst for knowledge and excel in problem-solving, innovation and creative thinking.

>*Geniuses may be driven by the desire to break established rules, solve problems or understand abstract concepts. Players that choose this Archetype may want their characters to specialize in magic or gain a very strong insight into a specific discipline.*

**Sages** are wise and composed individuals driven by self-discipline and a desire to help, educate or mentor others. They keep their head up in times of uncertainty, using their discernment to navigate hardships and find ways to progress in life without succumbing to despair. Their reflections and understanding of philosophy may also allow them to create new schools of thought.

>*Sages make perfect healers, monks, teachers or druids. Players that choose this Archetype may want their characters to specialize in some form of caretaking discipline or follow a spiritual path.*




## Combining Archetypes


When choosing 2 **Archetypes**, players must consider their character's general goals and ambitions in order to pick the right combination. This can be illustrated with the lives of two historical figures: *Julius Caesar* and *Marcus Aurelius*. Despite their shared influence as Roman emperors, both possessed distinct character traits, leadership styles and historical roles that reflect the approach of **Archetype** combination:

>*Julius Caesar, a charismatic and ambitious leader, exemplified the combination of champion and icon. His rise to power, from a successful general to dictator, showcased his desire for power, influence and popularity among people. He strategically used his oratory skills and military expertise to establish himself as the most prominent figure in ancient Rome.*

>*In contrast, Marcus Aurelius inherited his role of emperor. As a military leader, his purpose centered around upholding justice, fairness, and ethical values, thus embodying the combination of champion and sage. His renowned philosophical writings, such as the collection Meditations, reflected his commitment to stoic principles and the pursuit of wisdom.*

Although both characters can be classified as *champions*, their overall high-concept diverges based on their second **Archetype**. For instance, if a character has the combination of *champion* and *icon*, they might aspire to become a powerful combatant with a desire for fame and glory. Whereas a character that combines *champion* and *sage* may thrive to become a wandering knight guided by principles of wisdom and virtue.


### - Examples

Here's a list of combinations that can be used for new characters. Keep in mind that these are just examples, and players encouraged to come up with their own ideas:

- **Arcanic Scholar** (*Genius* & *Icon*) : this character aspires to become a well-known polymath, mastering multiple disciplines of magic and leaving behind a lasting legacy of discoveries and breakthroughs.

- **Ascetic Healer** (*Sage* & *Genius*) : this character aspires to become a reclusive yet compassionate caregiver with extensive medical knowledge, setting them apart as a trusted authority in the medical field.

- **Itinerant Bard** (*Icon* & *Sage*) : this character aspires to become a charismatic performer who uses their wit, charm, and musical talents to entertain and inspire others, encouraging social change and fostering peace.

- **Notorious Rogue** (*Icon* & *Champion*) : this character aspires to become a master of crime and legendary figure from the underworld, able to outmaneuver authorities and strike fear into the hearts of the rich.

- **War Artifist** (*Genius* & *Champion*) : this character aspires to become a skilled artisan of war machines, blending arcanic knowledge with combat expertise, and harnessing their intellect to shape the course of battles.

- **Warrior Monk** (*Champion* & *Sage*) : this character aspires to become a honorable warrior following the codes of a martial doctrine, roaming the land mastering both physical combat and practicing self-discipline.




## Drives


**Drives** represent a character's motivations, guiding their actions throughout the narrative. Each character must choose 2 **Drives**, with at least 1 **Drive** matching one of their **Archetypes**:

#### Champion

- **Authority** : the character wants to assert dominance and issue commands.

- **Revenge** : the character seeks justice or retribution.

- **Wealth** : the character is driven to amass riches and possessions.

#### Icon

- **Hedonism** : the character prioritizes indulgence and entertainment.

- **Distinction** : the character seeks to impress others and bring attention to themselves.

- **Influence** : the character aims to sway people to their side and mold their opinions.

<!-- - **Privilege** : the character seeks special attention, entitlement or validation. -->

#### Genius

- **Solution** : the character is driven by problem-solving.

- **Production** : the character wants to build or conceive things.

- **Curiosity** : the character is motivated by a thirst for knowledge.

#### Sage

- **Care** : the character wants to help and protect others.

- **Peace** : the character seeks to foster harmony and mend relationships.

- **Duty** : the character is dedicated to fulfilling their obligations.

<!-- By aligning their conduct with these motivations, characters earn XP and gain Advantage when performing actions. -->




## Quest


Each character also has a specific **Quest** that should be a reflection of their **Drives**. This is written as a concise statement, typically one or two sentences, describing a unique objective or aim for the character:

- **Wealth** & **Duty** : *"I want to travel to the mines of Eldoria to negotiate a trade agreement for my family's gemstone business."*

- **Distinction** & **Solution** : *I want to decipher the enigmatic Sandravi script to earn acclaim and notoriety in academic circles."*

- **Authority** & **Care** : *"I want to administer the city of Lüdim, where hunger and repression have plagued the inhabitants for months."*

- **Hedonism** & **Production** : *"I want to experience the serene sunset of Ignicor and compose a poem about it."*

<!-- Wealth & Duty : "I want to accumulate riches to protect my family from a powerful threat." -->

<!-- Distinction & Solution : I want to find a way to solve a scientific conundrum and gain recognition for my work". -->

<!-- Authority & Care : I want to lead by example, protecting vulnerable people and steering them to a better future. -->

<!-- Hedonism & Production : I want to create an enjoyable and innovative art piece, while indulging in life's pleasures. -->

>*Quests should be seen as active goals, involving things a character strives to accomplish rather than things they wish to avoid.*

<!-- When fulfilling (or at least significantly pursuing) their **Quest**, a character clears all of their their **Emotional** strain and earns large amounts of **XP**. -->

<!-- https://www.youtube.com/watch?v=E79DDGdX62I&t=1676s -->

<!-- https://www.reddit.com/r/WhiteWolfRPG/comments/2sz9se/personal_aspirations_supernatural_aspirations/ -->

<!-- HAVE PLAYERS WRITE BELIEFS LIKE IN THE BURNING WHEEL AND MOUSEGUARD https://gamersplane.com/forums/thread/2232/ -->

<!-- Invisible Sun - The Key p.166 -->




<!-- ## Flaws


**Flaws** are negative traits that mirror a character's **Drives**. They are used by the GM to force characters into taking a specific course of action or making unexpected choices. Each **Flaw** is associated with 2 **Drives**, and players must pick 1 **Flaw** for each one of their **Drives** (2 **Flaws** in total):

- **Harsh** (*Authority*, *Revenge*) : the character is excessively severe or critical in their words and actions. Their uncompromising demeanor may create undesired rifts with NPCs or allies.

- **Reckless** (*Revenge*, *Wealth*) : the character lacks tolerance for delays, often acting impulsively without considering consequences, potentially endangering themselves and those around them.

- **Greedy** (*Wealth*, *Hedonism*) : the character is consumed by an insatiable desire for material possessions, often prioritizing personal gain over the well-being of the group.

- **Vain** (*Hedonism*, *Distinction*) : the character is excessively superficial and concerned with their ego, making them easier to be manipulated through flattery or gratification.

- **Paranoiac** (*Distinction*, *Influence*) : the character is plagued by irrational fears and suspicions, often interpreting innocent actions as signs of betrayal or danger.

- **Lazy** (*Influence*, *Solution*) : the character avoids putting in effort, preferring to coast by on minimum exertion and relying on others to solve problems or fulfill obligations.

- **Obsessive** (*Solution*, *Production*) : the character may become fixated on particular tasks, ideas, or goals to the point of detriment, neglecting relationships and other aspects of life.

- **Weird** (*Production*, *Curiosity*) : the character exhibits eccentric or unconventional behavior, unsettling others and making them feel uncomfortable in social situations.

- **Intrusive** (*Curiosity*, *Care*) : the character lacks boundaries. They frequently insert themselves where they are not welcome, prying into others' affairs and invading their privacy.

- **Overinvested** (*Care*, *Peace*) : the character is too affected by the problems and tragedies of others, often to the point of being more concerned with their well-being than their own.

- **Submissive** (*Peace*, *Duty*) : the character consistently defers to others' authority or wishes, leading to exploitation by them or inhibiting the group's ability to assert themselves.

- **Dogmatic** (*Duty*, *Authority*) : the character rigidly adheres to a set of beliefs or principles, refusing to adapt or consider alternative perspectives. -->

<!-- They may also want to impose their ideology onto others. -->

<!-- **Impatient** -->

<!-- **Bitter** (*Revenge*, *Wealth*) -->

<!-- **Superficial** -->

<!-- **Jealous** -->

<!-- - **Arrogant** (*Distinction*, *Influence*) : the character believes themselves to be superior to others. They might underestimate opponents, dismiss valuable advice or refuse to perform tasks they deem unworthy. They may reveal important information by bragging too much. -->

<!-- **Overambitious** -->

<!-- **Anxious** -->

<!-- **Eccentric** -->

<!-- **Smothering** -->

<!-- **Overprotective** -->

<!-- **Naive** (*Care*, *Peace*) -->

<!-- **Overzealous** -->

<!-- **Fanatic** -->

<!-- **Intolerant** -->


<!-- - Certain (all?) **Flaws** should have ability score requirements (for example, a character shouldn't have *arrogant* if they have a *humility* score greater than 2 or something).

- Players should feel free to come up with their own **Flaws**? (as long as these match their chosen **Drives**). -->

<!-- *** -->

***








# **Abilities**

***




Characters have 12 **Abilities** that can be grouped into 4 different domains (*Physical*, *Social*, *Intellectual* & *Wise*):

|Ability    |Domain              |Usage |
|:----------|:-------------------|:-----|
|Strength   |Physical            |*Close combat, breaking things, moving or pushing heavy loads...* |
|Dexterity  |Physical            |*Reflexes, detection, perception, accuracy...* |
|Agility    |Physical & Social   |*Speed, athletics, acrobatics, proficiency in sports...* |
|Charm      |Social              |*Seduction, entertainment, persuading through looks, style or demeanor...* |
|Eloquence  |Social              |*Public speaking, bargaining, persuading through oratory skills...* |
|Creativity |Social & Intellectual |*Thinking outside the box, come up with lies, find solutions to problems or puzzles...* |
|Knowledge  |Intellectual        |*Analyzing, understanding or recalling information, identifying something, learning spells...* |
|Reasoning  |Intellectual        |*Casting spells, computing, calculating, crafting, building or repairing...* |
|Insight    |Intellectual & Wise |*Bargaining, reading a situation or person...* |
|Humility   |Wise                |*Helping, healing, understanding or tolerating others...* |
|Composure  |Wise                |*Maintaining emotional stability, staying calm under pressure, handling stressful situations...* |
|Endurance  |Wise & Physical     |*Stamina, fatigue, resisting physical or mental harm...* |

>*Abilities may be used actively as characters strive to accomplish tasks, or reactively to counteract enemy actions or evade environmental dangers.*

**Physical Abilities** relate to a character's bodily aptitude and physical prowess, encompassing their *strength*, *dexterity*, *agility* and *endurance*. These attributes not only determine their combat effectiveness but also play a vital role in athletic challenges:

- **Strength** represents a character's muscle mass, their capacity to lift, carry or move heavy loads, as well as their effectiveness in melee combat. It is used for close-range attacks, breaking things or lifting heavy weights. It is associated with imposing characters, and can play a role in their aptitude to intimidate others. It is the primary attribute for bodyguards, mercenaries and warriors.

- **Dexterity** symbolizes a character's quick reflexes and precision, measuring their capacity to perform tasks with sleight, hit accurately in combat, detect things, perform quick movements or manipulate objects and devices. It also determines reaction time against surprise attacks or sudden events. It is the primary attribute for sharpshooters, assassins and thieves.

**Social Abilities** relate to a character's social skills and capacity to interact with others, encompassing their *charm*, *eloquence*, *agility* and *creativity*. These attributes are used for navigating social situations and concealing intentions, giving opportunities for information gathering, persuasion or deception:

- **Charm** expresses a character's allure, attractiveness and likability. It measures their aptitude to draw people's attention and gain favors through their looks. It plays a role in first impressions and may determine a character's artistic taste, demeanor or dressing style. It is the primary attribute for performers, entertainers, actors or courtesans.

- **Eloquence** illustrates a character's affability, way of expression and aptitude to communicate. It measures their capacity to speak in a clear and compelling manner, as well as their prowess in rhetoric and argumentation (even for writing letters or poems). It is the primary attribute for bards, writers, diplomats or politicians.

**Intellectual Abilities** represent a character's mental acuity and problem-solving capabilities, encompassing their *knowledge*, *reasoning*, *creativity* and *insight*. These play a crucial role when building and repairing things, deciphering complex information or making decisions based on critical thinking:

- **Knowledge** characterizes the accumulation and recollection of information, reflecting a character's aptitude to recall details about history, science or literature. It is used when identifying or discerning relevant facts or pieces of information, and is the primary attribute for researchers, loremasters, historians or scholars.

- **Reasoning** represents a character's ability to think rationally, understand things and make sound assessments. It plays an important role when solving problems through logic; and is used in analysis, computing or deduction. It is the primary attribute for technicians, craftsmen, administrators or cartographers.

**Wise Abilities** represent a character's shrewdness and mental strength, reflecting their *humility*, *composure*, *endurance* and *insight*. These traits enable them to navigate hardships with discernment, provide guidance or maintain a steady presence in the face of adversity:

- **Humility** expresses a character's ability to consider the feelings of others, be tolerant or act with care and compassion. It illustrates their aptitude to relate to people, provide support and make decisions that benefit not only themselves but also those around them. It is the primary attribute for social activists, healers and teachers.

- **Composure** represents a character's capacity to remain level headed in the face of adversity. It determines how well they handle unfavorable impulses like temptation, disappointment or fear, and is used in situations that require self-control or discipline. It is the primary attribute for ascetics, monks, hermits and priests.

<!-- Composure = having control of your life/having gone through millitary training -->

<!-- Equanimity : https://en.wikipedia.org/wiki/Equanimity -->

**Dual Domain Abilities** are part of two domains simultaneously, encompassing *endurance*, *agility*, *creativity* and *insight*. Due to their nature, these attributes provide synergistic advantages, enabling characters to blend their talents and adapt to a variety of situations:

- **Agility** (*Social* & *Physical*) symbolizes a character's movement capabilities, encompassing their speed, body coordination and overall nimbleness. It measures their prowess in scaling, jumping, swimming or dodging attacks. It is the main attribute for martial artists, acrobats, dancers and swashbucklers.

- **Creativity** (*Intellectual* & *Social*) represents a character's aptitude to think outside the box, generate new ideas or come up with innovative solutions. It is used when designing devices, producing original pieces of art, coming up with lies or improvising. It is the primary attribute for inventors, visionaries and artists.

- **Insight** (*Wise* & *Intellectual*) represents a character's maturity and understanding of the world through firsthand experience. It shows their aptitude in making astute observations or remaining skeptical in suspicious situations. It is used for detecting lies, perceiving underlying motives or evaluating the value of goods and services. It is the primary attribute for philosophers, explorers and merchants.

- **Endurance** (*Physical* & *Wise*) embodies a character's toughness and capacity to withstand injuries or pressure, reflecting their overall vitality and mental fortitude. It illustrates their aptitude to survive in harsh environments, navigate through uneven terrain and is the primary attribute for scouts, athletes, soldiers and hunters.




## Ability Scores


Each **Ability** has a score ranging from 1 to 5 and each score is assigned with a dice class:

|Ability Score |Dice Class    |Description |
|:------------:|:------------:|------------|
|1             |d4            |The character is weak or inept in that ability. |
|2             |d6            |The character is intermediate or has basic aptitude in that ability. |
|3             |d8            |The character is above average or has good aptitude in that ability. |
|4             |d10           |The character is very apt, skilled, or competent in that ability. |
|5             |d12           |The character is extremely strong or highly accomplished in that ability. |




## Character Setup


### - Assessing Categories

**Abilities** are grouped into one of three categories (*proficient*, *crippled* or *standard*), depending on the player's choices:

- **Proficient Abilities** are those that a character has an affinity with, allowing for quicker training and development compared to others. They start with a score of 3 <!-- and have a XP track of 6 -->.

- **Crippled Abilities** are those that a character struggles with, holding them back as they cannot be leveled. They always have a score of 1<!-- and no XP track -->.

- Finally, **Standard Abilities** are those that don't fall into any of the other two categories. They start with a score of 2<!-- and have a XP track of 9 -->.

When creating a new character, a player must pick two **Proficient Abilities** and four **Crippled Abilities** for them:

- For **Proficient Abilities**, the choice should be based on the character's **Archetypes**. *Champions* may only pick between *physical* **Abilities**, *icons* between *social* **Abilities**, genius between *intellectual* **Abilities** and *sages* between *wise* **Abilities**.

- For **Crippled Abilities**, the choice is free as long as the character doesn't have more than one **Crippled Ability** aligned with an **Archetype**. For instance, *champions* cannot have more than one crippled *physical* **Ability**, *icons* cannot have more than one crippled *social* **Ability**, genius cannot have more than one crippled *intellectual* **Ability**, and *sages* cannot have more than one crippled *wise* **Ability**.

<!-- This means that new characters have a grand total of 22 Ability points. -->


### - Assessing Total Health

Each character has a maximum **Health** pool value, representing their capacity to withstand physical wounds and remain conscious. This is how it's calculated (starting at 0):

|Ability Score     |Endurance         |Strength          |Agility           |
|:----------------:|:----------------:|:----------------:|:----------------:|
|1                 |total *health* +5 |total *health* +2 |total *health* +1 |
|2                 |total *health* +6 |total *health* +3 |total *health* +2 |
|3                 |total *health* +7 |total *health* +4 |total *health* +3 |
|4                 |total *health* +8 |total *health* +5 |total *health* +4 |
|5                 |total *health* +9 |total *health* +6 |total *health* +5 |

<!-- Provide an example? -->


### - Assessing Emotional Thresholds

**Emotions** represent a characters' psychological vulnerabilities, influencing their behavior and responses to adversity. Each **Emotion** is assigned with a threshold, which determines how much of that **Emotion** a character can hold before entering a **Breakdown** state. This is how these values are established:

- First, each **Emotion** is associated with an **Ability** and an **Archetype** :

|Emotion     |Ability   |Archetype |
|:-----------|:---------|:---------|
|Anger       |Humility  |Champion  |
|Frustration |Composure |Icon      |
|Anxiety     |Composure |Genius    |
|Guilt       |Insight   |Sage      |

- Second, based on the character's **Ability** score associated with the **Emotion**, and if they've picked the associated **Archetype**, a value is assessed:

|Ability Score |Without Archetype |With Archetype |
|:------------:|:----------------:|:-------------:|
|1             |Threshold = 3     |Threshold = 2  |
|2             |Threshold = 4     |Threshold = 2  |
|3             |Threshold = 5     |Threshold = 3  |
|4             |Threshold = 6     |Threshold = 3  |
|5             |Threshold = 7     |Threshold = 4  |

>*During a game, Ability scores may increase and character Archetypes may change. Once a scenario is over, players should recalibrate their total Health and Emotional thresholds to reflect these changes (if any).*

***








# **Template Characters**

***




## Lucian


![*Lucian*](https://i.imgur.com/eIhWYrY.jpg)


- **Archetypes** : *Genius*, *Sage*

- **Drives** : *Solution*, *Care*

- **Quest** : *"I want to develop a treatment for the grey plague, and provide relief to those afflicted by its devastating effects."*

- **Proficient Abilities** : *Reasoning*, *Humility*

- **Crippled Abilities** : *Strength*, *Agility*, *Charm*, *Eloquence*

- **Standard Abilities** : *Dexterity*, *Creativity*, *Knowledge*, *Insight*, *Composure*, *Endurance*

- **Health Pool** : 9

- **Emotional Thresholds** : *Anger* (5), *Frustration* (4), *Anxiety* (2), *Guilt* (2)

>*Lucian is a dedicated healer with strong intellect and compassion. From a young age, he showed an affinity for herbalism and alchemy, while also helping care for the wounded at a local monastery. It wasn't until the grey plague swept across the region that he found his true calling: discover a cure for the disease and alleviate the suffering it caused.*

>*This character possesses a sharp mind, honed through years of rigorous study and practical experience. His keen insightful nature allows him to remain grounded, recognize the limits of his understanding and the importance of collaboration. However, his strength and nimbleness pale in comparison to his acumen, and his reserved personality leads to challenges when trying to command attention.*

<!-- Lucian maintains strong resilience, having experienced first hand the many facets of human suffering. He approaches challenges with calm and confidence, viewing them as opportunities for growth rather than obstacles. But he is also prone to self-doubt, often struggling with issues he can't understand, or feeling overwhelmed by the weight of past decisions. -->

<!-- POINTER -->




## Morwen


![*Lucian*](https://i.imgur.com/hGerJ5G.jpg)


- **Archetypes** : *Champion*, *Icon*

- **Drives** : *Revenge*, *Solution*

- **Quest** : *"I want to find the person who murdered my crew, and make them pay for what they've done."*

<!-- Make this about revenge instead. -->

- **Proficient Abilities** : *Dexterity*, *Eloquence*

- **Crippled Abilities** : *Knowledge*, *Endurance*, *Humility*, *Composure*

- **Standard Abilities** : *Strength*, *Agility*, *Charm*, *Creativity*, *Reasoning*, *Insight*

- **Health Pool** : 10

- **Emotional Thresholds** : *Anger* (2), *Frustration* (2), *Anxiety* (3), *Guilt* (4)

>*Morwen is a rogue who rose from humble beginnings in the streets of Lüdim. While growing up, she took part in various criminal operations, slowly making a name for herself and building a network of trusted allies. However, her life took a dark turn when her hideout was raided, resulting in the death of her crew. She now wants to uncover the culprit and avenge her friends.*

>*Morwen's strength lies in her dexterity and silver tongue, moving through shadows with grace and precision. However, she is not without her vulnerabilities. Beneath her charming facade, she may not always be the most wise or composed individual, with her inner turmoil often manifesting in fits of anger or frustration.*

<!-- Morwen is not without her vulnerabilities. If things don't go her way, her inner turmoil often manifests in fits of frustration. She also harbors a considerable fear of being judged, relentlessly seeking validation or approval from others. Every failure or criticism cuts deep, amplifying her already poor judgmental nature into cycles of anger and denial. -->

***
