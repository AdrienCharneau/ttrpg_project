# REMEMBER




- **Author** : Tutorialtuna (@Discord)

- **Date** : 3E 420




## Scale


**Bitter Coast** : about 211 km long / 632 sq km - traveling from Seyda Neen to Hla Oad on FOOT would take 1.5 days (Fast) or about 2 days (Normal)

- For reference, the scale assumes 500km from Ghostgate to Mournhold

- Remember the "Hex-Crawling" rules in Balladry. Hex have *scales* that vary depending on the "zoom range" of a map: *local scale* (1.5 Miles), *provincial scale* (6 Miles), *regional scale* (24 Miles)

- Maps made with wonderdraft: https://www.wonderdraft.net/


### - Traversing by land

A day’s travel by land allows for 12 Miles per day. This equates to 8 Hexes at *local Scale*, 3 Hexes at *provincial*, and half a Hex at *regional Scale*.


### - Traversing by water

|1d6|Wind Speed                      |
|:-:|:-------------------------------|
|1  |No wind, must row (8 Miles/day) |
|2  |6 Miles/hour                    |
|3  |12 Miles/hour                   |
|5  |18 Miles/hour                   |
|5  |24 Miles/hour                   |
|6  |36 Miles/hour                   |


### - View Distances

|Landmark Examples                        |View Distance  |
|:---------------------------------------:|:--------------|
|Campfire at night, Homestead...          |~ 1 Mile Away  | 
|Village, Small River or Lake...          |~ 3 Mile Away  |
|Watchtower, Smokestack...                |~ 9 Mile Away  |
|Castle on a Hill, Large River or Lake... |~ 27 Mile Away |
|Mountain Range, Large City...            |~ 81 Mile Away |




## Wind & Weather


Bitter Coast Region Weather Table:

|Type    |Likeliness|
|:------:|:--------:|
|Clear   |10%       |
|Cloudy  |60%       |
|Thunder |10%       |
|Foggy   |10%       |
|Rain    |10%       |

>https://en.uesp.net/wiki/Morrowind:Bitter_Coast




## Encounters


Every day of travel calls for one (*Encounter Check*?) to be made. Encounters are rolled normally using an (*Activity Die*?) assigned to the area.





## Links


### - Tamrielic Gazeteer - Core Notes

>https://docs.google.com/document/d/1tj1CzbbnQBGSzR1MaHxdcMKji5m9VE2pS1TyP3UTTFk


### - Morrowind Notes

>https://docs.google.com/document/d/1AA30wQfKJPeDl5ZUvxNKVzSBYNNMEYMD4O7SaIq5XG8


### - Skyrim Notes

>https://docs.google.com/document/d/1qWmjqN84iz-por8jF-26rdHO711UdIOCqZZrn4SY_9o


### - Maps

>https://drive.google.com/drive/folders/15GjUDpB9PmhRRRk0hHTUB1LzTjHncadv


### - To Escape One’s Fate - Introductory Module for Balladry

Use this as a reference on how to format and organize the project


### - UESRPG Homebrew

>https://drive.google.com/drive/folders/1OEyIGpNO5dL7USX7ASp4-Ls195QDge0l

***








# GENERAL IDEAS




- Divide the region in 3 areas : *Seyda Neen Area*, *Hla Oad Area* and *Gnaar Mok Area* (check map on cloud drive to get an idea)

- The Bitter Coast is not well mapped ( https://en.uesp.net/wiki/Morrowind:Nine-Toes )

- The Bitter Coast has salt marshes ( https://en.uesp.net/wiki/Morrowind:Generic_Dialogue_B ) -> salt production

- Hlormaren should be a Hlaalu held stronghold? -> I think the alternative of The Bitter Coast not having a road and the fortress being abandonned is better

- Netch breeding should only be kept for the West Gash and the Ascadian isles?

- Ancestral tomb sizes should vary depending on the name of the family being buried in them. For instance, the Venim ancestral tomb ( https://en.uesp.net/wiki/Morrowind:Venim_Ancestral_Tomb ) should be huge as it is the tomb of house Redoran's current archmaster.

- House Redoran used to control the Caldera mining area? There's in-game dialogue supporting this hypothesis (according to Tuna) ( https://en.uesp.net/wiki/Morrowind:Shut_the_Mines_Down https://en.uesp.net/wiki/Morrowind:Garisa_Llethri )




## Ashlanders


The Bitter Coast (and some other southern parts of Vvardenfell) still have ashlander tribes hanging around. But these are slowly being driven north by colonization efforts from the Empire and the Great Houses.

>An offshoot of the Ahemmusa tribe should be included somwhere in the Bitter Coast? According to ESO, they have been living there up to 3E 427. Initially I wanted them to breed netch, but I think it's better if I just have them be fishermen instead.

>Anything found in the Gazeteer is just a snapshot of what's in the region at the moment it is described, but can change over time. For instance, ashlander tribes are nomadic, therefore their camp location found in the book would only be temporary.




## Lore Notes


- Vvardenfell names : https://www.reddit.com/r/teslore/comments/55xlrt/balmora_aldruhn_and_maar_gan_from_morrowind_as/

- Big info dump on Vvardenfell : https://www.reddit.com/r/teslore/comments/wmdule/on_materialistic_aspects_of_warfare_in/




## Inconsistencies


### - How is Ald'Ruhn so big?

>https://www.reddit.com/r/teslore/comments/b8qlf5/how_is_aldruhn_so_big/




## Fan Fictions

>https://www.reddit.com/r/teslore/comments/ord6uy/dunmer_psychological_illnesses_in_the_3rd_era/

>https://www.reddit.com/r/teslore/comments/njdvag/outlanders/

***








# LOCATIONS




## Abaesen-Pulu Egg Mine

Abandonned egg mine/settlement because of the blight




## Addamasartus Range

Separates the Seyda Neen area from the Pelagiad area




## Aharunartus

Island further West from Seyda Neen. This is where the Sarys ancestral burial grounds are located.




## Ancestral Tombs :  & Thelas

Text




## Ascadian Pass

Goes to Pelagiad through the Addamasartus Range




## Akimaes, Assemanu & Ribbi

Three islands to the East of Seyda Neen. Ribbi has a Shipwreck on its western shoreline. Assemanu is home to a Sixth House base. (There is an underwater cave network?)




## Zainsipilu Ash Hills

Traverses the Bitter Coast and reaches the Bitter Coast & Hla Oad. The Samarys ancestral burial grounds are located not far from the south-eastern entrance of it.




## Nimawia Creek, Island & Peninsula

Located right to the West of Seyda Neen. You can find 1 holy shrine to *Saint Olms the Just* (patron of sailors) OR *Saint Felms the Bold* (patron of fishmongers). This is also where the Thelas ancestral burial grounds are located (Drarayne Thelas from Balmora goes to visit this place once in a while).




## Odai River


### - Option 1 : Trade route (most likely hypothesis)

The Odai river is being used as the main route to carry ebony from Caldera to Ebonheart. It is also the itinerary dunmer merchants have been using for centuries.

Ships are docked in Balmora and surveilled by imperial troops from the Moonmoth Legion Fort. There are a few settlements down the stream (Shulk - with a rich egg mine & Shurdan - poorer almost abandonned egg mine).

The (already built) Hlaalu owned Rethan Manor (fortress?) overlooks the estuary and the coast. It should serve as both a garrison and a checkpoint, protecting the ships and collecting transit taxes.

Hla Oad is now a much more important settlement, relocated (from the game) right at the estuary of the river. It shouldn't be as "dodgy" as its current in-game representation but should still have its fair share of slums. And that would be where Fatleg's Drop Off is located.

To control the route, the Empire had the perfect spot on Firemoth island. Until they lost it to Grurn's undead army.


### - Option 2 : Flatlands (legacy hypothesis)

A mostly shallow stream with miles of flat land that separate its waters from the hills and cliffs that surround it. It can be found at its deepest here:

- At its source and through Balmora
- Just south of Balmora, between two rock hills east of the Tharys ancestral burial grounds
- When passing the Odai plateau
- When reaching its estuary

These happen to be the locations where the river bank is closest to the hills. Other than these places, the stream is not navigable, hence why it is not used as a trade route. Still, pirogues and rowboats are able to transport in small quantities fish and other goods to Balmora and the Shulk Egg Mine.

>Check AI generated art and screenshots in `Cloud drive/Tamrielic Gazeteer/Art-Sketches/Odai River`




## Seyda Neen


- The town is being built (Vvardenfell has only been opened for 6 years)

- It should have around 100-300 population (take inspiration from *Wharram Percy* ? -> https://the-yorkshireman.com/wharram-percy/ )

- It used to be a Hlaalu port (ESO), place got abandoned eventually and only the lighthouse ruins remained. Since the opening of Vvardenfell, the Empire is now rebuilding the settlement as a base (get the Imperial Navy involved?). And because the place is technically still a Hlaalu town, it did not require obtaining a *construction contract* from the Duc of Vvardenfell.

>https://en.uesp.net/wiki/Lore:Gold_Coast_Trading_Company

- Just like Ellis Island was in New York, Seyda Neen is the main hub for taking in immigrants that come from the mainland.

>https://en.wikipedia.org/wiki/Ellis_Island#First_immigration_station




## Firemoth


- Have *Grurn* be the undead resurection of a prominent Nord sea warlord who served under king Wulfharth during the 1st Era? His ship sunk in the Inner Sea around the time of the Battle of Red Mountain. Have the necromancer Telura Ulver ( https://en.uesp.net/wiki/Morrowind:Telura_Ulver ) be the one trying to raise him from the dead, march out of the sea and launch his attack on Fort Firemoth?




## ADD

- 2 or three minor fishermen settlements

- Dres merchant in Hla Oad (or Gnaar Mok...?)

***