# General

***




The Song-Dai Viet war, also known as the Ly-Song War, was a military conflict between the Ly dynasty of Dai Viet and the Song dynasty of China between 1075 and 1077. The war was sparked by the shifting allegiances of tribal peoples such as the Zhuang/Nung on the frontier borderlands, and increasing state control over their administration.

In 1075, Emperor Ly Nhan Tong ordered a preemptive invasion of Song dynasty territory with more than 80,000 soldiers, razing the city of Yongzhou after a 42-day siege. The Song retaliated with an army of 300,000 the following year. In 1077, Song forces nearly reached Dai Viet's capital Thăng Long before being halted by General Ly Thuong Kiet at the Nhu Nguyet River in modern-day Bac Ninh Province.

After a prolonged stalemate and high casualties on both sides, Ly Thuong Kiet offered apologies for the invasion and the Song commander Guo Kui agreed to withdraw his troops, ending the war.




## Pointers


>https://en.wikipedia.org/wiki/Song_dynasty#History

>https://en.wikipedia.org/wiki/%C4%90%E1%BA%A1i_Vi%E1%BB%87t#Economy

>https://en.wikipedia.org/wiki/Song%E2%80%93%C4%90%E1%BA%A1i_Vi%E1%BB%87t_war#Song_expansion

***








# Sources

***




## Wikipedia


- [Emperor Shenzong of Song](https://en.wikipedia.org/wiki/Emperor_Shenzong_of_Song)

- [Song Dynasty](https://en.wikipedia.org/wiki/Song_dynasty)

- [Song-Dai Viet War](https://en.wikipedia.org/wiki/Song%E2%80%93%C4%90%E1%BA%A1i_Vi%E1%BB%87t_war)

***








# Geography-Locations

***




- Annam, Red River Delta

- Guangyuan (modern-day Cao Bằng, Vietnam)

- [Geological map](https://www.researchgate.net/publication/374722491/figure/fig1/AS:11431281210284049@1702036923593/Map-showing-the-survey-sites-in-northern-Vietnam-1-Bac-Giang-Province-2-Hai-Phong.jpg)

- [SEA general political map 1](https://upload.wikimedia.org/wikipedia/commons/c/c2/Map-of-southeast-asia_1000_-_1100_CE-es.svg)

- [SEA general political map 2](https://upload.wikimedia.org/wikipedia/commons/5/5e/VietnamChampa-fr.svg)

- [Guangnan west circuit map](https://upload.wikimedia.org/wikipedia/commons/7/79/Northern_Song_Circuits.png)

- [Guangxi province map](https://www.chinamaps.org/images/china-map/province-maps/guangxi-province-map.jpg)

<!-- https://en.wikipedia.org/wiki/Geography_of_Vietnam -->

<!-- https://en.wikipedia.org/wiki/Northern_Vietnam -->

<!-- https://en.wikipedia.org/wiki/Annam_(French_protectorate)#Etymology_and_pre-colonial_usage -->

<!-- https://en.wikipedia.org/wiki/B%E1%BA%AFc_Ninh_province -->

<!-- https://en.wikipedia.org/wiki/Cao_B%E1%BA%B1ng_province -->

<!-- https://en.wikipedia.org/wiki/Gui_Prefecture_(Guangxi) -->

<!-- https://en.wikipedia.org/wiki/Guangnan_West_Circuit -->

<!-- https://en.wikipedia.org/wiki/Kunlun_Pass -->




## Landmarks


- **Rivers** : Cau River, Red River/Hong River, Bach Dang River, Kinh Thay River, Duong River

<!-- https://en.wikipedia.org/wiki/Zuo_River -->

- **Seas** : Gulf of Tonkin, Halong Bay

- **Mountains** : Annamite Range, Kunlun Pass, Nhoi Mountain




## Cities


#### Thang Long

Emperor Ly Thai To (r. 1009–1028) moved the court to the abandoned city of Dai La, which had previously been a seat of power under the Tang dynasty, and renamed it to Thang Long in 1010. The city became what is now present-day Ha Noi.

<!-- https://en.wikipedia.org/wiki/Hanoi -->

<!-- https://en.wikipedia.org/wiki/Imperial_Citadel_of_Th%C4%83ng_Long -->

#### Haiphong

<!-- https://en.wikipedia.org/wiki/Haiphong -->

#### Yongzhou

<!-- https://en.wikipedia.org/wiki/Nanning -->

***








# Politics-Military

***




## Song dynasty


The Song dynasty was an imperial dynasty of China that ruled from 960 to 1279. The dynasty was founded by Emperor Taizu of Song, who usurped the throne of the Later Zhou dynasty and went on to conquer the rest of the Ten Kingdoms, ending the Five Dynasties and Ten Kingdoms period. At the time of the events, the capital was in the northern city of Bianjing (now Kaifeng) and the dynasty controlled most of what is now Eastern China. It was the first Chinese government to establish a permanent standing navy.

The Song court maintained diplomatic relations with Chola India, the Fatimid Caliphate of Egypt, Srivijaya, the Kara-Khanid Khanate in Central Asia, the Goryeo Kingdom in Korea, and other countries that were also trade partners with Japan. Chinese records even mention an embassy from the ruler of "Fu lin" (i.e. the Byzantine Empire), Michael VII Doukas, and its arrival in 1081.

The expansion of the population, growth of cities, and emergence of a national economy led to the gradual withdrawal of the central government from direct involvement in economic affairs. Although civil service examinations had existed since the Sui dynasty, they became much more prominent in the Song period. This led to a shift from a military-aristocratic elite to a scholar-bureaucratic elite, with scholar-officials assuming a larger role in local administration.

>*Scholar-officials were government officials and prestigious scholars in Chinese society, forming a distinct social class. They were appointed by the emperor of China to perform day-to-day political duties, and mostly came from the scholar-gentry who had earned academic degrees by passing the imperial examinations. Highly educated, especially in literature and the arts, including calligraphy and Confucian texts, they were the elite class of imperial China.*

<!-- https://en.wikipedia.org/wiki/Military_history_of_the_Song_dynasty -->


### - Personalities

#### Emperor Renzong

<!-- https://en.wikipedia.org/wiki/Emperor_Renzong_of_Song -->

#### Emperor Shenzong

The Emperor Shenzong of Song (25 May 1048 – 1 April 1085), personal name Zhao Xu, was the sixth emperor of the Song dynasty of China. He reigned from 1067 until his death in 1085 and is best known for making Wang Anshi the head of government and supporting his New Policies.

Shenzong and Wang Anshi also pursued direct military reforms. In theory, each commandery was 500 troops strong, but in actuality, the number was much lower and contained many old or weak soldiers due to corruption. Shenzong cut down the number of excess troops so that the entire army was less than 900,000 strong and established the Area Generalship System to improve communications, discipline, and troop levy efficiency. Meanwhile, the Baojia system was introduced as a village defense system intended to bolster domestic security and provide further support to the regular army.

Though the New Policies gave Shenzong a large budget surplus, they failed to achieve their goal of improving the Song dynasty's military. The Western Xia continued to inflict defeats on the Song and an attack on the Liao dynasty remained unthinkable. This was caused by continually low army quality, poor logistics, and overall poor leadership. The Baojia system, for example, did not produce troops capable enough to replace the imperial army. The military failures of the Reforms, to which Shenzong had devoted immense amounts of energy, contributed to his eventual illness and death.




## Dai Viet monarchy


Dai Viet (literally Great Viet), often known as Annam, was a monarchy in eastern Mainland Southeast Asia from the 10th century AD to the early 19th century, centered around the region of present-day Hanoi, Northern Vietnam. The territories of the early Viet state comprised the lowland Red River Basin to the Nghe An region.

Early Dai Viet emerged in the 960s as a hereditary monarchy with Mahayana Buddhism as its state religion and buddhist clergy were put in charge of important positions. In the early period, the Viet monarchy existed as a "charter state", "mandala state" or "centralized feudal system", with the Viet king "man of prowess" at its center. The kingdom was only able to control several inner areas, while outer areas (phu) were autonomously governed by local clans of various ethnolinguistic backgrounds. They aligned to the royal clan through Buddhist alliances, such as temples, while bureaucracy was still practically nonexistent.

>*For examples, an inscription dating from 1107 in Ha Giang records the religious-political connection between the Nung Ha clan with the dynasty, or another inscription dated 1100 commemorates Ly Thuong Kiet as the lord of Thanh Hoa.*

Starting during the reign of Le Hoan, the Viet expansion extended Viet territories from the Red River Delta in all directions. As a mandala realm, its direct territories could not exceed more than 150 miles in diameter, however, the Dai Viet kingdom was able to maintain a large influence sphere due to active coastal trade and maritime activities with other Southeast Asian states.

<!-- https://en.wikipedia.org/wiki/%C4%90%E1%BA%A1i_Vi%E1%BB%87t -->

<!-- https://en.wikipedia.org/wiki/L%C3%BD_dynasty -->


### - Personalities

#### Ly Nhan Tong

<!-- https://en.wikipedia.org/wiki/L%C3%BD_Nh%C3%A2n_T%C3%B4ng -->

#### Ly Thuong Kiet

<!-- https://en.wikipedia.org/wiki/L%C3%BD_Th%C6%B0%E1%BB%9Dng_Ki%E1%BB%87t -->




## Nung Clan


### - Personalities

#### Nung Tri Cao

<!-- https://en.wikipedia.org/wiki/Nong_Zhigao -->

#### Nung Tông Dan

<!-- https://en.wikipedia.org/wiki/Song%E2%80%93%C4%90%E1%BA%A1i_Vi%E1%BB%87t_war#Song_expansion : Frontier unrest began anew in 1057 when Nung Tông Dan (C. Nong Zongdan), a kinsman of Nung Tri Cao, entered Song territory. In 1062 when Nùng Tông Ðán requested his territory be incorporated into the Song empire, Renzong accepted his request. According to The Draft Documents Pertaining to Song Official Matters, Nùng Tông Ðán was regarded by the Song as the prefect of Leihuo prefecture, renamed "Pacified Prefecture" (Shun'anzhou), and possessed the title "Personal Guardian General of the Right." Nùng Trí Hội (C. Nong Zhihui) the brother of Nùng Trí Cao, received the title "Personal Guardian of the Left." Other members of the Nùng clan in Temo such as Nùng Binh, Năng Lượng, and Nùng Hạ Khanh swore loyalty to the Song. Nùng Trí Cao's former generals Lư Báo, Lê Mạo, and Hoàng Trọng Khanh were also granted official titles. -->

***








# Commerce-Society

***




## Song dynasty


Economically, the Song dynasty was unparalleled with a gross domestic product three times larger than that of Europe during the 12th century. China's population doubled in size between the 10th and 11th centuries. This growth was made possible by expanded rice cultivation, use of early-ripening rice from Southeast and South Asia, and production of widespread food surpluses. Social life during the Song was vibrant. Citizens gathered to view and trade precious artworks, the populace intermingled at public festivals and private clubs, and cities had lively entertainment quarters.


### - Personalities

#### Wang Anshi

<!-- https://en.wikipedia.org/wiki/Wang_Anshi -->

<!-- https://en.wikipedia.org/wiki/New_Policies_(Song_dynasty) -->

#### Wang Han

<!-- https://en.wikipedia.org/wiki/Song%E2%80%93%C4%90%E1%BA%A1i_Vi%E1%BB%87t_war#Song_expansion : Frontier unrest began anew in 1057 when Nung Tông Dan (C. Nong Zongdan), a kinsman of Nung Tri Cao, entered Song territory. The frontier administrator Wang Han visited Nung Tông Dan's camp at Leihuo (in Quang Nguyên) to discourage him from seeking inclusion in the Song dynasty since it would upset the Viet court. Instead he proposed that he stay outside Song territory as a loyal frontier militia leader. Wang feared that a resurgence of the Nung clan would spell trouble for the frontier. The Song court ignored his apprehensions and offered the Nung and other communities "Interior Dependency" status. -->




## Dai Viet monarchy


Contact between the Song dynasty of China and the Viet state increased through raids and tributary mission, which resulted in Chinese cultural influences on Vietnamese culture, the first civil examination based on the Chinese model was staged in 1075, Chinese script was declared the official script of the court in 1174, and the emergence of Vietnamese demotic script (Chu Nom) occurred in the 12th century.

<!-- https://en.wikipedia.org/wiki/%C4%90%E1%BA%A1i_Vi%E1%BB%87t#Etymology -->




## Cultures-Ethnicities


- **Nung People** :

- **Zhuang people** :

- **Baiyue people** :

***








# Philosophy-Religion

***




Philosophers such as Cheng Yi and Zhu Xi reinvigorated Confucianism with new commentary, infused with Buddhist ideals, and emphasized a new organization of classic texts that established the doctrine of Neo-Confucianism.

<!-- https://en.wikipedia.org/wiki/Neo-Confucianism -->

***








# Science-Technology

***




Technology, science, philosophy, mathematics, and engineering flourished during this time. The Song dynasty was the first in world history to issue banknotes or true paper money. The spread of literature and knowledge was enhanced by the rapid expansion of woodblock printing and the 11th-century invention of movable type printing.

The Song dynasty saw the first surviving records of the chemical formula for gunpowder, the invention of gunpowder weapons such as fire arrows, bombs, and the fire lance. It also saw the first discernment of true north using a compass, first recorded description of the pound lock, and improved designs of astronomical clocks.

***








# Timeline

***




<!-- https://en.wikipedia.org/wiki/Nong_Zhigao_rebellions -->

<!-- https://en.wikipedia.org/wiki/Battle_of_Nh%C6%B0_Nguy%E1%BB%87t_River_(1077) -->

***
