```
   __|  |  |    \    _ \    \     __| __ __| __|  _ \   __|
  (     __ |   _ \     /   _ \   (       |   _|     / \__ \
 \___| _| _| _/  _\ _|_\ _/  _\ \___|   _|  ___| _|_\ ____/
```

***








# Archetypes

***




**Archetypes** are the central aspects of a character, setting their general tone or high concept. When creating a new character, players must choose 2 **Archetypes** out of the 4 available:

- **Champions** thrive for power and specialize in physical abilities.

- **Icons** thrive for fame and specialize in social abilities.

- **Geniuses** thrive for knowledge and specialize in intellectual abilities.

- **Sages** thrive for wisdom and specialize in wise abilities.

>*For instance, if a character has the combination of champion and icon, they might aspire to become a powerful combatant with a desire for fame and glory. Whereas a character that combines champion and sage may thrive to become a wandering knight guided by principles of wisdom and virtue.*


### - Examples

Here's a list of combinations that can be used for new characters. Keep in mind that these are just examples, and players encouraged to come up with their own ideas:

- **Arcanic Scholar** (*Genius* & *Icon*) : this character aspires to become a well-known polymath, mastering multiple disciplines of magic and leaving behind a lasting legacy of discoveries and breakthroughs.

- **Ascetic Healer** (*Sage* & *Genius*) : this character aspires to become a reclusive yet compassionate caregiver with extensive medical knowledge, setting them apart as a trusted authority in the medical field.

- **Itinerant Bard** (*Icon* & *Sage*) : this character aspires to become a charismatic performer who uses their wit, charm, and musical talents to entertain and inspire others, encouraging social change and fostering peace.

- **Notorious Rogue** (*Icon* & *Champion*) : this character aspires to become a master of crime and legendary figure from the underworld, able to outmaneuver authorities and strike fear into the hearts of the rich.

- **War Artifist** (*Genius* & *Champion*) : this character aspires to become a skilled artisan of war machines, blending arcanic knowledge with combat expertise, and harnessing their intellect to shape the course of battles.

- **Warrior Monk** (*Champion* & *Sage*) : this character aspires to become a honorable warrior following the codes of a martial doctrine, roaming the land mastering both physical combat and practicing self-discipline.




## Drives


**Drives** represent a character's motivations, guiding their actions throughout the narrative. Each character must choose 2 **Drives**, with at least 1 **Drive** matching one of their **Archetypes**:

#### Champion

- **Authority** : the character wants to assert dominance and issue commands.

- **Revenge** : the character seeks justice or retribution.

- **Wealth** : the character is driven to amass riches and possessions.

#### Icon

- **Hedonism** : the character prioritizes indulgence and entertainment.

- **Distinction** : the character seeks to impress others and bring attention to themselves.

- **Influence** : the character aims to sway people to their side and mold their opinions.

#### Genius

- **Solution** : the character is driven by problem-solving.

- **Production** : the character wants to build or conceive things.

- **Curiosity** : the character is motivated by a thirst for knowledge.

#### Sage

- **Care** : the character wants to help and protect others.

- **Peace** : the character seeks to foster harmony and mend relationships.

- **Duty** : the character is dedicated to fulfilling their obligations.




## Quest


Each character also has a specific **Quest** that should be a reflection of their **Drives**. This is written as a concise statement, typically one or two sentences, describing a unique objective or aim for the character:

- **Wealth** & **Duty** : *"I want to travel to the mines of Eldoria to negotiate a trade agreement for my family's gemstone business."*

- **Distinction** & **Solution** : *I want to decipher the enigmatic Sandravi script to earn acclaim and notoriety in academic circles."*

- **Authority** & **Care** : *"I want to administer the city of Lüdim, where hunger and repression have plagued the inhabitants for months."*

- **Hedonism** & **Production** : *"I want to experience the serene sunset of Ignicor and compose a poem about it."*

***








# Abilities

***




Characters have 12 **Abilities** that can be grouped into 4 different domains (*Physical*, *Social*, *Intellectual* & *Wise*):

|Ability    |Domain              |Usage |
|:----------|:-------------------|:-----|
|Strength   |Physical            |*Close combat, breaking things, moving or pushing heavy loads...* |
|Dexterity  |Physical            |*Reflexes, detection, perception, accuracy...* |
|Agility    |Physical & Social   |*Speed, athletics, acrobatics, proficiency in sports...* |
|Charm      |Social              |*Seduction, entertainment, persuading through looks, style or demeanor...* |
|Eloquence  |Social              |*Public speaking, bargaining, persuading through oratory skills...* |
|Creativity |Social & Intellectual |*Thinking outside the box, come up with lies, find solutions to problems or puzzles...* |
|Knowledge  |Intellectual        |*Analyzing, understanding or recalling information, identifying something, learning spells...* |
|Reasoning  |Intellectual        |*Casting spells, computing, calculating, crafting, building or repairing...* |
|Insight    |Intellectual & Wise |*Bargaining, reading a situation or person...* |
|Humility   |Wise                |*Helping, healing, understanding or tolerating others...* |
|Composure  |Wise                |*Maintaining emotional stability, staying calm under pressure, handling stressful situations...* |
|Endurance  |Wise & Physical     |*Stamina, fatigue, resisting physical or mental harm...* |

>*Abilities may be used actively as characters strive to accomplish tasks, or reactively to counteract enemy actions or evade environmental dangers.*




## Ability Scores


Each **Ability** has a score ranging from 1 to 5 and each score is assigned with a dice class:

|Ability Score |Dice Class    |Description |
|:------------:|:------------:|------------|
|1             |d4            |The character is weak or inept in that ability. |
|2             |d6            |The character is intermediate or has basic aptitude in that ability. |
|3             |d8            |The character is above average or has good aptitude in that ability. |
|4             |d10           |The character is very apt, skilled, or competent in that ability. |
|5             |d12           |The character is extremely strong or highly accomplished in that ability. |




## Character Setup


### - Assessing Categories

**Abilities** are grouped into one of three categories (*proficient*, *crippled* or *standard*), depending on the player's choices:

- **Proficient Abilities** are those that a character has an affinity with, allowing for quicker training and development compared to others. They start with a score of 3.

- **Crippled Abilities** are those that a character struggles with, holding them back as they cannot be leveled. They always have a score of 1.

- Finally, **Standard Abilities** are those that don't fall into any of the other two categories. They start with a score of 2.

When creating a new character, a player must pick two **Proficient Abilities** and four **Crippled Abilities** for them:

- For **Proficient Abilities**, the choice should be based on the character's **Archetypes**. *Champions* may only pick between *physical* **Abilities**, *icons* between *social* **Abilities**, genius between *intellectual* **Abilities** and *sages* between *wise* **Abilities**.

- For **Crippled Abilities**, the choice is free as long as the character doesn't have more than one **Crippled Ability** aligned with an **Archetype**. For instance, *champions* cannot have more than one crippled *physical* **Ability**, *icons* cannot have more than one crippled *social* **Ability**, genius cannot have more than one crippled *intellectual* **Ability**, and *sages* cannot have more than one crippled *wise* **Ability**.


### - Assessing Total Health

Each character has a maximum **Health** pool value, representing their capacity to withstand physical wounds and remain conscious. This is how it's calculated (starting at 0):

|Ability Score     |Endurance         |Strength          |Agility           |
|:----------------:|:----------------:|:----------------:|:----------------:|
|1                 |total *health* +5 |total *health* +2 |total *health* +1 |
|2                 |total *health* +6 |total *health* +3 |total *health* +2 |
|3                 |total *health* +7 |total *health* +4 |total *health* +3 |
|4                 |total *health* +8 |total *health* +5 |total *health* +4 |
|5                 |total *health* +9 |total *health* +6 |total *health* +5 |


### - Assessing Emotional Thresholds

**Emotions** represent a characters' psychological vulnerabilities, influencing their behavior and responses to adversity. Each **Emotion** is assigned with a threshold, which determines how much of that **Emotion** a character can hold before entering a **Breakdown** state. This is how these values are established:

- First, each **Emotion** is associated with an **Ability** and an **Archetype** :

|Emotion     |Ability   |Archetype |
|:-----------|:---------|:---------|
|Anger       |Humility  |Champion  |
|Frustration |Composure |Icon      |
|Anxiety     |Composure |Genius    |
|Guilt       |Insight   |Sage      |

- Second, based on the character's **Ability** score associated with the **Emotion**, and if they've picked the associated **Archetype**, a value is assessed:

|Ability Score |Without Archetype |With Archetype |
|:------------:|:----------------:|:-------------:|
|1             |Threshold = 3     |Threshold = 2  |
|2             |Threshold = 4     |Threshold = 2  |
|3             |Threshold = 5     |Threshold = 3  |
|4             |Threshold = 6     |Threshold = 3  |
|5             |Threshold = 7     |Threshold = 4  |

>*During a game, Ability scores may increase and character Archetypes may change. Once a scenario is over, players should recalibrate their total Health and Emotional thresholds to reflect these changes (if any).*

***








# Template Characters

***




## Lucian


- **Archetypes** : *Genius*, *Sage*

- **Drives** : *Solution*, *Care*

- **Quest** : *"I want to develop a treatment for the grey plague, and provide relief to those afflicted by its devastating effects."*

- **Proficient Abilities** : *Reasoning*, *Humility*

- **Crippled Abilities** : *Strength*, *Agility*, *Charm*, *Eloquence*

- **Standard Abilities** : *Dexterity*, *Creativity*, *Knowledge*, *Insight*, *Composure*, *Endurance*

- **Health Pool** : 9

- **Emotional Thresholds** : *Anger* (5), *Frustration* (4), *Anxiety* (2), *Guilt* (2)

>*Lucian is a dedicated healer with strong intellect and compassion. From a young age, he showed an affinity for herbalism and alchemy, while also helping care for the wounded at a local monastery. It wasn’t until the grey plague swept across the region that he found his true calling: discover a cure for the disease and alleviate the suffering it caused.*

>*This character possesses a sharp mind, honed through years of rigorous study and practical experience. His keen insightful nature allows him to remain grounded, recognize the limits of his understanding and the importance of collaboration. However, his strength and nimbleness pale in comparison to his acumen, and his reserved personality leads to challenges when trying to command attention.*




## Morwen


- **Archetypes** : *Champion*, *Icon*

- **Drives** : *Revenge*, *Solution*

- **Quest** : *"I want to find the person who murdered my crew, and make them pay for what they've done."*

- **Proficient Abilities** : *Dexterity*, *Eloquence*

- **Crippled Abilities** : *Knowledge*, *Endurance*, *Humility*, *Composure*

- **Standard Abilities** : *Strength*, *Agility*, *Charm*, *Creativity*, *Reasoning*, *Insight*

- **Health Pool** : 10

- **Emotional Thresholds** : *Anger* (2), *Frustration* (2), *Anxiety* (3), *Guilt* (4)

>*Morwen is a rogue who rose from humble beginnings in the streets of Lüdim. While growing up, she took part in various criminal operations, slowly making a name for herself and building a network of trusted allies. However, her life took a dark turn when her hideout was raided, resulting in the death of her crew. She now wants to uncover the culprit and avenge her friends.*

>*Morwen's strength lies in her dexterity and silver tongue, moving through shadows with grace and precision. However, she is not without her vulnerabilities. Beneath her charming facade, she may not always be the most wise or composed individual, with her inner turmoil often manifesting in fits of anger or frustration.*

***