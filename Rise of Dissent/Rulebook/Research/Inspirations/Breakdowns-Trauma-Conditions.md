# BLADES IN THE DARK




>*Stress* & *Trauma* mechanic `Blades in the Dark.pdf` p.21, *Resistance roll* `Blades in the Dark.pdf` p.40

- **Resistances** : insight (deception, understanding), prowess (physical injury), resolve (mental strain, willpower)




## Trauma Conditions


Trauma conditions are permanent. Your character acquires the new personality quirk indicated by the condition, and can earn xp by using it to cause trouble. When you mark your fourth trauma condition, your character cannot continue as a daring scoundrel. You must retire them to a different life or send them to prison to take the fall for the crew’s wanted level.

- **Cold** : You’re not moved by emotional appeals or social bonds.

- **Haunted** : You’re often lost in reverie, reliving past horrors, seeing things.

- **Obsessed** : You’re enthralled by one thing: an activity, a person, an ideology.

- **Paranoid** : You imagine danger everywhere; you can’t trust others.

- **Reckless** : You have little regard for your own safety or best interests.

- **Soft** : You lose your edge; you become sentimental, passive, gentle.

- **Unstable** : Your emotional state is volatile. You can instantly rage, or fall into despair, act impulsively, or freeze up.

- **Vicious** : You seek out opportunities to hurt people, even for no good reason.

***








# CALL OF CTHULHU




>*Madness* `Call Of Cthulhu - Core Rulebook.pdf` p.157, p.418

- **Madness** : amnesia, psychosomatic disability, violence, paranoia, significant person, faint, flee in panic, physical hysterics or emotional outburst, phobia, mania

***








# CHRONICLES OF DARKNESS




>p.30, p.74

***








# D&D 5e




Conditions alter a creature's capabilities in a variety of ways and can arise as a result of a spell, a class feature, a monster's attack, or other effect. Most conditions, such as blinded, are impairments, but a few, such as invisible, can be advantageous.

A condition lasts either until it is countered (the prone condition is countered by standing up, for example) or for a duration specified by the effect that imposed the condition.

If multiple effects impose the same condition on a creature, each instance of the condition has its own duration, but the condition's effects don't get worse. A creature either has a condition or doesn't.

The following definitions specify what happens to a creature while it is subjected to a condition.


### - Blinded

- A blinded creature can't see and automatically fails any ability check that requires sight.

- Attack rolls against the creature have advantage, and the creature's attack rolls have disadvantage.


### - Charmed

- A charmed creature can't attack the charmer or target the charmer with harmful abilities or magical effects.

- The charmer has advantage on any ability check to interact socially with the creature.


### - Deafened

- A deafened creature can't hear and automatically fails any ability check that requires hearing.


### - Frightened

- A frightened creature has disadvantage on ability checks and attack rolls while the source of its fear is within line of sight.

- The creature can't willingly move closer to the source of its fear.


### - Grappled

- A grappled creature's speed becomes 0, and it can't benefit from any bonus to its speed.

- The condition ends if the grappler is incapacitated.

- The condition also ends if an effect removes the grappled creature from the reach of the grappler or grappling effect, such as when a creature is hurled away by the thunderwave spell.


### - Incapacitated

- An incapacitated creature can't take actions or reactions.


### - Invisible

- An invisible creature is impossible to see without the aid of magic or a special sense. For the purpose of hiding, the creature is heavily obscured. The creature's location can be detected by any noise it makes or any tracks it leaves.

- Attack rolls against the creature have disadvantage, and the creature's attack rolls have advantage.


### - Paralyzed

- A paralyzed creature is incapacitated and can't move or speak.

- The creature automatically fails Strength and Dexterity saving throws.

- Attack rolls against the creature have advantage.

- Any attack that hits the creature is a critical hit if the attacker is within 5 feet of the creature.


### - Petrified

- A petrified creature is transformed, along with any nonmagical object it is wearing or carrying, into a solid inanimate substance (usually stone). Its weight increases by a factor of ten, and it ceases aging.

- The creature is incapacitated, can't move or speak, and is unaware of its surroundings.

- Attack rolls against the creature have advantage.

- The creature automatically fails Strength and Dexterity saving throws.

- The creature has resistance to all damage.

- The creature is immune to poison and disease, although a poison or disease already in its system is suspended, not neutralized.


### - Poisoned

- A poisoned creature has disadvantage on attack rolls and ability checks.


### - Prone

- A prone creature's only movement option is to crawl, unless it stands up and thereby ends the condition.

- The creature has disadvantage on attack rolls.

- An attack roll against the creature has advantage if the attacker is within 5 feet of the creature. Otherwise, the attack roll has disadvantage.


### - Restrained

- A restrained creature's speed becomes 0, and it can't benefit from any bonus to its speed.

- Attack rolls against the creature have advantage, and the creature's attack rolls have disadvantage.

- The creature has disadvantage on Dexterity saving throws.


### - Stunned

- A stunned creature is incapacitated, can't move, and can speak only falteringly.

- The creature automatically fails Strength and Dexterity saving throws.

- Attack rolls against the creature have advantage.


### - Unconscious

- An unconscious creature is incapacitated, can't move or speak, and is unaware of its surroundings.

- The creature drops whatever it's holding and falls prone.

- The creature automatically fails Strength and Dexterity saving throws.

- Attack rolls against the creature have advantage.

- Any attack that hits the creature is a critical hit if the attacker is within 5 feet of the creature.

***








# EXALTED




>*Limit Triggers* p.135

***








# FATE




>*Stress and Consequences* p.58, p.66, p.170

***








# MASKS - CONDITIONS




>p.90


Conditions are negative emotional states that beset the characters in your story. Having a condition marked means that your character is experiencing that emotion—anger, or fear, or insecurity, or whatever. You’re always in charge of how your character is played; you can choose to play your character as calm and dispassionate, even with Angry marked. But that means you’re holding your anger inside, and it still impacts your actions as the story progresses.




## Effects


Conditions impact and interfere with your ability to take action. Each condition gives you an ongoing -2 to certain moves. Remember that the lowest modifier you can roll a move with is -3. When a move tells you to mark a condition, unless it says otherwise, mark any condition you choose. Sometimes the GM tells you a specific condition to mark, especially after a hard move. If you need to mark a condition and all conditions are already marked, you’re taken out. You lose consciousness or flee the scene—one way or another, you cannot continue there. The conditions and their associated penalties are:

- Afraid (-2 to directly engage)

- Angry (-2 to comfort or support or pierce the mask)

- Guilty (-2 to provoke someone or assess the situation)

- Insecure (-2 to defend someone or reject what others say)

- Hopeless (-2 to unleash your powers) 




## Clearing Conditions


Some of the basic moves help you clear conditions—comfort or support and defend, in particular. But the most straightforward way to clear conditions is to take a particular action to relieve that emotional state. The action varies depending on the specific condition.

At the end of any scene in which you take the corresponding action, clear that condition.

- To clear Angry, hurt someone or break something important.

- To clear Afraid, run from something difficult.

- To clear Guilty, make a sacrifice to absolve your guilt.

- To clear Hopeless, fling yourself into easy relief.

- To clear Insecure, take foolhardy action without talking to your team.

Clearing Angry requires you to vent your anger, either on someone or on something. It’s not enough to just punch a bag—you have to take your anger out on someone, or something important. What’s important is different for every character, but the GM should ask if and why an object is important when the Angry character breaks it. Hurting someone doesn’t necessarily mean hurting them physically—yelling at them and hurting their feelings would do the trick, too.

Clearing Afraid requires you to avoid or flee from a complicated, dangerous, or problematic situation. That could mean anything from running away from a villain to fleeing the room when someone wants to have a conversation about your recent actions. The key is avoidance—instead of confronting something, you’re running from it.

Clearing Guilty requires you to pay some cost on behalf of others, those you feel you’ve wronged or let down. It doesn’t require them to actually absolve you of your guilt—just so long as you pay a price in an attempt to redeem yourself. This might be anything from standing alone against a dangerous villain so your teammates can escape to agreeing to follow the older heroes’ rules even when it’s easier not to.

Clearing Hopeless requires you to seek the easiest and quickest way to relieve your feelings. Most likely, that means making stupid decisions in pursuit of stupid fun. It could be anything from finding some cheap booze and getting drunk to making out with the wrong person.

Clearing Insecure means following your worst, most impulsive instincts without consulting anybody first. You feel doubtful of your own abilities, so you’re proving yourself by following your own plan without talking to anyone first. That could be anything from deciding to attack the bad guy while the rest of your team waits in stealth to agreeing to give up the crucial component for the death ray in exchange for escaping right now, all without anyone else’s input.

Remember that even if you take the action, you only clear the condition at the end of the scene. You can clear multiple conditions in one scene, but you continue to be affected by the penalty for the entirety of the scene.

***








# SMALLVILLE




>p.10, p.58


- **Stress** : afraid, angry, exhausted, insecure, injured

***








# SPIRE - THE CITY MUST FALL




>*Stress*, *Fallout* p.14, p.16




## Stress


When you act and something goes wrong, you’ll take stress to one of your resistances. There are five kinds of resistance:

- **Blood** : Physical damage and exhaustion.

- **Mind** : Mental stress, instability and insanity.

- **Silver** : Loss of money or resources.

- **Shadow** : Loss of secrecy, damage to cover identities, police and government attention.

- **Reputation** : Loss of social standing in a group or community.

Sometimes it will be stated outright what kind of stress a situation doles out – for example, when a Lajhan casts a spell, most often they’ll be asked to mark stress against Mind as they channel the vast energies of their goddess. If it’s not clear where stress would go, the GM and the player can work it out together.

Situations inflict stress on players relative to the risk and danger involved; this is determined by the GM. Breaking into a low-rent slum in Derelictus will cause D3 stress on a failure; it’s D6 to infiltrate a gung-ho Red Row bar; and it’s D8 if you’re sneaking into somewhere really important, like the Duke’s personal paddle-steamer headquarters in the Docks.

If you’re fighting someone (or running away from them) and you take stress, you’ll usually take stress equal to the amount that their weapon inflicts.

If you are fighting multiple enemies at once and suffer stress, take +1 stress for each enemy after the first. Each time you take stress, the GM will roll a D10 to see if you suffer fallout; definite, codified negative effects. If the D10 rolls lower than your current total stress, you’ll suffer fallout (detailed below).


### - Removing Stress

You can actively remove stress from your character in one of three ways:

You can lay low to remove all stress suffered, but the plot will move ahead without you, and things will occur that are outside of your control.

You can act to remove stress in a particular category by narratively spending time doing something that would remove stress (i.e. borrowing money from a friend to lower Silver stress, visiting a doctor to lower Blood stress etc). Remove D3, D6 or D8 stress depending on the lengths you go to in order to recover.

You can refresh by acting in accordance with your character’s refresh action, outlined by your class or your additional abilities. When you refresh, remove D3, D6 or D8 stress depending on how fully, and how dramatically, you fulfilled the requirements of your refresh action:

- **Azurite** : Carry out a deal that benefits you more than it does the other party.

- **Bound** : Bring a criminal to justice.

- **Carrion-Priest** : Complete a hunt and take your quarry.

- **Firebrand** : Take something back from those who would oppress you.

- **Idol** : Someone feels deeply moved when they witness your art.

- **Knight** : Engage in reckless excess.

- **Lajhan** : Help those who cannot help themselves.

- **Masked** : Show someone they should not have underestimated you.

- **Midwife** : Defend the defenceless

- **Vermissian Sage** : Uncover hidden information.

Also, suffering fallout reduces the amount of stress your character carries – it shifts from abstract to definite. When you suffer minor fallout, remove 3 stress; when you suffer moderate fallout, remove 5; when you suffer severe fallout, remove 7.

>*Example: Ana is playing Harold, an aging Lajhan (a priest of the drow moon goddess). Harold’s total stress is getting pretty high after evading some city guard patrols –especially his Shadow stress – and Ana has a few options for how to lower it.*

>*Firstly, Harold could lay low, removing all stress from all resistances; the GM decides that this would give the city guard a chance to uncover one of the group’s hidden caches of weapons, and it would be confiscated.*

>*Alternatively, Harold could act to remove stress from his Shadow resistance; for example, bribing a sympathetic guard to say he’s fled the area, convincing a local bar owner to provide him an alibi, or burning evidence. This would remove D3, D6 or D8 stress from Shadow, and exactly how much is up the GM.*

>*Finally, Harold could use the refresh action from his class: Help Those Who Cannot Help Themselves. During play, Harold takes great lengths to protect those hurt by the aelfir, putting himself in danger as he does so. As above, this would remove D3, D6 or D8 stress, but it can be removed from any resistance as Harold finds the strength to continue with the rebellion.*




### - Keeping Track

The GM keeps track of the player characters’ stress. This has two effects: 1) it makes it much easier to roll for fallout, and 2) it shifts stress from a mechanical effect into a narrative description. When the GM allocates stress to a player character, they should describe what’s happening in-world – not just say the number and move on.

If a player wants to know how much stress they’re suffering from, the GM can either tell them numerically (which is simple, if a bit boring) or, again, describe it in narrative terms. Rather than saying “You’ve got 4 stress marked against Mind and Blood each,” the GM could say “You can feel your heartbeat ringing in your ears. You taste tin in your mouth. You can’t focus on what anyone is saying.”

The GM can ask players to keep track of their own stress and roll for their own fallout, but in practice they tend to forget about the second part. It’s up to you; we prefer it this way, but it might not work for your group.




## Fallout


Each time a player character takes stress, the GM checks for fallout – to see if there’s any kind of ongoing, serious effect at play. The GM rolls a D10 and compares it to the current total stress marked against the character’s resistances – if the result of the D10 roll is lower, the character suffers fallout. The level of fallout depends on the amount of total stress the character had when the fallout triggered:

- **2-4 Stress** : *Minor Fallout*

- **5-8 Stress** : *Moderate Fallout*

- **9+ Stress** : *Severe Fallout*

Work out what happens based on the type of stress that triggered the fallout; usually that’s the resistance type that has the most stress marked against it.

If there’s a mix, or it’s not clear, go with whatever sounds more interesting. You can choose from the list below or make up your own.

>*Remember: On minor fallout, remove 3 stress; on moderate fallout, remove 5; on severe fallout, remove 7.*

If you’d like, you can allocate two fallout results from the category before the one selected instead. (So: instead of being KNOCKED OUT, you can be BLEEDING and PANICKED.) You can also upgrade fallout from one stage to the next if a character suffers fallout from repeat sources. (So: if a character who’s already BLEEDING suffers further minor fallout during the same fight, you can get rid of that BLEEDING and give them a moderate result instead, like BROKEN LIMB.)

You, or an ally, can mitigate Blood or Mind fallout with an appropriate Fix check (see page 17); minor fallout can be removed, but anything moderate or above can only be stabilised and managed unless you receive long-term care. Magical sources can also cure fallout, rather than removing stress: 3 stress to repair minor fallout, 5 stress to repair moderate or fallout, and 7 stress to repair severe fallout. (But severe fallout generally isn’t the sort of thing that you “cure.”)

Non-Player Characters (NPCs) mark stress like players, but they only have one resistance. When an NPC takes total stress equal to their resistance, they flee the situation, drop out of the conflict, or do whatever it is the players want them to do.


### - Minor Fallout

Minor fallout comprises short-term, low-impact effects.

- **Bleeding** (*Blood*) : You’re leaking. Until you get proper medical care, each time you make an action, mark 1 stress against Blood before you roll the dice (but don’t check for fallout).

- **Stunned** (*Blood*) : You take a blow to the head, or are winded, giving your enemies opportunity to act. If they want to get away from you, they can do so while you stagger about and gather your senses; otherwise, you can’t use the Fight skill to earn additional dice for the remainder of the situation.

- **Tired** (*Blood*) : You struggle to stay awake, and you overlook something crucial in the current situation. The GM works out what it is, and it comes back to haunt you before the session is over.

- **Adrenaline** (*Blood/Mind*) : Your instincts kick in and you do something stupid. If you’re trying to get away or de-escalate, you lash out at your opponents. If you’re trying to fight, you get panicked and retreat. This is only momentary, and fades after a moment – long enough for a single, immediate action.

- **Panicked** (*Mind*) : Your heart hammers in your chest. Choose: either leave the scene and calm down, or make things difficult for everyone else around you and increase the difficulty of all actions performed nearby to you by 1.

- **Shaken** (*Mind*) : You can’t focus on what you’re doing. You may not gain dice from Domains until you take time to calm down.

- **Weird** (*Mind*) : You do something unsettling that bothers normal people – obsessive behaviour, singing to yourself, fulfilling a strange compulsion at inappropriate times. At the earliest opportunity, the GM can declare that your weirdness puts a useful NPC off you (and probably your allies, too). Once this happens, remove this fallout.

- **Lash Out** (*Reputation*) : You’re pissed off. You immediately lash out at the source of stress, no matter whether or not this is a sensible idea.

- **Lie** (*Reputation*) : Trying to justify your actions, you tell a lie that will cause a problem this or next session.

- **Compromised** (*Shadow*) : A friendly NPC asks you to justify your strange behaviour.

- **Rumour** (*Shadow*) : Word of your actions gets around the area. The GM and the other players should work out three statements that are whispered about you in bars and taverns: two true, one false.

- **Debtor** (*Silver*) : During the next session or later in this one, an NPC who lent you money will call in a favour.

- **Pawned** (*Silver*) : Until the end of the next session, you lose the use of one piece of equipment that’s important to you.


### - Moderate Fallout

Moderate fallout represents serious problems that are ongoing or acute.

- **Broken Arm** (*Blood*) : Your arm breaks under the strain, and splintered bone juts up through your skin. You can’t use the arm until it heals (which will take a month or so, or require powerful healing magic).

- **Broken Leg** (*Blood*) : Your leg bones splinter and crack. You can’t walk without crutches for a month or so, and you’ll automatically fail any Pursue attempts. Any action where you’d need to be quick on your feet (fighting, dancing, climbing) is either impossible or suffers from an increased difficulty.

- **Knocked Out** (*Blood*) : You fall unconscious for several hours, during which time your enemies get an advantage.

- **Freak Out** (*Mind*) : You lose it, and attempt to kill (or at least drive off) whatever caused your mental break. You won’t calm down until you’re restrained, it’s destroyed or flees the scene, or you’re knocked unconscious.

- **Memory Holes** (*Mind*) : You did things that you can’t quite recall. The GM and every player aside from you work together to determine what you did that you blocked out from your mind while you step outside of the room, or during downtime. These are generally pretty awful things, and they can have happened up to a year ago in game time or immediately upon suffering fallout. Your character has zero memory of the events, but everyone else involved knows what happened.

- **Permanently Weird** (*Mind*) : As WEIRD, but: it lasts until you get proper treatment, and the GM can trigger it whenever they like. You can suppress the effects of this fallout for a scene by marking D3 stress against Mind.

- **Phobia** (*Mind*) : You acquire a phobia of something related to the stress you suffered – a person, an item, a place, a situation, or something even more abstract. You will avoid the subject of your phobia whenever possible, and if you have to interact with it, difficulties are increased by 1.

- **Humiliated** (*Reputation*) : You make a total fool of yourself. You can’t make use of any allies associated with the actions that pushed you into fallout until you prove yourself once more in their eyes.

- **Vendetta** (*Reputation*) : During the next session, or later in this one, an NPC from this session will return with a vendetta against you.

- **Arrested** (*Shadow*) : You are caught and arrested by the guard and put in a jail cell, awaiting proper questioning for your crimes.

- **Criminal** (*Shadow*) : You are accused of criminal acts, whether true or not, and wanted posters featuring your face and name are put up throughout the district in which you committed the crime. If the crime is particularly exciting, your name and face may make it into the papers.

- **Watched** (*Shadow*) : At any point from now until the end of the next session, the GM can decide that someone tailing you watches you perform an action; they can tell you which action if they wish. This will come back to haunt you before long.

- **Sold** (*Silver*) : You’re forced to sell off something valuable to pay your debtors. Work out with the GM what you’re forced to sell.

- **Services Rendered** (*Silver*) : You’re forced to sell your skills to a third party to pay your debtors, and the work is not pleasant. Work out with the GM what your character doesn’t want to do but is prepared to in order to make ends meet.


### - Severe Fallout

Severe fallout can mean the end of a character’s story – or represent a huge change in it. They are permanent, lasting effects that may have an impact on a character’s allies and bonds as well as the character themselves.

- **Chosen** (*Blood*) : You pass out and awaken in a half-dream state before your god (and if you haven’t devoted your life to a god, someone else’s god) and you can strike a bargain with them in exchange for your life. If you accept their terms, you return to consciousness (changed, always changed) and if you don’t, you slip away into death.

- **Dying** (*Blood*) : You’re dying. Choose: do something useful before you die (and roll with mastery, because this is the last thing you’ll ever do) or desperately try to cling onto life (and lose something vital in the bargain).

- **Obsessed** (*Mind*) : Your mind splinters and shards until it is razor-sharp and barbed, focused on a single goal. You are now PERMANENTLY WEIRD (as above), but when you attempt to achieve your goal you roll with mastery, and the difficulty for all other actions increases by 1. Once you achieve your goal, your mind gives under the strain, and you are forcibly retired.

- **Renegade** (*Mind*) : Your mind shattered by the stress of your actions, you turn against the Ministry and all it stands for (the precise reasons why are up to you and the GM). This is not immediate; the fallout represents the first crack in the dam of your mind. Over the next few sessions, play out your descent. Should you survive, you join a rival faction, or become one unto yourself, set against the cell.

- **Reviled** (*Reputation*) : You are expelled from somewhere important to you, and you can no longer return on pain of death (or, in more civilised society, arrest). People in the district will spread stories of how utterly reprehensible you are throughout the city.

- **Burned** (*Shadow*) : The Ministry decides that you are no longer worth the risk of employing, and after feeding you some false information, sells you out to your enemies. Good luck.

- **Wrath of the Sun Gods** (*Shadow*) : Your operations are uncovered by the Solar Guard, the grand inquisitors of the aelfir church, and you are hunted. Many of your NPC bonds are dragged out into the street and shot. Your friends dare not speak your name.

- **Destitute** (*Silver*) : You are utterly without material resource, and deep in debt to several bad people. You have one last chance to gather up a vast sum of money or resources to pay back your creditors, or you’ll end up dead, mauled beyond recognition by loan sharks, or sold off into some Red Row sweatshop.

- **Turned** (*Silver/Shadow*) : You have sold out your allies in exchange for massive bribes to get you out of debt, or to lessen your own punishment. You are now working for one of your enemies as well as the Ministry, and you may be asked at any time to perform favours or rat out your team-mates. The only way out of this is to come clean (and be killed by the Ministry), completely eliminate the enemy forces who have leverage on you, or take the next skywhale out of Spire and never look back.


### - Bond Fallout

Bonds are relationships that player characters have with each other or non-player characters; and in the case of NPCs, they can suffer stress and fallout as though they were a Resistance. (In this text, we use “bond” to refer to both the relationship and the person with whom the relationship is shared.) As it’s tracked separately from regular stress, NPC bonds get their own section for fallout:

- **Mistake** (*Minor*) : Your bond’s actions raise suspicion. Mark D3 stress in Shadow.

- **Depleted** (*Minor*) : Your bond uses up their resources, suffers an injury, is upset with you, or is otherwise unable or unwilling to help you out. Next time you ask a favour of them, the difficulty of the task they attempt is increased by 1.

- **In Trouble** (*Moderate*) : Your bond finds themselves in trouble, and can’t be used until the problem is resolved.

- **Leak** (*Moderate*) : Your bond unwittingly gives out information that threatens the operation. Mark D6 stress in Shadow.

- **Betrayal** (*Severe*) : Mark D8 stress in Shadow. Your bond turns against you, although they will not tell you this until it is too late.

- **Made an Example of** (*Severe*) : Your bond’s connection to the resistance is uncovered, and they are made to pay the price. They are dragged into the streets and shot in public after being denounced for their crimes and declared dead.


### - Creating your own Fallout

Feel free to create you own fallout results specific to a particular situation once you’ve got a handle on the system – this list can’t, and shouldn’t try to, cater to every potential situation. As a guide, Minor fallout is generally resolved within a day or so; Moderate fallout takes longer, or has minor permanent effects; and Severe fallout can redirect a character arc or introduce an entirely new facet to it.

When you create fallout of your own, it’s important to consider the effect it will have on the game. Forcing a player to “sit out” and miss taking part is rarely fun for them; similarly, anything which imposes a flat penalty to all rolls encourages them not to act until the fallout is removed. Try to focus around pushing the story forward instead of punishing the characters and/or players.

***








# THE VEIL - STATES




>p.37


In The Veil, the states are your core emotions. The reason for this is to keep the players in-character, playing their protagonists and thinking about how their character is feeling when reacting to the world. Why a protagonist is doing something and how it makes them feel is a great way to add another layer of immersion to the world. Rolls become less about in what way a character does something, or how well they do it, and more about how they react to the world and what they’re feeling. Based on that, we can create a fiction that feels believable and real because we can picture the protagonist and empathize with them and their actions.

Just like in real life, the protagonists could be feeling multiple emotions at once, but the game requires a player to choose the strongest core emotion behind the action that triggers a move. Sometimes, the action you’re doing could be considered an emotion; maybe you’re being sexy but you’re feeling Scared. In that case, the core emotion is Scared, despite what you’re trying to portray to others.

There are six core states: Mad, Sad, Scared, Joyful, Powerful, and Peaceful. From each of these core emotions, almost anything that a character is feeling can be extrapolated. A feeling wheel follows to illustrate the point, and is part of the reference sheets along with basic moves, playbooks, and other handouts.

[The Feeling Wheel by Gloria Willcox]

Note that the placement of the core emotions on the wheel do not correlate to one another. The playbook handouts themselves show you what emotions are in opposition to one another in The Veil, this does not. This is purely a tool to help you figure out what your protagonist could be feeling in any given scene when a move is triggered.




## Emotion Spikes


If you look to the side of the states on the character sheet, there are places to mark bubbles. Every time you roll that state, you mark a bubble on it. These are called emotion spikes. The more you roll that emotion, the more intense that emotion will become for that character in the fiction. When you reach the last check mark, you change that state, temporarily, to +1 and all the other ones to -2. In order for a character to get out of this state where they are experiencing a single emotion acutely, there is a special move, called alleviate. Alleviate is a different type of move from the basic moves in terms of how it works, and will be discussed later on, on page 44. For now, suffice to say that it is a mechanical tool in the game to help people keep track of how intensely they are experiencing the emotion during gameplay. When you continue to experience something, it grows until it is resolved. When you’re triggering moves and you’re in the same state, things are getting more intense for your protagonist.

In the example with Takeshi using his Sad state when searching for his sword, he rolled his modifier, which was -1. He also would mark one of the bubbles next to his Sad state since that’s what he was when he rolled. Each time he rolls that state he would mark a bubble and, if he should ever mark the fifth bubble, he would trigger the alleviate move.




## Mitigating States


Describing in the fiction how the character deals with feeling a certain way is a means to mitigate these emotional spikes in the fiction. For instance, if a protagonist is almost maxed out in the Mad state, the player might describe how they become Peaceful when rolling the next state, as long as it makes sense for their character in the fiction. If they were to do so, they would add one emotional spike to Peaceful and remove one from Mad.

Whenever a player is describing an emotion they may remove an emotional spike of the opposing state. The player still marks a spike in the state they use even when it opposes another state. You’ll know if emotions oppose each other because they’re grouped together on the character sheet. Mad and Peaceful, Sad and Joyful, Powerful and Scared; each oppose one another.




## Assigning Values to States


Every player starts with the same six state values, but can distribute any of the numbers into any state that they like. These values are: -1, 0, 0, +1, +1, and +2. When you think about the implications of assigning the numbers, remember that a -1 in a state means that things tend to go wrong the most when the protagonist is experiencing that emotion, +2 things most often go their way, and work your way out from there.

Furthermore, you could look at it from another point of view and say that the +2 means the protagonist is most comfortable with this emotion, and the -1 means they have problems dealing with or facing this emotion. The zeroes and +1 modifiers are something in the middle—they’re probably pretty familiar with the emotions, and things can go either way for them when in these emotional states. Ultimately, how you go about assigning your states is up to you but it’s good to have a justification in your head for why you’re doing so.

During character creation, Joan goes about assigning her states to her protagonist, Hannah. She envisions her as an optimistic and cheerful person so she puts her +2 in Joyful. She sees her not really identifying with being fearful, because she’s so optimistic, she’s only Scared for splitseconds, mostly when she’s surprised. Bearing that in mind, she puts her -1 in Scared. She feels like Hannah has a temper and feels confident most of the time, so she puts +1 in the Mad and Powerful boxes. 

That leaves her with a 0 in Peaceful, which she figures makes sense given that some of the emotions coming from that core are pensive and thoughtful. Hannah is more likely to react impulsively than think on her actions due to her temper, and is generally pretty daring. When she’s at her lowest is when she’s feeling helpless, insecure, and anxious.

When you start playing, sometimes how you think or have your protagonist act or react is different than what you initially thought. Don’t sweat it; at the end of the first session, work with the MC and swap state modifiers to fit your new understanding of your protagonist. After the first session you should have a fairly clear understanding of your character; after the first session, though, if they react differently than how you thought, it’s just the character changing and growing, and being dynamic.

Remember to stay away from the idea of emotions meaning that your protagonist is bad or good at something while in this state; it’s better to think of it as just the propensity for the character to act in a certain manner.




## Alleviate


Alleviate is a special move that works a bit differently than the basic moves. The basic moves all trigger when something is described in the fiction. While this is also somewhat true for alleviate, what triggers it is the buildup of emotion, which is represented by the emotion spike bubbles next to each state.

When a PC marks the fifth bubble in an emotion all of the modifiers change and the state that the fifth bubble was marked in becomes +1 and all others are temporarily locked in at -2. In addition, the player chooses one of the following when another move is triggered:

To keep using the spiked out emotion, in which case the emotion spike in that state decreases by 1. When all emotion spikes have been removed, all state modifiers go back to normal.

To use a different state at a -2. If they do so they may erase all emotion spikes in the spiked out state. They must still mark an emotion spike in the state they just rolled.

***