# Action Roll




>*Emotions are raised when characters perform Daring Rolls.*

<!-- Alternative: Emotions are raised when characters perform Daring Rolls and obtain Limited Successes or Limited Failures. This is overall less "punishing", although a bit more "complex", plus I'd have to provide an explanation for the rule, other than "it's better for game balance". -->




## Cost-Benefits analysis


Here is how I rationalize the (un)balance in this system... It is clear that, overall, not attempting **Daring Rolls** is a better strategy than "wasting" your **Emotions**.

BUT **Daring Rolls** increase the probabilities of success and raising an **Emotion** isn't really that punishing (imo).

Plus if you look at other games like *Masks* or *The Veil*, they raise **Emotions** EVERY TIME a character takes an action, so there's that to consider as well.

#### If Success Score < Challenge AND Risk Score < Risk

- If perform **Daring Roll** :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |Maybe *success* or *limited success* |
|+                      |Can earn **Ability XP** |
|-                      |Maybe *failure* |
|-                      |Raise an **Emotion** |

- If give up :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |No *failure* |
|+                      |No **Emotion** raised |
|-                      |*Limited Failure* |
|-                      |Cannot earn **Ability XP** |

#### If Success Score < Challenge AND Risk Score > Risk

- If perform **Daring Roll** :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |Maybe *limited success* |
|+                      |Can earn **Ability XP** |
|-                      |Raise an **Emotion** |

- If give up :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |No **Emotion** raised |
|-                      |*Failure* |
|-                      |Cannot earn **Ability XP** |

#### If Success Score > Challenge AND Risk Score < Risk

- If perform **Daring Roll** :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |Can earn **Ability XP** |
|-                      |Maybe *limited success* |
|-                      |Raise an **Emotion** |

- If give up :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |No *limited Success* |
|+                      |No **Emotion** raised |
|-                      |Cannot earn **Ability XP** |

#### If Success Score > Challenge AND Risk Score > Risk

- If perform **Daring Roll** :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |Can earn **Ability XP** |
|-                      |Raise an **Emotion** |

- If give up :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |No **Emotion** raised |
|-                      |Cannot earn **Ability XP** |




## Standard Roll (4 die)


### - Success

|Ability Scores |Challenge 2 |Challenge 3 |Challenge 4 |
|:-------------:|:----------:|:----------:|:----------:|
|1/1            |26%         |5%          |1%          |
|2/2            |69%         |31%         |6%          |
|3/3            |85%         |52%         |15%         |
|4/4            |92%         |65%         |24%         |
|5/5            |95%         |74%         |32%         |


### - Trouble

|Ability Scores |Position 4 |Position 3 |Position 2 |
|:-------------:|:---------:|:---------:|:---------:|
|1/1            |6%         |31%        |69%        |
|2/2            |1%         |11%        |41%        |
|3/3            |1%         |5%         |26%        |
|4/4            |1%         |3%         |18%        |
|5/5            |1%         |2%         |13%        |


### - Formulas

```
output 2d{0, 0, 0, 1} + 2d{0, 0, 0, 1} >= 2 named "CHALLENGE - 2 : Ability 1/1 (4 die)"
output 2d{0, 0, 0, 1} + 2d{0, 0, 0, 1} >= 3 named "CHALLENGE - 3 : Ability 1/1 (4 die)"
output 2d{0, 0, 0, 1} + 2d{0, 0, 0, 1} >= 4 named "CHALLENGE - 4 : Ability 1/1 (4 die)"

output 2d{1, 1, 0, 0} + 2d{1, 1, 0, 0} >= 4 named "POSITION  - 4 : Ability 1/1 (4 die)"
output 2d{1, 1, 0, 0} + 2d{1, 1, 0, 0} >= 3 named "POSITION  - 3 : Ability 1/1 (4 die)"
output 2d{1, 1, 0, 0} + 2d{1, 1, 0, 0} >= 2 named "POSITION  - 2 : Ability 1/1 (4 die)"


output 2d{0, 0, 0, 1, 1, 1} + 2d{0, 0, 0, 1, 1, 1} >= 2 named "CHALLENGE - 2 : Ability 2/2 (4 die)"
output 2d{0, 0, 0, 1, 1, 1} + 2d{0, 0, 0, 1, 1, 1} >= 3 named "CHALLENGE - 3 : Ability 2/2 (4 die)"
output 2d{0, 0, 0, 1, 1, 1} + 2d{0, 0, 0, 1, 1, 1} >= 4 named "CHALLENGE - 4 : Ability 2/2 (4 die)"

output 2d{1, 1, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0} >= 4 named "POSITION  - 4 : Ability 2/2 (4 die)"
output 2d{1, 1, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0} >= 3 named "POSITION  - 3 : Ability 2/2 (4 die)"
output 2d{1, 1, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0} >= 2 named "POSITION  - 2 : Ability 2/2 (4 die)"


output 2d{0, 0, 0, 1, 1, 1, 1, 1} + 2d{0, 0, 0, 1, 1, 1, 1, 1} >= 2 named "CHALLENGE - 2 : Ability 3/3 (4 die)"
output 2d{0, 0, 0, 1, 1, 1, 1, 1} + 2d{0, 0, 0, 1, 1, 1, 1, 1} >= 3 named "CHALLENGE - 3 : Ability 3/3 (4 die)"
output 2d{0, 0, 0, 1, 1, 1, 1, 1} + 2d{0, 0, 0, 1, 1, 1, 1, 1} >= 4 named "CHALLENGE - 4 : Ability 3/3 (4 die)"

output 2d{1, 1, 0, 0, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0, 0, 0} >= 4 named "POSITION  - 4 : Ability 3/3 (4 die)"
output 2d{1, 1, 0, 0, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0, 0, 0} >= 3 named "POSITION  - 3 : Ability 3/3 (4 die)"
output 2d{1, 1, 0, 0, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0, 0, 0} >= 2 named "POSITION  - 2 : Ability 3/3 (4 die)"


output 2d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1} + 2d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1} >= 2 named "CHALLENGE - 2 : Ability 4/4 (4 die)"
output 2d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1} + 2d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1} >= 3 named "CHALLENGE - 3 : Ability 4/4 (4 die)"
output 2d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1} + 2d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1} >= 4 named "CHALLENGE - 4 : Ability 4/4 (4 die)"

output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} >= 4 named "POSITION  - 4 : Ability 4/4 (4 die)"
output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} >= 3 named "POSITION  - 3 : Ability 4/4 (4 die)"
output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} >= 2 named "POSITION  - 2 : Ability 4/4 (4 die)"


output 2d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1} + 2d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1} >= 2 named "CHALLENGE - 2 : Ability 5/5 (4 die)"
output 2d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1} + 2d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1} >= 3 named "CHALLENGE - 3 : Ability 5/5 (4 die)"
output 2d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1} + 2d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1} >= 4 named "CHALLENGE - 4 : Ability 5/5 (4 die)"

output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} >= 4 named "POSITION  - 4 : Ability 5/5 (4 die)"
output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} >= 3 named "POSITION  - 3 : Ability 5/5 (4 die)"
output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} >= 2 named "POSITION  - 2 : Ability 5/5 (4 die)"




output 2d{1, 1, 0, 0} + 2d{1, 1, 0, 0} = 0 named "CRITICAL SUCCESS - Ability 1/1 (4 die)"
output 2d{1, 1, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0} = 0 named "CRITICAL SUCCESS - Ability 2/2 (4 die)"
output 2d{1, 1, 0, 0, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0, 0, 0} = 0 named "CRITICAL SUCCESS - Ability 3/3 (4 die)"
output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 0 named "CRITICAL SUCCESS - Ability 4/4 (4 die)"
output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} + 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 0 named "CRITICAL SUCCESS - Ability 5/5 (4 die)"

output 2d{0, 0, 1, 1} + 2d{0, 0, 1, 1} = 0 named "CRITICAL FAILURE - Ability 1/1 (4 die)"
output 2d{0, 0, 1, 1, 1, 1} + 2d{0, 0, 1, 1, 1, 1} = 0 named "CRITICAL FAILURE - Ability 2/2 (4 die)"
output 2d{0, 0, 1, 1, 1, 1, 1, 1} + 2d{0, 0, 1, 1, 1, 1, 1, 1} = 0 named "CRITICAL FAILURE - Ability 3/3 (4 die)"
output 2d{0, 0, 1, 1, 1, 1, 1, 1, 1, 1} + 2d{0, 0, 1, 1, 1, 1, 1, 1, 1, 1} = 0 named "CRITICAL FAILURE - Ability 4/4 (4 die)"
output 2d{0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1} + 2d{0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1} = 0 named "CRITICAL FAILURE - Ability 5/5 (4 die)"
```






## Daring Roll (6 die)


### - Success

|Ability Scores |Challenge 2 |Challenge 3 |Challenge 4 |
|:-------------:|:----------:|:----------:|:----------:|
|1/1            |47%         |17%         |4%          |
|2/2            |89%         |66%         |34%         |
|3/3            |97%         |85%         |60%         |
|4/4            |99%         |93%         |75%         |
|5/5            |99%         |96%         |83%         |


### - Trouble

|Ability Scores |Position 4 |Position 3 |Position 2 |
|:-------------:|:---------:|:---------:|:---------:|
|1/1            |34%        |66%        |90%        |
|2/2            |10%        |32%        |65%        |
|3/3            |4%         |17%        |47%        |
|4/4            |2%         |10%        |34%        |
|5/5            |1%         |6%         |26%        |


### - Formulas

```
output 3d{0, 0, 0, 1} + 3d{0, 0, 0, 1} >= 2 named "CHALLENGE - 2 : Ability 1/1 (6 die)"
output 3d{0, 0, 0, 1} + 3d{0, 0, 0, 1} >= 3 named "CHALLENGE - 3 : Ability 1/1 (6 die)"
output 3d{0, 0, 0, 1} + 3d{0, 0, 0, 1} >= 4 named "CHALLENGE - 4 : Ability 1/1 (6 die)"

output 3d{1, 1, 0, 0} + 3d{1, 1, 0, 0} >= 4 named "POSITION  - 4 : Ability 1/1 (6 die)"
output 3d{1, 1, 0, 0} + 3d{1, 1, 0, 0} >= 3 named "POSITION  - 3 : Ability 1/1 (6 die)"
output 3d{1, 1, 0, 0} + 3d{1, 1, 0, 0} >= 2 named "POSITION  - 2 : Ability 1/1 (6 die)"


output 3d{0, 0, 0, 1, 1, 1} + 3d{0, 0, 0, 1, 1, 1} >= 2 named "CHALLENGE - 2 : Ability 2/2 (6 die)"
output 3d{0, 0, 0, 1, 1, 1} + 3d{0, 0, 0, 1, 1, 1} >= 3 named "CHALLENGE - 3 : Ability 2/2 (6 die)"
output 3d{0, 0, 0, 1, 1, 1} + 3d{0, 0, 0, 1, 1, 1} >= 4 named "CHALLENGE - 4 : Ability 2/2 (6 die)"

output 3d{1, 1, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0} >= 4 named "POSITION  - 4 : Ability 2/2 (6 die)"
output 3d{1, 1, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0} >= 3 named "POSITION  - 3 : Ability 2/2 (6 die)"
output 3d{1, 1, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0} >= 2 named "POSITION  - 2 : Ability 2/2 (6 die)"


output 3d{0, 0, 0, 1, 1, 1, 1, 1} + 3d{0, 0, 0, 1, 1, 1, 1, 1} >= 2 named "CHALLENGE - 2 : Ability 3/3 (6 die)"
output 3d{0, 0, 0, 1, 1, 1, 1, 1} + 3d{0, 0, 0, 1, 1, 1, 1, 1} >= 3 named "CHALLENGE - 3 : Ability 3/3 (6 die)"
output 3d{0, 0, 0, 1, 1, 1, 1, 1} + 3d{0, 0, 0, 1, 1, 1, 1, 1} >= 4 named "CHALLENGE - 4 : Ability 3/3 (6 die)"

output 3d{1, 1, 0, 0, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0, 0, 0} >= 4 named "POSITION  - 4 : Ability 3/3 (6 die)"
output 3d{1, 1, 0, 0, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0, 0, 0} >= 3 named "POSITION  - 3 : Ability 3/3 (6 die)"
output 3d{1, 1, 0, 0, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0, 0, 0} >= 2 named "POSITION  - 2 : Ability 3/3 (6 die)"


output 3d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1} + 3d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1} >= 2 named "CHALLENGE - 2 : Ability 4/4 (6 die)"
output 3d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1} + 3d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1} >= 3 named "CHALLENGE - 3 : Ability 4/4 (6 die)"
output 3d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1} + 3d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1} >= 4 named "CHALLENGE - 4 : Ability 4/4 (6 die)"

output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} >= 4 named "POSITION  - 4 : Ability 4/4 (6 die)"
output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} >= 3 named "POSITION  - 3 : Ability 4/4 (6 die)"
output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} >= 2 named "POSITION  - 2 : Ability 4/4 (6 die)"


output 3d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1} + 3d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1} >= 2 named "CHALLENGE - 2 : Ability 5/5 (6 die)"
output 3d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1} + 3d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1} >= 3 named "CHALLENGE - 3 : Ability 5/5 (6 die)"
output 3d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1} + 3d{0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1} >= 4 named "CHALLENGE - 4 : Ability 5/5 (6 die)"

output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} >= 4 named "POSITION  - 4 : Ability 5/5 (6 die)"
output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} >= 3 named "POSITION  - 3 : Ability 5/5 (6 die)"
output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} >= 2 named "POSITION  - 2 : Ability 5/5 (6 die)"




output 3d{1, 1, 0, 0} + 3d{1, 1, 0, 0} = 0 named "CRITICAL SUCCESS - Ability 1/1 (6 die)"
output 3d{1, 1, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0} = 0 named "CRITICAL SUCCESS - Ability 2/2 (6 die)"
output 3d{1, 1, 0, 0, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0, 0, 0} = 0 named "CRITICAL SUCCESS - Ability 3/3 (6 die)"
output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 0 named "CRITICAL SUCCESS - Ability 4/4 (6 die)"
output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} + 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 0 named "CRITICAL SUCCESS - Ability 5/5 (6 die)"

output 3d{0, 0, 1, 1} + 3d{0, 0, 1, 1} = 0 named "CRITICAL FAILURE - Ability 1/1 (6 die)"
output 3d{0, 0, 1, 1, 1, 1} + 3d{0, 0, 1, 1, 1, 1} = 0 named "CRITICAL FAILURE - Ability 2/2 (6 die)"
output 3d{0, 0, 1, 1, 1, 1, 1, 1} + 3d{0, 0, 1, 1, 1, 1, 1, 1} = 0 named "CRITICAL FAILURE - Ability 3/3 (6 die)"
output 3d{0, 0, 1, 1, 1, 1, 1, 1, 1, 1} + 3d{0, 0, 1, 1, 1, 1, 1, 1, 1, 1} = 0 named "CRITICAL FAILURE - Ability 4/4 (6 die)"
output 3d{0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1} + 3d{0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1} = 0 named "CRITICAL FAILURE - Ability 5/5 (6 die)"
```

***








# Resistance Roll




### - Impact 2

|Ability Score |Trouble Score 0 |Trouble Score 1 |Trouble Score 2 |
|:------------:|:--------------:|:--------------:|:--------------:|
|1             |25%             |50%             |25%             |
|2             |44%             |44%             |12%             |
|3             |56%             |38%             |6%              |
|4             |64%             |32%             |4%              |
|5             |69%             |28%             |3%              |

```
output 2d{1, 1, 0, 0} = 0 named "IMPACT - 2: Trouble Score: 0 / Ability 1"
output 2d{1, 1, 0, 0} = 1 named "IMPACT - 2: Trouble Score: 1 / Ability 1"
output 2d{1, 1, 0, 0} = 2 named "IMPACT - 2: Trouble Score: 2 / Ability 1"

output 2d{1, 1, 0, 0, 0, 0} = 0 named "IMPACT - 2: Trouble Score: 0 / Ability 2"
output 2d{1, 1, 0, 0, 0, 0} = 1 named "IMPACT - 2: Trouble Score: 1 / Ability 2"
output 2d{1, 1, 0, 0, 0, 0} = 2 named "IMPACT - 2: Trouble Score: 2 / Ability 2"

output 2d{1, 1, 0, 0, 0, 0, 0, 0} = 0 named "IMPACT - 2: Trouble Score: 0 / Ability 3"
output 2d{1, 1, 0, 0, 0, 0, 0, 0} = 1 named "IMPACT - 2: Trouble Score: 1 / Ability 3"
output 2d{1, 1, 0, 0, 0, 0, 0, 0} = 2 named "IMPACT - 2: Trouble Score: 2 / Ability 3"

output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 0 named "IMPACT - 2: Trouble Score: 0 / Ability 4"
output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 1 named "IMPACT - 2: Trouble Score: 1 / Ability 4"
output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 2 named "IMPACT - 2: Trouble Score: 2 / Ability 4"

output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 0 named "IMPACT - 2: Trouble Score: 0 / Ability 5"
output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 1 named "IMPACT - 2: Trouble Score: 1 / Ability 5"
output 2d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 2 named "IMPACT - 2: Trouble Score: 2 / Ability 5"
```


### - Impact 3

|Ability Score |Trouble Score 0 |Trouble Score 1 |Trouble Score 2 |Trouble Score 3 |
|:------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|1             |12%             |38%             |38%             |12%             |
|2             |30%             |44%             |22%             |4%              |
|3             |42%             |42%             |14%             |2%              |
|4             |51%             |38%             |10%             |1%              |
|5             |58%             |34%             |7%              |1%              |

```
output 3d{1, 1, 0, 0} = 0 named "IMPACT - 3: Trouble Score: 0 / Ability 1"
output 3d{1, 1, 0, 0} = 1 named "IMPACT - 3: Trouble Score: 1 / Ability 1"
output 3d{1, 1, 0, 0} = 2 named "IMPACT - 3: Trouble Score: 2 / Ability 1"
output 3d{1, 1, 0, 0} = 3 named "IMPACT - 3: Trouble Score: 3 / Ability 1"

output 3d{1, 1, 0, 0, 0, 0} = 0 named "IMPACT - 3: Trouble Score: 0 / Ability 2"
output 3d{1, 1, 0, 0, 0, 0} = 1 named "IMPACT - 3: Trouble Score: 1 / Ability 2"
output 3d{1, 1, 0, 0, 0, 0} = 2 named "IMPACT - 3: Trouble Score: 2 / Ability 2"
output 3d{1, 1, 0, 0, 0, 0} = 3 named "IMPACT - 3: Trouble Score: 3 / Ability 2"

output 3d{1, 1, 0, 0, 0, 0, 0, 0} = 0 named "IMPACT - 3: Trouble Score: 0 / Ability 3"
output 3d{1, 1, 0, 0, 0, 0, 0, 0} = 1 named "IMPACT - 3: Trouble Score: 1 / Ability 3"
output 3d{1, 1, 0, 0, 0, 0, 0, 0} = 2 named "IMPACT - 3: Trouble Score: 2 / Ability 3"
output 3d{1, 1, 0, 0, 0, 0, 0, 0} = 3 named "IMPACT - 3: Trouble Score: 3 / Ability 3"

output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 0 named "IMPACT - 3: Trouble Score: 0 / Ability 4"
output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 1 named "IMPACT - 3: Trouble Score: 1 / Ability 4"
output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 2 named "IMPACT - 3: Trouble Score: 2 / Ability 4"
output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 3 named "IMPACT - 3: Trouble Score: 3 / Ability 4"

output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 0 named "IMPACT - 3: Trouble Score: 0 / Ability 5"
output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 1 named "IMPACT - 3: Trouble Score: 1 / Ability 5"
output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 2 named "IMPACT - 3: Trouble Score: 2 / Ability 5"
output 3d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 3 named "IMPACT - 3: Trouble Score: 3 / Ability 5"
```


### - Impact 4

|Ability Score |Trouble Score 0 |Trouble Score 1 |Trouble Score 2 |Trouble Score 3 |Trouble Score 4 |
|:------------:|:--------------:|:--------------:|:--------------:|:--------------:|:--------------:|
|1             |6%              |25%             |38%             |25%             |6%              |
|2             |20%             |39%             |30%             |10%             |1%              |
|3             |32%             |42%             |21%             |4%              |1%              |
|4             |41%             |41%             |15%             |2%              |1%              |
|5             |48%             |39%             |11%             |1%              |1%              |

```
output 4d{1, 1, 0, 0} = 0 named "IMPACT - 4: Trouble Score: 0 / Ability 1"
output 4d{1, 1, 0, 0} = 1 named "IMPACT - 4: Trouble Score: 1 / Ability 1"
output 4d{1, 1, 0, 0} = 2 named "IMPACT - 4: Trouble Score: 2 / Ability 1"
output 4d{1, 1, 0, 0} = 3 named "IMPACT - 4: Trouble Score: 3 / Ability 1"
output 4d{1, 1, 0, 0} = 4 named "IMPACT - 4: Trouble Score: 4 / Ability 1"

output 4d{1, 1, 0, 0, 0, 0} = 0 named "IMPACT - 4: Trouble Score: 0 / Ability 2"
output 4d{1, 1, 0, 0, 0, 0} = 1 named "IMPACT - 4: Trouble Score: 1 / Ability 2"
output 4d{1, 1, 0, 0, 0, 0} = 2 named "IMPACT - 4: Trouble Score: 2 / Ability 2"
output 4d{1, 1, 0, 0, 0, 0} = 3 named "IMPACT - 4: Trouble Score: 3 / Ability 2"
output 4d{1, 1, 0, 0, 0, 0} = 4 named "IMPACT - 4: Trouble Score: 4 / Ability 2"

output 4d{1, 1, 0, 0, 0, 0, 0, 0} = 0 named "IMPACT - 4: Trouble Score: 0 / Ability 3"
output 4d{1, 1, 0, 0, 0, 0, 0, 0} = 1 named "IMPACT - 4: Trouble Score: 1 / Ability 3"
output 4d{1, 1, 0, 0, 0, 0, 0, 0} = 2 named "IMPACT - 4: Trouble Score: 2 / Ability 3"
output 4d{1, 1, 0, 0, 0, 0, 0, 0} = 3 named "IMPACT - 4: Trouble Score: 3 / Ability 3"
output 4d{1, 1, 0, 0, 0, 0, 0, 0} = 4 named "IMPACT - 4: Trouble Score: 4 / Ability 3"

output 4d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 0 named "IMPACT - 4: Trouble Score: 0 / Ability 4"
output 4d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 1 named "IMPACT - 4: Trouble Score: 1 / Ability 4"
output 4d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 2 named "IMPACT - 4: Trouble Score: 2 / Ability 4"
output 4d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 3 named "IMPACT - 4: Trouble Score: 3 / Ability 4"
output 4d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0} = 4 named "IMPACT - 4: Trouble Score: 4 / Ability 4"

output 4d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 0 named "IMPACT - 4: Trouble Score: 0 / Ability 5"
output 4d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 1 named "IMPACT - 4: Trouble Score: 1 / Ability 5"
output 4d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 2 named "IMPACT - 4: Trouble Score: 2 / Ability 5"
output 4d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 3 named "IMPACT - 4: Trouble Score: 3 / Ability 5"
output 4d{1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0} = 4 named "IMPACT - 4: Trouble Score: 4 / Ability 5"
```

***








# Old-Legacy




## Cost-Benefits analysis


### - Emotions raised when Risk is matched and Daring Roll is performed

#### If Success Score < Challenge AND Risk Score < Risk

- If perform **Daring Roll** :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |Maybe *success* or *limited success* |
|+                      |Can earn **Ability XP** |
|-                      |Maybe *failure* |
|-                      |Maybe raise an **Emotion** |

- If give up :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |No *failure* |
|+                      |No **Emotion** raised |
|-                      |*Limited Failure* |
|-                      |Cannot earn **Ability XP** |

#### If Success Score < Challenge AND Risk Score > Risk

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |Maybe *limited success* |
|+                      |Can earn **Ability XP** |
|-                      |Raise an **Emotion** |

- If give up :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |No **Emotion** raised |
|-                      |*Failure* |
|-                      |Cannot earn **Ability XP** |

#### If Success Score > Challenge AND Risk Score < Risk

- If perform **Daring Roll** :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |Can earn **Ability XP** |
|-                      |Maybe *limited success* |
|-                      |Maybe raise an **Emotion** |

- If give up :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |No *limited Success* |
|+                      |No **Emotion** raised |
|-                      |Cannot earn **Ability XP** |

#### If Success Score > Challenge AND Risk Score > Risk

- If perform **Daring Roll** :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |Can earn **Ability XP** |
|-                      |Must raise an **Emotion** |

- If give up :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |No **Emotion** raised |
|-                      |Cannot earn **Ability XP** |


### - Emotions raised to avoid negative consequences

#### If Success Score < Challenge AND Risk Score < Risk

- If perform **Daring Roll** :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |Maybe succeed |
|+                      |Can earn **Ability XP** |
|-                      |Maybe *failure* with consequence |

- If give up :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |No *failure* with consequence |
|-                      |Cannot succeed |
|-                      |Cannot earn **Ability XP** |

#### If Success Score < Challenge AND Risk Score > Risk

- If perform **Daring Roll** :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |Maybe *Limited Success* with consequence |
|+                      |Can earn **Ability XP** |

- If give up :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|-                      |*Failure* with consequence |
|-                      |Cannot earn **Ability XP** |

#### If Success Score > Challenge AND Risk Score < Risk

- If perform **Daring Roll** :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |Can earn **Ability XP** |
|-                      |Maybe *Limited Success* with consequence |

- If give up :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |No *Limited Success* with consequence |
|-                      |Cannot earn **Ability XP** |

#### If Success Score > Challenge AND Risk Score > Risk

- If perform **Daring Roll** :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|+                      |Can earn **Ability XP** |

- If give up :

|Positives / Negatives  |Description |
|----------------------:|:-----------|
|-                      |Cannot earn **Ability XP** |
