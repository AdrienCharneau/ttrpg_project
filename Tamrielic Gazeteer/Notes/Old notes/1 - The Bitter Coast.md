```
 __ __| |             _ ) _)  |    |                  __|                    |   
    |     \    -_)    _ \  |   _|   _|   -_)   _|    (      _ \   _` | (_-<   _| 
   _|  _| _| \___|   ___/ _| \__| \__| \___| _|     \___| \___/ \__,_| ___/ \__|
```

***








# GEOGRAPHY




Text

***








# SETTLEMENTS




## Seyda Neen


### - Travelling Chart

|Location   |Foot      |Guar Caravan |Silt Strider Caravan |Boat          |
|:---------:|:--------:|:-----------:|:-------------------:|:------------:|
|Balmora    |          |             |3 hours              |              |
|Ebonheart  |          |             |not available        |              |
|Hla Oad    |          |             |not available        |              |
|Pelagiad   |          |             |not available        |not available |
|Vivec City |          |             |2 hours              |              |

>https://en.uesp.net/wiki/Morrowind:Transport


### - Arrille's Tradehouse

Text

*Include shop inventory table*

*Include tavern event/rumors table?* : Tavern Brawl

**Arrille** :

>Rat Infestation: The local innkeeper asks the players to help clear out a rat infestation in the basement of the inn. The rats have been stealing food supplies and causing trouble for the guests.


### - Census and Excise Office & Warehouse

Text

**Hrisskar Flat-Foot** :

**Processus Vitellius** :

**Sellus Gravius** :

**Socucius Ergalla** :


### - Lighthouse

Text

**Thavere Vedrano** :


### - Other

**Fargoth** :

**Teruise Girvayne** :

***








# PLACES OF INTEREST




## (Addamasartus Ash Hills?)

Text

***








# DUNGEONS




## Abaesen-Pulu Egg Mine

Text

***