>Alright, this is gonna be a huge ramble. I'm writing a lore-heavy module for a pen & paper rpg. It's based in the Bitter Coast region (during late 3rd Era) and, in terms of world building, the "Odai river question" really bothers me. I'm already overlooking the fact that all imperial towns and forts present in TES:3 were built and populated in less than 15 years, but the Odai river being this empty just doesn't make any sense.

>Balmora is a major settlement in Vvarednfell. It's also the location for the district's Hlaalu council seat. Which, checks notes, is a faction largely concerned with business and diplomacy. If we look at maps, the Odai river is literally the most direct way to reach the Inner Sea (and by extension Morrowind's mainland).

>Which begs the question: why aren't goods being transported in and out of the city through the river? We already know that imperials built Pelagiad in order to protect the route in the Ascadian Isles, so why not have a fortress (or at least an outpost) located up there next to the Odai plateau as well? I don't get it.

>inb4 "the land route is better". No it isn't. Silt Strider or guar caravans can't carry nearly as much as transport ships, and definitely not (again) directly to the mainlain.

>inb4 "it's too narrow". Maybe, but it doesn't look like it. And even if it was too narrow (in some places at least), it seems like the Empire and House Hlaalu have enough administrative resources and manpower (ever heard of slavery?) to dig out some of the river banks to make it work.

>inb4 "the Odai river IS being used for transportation. The game is just not a perfect representation of the actual lore". Ok, I can work with that. But in that case why is Hla Oad such a small and desolated town and why is it located so far off the estuary? Shouldn't it be a bustling transit hub located right at the mouth of the river with lots of commercial activity...?

>https://www.reddit.com/r/teslore/comments/15cm53t/why_is_the_odai_river_not_being_used_for/

>https://www.reddit.com/r/Morrowind/comments/kdsrg2/question_about_the_odai_river/

**READ THIS:**

>https://www.sciencedirect.com/science/article/abs/pii/S0305748899901913

**SOLUTION:**

*Braided river*, *Shallows*, *Riffles*...

>https://en.wikipedia.org/wiki/Shallow_(underwater_relief)

>https://en.wikipedia.org/wiki/Riffle

>https://cranetrust.org/who-we-are/what-we-do/conservation/ecological-overview/braided-river-ecology.html

>"Braided river valleys are often several miles wide"

>https://en.wikipedia.org/wiki/Platte_River





# BEST ANSWER


>why is it located so far off the estuary? Shouldn't it be a bustling transit hub located right at the mouth of the river with lots of commercial activity...?

Flooding? There is a reason why Rome and Athens both had harbors outside the city.

I actually struggle to think of a city that sat in the corner of a river and the coast.

+ do not forget how old Tamrielic settlements often are, even if they once were on the coast, with sediments being washed out into the ocean by the river the coast would have moved over time.




# ESO

OP, check out the town of Balmora in ESO. Skiffs and cargo boats flank the docks along the main waterway, which means that the Odai was most certainly a route for commercial transportation.