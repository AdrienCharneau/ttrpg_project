```
   __|   _ \  _ \  __|     \  |  __|   __|  |  |    \     \ | _ _|   __|   __|
  (     (   |   /  _|     |\/ |  _|   (     __ |   _ \   .  |   |   (    \__ \
 \___| \___/ _|_\ ___|   _|  _| ___| \___| _| _| _/  _\ _|\_| ___| \___| ____/
```

***








# **Ability Rolls**

***




**Ability Rolls** are a simple way to test a character's **Abilities**. This is when the GM needs to resolve an outcome where a character assumes a passive position, where they aren't actively resisting something, or aren't carrying out a deliberate action. For example, an **Ability Roll** might be used to see if a character notices something unusual in their surroundings, or manages to recall a specific piece of information. But not if they attempt to climb a wall or dodge an attack.

>*Ability Rolls are typically made by players at the GM's request. However, the GM may sometimes roll in secret to keep players unaware.*




## Steps


### - Choosing an Ability

When an **Ability Roll** is called, the GM must first pick an **Ability** for it. Here are some examples:

- **Dexterity** : for determining if a character hears something unusual or spots a hidden object.

<!-- Charm : for determining an NPC's first impression of the character. -->

- **Knowledge** : for determining if a character recognizes an item, symbol, spell or anything that requires education or expertise.

<!-- Reasoning : for orientating oneself or finding direction in unknown territory. -->

- **Insight** : for figuring out a person and/or their ulterior motives in a conversation.

etc...

>*Depending on its score, the Ability determines which type of die is rolled.*


### - Resolution

The player (or GM) rolls the die. Any result of 3 or more is considered a *success*, a result of 1 or 2 is considered a *failure*.




## Example


Finn is observing a fortress from a safe distance, planning their next move. They notice movement on the walls. The player controlling the character asks if they can see anything:

- The GM tells the player to roll a *d6* (based on Finn's *dexterity* of 2), as they need to test Finn's eyesight.

- They get a result of 5, which counts as a *success*. The GM says that Finn notices five guards: three patrolling the walls and two standing on a tower.

***








# **Action Rolls**

***




An **Action Roll** is a roll where a character takes a deliberate action, initiating a specific course of events or using their **Abilities** to achieve something. The GM should only require this type of roll if something is at stake, or there's a possibility for a bad outcome<!-- (no matter how small it is)-->. It shouldn't be required for routine or very simple tasks either.

<!-- Many roleplay games have the concept of "failing forward". This means that every roll has some consequence which is usually narrated by the GM and usually bad for the character or the party as a whole. A roll without possible negative consequences should not be made.

Every moment of play, roll dice or say “yes.”

If nothing is at stake, say “yes” [to the player’s request], whatever they’re doing. Just go along with them. If they ask for information, give it to them. If they have their characters go somewhere, they’re there. If they want it, it’s theirs.

Sooner or later—sooner, because [your game’s] pregnant with crisis— they’ ll have their characters do something that someone else won’ t like. Bang! Something’s at stake. Start the confl ict and roll the dice.

Roll dice, or say “yes.” -->




## Steps


### - Declaring the Action

To perform an **Action Roll**, the player describes their objective and states the means by which they plan to achieve it. When necessary, it's the GM's job to ask and clarify this step, to ensure a clear understanding of the character's intentions and methods.

>*For example, while both "successfully casting a spell" and "successfully casting a spell to impress a powerful sorcerer" may seem like identical tasks, their underlying intentions and expected outcomes differ significantly.*


### - Assessing Challenge

Set by the GM, **Challenge** gauges the amount of effort or skill required to succeed the roll:

|Challenge |Rating |Example |
|:--------:|:-----:|:-------|
|Average   |2      |The action is straightforward but requires some level of effort or skill. Examples include sneaking past a distracted guard, performing a basic first aid procedure or walking across a sturdy plank of wood. |
|Difficult |3      |The action is tough and requires considerable levels of effort or skill. Examples include deciphering a complex code, climbing a steep wall or playing a convoluted musical piece on the piano. |
|Daunting  |4      |The action is excessively hard and requires exceptional levels of effort or skill. Examples include sneaking undetected past a heavily guarded perimeter, performing an advanced surgical procedure or walking on a tightrope. |

Depending on context, the GM may use these parameters as a guideline for setting a **Challenge**:

- **Environment** : the physical or social space in which the action takes place.

- **Actors** : is the action being helped or opposed by other people.

- **Means** : the resources, items, tools or pieces of knowledge available.

- **Condition** : the general state of the character performing the action, including their physical and mental health.

<!-- By default, all Action Rolls have a Challenge of 3. The GM may also hide this value from players if they feel like it fits the situation. -->

<!-- They may even go as far as to ask for a simple Insight or Knowledge Roll/ability check to reveal it. -->


### - Assessing Risk

Also set by the GM, **Risk** indicates potential danger, uncertainty or threat associated with the action. This value represents the likelihood of a troublesome outcome; with the greater the likelihood, the lower the value:

|Risk       |Rating |Example |
|:---------:|:-----:|:-------|
|Safe       |4      |The action presents minimal danger, granting the character a good opportunity to act. Examples include shooting atop a fortification through a murder hole, striking a crippled adversary or breaking into an unguarded warehouse. |
|Risky      |3      |The action is hazardous or involves potential harm, forcing the character to gamble their outcome. Examples include handling volatile explosives, attempting to traverse a crumbling bridge or trying to deceive a guard to infiltrate a restricted area. |
|Precarious |2      |The action hangs on a knife's edge, with the character facing imminent trouble. Examples include confronting a powerful enemy head-on, running accross a booby trapped minefield or trying to rescue hostages from a burning building.|

<!-- The examples written here have been copy-pasted from Blades in the Dark. -->

<!-- By default, all Action Rolls have a Risk of 3. The GM may also hide this value from players if they feel like it fits the situation. -->

Depending on the circumstances, **Risk** can be placed into one of four categories:

- **Narrative Risk** : plot-related consequences, involving complications or setbacks that impact the adventure or current situation <!-- some of which could induce Stress on the character..? -->.

- **Physical Risk** : consequences related to the physical well-being of the character, including loss of **Health** caused by injuries or disease.

- **Material Risk** : consequences related to possessions or resources, including the loss or damage of items, equipment or valuables.

- **Social Risk** : consequences pertaining to social rank, status and interactions, such as damaged reputation or loss of *respect*/*sympathy* from other characters.

<!-- Time Risk : consequences related to time, including missing a deadline or falling prey to the night. -->

<!-- The GM should never inflict a consequence that completelly negates the success of an action. -->

<!-- Don't inflict a complication that negates a successful roll. If a PC tries to corner an enemy and gets a 4/5, don't say that the enemy escapes. The player's roll succeeded, so the enemy is cornered... maybe the PC has to wrestle them into position and during the scuffle the enemy grabs their gun. -->


### - Choosing Abilitie Die

**Action Rolls** always combine two **Abilities**, based on the specifics of the action. For example, *agility* and *endurance* may be relevant for a climb roll, while *reasoning* and *creativity* may be relevant for a problem-solving roll. Here are a few other examples:

- **Brewing a potion** may require *knowledge* and *reasoning*, as it involves understanding the properties of various ingredients, as well as the proper techniques for preparing and mixing them.

- **Writing a poem** may require *eloquence* and *creativity*, as it involves using language and expression in imaginative ways to convey emotions and ideas.

- **Swimming** may require *agility* and *endurance*, as it involves the ability to move efficiently through water and sustain prolonged physical effort.

- **Painting** may require *creativity* and *dexterity*, as it involves the ability to express oneself through artistic mediums, and the manual skill to execute the desired strokes and forms.

- **Solving a puzzle** may require *reasoning* and *creativity*, as it involves analyzing available information and coming up with a unique solution.

- **Lockpicking** may require *dexterity* and *reasoning*, as it involves manipulating intricate mechanisms with precision, while also analyzing components and identifying potential vulnerabilities.

<!-- Walking across a wooden plank may require agility and composure, as it involves maintaining balance and control while navigating a narrow, precarious path. -->

etc...

>*Depending on their scores, these Abilities determine which type of die are rolled, with 2 die used for each Ability (4 in total). For instance, if a character wants to swim accross a river, and they have an endurance score of 3 and an agility score of 1, they pick two d8s and two d4s. If they want to solve a puzzle, and they have a reasoning score of 2 and a creativity score of 2, they pick four d6s etc...*


### - Resolution

Resolving an **Action Roll** involves two steps: a standard roll followed by an optional **Daring Roll**, with players trying to maximize **Success Die** while also minimizing **Trouble Die**:

- The player throws their 4 die.

- Results of 4 or more are considered **Success Die**. Results of 3 are ignored and results of 1 and 2 are considered **Trouble Die**.

- The player can choose to end their action after the first throw, or proceed with a **Daring Roll**.

- Opting for the latter raises one of the character's **Emotion** by 1, and allows them to roll 2 additional die (one for each **Ability**), aiming to gain more **Success Die** while also risking additional **Trouble Die**.

>*After a Daring Roll, a player should have rolled 6 die in total. If not, they should have rolled only 4 die.*

<!-- Alternative : Opting for the latter raises one of the character's Emotion by an amount dependent on the roll's Challenge (Average = 1, Difficult = 2, Overwhelming = 3) -->

#### Raising Emotion

As stated above, a character must raise an **Emotion** by one stage when attempting a **Daring Roll**. Which **Emotion** to raise is determined by context and the character's general mindset while performing the action:

|Emotion     |Description |
|:----------:|:-----------|
|Anger       |The character is acting out of resentment or animosity. They feel wronged or threatened, and think that they can overcome the obstacle through assertiveness or confrontation. |
|Frustration |The character is acting out of annoyance or irritation. Things aren't turning out as expected, they're dealing with an unforeseen obstacle or an unwanted task. |
|Anxiety     |The character is acting with uncertainty or apprehension. They are plagued by fear, second-guessing their decision or perceiving themselves as too weak for the task. |
|Guilt       |The character is acting against their own values or principles. They feel responsible for what they're doing or believe that they shouldn't be doing it in the first place. |

<!-- Once raised, Emotions cannot be lowered. But they can be cleared entirely if the character manages to fulfil (or at least significantly pursue) their current Quest. Or if they choose to leave the group (for a full day at least) while the plot progresses without them. -->


### - Outcomes

Action outcomes depend on whether the number of **Success Die** matched or exceeded the action's **Challenge**, and/or if the number of **Trouble Die** matched or exceeded the action's **Risk**:

|                        |Success Die >= Challenge |Success Die < Challenge |
|:-----------------------|:------------------------|:-----------------------|
|**Trouble Die >= Risk** |Limited Success          |Failure                 |
|**Trouble Die < Risk**  |Success                  |Limited Failure         |

For example, a character is trying to steal a guarded item:

- **Success** : the character succeeds, they achieve their intended goal or result without consequences.

>*The character manages to steal the item and remains undetected.*

- **Limited Success** : the character succeeds but it comes at a cost. There's a setback, a complication or the action isn’t as effective as anticipated.

>*The character manages to steal the item but gets detected by guards.*

- **Limited Failure** : the character fails but there's no impactful consequence. The GM may allow them to try again (after some time, or with increased **Risk**) or decide that the opportunity slips away.

>*The character fails at stealing the item but remains undetected.*

- **Failure** : the character fails and there's a consequence. A new threat appears, they lose control of the situation or suffer some form of harm.

>*The character fails at stealing the item and gets detected by guards.*

#### Critical Outcomes

- A **Critical Success** happens when a player manages to roll 2 **Success Die** over the action's **Challenge**. This counts as a *success* with extra bonus or advantage.

- A **Critical Failure** happens when a player scores 2 **Trouble Die** over the action's **Risk**. This counts as a *failure* with a more severe consequence.




## Example


Finn attempts to pick the lock of a chest to steal valuable items. The GM decides that the action requires *dexterity* and *reasoning*, with a **Challenge** of 2 (it's an *average* lock) and a **Risk** of 3 (guards are patrolling the area, it's a *risky* move).

- The player rolls two *d6s* and two *d4s* (based on Finn's *dexterity* of 2 and *reasoning* of 1).

- They get results of 4, 2, 3 and 2. 4 counts as one **Success Die**, both 2s count as two **Trouble Die** and 3 is ignored.

<!-- Briefly introduce Modifiers here...? -->

- Finn has one **Success Die** (not enough to succeed) and two **Trouble Die** (not enough to alert the guards), thus the player can choose to end on a *limited failure*. But instead they decide to push their luck with a **Daring Roll**.

- After raising their *anxiety* by 1 (Finn is scared of being caught), they roll another two die (one *d6* and one *d4*) and get results of 5 and 1 (one more **Success Die** and one more **Trouble Die**).

- Since the total number of **Success Die** (2) matched the **Challenge** and the total number of **Trouble Die** (3) also matched the **Risk**, the action ends in a *limited success*. The GM tells the player that Finn succeeded at picking the lock, but a guard heard them and is now walking towards their location.




<!-- ## Resisting Consequences


**Resistance Rolls** can be used to cancel out negative consequences resulting from *limited successes* and *failures* (converting them into *successes* and *limited failures*, respectively). If so, the character uses the **Ability** corresponding to the **Emotion** used (or hypothetically used) for their **Daring Roll**, and **Pressure** is set by the GM depending on how strong the consequence is:

|Daring Roll's Emotion |Resistance Roll's Ability |
|:--------------------:|:------------------------:|
|Anger                 |Humility                  |
|Frustration           |Insight                   |
|Anxiety               |Composure                 |
|Guilt                 |Composure                 |

>*As usual, the GM has the discretion to determine whether a consequence can be resisted or not.*

#### Example

Let's return to the previous situation. Finn achieved a *limited success* while picking a lock, inadvertently alerting a nearby guard. The GM tells the player that they may perform a **Resistance Roll** against a **Pressure** of 2 (the guard is only checking on some noise) using Finn's *composure* (Finn is scared of being caught). The player accepts:

- Finn's *composure* score is 1, so they roll two d4s.

- They get results of 4 and 3. Finn's *limited success* is upgraded to a *success* and, because there's no **Trouble Die**, there's no increase in *anxiety* either. The GM tells the player that Finn successfully picked the lock and did not alert a single guard. -->




## Advantage & Disadvantage


Sometimes, rules may specify that a character must perform an **Action Roll** using **Advantage** or **Disadvantage**. If a character rolls with **Advantage**, results of 3 now count as **Success Die**. If a character rolls with **Disadvantage**, results of 3 now count as **Trouble Die**.

<!-- Once per day, if a character is not in a Breakdown state, they may add 1 Stress to roll with Advantage on any Action roll thay want (or turn a roll with Disadvantage into a neutral roll). This is to show that characters are able to "force motivate" themselves into doing things that are not always aligned with their goals. -->

#### Drive Actions

For example, characters roll with **Advantage** if they perform an **Action Roll** aligned with at least one of their **Drives**:

- **Authority** : when addressing an obstacle through confrontation or intimidation.

- **Revenge** : when attempting vindication or retaliation against a perpetrator.

- **Wealth** : when tackling a challenge where money or valuables are at stake.

- **Hedonism** : when engaging in a gratifying experience or trying to fulfil a desire.

- **Distinction** : when trying to stand out while addressing an obstacle.

- **Influence** : when trying to solve a problem with persuasive communication or manipulation.

<!-- - **Privilege** : when leveraging their status, connections or when receiving exclusive benefits. -->

- **Solution** : when addressing an obstacle with rationale, technical skill or strategic thinking.

- **Production** : when creating things, like building a contraption or designing a piece of art.

- **Curiosity** : when venturing into the unknown or trying to gain insight into a specific subject.

- **Care** : when providing assistance or support for those in need.

- **Peace** : when mediating disputes or trying to resolve conflicts through reconciliation.

- **Duty** : when carrying out responsibilities to their family, gods or companions.

<!-- This move also gives **XP** to the character...? -->

On the other hand, characters roll with **Disadvantage** if their action goes against at least one of their **Drives** (or current **Quest**).

<!-- or when going against an **Emotion** causing a **Breakdown** state as well? -->

<!-- ### - Example ? -->


<!-- ### - Example ? -->

<!-- Blades in the Dark Playbooks:

- When you play a Cutter, you earn xp when you address a challenge with violence or coercion. Go ahead and get up in everyone’s smug faces and tell them who’s boss, then let your blades do the talking if they don’t get the message.

- When you play a Hound, you earn xp when you address a challenge with tracking or violence. Take the initiative to hunt down opportunities and targets for a score and be willing to hurt whoever stands in your way.

- When you play a Leech, you earn xp when you address a challenge with technical skill or mayhem. Duskwall is a city full of industrial machinery, clockworks, plumbing, and electrical systems for you to bend to your purposes or sabotage. Get out your tools and get your hands dirty.

- When you play a Lurk, you earn xp when you address a challenge with stealth or evasion. Stay out of sight, sneak past your enemies, and strike from the shadows. If things go wrong, there's no shame in disappearing into the darkness... for now. Your greatest ally is the dark and twisting city, its ink-dark streets, its rooftop pathways.

- When you play a Slide, you earn xp when you address a challenge with deception or influence. Talk your way into trouble, then talk your way out again. Pretend to be someone you're not. Bluff, lie, and manipulate to get your way. Every problem is ultimately a problem because of people—and people are the instrument upon which you play your music.

- When you play a Spider, you earn xp when you address a challenge with calculation or conspiracy. Reach out to your contacts, friends, and associates to set your crew up for success. Use your downtime activities and flashbacks wisely to prepare for trouble and to calculate the angles of success. When things go wrong, don't panic, and remember: you planned for this.

- When you play a Whisper, you earn xp when you address a challenge with knowledge or arcane power. Seek out the strange and dark forces and bend them to your will. By being willing to face the trauma from the stress-intensive abilities of the arcane, you'll slowly remove parts of yourself, and replace them with power. -->

***








# **Progress Clocks**

***




A **Progress Clock** is a gauge (or circle) divided into sections used to trace almost anything. This could be plot progression, time management, NPC health or any other variable essential to the unfolding of the narrative. It can be compared to a "progress bar" found in video games, but adapted to suit a variety of situations.

>*Progress Clocks transform narrative outcomes into something tangible that players can grasp, whether it's overcoming an enemy, escaping danger or fulfilling an objective.*

The greater the **Progress Clock**, the more sections it holds:

|Progress Clock |Sections |
|:-------------:|:-------:|
|Standard       |2        |
|Large          |4        |
|Huge           |6        |
|Mighty         |8        |

A **Progress Clock** should always reflect what's happening in the fiction, allowing players to check how they’re doing. For example:

- **Event Clocks** are used to track the progression of time. They can represent the countdown to an event, the arrival of reinforcements or the duration of a spell. Each segment should mark a specific time interval, enabling players to anticipate future developments or prepare accordingly.

- **Obstacle Clocks** are used to track ongoing effort against a challenge. Characters tick segments when succeeding **Action Rolls** aiming at overcoming the challenge, until the clock is filled and the obstacle is no more. Unless it's a *critical success*, a single action cannot fill more than one segment at a time.

<!-- When you create a clock, make it about the obstacle, not the method. The clocks for an infiltration should be “Interior Patrols” and “The Tower,” not “Sneak Past the Guards” or “Climb the Tower.” The patrols and the tower are the obstacles— the PCs can attempt to overcome them in a variety of ways. -->

- **Threat Clocks** are used to monitor impending trouble or danger. They can symbolize growing suspicion, the proximity of pursuers or the alert level of guards. Segments are ticked when complications arise or characters mess up, like when an action ends on a *limited success* or a *failure* (with *critical failures* ticking more than one segment at a time).

- **Combat Clocks** are used to track the progression of combat encounters. They can represent a number of enemies, their morale, their health or even their armor. Segments  are ticked when characters perform successful attacks on their adversaries and, unless there's a *critical success*, a single attack cannot fill more than one segment at a time.

- **Influence Clocks** are used to represent the effort required to sway an opinion, negotiate a deal or achieve an objective through persuasion, intimidation or manipulation. Segments can be ticked based on the narrative, or through the success of social actions aimed at influencing the target.

- **Project Clocks** are used to monitor the advancement of a long-term task or endeavor. This could be building a stronghold, learning a skill, writting a book or crafting an artifact. Segments should represent the various stages of the project, with each tick marking a significant milestone towards completion.

>*Progress Clocks are not required for every situation. The GM should use them for challenges where tracking advancement is essential. Otherwise, they should opt for resolving outcomes with a single Action Roll.*

<!-- Faction Clock : each faction has a long-term goal. When the PCs have downtime, the GM ticks forward the faction clocks that they’re interested in. In this way, the world around the PCs is dynamic and things happen that they’re not directly connected to, changing the overall situation in the city and creating new opportunities and challenges. The PCs may also directly affect NPC faction clocks, based on the missions and scores they pull off. Discuss known faction projects that they might aid or interfere with, and also consider how a PC operation might affect the NPC clocks, whether the players intended it or not. -->

<!-- Spellcasting Clock -->

<!-- When you create a clock, make it about the obstacle, not the method. The clocks for an infiltration should be “Interior Patrols” and “The Tower,” not “Sneak Past the Guards” or “Climb the Tower.” The patrols and the tower are the obstacles —the PCs can attempt to overcome them in a variety of ways. -->

<!-- https://www.reddit.com/r/bladesinthedark/comments/u2na85/help_with_understanding_progress_clocks -->

<!-- https://www.reddit.com/r/rpg/comments/bft68t/using_clocks_world_bitd_in_any_game/ -->

***








# **Resistance Rolls**

***




**Resistance Rolls** represent a character's attempt at withstanding undesirable events, rather than them actively initiating a course of action. Players may call for one anytime they want to negate something affecting their character; with the roll always succeeding provided the character isn't in a **Breakdown** state. For instance, if a character is struck by an enemy, they may perform a **Resistance Roll** to dodge the attack.

>*However, the decision on what can or can't be resisted is left at the GM's discretion. And they reserve the right to deny a player's request if deemed inappropriate. The GM may also suggest a player to perform a Resistance Roll at any point during a game, but cannot enforce it.*

<!-- This is when a character assumes a reactive position against a situation or circumstance. -->

<!-- Players always drive the action, making all the dice rolls. -->


### - Choosing an Ability

If a **Resistance Roll** is initiated, the GM must first pick an appropriate ability for it. Here are some examples:

- **Dexterity** : when reacting instinctively to danger.

- **Agility** : when dodging incoming threats or attacks.

- **Knowledge** : when refuting attempts at persuasion based on misinformation or quackery.

- **Insight** : when refuting attempts at persuasion based on trickery or lies.

- **Composure** : when refuting temptation or seduction.

- **Endurance** : when resisting complications arising from wounds or diseases.

etc...

>*Depending on its score, the Ability determines which type of die is rolled.*


### - Assessing Pressure

**Pressure** measures how much strain is being exerted on the character. It's a representation of the level of effort or resolve needed to resist the event:

|Pressure |Rating |Example |
|:-------:|:-----:|:-------|
|Moderate |1-2    |The burden on the character is manageable. |
|Severe   |3-4    |The burden significantly challenges the character's capabilities. |
|Extreme  |5-6    |The burden is overwhelming and pushes the character to their limits. |

>*The GM sets this value, which in turn determines the number of die rolled by the player.*


### - Resolution

The player rolls a number of **Ability** die equal to the roll's **Pressure**, with results of 1 and 2 counting as **Trouble Die**. Subsequently, the character raises one of their **Emotions** by the amount of **Trouble Die** obtained, and the event or consequence is negated.

<!-- Talk about Modifiers here...? -->

<!-- Anticipation/Alertness : if their character isn't in a Breakdown state, a player can choose to "predict" a future negative event. If so, they may raise one of their Emotions by 1 and, if the "predicted" event happens, their character succesfully resists it without having to perform a Resistance Roll. -->

<!-- When your PC suffers a consequence that you don’t like, you can choose to resist it. Just tell the GM, “No, I don’t think so. I’m resisting that.” Resistance is always automatically effective—the GM will tell you if the consequence is reduced in severity or if you avoid it entirely. Then, you’ll make a resistance roll to see how much stress your character suffers as a result of their resistance. -->

<!-- ### - Example -->

<!-- Spire - The City must Fall: -->

<!-- Stunned (Blood) : You take a blow to the head, or are winded, giving your enemies opportunity to act. If they want to get away from you, they can do so while you stagger about and gather your senses; otherwise, you can’t use the Fight skill to earn additional dice for the remainder of the situation. -->

<!-- Compromised (Shadow) : A friendly NPC asks you to justify your strange behaviour. -->

<!-- Pawned (Silver) : Until the end of the next session, you lose the use of one piece of equipment that’s important to you. -->

<!-- Knocked Out (Blood) : You fall unconscious for several hours, during which time your enemies get an advantage. -->

<!-- Leak (Moderate) : Your bond unwittingly gives out information that threatens the operation. Mark D6 stress in Shadow. -->

<!-- Passive Rolls (or "Ability Checks") work like Resistance Rolls, except they don't incur negative outcomes for the character if failed, and the character can't "negate" their failure by raising their Emotional Strain. -->

<!-- Insight : used when sensing ulterior motives or hidden agendas in people (without actively engaging in conversation). -->

#### Raising Emotion

As stated above, characters must raise an **Emotion** when obtaining **Trouble Die**. Which **Emotion** to raise depends on context, and the character's general mindset at the time of the roll:

|Emotion     |Description |
|:----------:|:-----------|
|Anger       |The character feels personally wronged or threatened by the event. |
|Frustration |The character feels irritated by the unexpected or displeasing event. |
|Anxiety     |The character feels vulnerable or in danger. |
|Guilt       |The character feels somewhat responsible for the event affecting them. |

<!-- Once raised, Emotions cannot be lowered. But they can be cleared entirely if the character manages to fulfil (or at least significantly pursue) their current Quest. Or if they choose to leave the group (for a full day at least) while the plot progresses without them. -->

<!-- provide example(s)? -->

<!-- Anger : attributing the cause of harm to someone else and believing they can still influence the situation through assertiveness or confrontation -->

<!-- Frustration : the character feels irritated due to unexpected events, unwanted tasks, or outcomes that didn't go their way. Their response is usually one of annoyance or bitterness. -->

<!-- Insecurity : the character perceives themselves as inadequate, doubting their own abilities due to underestimating an obstacle or feeling embarrassed in a social situation. -->

<!-- Falling prey to a thief : a situation where a minor theft occurs, involving the stealing of coins from a character. While somewhat inconsequential, it can still generate a sense of distress and insecurity to the character. -->

<!-- Social embarrassment : a character experiences a situation where they are publicly embarrassed or humiliated, such as being the target of a practical joke in front of important figures. This can lead to a significant damage to their self-esteem. -->

<!-- Fear : the character senses danger, perceiving it as insurmountable or mysterious. They feel like they can't deal with it or comprehend its origin or nature. -->

<!-- Guilt : the character feels responsible for their own actions, acknowledging wrongdoing or believing that the current situation arose from their own choices. -->

<!-- Spire - The City Must Fall : Sometimes it will be stated outright what kind of stress a situation does out – for example, when a Lajhan casts a spell, most often they’ll be asked to mark stress against Mind as they channel the vast energies of their goddess. If it’s not clear where stress would go, the GM and the player can work it out together. -->

<!-- Example of Anger : a situation where a character perceives like they are being treated unfairly or if they get robbed of their belongings while walking in a city. -->

<!-- Example of Insecurity : being anti-theist and participating in a wedding where religious people are having lots of fun. People not giving you due attention or respect etc... -->

<!-- Anger : the character is convinced someone or something else is responsible for the stress increase and feels like they can still influence the situation or cope with it. -->
<!-- Shame : the stress increase was caused by an event that was initiated by the character's own choices and feels like they cannot influence the situation or cope with it. -->
<!-- Anxiety : the character is convinced someone or something else is responsible for the stress increase and feels like they cannot influence the situation or cope with it. -->
<!-- Guilt : the stress increase was caused by an event that was initiated by the character's own choices and feels like they can still influence the situation or cope with it. -->




## Example


Finn wants to evade an attack performed by a rival character:

- Considering Finn's animosity towards the character, the GM states that this may raise Finn's *anger*, and test his *agility* against a **Pressure** of 2.

- Finn has an agility score of 3, so the player rolls two *d8s*.

- They get results of 4 and 2, with 2 counting as one **Trouble Die**. Finn's *anger* is increased by 1 and the attack is succesfully dodged.

***








# **Breakdown State**

***




Once a character's **Emotion** reaches its maximum threshold, they enter what is called a **Breakdown** state. They are now subjected to a specific set of rules, cannot perform **Resistance Rolls**, and none of their **Emotions** can be raised.

<!-- Smallville - Stressing Out (Corebook p.60) : Stressing Out can mean many things. It may simply mean you fall unconscious. It may mean you collapse in doubt and angst. It may mean you stalk out of the room before you hurt someone. It may mean you stare out a window plotting revenge. When you Stress Out, it’s up to you to decide what it means in the story. Whatever the details, though, you’re out for the rest of the scene. -->

<!-- Stress/Panic table mechanic found in Alien RPG: https://www.youtube.com/watch?v=H16lBiBSd-I -->
<!-- Check DATA/Resources/GAMEPLAY/TTRPG inspirations/[TO READ]/Year Zero Engine/ -->
<!-- https://online.anyflip.com/aeoqf/frgt/mobile/index.html -->
<!-- https://www.drivethrurpg.com/product/293976/ALIEN-RPG-Core-Rulebook?cPath=27806_34036 -->
<!-- https://www.legrog.org/jeux/alien -->

#### Action Rolls

Each time the player opts for a **Daring Roll**, instead of raising an **Emotion**, they must increase their character's **Stress** by 1. Additionally, they roll with **Disadvantage** on actions related to the **Emotion** that triggered the **Breakdown**:

|Emotion     |Description |
|:----------:|:-----------|
|Anger       |The character rolls with **Disadvantage** if they're trying to help, comfort or support someone. |
|Frustration |The character rolls with **Disadvantage** if they're trying to problem-solve or focus on intellectual tasks. |
|Anxiety     |The character rolls with **Disadvantage** if they're trying to directly engage an obstacle or threat. |
|Guilt       |The character rolls with **Disadvantage** if they're trying to hurt or provoke someone. |

<!-- Characters suffering a Breakdown also make things difficult for everyone else around them. This translates by worsening the Environment by 1 of all actions performed by allies nearby? -->

#### Struggle Rolls

If the player chooses to resist an  event or circumstance, they must perform a **Struggle Roll** instead of a **Resistance Roll**. This also involves picking an **Ability** and a **Pressure** rating, but only negates the event if no **Trouble Die** are rolled.

>*If the player rolls at least one Trouble Die, the Struggle Roll fails, the event or circumstance still occurs, and the character's Stress increases by the total number of Trouble Die rolled.*

<!-- Provide Example -->

#### Fallout

In a **Breakdown** state, if a character's **Stress** reaches or overcomes their current **Health**, they are taken out of action. They cannot continue as an adventurer and must retire to a different life.

<!-- Direct Stress Damage? -->

<!-- Heavy material loss : a character experiences a significant loss of possessions or resources that hold personal value or importance to them. This can include strong financial setbacks, damage to important tools or loss of cherished belongings. -->

<!-- Betrayal of trust : a close friend or ally betrays a character's trust, either by revealing sensitive information or turning against them for personal gain. This can be emotionally devastating, causing the character to question their judgment and potentially leading to a loss of friendship. -->

<!-- Death of a loved one : the loss of a close friend or parent has a profound impact on the character's emotional well-being. Grief and sadness can accumulate significant permanent stress, potentially affecting the character's ability to function optimally or make rational decisions. -->




## Overcoming a Breakdown


To remove their **Breakdown** state, a character must address the **Emotion** that triggered it and do something impactful with it. Whatever that is should affect the course of the story:

|Emotion     |Action description |
|:----------:|:-----------|
|Anger       |The character must hurt someone or break something important. |
|Frustration |The character must fling themselves into cheap relief, or take a foolhardy action that puts the group in a difficult situation. |
|Anxiety     |The character must give up an important task or desert the group at a critical moment. |
|Guilt       |The character must make an important sacrifice to redeem themselves. |

>*What is considered impactful is left to the GM's discretion.<!-- If this action requires performing an Action Roll, the character also rolls with Advantage.-->*

Alternatively, a **Breakdown** state can also be removed if the character manages to advance their current **Quest**, or takes a temporary leave from the group (for a full day at least, while the plot progresses without them).

>*Upon removing a Breakdown, all of the character's Emotions are set back to 0. And any steps described here can be repeated if the Character isn't in a Breakdown state, for the sole purpose of clearing out Emotions.*

<!--  -->

<!-- Characters should also be able to recover (1? 2?) Emotional Strain twice a day while eating or resting. -->

<!-- Characters should be able to recover Emotional Strain in downtime or by bonding with each other? The amount of emotional recovery should be dependent on the overall Empathy score for the group? (+Emotional Breakdowns should reduce the recovery rate -characters with a low Empathy score should even increase Emotional stress during downtime?) -->

***
