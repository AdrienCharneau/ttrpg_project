# MATHEMATICS




Base the different schools or skills of magic on the different branches of mathematics

- Arithmetic
- Algebra
- Geometry
- Trigonometry
- Calculus
- Statistics
- Probabilities
- ...

>https://en.wikipedia.org/wiki/Mathematics#Areas_of_mathematics

>https://en.wikipedia.org/wiki/Names_of_large_numbers

>https://www.vedantu.com/maths/branches-of-mathematics

[YouTube - This is the Calculus They Won't Teach You](https://www.youtube.com/watch?v=5M2RWtD4EzI)

[An Ancient Roman Shipwreck May Explain the Universe](https://youtu.be/o0A9M5wHBA4?si=BBYV7QAEVr5rvMgu&t=616)

***








# PHYSICS




## Classical Elements


The classical elements typically refer to *earth*, *water*, *air*, *fire*, and (later) *aether* which were proposed to explain the nature and complexity of all matter in terms of simpler substances. Ancient cultures in Greece, Tibet, and India had similar lists which sometimes referred, in local languages, to "*air*" as "*wind*" and the fifth element as "*void*".

In Greece, the classical elements were first proposed independently by several early Presocratic philosophers. The Sicilian Greek philosopher Empedocles (c450 BC) proved (at least to his satisfaction) that air was a separate substance by observing that a bucket inverted in water did not become filled with water, a pocket of air remaining trapped inside. Heraclitus championed fire, Thales supported water, and Anaximenes favored air. Empedocles was the first to propose the four classical elements as a set: fire, earth, air, and water. He called them the four "roots" (ῥιζώματα, rhizōmata).

>Western astrology uses the four classical elements in connection with astrological charts and horoscopes. The twelve signs of the zodiac are divided into the four elements: Fire signs are Aries, Leo and Sagittarius, Earth signs are Taurus, Virgo and Capricorn, Air signs are Gemini, Libra and Aquarius, and Water signs are Cancer, Scorpio, and Pisces.

>https://en.wikipedia.org/wiki/Classical_element


### - Aristotle

In his *On Generation and Corruption*, Aristotle related each of the four elements to two of the four sensible qualities:

- Fire + Earth = dry
- Earth + Water = cold
- Water + Air = wet
- Air + Fire = hot

>- Fire is both hot and dry.
>- Air is both hot and wet (for air is like vapor, ἀτμὶς).
>- Water is both cold and wet.
>- Earth is both cold and dry.


### - Proclus

The Neoplatonic philosopher Proclus rejected Aristotle's theory relating the elements to the sensible qualities hot, cold, wet, and dry. He maintained that each of the elements has three properties. Fire is sharp, subtle, and mobile while its opposite, earth, is blunt, dense, and immobile; they are joined by the intermediate elements, air and water, in the following fashion:

|Fire  |Air   |Water |Earth   |
|:----:|:----:|:----:|:------:|
|Sharp |Blunt |Blunt |Blunt   |
|Subtle|Subtle|Dense |Dense   |
|Mobile|Mobile|Mobile|Immobile|


### - Isidore of Seville

Isidore of Seville produced a catalogue of things he regarded as magic in which he listed divination by the four elements i.e. *geomancy*, *hydromancy*, *aeromancy*, and *pyromancy*, as well as by observation of natural phenomena e.g. the flight of birds and astrology.


### - Elementals

An *elemental* is a being that is described in occult works from around the time of the European Renaissance, and particularly elaborated in the 16th century works of Paracelsus. According to Paracelsus and his subsequent followers, there are four categories of *elementals*, which are *gnomes*, *undines*, *sylphs*, and *salamanders*. These correspond to the four classical elements of antiquity: *earth*, *water*, *air*, and *fire*, respectively.

- Gnome, being of earth
- Undine, being of water
- Sylph, being of air
- Salamander, being of fire

Paracelsus regarded them not so much as spirits but as beings between creatures and spirits, generally being invisible to mankind but having physical and commonly humanoid bodies, as well as eating, sleeping, and wearing clothes like humans. He conceived human beings to be composed of three parts, an elemental body, a sidereal spirit, and an immortal divine soul. Elementals lacked this last part, the immortal soul. However, by marriage with a human being, the elemental and its offspring could gain a soul.

- https://en.wikipedia.org/wiki/Elemental


### - Hinduism & Buddhism

From the *pancha mahabhuta*, or "five great elements":

- bhūmi or pṛthvī (earth) - smell
- āpas or jala (water) - taste
- agní or tejas (fire) - vision
- vāyu, vyāna, or vāta (air or wind) - touch
- ākāśa, vyom, or śūnya (space or zero) or (aether or void) - sound

>https://en.wikipedia.org/wiki/Pancha_Bhuta

>https://en.wikipedia.org/wiki/Mah%C4%81bh%C5%ABta

Tibetan Buddhist medical literature speaks of the pañca mahābhūta (five elements) or "elemental properties": earth, water, fire, wind, and space. They exist as "pure natures" represented by the five "female buddhas": *Ākāśadhātviśvarī*, *Buddhalocanā*, *Mamakī*, *Pāṇḍarāvasinī*, and *Samayatārā*.


### - Godai

Godai are the five elements in Japanese Buddhist thought. The concept is related to Buddhist Mahābhūta and came over China from India.

- Chi (earth) : stability/stubbornness; holding ground and using strength and presence (source: strength)
- Sui (water) : flexibility/emotionalism; defensive angling and footwork to overextend the attacker before counterattacking (source: power)
- Ka (fire) : aggression/fear; using high energy attacks defensively (source: energy)
- Fu (wind) : wisdom/love; evasive, elusive methods that redirect attacks away from their targets (source: resiliency)
- Ku (void) : creative/communicative; spontaneous and inventive fighting

>https://en.wikipedia.org/wiki/Godai_(Japanese_philosophy)


### - Bakongo

In traditional Bakongo religion (Central Africa), the four elements are incorporated into the Kongo cosmogram. Each element correlates to a period in the life cycle, which the Bakongo people also equate to the four cardinal directions and seasons. According to their cosmology, all living things go through this cycle.

- Water (South) represents musoni, the period of conception that takes place during spring.
- Fire (East) represent kala, the period of birth that takes place during summer.
- Air (North) represents tukula, the period of maturity that takes place during fall.
- Earth (West) represents luvemba, the period of death that takes place during winter.
- Aether represents mbûngi, the circular void that begot the universe.

>https://en.wikipedia.org/wiki/Kongo_cosmogram


### - Medicine Wheel

The medicine wheel is a sacred symbol across many Indigenous American cultures that signifies Earth's boundary and all the knowledge of the universe. It depicts the four cardinal directions, the path of the sun, the four seasons and the four sacred medicines. Each element is also represented by a color that signifies that four races of humans.

- Earth (South) represents the youth cycle, summer, the Indigenous race, and cedar medicine.
- Fire (East) represents the birth cycle, spring, the Asian race, and tobacco medicine.
- Wind/Air (North) represents the elder cycle, winter, the European race, and sweetgrass medicine.
- Water (West) represents the adulthood cycle, autumn, the African race, and sage medicine.

>The medicine wheel symbol is a modern invention dating to approximately 1972, with these descriptions and associations being a later addition. The associations with the classical elements are not grounded in traditional Indigenous teachings and the symbol has not been adopted by all Indigenous American nations.

>https://en.wikipedia.org/wiki/Medicine_wheel_(symbol)




## Alchemy


Alchemy is an ancient branch of natural philosophy, a philosophical and protoscientific tradition that was historically practiced in China, India, the Muslim world, and Europe. Alchemists attempted to purify, mature, and perfect certain materials. Common aims were chrysopoeia, the transmutation of "base metals" (e.g., lead) into "noble metals" (particularly gold); the creation of an elixir of immortality; and the creation of panaceas able to cure any disease.

>https://en.wikipedia.org/wiki/Alchemy


### - Wuxing

Wuxing, usually translated as *Five Phases* or *Five Agents*, is a fivefold conceptual scheme that many traditional Chinese fields used to explain a wide array of phenomena, from cosmic cycles to the interaction between internal organs, and from the succession of political regimes to the properties of medicinal drugs.

- Fire (火; huǒ)
- Water (水; shuǐ)
- Wood (木; mù)
- Metal or Gold (金; jīn)
- Earth or Soil (土; tǔ)

The Chinese already had very definitive notions of the natural world's processes and "changes". These were commonly thought to be interchangeable with one another; each were capable of becoming another element. The concept is integral, as the belief in outer alchemy necessitates the belief in natural elements being able to change into others.

>https://en.wikipedia.org/wiki/Wuxing_(Chinese_philosophy)


### - Taoist alchemy

The concept of yin-yang is pervasive throughout Chinese alchemical theory. Metals were categorized as being male or female, and mercury and sulphur especially were thought to have powers relating to lunar and solar respectively.

Chinese alchemy can be divided into two methods of practice, *waidan* or "external alchemy" and *neidan* or "internal alchemy". Doctrine can be accessed to describe these methods in greater detail; the majority of Chinese alchemical sources can be found in the *Daozang*, the "Taoist Canon".

>https://en.wikipedia.org/wiki/Chinese_alchemy

>https://en.wikipedia.org/wiki/Cantong_qi


### - Jābir ibn Ḥayyān' sulphur-mercury theory of metals

In the muslim world, medieval alchemy consisted of the four classical elements of *air*, *earth*, *fire*, and *water*, in addition to a new theory called the *sulphur-mercury theory of metals*. According to the Jabirian version of this theory, metals form in the earth through the mixing of *sulfur* and *mercury*.

Depending on the quality of the *sulfur*, different metals are formed, with gold being formed by the most subtle and well-balanced *sulfur*. This theory, which is ultimately based on ancient meteorological speculations such as those found in Aristotle's *Meteorology*, formed the basis of all theories of metallic composition until the 18th century.

>Jābir ibn Ḥayyān is the supposed author of an enormous number and variety of works in Arabic often called the Jabirian corpus. Popularly known as the father of chemistry, Jabir's works contain the oldest known systematic classification of chemical substances, and the oldest known instructions for deriving an inorganic compound (sal ammoniac or ammonium chloride) from organic substances (such as plants, blood, and hair) by chemical means. His classification was an elaborate numerology whereby the root letters of a substance's name in Arabic, when treated with various transformations, held correspondences to the element's physical properties.

>Jabir suggested three categories for the natural elements: Spirits, which vaporize on heating; metals like gold, silver, lead, iron and copper; and stones that can be converted to powder.

>https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6077026/

Jābir analyzed each Aristotelian element in terms of Aristotle's four basic qualities of *hotness*, *coldness*, *dryness*, and *moistness*. According to Jābir, in each metal two of these qualities were interior and two were exterior. For example, lead was externally *cold* and *dry* but internally *hot* and *moist*; gold, on the other hand, was externally *hot* and *moist* but internally *cold* and *dry*.

He believed that metals were formed in the Earth by fusion of *sulfur* (giving the *hot* and *dry* qualities) with *mercury* (giving the *cold* and *moist*.) These elements, *mercury* and *sulfur*, should be thought of as not the ordinary elements but ideal, hypothetical substances. Which metal is formed depends on the purity of the *mercury* and *sulfur* and the proportion in which they come together.

>Thus, Jābir theorized, by rearranging the qualities of one metal, a different metal would result. By this reasoning, the search for the philosopher's stone was introduced to Western alchemy.

>https://en.wikipedia.org/wiki/Alchemy_in_the_medieval_Islamic_world

>https://en.wikipedia.org/wiki/Physics_in_the_medieval_Islamic_world

>https://en.wikipedia.org/wiki/Jabir_ibn_Hayyan


### - Al-Rāzī's chemical processes

The later alchemist Al-Rāzī (c. 865–925), who followed Jābir's *mercury-sulfur theory* but added a third *salty* component, also mentions the following chemical processes:

- Distillation
- Calcination
- Solution
- Evaporation
- Crystallization
- Sublimation
- Filtration
- Amalgamation
- Ceration (a process for making solids pasty or fusible)

Some of these operations (calcination, solution, filtration, crystallization, sublimation and distillation) are also known to have been practiced by pre-Islamic Alexandrian alchemists.


### - Paracelsus' tria prima

The three *metallic principles* (*sulphur to flammability or combustion*, *mercury to volatility and stability* and *salt to solidity*) became the *tria prima* of the Swiss alchemist Paracelsus. He reasoned that Aristotle's four element theory appeared in bodies as three principles. He saw these as fundamental and justified them by recourse to the description of how wood burns in fire: the fire was the work of sulphur, the smoke was mercury, and the residual ash was salt.

The tria prima also defined the human identity. Salt represented the body; mercury represented the spirit (imagination, moral judgment, and the higher mental faculties); sulphur represented the soul (the emotions and desires). He was probably the first to give the element zinc (zincum) its modern name, invented chemical therapy, chemical urinalysis and suggested a biochemical theory of digestion.

>https://en.wikipedia.org/wiki/Paracelsus

>https://en.wikipedia.org/wiki/Elemental


### - Phlogiston theory

In 1667, Johann Joachim Becher published his book *Physica subterranea*, which contained the first instance of what would become the *phlogiston* theory. In his book, Becher eliminated *fire* and *air* from the classical element model and replaced them with three forms of the *earth*: *terra lapidea*, *terra fluida*, and *terra pinguis*. *Terra pinguis* was the element that imparted oily, sulphurous, or combustible properties. Becher believed that *terra pinguis* was released when combustible substances were burned. *Terra pinguis* was later renamed to *phlogiston*.

Eventually, quantitative experiments revealed problems, including the fact that some metals gained weight after they burned, even though they were supposed to have lost *phlogiston*. Still, *phlogiston* remained the dominant theory until the 1770s when Antoine-Laurent de Lavoisier showed that combustion requires a gas that has weight (specifically, *oxygen*) and could be measured by means of weighing closed vessels.

This culminated in the *principle of mass conservation*. These observations solved the mass paradox and set the stage for the new *oxygen theory of combustion*.

>https://en.wikipedia.org/wiki/Phlogiston_theory




## Modern Physics


The Aristotelian tradition and medieval alchemy eventually gave rise to modern chemistry, scientific theories and new taxonomies. By the time of Antoine Lavoisier, for example, a list of elements would no longer refer to classical elements. Modern science recognizes classes of elementary particles which have no substructure (or rather, particles that are not made of other particles) and composite particles having substructure (particles made of other particles).

>https://en.wikipedia.org/wiki/Chemical_element


### - Periodic Table of the Elements

>https://en.wikipedia.org/wiki/Types_of_periodic_tables

>https://en.wikipedia.org/wiki/Dmitri_Mendeleev

>https://en.wikipedia.org/wiki/Periodic_table


### - Problem of Time: General Relativity vs Quantum Physics

>https://en.wikipedia.org/wiki/Problem_of_time

>https://www.theguardian.com/news/2015/nov/04/relativity-quantum-mechanics-universe-physicists


### - States of matter

Atomic theory classifies atoms into more than a hundred chemical elements such as oxygen, iron, and mercury. These elements form chemical compounds and mixtures, and under different temperatures and pressures, can adopt different states of matter:

- **Solid** : a solid holds a definite shape and volume without a container. The particles are held very close to each other.

- **Liquid** : a mostly non-compressible fluid. Able to conform to the shape of its container but retains a (nearly) constant volume independent of pressure.

- **Gas** : a compressible fluid. Not only will a gas take the shape of its container but it will also expand to fill the container.

- **Plasma** : a state characterized by the presence of a significant portion of charged particles in any combination of ions or electrons. A gas is usually converted to plasma in one of two ways, either from a huge voltage difference between two points, or by exposing it to extremely high temperatures. Unlike gases, plasma may self-generate magnetic fields and respond strongly to electromagnetic forces. 

>The plasma state is often misunderstood, and although it is quite commonly generated by either lightning, electric sparks, fluorescent lights or neon lights, it is very uncommon on Earth (except for the ionosphere). But it is the most common state of matter found in the universe, being mostly associated with stars, including the Sun.

These different states share many attributes with the classical elements of *earth*, *water*, *air*, and *fire*, respectively, but they are due to similar behavior of different types of atoms at similar energy levels, and not due to containing a certain type of atom or substance.

>https://en.wikipedia.org/wiki/State_of_matter

>https://en.wikipedia.org/wiki/List_of_states_of_matter


### - Branches of physics

Base the different schools or skills of magic on the different branches of physics

- Classical mechanics
- Electromagnetism
- Thermodynamics
- Quantum mechanics
- Relativity

>https://en.wikipedia.org/wiki/Branches_of_physics

>https://www.thoughtco.com/the-5-branches-of-chemistry-603911

***








# PROGRAMMING PARADIGMS, ALGORITHMS & DATA STRUCTURES




- https://en.wikipedia.org/wiki/Programming_paradigm

- https://en.wikipedia.org/wiki/List_of_algorithms
- https://en.wikipedia.org/wiki/List_of_data_structures

- https://medium.com/techie-delight/top-25-algorithms-every-programmer-should-know-373246b4881b
- https://medium.com/techie-delight/top-algorithms-data-structures-concepts-every-computer-science-student-should-know-e0549c67b4ac