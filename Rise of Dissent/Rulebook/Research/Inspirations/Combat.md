# Balladry

***




## Playing the Game


### - Timekeeping

Time in game is handled through the tracking of Rounds and Turns.

- **Rounds** : Represents roughly one minute and is the time it takes for one rotation of declared actions to be resolved.

- **Turns** : Represents roughly ten minutes, or ten rounds. Most durations, such as light sources and spells, will be measured in Turns. Events are rolled for at the start of every Turn.


### - Order of Play

At the start of play, the Gamemaster should follow these steps every round; Describe, Declare, Resolve, and Repeat. This order is used when a structure to the flow of gameplay is deemed necessary by the Gamemaster.

- **Describe** : The Gamemaster describes the location and the situation that the characters find themselves in. What do they see, hear, smell?

- **Declare** : The Players declare their desired actions to the Gamemaster in a clockwise order from the Gamemaster.

>*If the Gamemaster has actions to declare, they may do so at the start of the round before the players or at the end of the round after the players, or a combination of the two.*

- **Resolve** : The Gamemaster should then go through the declared actions in order and resolve them. Ask for clarifications if needed, call for any needed checks, and handle any effects. Some actions may take multiple rounds to complete.

>*Advance the time records, update any spent resources, and check for any effect durations.*

- **Repeat** : Return to Describe with the situation being updated to reflect the actions of the players.

#### Using Initiative

Rather than simply using a clockwise order for declaring player actions, the Gamemaster may instead have the players all roll a D10 adding their Reflex Bonus to determine the order.

Gamemasters roll Initiative once regardless of how many characters they are managing at the time, using the highest Reflex bonus. All of the Gamemaster’s controlled characters act on the same initiative.


### - Attacking

Attacking a target on your action calls for a Reflex (Small weapons only) or Might check, Sense if using a ranged weapon, opposed by the target’s Reflex. If successful, then the attacker rolls their attack’s damage die and adds the appropriate modifiers.

- **Attack Damage** : Successful attacks inflict damage based on the attack’s damage die, typically determined by the weapon used for the attack modified by the bonus of the attribute used to make the attack.

- **Critical Hits** : A critical hit is when max damage is rolled on the attacks damage die. Unless otherwise noted, a critical hit adds an additional die of damage to the attack.

- **Sneak Attacks** : Attacks made against targets who are unaware of their attacker are made with Advantage.

- **Knockouts & Executions** : Targets reduced to zero Health are knocked unconscious and left Wounded. A character cannot wake up until they have been restore to at least one Health. Another attack must be made to execute the wounded target. Alternatively, Critical Hits that reduce the target to zero Health may instantly execute the target if the attacker wishes.

- **Bleeding Out** : After a Turn of being Wounded and Unconscious, the character will make a Vigor check and regain 1 HP if successful or die on a failure. A successful check to offer medical aid will stop this from happening.

- **Alternative Attacks** : Not all attacks have to be made to inflict damage. Characters may forgo damage to force some effect on their target, such as tripping them, pushing them, grappling them, or some other reasonable effect.


### - Wounds & Death

Whenever a character is reduced to zero Health, they are knocked unconscious and suffer the Wounded condition. While wounded, any damage they take while unconscious or that would reduce back to zero instantly kills them.

- **Mending Wounds** : Removing the Wounded condition requires Medical Attention or a week of proper rest.

- **Drastic Attribute Loss** : If a character loses more the 5 points of an Attribute at once they go into Shock and become unable to take actions for a round and suffers the Wounded condition. If, for any reason, an attribute is reduced to zero, the character instantly dies.


### - Movement & Distance

Movement and Distance are largely abstracted in this system. Most characters can freely move up to a distance of Near as part of another action and up to Distant if they dedicate their action to movement. If a character’s movement is more or less than normal it will be noted as a feature.

- **Distance Intervals** : Distance in Balladry is divided into the following four intervals; Close (~2 yards), Near (~10 yards), Distant (~30 yards), and Far (~100 yards).

- **Stealth** : Characters can only move up to Near while trying to remain stealthy. If needed, characters should make Reflex Checks opposed by the Sense of any creature that is actively keeping a lookout or who has a significant change of spotting the sneaking character

- **Terrain** : Hazardous or difficult terrain can lead to crossing it requiring a Check, inflicting damage, causing slowed movement, or simply being impassable.

>*Swimming is considered a form of Terrain for the
purpose of restrictions.*

- **Area of Effects** : Certain abilities, actions, or items will effect an area at a distance, in such a case the distance of the effect will by displayed as such; Distance(Area).


### - Resting

Characters all need rest to recover from harm and exhaustion. All rests require Three Comforts to receive the benefits of the rest.

#### Partial Rests

Takes 30 Minutes, or 3 Turns.

- Replenish one lost Mana
- Recover one spent Focus

#### Full Rests

Takes a full day of rest.

- Replenish all lost Mana
- Recover all spent Focus
- Recover one lost Health
- Recover 1 lost Attribute Point

#### Comforts

Comforts could be many different things based on the character and situation, but here are some common comforts.

- A meal
- A good drink
- Sleep
- A relaxing and safe place to rest
- Personal Rituals
- Decent bedding
- Some entertainment

Some environmental conditions may impose “Negative Comforts” that will need to be counteracted to get a rest. These could be extreme heat or cold, weather, or the unsettling feeling one gets from exploring a tomb.

#### Medical Attention

A character who has the skill to provide medical aid to someone may attempt to help to the wounded. For every character that the medic attends to, they take a Negative Comfort. The character will make an Insight check.

If successful on a Partial Rest, the attended will recover 1D4 Health or Attribute Points.

If successful on a Full Rest, the attended lose the Wounded condition if they have it and recover 2D10 Health or Attribute Points.

#### Interrupted Rests

If a rest is interrupted by a threat, all characters must succeed on a Vigor Check after they return to and finish their rest in order to receive the benefits of the Rest.

#### Exhaustion

Going too long without Food, Water, or Sleep will stack as Negative Comforts and can eventually kill the suffering character.


### - Conditional Rules

These are some behaviors or conditions that have some meaningful effects while playing the game.

- **Cover** : Character’s who are behind sufficient cover can either not be targeted by attacks or impose Disadvantage on attacks against them, based on the coverage.

- **Darkness & Blindness** : The dark is a dangerous and oppressive condition for anything not meant to be there. Creatures deprived of their primary senses suffer Disadvantage on all checks regardless of their normal Advantages. While most creatures use sight as their primary sense, some creatures could use their hearing, scent, or some form of touch.

- **Dual-Wielding** : When fighting with a weapon in either hand, damage is rolled with both weapon’s die together and the attacker may choose which to Crit to use if both trigger. Only small weapons can be used in the off hand.

- **Flanking** : Characters attacking with an ally in melee on the rear side of their target has both them and their ally roll damage on a successful hit. The non-attacking allies cannot crit on this damage roll.

- **Forced Movement** : If a character falls, is thrown, pushed, or otherwise forced into hazardous movement, they will take damage upon impact with a hard surface equal to 1d6 for every three yards.

- **Holding your Breath** : A character can hold their breath for an amount of Rounds equal to their Vigor Bonus (min. of 1). Characters take 1d8 damage every Round beyond this limit.

- **Mounted** : Taking Actions while mounted allows the player to have both the character and the mount take actions as normal. So they can move using their mount’s action and attack using their own. Also, melee attacks made while mounted add the mount’s Might Bonus to the damage in addition to the rider’s

- **Prone** : Attacking a character who is prone results in Advantage for melee attacks and Disadvantage for ranged attacks. Attacking while prone also causes Disadvantage. Dropping prone or rising from prone takes a character’s action.

- **Surprised** : Character’s who are Surprised cannot act on the first round after their Surprise.

#### Teamwork on Checks

When working together to perform a physical task, such as lifting a large stone or giving a lift when climbing, the characters may use the following rules. When using Teamwork, the assisting characters forgo their actions for the round to help.

The check gains a modifier of one for every helper that meaningfully contributes to the check. This modifier functions the same as the Skilled Bonus.

Characters can use teamwork on attack and spellcasting checks as normal, however the assisting characters forfeit their action for the round to lend the aid to the check.

#### Elemental Attack Traits

Attacks that use an Elemental source, such as a lightning bolt, use these special critical traits.

- *Fire* : can crit up to three times.

- *Frost* : can forgo crit damage to stun target

- *Shock* : can have crit damage affect an additional target within Close of the original target.




## Equipment


### - Armor

This section covers the rules for armor, shields, and helmets and how they benefit the wearer.

- **Armor & Helmets** : A character’s worn armor has a series of effects listed in their entry such as a bonus to the wearer’s max health, a penalty to their Reflex or Sense score, and how many slots they take up in inventory.

- **Shields & Block Die** : Shields have a listed Block Die that is rolled when they are hit by an external attack. Negating any damage die less than the roll of the Block Die.

>*Shields can be used to block for a Close ally. However, only one shield can be used for blocking.*

- **Shields Shall be Splintered** : A character may elect to have their shield take the brunt of an attack, negating all damage from the attack made against the shield’s wielder or an adjacent ally. However, the shield is broken and cannot be used until it has been repaired.

- **Masterwork Armor** : Masterfully crafted armor is rare and powerful, worn by nobility and adventurers of great renown and success. Masterwork Armor and Helmets gains a reduce damage taken by 1 each. Masterwork Shields get a +1 to the Block Die roll.

>*Masterwork armor is typically ten times the normal value of the item.*

>*Only masterwork armor can be used to make enchanted armor.*


### - List of Armors

Detailed below are the various combinations of the armor variables and there associated effects.

#### Doublet

- +1 Max Health
- Occupies One Slot
- Value Bracket: Common/Practical

#### Gambeson

- +3 Max Health
- Occupies One Slot
- Value Bracket: Uncommon/Practical

#### Mail-Coat

- +5 Max Health
- Reflex capped at 14
- 11 Might Required
- Occupies One Slot
- Value Bracket: Uncommon/Practical

#### Half-Plate

- +7 Max Health
- Reflex capped at 12
- 13 Might Required
- Occupies Two Slots
- Value Bracket: Rare/Practical

#### Full-Plate

- +10 Max Health
- Reflex capped at 10
- 15 Might Required
- Occupies Two Slots
- Value Bracket: Rare/Intricate

#### Shield

- D6 Block Die
- Occupies One Slot
- Value Bracket: Uncommon/Useful

#### Great Shield

- D8 Block Die
- 12 Might Required
- Occupies Two Slots
- Value Bracket: Uncommon/Practical

#### Helmet

- +1 Max Health
- Sense capped at 14
- Occupies One Slot
- Value Bracket: Uncommon/Useful

#### Great Helm

- +2 Max Health
- Sense capped at 12
- Occupies One Slot
- Value Bracket: Uncommon/Practical


### - Weapons

The rules in this section will cover the uses and mechanics of weapons in this system. This section does not cover combat, just the rules for weapons specifically.

- **Improvised Weapons** : Improvised weapons lack traits and will share their other features with a weapon that best correlates to whatever is being used as a weapon. Additionally, improvised weapons cannot land a critical hit.

- **Unarmed Attacks** : Unarmed attacks, as in using your hands, feet, or other natural weapons, have a damage die of a D4 and have no traits (unless otherwise noted), cannot crit, take no inventory slots, and cannot be disarmed.

- **Weapon Traits** : Weapons feature Traits that offer special uses of the weapon that is triggered in certain cases.

- **Weapon Sizes** : A weapon’s size dictates how it needs to be held and how many Container Slots it takes up. Small weapons can only be held in one hand, can be used for dual wielding, and can be more easily concealed and takes one Slot. Medium weapons can be held in either one or two hands and takes one Slot. Large weapons can only be held in two hands and takes two Slots.

- **Two-Handed Weapons** : Weapons held in two hands inflict 2 die of damage. Bows and Crossbows count as two-handed. Only one of these 2 die can trigger a Critical Hit.

- **Silvered Weapons** : These weapons can harm supernatural creatures that are normally resistant or immune to mundane weapons. Silvered weapons are double the normal value.

- **Masterwork Weapons** : Masterfully crafted weapons are rare and powerful, used by nobility and adventurers of great renown and success. Masterwork Weapons gain a +1 to its Attack and Damage Rolls.

>*Masterwork weapons are always of Rare scarcity.*

>*Only masterwork weapons can be used to make enchanted weapons.*

- **Weapon Values** : Instead of using the Functionality Bracket for value, use two of the weapon’s Die. Rarity should usually be Uncommon or up to the GM.

#### Ammunition

Ranged Weapons require ammunition to use in combat. Stones for slings, arrows for bows, and bolts for crossbows. Slings can also make use of improvised ammunition for a -1 to damage.

#### Weapon Types

Weapons are divided into Ranged, Melee, and Thrown. The differences are detailed below.

- *Ranged* : Can be used to attack targets up to a reach of Far. Needs Ammunition and one hand to carry, two to use, except for Slings.

- *Melee* : Can only be used against targets within Close.

- *Thrown* : Can be used as a melee weapon or as a ranged weapon with a max reach of Distant.

#### Custom Weapons

Players and Gamemasters may work together to mix and match these weapon variables to to create new weapons if no existing ones fit the weapon they wish to use.


### - List of Weapons

#### Bow

- *Die* : d8
- *Type* : ranged
- *Size* : medium
- Forgo critical hit to attack another target

#### Crossbow

- *Die* : d10
- *Type* : ranged
- *Size* : medium
-  Crits double damage rather than add a die

#### Dagger

- *Die* : d4
- *Type* : thrown
- *Size* : small
-  Can critically hit on critical damage rolls

#### Greataxe

- *Die* : d12
- *Type* : melee
- *Size* : large
- Critically hits on damage rolls of 10+

#### Greatsword (Claymore)

- *Die* : d12
- *Type* : melee
- *Size* : large
- Can hit two adjacent targets within reach

#### Longsword

- *Die* : d8
- *Type* : melee
- *Size* : medium
- Crits double damage rather than add a die

#### Mace

- *Die* : d6
- *Type* : melee
- *Size* : small
- Roll damage twice, pick which to keep

#### Maul

- *Die* : d10
- *Type* : melee
- *Size* : large
- Target is knocked prone on a critical hit

#### Pike

- *Die* : d10
- *Type* : melee
- *Size* : large
- Can be used to attack targets up to Near

#### Shortsword

- *Die* : d6
- *Type* : melee
- *Size* : large
- Forgo critical hit to attack another target

#### Sling

- *Die* : d4
- *Type* : ranged
- *Size* : small
- Can critically hit on critical damage rolls

#### Spear

- *Die* : d6
- *Type* :thrown
- *Size* : medium
- Critically hits on Damage Rolls of 1 or 6

#### Staff

- *Die* : d4
- *Type* : melee
- *Size* : medium
- Forgo critical damage to trip target

#### Waraxe

- *Die* : d8
- *Type* : melee
- *Size* : medium
-  Critically hits on damage rolls of 6+

#### Warhammer

- *Die* : d8
- *Type* : melee
- *Size* : medium
- Critically hits on Damage Rolls of 1 or 8

#### Whip

- *Die* : d4
- *Type* : melee
- *Size* : small
- Can hit at Near & can forgo crit to Disarm

***








# FATE - Conflicts

***




In a conflict, characters are actively trying to harm one another. It could be a fist fight, a shootout, or a sword duel. It could also be a tough interrogation, a psychic assault, or a shouting match with a loved one. As long as the characters involved have both the intent and the ability to harm one another, then you’re in a conflict scene.

Conflicts are either physical or mental in nature, based on the kind of harm you’re at risk of suffering. In physical conflicts, you suffer bruises, scrapes, cuts, and other injuries. In mental conflicts, you suffer loss of confidence and self-esteem, loss of composure, and other psychological trauma.

Setting up a conflict is a little more involved than setting up contests or challenges. Here are the steps:

- Set the scene, describing the environment the conflict takes place in,
creating situation aspects and zones, and establishing who’s participating and what side they’re on.

- Determine the turn order.

- Start the first exchange:

>On your turn, take an action and then resolve it.

>On other people’s turns, defend or respond to their actions as necessary.

>At the end of everyone’s turn, start again with a new exchange.

You know the conflict is over when everyone on one of the sides has conceded or been taken out.




## Setting the Scene


- Who’s in the conflict?

- Where are they positioned relative to one another?

- When is the conflict taking place? Is that important?

- What’s the environment like?

You don’t need an exhaustive amount of detail here, like precise measures of distance or anything like that. Just resolve enough to make it clear for everyone what’s going on.

#### Situation Aspects

GMs, when you’re setting the scene, keep an eye out for fun-sounding features of the environment to make into situation aspects, especially if you think someone might be able to take advantage of them in an interesting way in a conflict. Don’t overload it-find three to five evocative things about your conflict location and make them into aspects.

Good options for situation aspects include:

- Anything regarding the general mood, weather, or lighting-dark or badly lit, storming, creepy, crumbling, blindingly bright, etc.

- Anything that might affect or restrict movement-filthy, mud everywhere, slippery, rough, etc.

- Things to hide behind-vehicles, obstructions, large furniture, etc.

- Things you can knock over, wreck, or use as improvised weapons-bookshelves, statues, etc.

- Things that are flammable




## Zones


GMs, if your conflict takes place over a large area, you may want to break it down into zones for easier reference.

A zone is an abstract representation of physical space. The best definition of a zone is that it’s close enough that you can interact directly with someone (in other words, walk up to and punch them in the face).

Generally speaking, a conflict should rarely involve more than a handful of zones. Two to four is probably sufficient, save for really big conflicts. This isn’t a miniatures board game -zones should give a tactile sense of the environment, but at the point where you need something more than a cocktail napkin to lay it out, you’re getting too complicated.

- If you can describe the area as bigger than a house, you can probably d0ivide it into two or more zones-think of a cathedral or a shopping center parking lot.

- If it’s separated by stairs, a ladder, a fence, or a wall, it could be divided zones, like two floors of a house.

- “Above X” and “below X” can be different zones, especially if moving between them takes some doing-think of the airspace around something large, like a blimp.

When you’re setting up your zones, note any situation aspects that could make moving between those zones problematic. They’ll be important later, when people want to move from zone to zone.

>*Amanda decides the warehouse needs to be multiple zones. The main floor is big enough, in her mind, for two zones, and the Heavy Crates she mentioned earlier make it hard to freely move between them.*

>*She knows there’s also a second floor ringing the inner walls, so she makes that an additional zone. She adds Ladder Access Only to the scene.*

>*If, for some reason, someone decides to run outside, she figures that can be a fourth zone, but she doesn’t think she needs any aspects for it.

>*She sketches the rough map on an index card for everyone to see.*




## Establishing Sides


It’s important to know everyone’s goal in a conflict before you start. People fight for a reason, and if they’re willing to do harm, it’s usually an urgent reason.

Make sure everyone agrees on the general goals of each side, who’s on which side, and where everyone is situated in the scene (like who’s occupying which zone) when the conflict begins.

It might also help, GMs, to decide how those groups are going to “divvy up” to face one another-is one character going to get mobbed by the bad guy’s henchmen, or is the opposition going to spread itself around equally among the PCs? You might change your mind once the action starts, but if you have a basic idea, it gives you a good starting point to work from.




## The Exchange


In an exchange, every character gets a turn to take an action. GMs, you get to go once for every NPC you control in the conflict.

>*GMs, if you have a lot of nameless NPCs in your scene, feel free to have them use passive opposition to keep your dice rolling down. Also, consider using mobs instead of individual NPCs to keep things simple.*

Most of the time, you’re going to be attacking another character or creating an advantage on your turn, because that’s the point of a conflict-take your opponent out, or set things up to make it easier to take your opponent out.

You only get to make one skill roll on your turn in an exchange, unless you’re defending against someone else’s action -you can do that as many times as you want. You can even make defend actions on behalf of others, so long as you fulfill two conditions: it has to be reasonable for you to interpose yourself between the attack and its target, and you have to suffer the effects of any failed rolls.

#### Turn Order

Your turn order in a conflict is based on your skills. In a physical conflict, compare your Notice skill to the other participants. In a mental conflict, compare your Empathy skill. Whoever has the highest gets to go first, and then everyone else in descending order.

If there’s a tie, compare a secondary or tertiary skill. For physical conflicts, that’s Athletics, then Physique. For mental conflicts, Rapport, then Will.

GMs, for a simple option, pick your most advantageous NPC to determine your place in the turn order, and let all your NPCs go at that time.

#### Full Defense

If you want, you can forgo your action for the exchange to concentrate on defense. You don’t get to do anything proactive, but you do get to roll all defend actions for the exchange at a +2 bonus.

#### Mobs

Whenever possible, identical nameless NPCs like to form groups, or mobs. Not only does this better ensure their survival, it reduces the workload on the GM. For all intents and purposes, you can treat a mob as a single unit- instead of rolling dice individually for each of three thugs, just roll once for the whole mob.




## Movement


In a conflict, it’s important to track where everyone is relative to one another, which is why we divide the environment where the conflict’s taking place into zones. Where you have zones, you have people trying to move around in them in order to get at one another or at a certain objective.

Normally, it’s no big deal to move from one zone to another-if there’s nothing preventing you from doing so, you can move one zone in addition to your action for the exchange.

If you want to move more than one zone (up to anywhere else on the map), if a situation aspect suggests that it might be difficult to move freely, or if another character is in your way, then you must make an overcome action using Athletics to move. This counts as your action for the exchange.

GMs, just as with other overcome rolls, you’ll set the difficulty. You might use the number of zones the character is moving or the situation aspects in the way as justification for how high you set passive opposition. If another character is impeding the path, roll active opposition and feel free to invoke obstructing situation aspects in aid of their defense.

If you fail that roll, whatever was impeding you keeps you from moving. If you tie, you get to move, but your opponent takes a temporary advantage of some kind. If you succeed, you move without consequence. If you succeed with style, you can claim a boost in addition to your movement.




## Advantages


Remember that aspects you create as advantages follow all the rules for situation aspects -the GM can use them to justify overcome actions, they last until they’re made irrelevant or the scene is over, and in some cases they represent as much a threat to you as an opponent.

When you create an advantage in a conflict, think about how long you want that aspect to stick around and whom you want to have access to it.

In terms of options for advantages, the sky’s the limit. Pretty much any situational modifier you can think of can be expressed as an advantage. If you’re stuck for an idea, here are some examples:

- **Temporary Blinding** : Throwing sand or salt in the enemy’s eyes is a classic action staple. This places a Blinded aspect on a target, which could require them to get rid of the aspect with an overcome action before doing anything dependent on sight. Blinded might also present opportunities for a compel, so keep in mind that your opponent can take advantage of this to replenish fate points.

- **Disarming** : You knock an opponent’s weapon away, disarming them until they can recover it. The target will need an overcome action to recover their weapon.

- **Positioning** : There are a lot of ways to use advantages to represent positioning, like High Ground or Cornered, which you can invoke to take advantage of that positioning as context demands.

- **Winded and Other Minor Hurts** : Some strikes in a fight are debilitating because they’re painful, rather than because they cause injury. Nerve hits, groin shots, and a lot of other “dirty fighting” tricks fall into this category. You can use an advantage to represent these, sticking your opponent with Pain-Blindness or Stunned or whatever, then following up with an attack that exploits the aspect to do more lasting harm.

- **Taking Cover** : You can use advantages to represent positions of cover and invoke them for your defense. This can be as general as Found Some Cover or as specific as Behind the Big Oak Bar.

- **Altering the Environment** : You can use advantages to alter the environment to your benefit, creating barriers to movement by scattering Loose Junk everywhere, or setting things On Fire. That last one is a favorite in Fate.




## Resolving Attacks


A successful attack lands a hit equivalent to its shift value on a target. So if you get three shifts on an attack, you land a 3-shift hit.

If you get hit by an attack, one of two things happen: either you absorb the hit and stay in the fight, or you’re taken out.

Fortunately, you have two options for absorbing hits to stay in the fight-you can take stress and/or consequences. You can also concede a conflict before you’re taken out, in order to preserve some control over what happens to your character.

#### Stress

One of your options to mitigate the effect of a hit is to take stress.

The best way to understand stress is that it represents all the various reasons why you just barely avoid taking the full force of an attack. Maybe you twist away from the blow just right, or it looks bad but is really just a flesh wound, or you exhaust yourself diving out of the way at the last second.

Mentally, stress could mean that you just barely manage to ignore an insult, or clamp down on an instinctive emotional reaction, or something like that.

Stress boxes also represent a loss of momentum-you only have so many last-second saves in you before you’ve got to face the music. After a conflict, when you get a minute to breathe, any stress boxes you checked off become available for your use again.

#### Consequences

The second option you have for mitigating a hit is taking a consequence. A consequence is more severe than stress-it represents some form of lasting injury or setback that you accrue from the conflict, something that’s going to be a problem for your character after the conflict is over.

Consequences come in three levels of severity -mild, moderate, and severe. Each one has a different shift value: two, four, and six, respectively.

When you use a consequence slot, you reduce the shift value of the attack by the shift value of the consequence. You can use more than one consequence at a time if they’re available. Any of the hit’s remaining shifts must be handled by a stress box to avoid being taken out.

Unlike stress, a consequence slot may take a long time to recover after the conflict is over. Also unlike stress, you only have one set of consequences; there aren’t specific slots for physical versus mental consequences. This means that, if you have to take a mild consequence to reduce a mental hit and your mild consequence slot is already filled with a physical consequence, you’re out of luck!

Here are some guidelines for choosing what to name a consequence:

- **Mild consequences** don’t require immediate medical attention. They hurt, and they may present an inconvenience, but they aren’t going to force you into a lot of bed rest. On the mental side, mild consequences express things like small social gaffes or changes in your surface emotions. Examples: Black Eye, Bruised Hand, Winded, Flustered, Cranky, Temporarily Blinded.

- **Moderate consequences** represent fairly serious impairments that require dedicated effort toward recovery (including medical attention). On the mental side, they express things like damage to your reputation or emotional problems that you can’t just shrug off with an apology and a good night’s sleep. Examples: Deep Cut, First Degree Burn, Exhausted, Drunk, Terrified.

- **Severe consequences** go straight to the emergency room (or whatever the equivalent is in your game)-they’re extremely nasty and prevent you from doing a lot of things, and will lay you out for a while. On the mental side, they express things like serious trauma or relationship-changing harm. Examples: Second-Degree Burn, Compound Fracture, Guts Hanging Out, Crippling Shame, Trauma-Induced Phobia.




## Recovering from a Consequence


In order to regain the use of a consequence slot, you have to recover from the consequence. That requires two things -succeeding at an action that allows you to justify recovery, and then waiting an appropriate amount of game time for that recovery to take place.

The action in question is an overcome action; the obstacle is the consequence that you took. If it’s a physical injury, then the action is some kind of medical treatment or first aid. For mental consequences, the action may involve therapy, counseling, or simply a night out with friends.

The difficulty for this obstacle is based on the shift value of the consequence. Mild is Fair (+2), moderate is Great (+4), and severe is Fantastic (+6). If you are trying to perform the recovery action on yourself, increase the difficulty by two steps on the ladder.

Keep in mind that the circumstances have to be appropriately free of distraction and tension for you to make this roll in the first place-you’re not going to clean and bandage a nasty cut while ogres are tromping through the caves looking for you. GMs, you’ve got the final judgment call.

If you succeed at the recovery action, or someone else succeeds on a recovery action for you, you get to rename the consequence aspect to show that it’s in recovery. So, for example, Broken Leg could become Stuck in a Cast, Scandalized could become Damage Control, and so on. This doesn’t free up the consequence slot, but it serves as an indicator that you’re recovering, and it changes the ways the aspect’s going to be used while it remains.

>*Whether you change the consequence’s name or not -and sometimes it might not make sense to do so-mark it with a star so that everyone remembers that recovery has started.*

Then, you just have to wait the time.

- For a **mild consequence**, you only have to wait one whole scene after the recovery action, and then you can remove the aspect and clear the slot.

- For a **moderate consequence**, you have to wait one whole session after the recovery action (which means if you do the recovery action in the middle of a session, you should recover sometime in the middle of next session).

- For a **severe consequence**, you have to wait one whole scenario after the recovery action.

>*Many genres have some sort of mechanism by which characters can quickly recover from injuries. Fantasy settings have the ubiquitous healing potion or spell. Sci-fi has superscience dermal regenerators or biogel. Usually, these mechanisms exists because many games express injuries in terms of a constant numerical penalty that drastically affects a character’s effectiveness.*

>*In Fate, however, a consequence is largely just like any other aspect. It only comes into play when someone pays a fate point to invoke it (after the initial free invoke, of course), or when it’s compelled.*

>*At best, powerful healing should simply eliminate the need to roll for a recovery action, or should reduce the severity of a consequence by one level or more. So, a healing potion might turn a severe consequence into a moderate one, making the recovery time much shorter. The PC should have to spend at least one scene where the consequence could affect things, before you let it go away.*




## Conceding the Conflict


When all else fails, you can also just give in. Maybe you’re worried that you can’t absorb another hit, or maybe you decide that continuing to fight is just not worth the punishment. Whatever the reason, you can interrupt any action at any time before the roll is made to declare that you concede the conflict.

Concession gives the other person what they wanted from you, or in the case of more than two combatants, removes you as a concern for the opposing side. You’re out of the conflict, period.

But it’s not all bad. First of all, you get a fate point for choosing to concede. On top of that, if you’ve sustained any consequences in this conflict, you get an additional fate point for each consequence. These fate points may be used once this conflict is over.




## Getting Taken Out


If you don’t have any stress or consequences left to buy off all the shifts of a hit, that means you’re taken out.

Taken out is bad -it means not only that you can’t fight anymore, but that the person who took you out gets to decide what your loss looks like and what happens to you after the conflict. Obviously, they can’t narrate anything that’s out of scope for the conflict (like having you die from shame), but that still gives someone else a lot of power over your character that you can’t really do anything about.

#### Character Death

So, if you think about it, there’s not a whole lot keeping someone from saying, after taking you out, that your character dies. If you’re talking about a physical conflict where people are using nasty sharp weapons, it certainly seems reasonable that one possible outcome of defeat is your character getting killed.




## Ending a Conflict


Under most circumstances, when all of the members of one side have either conceded the conflict or have been taken out, the conflict is over.

***








# Jface's Pressure System - Combat

***




>https://www.youtube.com/playlist?list=PL1h5ZjB1jF-K3sum-CMnuVl5RoaP38V8U




## Zones & Distance


>https://www.youtube.com/watch?v=2N0JGNFShnw


As a GM, you can make zones whatever you want, it's however you design spaces. Zones can touch each other or be separated.

#### Distances

- Any characters found together in the same zone are considered **Close**.

- If two characters are touching (and are in the same zone obviously), they're considered **Engaged**.

- A character close to a zone, in its periphery or adjacent to it, is considered **Nearby**. This also works if the character is in a zone that is touching the current zone.

- If a character is somewhere else that is not adjacent (like in a zone that isn't touching the specific zone), they're considered **Far Away**.

#### Movement

- If a character is moving from a zone to another separated zone, they go from zone 1 to **Nearby** zone 1, to **Nearby** zone 2 and then finally into zone 2.

- If, using this method, two characters happen to meet in-between zones, just create a new third zone and place both characters in it.

***








# The One Ring Starter Set - Combat

***




## Opening Volleys


If the Player-heroes and their opponents are separated by a distance, at the start of a fight all combatants are allowed to make ranged attacks before combat at close quarters is initiated.

The Loremaster must determine how many volleys to allow, based on the distance separating the fighters - under most circumstances, all combatants are entitled to at least one volley using a bow or a thrown weapon. If the distance is greater, then the Loremaster might allow combatants to let loose two volleys, or even more.

>*All volley attacks are resolved as normal ranged attacks (see next page). Normally, the volley exchange is resolved with the Player-heroes launching their volleys first, unless the Loremaster considers the circumstances to favour the opposition.*

When the opening volleys are completed, the combatants cover the distance that separates them and begin fighting at close quarters.




## Close Quarters Rounds


Once fighting at close quarters is initiated, the gameplay is broken down into a cycle of rounds, played one after the other until the end of the battle.

Each round follows the sequence set out below:

1. **Stance** : The Player-heroes choose their stances.

2. **Engagement** : All combatants in Close Combat are paired with one or more opponents.

3. **Attack Rolls Resolution** : The actions of all combatants are resolved, with those fighting in close combat going first.

### 1 - Stance

All Player-heroes select whether they will fight the round in Close Combat, or Ranged Combat - Player-heroes can choose freely to fight in Close Combat at the start of each round, while they are allowed to fight in Ranged Combat only if there are at least two other adventurers fighting in Close Combat.

>*Fighters in Close Combat exchange blows in the thick of the fight, using swords, axes and other close combat weapons, while those engaged in Ranged Combat stay back, using ranged weapons like bows or spears, and can be targeted only by attackers using similar weapons.*

#### Forward (Close Combat)

- Your attack rolls gain *1d6*.

- All close combat attacks aimed at you gain *1d6*.

#### Open (Close Combat)

- No advantage or disadvantage

#### Defensive (Close Combat)

- All close combat attacks aimed at you lose *1d6*.

- Your attack rolls lose *1d6* for each opponent engaging you.

#### Rearward (Ranged Combat)

- Can only attack with, and be targeted by, ranged weapons.


### 2 - Engagement

At the start of each round, Player-heroes fighting in Close Combat must choose one or more opponents to engage. If all opponents are already engaged, Player-heroes without an adversary must join another Player-hero fighting an enemy.

Player-heroes fighting in Ranged Combat cannot be engaged in Close Combat, and can only be attacked by enemies using ranged weapons.

>*If there are more enemies than heroes, it is possible that there will be enemies left when everyone fighting in Close Combat has engaged an adversary. If this happens, the Loremaster chooses whether the ‘spare’ enemies engage a Player-hero who is already fighting in a Close Combat stance, or stand back to attack with a ranged weapon. Enemies who stand back to use a ranged weapon may attack any Player-hero involved in the fight.*


### 3 - Attack Rolls Resolution

Unless differently specified, the attacks of all combatants are resolved with the Player-heroes going first. When all Playerheroes have resolved their attacks, the Loremaster will resolve those of their adversaries. Close Combat attack rolls are resolved first, then all Ranged Attack rolls follow.

An attack is a die roll made using the Combat Proficiency corresponding to the weapon used.

- The difficulty of all attack rolls by the Player-heroes is based on their STRENGTH TN, modified by the Parry rating of the targeted enemy.

- The difficulty of all attack rolls made by adversaries against Player-heroes are equal to the target hero’s Parry score instead.

A successful attack roll inflicts damage in the form of an Endurance Loss based on the weapon used, and may inflict additional Special Damage types based on the Degree of Success of the roll. Finally, attacks may cause more long-lasting injuries if a Piercing Blow is scored.

#### Endurance Loss

When a melee or ranged attack roll succeeds, the target suffers an immediate loss of Endurance equal to the Damage rating of the weapon used.

- Player-heroes who see their Endurance decrease to match or go below their total Load score become Weary; if their Endurance is reduced to zero, they drop unconscious instead.

- Adversaries do not normally become Weary due to Endurance loss, but are normally eliminated upon reaching zero Endurance.

#### Special Damage

If a successful attack roll produces one or more Success icons (X), they can be used to trigger one or more special results. Each heading below indicates how many Success icons are required to trigger the corresponding effect. Multiple Success icons can be used to trigger different results, or the same one multiple times.

- **Heavy Blow** - any weapon (X): You have hit your opponent with great force and precision - Spend 1 Success icon to inflict to your target an additional loss of Endurance equal to your STRENGTH rating (adversaries use their Attribute Level instead).

- **Fend Off** - any close combat weapon (X): You exploit your successful attack to place yourself in an advantageous position - Spend 1 Success icon to gain a Parry modifier of +2 against the next attack aimed at you.

- **Pierce** - bows and spears (X), swords (X)(X): You have hit a less-protected part of the adversary’s body - Spend 1 Success icon to modify a Feat die numerical result of your attack by +2, thus possibly triggering a Piercing Blow.

#### Piercing Blows

In addition to causing the loss of Endurance points, any successful attack may inflict a Piercing Blow, and Characters must immediately roll one Feat Die, plus a number of Success Dice equal to the PROTECTION value of the armour worn (a PROTECTION Test).

>*The Target Number for the roll is equal to the Injury rating of the weapon used by the attacker. If the roll fails, then the target of the attack has received a lifethreatening blow - a Wound (see below).*




## Wounds


Most adversaries are killed outright when Wounded (unless specified differently in their description). Player-heroes, on the other hand, can resist being Wounded once without serious consequences, but risk more if injured a second time.

>*When Player-heroes are Wounded for the first time they check the Wounded box on their character sheet.*

As seen under Endurance on page 18, Player-heroes whose Wounded box is checked recover Endurance slowly.

>*Player-heroes who are wounded a second time (they receive a Wound when their Wounded box was already checked) see their Endurance drop to zero, and they fall unconscious. This second Wound is not recorded on the character sheet.*

Player-heroes recover from their injuries between adventures, or in a week. They are allowed to reduce the severity of their Wound with a HEALING roll:

>*A successful HEALING roll reduces the time required to recover by 1 day, plus 1 day for each (X) icon scored (to a minimum of 1 day).*

Each hero may be administered a successful HEALING roll only once. A failed roll cannot be repeated until at least a day has passed, as the failure of the treatment isn’t immediately apparent.

***








# The Riddle of Steel

***




## Basic Rules and Game Mechanics


>p.10


### - Dice

The Riddle of Steel uses two kinds of dice to resolve situations where chance is involved: several ten sided dice (referred to as Id, 2d, 3d, or 1 d 1 0, 2d10, etc.), and one six-sided die (1 d6 or d6). Any time a reference is made to "dice" without stating d6 or d10, assume the reference applies to d10 's. Most players will need one d6 and 10 or more dl0's (d 10's may be purchased at any gaming store and most comic stores). We also recommend that at least one of those dice be red and another white, and that each player have a small bowl or two to keep his own dice in on the gaming table.


### - The Major Rolls and Tests (Rolling Dice)

Five different kinds of rolls are made during the course of play, most of which are very similar.

#### Attribute Tests

Attribute Tests are made by rolling a number of dice against a Target Number (TN) based on the difficulty of the task at hand. The TNs for Attribute Tests are set by the Seneschal, based on standard TNs in Table 1.1. Target Numbers range from 2 (foolproof) to 12 (very difficult) or higher (nearly impossible). Every individual die that equals or beats that TN is kept as a success; the rest are thrown away. Target Numbers over 10 may be achieved by re-rolling any roll of 10 and adding it to the original roll. This process is called Stacking; any given die may continue to Stack so long as a 10 is rolled each time. Usually only one success is necessary for an action to be successful, though some may require more. Likewise, the quantity of successes indicates an action's degree of success: one is just enough, while four or five are nearly flawless (see Table 1.2).

Occasionally circumstances make executing a given task easier or more difficult than it would normally be on its own. Penalties or bonuses to your Attributes may be applied in any such situation, reducing or increasing the available number of dice to be rolled.

- **Table 1.1 - Target Numbers**

|TN |Difficulty |
|:-:|:----------|
|2  |Foolproof |
|4  |Easy |
|6  |Average |
|8  |Challenging |
|10 |Difficult |
|12 |Very Difficult |
|14 |Amazing |
|16 |Heroic |
|20 |Nearly Impossible |
|22 |The Unbelievable |

- **Table 1.2 - Degrees of Success**

|Margin of Success |Result |
|:----------------:|:------|
|Zero              |Failure |
|(Ties)            |Result Varies |
|One               |Narrow Success |
|Two               |Comfortable |
|Three             |Room to spare |
|Four              |Expertly done |
|Five              |Flawless |

>*Cameron, a young Stahlnish knight, has just rescued his one and only true love from his own wicked lord. As he flees from the dungeon (with a maiden over his shoulder!) the walls begin to collapse around him. Hoping to avoid being crushed by a falling slab of rock and debris, Cameron leaps through the dungeon entrance. The Seneschal calls for a Test ofAgility and assigns Cameron a TN of 9 (he considers this to be fairly challenging, as the rock is falling pretty fast). Additionally, the Seneschal notes, Cameron has a girl over his shoulder. Because Cameron is rather strong (and his fair maiden is rather thin) the Seneschal only imposes a -1 die penalty to Cameron's Agility. Cameron's player now rolls Agility (in this case 4 dice, 5 -1, for the penalty). He rolls 4, 6, 7, 9... one success! He narrowly escapes the falling dungeon walls. Had Cameron not been carrying his dearest love he would have had 5 dice, not 4, to roll.*

#### Skill Tests

Skill Tests are a specialized kind of Attribute Test. Skill checks are made by rolling an appropriate Attribute Test against the Skill Rating (SR) of that skill. The Skill Rating thus acts as a Target Number. Lower SRs are better than higher, as they allow more possible successes. See Table 1.3 for a generic listing of Skill Ratings.

As with Attribute Tests, die modifiers may be applied to any roll should the task at hand be easier or more difficult than "average."

#### Proficiency Tests

Proficiency Tests, or tests made from a Pool, are made by rolling any number of dice from the appropriate Pool (Combat Pool, Missile Pool, or Sorcery Pool) against Target Numbers based on the difficulty of using the weapon or magic at hand. Sample Target Numbers will be found in Book Three: Training and Book Six: Sorcery.

#### Contested Rolls

Contested Rolls are made whenever two characters or forces are competing for precedence. Examples include trying to defend yourself from an attacker, sneaking past a guard (or listening for such), and arm wrestling. Any of the above tests may be contested against any other: one player might roll a Skill Test while the second might roll an Attribute Test in opposition. In such instances both parties roll against their own Target Numbers as determined by the individual circumstances of their given Test. Successes are then tallied up; he with the most successes is the winner-the other is the loser. The loser's successes are taken away from the winner's, leaving the winner's Margin ofSuccess as an indication of how well he did. An example is provided below:

>*Mik, a Siehe roguish fellow, is trying to sneak past a royal guard on duty. The Seneschal asks Mik to make a contested Stealth roll (a Skill Test) against the Guard's Perception (PER). Mik rolls 6 dice-his Agility (AG)-against his Skill Rating of 6 (Mik's done this sort of thing before). He rolls 2, 2, 7, 8, 0, and 0 (Zeros are always read as 10V: four successes! The Seneschal simultaneously rolls the Guard's Perception (PER) -an Attribute Test- 4 dice against a TN of 7 (average difficulty, determined by the environment that the guard is listening in). The Guard gets 2, 5, 9, and 9: two successes. So Mik wins by two (a comfortable margin) and sneaks on by the guard...*

#### Hit Location

Hit Location rolls are made by rolling one (just one!) d6 and referring to the appropriate tables (see Book Four: The Codex of Battle).

#### Fumbling

Sometimes Tests don't just fail, they're completely botched. This is called botching or fumbling, and happens any time you fail a roll and have two or more " 1 's" showing on any die. The Seneschal will generally determine what the effect of any given fumbled roll is, though some situations, like combat, have pre-figured results. The only real guideline for fumbles is that they should be worse than a regular failure.

>*Mik, our little Siehefriend, is trying to convince a merchant that he didn't steal anything. The Seneschal asks Mik's player to roll Test of Soc/TN 8. Mik rolls 4 dice-his Soc Attribute-and gets 1, 1, 6, 7... no success and two 1's- a fumble! The merchant is going to react very poorly indeed.. .*


### - Attributes Described

Each character in The Riddle of Steel possesses a number of Attributes. Attributes represent basic natural and developed traits or aspects of your character, such as how strong he is, how well he gets along with other people, and what drives him to live and die. These important traits are divided into four groups: Temporal, Mental, Spiritual, and Derived.

#### Temporal

Temporal Attributes deal with the tangible, physical realm. An average human's ability is ranked at 4, while 10 is the maximum degree attainable.

- **Strength (ST)** is a measure of physical power and brawn, and has a great influence on damage dealt in combat, as well as some physical feats.

- **Agility (AG)** is a measure of nimbleness, dexterity, speed, and hand-eye coordination. Agility is a key element in all physically active characters such as warriors, thieves, and some entertainers.

- **Toughness (TO)** is a measure of physical grit and hardiness. A high Toughness protects characters from bodily harm.

- **Endurance (EN)** is a measure of general "fitness," and plays a large roll in any long-term physical activity.

- **Health (HT)** is a measure of one's immune system and healing capabilities.

#### Mental

Mental Attributes deal with the mind and thought processes. An average human's ability is ranked at 4, while 10 is the maximum degree attainable.

- **Will Power (WP)** is a measure of mental endurance and determination. This extremely useful virtue often means the difference in tight spots. This kind of personal determination and grit is best found in hardened soldiers and those fiercely dedicated to their causes.

- **Wit (Wit)** is a measure of mental reflex and sharpness, best exhibited in comedians and fencers. This trait is key element for both good fighters and those that deal in the cutthroat intrigue of Weyrth's royal courts and palaces.

- **Mental Aptitude (MA)** is a measure of how quickly one learns and how much they retain, exemplified by scholars, know-it-alls, and the finest pupils. This is not a measure of intelligence or cleverness-that's up to the player, not the character sheet! This trait has a great effect on skill advancement. This Attribute is especially important for Skill-based characters such as courtiers, thieves, and academics.

- **Social (Soc)** is a measure of how charismatic, empathetic, and culturally adept your character is. This is a crucial ability for entertainers, courtiers, leaders, wheelers, and dealers.

- **Perception (Per)** is a measure of alertness and awareness to one's surroundings. This attribute can warn your group of an impending ambush or a nighttime attacker, or help you find the keys to the king's secret passage... Woodsmen and rogues are often noted for their keen senses and Perception.

#### Spiritual

Spiritual Attributes differ from other attributes in many ways. There are no averages. These traits fluctuate often during a character's lifetime.

#### Derived

Derived Attributes, as their name suggests, come from a combination of other Attributes. These traits are used primarily in special situations, such as combat and sorcery.

- **Reflex** is a combination of Agility and Wit, and determines how quickly a character may physically react to external stimulus. Average Reflex is 4.

- **Aim**, extracted from Perception and Agility, quantify one's natural ability to hit a target over distances.

- **Knockdown** is a measure of how solid and balanced one remains after taking a blow. Average Knockdown is 4.

- **Knockout** is a measure of how hard it is to knock a character unconscious, based on Toughness and Will Power. Average Knockout is 6.

- **Move** is a measure of h ow much distance-in yards- one can cover on foot in approximately 1 or 2 seconds (one combat round). Average Move is 6.

>*A special set of Derived Attributes for Sorcerers is addressed in Book Six: Sorcery.*




## Training


### - Proficiencies

>p.54

Proficiencies, where combat is concerned, measure the amount of training, practice, and familiarity a character has with a given brand of weapon or fighting style. Proficiency covers the basic tenants of how such a weapon or combination is used, and what its effects are. Proficiencies represent relatively loose categories-a short club, for example, can be used with sword-related proficiency, based on weight and method of use. This assumes that the two items (swords and short clubs) are used similarly; it is conceivable that another proficiency could cover the club, or any weapon. What is important is how that weapon is being used-not necessarily what the weapon is. Use common sense and remember that the Seneschal has the final say.

The following descriptions cover most every weapon that a character may use or come in contact with in the course of play. Each entry consists of a description of what falls under that fighting style's "domain," followed by optional rules for using those weapons.

Maneuvers, listed under each Proficiency, are specialized uses of each given proficiency. Some maneuvers-such as most basic attacks- are automatic to anyone possessing even one point in the proficiency in question. Many maneuvers also have an "activation cost," paid from the Combat pool and found in parentheses to the right of each maneuver listed under a given Proficiency. Others must be learned through play-most of these are even then only available at certain levels of proficiency. Unless otherwise noted one may attempt any maneuver with some knowledge of the principles behind it. Maneuvers may be attempted at default for a penalty of 2 CP or more over the maneuver's original cost. In addition, many maneuvers work for different kinds of weapons (many sword maneuvers transfer easily to mass and pole weapons. The Seneschal has final say over any crossed-over maneuvers.

Defaults are similar to those in Skills. Some fighting styles are very similar to others, and each Proficiency lists what other styles are closely related. Defaults are signified with a negative Proficiency modifier based on which Proficiency the default originates from. Defaulted Profiencies may never exceed 6 without formal training (after which its no longer a defaulted Proficiency). Most maneuvers, unless very similar to those in the original Proficiency, are assumed unusable through Default. Defaults listed in the following section follow the weapon they originate from. Thus, if you need to find your default for Doppelhanders when your primary weapon proficiency is "Sword and Shield" look to the end of the Sword and Shield proficiency where Doppelhander is listed as -4 (meaning your Default Dopplehander proficiency is 4 less than your normal Sword and Shield Proficiency).

#### Sword and Shield

As long as the sword has been in use, the shield has been present to support it. This is one of the oldest fighting styles found in Weyrth, and is still taught in many schools. Throughout the world Gentry and Nobility are particularly fond of the sword and shield, finding it the perfect combination for knights both mounted and on foot.

This Proficiency assumes any one-handed sword and a medium sized shield (at least 18" across, but usually overt 2'). Primary advantages of the shield are its ease of effective use and its strong defense (allowing the sword to deliver a strong offence). This proficiency may also be used without a shield at no penalty, but any shield-based maneuvers are also impossible.

Offensive Maneuvers:

- Bind and Strike (0)
- Cut (0)
- Feint (variable)
- Simultaneous Block/Strike (0)
- Thrust (0)

Defensive Maneuvers:

- Block Open and Strike (2)
- Block (0)
- Counter (3, 2 w/o shield)
- Parry (1, 0 w/o shield)

Defaults:

- Case of Rapiers -4
- Cut & Thrust -2
- Dagger -2
- Doppelhander -4
- Greatsword -2
- Mass-weapon and Shield -1
- Pole-arms -4
- Pole-axe -4
- Pugilism/Brawling -4
- Rapier -4
- Wrestling -4

#### Greatsword/Longsword

As better armors came into use warriors began discarding shields in favor of larger swords. These longer blades have gained great popularity in Cyrinthmeir, Stahl, Oustenriech, and Xanarium over the last 200 years. They are elegant weapons, capable of cutting and thrusting and complicated maneuvering and countering. Greatswords, longswords, and bastard swords are effective weapons both in personal duel and on the battlefield.

The Greatsword Proficiency covers greatswords (roughly 4' 6" to 5' long), longswords (about 4' long), bastard swords (about 4' long, with a hard tapering blade) and estocs (4' long, unedged for halfswording), along with any other twohanded or hand-and-a-half variety. Primary advantages of these weapons include a long range, lots of cutting power, and a plethora of maneuvers.

Offensive Maneuvers:

- Beat (0)
- Cut (0)
- Evasive Attack (1)
- Feint (variable)
- Half-sword (variable)
- Stop Short (0)
- Thrust (0)

Defensive Maneuvers:

- Counter (2)
- Expulsion (2)
- Grapple (2)
- Half-sword (variable)
- Parry (0)

Defaults:

- Case of Rapiers -4
- Cut & Thrust -3
- Dagger -2
- Doppelhander -2
- Mass-weapon and Shield -3
- Pole-arms -2
- Pole-axe -2
- Pugilism/Brawling -4
- Rapier -3
- Sword and Shield -2
- Wrestling -3

#### Cut and Thrust

As longbows, crossbows, and primitive firearms began to make plate armor less practical a new form of combat evolved. This fighting style, the predecessor to the rapier, consists of a somewhat lighter version of the old-fashioned knightly sword, with a new emphasis on thrusting in addition to cutting. A companion weapon, such as a buckler, dagger, or arming glove usually complements this quick and deadly weapon. This style is popular in Gelure, Fauth, and -recently- in Cyrinthmeir, the Empire (Xanarium), and parts of Stahl and Sarmatov (with sabers especially).

This school covers all lighter one-handed swords such as basket-hilted broadswords, backswords, most sabers, sword-rapiers, and so on. The primary sword is almost always accompanied by a buckler, poniard/dagger, or arming glove (a leather and chain gauntlet for grasping and deflecting blades).

Offensive Maneuvers:

- Beat (0)
- Bind and Strike (0)
- Cut (0)
- Double Strike (0)
- Feint (variable)
- Simultaneous Block/Strike (0)
- Stop Short (0)
- Thrust (0)
- Toss (0)

Defensive Maneuvers:

- Block (with bucklers) (0)
- Counter (2)
- Expulsion (2)
- Grapple (2)
- Parry (0)

Defaults:

- Case of Rapiers -3
- Dagger -2
- Doppelhander -4
- Greatsword -3
- Mass-weapon and Shield -2
- Pole-arms -3
- Pole-axe -4
- Pugilism/Brawling -2
- Rapier -2
- Sword and Shield -2
- Wrestling -3

#### Rapier

The newfangled rapier is an urban descendent of the Cut-and-Thrust style, focusing entirely on the thrust aspect. This new weapon has just begun to gain popularity in upper circles of Xanarium, parts of Gelure, and in Fauth. There is a hot (and bloody) debate between those that prefer the more traditional longsword or cut and thrust swords, but the rapier is steadily growing in popularity as a civilian dueling weapon. It is relatively useless on the battlefield.

Rapiers are long, with blades sometimes reaching 4 feet. Despite their weight, superior balance makes them quick and deceptively deadly thrusting weapons. They are almost always used along with a companion weapon such as a cloak, a poniard/dagger, or an arming glove.

Offensive Maneuvers:

- Beat (0)
- Bind and Strike (0)
- Double Strike (0)
- Feint (1, may be performed from a thrust)
- Simultaneous Block/Strike (0)
- Stop Short (0)
- Thrust (0)
- Toss (0)

Defensive Maneuvers:

- Block (with bucklers) (0)
- Counter (3, vs. thrusts and for thrusting only)
- Expulsion (2)
- Grapple (2)
- Parry (0)

Defaults:

- Case of Rapiers -1
- Cut & Thrust -2
- Dagger -2
- Doppelhander -4
- Greatsword -4
- Mass-weapon and Shield -4
- Pole-arms -3
- Pole-axe -4
- Pugilism/Brawling -2
- Sword and Shield -3
- Wrestling -3

#### A Case of Rapiers

The capital city of Xanarium, the Seat of the Empire, recently developed this unusual style. A case of rapiers consists of one full-size rapier in each hand. Popularity for this style is beginning to grow.

This school's primary advantage is the extended range of both weapons-possible only due to the thrusting-only attack line of a rapier. On the downside, a swordsman is left without a smaller weapon in the case of suddenly-closing range.

Offensive Maneuvers:

- Beat (1)
- Bind and Strike (1)
- Double Strike (1)
- Feint (1)
- Simultaneous Block/Strike (1)
- Thrust (0)

Defensive Maneuvers:

- Expulsion (3)
- Parry (0)

Defaults:

- Cut & Thrust -2
- Dagger -4
- Doppelhander -4
- Greatsword -4
- Mass-weapon and Shield -4
- Pole-arms -3
- Pole-axe -4
- Pugilism/Brawling -2
- Rapier -1
- Sword and Shield -3
- Wrestling -3

#### Doppelhander

The true two-handed sword, an enoumous weapon of Stahlnish origin (the name means "double-hander"), is the latest addition to the battlefield. Meant for cutting down pike formations and other pole-armed infantry, this weapon resembles a spear more than a sword in actual use. It is popular in some Stahlnish armies, Gelure, and Ouestenriech.

Doppelhanders are generally about 6 feet long or more, with an 18" grip and a second cross-guard about 10" up from the first. They are fearsome weapons with great range, but easily overcome by any fighter quick enough to close range.

Offensive Maneuvers:

- Beat (0)
- Cut (1)
- Feint (variable)
- Half-sword (variable)
- Stop Short (0)
- Thrust (0)

Defensive Maneuvers:

- Half-sword (variable)
- Parry (0)

Defaults:

- Case of Rapiers -4
- Cut & Thrust -4
- Dagger -4
- Greatsword -2
- Mass-weapon and Shield -3
- Pole-arms -2
- Pole-axe -2
- Pugilism/Brawling -4
- Rapier -4
- Sword and Shield -3
- Wrestling -4

#### Pole-axe

This ancient weapon has proven effective and valuable throughout history. The pole-axe's primary purpose is cleaving through armor and shields, providing a powerful blow. Most knights and infantry have trained with this weapon. It is popular in every corner of the world in some form.

Pole-axes are generally 4 feet long, with a hardwood haft and a reinforced steel head. They are often adorned with a spike or hammerhead on the back or top or both. The pole-axe is especially useful against armored opponents. Because it requires the use of both hands, no shield is possible.

Offensive Maneuvers:

- Beat (2)
- Cut (0)
- Hook (1)
- Thrust (1)

Defensive Maneuvers:

- Counter (3)
- Grapple (2)
- Parry (0)

Defaults:

- Case of Rapiers -4
- Cut & Thrust -4
- Dagger -4
- Doppelhander -3
- Greatsword -2
- Mass-weapon and Shield -3
- Pole-arms -2
- Pugilism/Brawling -4
- Rapier -4
- Sword and Shield -3
- Wrestling -4

#### Pole-arms

This wide grouping of weapons encompasses items as ancient as the spear, as new as the pike, and as everlasting as the quarterstaff. Different forms are popular in different places-pikes in Ouestenriech, Gelure, and the Empire, halberds in Stahl, bills in Farrenshire, and spears and staves worldwide.

This school includes all staff or pole-mounted weapons over 6 feet long, though most range anywhere from 8 feet (spears and staves) to 14 feet (pikes). They have the advantage of range and (often) speed, but are hard to transport.

Available maneuvers vary greatly due to the design of each individual weapon. Use discretion and common sense.

Offensive Maneuvers:

- Bash (0)
- Beat (0)
- Cut (0)
- Feint (variable)
- Hook (1)
- Stop Short (0)
- Thrust (0)

Defensive Maneuvers:

- Counter (4)
- Parry (0)

Defaults:

- Case of Rapiers -4
- Cut & Thrust -4
- Dagger -4
- Doppelhander -2
- Greatsword -3
- Mass-weapon and Shield -3
- Pole-axe -2
- Pugilism/Brawling -3
- Rapier -3
- Sword and Shield -3
- Wrestling -4

#### Mass Weapon and Shield

Since the earliest clubs mass weapons have found their place in warfare. With the advent of better and better armors clubs and farming tools were transformed into battle-axes, picks, flails, morning stars, and maces. These weapons are in use the world-over, holding particular popularity with the peasant infantryman (these weapons are cheap) and the armored gentry (these weapons are especially effective against plate armor).

Mass weapons include any single-handed (and occasionally two-handed) weapon that is particularly heavy on the business end. Axes, maces, flails, and picks are good examples. These weapons' balance makes parrying especially difficult, and forces the use of constant evasion or-more commonly-shields. Additionally most axes and hammers are outfitted with a secondary spike or hammer on the back and/or top. To flip the weapon over spend 1 CP if done during a round. As with Pole-arms, exact maneuvers are dependent on the weapon at hand. Use discretion and common sense. This proficiency may also be used without a shield at no penalty, but any shield-based maneuvers are also impossible.

Offensive Maneuvers:

- Bash (0)
- Bind and Strike (0)
- Cut (0)
- Hook (1)
- Simultaneous Block/Strike (0)
- Thrust (1)

Defensive Maneuvers:

- Block Open and Strike (2)
- Block (0)
- Parry (1)

Defaults:

- Case of Rapiers -4
- Cut & Thrust -3
- Dagger -3
- Doppelhander -4
- Greatsword -3
- Pole-arms -4
- Pole-axe -2
- Pugilism/Brawling -4
- Rapier -4
- Sword and Shield -2
- Wrestling -4

#### Dagger

This small, easily hidden weapon comes in many forms. Found in every place in the world, daggers and knives are used as tools and instruments of death. Daggers are extremely popular civilian weapons, and are carried by almost everyone.

This Proficiency covers stilettos, poniards, rondels, and all manners of short stabbing and slicing blades. This Proficiency is directly attached to street brawling and wrestling due to the short range of daggers and the like.

Offensive Maneuvers:

- Cut (0)
- Grapple (2 or 4)
- Kick (2)
- Punch (1)
- Thrust (0)

Defensive Maneuvers:

- Grapple (2)
- Parry (0)

Defaults:

- Case of Rapiers -4
- Cut & Thrust -4
- Doppelhander -4
- Greatsword -4
- Mass-weapon and Shield -4
- Pole-arms -4
- Pole-axe -4
- Pugilism/Brawling -1
- Rapier -3
- Sword and Shield -4
- Wrestling -1

#### Pugilism/Brawling

The one weapon everyone has is his or her own body. Variations of unarmed combat exist all over the world. This version is popular throughout the western half.

Pugilism and Brawling consist of punching, kicking, grappling, and dirty tricks. These weapons are infinitely portable, but ineffective when compared to a "real weapon".

Offensive Maneuvers:

- Grapple (2 or 4)
- Kick (1)
- Punch (0)

Defensive Maneuvers:

- Grapple (2)
- Parry (0)

Defaults:

- Case of Rapiers -4
- Cut & Thrust -4
- Dagger -1
- Doppelhander -4
- Greatsword -4
- Mass-weapon and Shield -3
- Pole-arms -4
- Pole-axe -4
- Rapier -3
- Sword and Shield -4
- Wrestling -1

#### Wrestling

Wrestling, though popular as a sport, is really the most desperate of all battles. As with Pugilism and Brawling, Wrestling is always an option for the unarmed character. Aditionally, many fighters find themselves unarmed on the ground in the course of battle or a duel (the Dagger, Greatsword, and Poleaxe schools are particularly fond of this).

The following maneuvers are by-products of training as a wrestler. They are generally used to enter a wrestling situation, not used during.

Offensive Maneuvers:

- Grapple (1 or 3)
- Punch (1)
- Kick (2)

Defensive Maneuvers:

- Grapple (1)

Wrestling, in addition to maneuvers, has special rules once combatants hit the ground. When wrestlers hit the ground both are assumed to be "Free," meaning that they have use of most of their body. Both contestants then divide their Combat Pool (derived from Wrestling, of course) into offense and defense. Any dice not allotted to one category or the other are lost. Both players throw their offense in a contested roll against the other's defense, against a Target Number equal to the opponent's ST +1/ 10lbs over your weight. Thus if wrestler A has ST 5, and weighs 180lbs, and wrestler B has ST 4 and weighs 250lbs, then wrestler A's TN is 11 (4 + 70/10) and wrestler B's TN is just 5.

If you win both contests, then your opponent becomes "held" (or pinned if he's already held) and loses 1/4 ( 3/4 if pinned) of his total CP. Alternatively you may improve your own condition (from pinned to held, from held to free) instead of making your opponent's worse. If you win one contest, then your opponent remains free, as do you. If you lose both contests, then you are now held (if free) or pinned (if held). Only one character may be pinned at any given time. If you successfully pin whomever is pinning you, then you become held and he becomes pinned.

These rolls are made every round until one opponent is killed or yields. Small weapons (including punches and kicks) may be used by any free character out of the Wrestling Combat Pool. Any such attacks are made after the two contests are rolled with remaining dice from the original pool.

#### Lances

As fighting from horseback became more and more practical, efficient, and popular, heavy cavalry came into being. Their weapon of choice for the opening charge is the lance. This weapon is found anywhere knights or heavy cavalry are.

Lances range from 8' to 14' in length, depending on the technological advancement of any given area. They are usually butted under the right arm and used with a shield. Lances, though made of wood, are usually capped with a long steel or iron point. A sporting version, the jousting lance, has a blunted tip for tournaments. The primary advantages to a lance are its range and power in a charge, where it uses the mount's ST, not the rider's.

Offensive Maneuvers:

- Simultaneous Block/Strike (0) used in a charge (mount's ST)
- Thrust (1) used when stationary (rider's ST)

Defensive Maneuvers:

- Block (0)

#### Missile Weapon Proficiencies

Missile Weapon Proficiencies are not nearly as complicated as hand-to-hand Weapon Proficiencies. These Proficiencies go towards building the Missile Pool (see Book Four). There are no attached maneuvers, though each has a few defaults. These Proficiencies are:

- Bow (including long and short varieties; defaults to crossbow at -3)
- Crossbow (all sizes; defaults to Bow at -4)
- Darts (defaults to Javelin -3)
- Slings
- Spear/Javelin (defaults from Polearm -3)
- Thrown Knives
- Thrown Axes, etc.
- Thrown Rocks/irregularly shaped objects (defaults from any other Missile Proficiency at -3)




## Combat


>p.77


Combat in The Riddle of Steel is unlike any combat system you've ever seen. There are no hit-points, no initiative rolls, and as little abstract thought as possible. Instead this system is based on years of hands-on martial research and training. Though still a game, it is closer to representing real fighting than any RPG combat system ever written. A few words of advice are then in order: (a) never get hit...ever! You probably won't recover. (b) Use your head. Here, as in the real world, fights are won very much through strategy, not just high "stats" or big swords. (c) Even the smallest weapon is deadly. Would you want to get stuck with a knife? Neither would your character. (d) Teamwork, teamwork, TEAMWORK!!!... need we say more? (e) There's a fine line between brave and stupid. Don't be stupid. (f) Have a back-up plan, or a good idea of what your next character should be like. It's up to you.

To illustrate, picture three new characters walking along the road. They hear some noise up ahead and see five burly troll-like guys (some folks call them gols... read about them in Book Seven). Should our three heroes charge them head on? Not in The Riddle of Steel! Five on three is bad odds (just think back to wrestling your friends as a kid). So instead our Heroes hide in the bushes and concoct aplan (heaven forbid! A plan!). They decide to rush out of the bushes as these trolls pass, striking three of them down before they even know what hit them. That puts the odds at three on two in their favor... much better! Remember that it only takes one hit to ruin your character's day...forever.


### - Melee Combat

Melee combat is often referred to as hand-to-hand fighting. It involves swords, axes, fists, shields, and footwork. All melee combat is based on one major set of dice: the Combat Pool (each point in you Combat Pool equals one die). The Combat Pool (CP) is used to activate certain maneuvers, to attack, and to defend. Over the course of a duel or bout you will divvy up and refill your pool several times. Factors such as wounding and external circumstances will cause your pool to fluctuate. We recommend using two small bowls to keep track of how many dice are in your Combat Pool at any given time, and how many have been spent.

#### The Flow of Time in Combat

Whenever a situation involving combat begins the Seneschal declares that time is now measured in Combat Rounds. A Round in The Riddle of Steel lasts approximately 1 or 2 seconds, during which attacks are made and other events take place. Each Round is then divided into two Combat Exchanges (sometimes called an "Exchange of Blows"). An Exchange, being half a Round, is roughly how much time it takes to attack with a weapon and begin to recover. A whole series of Rounds constitutes a Bout, or a fight. Bouts occasionally undergo a pause, such as when opponents circle or break apart from one another. Though these pauses are still measured in Rounds, one may assume that these Rounds last much longer than 1 or 2 seconds.

#### The Order of a Round

The order of one melee combat round is summarized in the following steps:

1. Declare stance.

2. Establish aggressor and defender (initiative). Combat Pool fills or refreshes, with all Modifiers.

3. First half of the "Exchange of Blows"; aggressor attacks and defender defends.

4. Resolve damage and/or determine new attitude (aggressor or defender).

5. Second halfofthe "Exchange ofBlows;" aggressor attacks and defender defends. These roles may have reversed since the first Exchange.

6. Resolve damage and/or determine new attitude (aggressor or defender).

Steps 2-6 continue to repeat until the end of the Bout or the flow is interrupted (a pause). At the beginning of a new Bout (or following a pause) begin at step 1, then repeat 2-6 as normal.

#### Steps 1 & 2: Initiative

Very rarely do both opponents attack simultaneously-that kind of thing might look neat, but it's not for those who wish to live a long time. At the beginning of a melee, duel, Bout, or any kind of combat mess each character must secretly decide whether they are going to attack (assuming the attitude of an "aggressor") at this time or hold back and see what their opponent plans (taking the roll of a "defender"). If enough time is present, both parties should declare a stance-this is done out loud, and should be done without hesitation (see Stances, below, for more).

Now remember those red and white dice that we recommended in Book One? Here's where they really come in handy. When time slows to Combat Rounds each active character takes one red die and one white die into their hands. After stances are declared (if there's time), the Seneschal calls out "throw," and each combatant drops one of those two dice onto the table- a red die indicates aggression and a white die indicates defense. If a combatant fails to throw any die down, then that character has hesitated and may only defend for one full Exchange (see Surprise, below).

Each aggressor, in order from lowest Reflex to highest, now declares how many dice from his Combat Pool are being spent on the attack, where the attack is aimed, and what maneuver-if any-is being used. Defenders now-in response to their attackers-declare how many dice from their Combat Pool are going towards defense, what kind of defense they're using, and what maneuver- if any-is being used.

Matters grow especially messy should both combatants attack (throwing a red die). A contest of Reflex (with the combattant's ATNs for Target Numbers-see "Exchange of Blows" and the Appendix for weapon ATNs) determines who's strike lands first. Remember that no defense is possible in the middle of an attack, so the loser of this contest usually ends up dead.

This process (red ad white dice) is only used at the beginning of a Bout or following a Pause (see below). In all other rounds the winner of the previous exchange either takes or retains initiative.

Stances provide stronger attacks and defenses at the cost of predictability and flexibility. While each school has its own stances and positions that it claims are better, three are universal (at least where game purposes are involved-appearance or exact position may be more varied): Aggressive stances, Defensive stances, and Neutral stances.

- **Aggressive stances** provide a bonus to attack while making defense more difficult-the classic samurai with katana raised high is a good example. Such stances add 2 CP dice when attacking but increase the Activation cost for any Defensive Maneuver by 2 CP.

- **Defensive stances** provide a bonus to defense while making attacking harder-most stances with the blade or weapon head facing down are good examples. Such stances add 2 CP dice when Defending but increase the Activation cost for any Offensive Maneuver by 2 CP.

- **Neutral stances** allow a great deal of flexibility, and are favored by many schools. In a Neutral stance the sword (or other weapon) is positioned to allow a quick attack or defense. Such stances provide no bonuses to either attack or defense.

Stances only last until the first blow or movement-if a character's weapon moves, the stance is broken. Very few swordsmen can evade an attack and retain their stance. During the Bout itself the stances of both attacker and defender are considered to be neutral-no bonuses either way. A stance cannot be taken (or re-taken) until the combatants separate and cease swinging for a moment-the Bout must be interrupted by a full pause.

**Surprise** is a common event-even when one is aware of an enemy. Sometimes a character is unprepared for an incoming attack-perhaps they are surprised or hesitated (so you better throw a die at the beginning of the melee, huh...). To check for surprise or in a similar situation the unsuspecting character rolls Reflex against a TN based on how alert the character was. Table 4.1 suggests guidelines-the Seneschal has the final say.

Failure indicates that no action can be taken until next round. Success-even one-means that you may defend (or attempt to buy initiative).

- **Table 4.1 - Surprise and Hesitation Target Numbers**

|TN |Description |
|:-:|:-----------|
|5  |Purposely standing with no stance (perhaps to invite an attacker...). |            |Failure |
|7  |Aware of opponent -Victim of a cheap shot or you hesitated. |
|10 |Unsuspecting or inattentive. |
|13 |Blinds ided! |

**Buying initiative**, or the preemptive strike, is the art of the samurai or the gunslinger (in our case, Bladeslinger)-waiting for your opponent to strike, then attempting to beat him to the blow. It's a risky move, used by the desperate, the quick, and the suicidal.

Initiative is usually bought in one of two circumstances. The first is when two combatants attack simultaneously. The loser of the previous contest for initiative may attempt to buy initiative by spending a number of dice equal to his opponent's Perception (Per). A contested roll is then made of the buyer's WP and his opponent's Wit, with both TN's being equal to the other's Reflex. The buyer (and only the buyer) may opt to raise his opponent's TN by one for every extra Combat Die spent, to a maximum bonus of his own Wit. The winner of the contest strikes first-the loser may strike second, assuming any dice remain in his Combat Pool.

The second situation where one may wish to buy initiative is when a character who has previously declared defense wishes to attack instead. The process is identical to that above.

In either case both attackers have committed themselves and may not withdraw, which may cause a real mess.

**Taunting** is the last resort of a fighter who is tired of circling and circling (the effects of both sides throwing white dice and declaring "defender" for several rounds in a row). By taunting, one can force one's opponent into becoming the attacker. This is accomplished by trying the patience (WP) of your opponent through insults and the like. This is not possible until both opponents have circled for at least 5 rounds.

This effective tactic is handled with the "Ridicule" skill (see Book Three: Training), which may be defaulted for those without the skill.

#### Steps 3 & 5: Exchange of Blows

Each Round -as discussed above- consists of two "Exchanges." During each individual Exchange each party gets one "action," meaning that one usually attacks and the other defends (usually...). Every attack must be directed at a specific location-numbered with Roman numerals I-XIV (see Table 4.2). I-VI are for swinging attacks; VII-XIV are for thrusting or missile attacks. It is not necessary for players to declare the exact number, but the location and variety of attack (overhand slash, upper-hand slash, thrust for the face, &c.) are completely necessary-no one attacks randomly but those who hesitate! Maneuvers and targeted locations may change difficulties for either the attacker or the defender.

- **Table 4.2 - Hit Location Zones**

![Hit Location Zones](https://i.imgur.com/91cd1fQ.png)

Both combatants-aggressor and defender-now roll the allotted Combat Dice (from Step 2), with all modifications from maneuvers, stances, or other factors.

This is a normal contested roll, where the loser's successes are subtracted from the winner's, leaving the winner with the Margin of Success. All Target Numbers for this roll come from the ATN (Attack Target Number, used when attacking) or DTN (Defense Target Number, used when defending) listed with each weapon (see the Appendix). The winner, after damage is resolved, may press the advantage by taking or keeping initiative (i.e. the roll of aggressor) if he so chooses. If the defender successfully defends, then he may take the role of the aggressor. Ties, while no damage is dealt, are considered to go to the attacker for purposes of initiative. Such ties usually imply that the defender has just barely deflected the strike, and is still recovering from the momentum of the blow that he just blocked, parried, or dodged (see Defense, below).

Finally, should the loser wish to attack during the next Exchange (normally impossible), he has two choices. First, he may simply declare an attack. The winner attacks and resolves damage first; if the loser has any dice left, he can attack. This is a foolhardy maneuver. The second choice is to buy initiative, as per Step 2. This requires no additional rolls other than the standard process of buying initiative (see above).

Multiple opponents may be dealt with by simply dividing up one's Combat Pool between the two opponents in any proportion. Wise fighters will try to move around so that they only have to face one opponent at a time. Doing so acts as "Terrain" (below). Success in such a situation allows you to fight just one opponent at a time. Failure indicates that one more opponent has gotten through (forcing you to split your CP). Fumbling brings two more opponents on you (forcing you to split your CP three ways). No more than three opponents may engage a character at a time (just imagine six guys in a tight circle swinging sharp objects every which way trying to hit you but not their buddies). Exceptions to this rule include groups of spearmen and the like, who may gather tightly together and thrust, group chases (handled through the Terrain mechanic, below). Even two or three opponents will drain your Combat Pool very quickly. Encounters with odds like this should be avoided at all costs.

Terrain, or the ground that you're fighting on, has an immense effect during any melee encounter, whether it's a battle or a duel. Each terrain carries a TN, against which any number or CP dice may be allotted and rolled. This TN varies with how quickly one wishes to move. All characters engaged in combat are assumed to be "hurried" or even "sprinting" when attacking, and "normal" when defending. Characters not in combat may set their own pace, and make the rolls with AG instead of CP dice. Successful terrain rolls mean that the character's actions may go on unhindered. Failed rolls lead to disaster-a slip, a fall, a stuck weapon, resulting in the loss of half of one's CP for the duration of the round. Fumbled rolls mean a nasty fall or other disaster, removing all CP dice and leaving one prone or otherwise momentarily disabled.

- **Table 4.3 - Terrain Modifiers**

|Terrain                       |Stand/Crawl |Cautious |Normal |Hurried |Sprinting |
|:-----------------------------|:----------:|:-------:|:-----:|:------:|:--------:|
|Solid/Flat                    |0           |0        |0      |0       |0         |
|Narrow (ledges, walls, roofs) |1           |2        |3      |5       |7         |
|Swampy or rocky               |2           |3        |4      |6       |8         |
|Ice/Slippery                  |2           |4        |6      |8       |10        |
|Tight spaces (to get stuck)   |2           |3        |4      |5       |6         |
|2 opponents                   |n/a         |n/a      |n/a    |6       |5         |
|3-5 opponents                 |n/a         |n/a      |n/a    |8       |6         |
|5-10 opponents                |n/a         |n/a      |n/a    |9       |7         |
|10+ opponents                 |n/a         |n/a      |n/a    |10      |8         |

Visibility effects combat as well. In near-darkness (dusk) reduce all Combat Pools by 1. At night (just moonlight) reduce all Combat Pools by 1/4 total. In pitch darkness reduce all Combat Pools by half. These penalties obviously do not apply to those that can see just fine in the dark.

Higher Footing (such as stairs or horses) adds 2 CP at the beginning of the Round (Step 2). This also effects availability of targets for all involved combatants according to common sense.

Weapon Length, or reach, often means the difference between striking one's enemy and missing them entirely. Weapon length has been divided into six categories:

1. Hand (fists, daggers, knee-strikes, grappling) less than I' reach

2. Short (hatchets, short swords, long knives) less than 2' reach

3. Medium (arming swords, flails) less than 4' reach

4. Long (greatswords and bastard swords, spears) less than 6' reach

5. Very long (long spears, polearms) less than 8' reach

6. Extremely long (pikes, lances) everything over 8' reach

Attacks against a longer weapon are made at -1 CP for each "step" (i.e.: Short to Medium, or Medium to Long) the attacker wishes to close- attacking a Pikeman with a dagger would cost 5 CP! This penalty holds until the shorter weapon makes a damaging strike, after which the penalty transfers to the longer weapon until he scores a damaging blow, when again the penalty goes to the shorter weapon. When the shorter weapon is out of range this penalty applies only to attacks; when the longer weapon is penalized, it applies to both offense and defense.

Often the best course of action for long weapons is to use a full evasion in the event of sudden close combat, or to drop weapons and wrestle! Certain maneuvers (such as the Half-sword) are particularly effective in such situations. Likewise, many weapons-such as spears and other two-handed weapons-may shorten their reach.

Fumbling (or "botching") results in failure to strike anything and allows the weapon's momentum to lose control. Botching on any attack may cause the weapon to drop, break, or otherwise be rendered useless. Botching reduces your Combat Pool on the next exchange by half the number of dice you spent on the failed attack.

#### Steps 4 & 6: Damage

Should an attacker have one or more successes over his opponent then the attack has landed (see Steps 3 & 5), and a d6 is rolled to determine specific location within the targeted zone (i.e. I-XIV, as seen in Table 4.2). Generally speaking lower rolls mean lower hits, higher rolls mean higher hits. Exact location is important, as in The Riddle of Steel there are no hit-points or generic damage-every hit causes a very real wound. As such, the only way to kill a man is to deal a fatal blow or let him bleed to death. Wound tables are found in the Appendix.

Damage is determined by taking the number of final successes in the Margin and adding to that number the Damage Rating (DR) of the weapon being used. Most melee weapons have a DR of ST-2 to ST +3; other factors, such as sorcery, may affect this quantity as well. The defender-now struck-subtracts his own TO and Armor Value (if any, depending on location) from the attacker's total damage. The result is the Wound Level, which is cross-referenced on the Damage Tables (see Appendix) according to type (cutting, puncturing, or bludgeoning) and location (1d6 per zone...see above).

Armor, a rare and expensive commodity, only provides protection over those areas which it covers. Hence, if someone wearing a helmet is struck in zone IV (see Table 4.2), and the attacker rolls a 4, the neck (see Appendix), then that helmet provides no protection whatsoever! The following example should help:

>*Geralt, a well-practiced (but poor and unarmored) bladeslinger and swordsman, is dueling at swords with Felix, < a lightly armored knight. Geralt has a ST of 5, TO of 4, and his Combat Pool is 15; Felix has a ST of 6, TO of 5, and a Combat Pool of 12. Felix, having initiative from a successful parry, strikes at IV from his own right-hand side with 7 dice (an ambitious attack). Geralt, wisely knowing how important it is that he not get hit, assigns 9 dice to his defense-a parry from below. Both combatants throw their allocated dice: Felix, the attacker, gets 4 successes; Geralt rolls 5 successes and parries the blow. Because of his successful defense Geralt now has initiative, and thus opts to attack area IV from his own left (notice the fluid motion from parry to attack-this is intentional!), spending his remaining 6 dice. Felix realizes his mistake, and throws the last 5 of his dice in for a parry from the side. Geralt scores 5 successes and Felix scores only one, leaving Geralt with a Margin of 4 (this looks really bad for old Felix). Geralt's weapon has a Damage Rating of ST +1, so the sub-total (or Wound Rating) is 10. Rolling location on a d6, Geralt scores a direct hit to the neck -Felix is unarmored there! Felix then subtracts only his TO (5) from the subtotal... 10 - 5 = 5. A level 5 wound anywhere is nasty and usually fatal-in the neck it kills instantly (decapitation does that...).*

Wound levels are rated from 0 to 5. A 0-level wound is usually a bruise or scratch, with no notably adverse effects. Level one wounds are a tad bit worse, level threes are quite dangerous, and level fives are usually fatal or close to it. Location always makes a difference as well, as a level three to the head is a lot worse than to the shoulder. Wound levels are not cumulative (though the Seneschal may rule that several smaller wounds to the exact same location may add up in some way)-it takes a solid hit to do a man in. As is evident from the above example, only one solid hit is necessary for a kill-skill, luck, and strategy are all needed elements in the fight for survival...so choose your battles wisely.

Damage Types are an important part of the damage that a weapon does. Barring sorcery, fire, and other sources of damage, the most common are listed below.

Most melee and missile weapons are counted as one of three types: those dealing cutting wounds, puncture wounds, or blunt (or bludgeoning) wounds. Cutting wounds are the most common (swords being what they are), although the other types have certain advantages.

Puncture wounds, caused by spears, arrows, sword points, and the like, do more internalized damage, and are generally deadlier than cuts. They use the thrusting locations (XI-XIV). Bleeding from puncture wounds is harder to stop than from cuts, as much of it is usually internal.

Bludgeoning wounds, caused by hammers, clubs, pommels, quarterstaffs, and the like are the least damaging though still deadly. The primary advantage is that when striking the shoulders or pelvic region they can get a kill with a level 5 wound, as crushed bone severs arteries and other vessels. They also incite a high shock number (even against armored opponents-see Knockout, below), representing the momentum of the blow and blunt trauma. Bludgeoning attacks are usually targeted for the arced areas (I-VII), although weapons such as a quarterstaff or fist may be targeted to "forward," or "thrusting," areas (VIII-XIV).

Finally, for fire, electricity, cold, and other more abstract forms of damage there is a "Generic Damage Table" (see Appendix).

**Shock** and **Pain** measure how badly a given wound immediately effects one's ability to fight and carry on. Every wound has its own rating for both.

*Shock* subtracts dice from all your dice pools (both combat and sorcery related pools) immediately upon receiving the blow. Should the Shock rating exceed the total current CP, the remainder of the penalty is applied at the beginning of the next round unless the "Pain" penalty is greater. Shock is only applied once for each wound, unless that same body part is struck a second time.

*Pain* is subtracted from one's pools at the beginning of every Combat round. Where shock is a static number, one's WP can reduce the effects of pain. Pain is also important in figuring out healing times, as described in Book Five: The Laws of Nature.

Shock and pain are cumulative as long as they are applied to different zones. Should one attack the same area multiple times, the highest shock penalty -new or old- is applied (or re-applied) to the wounded party. It can be beneficial to attack a well-wounded area overand-over, even with weak attacks -the original shock penalty will keep on returning to haunt the wounded!

When recording wounds on your character sheet, make a note of where the exact wound is and what effects it may have, such as shock, pain, and blood loss (BL). We use notation such as "level 3 cut, VI-4 left," meaning "level three cutting wound at zone VI, section 4-from the d6 roll-on the left side; or just write " lvl 3 cut left neck." Make sure to record the Pain rating as for each individual wound as well. Hopefully receiving a wound will be a rare occurrence, meaning that you won't need to write this information down too often.

**Blood Loss (BL)** covers the deterioration of one's health due to bleeding and internal damage. Each wound carries, alongside Shock and Pain, a Blood Loss Target Number. These numbers are cumulative -adding up to one big BL number- so long as the wounds come from different parts of the body. At the beginning of each Round all wounded characters must roll Endurance (EN) vs. Blood Loss. Whenever a roll is failed one point of Health (HT) is temporarily lost. When HT reaches 1 all Attributes and Pools are halved. When HT reaches zero (0), the character enters coma and dies. Assuming the wounded party lives, one point of HT is recovered for every day of rest.

**Knockdown** and **Knockout** are covered by the derived statistics of the same name.

Any time a character receives a blow that through pain or shock reduces his CP to less than zero he may be knocked down. In such an instance roll the Knockdown trait against a TN equal to twice the number of your attacker's Margin of Success (counting after your defense, but before armor); the TN is three times your attacker's successes if struck with a mass weapon. Many blows to the head or legs may also call for Knockdown rolls (see Appendix). The TN for such rolls is equal to the Shock TN, without any WP modificaitons.

Knockout rolls are covered by the Knockout trait. Such rolls may be called for due to certain wounds (especially blows to the head-see Appendix) or under other circumstances under Seneschal discretion. Such rolls are made against a TN of 7, modified according to the severity of the wound (again, as outlined in the Appendix). Failure of one of these rolls results in 1d10 seconds of unconsciousness; fumbling such a knockout roll result in 1d10x10 minutes of unconsciousness.

Optionally, Knockout rolls are also called for every time a character receives a blunt blow to the head, whether armored or not at a TN of 10-AR (armor rating). Such blunt trauma causes a shock penalty equal to half of the attacker's damage (that's DR + attack successes).

Each success on this Knockout roll drops the Shock by one die. This modifier is not in addition to normal shock and pain modifiers inflicted by telling wounds. The only result of failure is that the Shock value is not lessened- one does not lose consciousness unless real damage is dealt! This rule helps simulate both fist fighting and the disorienting but not damaging blows that a helmeted combatant may receive to the head.

Fatigue is an optional rule that simulates the wear and exhaustion that comes from constant physical exertion. Those characters wearing heavy armors (those made of metal or that cover most of the body) lose 1 CP every EN Rounds. Thus Sir Vhord, who has an EN of 4, loses 1 CP for every four uninterrupted Rounds spent fighting or otherwise physically active. Those persons wearing little or no armor lose 1 CP every 2 x EN Rounds. Sir Vhord, fighting unarmored this time, now loses 1 CP every 8 Rounds. These points may be recovered by taking a few seconds to rest and rolling EN/TN 6. Each success reduces Fatigue by one point. For more on Fatigue see Book Five: The Laws of Nature.


### - Missile Combat (Ranged Weapons)

Missile weapons-such as bows or thrown weapons-work differently from melee weapons. When using a missile weapon all attacks are made from the Missile Pool (MP), which refreshes and is used differently from the Combat Pool.

The Missile Pool refreshes-or fills up-in "segments" equal to a character's Wit score at a rate of one "segment" every round. Naturally, refreshing begins at zero. This represents how quickly the character can process his surroundings before taking a shot. The more time one has to aim and prepare, the more accurate his shot is going to be. A pool can never refresh to a higher number than the pool's own maximum: if a character's MP is 9, it can only refresh up to a 9-any additional points or segments are lost. Once a shot is fired the refresh must begin anew, starting at zero.

MP refresh begins as soon as a weapon is in position; for arrows, that means as soon as they're knocked, for knives, as soon as they're blade-down in the throwing character's hand. Most weapons therefore require a small amount of preparation time (especially between shots) before they can be fired. This time may be lengthened or shortened by circumstances or by sheer haste or hesitation. Generally preparation time may be reduced by one round by spending dice from one's MP and making a successful Reflex test. MP costs and TNs for hasty preparation are found in a weapon's statistics.

Range is also an important factor when dealing with missile weapons. In The Riddle of Steel range is measured in increments. If a weapon has a Range Increment of "+ 1 per 10 yards," then the target number increases by one for every ten yards, rounding down. Thus all shots made at 0 to 9 yards are made at a weapon's base TN, all shots at 10 to 19 yards are at + 1, and so on.

The statistics for a missile weapon appear thus:

*Standard Short Bow*

- 2-4 rounds preparation time - Pull arrow: 0 (on ground), 2 (from quiver);
knock and draw: 2
- Refresh begins with drawing the arrow.
- 2 MP dice to reduce prep. time by one second at Reflex/TN of 8
- Attack Target Number (ATN): 6
- Effective ST: 4
- DR (damage rating): ST + 1 p (5 total)
- Range: +1 ATN per 10 yards

Common sense should always be a factor, especially when determining preparation time. A previously knocked arrow would have a total preparation time of 1 or even zero, while a character with his crossbow in his backpack would be way behind.

Most missile weapons will use the piercing tables and areas (VII - XIV. Damage Ratings are based on the strength of the user (knives, javelins), or a built-in strength (for bows and crossbows, called the "Effective ST"). Bows cannot be used by someone that lacks the strength to pull them.

Moving targets are especially troublesome. Any target that is moving erratically costs 3 MP to hit in addition to a normal attack. Those targets moving at any constant rate -no matter how fast-require the expenditure of only 2 extra MP dice.

>*Lira, a Dardanian freedom fighter (MP 12, Wit 6), has come to Otamarluk to assassinate the Sul'taan. Taking a perch on a rooftop across from the palace entrance (about 25 yards away), she prepares her short bow and sticks three arrows into the earthen roof. Some time later the Surtaan himself exits the palace surrounded by guards. Lira immediately grabs an arrow and knocks it (2 rounds). Feeling that she has enough time she takes careful aim at the Sul'taan's chest, waiting 2 more rounds for her MP to fill to 12 (at 6 dice per round). She removes 2 dice from her MP (because the Sul'taan is walking, an example of "constant" movement) and uses the rest (10 dice) for her shot. Her ATN is 6 for the bow, plus 2 for the range (25 yards), for a final ATN of 8. She throws her dice: 1, 2, 3, 4, 5, 5, 7, 8, 8, 8... three successes. Her damage level is 8 (5 for the bow, plus 3 for her successes), the poor unarmored Sul'taan's TO is 5, leaving him a level 3 wound in the torso as the arrow strikes him. Rolling a d6 on Zone XII Puncture Damage Chart (in the Appendix) she gets a 2 - just below the ribs - which reads: BL: 10, Shock: 8, Pain: 10-WP, "Belly wound- internal bleeding is going to be a problem." The Surtaan reels back, spouting blood everywhere. Lira wants to see him dead, though, and reaches for another arrow. This time she's in a hurry as palace guards scatter to protect their liege. She opts to attempt reducing the preparation time by one second, and rolls Reflex/TN 8 (her reflex is 6, and she manages a lucky 3 successes). Reaching for the arrow sticking from the rooftop takes no time, and her haste has reduced knocking time from 2 rounds to I. After a total prep time of only one round her pool begins refreshing. She holds her aim for one round (giving her 6 dice) and releases the second arrow at the bleeding Sul'taan's belly. 2 of her 6 dice are removed because of her hasty preparation, but none for movement (the Sul'taan isn't walking at present... he's just lying there). Lira's player rolls the remaining 4 dice (vs. TN 8): 4, 4, 6, 7... miss! Lira must now choose between firing that third arrow, or making her escape before the palace guards catch up to her.*


### - Defense

Defense is the art of how not to get hit. Defense can be active-dodging and evading, parrying, or blocking, or it can be passive-hiding behind a tree or wearing armor.

#### Evasion (Dodging)

Evasion -also called voiding or dodging- is the most natural of all defenses. The advantage to dodging is that it leaves your weapon completely open for a strike as your opponent's is whistling through the air. The disadvantage is that it's difficult to actually pull off. Any quantity of Combat Dice may be spent in dodging; the TN depends on what the evasive party is exactly trying to pull off. Table 4.4 explains and provides TNs for three forms of dodging.

Two forms from Table 4.4 are particularly noteworthy: Full Evasion and the "Duck and Weave." Full Evasion brings the combat to a pause as combatants separate and must re-establish initiative. The "Duck and Weave" places the evading party in a prime place to attack from: shields and weapon-length bonuses are lost or rendered useless, allowing the dodger a clean shot that may only be parried or dodged.

Stances have no effect on the full evasion, but work normally with Partial Evasions and the "Duck and Weave."

Fumbling a dodge is bad news, sending you either stepping into the blow or tumbling to the ground. Add 2 levels to any damage done.

Dodging, using dice from the Combat Pool, may also be used as a defense against incoming missile attacks that you are aware of Unexpected attacks provide no defense. When not engaged in hand-to-hand combat use the Reflex Attribute for dodging.

- **Table 4.4 - Evasion Target Numbers**

|TN |Description |
|:--|:-----------|
|4  |Full evasion or retreat. No attack possible for either party on the next exchange. This pause in combat calls for a new initiative. Evasion may not be attempted on an exchange immediately following one's own attack. |
|7  |Partial evasion. May take initiative as the aggressor on the next exchange by paying 2 CP dice, or if opponent fumbles or fails completely. |
|9  |"Duck and weave." May attack on the next exchange as if opponent had botched his attack (he will be at a CP disadvantage). |

#### Defensive Maneuvers

The foundations of all Defensive Maneuvers, as described in Book Three: Training, are the Block and the Parry. Most all other Defensive Maneuvers are directly related to these two.

Parrying is the chosen defense of duelists and bladeslingers everywhere. It has the practicality of speed, transportability (no shield or armor to carry around), and effectiveness. Most swordsmen have a rudimentary understanding of parrying; some choose to neglect it in favor of greater skill with a shield (such as knights and soldiers, for whom parrying is often less practical on the battlefield). Any quantity of Combat Dice may be spent on parrying when defending. The difficulty to parry with any given weapon is determined by that weapon's Defense Target Number (DTN).

Blocking is the reasonably unscientific action of imposing a foreign object (usually a shield) between an incoming attack and oneself. It lacks the grace of a wellperformed parry, but is very effective. Many shields have the disadvantage of being cumbersome and slow; the advantage being a large area of efficient cover and that it's easy to learn.

As passive instruments, shields act as armor offering almost total protection to the off-hand front side of the body, generally providing between 4 and 10 points of protection to areas III, XI, and IV or more, on the shieldside (depending on the size and make of the shield).

Actively they can be used to deflect blows, similarly to a parry. Any number of dice can be spent on a block. A successful block deflects an incoming blow just as a parry would, lessening the attacker's number of successes. The Defensive Target Number depends on the kind of shield. See Armor, and Table 4.5, below.

#### Armor

Armor, as previously mentioned, reduces the damage done by successful attacks by deflecting blows and/or by absorbing them. Both of these qualities are represented by a piece's Armor Value. As seen above, AV is directly subtracted from the damage dealt by one's opponent. While armor can save one's life, most fighters can't afford it, don't like traveling with it, or simply find it too cumbersome. In addition to Armor Value, armor usually negatively affects the wearer's Combat Pool; thus a heavily armored figure is easier to strike, but harder to injure. Table 4.5 contains several examples of armor with their various modifiers. Various pieces of armor may affect other things as well, such as hearing, vision, or movement.

It is crucial to note that all modifiers from Table 4.5 are for well-fitted pieces of armor; any piece of plate armor not specifically tailored will have more severe modifiers (-1 or worse), as will any other non-plate armor that does not have a good, comfortable fit. By the same token, one cannot wear multiple suits of armor (though mixing-and-matching is acceptable). Shields, Helms and other headgear are the exception to this rule, and may be worn in addition to a full suit (or no suit) of armor. When wearing piecemeal armor, all negative modifiers are based on the highest modifier, +/- 1 for each additional piece. The maximum negative modifier is -4 (as long as the pieces fit well).

Other factors, such as being mounted, effect CP modifiers from armor substantially. Mounted combatants may ignore one negative CP modifier as well as all negative CP modifiers from leg-armor and shields. For fully armored characters simply cut the total CP penalty in half, rounding down.

- **Table 4.5 - Armor and Shields**

|Type/description                  |Armor Value* |CP modifier |Other Mods |
|:---------------------------------|:-----------:|:----------:|:---------:|
|Leather Jack (w/ sleeves)         |2            |            |           |
|Leather Jack (w/o sleeves)        |2            |            |           |
|Chain shirt (light, w/ sleeves)   |4            |            |           |
|Chain shirt (light, w/o sleeves)  |4            |            |           |
|Chain (full suit)                 |4            |-2          |-1 Move    |
|Piecemeal plating (sample pieces) |3-5          |-0 to -2 ea |Various    |
|- Bracers or well-made gauntlets  |3            |            |           |
|- Large shoulder cop              |5            |-1**        |           |
|- Shoulder and whole arm          |4-5          |-1/-2**     |           |
|- Knee cop                        |3-5          |-1          |           |
|Breast Plate (front and back)     |6            |-1          |           |
|Plate (full suit, w/o helm)       |6            |-3          |           |
|Chainmail coif                    |3            |-1 or 0***  |-1 Per     |
|Pot helm                          |5            |-1          |-1 Per     |
|Full helm                         |6            |-2          |-2 Per     |
|Buckler shield (hand) (TN 6)      |4            |            |           |
|Medium round shield (TN 5)        |6            |            |-1 Move    |
|Medium "heater" shield (TN 5)     |7            |-1          |-2 Move    |
|Large "kite" shield (TN 5)        |8            |-3          |-3 Move    |

- (*) Applies only to covered and protected areas.
- (**) Applies when protecting sword or weapon arm.
- (***) When worn under any kind of helmet.

#### Cover

Cover, such as hiding behind trees or ledges, is a simple matter. Those areas protected by cover may not be hit, thus limiting your opponent's choice of targets. This is most effective when used in ranged combat.


### - Movement and Combat

As discussed in Book Five: The Laws of Nature, characters may cover distance equal to their Move score in yards each Round. During a combat Exchange this number is reduced to their Move score in feet. Moving so far constitutes a "charge," which is treated as an Offensive Stance (granting +2 CP for attacks, but inflicting a -2 CP penalty for defense for the whole round). Even if not charging, one must move half as far (1/2 Move) during each Exchange (melee combat only). This movement may be forward, backward, or to the sides. We recommend using 25mm-scale miniatures on a hexgrid. Turning and more complicated movement is covered in Book Five.

In order to fire a bow or use any other missile weapon (except a javelin or similar instrument) one must be standing still.

Movement also impacts Terrain Modifiers, above.


### - Mounted Combat

Fighting from horseback offers many advantages. In addition to increased speed and movement (when mounted use the horse's Move score), mounted fighters receive the following bonuses:

- Height/reach advantage: +2CP
- Mounted Charge: +2 CP for the whole Round (attack or defense)
- Armor: Ignore 1 CP loss due to armor, in addition to any modifiers from armored legs
- Terrain: No need to subtract dice from your CP! Roll a Riding Test instead

There are a few drawbacks as well:

- Evasion: no dodges other than "Duck and Weave" are available to your character (without falling off horse, that is).
- Wounds: any time you or your mount is wounded, roll Riding or fall 6' to the ground.
- Knockdown/Knockout: if either of these things happen to you or your mount, treat it as a 6' fall
- Weapons: only one-handed weapons, spears, and lances may be used from horseback, though off hand shields are fine
- Any complicated movement requires a Test of Riding. Failure means you don't move, fumble -you fall.
- Missile weapons: –1 MP when stationary, -2 when moving at a walk, -5 when galloping.

See Book Five: The Laws of Nature for details on falling.


### - An Example of Combat

Our hero Geralt has been called out on a duel by Felix' brother, Stefan. Geralt has a ST of 5, a TO of 4, a Reflex of 6, and his Combat Pool is 15. He is carrying a longsword and wearing no armor. Stefan has a ST of 5, a TO of 5, a Reflex of 4, and his Combat Pool is 13. He is wearing a full suit of chainmail, a pot-helmet, and carrying a heater shield; this reduces his CP to 9. They are fighting on foot.

- Seneschal (controlling Stefan): Stefan salutes you and inches forward in a neutral stance.

- Geralt: I set up in a defensive stance. This guy looks dangerous-and he's got a shield. I hate shields.

- Seneschal: Declare attack or defense.

Both parties grab a red die and a white die and throw one simultaneously. Both throw white dice.

- Seneschal: The two of you circle for a moment, sizing up your respective opponents. Throw again.

Both parties again grab a red die and a white die and throw one simultaneously. Both again throw white dice. After circling for a moment they both throw again and again.

- Seneschal: Stefan, tired of circling, begins to taunt you, insulting your family and your skill.

- Geralt: I'll return the favor. I say, "It's your brother whose head this inbred, unskilled blade-slinger tore from its shoulders. Perhaps you would like to join him in hell!" Seneschal: Your insult seems to be really working him up. He changes to an aggressive stance and increases the ferocity of his insults. Throw initiative.

Again, both throw white dice.

- Geralt: He wants me to attack first, but this just might work. I say, "Your brother never even put up a fight. I felt bad after I killed him -it was like slaying a handmaiden!"

- Seneschal: It looks like that did it. Throw initiative.

This time Stefan throws a red die-he was taunted into it-while Geralt stays white (so as to benefit the most from his stance).

- Seneschal: He comes in quickly, cutting sideways at your head, zone IV, from his right. He's spending 5 dice on that attack. Geralt: It's about time! I'm going to duck and weave so that I can strike him from the side, and get past that darn shield! I'm spending 9 dice.

They both roll. Stefan rolls 1, 2, 2, 4, 5, 8, 9... even with the aggressive stance bonus of +2 dice that's only two successes against his weapon's ATN of 6 Geralt rolls 2, 2, 3, 5, 6, 7, 7, 7, 9, 9, 0... three successes with the defensive stance bonus (+2 dice), beating Stefan by 1! 1t's plenty, and Stefan's sword goes whizzing harmlessly by -opening a big hole for Geralt to strike through.

- Seneschal: You've evaded his blow and may attack. He loses 3 CP because of the Duck and Weave.

- Geralt: Excellent! It was close, but it worked. I'm spending my remaining six dice trying to hit his left side; that's zone III.

- Seneschal: He's going to try and evade (partially), spending his last 4 dice.

Again, they roll. Geralt, the attacker, rolls 4, 5, 5, 7, 8, 0... five successes at his weapon's ATN of 5 (it's a very nice sword). Stefan rolls 1, 7, 8, 0... three success at his DTN of 7 (due to partial evasion). That gives Geralt a margin of 2, plus his sword's damage rating of 8 (ST +3, a greatsword), for a subtotal of 10. He then rolls 1d6, getting a 3; according to the cutting damage tables (see Appendix) that's a blow to Stefan's upper abdomen, just below the ribs. Stefan subtracts his own toughness and armor rating (his chainmail covers that area), total 8, to finally receive a level two wound (10 - 8 = 2). That wound (again, see Appendix) reads: "Deep laceration, bleeding, and some torn muscle. BL 5, Shock 3, Pain 6 -WP" Next exchange Stefan will have only 6 dice to work with, and only 7 dice every exchange thereafter.

- Seneschal: You duck under his sword and land a solid blow to his side-were it not for his armor you would have killed him. Nonetheless your blow leaves him reeling somewhat, and you may attack again, though he is able to spin himself around for defense somewhat. We're beginning round two; pools refresh.

- Geralt: Let's do that. I'll swing up from below at VI for 10 dice. I'm gonna gut this puppy. Seneschal: He's going to attempt to block, using his shield, with 4 dice.

Geralt rolls 1, 1, 1, 2, 3, 4, 4, 7, 7, and 7... (bad luck!) only three successes against an ATN of 5. Stefan rolls 2, 3, 7, 9... two successes against a difficulty of5 (shield's DTN). Geralt hits with a margin of one, plus weapon damage gives a subtotal of 9. Rolling location (see Appendix), Geralt's blow lands on Stefan's inner thigh. Stefan's armor and TO bring that down to zero (0), giving Stefan a level 0 wound to deal with - just a scratch.

- Seneschal: You land a hit on the inside of his leg, but it fails to break Stefan's chainmail armor. You still have initiative, and may attack.

- Geralt: You know it! I'm spending those last five dice to hit him in that same side again (zone III).

- Seneschal: He's blocking with two (that's all he's got).

Geralt rolls 1, 2, 2, 4, 8... one success (ATN 5). Stefan rolls land 9... two successes! He manages to block Geralt's attack (TN 7). Thus ends round two. As round three begins, the Seneschal rolls blood loss (TN 2) for Stefan. He rolls 2 successes, and Stefan is fine for now. The Seneschal also applies pain modifiers to Stefan's CP leaving him with 7 dice.

- Seneschal: This is round three; pools refresh. Stefan now has initiative and attacks your side, zone III, from his own right. He's spending five dice.

- Geralt: No problem. I'll parry sideways with 8 dice.

Stefan rolls 4, 5, 7, 7, 9... three successes (weapon ATN 6). Geralt parries, rolling 1, 2, 4, 5, 9, 9, 0, 0... four successes (weapon DTN 6).

- Seneschal: You just barely manage to knock his sword to the side, and may now attack.

- Geralt: Time to finish this guy. Seven dice at IV from my left.

- Seneschal: He's going to try a block again, for two dice.

Geralt rolls 2, 2, 3, 7, 7, 7, 8, 8... five successes. Stefan rolls 1 and 9... only one success. Geralt margin is 4, and rolling a d6 Geralt gets a 5-the blow lands on 5 -the face! Geralt's blow does 12, minus Stefan's TO (5), but no armor. That's a level five wound! The damage table reads: "Death. Destruction of cerebellum. Really messy."And that ends the fight.

- Seneschal: Blood spatters all over you as your opponent drops like a bag of sand. Now his retainers start advancing on you...

- Geralt: Okay, time to get out of here...

And that's how combat works. Lots of freedom, but it was really that first hit that won the fight.

***








# Vampire the Masquerade 5th Edition - Three Turns and Out

***




We strongly recommend ending conflicts after roughly three turns, unless everyone is still having fun. Too much dice rolling slows down the drama and becomes harder and harder to describe creatively. If the Storyteller and players want the old-school feeling of fighting down to the last Health box, they’re welcome to do so, of course. But for the rest of us, here’s a few ways to decide who won if you’ve gone three rounds and both sides are still standing.

- Allow the players to break the conflict off if they want. The Storyteller may require a basic contest to do so (for example, Strength or Dexterity + Athletics to flee or Composure + Etiquette to divert the discussion), or their foes may simply let them leave.

- If the players’ foes have taken more losses – or even an unexpected amount of damage -the Storyteller can simply decide that they break off the conflict, as above.

- Simply award victory to the side that won the most contests or to the side with the fewest points of Aggravated damage. The Storyteller narrates the end of the conflict based on the results of the previous contests. Ideally, if the player characters lost, they have a chance to flee or at least to surrender with some dignity.

- Change the situation. Perhaps some new, third force enters the scene, such as the police, or the Prince. Perhaps the conflict simply changes venue from the alley to a nearby warehouse or from the court to a concert. The change should present new options to both sides.

#### One Roll

If none of the above scenarios apply, consider raising the stakes and resolving the conflict with a one-roll conflict (see p. 298), where all consequences are amplified. You can set a Difficulty based on how the previous three rounds have gone. One-on-one conflicts have clear winners each round; sometimes larger combats do also.

- If most of the combat has gone the player characters’ way or if they’ve won all three previous rounds, set the Difficulty at 3.

- If both sides have suffered fairly evenly or if the player characters have won two out of three rounds, set the Difficulty at 4.

- If the player characters have gotten the worst of it or won only one exchange out of three, set the Difficulty at 5.

- If the player characters have been lucky to survive this long or have lost all three previous rounds, set the Difficulty at 6.

>*Note that conclusion of a conflict need not resolve it completely. It could also just move it into a new scene.*

***
