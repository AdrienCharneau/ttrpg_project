# OTHER/REMEMBER




>https://en.wikipedia.org/wiki/List_of_revolutions_and_rebellions

>https://en.wikipedia.org/wiki/Malayan_Emergency

>https://en.wikipedia.org/wiki/Zanj_Rebellion

>https://en.wikipedia.org/wiki/Mau_Mau_rebellion

>https://en.wikipedia.org/wiki/Red_Turban_Rebellions

>https://en.wikipedia.org/wiki/Li_Zicheng

>https://en.wikipedia.org/wiki/Central_Asian_revolt_of_1916

>https://en.wikipedia.org/wiki/Siamese_revolution_of_1688

>https://en.wikipedia.org/wiki/Jacquerie



## - Abbasid Revolution

>https://en.wikipedia.org/wiki/Abbasid_Revolution

>https://en.wikipedia.org/wiki/Abu_Muslim


## - Burkinabé revolution

>https://en.wikipedia.org/wiki/Burkina_Faso

>https://en.wikipedia.org/wiki/French_Upper_Volta

>https://en.wikipedia.org/wiki/Republic_of_Upper_Volta

>https://en.wikipedia.org/wiki/1966_Upper_Voltan_coup_d%27%C3%A9tat

>https://en.wikipedia.org/wiki/1983_Upper_Voltan_coup_d%27%C3%A9tat

>https://en.wikipedia.org/wiki/Maurice_Yam%C3%A9ogo

>https://en.wikipedia.org/wiki/Sangoul%C3%A9_Lamizana

>https://en.wikipedia.org/wiki/Thomas_Sankara


## - Turkish Oppression

>https://en.wikipedia.org/wiki/Armenian_genocide

>https://en.wikipedia.org/wiki/Kurdish_nationalism


## - European Protestant Wars

>https://en.wikipedia.org/wiki/European_wars_of_religion

>https://en.wikipedia.org/wiki/Thirty_Years%27_War

>https://en.wikipedia.org/wiki/Bohemian_Revolt


## - Chinese Cultural Revolution

>https://en.wikipedia.org/wiki/Cultural_Revolution

>https://en.wikipedia.org/wiki/1959_Tibetan_uprising

***








# ALSACE-LORRAINE DISPUTE




## Links


>https://en.wikipedia.org/wiki/Alsace%E2%80%93Lorraine




## Unfolding


The modern history of Alsace-Lorraine was largely influenced by the rivalry between French and German nationalism. France long sought to attain and then preserve what it considered to be its "natural boundaries", which it considered the Pyrenees to the southwest, the Alps to the southeast, and the Rhine River to the northeast. These strategic claims led to the annexation of territories located west of the Rhine river in the Holy Roman Empire.

What is now known as Alsace was progressively conquered by France under Louis XIII and Louis XIV in the 17th century, while Lorraine was incorporated from the 16th century under Henry II to the 18th century under Louis XV (in the case of the Three Bishoprics, as early as 1552). These border changes at the time meant more or less that one ruler (the local princes and city governments, with some remaining power of the Holy Roman Emperor) was exchanged for another (the King of France).

German nationalism on the other hand, which in its 19th century form originated as a reaction against the French occupation of large areas of Germany under Napoleon, sought to unify all the German-speaking populations of the former Holy Roman Empire into a single nation-state. As various German dialects were spoken by most of the population of Alsace and Moselle (northern Lorraine), these regions were viewed by German nationalists to be rightfully part of hoped-for united Germany in the future, despite what the French parts of their population wanted.

***








# FIRST JEWISH-ROMAN WAR




## Links


>https://en.wikipedia.org/wiki/First_Jewish%E2%80%93Roman_War




## Context


Text




## Unfolding


The revolt began in 66 CE, during the twelfth year of the reign of Nero, originating in the oppressive rule of Roman governors, the widening gaps between the wealthy aristocracy and the downtrodden masses, and Roman and Jewish religious tensions. The crisis escalated due to anti-taxation protests and clashes between Jews and pagans in mixed cities.

The Roman governor, Gessius Florus, seized money from the Second Temple's treasury and arrested numerous senior Jewish figures. This prompted widespread rebellion in Jerusalem that culminated in the capture of the Roman garrison by rebel forces as the pro-Roman king Herod Agrippa II and Roman officials fled. To quell the unrest, Cestius Gallus, the legate of Syria, brought in the Syrian army, consisting of the Legion XII Fulminata and auxiliary troops.

Despite initial advances and the conquest of Jaffa, the Syrian Legion was ambushed and defeated by Jewish rebels at the Battle of Beth Horon with 6,000 Romans massacred and the Legion's aquila lost. In 66, a Judean provisional government was formed in Jerusalem led by former High Priest Ananus ben Ananus, Joseph ben Gurion and Joshua ben Gamla. Yosef ben Matityahu (Josephus) was appointed as the rebel commander in Galilee and Eleazar ben Hanania as the commander in Edom. Later, in Jerusalem, an attempt by Menahem ben Yehuda, leader of the Sicarii, to take control of the city failed. He was executed and the remaining Sicarii were ejected from the city. Simon bar Giora, a peasant leader, was also expelled by the new government.

The Roman general Vespasian was given four legions and tasked by Nero with crushing the rebellion. Assisted by forces of King Agrippa II, Vespasian invaded Galilee in 67, and within several months had claimed the major Jewish strongholds of Galilee, Jodapatha and Tarichaea.[7] Driven from Galilee, Zealot rebels and thousands of refugees arrived in Jerusalem, creating tensions between the mainly Sadducee Jerusalemites and the Zealot rebel factions that soon erupted into bitter infighting.

In 69, Vespasian was called to Rome and appointed emperor, leaving Titus to besiege Jerusalem in 70 CE. Following a brutal seven-month siege, during which Zealot infighting resulted in the burning of the entire food supplies of the city, the Romans finally succeeded in breaching the defenses in the summer of 70. Following the fall of Jerusalem, Titus departed for Rome, leaving the Legion X Fretensis to defeat the remaining Jewish strongholds, including Herodium and Machaerus. The Roman campaign ended with their success at the siege of Masada in 72–74.

The Roman suppression of the revolt had a significant impact on the local population, with many rebels perishing in battle, displaced, or being sold into slavery. The temple of Jerusalem and much of the city was destroyed by fire and the Jewish community was thrown into turmoil by the devastation of its political and religious leadership.




## Aftermath


Text

***