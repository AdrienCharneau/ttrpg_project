# NOTES RAPH




## Histoire contemporaine - Royaumes & peuples actuels


## - Histoire politique

Un jour, les dieux quittèrent la terre, et l'ère des rois commença. Les plus grands des hommes de chaque région du monde se rencontrèrent pour maintenir la paix de Drekun. Ouzhman, le plus sage des patriciens de Tihsen, à qui Drekun lui-même avait confié la tutelle du royaume, permis de maintenir cette paix, car il laissa le choix à chacun des chefs du continent de prendre son indépendance vis à vis du royaume s'il le désirait.  Ouzhman était admiré de tous, et aucun chef ne refusa jamais de reconnaitre sa grandeur, et tous s'accordèrent pour suivre son patronage avisé. Il fut couronné, Ouzhman Drekun-Mu, premier roi de Yolzhin.

Son règne fut prospère et incontesté. A sa mort, les Grands se réunirent encore pour choisir un successeur. On choisît alors son plus fidèle disciple Rezhdin, qui à son tour laissa chacun des chefs libre de quitter le royaume. Cette fois-ci cependant, les sages de Siejden prirent leur indépendance, arguant que le pouvoir se transmettait strictement entre Tihseniens. Siejden fut ainsi la première ville à quitter le royaume de Yolgen. Les chefs des régions autour de la capitale des sages, plus habitués à traiter Siejden qu'avec Tihsen, quittèrent alors eux aussi le royaume pour rejoindre le Royaume du Sud, Niimzhin, désormais dirigé par le conclave des sages de Siejden.

Pendant les génération qui suivirent, chaque successeur du roi de Yolzhin vit plus de contrée quitter le royaume, jusqu'à ce qu'il n'en reste que la région attenante à sa capitale, Tihsen. Le monde comptait alors plus d'une centaine de royaumes et principautés, et les guerres commencèrent à éclater.

Un des rois de Yolzhin, connu pour évoquer avec mélancolie le temps où la terre était dirigée par une seule bannière maintenant la paix, décida de prendre comme nom Ouzhman le jeune et d'entreprendre l'unification du continent. Il commença par conquérir les petits royaumes qui bordaient Tihsen, ce qui fut aisé car la capitale restait la plus grande ville du monde, et sa puissance, bien qu'amoindrie, surpassait largement celle des petits royaumes, même unis dans l'adversité. Petit à petit, le roi prit possession de tous les royaumes qui formaient la centre du continent. 

Ensuite il se confronte à Siejden, et la guerre fait rage mais Tihsen finit par gagner, toutefois il reconnait l'indépendance de Siejden, ses propres impôts, et la liberté de l'université.

Ensuite il essaye d'envahir Skensen mais là c'est la grosse merde, il fait 100 sièges qui échouent tous parce que Skensen c'est la ville la plus imprenable du monde tralala. Du coup Yohlzhin a repris de la gueule mais il y a un royaume indépendant au nord, celui de Skensen, une enclâve libre (la cité de Siejden). Plus quelques régions qu'il a pas conquises après la déconfiture contre Skensen : la montagne, l'île des Ima-Mu. Même si la plus grande partie du monde appartient à Yolzhin, ça reste un gouvernement assez lointain, il y a juste des  impôts royaux et c'est tout quoi.


### - Les Peuples Actuels


Les peuples actuel, les royaumes, les petits peuples qui son nouveaux.
Les peuples actuels correspondent pour la majeure partie aux Kusa-Mu qui ont peuplé la majeure partie du monde. Les seuls peuples resté plus ou moins d'origine sont les Ima-Mu, très peu nombreux et vivant presque sans contact avec le reste du monde. Le Hikki-Mu, habitants du nord, se sont beaucoup mélangés aux Kusa-Mu, et seules quelques familles ont encore du sang uniquement Kusa-Mu. Au sud, on trouve quelques héritiers des Zuu-Mu, mais en petit nombre.
Les Yaa-Mu vivent très isolés dans la montagne, on peut les trouver un peu partout car ce sont souvent des capitaines ou ingénieurs mais ils restent très rares, d'autant que leur population est relativement restreinte et les étrangers malvenus dans leur montagne.
Une nouvelle race que les peuples anciens est apparue, celle de la tribu du dieu vagabond (le 8e) qui ont la marque du dieu, mais je sais pas encore quoi. Mais en gros c'est toutes les tribus mélangés mais un truc en plus, à voir.




## Siejden, TIhsen & Skensen


Tihsen est la plus grande ville de [Nom du monde], elle est bâtie à dos de rivière

Siejden abrite l'université, le plus grand pôle scientifique du monde, qui étudie le Souffle et ses Vents.

Skensen est construite toute en hauteur parce que la dalle originelle est posée sur d'immenses pilliers construits par Sken et que personne a jamais réussi à en poser une autre, du coup la ville a plusieurs niveaux de verticalité avec plein de plateformes qui débordent.




## Le Souffle (Magie)


La magie dans le jeu provient du souffle d'Ima : elle provient de ses différents corps et de son souffle.

>Le Souffle est présent en chaque être car tous sont nés de la voix d'Ima, mais la manifestation la plus puissante du souffle, suffisamment pour qu'elle puisse agir sur d'autres éléments animés par le souffle, réside dans le ventre des vents. Quand Ima donna ce qui restait de sa vie pour animer le dernier corps de son père, il donna naissances aux cinq vents, qui portaient en eux le Souffle tel qu'on le connait aujourd'hui.
Le Souffle se scinda en cinq vents qui se répandirent sur la terre.

>-Bidule Bidule, prof de magie lol, La magie pour les nuls, vol. 4.

Le vent d'une manière générale porte toujours en lui une partie des cinq Vents, mais dans des proportions trop modestes pour pouvoir en tirer autre chose que de la petite magie quotidienne (allumer un feu par mauvais temps, guérir de petites plaie ou distiller une liqueur. Toutefois, chaque vent suit des courants qu'il est possible de canaliser avec les outils adéquats, permettant alors d'en obtenir une forme plus ou moins pure suivant les techniques et outils utilisés. Si les quatre premiers vents peuvent se canaliser avec plus ou moins de maîtrise, le dernier d'entre est presque impossible à canaliser sous une forme suffisamment pure pour en tirer de véritable résultats. (par contre il y a des légendes de 2-3 mecs qui auraient réussi)

comme ça :

- Vent d'agrégation : (magie élémentaire de base) permet d'agréger les élément d'un même type ensemble, feu, eau etc.

- Vent de combinaison : (plus ou moins l'alchimie), permet des transmutations d'élément ensemble.

- Vent d'imprégnation : la magie médicale, permet d'imprégner un corps de la volonté du mage pour le guérir (il y a des variantes mais officiellement c'est ce qu'on peut faire avec).

- Vent de déchainement : libère la puissance des choses (genre fait exploser des tempêtes, pousser des fleurs, etc.)

- Vent d'insufflation : vent le plus puissant, permet de créer la vie, mais pour ça il faut trouver le cinquième vent qui est vraiment chaud patate tout ça.

http://fr.wikipedia.org/wiki/Magie_%28surnaturel%29#Fonctionnement

Avant de continuer il va falloir chercher quel type de magie on fait en fait, comment elle marche tout ça.




## Les Vrais Dieux


* Sont appelés comme ça parce que c'est ceux qui ont été créés par Ima. En opposition avec les engeances de Mo-Nai ou les créatures dérivées.

Ils sont neufs :

1. Tih (Dieu de l'été - saint-patron des populations tempérés, fondateur de la ville centrale - la plus grande capitale),
2 .Sken (Dieu du froid - saint patrons des populations du Nord, fondateur de la capitale du nord),
3. Ekh (dieu de la guerre),
4. Yoh (dieu de la nature),
5. Zehi (dieu du vent),
6. Siej (Dieu des mages & de la sagesse - saint-patron des populations de la région du sud, fondateur de la capitale du Sud),
7. dieu qui décide de voyager pour l'éternité genre, les gens qui le suivent finissent par devenir le peuple nomade
8. The Fisherman (il faut lui trouver un nom) qui est le dieu de rien du tout parce qu'il a disparu mais super mystérieux ouhlala, mais en vrai c'est le dieu métaphysique par excellence) le vagabond
9. Drekun (le trois-fois Roi).

Quand les dieux, éveillés par Ima, se levèrent, ils étaient neuf, et décidèrent de partir ensemble à travers le monde.

Ils rencontrèrent d'abord les Kusa-Mu, qui s'étaient installés partout sur la terre où s'étendaient des prairies et qui les accueillirent. Ils leur offrirent des bêtes pour voyager, du blé et du vin. Les dieux reçurent leur présent avec des sourires et leur firent cadeau de la joie en retour. Ils restèrent longtemps parmis les Kusa-Mu et ils les aimaient tant que quand vint l'heure du départ, l'un des dieux resta parmi eux : un grand dieu qu'on nommait Tih (le bienheureux) car il souriait sans cesse. Tih rassembla alors les Kusa-Mu qui résidaient dans la vallée la plus douce sur la terre et les aida à construire leur capitale : Tihsen.

Les dieux repartirent alors, vers le nord où ils rencontrèrent les Hikki-Mu qui menaient leur caravanes à travers les langues glacées. Les dieux furent émus devant le courage de ces hommes qui avaient refusé une vie douce par amour de leur terre.  Les Hikki-Mu leur sculptèrent de grandes cornes que seuls les dieux pouvaient sonner et en échange ces derniers leur offrirent la force. L'un des dieux, Sken, resta parmis eux, et lui aussi les aida à dresser une grande cité sur les landes gelées dont les fondements étaient creusés sous la glace même : Skensen.

Les sept dieux restant reprirent la route une fois de plus. Ils rencontrent les Ima-Mu qui leur parlèrent longtemps d'Ima. Les dieux étaient fascinés par la sagesse du peuple et les grandes machines qu'ils avaient dressées en domptant la force du vent. Les Ima-Mu leur parlèrent de l'Orme, de la Montagne et des Vents qui étaient les corps d'Ima. Les dieux leur offrirent la longévité pour que toujours ils soient la mémoire du monde.

Après les récits des Ima-Mu, les dieux se séparèrent, désireux de découvrir les merveilles dont les hommes leur avaient parlés. Ekh se dirigea vers la montagne, Yoh le silencieux parti voir l'Orme et Zehi parti à la poursuite des vents.
Yoh arriva à l'Orme et l'un d'entre eux reste pour s'occuper des enfants de celui-ci : grande forêt avec pleins de créatures magiques genre fallens (l'Orme c'est la source de toutes les créatures). 
Quand y en 4 ils découvrent les ombres des Zuu-Mu restés sur la terre et les aide à reconstruire leur cité qui reste en ruine, les dieux leur donnent la sagesse et l'intelligence pour redresser leur ancienne civilisation. Siej reste avec eux et reconstruit la ville qui s'appelle alors Siejden.
Des trois restant, y en a deux qui font des trucs : 

l'un s'installe dans le pire des petits villages en cachette The Fisherman (l'histoire le dit pas, ça dit juste qu'il a disparu et quelques indices qui permettent au joueur de la reconnaitre)
L'avant dernier c'est le vagabond qui va fonder le peuple des nomades
Le dernier c'est Drekun, qui alla au bord du monde pour contempler le chaos avant de revenir pour être le roi du monde (mais en pas dictateur haha).

A ajouter : Histoire des trois trials de Drekun ;; épisode de Ekh avec les Yaa-Mu et comment il devient dieu de la guerre, trouver le 8e dieu plus précisément, raconter plus en détail l'histoire de Fisherman, lui trouver un nom, développer sur Yoh. Raconter comment un disciple de Siej fonde la pratique de la magie à Siejden, raconter des trucs sur la quête de Zehi.




## Mythologie


### - Ima (Les Titans)

Le monde dormait, alors Ii-Nai tira de son sourire Ima, et l'envoya sur le monde pour l'éveiller doucement. Elle lui avait donné une voix, pour parler avec douceur aux choses et leur dire ce qu'elles étaient.

Ima parla aux fleuves, à l'herbe, au soleil et au ciel. Il parla à la mer et à la terre, au feu et à la lune, aux pierres et aux lucioles et le souffle d'Ima s'épuisa. Alors il inspira son propre corps pour donner naissance à son fils, qui était lui-même, Ima encore et pourtant lui n'était plus.

Ima parla aux oiseaux, aux fleurs, aux nuages et aux hommes. Il parla au corps de son père, qui sans être était devenu un grand cristal. Il parla au corps de son père qui avait été lui-même et l'appela montagne pour qu'il puisse veiller encore sur ceux qu'il avait éveillé. Il fut fatigué à son tour et s’endormit à son tour, non sans avoir à son tour donné son souffle à son propre fils. Ima encore, et trois fois né, deux fois mort, parla encore aux choses.

Ima parla aux dieux, lentement et avec application, et cette tâche à elle seule l'épuisa. Avant de s'endormir toutefois, il parla au corps de son père et l'appela Orme, il donna enfin son souffle à son fils et dormi à son tour.

Cette fois Ima était petit et marchait lentement et son souffle n'était plus que murmure. Il murmura à son père et l'appela Vent, mais déjà il sentait ses forces s'amoindrir. Alors Ima donna tout son souffle au corps de son père qui se scinda en cinq vents qui partirent dans toutes les directions. Enfin Ima s'effondra dans la mer, sans souffle, et s’endormit.


### - Les anciens peuples

Quand Ima éveilla les hommes, ces derniers partirent dans cinq directions, et devinrent les cinq peuples anciens qui sont les pères des peuples actuels.

Les premiers partirent vers le sud jusqu'au bord de la terre et décidèrent de s'installer au bord de la mer à laquelle ils devinrent semblables. L'océan leur donna son vrai nom dont ils se proclamèrent les fils, les Zuu-Mu ils furent désormais. Leur peau se hâla d'un teinte bleue car ils portaient un peu de la mer en eux et longues chevelures devinrent vert sombre à l'instar des algues. Les hommes-récifs construisirent alors des bateaux pour ne jamais quitter la mer qu'ils aimaient tant, et finirent par construire des villages flottant qu'il lancèrent sur le rivage, abandonnant la terre et laissant seulement derrière eux quelque uns d'entre eux pour garder leur mémoire.

Les seconds partirent vers l'est pour trouver les montagnes qu'il gravirent jusqu'au ciel qu'ils aimaient comme les Zuu-Mu aimaient la mer. Ils bâtirent de grandes cités en porte-à-faux, adossées à la montagne dont ils se proclamèrent les enfants, et les Yaa-Mu furent-ils. Leurs yeux devinrent clair et immense comme les cieux qu'ils contemplaient tout le jour et leur peau grise comme la roche qui les portait. Enfin, ils construisirent de grandes barges de bois pour sillonner le ciel d'une ville à l'autre.

Ensuite ce fut au tour des Kusa-Mu, de quitter leur berceau pour les prairies où ils s'éparpillèrent aux quatre coins de la terre. Leurs villages recouvrirent le mondes et ils sont les ancêtres de la plupart des peuples actuels. D'un tempérament calme, ils préféraient la compagnie des bêtes à celle des tempêtes, alors leurs cheveux devinrent bruns comme la terre ou blonds comme le blé, leur peau tendre et hâlée comme la vie qu'ils menaient.

Les quatrièmes partirent au nord où la mer et la terre se rencontrent dans les langues glacée. Ils se proclamèrent Hikki-Mu, enfants du froid, et apprirent la pêche et la chasse. Ce furent de grands hommes à la chevelure blanche et à la peau noire comme l'obsidienne, et menaient de grandes caravanes à travers tundras et taïgas.

Enfin, le dernier des anciens peuples s'installa sur le dernier corps d'Ima car ils l'aimaient plus que toute autre chose au monde. Comme son corps était à la source des vents et que ces derniers y revenaient sans cesse, ils construisirent de grands moulins et de grands voiles pour les dompter comme d'autres les chevaux. Ima-Mu s'intitulèrent-ils, et leurs visages furent semblables aux falaises découpées dans la terre, leurs cheveux frisés comme la course du vent.


### - Mo-Nai & Ii-Nai

Mo-Nai [Le Chaos : indication pour toi mais jamais il sera fait mention de "Chaos"]  

>Au commencement, il n'y avait que Mo-Nai : sans temps et sans matière. Mo-Nai est, tout en n'étant pas, Mo-Nai dort éveillé, regarde les yeux fermés, Mo-Nai attend ce qui déjà s'est produit, puisque pour Mo-Nai ce qui se produit une fois pour les êtres de vie s'étire à l'infini, tout en n'ayant jamais lieu.  Car Mo-Nai est non-vie et non chose et pourtant chaque être porte en lui une partie-tout de Mo-Nai.
Le sommeil est l'enfant de Mo-Nai en ce que le dormeur vit et ne vit pas tout à la fois.
Le ciel est semblable à Mo-Nai puisqu'il est plein de vide.
Et le monde est Mo-Nai, et avant le temps et avant les choses et avant la vie il n'était pas, en attendant d'être, mais déjà prairies et montagne, bergers et machines, soleil ; déjà mais pas encore, car tel est Mo-Nai.

>-Uzhman Drekun-Mu, Lettres (extrait)

Ii-Nai [La vie/Gaia ; pareil, indication pour toi mais jamais indication dans le jeu]

>Un jour, Ii-Nai est apparue. Ii-Nai n'est pas née, simplement elle avait toujours été en Mo-Nai, n'étant pas. Un jour elle fut et elle avait deux visages : Vel & Lin, qui étaient comme ses enfants mais aussi son corps. Ii-Nai s'éveilla et sourit à Mo-Nai, même si son sourire n'était qu'une plume du grand corps de Mo-Nai. Et Ii-Nai eut des mains pour donner naissance aux êtres.
Vel souffla : le temps fut.
Lin ouvrit les yeux : le monde était.

>-Uzhman Drekun-Mu, Lettres (extrait)

***








# WEIRD PREMISE




In a lunar asteroid called Secunda, populated by forests of giant mushrooms, gray sand deserts, and ancient volcanic craters, lives a humanoid people called the Cherok. They dwell underground due to their fear of sunlight and feed on fireflies that swarm in the tunnels. The Cherok have no history, or at least they do not know why they are there. Their intelligence is quite limited, but they are aware that their presence is not accidental. Once a month, they appoint a Cherok as a scout to send them to the surface, which is highly risky as they rarely return. The only time a scout came back, they reported seeing numerous ruins and marshy mushroom forests inhabited by tribal Cherok with dark green skin.

Dipok is a rather ordinary Cherok, known among his people for being naive and not very perceptive. However, he is very agile and swift, which sets him apart from others. For this reason, the other Cherok have chosen him as the scout to uncover their origins. Dipok is an average-sized Cherok with grayish-blue skin covered in several scars, and he possesses glowing red eyes. He wields a fragment of a meteor, entrusted to him when he was designated as a scout.

If Secunda resembles a Swiss cheese and is an asteroid dotted with tunnels, it is because several thousand years ago, beetles fed on its lunar rock and formed these catacomb-like tunnels. These beetles live on another planet called Namir, populated by giant and repulsive insects, all under the rule of a queen who gives them life. They are known as the Namirians and were, long ago, the Cherok's worst enemies when the Cherok still lived on the surface of Secunda. Occasionally, the Cherok experience horrific nightmares set in the insectoid planet of Namir, and only the tribe's shaman knows that these nightmares take place on a real planet. Today, the Namirians return from space, flying with their beetle wings, either to repopulate the underground or because they are searching for something.

How does Dipok annihilate the Namirians? The shaman has bestowed upon him certain insect-shaped dolls that enable him to torture the Namirians when he has enough mental energy to do so.

After numerous explorations and battles against the Namirians on the surface of his planet, Dipok discovers something incredible that was never imagined before—an tree surrounded by red flowers (poppies). He and his tribe are truly impressed by this finding as, amidst the mushrooms and lunar rock, they have never seen such vibrant colors. But the most shocking revelation to Dipok is that all the previous Cherok scouts never returned not because of sunlight but because they chose to stay by this tree until the end; they are actually still alive, and none of the Cherok ever feared the sun. It was an invention by the shaman to keep them underground, safe. Finally, the entire Cherok people settle near the tree and live happily for a very long time.

***