```
   __|   _ \  _ \  __|     \  |  __|   __|  |  |    \     \ | _ _|   __|   __|
  (     (   |   /  _|     |\/ |  _|   (     __ |   _ \   .  |   |   (    \__ \
 \___| \___/ _|_\ ___|   _|  _| ___| \___| _| _| _/  _\ _|\_| ___| \___| ____/
```

***








# **Ability Rolls**

***




**Ability Rolls** are a simple way to test a character's **Abilities**. This is when the GM needs to resolve an outcome where a character assumes a passive position, where they aren't actively resisting something, or aren't carrying out a deliberate action. For example, an **Ability Roll** might be used to see if a character notices something unusual in their surroundings, or manages to recall a specific piece of information. But not if they attempt to climb a wall or dodge an attack.

>*Ability Rolls are typically made by players at the GM's request. However, the GM may sometimes roll in secret to keep players unaware.*




## Steps


### - Choosing an Ability

When an **Ability Roll** is called, the GM must first pick an **Ability** for it. Here are some examples:

- **Dexterity** : for determining if a character hears something unusual or spots a hidden object.

<!-- Charm : for determining an NPC's first impression of the character. -->

- **Knowledge** : for determining if a character recognizes an item, symbol, spell or anything that requires education or expertise.

<!-- Reasoning : for orientating oneself or finding direction in unknown territory. -->

- **Insight** : for figuring out a person and/or their ulterior motives in a conversation.

Depending on its score, this **Ability** determines which class of die is rolled.


### - Resolution

The player (or GM) rolls their die:

- A result of 4 or more is considered a *success*.

- A result of 3 means they roll again.

- A result of 1 or 2 is considered a *failure*.




## Example


Finn is observing a fortress from a safe distance, planning their next move. They notice movement on the walls. The player controlling the character asks if they can see anything:

- The GM tells the player to roll a *d6* (based on Finn's *dexterity* of 2), as they need to test Finn's eyesight.

- They get a result of 5, which counts as a *success*. The GM says that Finn notices five guards: three patrolling the walls and two standing on a tower.

***








# **Action Rolls (Part 1)**

***




An **Action Roll** is a roll where a character takes a deliberate action, initiating a specific course of events or using their **Abilities** to achieve something. The GM should only require this type of roll if something is at stake, or there's a possibility for a bad outcome<!-- (no matter how small it is)-->. It shouldn't be required for routine or trivial tasks.

<!-- Many roleplay games have the concept of "failing forward". This means that every roll has some consequence which is usually narrated by the GM and usually bad for the character or the party as a whole. A roll without possible negative consequences should not be made.

Every moment of play, roll dice or say “yes.”

If nothing is at stake, say “yes” [to the player’s request], whatever they’re doing. Just go along with them. If they ask for information, give it to them. If they have their characters go somewhere, they’re there. If they want it, it’s theirs.

Sooner or later—sooner, because [your game’s] pregnant with crisis— they’ ll have their characters do something that someone else won’ t like. Bang! Something’s at stake. Start the confl ict and roll the dice.

Roll dice, or say “yes.” -->




## Steps


### - Declaring the Action

To perform an **Action Roll**, the player describes their objective and states the means by which they plan to achieve it. When necessary, it's the GM's job to ask and clarify this step, to ensure a clear understanding of the character's intentions and methods.

>*For example, while both "successfully casting a spell" and "successfully casting a spell to impress a powerful sorcerer" may seem like identical tasks, their underlying intentions and expected outcomes differ significantly.*


### - Assessing Challenge

Set by the GM, **Challenge** gauges the amount of effort or skill required to succeed the roll:

|Challenge    |Rating |Description |
|:-----------:|:-----:|------------|
|Average      |5      |The action requires minimal effort or skill. <!--Examples include untying a knot, -->|
|Challenging  |10     |The action is straightforward but requires some level of effort or skill. <!--Examples include sneaking past a distracted guard, performing a first aid procedure or running across a sturdy plank of wood. -->|
|Difficult    |15     |The action is tough and requires considerable levels of effort or skill. <!--Examples include deciphering a complex code, climbing a steep wall or playing a convoluted musical piece on the piano. -->|
|Daunting     |20     |The action is excessively challenging and requires exceptional levels of effort or skill. <!--Examples include sneaking undetected past a heavily guarded perimeter, performing an advanced surgical procedure or walking on a tightrope. -->|
|Overwhelming |25     |The action seems impossible and requires extraordinary levels of effort or skill. <!--Examples include performing a groundbreaking experimental medical procedure, -->|

<!-- Easy, Average, Difficult, Daunting, Overwhelming -->

<!--|Trivial      |Resolution not required |-->
<!--|Impossible   |Cannot be resolved      |-->

<!-- **Challenge** should indicate the difficulty of an action in its "purest" sense, without considering any circumstances or external factors (things like environmental conditions, people, special states or equipment -this what **Context Modifiers** are for, see next chapter).

>*For example, shooting a bow at a target 50 meters away should always present an Average Challenge, regardless of whether the character is positioned on a hill, facing strong winds or using a broken weapon.* -->

<!-- Include an other example about persuasion? -->

<!-- By default, all Action Rolls have a Challenge of 10. The GM may also hide this value from players if they feel like it fits the situation. They may even go as far as to ask for a simple Insight or Knowledge Roll/ability check to reveal it. -->


### - Assessing Risk

Also set by the GM, **Risk** indicates potential danger, uncertainty or threat associated with the action. This value represents the likelihood of a troublesome outcome; with the greater the likelihood, the lower the value:

|Risk       |Rating |Description |
|:---------:|:-----:|------------|
|Safe       |4      |The action presents minimal danger, granting the character a good opportunity to act. <!--Examples include shooting atop a fortification through a murder hole, striking a crippled adversary or breaking into an unguarded warehouse. -->|
|Risky      |3      |The action is hazardous or involves potential harm, forcing the character to gamble their outcome. <!--Examples include handling volatile explosives, attempting to traverse a crumbling bridge or trying to deceive a guard to infiltrate a restricted area. -->|
|Precarious |2      |The action hangs on a knife's edge, with the character facing imminent trouble. <!--Examples include confronting a powerful enemy head-on, running accross a booby trapped minefield or trying to rescue hostages from a burning building. -->|

<!-- The examples written here have been copy-pasted from Blades in the Dark. -->

<!-- By default, all Action Rolls have a Risk of 3. The GM may also hide this value from players if they feel like it fits the situation. -->

Depending on the circumstances, **Risk** can be placed into one of four categories:

- **Narrative Risk** : plot-related consequences, involving complications or setbacks that impact the adventure or current situation <!-- some of which could induce Stress on the character..? -->.

- **Physical Risk** : consequences related to the physical well-being of the character, including loss of **Health** caused by injuries or disease.

- **Material Risk** : consequences related to possessions or resources, including the loss or damage of items, equipment or valuables.

- **Social Risk** : consequences pertaining to social rank, status and interactions, such as damaged reputation or loss of *respect*/*sympathy* from other characters.

<!-- Time Risk : consequences related to time, including missing a deadline or falling prey to the night. -->

<!-- The GM should never inflict a consequence that completelly negates the success of an action. -->

<!-- Don't inflict a complication that negates a successful roll. If a PC tries to corner an enemy and gets a 4/5, don't say that the enemy escapes. The player's roll succeeded, so the enemy is cornered... maybe the PC has to wrestle them into position and during the scuffle the enemy grabs their gun. -->


### - Choosing Abilitie Die

**Action Rolls** always combine two **Abilities**, based on the specifics of the action. For example, *agility* and *endurance* may be relevant for a climb roll, while *reasoning* and *creativity* may be relevant for a problem-solving roll. Here are a few other examples:

- **Brewing a potion** may require *knowledge* and *reasoning*, as it involves understanding the properties of various ingredients, as well as the proper techniques for preparing and mixing them.

- **Writing a poem** may require *eloquence* and *creativity*, as it involves using language and expression in imaginative ways to convey emotions and ideas.

- **Swimming** may require *agility* and *endurance*, as it involves the ability to move efficiently through water and sustain prolonged physical effort.

- **Painting** may require *creativity* and *dexterity*, as it involves the ability to express oneself through artistic mediums, and the manual skill to execute the desired strokes and forms.

- **Solving a puzzle** may require *reasoning* and *creativity*, as it involves analyzing available information and coming up with a unique solution.

- **Lockpicking** may require *dexterity* and *reasoning*, as it involves manipulating intricate mechanisms with precision, while also analyzing components and identifying potential vulnerabilities.

<!-- Walking across a wooden plank may require agility and composure, as it involves maintaining balance and control while navigating a narrow, precarious path. -->

Depending on their scores, these **Abilities** determine which class of die are rolled, with 2 die rolled for each **Ability** (4 in total).

>*For instance, if a character wants to swim accross a river, and they have an endurance score of 3 and an agility score of 1, they pick two d8s and two d4s. If they want to solve a puzzle, and they have a reasoning score of 2 and a creativity score of 2, they pick four d6s etc...*


### - Resolution

Resolving an **Action Roll** involves two steps: a standard roll followed by an optional **Daring Roll**, with players trying to maximize their **Success Score** while also minimizing **Trouble Die**:

- The player throws their four die.

- Results of 4 or more are added together to form the action's **Success Score**. Results of 3 are ignored and results of 1 and 2 are considered **Trouble Die**.

- The player can choose to end their action after the first throw, or proceed with a **Daring Roll**.

- Opting for the latter raises one of the character's **Emotion** by 1, and allows them to throw two additional die (one for each **Ability**), aiming to increase their **Success Score** while also risking additional **Trouble Die**.

>*After a Daring Roll, a player should have rolled 6 die in total. If not, they should have rolled only 4 die.*

<!-- Alternative : Opting for the latter raises one of the character's Emotion by an amount dependent on the roll's Challenge (Average = 1, Difficult = 2, Overwhelming = 3) -->

#### Raising Emotional Strain

As stated above, a character must raise an **Emotion** by one stage when attempting a **Daring Roll**. Which **Emotion** to raise is determined by context and the character's general mindset while performing the action:

| Emotion | Description                                                                 |
| :-----: | --------------------------------------------------------------------------- |
|  Anger  | The character is acting out of frustration, resentment, or animosity.       |
| Anxiety | The character is acting with apprehension, nervousness or insecurity.       |
| Sorrow  | The character is acting from a place of pessimism or half-heartedness.      |
|  Guilt  | The character is acting against their own values or engaging in misconduct.


### - Outcomes

Action outcomes depend on whether the **Success Score** matched or exceeded the action's **Challenge**, and/or if the number of **Trouble Die** matched or exceeded the action's **Risk**:

|                        |Success Score >= Challenge |Success Score < Challenge |
|:----------------------:|:-------------------------:|:------------------------:|
|**Trouble Die >= Risk** |Limited Success            |Failure                   |
|**Trouble Die < Risk**  |Success                    |Limited Failure           |

For example, a character is trying to steal a guarded item:

- **Success** : the character succeeds, they achieve their intended goal or result without consequences.

>*The character manages to steal the item and remains undetected.*

- **Limited Success** : the character succeeds but it comes at a cost. There's a setback, a complication or the action isn’t as effective as anticipated.

>*The character manages to steal the item but gets detected by guards.*

- **Limited Failure** : the character fails but there's no impactful consequence. The GM may allow them to try again (after some time, or with increased **Risk**) or decide that the opportunity slips away.

>*The character fails at stealing the item but remains undetected.*

- **Failure** : the character fails and there's a consequence. A new threat appears, they lose control of the situation or suffer some form of harm.

>*The character fails at stealing the item and gets detected by guards.*

#### Critical Outcomes

- A **Critical Success** happens when a player manages to exceed the action's **Challenge** by 10 points (or more) with their **Success Score**.

<!-- A Critical Success happens when a player manages to double the action's Challenge with their Success Score. -->

- A **Critical Failure** happens when a player scores 2 **Trouble Die** (or more) over the action's **Risk**. This counts as a *failure* with a more severe consequence.




## Example


Finn attempts to pick the lock of a chest to steal valuable items. The GM decides that the action requires *dexterity* and *reasoning*, with a **Challenge** of 10 (it's a *challenging* lock) and a **Risk** of 3 (guards are patrolling the area, it's a *risky* move).

- The player rolls two *d6s* and two *d4s* (based on Finn's *dexterity* of 2 and *reasoning* of 1).

- They get results of 4, 2, 3 and 2. 4 counts towards their **Success Score**, both 2s count as 2 **Trouble Die** and 3 is ignored.

- Finn has a **Success Score** of 4 (not enough to succeed) and 2 **Trouble Die** (not enough to alert the guards), thus the player can choose to end on a *limited failure*. But instead they decide to push their luck with a **Daring Roll**.

- After raising their *anxiety* by 1 (Finn is scared of being caught), they roll another two die (one *d6* and one *d4*) and get results of 6 and 1. 6 is added to the total **Success Score** (now 10), and 1 counts as another **Trouble Die** (now 3).

- Since the **Success Score** matched the **Challenge** (10) and the total number of **Trouble Die** also matched the **Risk** (3), the action ends in a *limited success*. The GM tells the player that Finn succeeded at picking the lock, but a guard heard them and is now walking towards their location.

***








# **Action Rolls (Part 2)**

***




## Advantage & Disadvantage


Sometimes, rules may specify that a character must perform an **Action Roll** using **Advantage** or **Disadvantage**:

- If a character rolls with **Advantage**, results of 3 are not ignored and count towards the total **Success Score**.

- If a character rolls with **Disadvantage**, results of 3 are not ignored and count as **Trouble Die**.

>*If a character has multiple instances of Advantage and Disadvantage, each Advantage cancels out one Disadvantage.*

<!-- Once per day, if a character is not in a Breakdown state, they may add 1 Stress to roll with Advantage on any Action roll thay want (or turn a roll with Disadvantage into a neutral roll). This is to show that characters are able to "force motivate" themselves into doing things that are not always aligned with their goals. -->

#### Drive Actions

Characters roll with **Advantage** if they perform an **Action Roll** aligned with at least one of their **Drives** (this is called a **Drive Action**):

- **Authority** : when addressing an obstacle through confrontation or intimidation.

<!-- https://www.youtube.com/watch?v=sYCJIwAkvxs&t=377s -->

- **Revenge** : when attempting vindication or retaliation against a perpetrator.

- **Wealth** : when tackling a challenge where money or valuables are at stake.

- **Hedonism** : when engaging in a gratifying experience or trying to fulfil a desire.

- **Distinction** : when trying to stand out while addressing an obstacle.

- **Influence** : when trying to solve a problem with persuasive communication or manipulation.

<!-- - **Privilege** : when leveraging their status, connections or when receiving exclusive benefits. -->

- **Solution** : when addressing an obstacle with rationale, technical skill or strategic thinking.

- **Production** : when creating things, like building a contraption or designing a piece of art.

- **Curiosity** : when venturing into the unknown or trying to gain information.

- **Care** : when providing assistance or support for those in need.

- **Peace** : when mediating disputes or trying to resolve conflicts through reconciliation.

- **Duty** : when carrying out responsibilities to their family, gods or companions.

<!-- This move also gives XP to the character...? -->

On the other hand, characters roll with **Disadvantage** if their action goes against at least one of their **Drives** (or current **Quest**).

<!-- or when going against an **Emotion** causing a **Breakdown** state as well? -->

<!-- ### - Example ? -->

<!-- Blades in the Dark Playbooks: -->

<!-- - When you play a Cutter, you earn xp when you address a challenge with violence or coercion. Go ahead and get up in everyone’s smug faces and tell them who’s boss, then let your blades do the talking if they don’t get the message. -->

<!-- - When you play a Hound, you earn xp when you address a challenge with tracking or violence. Take the initiative to hunt down opportunities and targets for a score and be willing to hurt whoever stands in your way. -->

<!-- - When you play a Leech, you earn xp when you address a challenge with technical skill or mayhem. Duskwall is a city full of industrial machinery, clockworks, plumbing, and electrical systems for you to bend to your purposes or sabotage. Get out your tools and get your hands dirty. -->

<!-- - When you play a Lurk, you earn xp when you address a challenge with stealth or evasion. Stay out of sight, sneak past your enemies, and strike from the shadows. If things go wrong, there's no shame in disappearing into the darkness... for now. Your greatest ally is the dark and twisting city, its ink-dark streets, its rooftop pathways. -->

<!-- - When you play a Slide, you earn xp when you address a challenge with deception or influence. Talk your way into trouble, then talk your way out again. Pretend to be someone you're not. Bluff, lie, and manipulate to get your way. Every problem is ultimately a problem because of people—and people are the instrument upon which you play your music. -->

<!-- - When you play a Spider, you earn xp when you address a challenge with calculation or conspiracy. Reach out to your contacts, friends, and associates to set your crew up for success. Use your downtime activities and flashbacks wisely to prepare for trouble and to calculate the angles of success. When things go wrong, don't panic, and remember: you planned for this. -->

<!-- - When you play a Whisper, you earn xp when you address a challenge with knowledge or arcane power. Seek out the strange and dark forces and bend them to your will. By being willing to face the trauma from the stress-intensive abilities of the arcane, you'll slowly remove parts of yourself, and replace them with power. -->




## Context Modifiers


**Context Modifiers** represent different benefits or drawbacks affecting a character during a roll. They stem from circumstances and add to the total **Success Score** of the action:

- **Easing Modifiers** are positive values that increase the total **Success Score** of a roll. They represent advantageous circumstances.

- **Trying Modifiers** are negative values that decrease the total **Success Score** of a roll. They represent detrimental circumstances.

>*For example, if a character has a Modifier of -2, they turn an action with a total Success Score of 15 into a Success Score of 13. Conversely, if they have a Modifier of +3, they turn an action with a total Success Score of 15 into a Success Score of 18.*

<!-- If Modifiers manage to reduce the Challenge to 0 (or below), the character automatically succeeds their roll. -->


### - Weight

**Context Modifiers** exist in three different **Weights**, indicating their level of impact. A lower **Weight** means a smaller bonus or penalty, while a higher **Weight** means a greater bonus or penalty:

| Weight      |  Value   | Description                                                                                                  |
| ----------- | :------: | ------------------------------------------------------------------------------------------------------------ |
| Minor       | -2 or +2 | The **Modifier** is marginal and doesn't significantly affect the action's outcome.                          |
| Significant | -3 or +3 | The **Modifier** has a noticeable impact, shifting the balance towards a particular outcome.                 |
| Decisive    | -4 or +4 | The **Modifier** is crucial in determining the outcome, making it the defining factor in success or failure. |


### - Sources

**Context Modifiers** are also classified into **Sources** that describe where the **Modifier** comes from. If a character gains **Modifiers** from multiple **Sources**, these are considered cumulative and stacked with each other. But if multiple **Modifiers** have the same **Source**, they do not stack. In that case only the **Modifier** with the greatest **Weight** counts. **Sources** exist in four categories:

#### Environment

*Environment* represents the physical or social space in which the action takes place. This includes tangible aspects like physical structures or weather, as well as intangible aspects like faction territory or cultural expectations of a location. Here are some examples:

- **Minor Environment Modifier** (*+2*) : *the character is trying to persuade someone in a suitable social setting (like a tavern or a cosy garden).*

- **Significant Environment Modifier** (*+3*) : *the character is fighting from a high ground position.*

- **Decisive Environment Modifier** (*-4*) : *the character is dealing with strong headwinds while shooting with a bow*.

<!-- Easing Environment : the character is fighting from a high ground position (-3), they are attempting a stealth maneuver at night (-4), they are located in a social setting suitable for persuasion (like a tavern or a cosy garden, -2), they are using snow-covered terrain to track enemies or prey (-3) etc... -->

<!-- Trying Environment : the character is fighting in a cramped space (+2), they are navigating an unknown area with confusing topography (+3), they are dealing with strong headwinds while trying to shoot with a bow (+4), they are challenging the words of a noble in their own castle (+3) etc... -->

#### Actors

*Actors* refer to the individuals or entities involved, either aiding or opposing the character's action. These can include friends, people with conflicting interests or bystanders whose behavior may impact the outcome of the roll. Here are some examples:

- **Minor Actors Modifier** (*+2*) : *the character is participating in a scuffle where the number of allies slightly outnumbers the number of opponents.*

- **Significant Actors Modifier** (*+3*) : *the character is trying to break down a sturdy door with the help of a few companions.*

- **Decisive Actors Modifier** (*-4*) : *the character is attempting to intimidate a bandit leader while surrounded by their entire crew.*

<!-- Easing Actors : -->

<!-- the character is supported by a group of skilled allies, they have a healer nearby providing continuous aid, they are joined by a powerful creature or summoned entity, they are surrounded by friendly townsfolk cheering them on etc... -->

<!-- Trying Actors : the character is attempting to intimidate a group of thugs alone, they are fighting outnumbered, they are trying to contain a herd of animals by themselves -->

#### Means

*Means* refer to the various assets available or unavailable to a character, such as weapons, tools, pieces of information or any other resource that can facilitate or hinder their action:

- **Minor Means Modifier** (*+2*) : the character knows about an enemy's weak spot during a fight.

- **Significant Means Modifier** (*-3*) : the character is working on a project with broken, improvised or makeshift tools.

- **Decisive Means Modifier** (*+4*): the character is negotiating terms with a tribal leader using their ancestral tongue.

<!-- Easing Means : the character is using a map to navigate unfamiliar terrain, they know about an enemy's weak spot during a fight, they are attempting to bribe someone with a substantial amount of coins, they are cutting down a tree with an exceptionally good axe etc... -->

<!-- Trying Means : the character is fighting with a broken weapon, they're attempting to convince someone using inaccurate or outdated information, they are working with improvised or makeshift tools, they are facing a language barrier in a negotiation etc... -->

<!-- If I ever implement "Disposition Modifiers", they should be considered as "Means Modifiers". New "Sympathy Scale" : Hate, Dislike, Tepid/Reluctant, Neutral, Keen/Appreciate, Like, Adore -->

#### Condition

*Condition* describes the overall state of the character, factoring in their fatigue, carry load, hygiene, drug influence or any other comfort or affliction that may help or hinder them:

- **Minor Condition Modifier** (*+2*) : the character is attempting to persuade someone while bathed and perfumed.

- **Significant Condition Modifier** (*-3*) : the character is attempting an acrobatic maneuver while drunk.

- **Decisive Condition Modifier** (*-4*) : the character is fighting an enemy while exhausted and sleep-deprived.

<!-- Easing Condition : the character is well-rested and energized, they have recently received a beneficial magical blessing, they're attempting to persuade someone while bathed, well-dressed and wearing perfume, they are under the influence of a powerful and positive elixir etc... -->

<!-- Trying Condition : the character is exhausted and sleep-deprived, they are attempting an acrobatic maneuver while burdened by heavy equipment, they are fighting an enemy while suffering from a debilitating illness, they are trying to persuade someone while drunk etc... -->

<!-- If I ever implement "Racial/Nature Modifiers", they should be considered as "Condition Modifiers". -->




## Opposed Rolls


**Opposed Rolls** are made when two characters or entities directly compete against each other, each rolling their own **Action Roll** to determine who can achieve the greatest **Success Score** (with **Risk** and relevant **Context Modifiers** still applying).

#### Steps

- Both players throw their initial 4 die together.

- Each player then chooses if they want to continue with a **Daring Roll** or not. Do not perform the second throw until both players have made their choice.

- Both players perform their **Daring Roll** (if any) at the same time.

- The player with the highest **Success Score** wins the contest. However, anyone who matches or exceeds the **Risk** must also face a negative consequence.

<!-- The Riddle of Steel : Contested Rolls ("Contests of ") are made whenever two characters or forces are competing for precedence. Examples include trying to defend yourself from an attacker, sneaking past a guard (or listening for such), and arm wrestling. Any of the above tests may be contested against any other: one player might roll a Skill Test while the second might roll an Attribute Test in opposition. In such instances both parties roll against their own Target Numbers as determined by the individual circumstances of their given Test. Successes are then tallied up; he with the most successes is the winner—the other is the loser. -->




## Experience


As stated in a previous chapter, each non-*crippled* **Ability** is accompanied with its own **XP** track. A player marks this track when performing an **Action Roll** with a **Daring Roll**, and obtaining the top number on a die associated with one of the two **Abilities**<!-- used for the roll-->. Unless it's a *crippled* **Ability**, each top result marks 1 **XP** for the **Ability** associated with it, regardless of the outcome.

<!-- If a player performs an Action Roll with a Daring Roll using Abilities rated d6 and d8, they mark 1 XP to the first Ability for each 6 obtained on a d6, and 1 XP to the second Ability for each 8 obtained on a d8. -->

#### Example

Finn wants to sneak behind a group of bandits and disarm their leader before a fight breaks out:

- The GM sets a **Challenge** of 15, a **Risk** of 2 and picks *dexterity* (score 2) and *agility* (score 3) for the action. This gives the player a set of d6s and d8s for their roll.

- After performing a standard roll followed by a **Daring Roll**, the results are 6-2-1 on d6s and 5-3-3 on d8s. The action ends with a **Success Score** of 11 and 2 **Trouble Die**. <!-- *Guilt* was raised as Finn used to know the bandits as his friends. -->

- Since the **Success Score** did not match the **Challenge** but the total number of **Trouble Die** did match the **Risk**, the action resolves in a *failure*. As Finn tries to slip through the shadows, a twig snaps under his foot, alerting the bandits to his presence.

- Yet, because the player rolled a 6 on a d6 and decided to perform a **Daring Roll**, Finn marks 1 **XP** to the **Ability** associated with that result, here being *dexterity*.

#### Leveling Up

When a character fills an **XP** track entirely, its associated **Ability** score increases by 1 and the track is reset back to 0.

<!-- OLD IDEA : Abilities level up when characters attempt a daring roll (or obtain the highest result on a die?) with a specific challenge rating: -->

<!-- |Ability Die |Required Challenge |Required result | -->
<!-- |:-----------|:------------------|:---------------| -->
<!-- |d6          |2+ (3+?)           |6               | -->
<!-- |d8          |3+ (4?)            |8               | -->
<!-- |d10         |4  (4?)            |10              | -->

***








# **Resistance Rolls**

***




**Resistance Rolls** represent a character's attempt at withstanding undesirable events, rather than them actively initiating a course of action. Players may call for one anytime they want to negate something affecting their character; with the roll always succeeding provided the character isn't in a **Breakdown** state. For instance, if a character is struck by a surprise attack, they may perform a **Resistance Roll** to dodge the blow.

>*However, the decision on what can or can't be resisted is left at the GM's discretion, with the right to deny a player's request if deemed inappropriate. The GM may also suggest a player to perform a Resistance Roll at any point during a game, but cannot enforce it.*

<!-- This is when a character assumes a reactive position against a situation or circumstance. -->

<!-- Players always drive the action, making all the dice rolls. -->

<!-- Old Resistance Roll stats idea : nimbleness / reflexes / likeability / sharpness / restraint / vigor -->


### - Choosing an Ability

If a **Resistance Roll** is initiated, the GM must first pick an appropriate ability for it. Here are some examples:

- **Dexterity** : when reacting instinctively to danger.

- **Agility** : when dodging incoming threats or attacks.

- **Knowledge** : when refuting attempts at persuasion based on misinformation or quackery.

- **Insight** : when refuting attempts at persuasion based on trickery or lies.

- **Composure** : when refuting temptation or seduction.

- **Endurance** : when resisting complications arising from wounds or diseases.

Depending on its score, this **Ability** determines which class of die is rolled.


### - Assessing Pressure

**Pressure** measures how much strain is being exerted on the character. It's a representation of the level of effort or resolve needed to resist the event:

|Pressure |Rating |Example |
|:-------:|:-----:|--------|
|Mild     |1-2    |The burden on the character is manageable. |
|Moderate |3-4    |The burden is challenging and demands some effort to withstand. |
|Tough    |5-6    |The burden significantly challenges the character's capabilities. |
|Severe   |7-8    |The burden is overwhelming and pushes the character to their limits. |
|Extreme  |9-10   |The burden is crushing, leaving the character on the brink of collapse. |

The GM sets this value, which in turn determines the number of die rolled by the player.


### - Resolution

The player rolls a number of **Ability** die equal to the roll's **Pressure**:

- Results of 1 and 2 are considered **Trouble Die**.

- Results of 3 are rolled again.

- The character raises one of their **Emotions** by the number of **Trouble Die** obtained, and the event or consequence is negated.

<!-- Anticipation/Alertness : if their character isn't in a Breakdown state, a player can choose to "predict" a future negative event. If so, they may raise one of their Emotions by 1 and, if the "predicted" event happens, their character succesfully resists it without having to perform a Resistance Roll. -->

<!-- When your PC suffers a consequence that you don’t like, you can choose to resist it. Just tell the GM, “No, I don’t think so. I’m resisting that.” Resistance is always automatically effective—the GM will tell you if the consequence is reduced in severity or if you avoid it entirely. Then, you’ll make a resistance roll to see how much stress your character suffers as a result of their resistance. -->

<!-- ### - Example -->

<!-- Spire - The City must Fall: -->

<!-- Stunned (Blood) : You take a blow to the head, or are winded, giving your enemies opportunity to act. If they want to get away from you, they can do so while you stagger about and gather your senses; otherwise, you can’t use the Fight skill to earn additional dice for the remainder of the situation. -->

<!-- Compromised (Shadow) : A friendly NPC asks you to justify your strange behaviour. -->

<!-- Pawned (Silver) : Until the end of the next session, you lose the use of one piece of equipment that’s important to you. -->

<!-- Knocked Out (Blood) : You fall unconscious for several hours, during which time your enemies get an advantage. -->

<!-- Leak (Moderate) : Your bond unwittingly gives out information that threatens the operation. Mark D6 stress in Shadow. -->

<!-- Passive Rolls (or "Ability Checks") work like Resistance Rolls, except they don't incur negative outcomes for the character if failed, and the character can't "negate" their failure by raising their Emotional Strain. -->

<!-- Insight : used when sensing ulterior motives or hidden agendas in people (without actively engaging in conversation). -->

#### Raising Emotional Strain

As stated above, characters must raise an **Emotion** when obtaining **Trouble Die**. Which **Emotion** to raise depends on context, and the character's general mindset at the time of the roll:

| Emotion | Description                                                  |
| :-----: | ------------------------------------------------------------ |
|  Anger  | The character feels irritated or wronged by the event.       |
| Anxiety | The character feels threatened, vulnerable or in danger.     |
| Sorrow  | The character feels jaded, overwhelmed or powerless.         |
|  Guilt  | The character feels remorse or responsibility for the event.

<!-- Anger : attributing the cause of harm to someone else and believing they can still influence the situation through assertiveness or confrontation -->

<!-- Frustration : the character feels irritated due to unexpected events, unwanted tasks, or outcomes that didn't go their way. Their response is usually one of annoyance or bitterness. -->

<!-- Insecurity : the character perceives themselves as inadequate, doubting their own abilities due to underestimating an obstacle or feeling embarrassed in a social situation. -->

<!-- Falling prey to a thief : a situation where a minor theft occurs, involving the stealing of coins from a character. While somewhat inconsequential, it can still generate a sense of distress and insecurity to the character. -->

<!-- Social embarrassment : a character experiences a situation where they are publicly embarrassed or humiliated, such as being the target of a practical joke in front of important figures. This can lead to a significant damage to their self-esteem. -->

<!-- Fear : the character senses danger, perceiving it as insurmountable or mysterious. They feel like they can't deal with it or comprehend its origin or nature. -->

<!-- Guilt : the character feels responsible for their own actions, acknowledging wrongdoing or believing that the current situation arose from their own choices. -->

<!-- Spire - The City Must Fall : Sometimes it will be stated outright what kind of stress a situation does out – for example, when a Lajhan casts a spell, most often they’ll be asked to mark stress against Mind as they channel the vast energies of their goddess. If it’s not clear where stress would go, the GM and the player can work it out together. -->

<!-- Example of Anger : a situation where a character perceives like they are being treated unfairly or if they get robbed of their belongings while walking in a city. -->

<!-- Example of Insecurity : being anti-theist and participating in a wedding where religious people are having lots of fun. People not giving you due attention or respect etc... -->

<!-- Anger : the character is convinced someone or something else is responsible for the stress increase and feels like they can still influence the situation or cope with it. -->
<!-- Shame : the stress increase was caused by an event that was initiated by the character's own choices and feels like they cannot influence the situation or cope with it. -->
<!-- Anxiety : the character is convinced someone or something else is responsible for the stress increase and feels like they cannot influence the situation or cope with it. -->
<!-- Guilt : the stress increase was caused by an event that was initiated by the character's own choices and feels like they can still influence the situation or cope with it. -->


### - Example

Finn wants to evade a shot performed by a rival character:

- Considering Finn's animosity towards their rival, the GM states that this may raise Finn's *anger*, and test their *agility* against a **Pressure** of 2. The player accepts.

- Finn has an agility score of 3, so the player rolls two *d8s*.

- They get results of 4 and 2, with 2 counting as one **Trouble Die**. Finn's *anger* is increased by 1 and the shot is succesfully evaded.




## Context Modifiers


**Context Modifiers** may also affect **Resistance Rolls**, by directly increasing or decreasing the roll's **Pressure**:

- **Easing Modifiers** become negative values that decrease the number of die rolled.

- **Trying Modifiers** become positive values that increase the number of die rolled.

The amount decreased or increased depends on the **Modifier**'s **Weight**:

|Weight      |Value                 |
|:----------:|:--------------------:|
|Minor       |-1 or +1 **Pressure** |
|Significant |-2 or +2 **Pressure** |
|Decisive    |-3 or +3 **Pressure** |

>*For example, a character behind cover might receive a Significant Environment Modifier (-2) to dodge a shot.*

**Sources** remain the same and stack with each other. But if multiple **Modifiers** come from the same **Source**, only the one with the greatest **Weight** applies. And if **Context Modifiers** reduce the **Pressure** of a roll to 0 or below, the character succeeds without throwing any die.

<!-- #### Example? -->




<!-- ## Resisting Consequences


**Resistance Rolls** can be used to cancel out negative consequences resulting from *limited successes* and *failures* (converting them into *successes* and *limited failures*, respectively). If so, the character uses the **Ability** corresponding to the **Emotion** used (or hypothetically used) for their **Daring Roll**, and **Pressure** is set by the GM depending on how strong the consequence is:

|Daring Roll's Emotion |Resistance Roll's Ability |
|:--------------------:|:------------------------:|
|Anger                 |Humility                  |
|Frustration           |Insight                   |
|Anxiety               |Composure                 |
|Guilt                 |Composure                 |

>*As usual, the GM has the discretion to determine whether a consequence can be resisted or not.*

#### Example

Let's return to the previous situation. Finn achieved a *limited success* while picking a lock, inadvertently alerting a nearby guard. The GM tells the player that they may perform a **Resistance Roll** against a **Pressure** of 2 (the guard is only checking on some noise) using Finn's *composure* (Finn is scared of being caught). The player accepts:

- Finn's *composure* score is 1, so they roll two d4s.

- They get results of 4 and 3. Finn's *limited success* is upgraded to a *success* and, because there's no **Trouble Die**, there's no increase in *anxiety* either. The GM tells the player that Finn successfully picked the lock and did not alert a single guard. -->




## Targeted Rolls


Sometimes, a character may want to direct an **Action Roll** against another character, who in turn will try to resist it with a **Resistance Roll**. This sequence is called a **Targeted Roll**.

>*Characters initiating a Targeted Roll are called Instigators, whereas those who are the focus of it are referred as Targets.*

<!-- A Targeted Roll is a deliberate action in which one character directs their efforts against another character. -->

#### Resolution

- The **Instigator** performs an **Action Roll** in which they combine two **Abilities**, and where only **Risk** is assessed.

- The **Target**, on the other hand, performs a **Resistance Roll**, and picks a single **Ability** deemed appropriate to resist the **Instigator**'s action.

- The **Instigator**'s total **Success Score** is then divided by 3 to set the **Pressure** of the **Target**'s **Resistance Roll** (rounded down).

Regardless of outcome, setbacks and complications still apply to the **Instigator**. **Modifiers** apply to both characters, except for *environment*, which applies only to the **Instigator**.

<!-- "Impossible" Targeted Checks = cannot be performed : trying to sneak on someone on broad daylight without any place to hide, trying to seduce someone while drunk or dirty etc... -->

#### Example

Finn attempts to pickpocket another character, Eldrin:

1. Finn performs an **Action Roll**, combining their *dexterity* and *agility*. They achieve a total **Success Score** of 11.

2. To avoid getting snatched, Eldrin performs a **Resistance Roll** using their *dexterity*. The **Pressure** for the roll is 3 -after dividing Finn’s **Success Score** by 3 (rounded down).

3. Eldrin has a _dexterity_ score of 2, and thus rolls 3d6. They get results of 3, 5 and 2 (1 **Trouble Die**).

4. Eldrin thwarts Finn’s pickpocketing, but they also raise their *anger* by 1 point for the **Trouble Die** they rolled.

<!-- ## Support Rolls -->

<!-- Support Rolls are used for helping other characters recover Health and Stress : Target's Resistance Roll = 4 (6?) - Instigator's Success Score. -->




## Experience


As with **Action Rolls**, a player marks 1 **XP** to an **Ability** when obtaining the top result on a die (marking 1 **XP** for each top result obtained).

>*For instance, if a player performs a Resistance Roll using an Ability rated d6, they mark 1 XP to that Ability for each 6 obtained during the roll.*

<!-- #### Example? -->

<!-- Subsequently, the GM allows the player to resist the consequence of the action against a Pressure of 3 using Finn's insight: -->

<!-- Finn's insight score is 2, so they roll three d6s. -->

<!-- They get results of 6, 6 and 2, with 2 counting as 1 Trouble Die. His frustration is increased by 1 and Finn's failure is upgraded to a limited failure. The GM tells the player that Finn second-guesses their action, retreats and no one notices him. -->

<!-- And because the player rolled two 6s, Finn also marks 2 XP to the Ability associated with the roll, here being insight. -->

***








# **Character State**

***




During a game, players track their character's physical and mental state using their **Health**, **Emotional Strain** and **Stress**:

- **Health** represents a character's physical condition. <!-- and decreases as they sustain injuries or endure physical damage - with low levels indicating a risk of death -->

- **Emotional Strain** represent a character's increasing psychological pressure.

- **Stress** represents a character's deteriorating mental condition. <!-- or morale -->

<!-- Emotional Strain generally covers situations that may cause annoyance, fatigue or unease to a character, but do not have long-lasting effects on their psychological well-being. -->

<!-- A **Breakdown** state occurs when an **Emotion** exceeds its maximum, forcing the character to raise their **Stress** when making rolls. -->

<!-- with high Despair indicating a risk of abandoning the group forever -->

<!-- Despair illustrates a permanent loss of morale, resulting from events that directly impact a character's sense of self-worth, individuality, or deeply held values. -->

<!-- A "pool" serves as a resource or reserve that is depleted or consumed, whereas a "stack" represents a cumulative value or number that is added or accumulated. -->




## Health


If a character suffers physical damage, whether from combat or environmental hazards, their **Health** decreases. Injuries, ranging from minor wounds to broken limbs, may also impose a **Trying Condition Modifier** on most rolls, depending on their severity:

|Health reduction|Effects                   |
|:--------------:|--------------------------|
|1-2             |No effects                |
|3-4             |*-2* Condition Modifier   |
|5-6             |*-3* Condition Modifier   |
|7+              |*-4* Condition Modifier   |

<!-- Scratch : a shallow wound caused by a sharp object, like a blade or a thorny bush. -->

<!-- A wound that may cause bleeding and stinging pain, but it is generally manageable and does not pose a significant threat. -->

<!-- Sprain : an injury to a ligament caused by the twisting of a joint. -->

<!-- A strong, painful injury that, with proper rest and care, can heal without long-term consequences. -->

<!-- Burn : damage caused by exposure to heat or fire. Burns can cause extensive damage to the skin, tissues, and nerve endings. They often lead to substantial pain, risk of infection and scarring. -->

<!-- Fracture : a broken bone resulting from a significant impact or force applied to a specific area. -->

<!-- A broken bone resulting from a powerful impact, causing potential long-term disabilities. -->

<!-- Internal organ damage : critical damage to a character's internal organs, such as from a powerful blow or impalement. -->

<!-- Critical damage to a character's internal organs, leading to severe internal bleeding and compromised organ function. -->

- A character may restore their **Health** through various methods, ranging from self-healing techniques to potions or medicines. Seeking help from professionals may offer more advanced treatments.

>*The severity of the condition determines the level of intervention needed.*

<!-- |Health recovery |Example                                                | -->
<!-- |:---------------|-------------------------------------------------------| -->
<!-- |1               |Herbal Medicine : using the healing properties of various plants to treat injuries or illnesses. Herbal remedies can be applied topically, consumed orally, or used in the form of poultices, salves, or teas. | -->
<!-- |2               |Healing Bandages : the application of specially prepared poultices or bandages infused with medicinal substances directly onto wounds or affected areas. These poultices may contain herbs, oils, or other ingredients that aid in healing and preventing infection. | -->
<!-- |3               |Therapeutic Techniques : the application of specialized therapeutic techniques or modalities to promote healing and recovery. These techniques can include physical therapy, massage therapy, acupuncture, chiropractic adjustments, or other forms of hands-on or non-invasive treatments. | -->
<!-- |4               |Alchemical Potions : potions created through alchemical processes, which can have a variety of healing effects. These potions may restore health, cure specific ailments, or provide temporary boosts to the body's natural healing abilities. | -->
<!-- |5               |Surgery : a complex medical procedure that involves the use of instruments to repair or remove damaged tissues, organs, or body parts. Surgery is typically performed by skilled surgeons and can address a wide range of injuries or ailments. | -->

- Characters must eat and sleep at least once a day. If these needs are ignored for too long, the GM may apply **Trying Condition Modifiers**, or reduce character **Health** based on the duration of the neglect.

>*The GM decides how to enforce this rule.*

#### Character Death

When a character's **Health** goes below 0, their injuries have surpassed their body's capacity to sustain harm. The character is considered dead, their life force extinguished.




<!-- ## Fatigue -->

<!-- Fatigue represents how long a character has been awake or without food: -->

<!-- - Once per day, if a character does not eat at least a full meal, they must add 1 point to their Fatigue counter. -->

<!-- - Similarly, if they don't get a full night of sleep (at least 5 hours), they also add 1 point to their Fatigue counter. -->

<!-- For instance, if a character goes a full day and night without both eating and sleeping, they must add 2 points to their Fatigue (1 point for lack of food + 1 point for lack of sleep). -->


<!-- ### - Fatigue Roll -->

<!-- Fatigue may affect a character by applying a Trying Condition Modifier to most of their rolls. Additionally, at the end of each day, characters must perform a Fatigue Roll to determine if they lose any Health due to accumulated exhaustion. A Fatigue Roll functions like a Resistance Roll, but instead of increasing an Emotion when obtaining Trouble Die, the character risks losing Health. d6s are used, and the Pressure is determined by how much Fatigue the character has at the time of the roll: -->

<!-- |Fatigue |Effects | -->
<!-- |:------:|--------| -->
<!-- |0-2     |No effects | -->
<!-- |3-6     |Condition Modifier (-2). Daily Fatigue Roll with a Pressure of 2 | -->
<!-- |x-x     |Condition Modifier (-x). Daily Fatigue Roll with a Pressure of x | -->
<!-- |x-x     |Condition Modifier (-x). Daily Fatigue Roll with a Pressure of x | -->
<!-- |x-x     |Condition Modifier (-x). Daily Fatigue Roll with a Pressure of x | -->
<!-- |x-x     |Condition Modifier (-x). Daily Fatigue Roll with a Pressure of x | -->

<!-- no food (between 8 and 21 days) : https://www.healthline.com/health/food-nutrition/how-long-can-you-live-without-food -->

<!-- no sleep (11 days) : https://health.clevelandclinic.org/how-long-can-you-go-without-sleep -->

<!-- #### Example -->


<!-- ### - Recovery -->

<!-- Both eating and sleeping a full night clears a character's Fatigue entirely. -->




## Emotional Strain


As seen before, **Emotions** are raised when a character either chooses to perform a **Daring Roll**, or gains **Trouble Die** during a **Resistance Roll**. But it can also be incurred at the GM's discretion, depending on events or circumstances, and by triggering an **Emotional Roll**:

- Based on the unfolding of the narrative, the GM selects an **Emotion** and a **Strain** value (indicating both type and intensity).

- The player rolls a number of **Ability Die** equal to the **Strain** value. Which **Ability** to use depends on the **Emotion** chosen (*Humility* for *Anger*, *Insight* for *Anxiety* and *Composure* for both *Sorrow* and *Guilt*).

- Each result of 1 or 2 increases the character’s **Emotion** by 1 point.

#### Examples

| Strain | Emotion | Ability   | Description                                                                    |
| :----: | :------ | :-------- | :----------------------------------------------------------------------------- |
|  1-3   | Guilt   | Composure | The character accidentally injures an ally.                                    |
|  4-6   | Anxiety | Insight   | The character feels something crawling up their leg in the pitch-black tunnel. |
|  7-10  | Anger   | Humility  | An enemy kills the character’s loyal companion in cold blood.                  |

>*Emotional Rolls should be used sparingly, only when they impact the story or the character.*

<!-- #### Tension Roll -->

<!-- To ensure characters pursue their Quest, require a daily Resistance/Emotional roll if no progress is made towards it. The Pressure for the roll increases with the number of days of no substantial progress on the Quest. -->

<!-- Tension Rolls should only be performed if Tension is greater than Fatigue? -->


### - Recovery

If left unchecked, **Emotional Strain** may lead a character into a **Breakdown** state. However, there are a few ways for them to relieve their **Emotions** if needed:

#### Succeeding a Drive Action

Once per day, a character may clear all **Emotional Strain** by successfully performing a **Drive Action** (*see Action Rolls - Part 2*).

<!-- Once per day, a character may clear 2 points of their Emotional Strain by successfully performing a Drive Action (3 if they achieved a Critical Success). This also gifts the character a +2 Condition Modifier for the next 24 hours (+3 if they achieved a Critical Success). -->

<!-- Make it so Drive Actions don't clear Emotions and instead just provide a +2 Condition Modifier for the next 24 hours? -->

#### Addressing an Emotion

A character may clear all **Emotional Strain** by addressing the **Emotion** currently closest to its maximum threshold. This means taking a meaningful action using that **Emotion**, and influence the course of the story in some way or another:

<!-- A character may clear all of their Emotional Strain by addressing the Emotion currently closest to its maximum threshold. This means they should do something impactful related to that Emotion, and whatever they do should affect the course of the story -->

|Emotion |Action description |
|:------:|:-----------|
|Anger   |The character must hurt someone or break something important, either physically or emotionally, and whether willingly or by accident. |
|Anxiety |The character must desert the group at a critical moment, or take a foolish action that puts the group in a difficult situation. |
|Sorrow  |The character must take a temporary leave from the group, for a full day at least, while the plot progresses without them. |
|Guilt   |The character must make an important sacrifice to redeem themselves, like giving up something of value or yielding to someone else's will. <!-- abandoning a long-held belief? --> |

>*What is considered meaningful or impactful is left to the GM's discretion.<!-- If this requires performing an Action Roll, the character also rolls with Advantage.-->*

<!--

|Emotion |Action description |
|:------:|:-----------|
|Anger   |The character must hurt someone or break something important. |
|Anxiety |The character must desert the group at a critical moment. |
|Sorrow  |The character must take a temporary leave from the group {2}. |
|Guilt   |The character must make an important sacrifice to redeem themselves. |

>*{2} This should count for a full day at least, while the plot progresses without them.*

-->

#### Rewarding <!--or satisfying -->Event

Things beyond the character’s control may relieve their **Emotional Strain**, at the GM’s discretion, and based on the story or world events. For example:

| Recovery | Description                                                                         |
| :------: | :---------------------------------------------------------------------------------- |
|    1     | The character overhears townsfolk praising their deeds.                             |
|    2     | The character gets a full night's sleep in a very comfortable and cozy environment. |
|    3     | The character hears news of a key enemy leader being overthrown.                    |

<!-- The character stumbles upon a gorgeous scenery after a day of rain. -->

>*When relieving Emotional Strain, players should always start removing points from the Emotion with the most strain first.*

<!-- #### Taking a Break -->

<!--

- A character may remove up to 1 point of **Emotional Strain** after **Taking a Break** (~1 hour at least, and limited to once per day); or 2 points if the break was accompanied by a particularly entertaining activity (like eating a good meal, witnessing a musical performance etc...).

- They also remove up to 2 points after a **Full Night's Sleep** (~5 hours at least); or 3 points if the sleeping environment was particularly comfortable.

-->

<!-- Additionally, they may remove up to 1 point after Eating a Meal (limited to once per day), or 2 points if the meal was particularly consistent. -->

<!-- Characters should be able to recover Emotional Strain in downtime or by bonding with each other? The amount of emotional recovery should be dependent on the overall Empathy score for the group? (+Emotional Breakdowns should reduce the recovery rate -characters with a low Empathy score should even increase Emotional stress during downtime?) -->

<!-- Temporary stress may be alleviated through simple activities such as entertainment or resting. By consuming nourishing food or engaging in activities that bring them comfort and pleasure, the character can effectively find relief and regain a sense of calm and balance. Taking breaks and unwinding allows a character's mind and body to recuperate, shedding the burdensome layers of fatigue and restore their emotional well-being. -->

<!-- |Temp. stress removal|Example                                                | -->
<!-- |:-------------------|-------------------------------------------------------| -->
<!-- |1                   |Short rest : taking a power nap to catch a breath, stretch, and relax for a brief period. This allows the character to recover a small amount of their weariness. | -->
<!-- |2                   |Listening to music : immersing themselves in melodies and rhythms, whether by attending a live performance or playing an instrument, uplifts a character's mood and provides a temporary escape from their worries. | -->
<!-- |3                   |Finding solace in nature : taking a walk in a peaceful forest on a quiet sunny day, sitting by a serene lake, or observing the beauty of nature can have a calming effect on the character's psyche. | -->
<!-- |4                   |Enjoying a favorite food or drink : indulging in a character's favorite treat can offer momentary comfort and pleasure, alleviating a substantial amount of temporary stress. | -->
<!-- |5                   |Good night's sleep : enjoying a full night of uninterrupted sleep, allowing the body to rest and recover fully is probably the best way to recover from temporary stress and rejuvenate both physically and mentally. | -->


## Breakdown


When an **Emotion** goes over its maximum threshold, it triggers a **Breakdown** state. A character is now subjected to a specific set of rules:

<!-- Whenever a character's Emotion goes over its maximum threshold, the character enters a Breakdown state.  They cannot perform Resistance Rolls, none of their Emotions can be raised or relieved anymore, and they are subjected to a specific set of rules: -->

<!-- Smallville - Stressing Out (Corebook p.60) : Stressing Out can mean many things. It may simply mean you fall unconscious. It may mean you collapse in doubt and angst. It may mean you stalk out of the room before you hurt someone. It may mean you stare out a window plotting revenge. When you Stress Out, it’s up to you to decide what it means in the story. Whatever the details, though, you’re out for the rest of the scene. -->

<!-- Stress/Panic table mechanic found in Alien RPG: https://www.youtube.com/watch?v=H16lBiBSd-I -->
<!-- Check DATA/Resources/GAMEPLAY/TTRPG inspirations/[TO READ]/Year Zero Engine/ -->
<!-- https://online.anyflip.com/aeoqf/frgt/mobile/index.html -->
<!-- https://www.drivethrurpg.com/product/293976/ALIEN-RPG-Core-Rulebook?cPath=27806_34036 -->
<!-- https://www.legrog.org/jeux/alien -->

- They roll with **Disadvantage** on actions related to that **Emotion**:

| Emotion | Action description                                                 |
| :-----: | :----------------------------------------------------------------- |
|  Anger  | If they're trying to help, comfort or support someone.             |
| Anxiety | If they're trying to directly engage an obstacle or threat.        |
| Sorrow  | If they're trying to problem-solve or focus on intellectual tasks. |
|  Guilt  | If they're trying to hurt or provoke someone.                      |

- **Resistance Rolls** can only succeed if no **Trouble Die** are rolled. If there's at least one **Trouble Die**, the roll fails and the undesirable event happens -and the character still suffers **Emotional Strain**.

- They make things difficult for the people around them. Nearby allies suffer a *-2* **Actors Modifier** on most of their rolls. <!-- and cannot Take a Break to relieve their Emotions? -->

#### Raising Stress

In a **Breakdown** state, **Emotional Strain** is replaced with **Stress**. Meaning that whenever a rule requires raising an **Emotion**, the character's **Stress** increases by the same amount instead.

- For instance, each time the player opts for a **Daring Roll**, they must now increase their character's **Stress** by 1.

- With **Resistance Rolls** and **Emotional Rolls**, the amount of **Stress** gained depends on the number of 1s and 2s rolled.

- If a character is not in a **Breakdown** state but their **Emotional Strain** happens to exceed an **Emotion**'s maximum threshold, each excess point increases their **Stress** by 1. If this happens after a **Resistance Roll**, the roll fails and the undesirable event still occurs.

<!-- Provide Example? -->

#### Overcoming a Breakdown

To get rid of a **Breakdown** state, a character may:

- **Address the Emotion** that caused it (see *Emotional Recovery* above).

- Fulfill their current **Quest**. This has the added benefits of clearing all of their accumulated **Stress**, and provide a *+3* **Condition Modifier** to most rolls until the end of the adventure.

Both options clear all **Emotional Strain** from the character as well. And while in a **Breakdown** state, they may not relieve **Emotional Strain** through any other means.

<!-- stress can also be recovered through character bonding and/or therapy. -->

<!-- A character may clear all of their Emotional Strain and remove their Breakdown (if applicable) if they manage to significantly advance in their current Quest. This also provides the character with a +2 Condition Modifier on most rolls for the next 3 days. -->

#### Character Fallout

When a character's level of **Stress** reaches or overcome their current level of **Health**, they are taken out of action. They cannot continue as an adventurer and must retire to a different life.

***








# **Progress Clocks**

***




A **Progress Clock** is a gauge (or circle) divided into sections used to trace almost anything. This could be plot progression, time management, NPC health or any other variable essential to the unfolding of the narrative. It can be compared to a "progress bar" found in video games, but adapted to suit a variety of situations.

>*Progress Clocks transform narrative outcomes into something tangible that players can grasp, whether it's overcoming an enemy, escaping danger or fulfilling an objective.*

The greater the **Progress Clock**, the more sections it holds:

|Progress Clock |Sections |
|:-------------:|:-------:|
|Standard       |2        |
|Large          |4        |
|Huge           |6        |
|Mighty         |8        |

A **Progress Clock** should always reflect what's happening in the fiction, allowing players to check how they’re doing. For example:

- **Event Clocks** are used to track the progression of time. They can represent the countdown to an event, the arrival of reinforcements or the duration of a spell. Each segment should mark a specific time interval, enabling players to anticipate future developments or prepare accordingly.

- **Obstacle Clocks** are used to track ongoing effort against a challenge. Characters tick segments when succeeding **Action Rolls** aiming at overcoming the challenge, until the clock is filled and the obstacle is no more. Unless it's a *critical success*, a single action cannot fill more than one segment at a time.

<!-- When you create a clock, make it about the obstacle, not the method. The clocks for an infiltration should be “Interior Patrols” and “The Tower,” not “Sneak Past the Guards” or “Climb the Tower.” The patrols and the tower are the obstacles— the PCs can attempt to overcome them in a variety of ways. -->

- **Threat Clocks** are used to monitor impending trouble or danger. They can symbolize growing suspicion, the proximity of pursuers or the alert level of guards. Segments are ticked when complications arise or characters mess up, like when an action ends on a *limited success* or a *failure* (with *critical failures* ticking more than one segment at a time).

- **Combat Clocks** are used to track the progression of combat encounters. They can represent a number of enemies, their morale or even their armor. Segments  are ticked when characters perform successful attacks on their adversaries and, unless there's a *critical success*, a single attack cannot fill more than one segment at a time.

- **Influence Clocks** are used to represent the effort required to sway an opinion, negotiate a deal or achieve an objective through persuasion, intimidation or manipulation. Segments can be ticked based on the narrative, or through the success of social actions aimed at influencing the target.

- **Project Clocks** are used to monitor the advancement of a long-term task or endeavor. This could be building a stronghold, learning a skill, writting a book or crafting an artifact. Segments should represent the various stages of the project, with each tick marking a significant milestone towards completion.

>*Progress Clocks are not required for every situation. The GM should use them for challenges where tracking advancement is essential. Otherwise, they should opt for resolving outcomes with a single Action Roll.*

<!-- Faction Clock : each faction has a long-term goal. When the PCs have downtime, the GM ticks forward the faction clocks that they’re interested in. In this way, the world around the PCs is dynamic and things happen that they’re not directly connected to, changing the overall situation in the city and creating new opportunities and challenges. The PCs may also directly affect NPC faction clocks, based on the missions and scores they pull off. Discuss known faction projects that they might aid or interfere with, and also consider how a PC operation might affect the NPC clocks, whether the players intended it or not. -->

<!-- Spellcasting Clock -->

<!-- When you create a clock, make it about the obstacle, not the method. The clocks for an infiltration should be “Interior Patrols” and “The Tower,” not “Sneak Past the Guards” or “Climb the Tower.” The patrols and the tower are the obstacles —the PCs can attempt to overcome them in a variety of ways. -->

<!-- https://www.reddit.com/r/bladesinthedark/comments/u2na85/help_with_understanding_progress_clocks -->

<!-- https://www.reddit.com/r/rpg/comments/bft68t/using_clocks_world_bitd_in_any_game/ -->

***
