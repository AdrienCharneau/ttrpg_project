# BLADES IN THE DARK




## Progress Clocks


>p.24


A progress clock is a circle divided into segments (see examples at right). Draw a progress clock when you need to track ongoing effort against an obstacle or the approach of impending trouble.

>*Sneaking into the Bluecoat Watch tower? Make a clock to track the alert level of the patrolling guards. When the PCs suffer consequences from partial successes or missed rolls, fill in segments on the clock until the alarm is raised.*

Generally, the more complex the problem, the more segments in the progress clock.

A complex obstacle is a 4-segment clock. A more complicated obstacle is a 6-clock. A daunting obstacle is an 8-segment clock.

The effect level of an action or circumstance is used to tick segments on a clock (see Effect Levels, page 24). It’s the GM’s job to tick a clock so it reflects the fictional situation. If the PCs are making a lot of progress, the clock should be ticked a lot. This comes with practice, by properly judging effect levels. But you should always feel free to adjust a clock in play to better reflect the situation.

You can’t usually fill a clock with the effect of a single action. This is by design. If a situation is simple enough for one action, don’t make a clock, just judge the outcome based on the effect level of the action.

When you create a clock, make it about the obstacle, not the method. The clocks for an infiltration should be “Interior Patrols” and “The Tower,” not “Sneak Past the Guards” or “Climb the Tower.” The patrols and the tower are the obstacles —the PCs can attempt to overcome them in a variety of ways.

Complex enemy threats can be broken into several “layers,” each with its own progress clock. For example, the Lampblacks’ HQ might have a “Perimeter Security” clock, an “Interior Guards” clock, and a “Bazso’s Office Security” clock. The crew would have to make their way through all three layers to reach Bazso’s personal safe and the whiskey collection within.

Remember that a clock tracks progress. It reflects the fictional situation, so the group can gauge how they’re doing. A clock is like a speedometer in a car. It shows the speed of the vehicle—it doesn’t determine the speed.

Examples of progress clocks follow. This is not an exhaustive list. Use them as you see fit!


### - Simple Obstacles

Not every situation and obstacle requires a clock. Use clocks when a situation is complex or layered and you need to track something over time —otherwise, resolve the result of an action with a single roll.


### - Danger Clocks

The GM can use a clock to represent a progressive danger, like suspicion growing during a seduction, the proximity of pursuers in a chase, or the alert level of guards on patrol. In this case, when a complication occurs, the GM ticks one, two, or three segments on the clock, depending on the consequence level. When the clock is full, the danger comes to fruition—the guards hunt down the intruders, activate an alarm, release the hounds, etc. (See Consequences, page 30.)


### - Racing Clocks

Create two opposed clocks to represent a race. The PCs might have a progress clock called “Escape” while the Bluecoats have a clock called “Cornered.” If the PCs finish their clock before the Bluecoats fill theirs, they get away. Otherwise, they’re cornered and can’t flee. If both complete at the same time, the PCs escape to their lair, but the hunting Bluecoats are outside!

You can also use racing clocks for an environmental hazard. Maybe the PCs are trying to complete the “Search” clock to find the lockbox on the sinking ship before the GM fills the “Sunk” clock and the vessel goes down.


### - Linked Clocks

You can make a clock that unlocks another clock once it’s filled. For example, the GM might make a linked clock called “Trapped” after an “Alert” clocks fill up. When you fight a veteran warrior, she might have a clock for her “Defense” and then a linked clock for “Vulnerable.” Once you overcome the “Defense” clock, then you can attempt to overcome the “Vulnerable” clock and defeat her. You might affect the Defense clock with violence in a knife-fight, or you lower her defense with deception if you have the opportunity. As always, the method of action is up to the players and the details of the fiction at hand.


### - Mission Clocks

The GM can make a clock for a time-sensitive mission, to represent the window of opportunity you have to complete it. If the countdown runs out, the mission is scrubbed or changes—the target escapes, the household wakes up for the day, etc.


### - Tug-of-War Clocks

You can make a clock that can be filled and emptied by events, to represent a back-and-forth situation. You might make a “Revolution!” clock that indicates when the refugee Skovlanders start to riot over poor treatment in Doskvol. Some events will tick the clock up and some will tick it down. Once it fills, the revolution begins. A tug-of-war clock is also perfect for an ongoing turf war between two crews or factions.


### - Long-term Project

Some projects will take a long time. A basic long-term project (like tinkering up a new feature for a device) is eight segments. Truly long-term projects (like creating a new designer drug) can be two, three, or even four clocks, representing all the phases of development, testing, and final completion. Add or subtract clocks depending on the details of the situation and complexity of the project.

A long-term project is a good catch-all for dealing with any unusual player goal, including things that circumvent or change elements of the mechanics or the setting. For example, by default in the game, trauma is permanent. But maybe a player wants to work on a project where they create a device to draw traumatic spiritenergies into the ghost field, thus reducing a character’s trauma and unleashing a storm of enraged ghosts in the area. It will be a long and dangerous process to set up everything needed to begin and work on a project like this, but almost anything can be attempted as long as the group is interested and it seems feasible to everyone.


### - Faction Clocks

Each faction has a long-term goal (see the faction write-ups, starting on page 283). When the PCs have downtime (page 145), the GM ticks forward the faction clocks that they’re interested in. In this way, the world around the PCs is dynamic and things happen that they’re not directly connected to, changing the overall situation in the city and creating new opportunities and challenges.

The PCs may also directly affect NPC faction clocks, based on the missions and scores they pull off. Discuss known faction projects that they might aid or interfere with, and also consider how a PC operation might affect the NPC clocks, whether the players intended it or not.


### - Progress Clocks in play

- **Infiltrating Strangford House** : The scoundrels are sneaking in to Lord Strangford's house to steal his personal log book (in which he keeps the secret maps and hunting methods for his leviathan hunter ship—worth a small fortune to the right buyer). The GM makes a progress clock for the alert level of Strangford's personal staff and bodyguards. She makes a 4-clock because it's a single house, not a sprawling estate—only a few suspicious events will rouse the whole place. During the operation, Silver rolls to Prowl through the first floor and rolls a 4/5. She gets past, but the complication is a tick on the “Alert” clock. The GM ticks it once to represent the threat level of the kitchen staff downstairs—they're not trained security, so limited effect is called for. Later, when Cross rolls a 1-3 on a desperate action to sneak into Strangford's private suite, the GM fills three segments—the Lord's bodyguards are Tier IV professionals and are experts at spotting trouble. This fills the clock! When Cross eases the door open, he's set upon by the first pair of bodyguards, while the other two attempt to hustle Strangford (and his precious book!) out the back way.


- **Assaulting the Red Sashes** : The scoundrels attack the lair of the Red Sashes, in a final showdown to see which group will survive to control the drug market in Crow's Foot. The GM makes a clock for the forces of each gang. As the PCs take actions and suffer consequences, the GM ticks the clocks to show the waning strength and morale of each side. When one side's clock is filled, they've reached a breaking point—will they flee, surrender, or fall into a suicidal rage?




## Action Roll


>p.27


When a player character does something challenging, we make an action roll to see how it turns out. An action is challenging if there's an obstacle to the PC's goal that's dangerous or troublesome in some way. We don't make an action roll unless the PC is put to the test. If their action is something that we'd expect them to simply accomplish, then we don't make an action roll.

Each game group will have their own ideas about what “challenging” means. This is good! It's something that establishes the tone and style of your Blades series.

To make an action roll, we go through six steps. In play, they flow together somewhat, but let's break each one down here for clarity.

1. The player states their goal for the action.

2. The player chooses the action rating.

3. The GM sets the position for the roll.

4. The GM sets the effect level for the action.

5. Add bonus dice.

6. The player rolls the dice and we judge the result.


### - 1) The Player states their goal

Your goal is the concrete outcome your character will achieve when they overcome the obstacle at hand. Maybe your goal is “I want to get into the manor house” or it might be “I want to see who comes and goes at the house.” In both cases, the obstacle is “the house guard patrol.” The guards are the challenging obstacle that may be dangerous or troublesome for the PC.

Usually the character's goal is pretty obvious in context, but it's the GM's job to ask and clarify the goal when necessary.

>*“You're punching him in the face, right? Okay... what do want to get out of this? Do you want to take him out, or just rough him up so he'll do what you want?”*


### - 2) The Player chooses the action rating

The player chooses which action rating to roll, following from what their character is doing on-screen. If you want to roll your Skirmish action, then get in a fight. If you want to roll your Command action, then order someone around. You can't roll a given action rating unless your character is presently performing that action in the fiction.

There's definitely some gray area here, where actions overlap and goals can be attempted with a variety of approaches. This is by design. If your goal is to hurt someone with violence, you might Skirmish or Hunt or Prowl or Wreck, depending on the situation at hand. If your goal is to dismay and frighten an enemy, you might Command or Sway or Wreck. It's the player's choice.


### - 3) The GM sets the position

Once the player chooses their action, the GM sets the position for the roll. The position represents how dangerous or troublesome the action might be. There are three positions: controlled, risky, and desperate. To choose a position, the GM looks at the profiles for the positions below and picks one that most closely matches the situation at hand.

|Positions   |Description                                                                                               |
|:----------:|:---------------------------------------------------------------------------------------------------------|
|Controlled  |You have a golden opportunity. You're exploiting a dominant advantage. You're set up for success.         |
|Risky       |You go head to head. You're acting under duress. You're taking a chance.                                  |
|Desperate   |You're in serious trouble. You're overreaching your capabilities. You're attempting a dangerous maneuver. |

By default, an action roll is risky. You wouldn't be rolling if there was no risk involved. If the situation seems more dangerous, make it desperate. If it seems less dangerous, make it controlled.

Choosing the position is an important judgment call and stylistic choice for your game. More controlled action rolls mean generally safer and more dominant scoundrels. More desperate action rolls give the game a gritty, underdog kind of feel. There's no ironclad rule about how to choose positions. It's meant to be an expressive element of the game. Make the choice that feels right to you and the rest of the group. If you're ever unsure about which position to pick, ask the other players.

As a player, if you're angling for a particular position, ask the GM what you might do to get it, or ask them to clarify the situation to explain their choice.

>*“It's risky? I was thinking it'd be controlled. I know this barkeep is supposed to be a tough old buzzard, but he's not a real threat to me, is he?”*

>*“No, I think the danger is in the situation instead. You're out in the common room in front of everyone, ordering the barkeep to hand over the protection money that he's supposed to pay the Grinders. Who knows if someone in the room might decide to step up and play hero, or to get on the Grinders’ good side? It's an uncertain environment. Maybe if you confront the guy alone, that's more of a controlled position for you. Or maybe if you bring the gang with you and make a show of force in front of everyone.”*

As GM, you have final say over the position for the roll, but explain and clarify things as needed, especially when you're starting out. By discussing the position (and how it might be better or worse) you'll help everyone build a better view of the fictional situation in their minds' eye and get on the same page about the tone of the game.

You'll also set precedents that the players can build on to make better decisions in the future. “Ah, so we got a controlled Consort roll when we wined and dined them and showed them how friendly we are. Noted.”


### - NPC Threat Levels

The severity of the consequences that you describe (and the position for the action roll) determines the threat level of the opposition. If they’re facing an NPC that you’ve described as a standard thug, then the consequences will be things like wrestling the PC to the ground, or punching them in the face, or maybe cracking a rib with her blackjack if the thug has the upper hand.

If they’re facing an NPC that you’ve described as a master assassin, then the consequence might be a lightning-fast move that puts a dagger in the PC’s heart.

Since NPCs don’t have stats and action ratings, it’s by the severity of their consequences and the position and effect of the PC’s action roll that their capabilities are manifest in the game.

Also, a dangerous NPC can take the initiative. If the NPC is skilled, tell the players what the NPC is about to accomplish, then ask them what they do.

>*“She corners you at the top of the stairs and tries to wrestle you into the manacles. What do you do?”*

The PC’s next move—and the action roll that results—will tell us how effective the NPC’s maneuver was. Remember, an action roll usually serves double duty, resolving both PC and NPC actions at the same time.

If the NPC is a master, tell the players what the NPC has already done, then ask if they want to resist it.

>*“She’s just way too fast for you to deal with. Before you realize what’s happening, the manacles are on your wrists. Yeah? Or do you want to resist that?”*

This is something that will grow and evolve over time as you play the game. Talk to the group about it as you go. Do you want a more deadly, high-stakes, kind of game? Then PCs and NPCs should threaten lethal harm all the time, and resistance rolls should only reduce consequences. Do you want a more cinematic, adventurous kind of game? Then describe dangers with less dire consequences. People are “left for dead.” Explosions make you look cool when you dive out of the way.

You don’t have to decide on one approach and stick to it. Be flexible and let it change over time according to what the group is most excited about.


### - 4) The GM sets the effect level

The GM assesses the likely effect level of this action, given the factors of the situation. Essentially, the effect level tells us “how much” this action can accomplish: will it have limited, standard, or great effect? Effect level is explained in detail in the next section, starting on page 24.

>*The GM's choices for effect level and position can be strongly influenced by the player's choice of action rating. If a player wants to try to make a new friend by Wrecking something—well... maybe that's possible, but the GM wouldn't be crazy to say it's a desperate roll and probably limited effect. Seems like Consorting would be a lot better for that. The players are always free to choose the action they perform, but that doesn't mean all actions should be equally risky or potent.*


### - Setting Position & Effect

The GM sets position and effect for an action roll at the same time, after the player says what they're doing and chooses their action. Usually, Risky / Standard is the default combination, modified by the action being used, the strength of the opposition, and the effect factors.

When you first start learning the game, you might step through the process with some deliberation, but after a bit of practice, you'll be able to set position and effect with a quick “gut feeling” that can then be tweaked if a PC has a particular ability or item or some other element to consider as a special case.

The ability to set position and effect as independent variables gives you nine combinations to choose from, to help you convey a wide array of fictional circumstances.

>*For example, if a scoundrel is facing off alone against a small enemy gang, the situation might be:*

> - *She fights the gang straight up, rushing into their midst, hacking away in a wild Skirmish. In this case, being threatened by the larger force lowers her position to indicate greater risk, and the scale of the gang reduces her effect (Desperate / Limited).*

> - *She fights the gang from a choke-point, like a narrow alleyway where their numbers can't overwhelm her at once. She's not threatened by several at once, so her risk is similar to a one-on-one fight, but there's still a lot of enemies to deal with, so her effect is reduced, but (Risky / Limited).*

> - She doesn't fight the gang, instead trying to maneuver her way past them and escape. She's still under threat from many enemy attacks, so her position is worse, but if the ground is open and the gang can't easily corral her, then her effect for escaping isn't reduced (Desperate / Standard). If she had some immediate means of escape (like leaping onto a speeding carriage), then her effect might even be increased (Desperate / Great).

> - *The gang isn't aware of her yet—she's set up in a sniper position on a nearby roof. She takes a shot against one of them. Their greater numbers aren't a factor, so her effect isn't reduced, and she's not immediately in any danger (Controlled / Great). Maybe instead she wants to fire off a salvo of suppressing fire against the whole gang, in which case their scale applies (Controlled / Limited). If the gang is on guard for potential trouble, her position is more dangerous (Risky / Great). If the gang is alerted to a sniper, then the effect may be reduced further, as they scatter and take cover (Risky / Limited). If the gang is able to muster covering fire while they fall back to a safe position, then things are even worse for our scoundrel (Desperate / Limited).*


### - 5) Add bonus dice

You can get up to two bonus dice for your action roll. For one bonus die, you can get assistance from a teammate. They take 1 stress, say how they help you, and give you +1d. See Teamwork, page 134.

For another bonus die, you can either push yourself (take 2 stress) or you can accept a Devil's Bargain (you can't get dice for both, it's one or the other).


### - 6) Roll the dice and judge the result

Once the goal, action rating, position, and effect have been established, add any bonus dice and roll the dice pool to determine the outcome. (See the sets of possible outcomes, by position, on the next page.)

The action roll does a lot of work for you. It tells you how well the character performs as well as how serious the consequences are for them. They might succeed at their action without any consequences (on a 6), or they might succeed but suffer consequences (on a 4/5) or it might just all go wrong (on a 1-3).

On a 1-3, it's up to the GM to decide if the PC's action has any effect or not, or if it even happens at all. Usually, the action just fails completely, but in some circumstances, it might make sense or be more interesting for the action to have some effect even on a 1-3 result.

>*Oskarr Attunes to the demonic entity he found in the secret hold of the leviathan hunter ship. The roll is a 1-3. The GM could say that Oskarr fails to Attune to the demonic power, and the backlash from the desperate failure manifests as psychic harm, level 3. But it would be much more interesting if the attunement happened, and Oskarr was confronted with this horrific entity, mind-to-mind, right? Oskarr touches the horrible will of that ancient creature, and the twisting madness within it overwhelms him. He suffers level 3 psychic harm, sure, but also gets a new 6-clock: “Get Rid of These Nightmare Demonic Visions.”*

Each 4/5 and 1-3 outcome lists suggested consequences for the character. The worse your position, the worse the consequences are. The GM can inflict one or more of these consequences, depending on the circumstances of the action roll. Consequences are explained in detail on page 30. PCs have the ability to avoid or reduce the severity of consequences that they suffer by resisting them. See page 32 for details about resistance.

When you narrate the action after the roll, the GM and player collaborate together to say what happens on-screen. Tell us how you vault across to the other rooftop. Tell us what you say to the Inspector to convince her. The GM will tell us how she reacts. When you face the Red Sash duelist, what's your fighting style like? Etc.


### - Action Roll summary

- A player or GM calls for a roll. Make an action roll when the character performs a dangerous or troublesome action.

- The player chooses the action rating to roll. Choose the action that matches what the character is doing in the fiction.

- The GM establishes the position and effect level of the action. The choice of position and effect is influenced strongly by the player's choice of action.

- Add up to two bonus dice. 1) Assistance from a teammate. 2) Push yourself (take 2 stress) or accept a Devil's Bargain.

- Roll the dice pool and judge the outcome. The players and GM narrate the action together. The GM has final say over what happens and inflicts consequences as called for by the position and the result of the roll.


### - Outcome : Controlled

>*You act on your terms. You exploit a dominant advantage.*

|Action Roll |Description                                                                                                                                                                                                |
|:----------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|Critical    |You do it with increased effect.                                                                                                                                                                           |
|6           |You do it.                                                                                                                                                                                                 |
|4-5         |You hesitate. Withdraw and try a different approach, or else do it with a minor consequence: a minor complication occurs, you have reduced effect, you suffer lesser harm, you end up in a risky position. |
|1-3         |You falter. Press on by seizing a risky opportunity, or withdraw and try a different approach.                                                                                                             |


### - Outcome : Risky

>*You go head to head. You act under fire. You take a chance.*

|Action Roll |Description                                                                                                                                                                                                |
|:----------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|Critical    |You do it with increased effect.                                                                                                                                                                           |
|6           |You do it.                                                                                                                                                                                                 |
|4-5         |You do it, but there's a consequence: you suffer harm, a complication occurs, you have reduced effect, you end up in a desperate position.                                                                 |
|1-3         |Things go badly. You suffer harm, a complication occurs, you end up in a desperate position, you lose this opportunity.                                                                                    |


### - Outcome : Desperate

>*You overreach your capabilities. You're in serious trouble.*

|Action Roll |Description                                                                                                                                                                                                |
|:----------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|Critical    |You do it with increased effect.                                                                                                                                                                           |
|6           |You do it.                                                                                                                                                                                                 |
|4-5         |You do it, but there's a consequence: you suffer severe harm, a serious complication occurs, you have reduced effect.                                                                                      |
|1-3         |It's the worst outcome. You suffer severe harm, a serious complication occurs, you lose this opportunity for action.                                                                                       |


### - Double-duty Rolls

Since NPCs don’t roll for their actions, an action roll does double-duty: it resolves the action of the PC as well as any NPCs that are involved. The single roll tells us how those actions interact and which consequences result. On a 6, the PC wins and has their effect. On a 4/5, it's a mix—both the PC and the NPC have their effect. On a 1-3, the NPC wins and has their effect as a consequence on the PC.




## Effect


>p. 33


In Blades in the Dark, you achieve goals by taking actions and facing consequences. But how many actions does it take to achieve a particular goal? That depends on the effect level of your actions. The GM judges the effect level using the profiles on the facing page. Which one best matches the action at hand—great, standard, or limited? Each effect level indicates the questions that should be answered for that effect, as well as how many segments to tick if you’re using a progress clock.

|Effect Levels |Description                                                                                                    |Ticks |
|:------------:|:--------------------------------------------------------------------------------------------------------------|:-----|
|Great         |You achieve more than usual. How does the extra effort manifest? What additional benefit do you enjoy?         |3     |
|Standard      |You achieve what we'd expect as “normal” with this action. Is that enough, or is there more left to do?        |2     |
|Limited       |You achieve a partial or weak effect. How is your impact diminished? What effort remains to achieve your goal? |1     |


### - Assessing Factors

To assess effect level, first start with your gut feeling, given this situation. Then, if needed, assess three factors that may modify the effect level: potency, scale, and quality. If the PC has an advantage in a given factor, consider a higher effect level. If they have a disadvantage, consider a reduced effect level.

- **Potency** : The potency factor considers particular weaknesses, taking extra time or a bigger risk, enhanced effort from pushing yourself (p. 13), or the influence of arcane powers. The electrical discharge of a lightning hook is potent against a ghost. The supernatural powers of a ghost are potent against a human. An infiltrator is more potent if all the lights are extinguished and they move about in the dark.

- **Quality** / **Tier** : Quality represents the effectiveness of tools, weapons, or other resources, usually summarized by Tier. Fine items count as +1 bonus in quality, stacking with Tier.

>*Arlyn is picking the lock to a safehouse run by the Circle of Flame. Her crew is Tier I and she has fine lockpicks—so she’s effectively Tier II. The Circle are Tier III. Arlyn is outclassed in quality, so her effect will be limited on the lock.*

- **Scale** : Scale represents the number of opponents, size of an area covered, scope of influence, etc. Larger scale can be an advantage or disadvantage depending on the situation. In battle, more people are better. When infiltrating, more people are a hindrance.

When considering factors, effect level might be reduced below limited, resulting in zero effect—or increased beyond great, resulting in an extreme effect.

If a PC special ability gives “+1 effect,” it comes into play after the GM has assessed the effect level. For example, if you ended up with zero effect, the +1 effect bonus from your Cutter's Bodyguard ability would bump them up to limited effect. Also, remember that a PC can push themselves (take 2 stress) to get +1 effect on their action. See page 13.

For a master table of factor examples, see Magnitude on page 220. Every factor won’t always apply to every situation. You don't have to do an exact accounting every time, either. Use the factors to help you make a stronger judgment call—don't feel beholden to them.


### - Dominant Factor

If one effect factor overshadows the others, the side with that advantage dominates the situation. It doesn’t matter if you have a fine sword and extra effect if you try to fight 20 people at once. Their scale dominates the battle and you’re left with very limited effect, or no effect at all. The same principle applies to "impossible” actions.

>*Una wants to tear down a stone guard tower that the Silver Nails are using as a lair. She says, “I take my sledgehammer over there and I Wreck the thing, smashing it down stone by stone. Ha! I rolled a crit! Great effect!” Obviously, this isn’t possible. A person can’t smash down a stone tower with a sledgehammer. We know it’s inherently silly, like jumping over the moon. But this is also codified in the effect factors. The tower is dominant in quality, scale, and potency. Unless those factors are countered somehow, Una’s effect level is zero before she starts. No matter what she rolls for her action, she’ll have no effect. This concept is useful when assessing other very tough (but achievable) situations.*

>*Let’s imagine instead that Una is facing a demon. She wants to Skirmish with it, by engaging it with her sword and dagger. This is similar to knocking down the tower with the sledgehammer. Even on a critical, the GM says, “You manage to land a solid blow against the creature’s skull, but there’s no wound and your hand throbs with the impact of steel against its scaly hide.” In other words, zero effect! (On a 1-3, the GM might say, “The creature swats your sword aside, clutches you in its hideous grip, and breaks your spine in two.” Seriously. Don’t mess with demons.)*

>*But this situation isn’t entirely hopeless. There must be some way to battle a demon. This is where effect factors can help make sense of the situation. If the demon is dominant in quality, scale, and potency, then the PCs can try to understand the factors, and take actions to address them. What’s the demon vulnerable to? They can use that to remove its potency (and seize that advantage for themselves). What scale is it? They need to bring more troops. Etc.*

>*Effect factors are a way to codify the situation into a few key factors so it’s easier to talk about what needs to change in order to have the desired effect.*


### - Trading Position for Effect

After factors are considered and the GM has announced the effect level, a player might want to trade position for effect, or vice versa. For instance, if they're going to make a risky roll with standard effect (the most common scenario, generally), they might instead want to push their luck and make a desperate roll but with great effect.

This kind of trade-off isn't included in the effect factors because it's not an element the GM should assess when setting the effect level. Once the level is set, though, you can always offer the trade-off to the player if it makes sense in the situation.

>*“I Prowl across the courtyard and vault over the wall, hiding in the shadows by the canal dock and Lyssa's gondola.”*

>*“I don’t think you can make it across in one quick dash. The scale of the courtyard is a factor here, so your effect will be limited. Let’s say you can get halfway across with this action, then you’ll have to Prowl through the other half of the space (and the rest of the guards there) to reach the other side.”*

>*“I didn’t realize it was that far. Hmmm. Okay, what if I just go as fast as I can. Can I get all the way across if I make a desperate roll?”*

>*“Yep, sounds good to me!”*


### - Effects in the Fiction

Effects aren’t simply a matter of a level name or ticking clock segments. After the action roll, when you narrate the outcome, answer the effect questions by describing what happens “on screen.” The answers to the questions will tell the group what the new situation is like, creating a natural bridge to further actions.

For a simple action, the effect level determines the end result. Do you achieve your goal partially, fully, or with great effect? For a more complex obstacle, the GM creates a progress clock to track the effort made to overcome it. You tick a number of segments on the clock depending on the effect level of your action and the factors involved. When you fill the clock, the obstacle is overcome. See Progress Clocks, page 15.

>*For example, if the player says “I shove him and run away,” that might be a simple action. It will have limited, standard, or great effect on the enemy, resulting in a new situation. The enemy might be slowed down (limited), knocked off their feet and delayed in the chase (standard), or even injured by a powerful throw (great)—depending on the assessment of the factors at hand.*

>*If the player says, “I engage this guy in a knife fight to the death,” that might be a more complex obstacle. The GM creates a clock for the thug’s level of threat, then there are several action rolls to resolve the fight, each ticking the clock according to the effect level (and risking consequences from the outcome of each).*

Go with your gut and use simple or complex obstacles as you like, moment to moment in play. There’s no hard and fast rule for what’s “simple” or “complex.


### - Why we do this

The reason we assess effect is to set expectations and make the fictional situation more clear, so everyone is on the same page.

>*You tangle with the Hive enforcer, blade to blade. Do you inflict a grievous mortal wound? Do you only give them a shallow cut? Why are you having the effect that you have? How could it be worse? How could it be better?*

By assessing effect and describing it in the fiction, the players understand how much progress they're making and how much they're risking. By understanding effect, the group understands how many actions (and risk of consequences) will be needed to achieve their goals. Maybe a shallow cut is all you need to prove your point. Maybe nothing short of death will suffice. After each instance of action, effect, and consequences, the players know where they stand, and can make informed decisions about what to do next.

If you've played other roleplaying games or video games, you're probably familiar with the concept of “hit points” for a character or a progress bar during a boss fight. The effect system in Blades is this type of pacing mechanic, abstracted so it can apply to any type of situation, from fighting, to social manipulation, investigations, arcane powers, infiltration, whatever! Every action has an explicit effect that everyone playing the game can understand—either resolving the current situation so we can move on to the next one, or incrementing progress toward the current goal.


### - Consequences

When a PC suffers an effect from an enemy or a dangerous situation, it's called a consequence. Consequences are the companion to effects. PCs have effect on the world around them and they suffer consequences in return from the risks they face. See page 30 for details on consequences and how they impact the player characters.




## Consequences & Harm


>p. 39


Enemy actions, bad circumstances, or the outcome of a roll can inflict consequences on a PC. There are five types:

- **Reduced Effect**

- **Complication**

- **Lost Opportunity**

- **Worse Position**

- **Harm**

A given circumstance might result in one or more consequences, depending on the situation. The GM determines the consequences, following from the fiction and the style and tone established by the game group.


### - Reduced Effect

This consequence represents impaired performance. The PC's action isn’t as effective as they’d anticipated. You hit him, but it's only a flesh wound. She accepts the forged invitation, but she'll keep her eye on you throughout the night. You're able to scale the wall, but it's slow going—you're only halfway up. This consequence essentially reduces the effect level of the PC's action by one after all other factors are accounted for.


### - Complication

This consequence represents trouble, mounting danger, or a new threat. The GM might introduce an immediate problem that results from the action right now: the room catches fire, you're disarmed, the crew takes +1 heat from evidence or witnesses, you lose status with a faction, the target evades you and now it's a chase, reinforcements arrive, etc.

Or the GM might tick a clock for the complication, instead. Maybe there's a clock for the alert level of the guards at the manor. Or maybe the GM creates a new clock for the suspicion of the noble guests at the masquerade party and ticks it. Fill one tick on a clock for a minor complication or two ticks for a standard complication.

A serious complication is more severe: reinforcements surround and trap you, the room catches fire and falling ceiling beams block the door, your weapon is broken, the crew suffers +2 heat, your target escapes out of sight, etc. Fill three ticks on a clock for a serious complication.

Don't inflict a complication that negates a successful roll. If a PC tries to corner an enemy and gets a 4/5, don't say that the enemy escapes. The player's roll succeeded, so the enemy is cornered... maybe the PC has to wrestle them into position and during the scuffle the enemy grabs their gun.


### - Lost Opportunity

This consequence represents shifting circumstance. You had an opportunity to achieve your goal with this action, but it slips away. To try again, you need a new approach—usually a new form of action or a change in circumstances. Maybe you tried to Skirmish with the noble to trap her on the balcony, but she evades your maneuver and leaps out of reach. If you want to trap her now you'll have to try another way—maybe by Swaying her with your roguish charm.


### - Worse Position

This consequence represents losing control of the situation—the action carries you into a more dangerous position. Perhaps you make the leap across to the next rooftop, only to end up dangling by your fingertips. You haven't failed, but you haven't succeeded yet, either. You can try again, re-rolling at the new, worse position. This is a good consequence to choose to show escalating action. A situation might go from controlled, to risky, to desperate as the action plays out and the PC gets deeper and deeper in trouble.


### - Harm

This consequence represents a long-lasting debility (or death). When you suffer harm, record the specific injury on your character sheet equal to the level of harm you suffer. If you suffer lesser harm, record it in the bottom row. If you suffer moderate harm, write it in the middle row. If you suffer severe harm, record it in the top row. See examples of harm and the harm tracker, below.

Your character suffers the penalty indicated at the end of the row if any harm recorded in that row applies to the situation at hand. So, if you have “Tired” harm in the bottom row, you’ll suffer reduced effect when you try to run away from the Bluecoats. When you’re impaired by harm in the top row (severe harm, level 3), your character is incapacitated and can't do anything unless you have help from someone else or push yourself to perform the action.

If you need to mark a harm level, but the row is already filled, the harm moves up to the next row above. So, if you suffered standard harm (level 2) but had no empty spaces in the second row, you’d have to record severe harm (level 3), instead. If you run out of spaces on the top row and need to mark harm there, your character suffers a catastrophic, permanent consequence (loss of a limb, sudden death, etc... depending on the circumstances).
